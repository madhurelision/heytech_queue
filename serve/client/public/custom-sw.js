self.addEventListener('push', event => {
  //console.log('Push Event', event)
  const data = event.data.json()
  //console.log('New notification', data)
  // const options = {
  //   body: data.body,
  // }
  const options = {
    data: data.url,
    body: data.description,
    icon: data.image,
    vibrate: [200, 100, 200],
    tag: "new-product",
    image: data.image,
    renotify: true,
    badge: "https://spyna.it/icons/favicon.ico",
    actions: [{ action: "Detail", title: data.view, icon: "https://via.placeholder.com/128/ff0000" }],
  };

  event.waitUntil(
    self.registration.showNotification(data.title, options)
  );
})

self.addEventListener('notificationclick', function (event) {
  //console.log('notificationclick Event', event)
  // console.log('notificationclick Event', event.notification.data)
  event.notification.close();
  if (event.action === 'Detail') {
    // Archive action was clicked

    clients.openWindow((!event.notification.data || event.notification.data == null || event.notification.data == undefined || event.notification.data == "") ? 'https://google.com' : event.notification.data);
  }
}, false);
