import React, { useEffect } from 'react';
import './App.css';
import './assets/css/style.css'
import Routes from './pages/routes.js';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { PRODUCTION } from './config.keys.js';
import { RecoilRoot } from 'recoil';
import { DebugObserver } from './store/index.ts';
import { BrowserRouter as Router, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { customerProfileData, shopProfileData, getAllService, getAllTag, employeeProfileData, getAllTagMetaData, getIpHost } from 'redux/action';
import { FailedSnackbar, SuccessSnackbar } from 'pages/Shop/UsableComponent/SnackBar';
import ApiLoader from 'pages/Shop/UsableComponent/ApiLoader';

const App = () => {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
      },
    },
  });

  const openVal = useSelector(state => state.auth.open)
  const closeVal = useSelector(state => state.auth.close)
  const loadVal = useSelector(state => state.auth.loader)

  const dispatch = useDispatch()
  const user = useSelector(state => state.auth.isAuthenticated)
  const serviceD = useSelector(state => state.auth.serviceList)
  const tagD = useSelector(state => state.auth.tagList)
  const checkService = useSelector(state => state.auth)
  const hostD = useSelector(state => state.auth.hostName)

  useEffect(() => {

    if (localStorage.getItem('auth-token') && user == false && localStorage.getItem('type') == 'shop') {
      dispatch(shopProfileData())
    } else if (localStorage.getItem('auth-token') && user == false && localStorage.getItem('type') == 'customer') {
      dispatch(customerProfileData())
    } else if (localStorage.getItem('auth-token') && user == false && localStorage.getItem('type') == 'employee') {
      dispatch(employeeProfileData())
    }

  }, [user])

  useEffect(() => {

    if (serviceD.length == 0 && checkService.serviceRequestFailed == false) {
      dispatch(getAllService(0, 50))
    }

  }, [serviceD])

  useEffect(() => {

    if (tagD.length == 0) {
      dispatch(getAllTagMetaData())
    }

  }, [tagD])

  useEffect(() => {

    if (Object.keys(hostD).length == 0) {
      dispatch(getIpHost({ id: window.location.href }))
    } else {

    }

  }, [hostD])

  return (
    <QueryClientProvider client={queryClient}>
      <RecoilRoot>
        <Router>
          {!PRODUCTION && (
            <>
              <DebugObserver />
              <ReactQueryDevtools initialIsOpen />
            </>
          )}
          <Routes />
        </Router>
        {openVal && <SuccessSnackbar open={openVal} />}
        {closeVal && <FailedSnackbar open={closeVal} />}
        {loadVal && <ApiLoader />}
      </RecoilRoot>
    </QueryClientProvider>
  );
};

export default App;
