import axios from 'axios';
import { SERVER_URL, WebPushKey } from 'config.keys';


const convertedVapidKey = urlBase64ToUint8Array(WebPushKey)

function urlBase64ToUint8Array(base64String: any) {
  const padding = "=".repeat((4 - base64String.length % 4) % 4)
  // eslint-disable-next-line
  const base64 = (base64String + padding).replace(/\-/g, "+").replace(/_/g, "/")

  const rawData = window.atob(base64)
  const outputArray = new Uint8Array(rawData.length)

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i)
  }
  console.log('dd', outputArray)
  return outputArray
}

function sendSubscription(subscription: any) {
  console.log('text', subscription)
  return axios.post(SERVER_URL + '/api/subscribe', subscription)
}

export const subscribeUser = () => {
  console.log('subscriber called', 'serviceWorker' in navigator, "PushManager" in window, 'Notification' in window)
  if ('serviceWorker' in navigator && "PushManager" in window) {

    navigator.serviceWorker.register("/custom-sw.js");
    console.log(' subscriber true', navigator)
    Notification.requestPermission().then(async (cs) => {


      console.log('gggg', cs)
      if (cs !== 'granted') {
        console.log('Permission was not granted.')
        return
      } else {

        console.log('gggg', cs)
        const mainWorker = await navigator.serviceWorker.ready

        console.log('mainworker', mainWorker)

        await mainWorker.pushManager.getSubscription().then(async function (existedSubscription) {
          console.log('getSubs started')
          if (existedSubscription === null) {
            console.log('No subscription detected, make a request.')
            await mainWorker.pushManager.subscribe({
              applicationServerKey: convertedVapidKey,
              userVisibleOnly: true,
            }).then(function (newSubscription) {
              console.log('New subscription added.')
              sendSubscription(newSubscription)
            }).catch(function (e) {
              if (Notification.permission !== 'granted') {
                console.log('Permission was not granted.')
              } else {
                console.error('An error ocurred during the subscription process.', e)
              }
            })
          } else {
            console.log('Existed subscription detected.')
            sendSubscription(existedSubscription)
          }
        })
      }
    }).catch((err) => {
      console.error('An error ocurred during the subscription process.', err)
    })
  }
}