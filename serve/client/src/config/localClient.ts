import localforage from 'localforage'

localforage.setDriver([localforage.INDEXEDDB, localforage.WEBSQL, localforage.LOCALSTORAGE])

export const setItemStore = (key: any, value: any) => {
  localforage.setItem(key, value).then((cc) => {
    console.log(key, cc)
  }).catch((err) => {
    console.log('localforage setItem Err', err)
  })
}

export const getItemStore = (key: any) => {
  localforage.getItem(key).then((cc) => {
    console.log('get check', cc)
  }).catch((err) => {
    console.log('localforage getItem Err', err)
  })
}

export const removeItemStore = () => {
  localforage.clear().then(function () {
    console.log('Database is now empty.');
  }).catch(function (err) {
    console.log('localforage clear', err);
  });
}