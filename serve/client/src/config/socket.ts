import React from 'react';
import io from 'socket.io-client';
import store from '../redux/index'
import { SERVER_URL, SOCKET_URL } from 'config.keys';
import { SOCKET_CONNECT } from 'redux/action/types';

var socket = io({ reconnectionDelayMax: 10000, autoConnect: false });

var socketAuth = (token: any) => {
  console.log('request to connect');
  socket = io(SOCKET_URL, {
    reconnection: false,
    transports: ["websocket"],
    upgrade: false,
    // query: {
    //   token: token
    // }
  });
  store.dispatch({ type: SOCKET_CONNECT, payload: true })
  return socket;
};

export { socket, socketAuth };
var SocketContext = React.createContext({ socket, socketAuth });
export default SocketContext;