import Compressor from 'compressorjs';
import { DayEnumType } from 'types';

export const Languages = [
  'Hindi',
  'Punjabi',
  'Bengali',
  'Gujarati',
  'Tamil',
  'Telugu',
  'Marathi',
  'English'
];

export const ChartColors = [
  'red',
  'orange',
  'yellow',
  'lime',
  'green',
  'teal',
  'blue',
  'purple',
  'white',
];

export const getMyColor = () => {
  let n = (Math.random() * 0xfffff * 1000000).toString(16);
  return '#' + n.slice(0, 6);
};

export const MonthType = [
  'Monthly',
  'Yearly'
]

export const Seat = () => {
  return [...Array(11).keys()].slice(1)
}

export const SeatForm = (value: any) => {
  return [...Array(parseInt(value) + 1).keys()].slice(1)
}

export const EmogiItem = [
  '🧑', '🧑🏻', '💬', '💁🏻', '👋', '🌠', '📚', '📈', '🤝', '🤓', '🌱', '🤴', '🧨', '🌚', '🥧',
  '☕', '🙌', '👨‍🏫', '👯', '⚒️', '🧙‍♂️', '🙊', '💨', '👩‍💻', '🔮', '🔍', '⚡', '✊', '🚀',
];

export const keyLengthValidation = {
  otp: 6,
  description: 150,
  service_name: '',
  motivation_name: '',
  mobile: 10,
  size: 20,
  sizeLength: 2,
  start_time: 0,
  end_time: 24,
  pagination: 10,
}

export const handleWhatsapp = (mobile: any, url: any) => {
  var a = document.createElement('a')
  a.target = "_blank"
  a.href = `https://wa.me/+91${mobile}?text=${url}`
  a.click()
  console.log('link', `https://wa.me/${mobile}?text=${url}`)
}

export const ChangeNumber = (mobile: any) => {
  return (!mobile || mobile == null || mobile == undefined) ? '-----' : '+91' + mobile
}

export const minusDate = (start: Date, end: Date) => {
  var now = new Date(start).getTime();
  var later = new Date(end).getTime();
  var result: any = (now - later) / 60000;
  return (result > 0) ? true : false
}

export const ReviewTag = [
  {
    name: 'Tag 1',
    type: 'Tag 1'
  },
  {
    name: 'Tag 2',
    type: 'Tag 2'
  },
  {
    name: 'Tag 3',
    type: 'Tag 3'
  },
  {
    name: 'Tag 4',
    type: 'Tag 4'
  },
  {
    name: 'Tag 5',
    type: 'Tag 5'
  }
]

export const RegistrationTag = [
  {
    name: 'Welcome to the Shop 1 ',
    type: 'Desc 1'
  },
  {
    name: 'Welcome to the Shop 2',
    type: 'Desc 2'
  },
  {
    name: 'Welcome to the Shop 3',
    type: 'Desc 3'
  },
  {
    name: 'Welcome to the Shop 4 ',
    type: 'Desc 4'
  },
  {
    name: 'Welcome to the Shop 5',
    type: 'Desc 5'
  }
]

export const ServiceTag = [
  {
    name: 'Welcome to the Service 1 ',
    type: 'Service 1'
  },
  {
    name: 'Welcome to the Service 2',
    type: 'Service 2'
  },
  {
    name: 'Welcome to the Service 3',
    type: 'Service 3'
  },
  {
    name: 'Welcome to the Service 4 ',
    type: 'Service 4'
  },
  {
    name: 'Welcome to the Service 5',
    type: 'Service 5'
  }
]

export const getDateChange = (num: any) => {
  var hours = Math.floor(num / 60);
  var minutes: any = num % 60;
  return (hours > 0) ? hours + ' ' + 'hr' + ' ' + parseInt(minutes) + ' ' + 'min' : parseInt(minutes) + ' ' + 'min'
}

export const compressImage = (e: any) => {
  const file = e.target.files[0];

  if (!file) {
    return;
  }

  new Compressor(file, {
    quality: 0.6,

    // The compression process is asynchronous,
    // which means you have to access the `result` in the `success` hook function.
    success(result) {
      console.log('file', result);
    },
    error(err) {
      console.log(err.message);
    },
  });
}

export const setDateTime = (date: Date, hour: any) => {
  var now = new Date(date);
  now.setHours(hour)
  now.setMinutes(0)
  return now
}

export const distanceFind = (lat1: any, lon1: any, lat2: any, lon2: any, unit: any) => {
  var radlat1 = Math.PI * lat1 / 180
  var radlat2 = Math.PI * lat2 / 180
  var radlon1 = Math.PI * lon1 / 180
  var radlon2 = Math.PI * lon2 / 180
  var theta = lon1 - lon2
  var radtheta = Math.PI * theta / 180
  var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
  dist = Math.acos(dist)
  dist = dist * 180 / Math.PI
  dist = dist * 60 * 1.1515
  if (unit == "K") { dist = dist * 1.609344 }
  if (unit == "N") { dist = dist * 0.8684 }
  return Math.round(dist * 1000) / 1000
}

export type Coordinate = {
  lat: number;
  lon: number;
};

export default function getDistanceBetweenTwoPoints(cord1: Coordinate, cord2: Coordinate) {
  if (cord1.lat == cord2.lat && cord1.lon == cord2.lon) {
    return 0;
  }

  const radlat1 = (Math.PI * cord1.lat) / 180;
  const radlat2 = (Math.PI * cord2.lat) / 180;

  const theta = cord1.lon - cord2.lon;
  const radtheta = (Math.PI * theta) / 180;

  let dist =
    Math.sin(radlat1) * Math.sin(radlat2) +
    Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

  if (dist > 1) {
    dist = 1;
  }

  dist = Math.acos(dist);
  dist = (dist * 180) / Math.PI;
  dist = dist * 60 * 1.1515;
  dist = dist * 1.609344; //convert miles to km

  return dist;
}

export const dateMinus = (start: Date, end: Date) => {
  //console.log('date Start', start, 'date End', end)
  var now = new Date(start).getTime();
  // now.setHours(now.getHours() + 5);
  // now.setMinutes(now.getMinutes() + 30);
  var later = new Date(end).getTime();
  //console.log('now Time', now, 'later Time', later, 'deduct', now - later)
  var result: any = (now - later) / 60000;
  //console.log('Res', result, Math.floor(result / 60) + ":" + result % 60)
  return result > 0 ? parseInt(result) * 60 : 0
}


export const ReviewBack = {
  white: "data:image/svg+xml,%3Csvg class='bd-placeholder-img bd-placeholder-img-lg d-block w-100' width='800' height='400' xmlns='http://www.w3.org/2000/svg' role='img' preserveAspectRatio='xMidYMid slice' focusable='false'%3E%3Crect width='100%25' height='100%25' fill='%23f5f5f5'%3E%3C/rect%3E%3Ctext x='50%25' y='50%25' fill='%23aaa' dy='.3em'%3E%3C/text%3E%3C/svg%3E",
  black: "data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17e8a8fd0e1%20text%20%7B%20fill%3A%23ffffff%3Bfont-weight%3Anormal%3Bfont-family%3Avar(--bs-font-sans-serif)%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17e8a8fd0e1%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23282c34%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22251.97500610351562%22%20y%3D%22221.35999908447266%22%3E%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
}


export const dayOfWeek: DayEnumType[] = [
  'sunday',
  'monday',
  'tuesday',
  'wednesday',
  'thursday',
  'friday',
  'saturday',
];

export const rupeeIndian = window.Intl.NumberFormat("en-IN");