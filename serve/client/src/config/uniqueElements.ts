export const uniqueElements = (arr: any) => {
  var main: any = []

  var cc = arr.reduce((current: any, prev: any) => {
    var key = prev['_id']
    if (!current[key]) {
      current[key] = []
    } else {
      current[key].pop()
    }
    current[key].push(prev)
    return current
  }, {})

  Object.values(cc).map((cd: any) => {
    main.push(cd[0])
  })
  return main.sort(function (a: any, b: any) {
    var dateA: any = new Date(a.createdAt), dateB: any = new Date(b.createdAt)
    return dateB - dateA
  });
}

export const uniqueServiceElements = (arr: any) => {
  var main: any = []

  var cc = arr.reduce((current: any, prev: any) => {
    var key = prev['_id']
    if (!current[key]) {
      current[key] = []
    } else {
      current[key].pop()
    }
    current[key].push(prev)
    return current
  }, {})

  Object.values(cc).map((cd: any) => {
    main.push(cd[0])
  })
  return main
}

export const removeElements = (arr: any, id: any) => {

  var cc = arr.filter((item: any) => item._id !== id)

  return cc
}