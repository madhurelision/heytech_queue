import { useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { SOCKET_CONNECT } from 'redux/action/types';
import SocketContext from './socket';

const useRouteEmit = () => {
  const location = useLocation();
  const [initialized, setInitialized] = useState(false);
  const context = useContext(SocketContext)
  const userData = useSelector((state) => state.auth.profile)
  const dispatch = useDispatch()

  useEffect(() => {
    console.log('track', context.socket.connected)

    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      if (localStorage.getItem('auth-token') !== null) {
        context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
      }
    })

    context.socket.on('disconnect', () => {
      context.socket.removeAllListeners()
      dispatch({type:SOCKET_CONNECT, payload:false})
    })

    setInitialized(true);
  }, []);

  useEffect(() => {
    if (initialized) {


      if (context.socket.connected == false) {
        context.socket = context.socketAuth(localStorage.getItem('auth-token'))
      }

      context.socket.emit('route', {
        route: location.pathname + location.search,
        type: localStorage.getItem('type') == null ? 'global' : localStorage.getItem('type'),
        user_id: (Object.keys(userData).length == 0) ? null : localStorage.getItem('type') == 'shop' ? userData?.user_information : userData?._id,
        key: context.socket.id
      })
    }
  }, [initialized, location]);
};

export default useRouteEmit;
