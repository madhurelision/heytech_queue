import Analytics from 'analytics'
import { ANALYTIC_URL } from 'config.keys';
import axios from 'axios';

/* initialize analytics and load plugins */
const analytics = Analytics({
  debug: true,
  plugins: [
    exampleProviderPlugin({
      xyz: '123'
    })
  ]
})

/* This is an example plugin */
function exampleProviderPlugin(userConfig = {}) {
  return {
    NAMESPACE: 'provider-example',
    config: userConfig,
    initialize: (item: any) => {
      console.log('Load stuff')
    },
    page: (item: any) => {
      console.log(`Example Page > [payload: ${JSON.stringify(item.payload, null, 2)}]`)
    },
    /* Track event */
    track: (item: any) => {
      console.log(`Example Track > [${item.payload.event}] [payload: ${JSON.stringify(item.payload, null, 2)}]`)
    },
    /* Identify user */
    identify: (item: any) => {
      console.log(`Example identify > [payload: ${JSON.stringify(item.payload, null, 2)}]`)
    },
    loaded: () => {
      return true
    },
    ready: () => {
      console.log('ready: exampleProviderPlugin')
    }
  }
}

analytics.on('*', ({ payload }) => {
  console.log(`Event ${payload.type}`, payload)
  axios.post(ANALYTIC_URL + '/api/log', { ...payload, type: localStorage.getItem('type') == null ? 'global' : localStorage.getItem('type'), user_type: 'queuickweb' }).then((cc) => {

  }).catch((err) => {
    console.log('server err', err)
  })
});

declare const window: Window &
  typeof globalThis & {
    Analytics: any
  }

window.Analytics = analytics

/* export analytics for usage through the app */
export default analytics