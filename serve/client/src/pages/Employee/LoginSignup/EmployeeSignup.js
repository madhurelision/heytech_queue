import React, { useEffect, useState, useCallback } from 'react'
import { Grid, Typography, Button, TextField, FormControl, InputLabel, Select, MenuItem } from '@mui/material';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';
import FormError from 'pages/Registration/FormError';
import { keyLengthValidation } from 'config/KeyData';
import { employeeNumberLogin, employeeSignupVerifys, getAllShopListMatch, employeeNewSignup } from 'redux/action';
import { BookingButton } from "components/common";
import ProgressTimer from 'pages/Shop/UsableComponent/ProgressTimer';
import { Password } from '@mui/icons-material'

const EmployeeSignup = () => {

  const loading = useSelector(state => state.auth.loader)
  const created = useSelector(state => state.auth.created);
  const updated = useSelector(state => state.auth.updated);
  const link = useSelector(state => state.auth.link);
  const userData = useSelector(state => state.auth.empLoginData);
  const shopData = useSelector(state => state.auth.shop_list);
  const history = useNavigate();
  const [element, setElement] = useState([]);
  const [shop, setShop] = useState([]);
  const dispatch = useDispatch()

  const [data, setData] = useState({
    name: '',
    mobile: '',
    type: '',
    otp: '',
    user_id: '',
    id: ''
  })
  const [count, setCount] = useState(0)
  const [show, setShow] = useState({
    open: false,
    date: new Date().setSeconds(120)
  })

  const [err, setErr] = useState({})

  useEffect(() => {

    if (created) {

      setElement(userData.data)
      setShow({ ...show, open: true, date: new Date().setSeconds(120) })
    }

  }, [created, userData])

  useEffect(() => {

    if (updated) {
      setElement([])
      setData({
        ...data,
        name: '',
        mobile: '',
        type: '',
        otp: '',
        user_id: '',
        id: ''
      })
    }

  }, [updated])

  useEffect(() => {
    if (shopData.length == 0) {
      dispatch(getAllShopListMatch())
    } else {
      setShop(shopData.shop)
    }
  }, [])

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleLoginWithMobile = () => {
    var check = checkMobileSignup(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(employeeNewSignup({
      name: data.name,
      type: data.type,
      mobile: data.mobile,
      user_id: data.user_id
    }))
  }

  const handleSubmitLogin = () => {
    var check = checkMobileOtp({ mobile: data.mobile, otp: data.otp, id: element[0]?.employee_id })
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(employeeSignupVerifys({ mobile: data.mobile, otp: data.otp, id: element[0]?.employee_id }))
  }

  const handleResendOtp = () => {
    setCount(count + 1)
    setShow({ ...show, open: true, date: new Date().setSeconds(120) })
    dispatch(employeeNumberLogin({ mobile: data.mobile, resend: 'multiple' }))
  }
  const handleCloseTimer = useCallback((val) => {
    setShow({ ...show, open: false })
  }, [show])


  return (
    <Grid container spacing={2}>
      <Grid item xs={12} md={12} sm={12}>
        <Typography className='title_head' variant="h5" component="h2" align="center">
          Employee Signup
        </Typography>
      </Grid>
      {element.length > 0 && <><Button sx={{ textTransform: 'capitalize' }} disabled={(loading || count == 2 || show.open)} startIcon={<Password />} onClick={() => { handleResendOtp() }} >
        Resend Otp
      </Button>
        {show.open == true && <ProgressTimer time={show.date} ma={'hello'} func={handleCloseTimer} show={false} />}</>}
      <Grid item xs={12} sm={12} md={12}>
        <TextField name="name" type="text" value={data.name} disabled={element.length > 0 || loading} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Name" />
        {err.name && (<FormError data={err.name}></FormError>)}
      </Grid>
      <Grid item xs={12} sm={12} md={12}>
        <TextField name="type" type="text" value={data.type} disabled={element.length > 0 || loading} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Type" />
        {err.type && (<FormError data={err.type}></FormError>)}
      </Grid>
      <Grid item xs={12} sm={12} md={12}>
        <TextField name="mobile" type="tel" inputProps={{
          maxLength: keyLengthValidation.mobile
        }} value={data.mobile} disabled={element.length > 0 || loading} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Mobile Number" />
        {err.mobile && (<FormError data={err.mobile}></FormError>)}
      </Grid>
      <Grid item xs={12} sm={12} md={12}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Shop</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={data.user_id}
            label="Shop"
            name="user_id"
            onChange={handleChange}
            disabled={element.length > 0 || loading}
          >
            {shop.map((cc) => {
              return (
                <MenuItem style={{ textTransform: 'capitalize' }} key={cc._id} value={cc.user_information}>{cc.shop_name}</MenuItem>
              )
            })}
          </Select>
        </FormControl>
        {err.user_id && (<FormError data={err.user_id}></FormError>)}
      </Grid>
      {element.length > 0 && <Grid item xs={12} sm={12} md={12} textAlign="center">{'Verifying on '}<span className="booking_number_high" >{'+91' + data.mobile}</span> </Grid>}
      {element.length > 0 &&
        <Grid item xs={12} sm={12} md={12}>
          <TextField name="otp" type="tel" inputProps={{
            maxLength: keyLengthValidation.otp
          }} value={data.otp} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} label="Otp"
            onChange={handleChange}
          />
          {err.otp && (<FormError data={err.otp}></FormError>)}
        </Grid>}
      <Grid container spacing={2} sx={{ marginTop: '5px' }}>
        <Grid item xs={12} align="right">
          {element.length == 0 && <BookingButton type="button" disabled={loading} variant="contained" onClick={() => { handleLoginWithMobile() }}>Submit</BookingButton>}
          {element.length > 0 && <BookingButton type="button" disabled={loading} variant="contained" onClick={() => { handleSubmitLogin() }}>Verify</BookingButton>}
        </Grid>
      </Grid>
    </Grid>
  )
}

export default EmployeeSignup


const checkMobileSignup = (data) => {
  let errors = {}

  if (!data.mobile) {
    errors['mobile'] = "Mobile Number is Required";
  }

  if (data.mobile) {
    if (!data["mobile"].match(/^[0-9]{10}$/)) {
      errors["mobile"] = "Please enter valid mobile no.";
    }
  }
  if (!data.name) {
    errors['name'] = "Name is Required";
  }

  if (!data.type) {
    errors['type'] = "Type is Required";
  }

  if (!data.user_id) {
    errors['user_id'] = "Shop is Required";
  }

  return errors
}


const checkMobileOtp = (data) => {
  let errors = {}

  if (!data.mobile) {
    errors['mobile'] = "Mobile Number is Required";
  }

  if (data.mobile) {
    if (!data["mobile"].match(/^[0-9]{10}$/)) {
      errors["mobile"] = "Please enter valid mobile no.";
    }
  }
  if (!data.otp) {
    errors['otp'] = "Otp is Required";
  }

  if (!data.id) {
    errors['id'] = "Select Employee is Required";
  }

  return errors
}