import React, { useEffect, useState, useCallback } from 'react'
import { Grid, Typography, Button, Card, CardContent, Box, TextField, FormLabel, Checkbox, Dialog, IconButton } from '@mui/material';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';
import FormError from 'pages/Registration/FormError';
import { keyLengthValidation } from 'config/KeyData';
import { employeeNumberLogin, employeeNumberLoginVerify } from 'redux/action';
import { BookingButton } from "components/common";
import ProgressTimer from 'pages/Shop/UsableComponent/ProgressTimer';
import { Password, Close } from '@mui/icons-material';

const EmployeeMobileLogin = () => {

  const loading = useSelector(state => state.auth.loader)
  const created = useSelector(state => state.auth.created);
  const updated = useSelector(state => state.auth.updated);
  const link = useSelector(state => state.auth.link);
  const shopData = useSelector(state => state.auth.shopLoginData);
  const history = useNavigate();
  const [element, setElement] = useState([]);
  const dispatch = useDispatch()

  const [data, setData] = useState({
    id: '',
    mobile: '',
    otp: ''
  })
  const [count, setCount] = useState(0)
  const [show, setShow] = useState({
    open: false,
    date: new Date().setSeconds(120)
  })

  const [open, setOpen] = useState(false)
  const [value, setValue] = useState({
    name: '',
    email: '',
    mobile: ''
  })

  const [err, setErr] = useState({})

  useEffect(() => {

    if (created) {

      setElement(shopData.data)
      setShow({ ...show, open: true, date: new Date().setSeconds(120) })

      if (data.id == '') {
        handleOpen(true)
      }
    }

  }, [created, shopData])

  useEffect(() => {

    if (updated) {
      history(link)
    }

  }, [updated, link])


  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleLoginWithMobile = () => {
    var check = checkMobileLogin({ mobile: data.mobile })
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(employeeNumberLogin({ mobile: data.mobile, resend: 'single' }))
  }

  const handleSubmitLogin = () => {
    var check = checkMobileOtp(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(employeeNumberLoginVerify(data))
  }

  const handleSelect = (e, cc) => {
    if (e.target.checked == true) {
      setData({ ...data, id: cc.employee_id })
      setErr({ ...err, id: '' })
      setValue({ ...value, name: cc?.name, email: cc?.employee_id, mobile: cc?.mobile })
    } else {
      setData({ ...data, id: '' })
      setErr({ ...err, id: '' })
      setValue({ ...value, name: '', email: '', mobile: '' })
    }
  }



  const handleResendOtp = () => {
    setCount(count + 1)
    setShow({ ...show, open: true, date: new Date().setSeconds(120) })
    dispatch(employeeNumberLogin({ mobile: data.mobile, resend: 'multiple' }))
  }
  const handleCloseTimer = useCallback((val) => {
    setShow({ ...show, open: false })
  }, [show])

  const handleCloseUser = () => {
    setOpen(false)
  }

  const handleOpen = () => {
    setOpen(true)
  }

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} md={12} sm={12}>
        <Typography className='title_head' variant="h5" component="h2" align="center">
          Employee Login
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={12}>
        <TextField name="mobile" type="tel" inputProps={{
          maxLength: keyLengthValidation.mobile
        }} value={data.mobile} disabled={element.length > 0 || loading} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Mobile Number" />
        {err.mobile && (<FormError data={err.mobile}></FormError>)}
      </Grid>
      {data.length > 0 && <Grid item xs={12} sm={12} md={12} textAlign="center">{'Verifying on '}<span className="booking_number_high" >{'+91' + element.mobile}</span> </Grid>}
      {element.length > 0 &&
        <Grid item xs={12} sm={12} md={12}>
          <TextField name="otp" type="tel" inputProps={{
            maxLength: keyLengthValidation.otp
          }} value={data.otp} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} label="Otp"
            onChange={handleChange}
          />
          {err.otp && (<FormError data={err.otp}></FormError>)}
        </Grid>}

      {element.length > 0 && <div className="shop-login-btn"><Button sx={{ textTransform: 'capitalize' }} disabled={(loading || count == 2 || show.open)} startIcon={<Password />} onClick={() => { handleResendOtp() }} >
        Resend Otp
      </Button>
        {show.open == true && <ProgressTimer time={show.date} ma={'hello'} func={handleCloseTimer} show={false} />}</div>}
      {data.id !== null && data.id !== undefined && data.id !== '' && <Grid container spacing={2} sx={{ marginTop: '10px', paddingLeft: '15px' }}>
        <Grid item xs={12} md={6} sm={6} onClick={handleOpen}>
          <Card >
            <Box sx={{ background: 'white', color: 'black' }}>
              <CardContent>
                <Grid container>
                  <Grid item xs={12} sm={12} >
                    <div style={{ fontSize: '0.75rem' }}> <b>Name:</b>  {value?.name}</div>
                  </Grid>
                  <Grid item xs={12} sm={12} >
                    <div style={{ fontSize: '0.75rem' }}> <b>Employee_id:</b> {value?.email}</div>
                  </Grid>
                  <Grid item xs={12} sm={12} >
                    <div style={{ fontSize: '0.75rem' }}> <b>Mobile:</b> {value?.mobile}</div>
                  </Grid>
                </Grid>
              </CardContent>
            </Box>
          </Card>
        </Grid>
      </Grid>}

      {/* {element.length > 0 && <Grid item xs={12} sm={12} md={12}>
        <Button variant="contained" onClick={handleOpen} >Select User</Button>
      </Grid>} */}
      {err.id && (<FormError data={err.id}></FormError>)}
      <Grid container spacing={2} sx={{ marginTop: '5px' }} >
        <Grid item xs={12} align="right">
          {/* <Button type="button" disabled={(loading) ? true : false} variant="contained" onClick={() => { handleLoginWithMobile() }} color="primary">Login</Button> */}
          {element.length == 0 && <BookingButton type="button" disabled={loading} variant="contained" onClick={() => { handleLoginWithMobile() }}>Login</BookingButton>}
          {element.length > 0 && <BookingButton type="button" disabled={loading} variant="contained" onClick={() => { handleSubmitLogin() }}>Login</BookingButton>}
        </Grid>
      </Grid>
      <Dialog
        open={open}
        onClose={() => {
          if (data.id !== '') {
            handleCloseUser()
          }
        }}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <Grid>
          <Card>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Typography className='title_head' flexGrow={1} variant="h5" component="div" align="center">
                      Select User
                    </Typography>
                    <Box>
                      <IconButton onClick={() => {
                        if (data.id !== '') {
                          handleCloseUser()
                        }
                      }}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                {element.map((cc) => {
                  return (
                    <Grid key={cc.employee_id} item xs={12} sm={4} md={4}>
                      <Card onClick={() => {
                        if (data.id == cc.employee_id) {
                          handleSelect({ target: { checked: false } }, cc)
                        } else {
                          handleSelect({ target: { checked: true } }, cc)
                        }
                      }}>
                        <Box sx={data.id == cc.employee_id ? { background: 'red', color: 'black' } : { background: 'white', color: 'black' }}>
                          <CardContent>
                            <Grid container>
                              <Grid item xs={12} sm={12} >
                                <div style={{ fontSize: '0.75rem' }}> <b>Name:</b>  {cc?.name}</div>
                              </Grid>
                              <Grid item xs={12} sm={12} >
                                <div style={{ fontSize: '0.75rem' }}> <b>Employee_id:</b> {cc?.employee_id}</div>
                              </Grid>
                              <Grid item xs={12} sm={12} >
                                <div style={{ fontSize: '0.75rem' }}> <b>Mobile:</b> {cc?.mobile}</div>
                              </Grid>
                              <Grid item xs={12} sm={12} md={12}>
                                <Checkbox
                                  color="primary"
                                  sx={{ color: 'blue' }}
                                  inputProps={{ 'aria-label': 'secondary checkbox' }}
                                  onClick={(e) => { handleSelect(e, cc) }}
                                  id={cc._id}
                                  name={cc._id}
                                  value={cc._id}
                                  checked={data.id == cc.employee_id ? true : false}
                                />
                              </Grid>
                            </Grid>
                          </CardContent>
                        </Box>
                      </Card>
                    </Grid>
                  )
                })}
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Dialog>
    </Grid>
  )
}

export default EmployeeMobileLogin

const checkMobileLogin = (data) => {
  let error = {}

  if (!data.mobile) {
    error['mobile'] = "Mobile Number is Required";
  }

  if (data.mobile) {
    if (!data["mobile"].match(/^[0-9]{10}$/)) {
      error["mobile"] = "Please enter valid mobile no.";
    }
  }
  return error
}


const checkMobileOtp = (data) => {
  let errors = {}

  if (!data.mobile) {
    errors['mobile'] = "Mobile Number is Required";
  }

  if (data.mobile) {
    if (!data["mobile"].match(/^[0-9]{10}$/)) {
      errors["mobile"] = "Please enter valid mobile no.";
    }
  }
  if (!data.otp) {
    errors['otp'] = "Otp is Required";
  }

  if (!data.id) {
    errors['id'] = "Select Employee is Required";
  }

  return errors
}