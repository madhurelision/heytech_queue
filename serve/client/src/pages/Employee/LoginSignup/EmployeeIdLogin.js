import React, { useEffect, useState, useCallback } from 'react'
import { Grid, Typography, TextField, } from '@mui/material';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';
import FormError from 'pages/Registration/FormError';
import { loginEmployeeId } from 'redux/action';
import { BookingButton } from "components/common";

const EmployeeIdLogin = () => {

  const loading = useSelector(state => state.auth.loader)
  const updated = useSelector(state => state.auth.updated);
  const link = useSelector(state => state.auth.link);
  const history = useNavigate();
  const dispatch = useDispatch()

  const [data, setData] = useState({
    id: '',
    mobile: '',
    otp: ''
  })

  const [err, setErr] = useState({})

  useEffect(() => {

    if (updated) {
      history(link)
    }

  }, [updated, link])


  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleLoginWithId = () => {
    var check = checkLogin({ id: data.id })
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(loginEmployeeId({ id: data.id }))
  }

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} md={12} sm={12} textAlign="center">
        <Typography className='title_head' variant="h5" component="h2" align="center">
          Employee Login
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={12}>
        <TextField
          fullWidth
          InputLabelProps={{
            shrink: true
          }}
          type="text"
          value={data.id}
          label="Employee Id"
          name="id"
          onChange={handleChange}
        />
        {err.id && (<FormError data={err.id}></FormError>)}
      </Grid>
      <Grid container spacing={2} sx={{ marginTop: '5px' }} >
        <Grid item xs={12} align="right">
          <BookingButton type="button" disabled={(loading) ? true : false} variant="contained" onClick={() => { handleLoginWithId() }}>Login</BookingButton>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default EmployeeIdLogin


const checkLogin = (data) => {
  let error = {}

  if (!data.id) {
    error['id'] = "Employe Id Required"
  }
  return error
}