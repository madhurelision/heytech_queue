import React from 'react'
import { Card, CardContent, Grid, Avatar, CardHeader } from '@mui/material'
import '../../Shop/Queue/queue.css'

const EmListView = (props) => {

  return (
    <Card className='employee-card' >
      <CardHeader
        avatar={
          <Avatar src={props.image}>
            {props?.name.slice(0, 1)}
          </Avatar>
        }
        sx={{ display: { xs: 'none', sm: 'block', md: 'block' } }}
      />
      <CardContent className='employee-cardC' >
        <Grid container spacing={1}>
          <Grid item xs={12} sm={6} md={6} className="div-flex-se">
            <Avatar src={props.image} sx={{ width: '100px', height: '100px', fontSize: '5.25rem' }}>
              {props?.name.slice(0, 1)}
            </Avatar>
            <div>
              <div className='employee-name'>
                {props?.name}
              </div>
              <div className='employee-list'>
                EmployeeId: {props?.employee_id}
              </div>
              <div className='employee-list'>
                Type: {props?.type}
              </div>
              <div className='employee-list'>
                Number:{props?.mobile}
              </div>
            </div>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  )

}

export default EmListView