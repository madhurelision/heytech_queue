import React, { useCallback, useContext, useEffect, useState } from 'react'
import { Grid, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Typography, Card, CardContent, IconButton, Dialog, DialogTitle, DialogContent, Box, Collapse, Button } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import TableLoader from 'pages/Shop/UsableComponent/TableLoader'
import NoData from 'pages/Shop/UsableComponent/NoData'
import StatusBox from 'pages/Shop/UsableComponent/StatusBox'
import DialogBox from 'pages/Shop/Dashboard/DialogBox'
import { employeeStyles, tableStyles } from 'pages/Shop/Dashboard/styles'
import { getEmployeeAssignBooking } from 'redux/action'
import moment from 'moment'
import EmailBox from 'pages/Shop/UsableComponent/EmailBox'
import ProgressTimer from 'pages/Shop/UsableComponent/ProgressTimer'
import { MoreVert, Refresh, KeyboardArrowDown, KeyboardArrowUp, Star, Close } from '@mui/icons-material'
import SocketContext, { socket, socketAuth } from 'config/socket'
import { EMPLOYEE_COUNT, SOCKET_EMPLOYEE_BOOKING, SOCKET_EMPLOYEE_BOOKING_ASSIGN, SOCKET_EMPLOYEE_BOOKING_ASSIGN_REMOVE, SOCKET_EMPLOYEE_BOOKING_REMOVE } from 'redux/action/types'
import TooltipBox from 'pages/Shop/UsableComponent/TooltipBox'
import ListCard from 'pages/Shop/UsableComponent/ListCard'
import { getDateChange } from 'config/KeyData'
import ShopAppointmentView from 'pages/Shop/Dashboard/ShopAppointmentView'
import MobileLoader from 'pages/Shop/UsableComponent/MobileLoader'
import '../../Shop/Queue/queue.css'

var DailogData
const EmployeeDashboard = () => {

  const [element, setElement] = useState([])
  const [box, setBox] = useState([])
  const [flag, setFlag] = useState(false)
  const [open, setOpen] = useState(false)
  const [view, setView] = useState('')
  const [boxview, setBoxView] = useState('')
  const bookingData = useSelector((state) => state.auth.employeeCurrentBooking)
  const profileData = useSelector((state) => state.auth.profile)
  const cted = useSelector((state) => state.auth.open)
  const loading = useSelector((state) => state.auth.loader)
  const dispatch = useDispatch()
  const classes = employeeStyles()
  const [data, setData] = useState({
    open: false,
    _id: ''
  })

  const [book, setBook] = useState({
    open: false,
    data: {}
  })

  const context = useContext(SocketContext)


  useEffect(() => {

    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
    })

    context.socket.on('userDisconnect', (data) => {
      console.log('Socket Err', data)
    })

    context.socket.on('employeeCount', (data) => {
      console.log('employee Count Data', data)
      dispatch({ type: EMPLOYEE_COUNT, payload: data })
    })

    context.socket.on('employeeBooking', (data) => {
      console.log('employee Booking', data)
      dispatch({ type: SOCKET_EMPLOYEE_BOOKING, payload: [data] })
    })

    context.socket.on('employeeBookingRemove', (data) => {
      console.log('employee Booking Remove', data)
      dispatch({ type: SOCKET_EMPLOYEE_BOOKING_REMOVE, payload: data })
    })

    context.socket.on('employeeAssignBooking', (data) => {
      console.log('employee Booking', data)
      dispatch({ type: SOCKET_EMPLOYEE_BOOKING_ASSIGN, payload: [data] })
    })

    context.socket.on('employeeAssignBookingRemove', (data) => {
      console.log('employee Booking Remove', data)
      dispatch({ type: SOCKET_EMPLOYEE_BOOKING_ASSIGN_REMOVE, payload: data })
    })

    context.socket.on('disconnect', () => {
      console.log('socket is disconnect')
    })

    return () => {
      context.socket.removeListener(['employeeCount', 'employeeBooking', 'employeeBookingRemove', 'employeeAssignBooking', 'employeeAssignBookingRemove'])
    }

  }, [])

  useEffect(() => {

    if (bookingData.length == 0) {
      dispatch(getEmployeeAssignBooking())
    } else {
      setElement(bookingData.booking)
      setFlag(true)
    }

  }, [bookingData])


  const handleDialogClose = () => {
    setOpen(false)
  }

  const handleDialogOpen = (item) => {
    DailogData = <ShopAppointmentView item={item} />
    setOpen(true)
  }

  const handleRefresh = () => {
    dispatch(getEmployeeAssignBooking())
  }

  const handleTimerClose = useCallback((id) => {
    console.log('Timer Close', id)
    var arr = [...box]
    arr.push(id)
    setBox(arr)
  }, [box.length]);

  const handleViewCard = (row) => {
    setBook({
      ...book,
      open: true,
      data: row,
    })
  }

  const handleBookingClose = () => {
    setBook({ ...book, open: false })
  }

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card>
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={2} sm={2} md={2} >
                {/* <IconButton title="Refresh" disabled={loading} onClick={handleRefresh} >
                  <Refresh />
                </IconButton> */}
                <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh} >
                  Refresh
                </Button>
              </Grid>
              <Grid item xs={8} sm={8} md={8} >
                <Typography className='title_head' variant="h5" component="h2" align="center">
                  Booking List
                </Typography>
              </Grid>
              <Grid item xs={2} sm={2} md={2} ></Grid>
            </Grid>
            {/* <Grid container spacing={1}>
              <Grid item xs={2} sm={2} md={4} ></Grid>
              <Grid item xs={12} sm={12} md={4}>
                {Object.keys(profileData).length > 0 && <EmListView {...profileData} />}
              </Grid>

              <Grid item xs={2} sm={2} md={4} ></Grid>
            </Grid> */}
            <TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "none" } }}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                    <TableCell>Booking Type</TableCell>
                    <TableCell>Timer</TableCell>
                    <TableCell>Appointment Date</TableCell>
                    <TableCell>Appointment Time</TableCell>
                    <TableCell>Waiting Time</TableCell>
                    <TableCell>Service Name</TableCell>
                    <TableCell>User Size</TableCell>
                    <TableCell>Status</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <React.Fragment
                      key={row._id}>
                      <TableRow
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >

                        <TableCell>
                          {
                            boxview == row._id ? <IconButton
                              title='Open'
                              aria-label="open"
                              size="small"
                              onClick={() => setBoxView('')}
                            >
                              <KeyboardArrowUp />
                            </IconButton> : <IconButton
                              title='Close'
                              aria-label="close"
                              size="small"
                              onClick={() => setBoxView(row._id)}
                            >
                              <KeyboardArrowDown />
                            </IconButton>
                          }
                        </TableCell>
                        <TableCell>
                          {/* <IconButton title="Add Waiting Time" onClick={() => {
                            if (row.queue == true) {
                              history('/shop')
                            } else {
                              handleEdit(row)
                            }
                          }}>
                            <Edit />
                          </IconButton> */}
                          <IconButton title="Details" onClick={() => { handleDialogOpen(row) }} >
                            <MoreVert />
                          </IconButton>
                          {/* {box.includes(row._id) == false && <IconButton title="Cancel Booking" onClick={() => { handleCancelOpen(row._id) }} >
                            <Cancel />
                          </IconButton>} */}
                        </TableCell>
                        <TableCell >
                          {row.queue == true ? 'Queue' : 'Appointment'}
                        </TableCell>
                        <TableCell >
                          {row.waiting_verify == true ? <ProgressTimer time={row.end_time} ma={row._id} func={handleTimerClose} show={true} /> : '.......'}
                        </TableCell>
                        <TableCell >{moment(row.appointment_date).format('LL')}</TableCell>
                        <TableCell >{moment(row.appointment_date).calendar()}</TableCell>
                        <TableCell >{(!row.waiting_number || row.waiting_number == null || row.waiting_number == undefined) ? row?.waiting_time : (row?.waiting_number < 60) ? row?.waiting_number + ' ' + row.waiting_key : getDateChange(row.waiting_number)}</TableCell>
                        <TableCell >{row?.service_id?.name}</TableCell>
                        <TableCell  >{row?.size}</TableCell>
                        <TableCell >
                          <StatusBox value={row.status} />
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className='customer-tablecell' colSpan={12}>
                          <Collapse in={boxview == row._id} timeout="auto" unmountOnExit>
                            <Box sx={{ margin: '3px' }}>
                              <Typography variant="h4" gutterBottom component="div" sx={{ fontSize: '1.1rem' }}>
                                User Details
                              </Typography>
                              <Table size="small" aria-label="shopDetail">
                                <TableHead>
                                  <TableRow>
                                    <TableCell></TableCell>
                                    <TableCell>Description</TableCell>
                                    <TableCell>User Rating</TableCell>
                                    <TableCell>User Name</TableCell>
                                    <TableCell>User Email</TableCell>
                                  </TableRow>
                                </TableHead>
                                <TableBody>
                                  <TableRow>
                                    <TableCell></TableCell>
                                    <TableCell >
                                      <TooltipBox title="hover" data={row.description} />
                                    </TableCell>
                                    <TableCell>
                                      <Star color="warning" />{row.totalUser}
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                      {row?.user_id?.first_name + ' ' + row?.user_id?.last_name}
                                    </TableCell>
                                    <TableCell >
                                      <EmailBox title={row.user_email} />
                                    </TableCell>
                                  </TableRow>
                                </TableBody>
                              </Table>
                            </Box>
                          </Collapse>
                        </TableCell>
                      </TableRow>
                    </React.Fragment>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <TableLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>

            {/* Mobile View */}
            <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "block" } }} >
              <Table size="small" aria-label="simple table">
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <React.Fragment
                      key={row._id}>
                      <TableRow
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell className='customer-table' >
                          <ListCard {...row} handleClick={handleViewCard} />
                          {/* {
                            view == row._id ? <IconButton
                              aria-label="expand row"
                              size="small"
                              sx={{ textAlign: 'center' }}
                              onClick={() => setView('')}
                            >
                              <KeyboardArrowUp />
                            </IconButton> : <IconButton
                              aria-label="expand row"
                              size="small"
                              sx={{ textAlign: 'center' }}
                              onClick={() => setView(row._id)}
                            >
                              <KeyboardArrowDown />
                            </IconButton>
                          } */}
                        </TableCell>
                      </TableRow>
                      {/* <TableRow>
                        <TableCell className='customer-tablecell' colSpan={6}>
                          <Collapse in={view == row._id} timeout="auto" unmountOnExit>
                            <Box sx={{ margin: 1 }}>
                              <Grid container spacing={2}>

                                <Grid item xs={6} sm={6}>
                                  <div>{row.description}</div>
                                </Grid>
                                <Grid item xs={6} sm={6} >
                                  <div>Waiting Time</div>
                                  <div className='customer-font'>
                                    {(!row.waiting_number || row.waiting_number == null || row.waiting_number == undefined) ? row?.waiting_time : (row?.waiting_number < 60) ? row?.waiting_number + ' ' + row.waiting_key : getDateChange(row.waiting_number)}
                                  </div>
                                </Grid>
                                <Grid item xs={12} sm={12}>

                                  <span className='customer-fonts'>
                                    {row.waiting_verify == true ? <ProgressTimer time={row.end_time} ma={row._id} func={handleTimerClose} show={true} /> : '.......'}
                                  </span>
                                </Grid>
                                <Grid item xs={12} sm={12}>
                                  <div>{row?.user_id?.first_name} <Star color="warning" />{row.totalUser}</div>
                                  <div>{row.user_email}</div>
                                </Grid>
                                <Grid item xs={12} sm={12}>
                                  <IconButton title="Details" onClick={() => { handleDialogOpen(row) }} >
                                    <MoreVert color="info" />
                                  </IconButton>
                                </Grid>
                              </Grid>
                            </Box>
                          </Collapse>
                        </TableCell>
                      </TableRow> */}
                    </React.Fragment>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <MobileLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
          </CardContent>
        </Card>
      </Grid>

      <DialogBox open={open} handleClose={handleDialogClose} title={'Appointment Detail'} data={DailogData} />
      <Dialog
        open={book.open}
        onClose={handleBookingClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <Grid>
          <Card>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Box flexGrow={1} >
                      <Typography className='title_head loginHead' variant="h4" component="h2" align="center">
                        Booking Details
                      </Typography>
                    </Box>
                    <Box>
                      <IconButton onClick={handleBookingClose}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>

                <Grid item xs={6} sm={6}>
                  <div>{book.data.description}</div>
                </Grid>
                <Grid item xs={6} sm={6} >
                  <div>Waiting Time</div>
                  <div className='customer-font'>
                    {(!book.data.waiting_number || book.data.waiting_number == null || book.data.waiting_number == undefined) ? book.data?.waiting_time : (book.data?.waiting_number < 60) ? book.data?.waiting_number + ' ' + book.data.waiting_key : getDateChange(book.data.waiting_number)}
                  </div>
                </Grid>
                <Grid item xs={12} sm={12}>

                  <span className='customer-fonts'>
                    {book.data.waiting_verify == true ? <ProgressTimer time={book.data.end_time} ma={book.data._id} func={handleTimerClose} show={true} /> : '.......'}
                  </span>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <div>{book.data?.user_id?.first_name} <Star color="warning" />{book.data.totalUser}</div>
                  <div>{book.data.user_email}</div>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <IconButton title="Details" onClick={() => { handleDialogOpen(book.data) }} >
                    <MoreVert color="info" />
                  </IconButton>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Dialog>
    </Grid>
  )
}

export default EmployeeDashboard
