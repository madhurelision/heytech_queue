import React from 'react'
import { Grid } from '@mui/material'
import MaterialAppBar from '@mui/material/AppBar';
import { styled } from '@mui/material/styles';
import { Outlet } from 'react-router-dom'
import ScrollTop from 'components/ScrollToTop';
import EmployeeHeader from './EmployeHeader';

const PageOneWrapper = styled('div')({
  backgroundColor: 'transparent',
  height: '100%',
  width: '100%',
  overflow: 'hidden',
  position: 'relative',
});


const EmployeeHome = ({ children }) => {
  return (
    <PageOneWrapper>
      <Grid container direction="column" wrap="nowrap" sx={{ height: '100%' }}>
        {/* <Grid item >

          <MaterialAppBar
            variant="outlined"
            position="relative"
            elevation={0}
            sx={{ padding: '8px', backgroundColor: '#242424' }}>
            <EmployeeHeader />
          </MaterialAppBar>
          <div id="back-to-top-anchor" />
          <ScrollTop />
        </Grid> */}
        {children}
        <Outlet />
      </Grid>
    </PageOneWrapper>
  )
}

export default EmployeeHome
