import * as React from 'react';
import { styled } from '@mui/material/styles';
import { TabContext, TabList, TabPanel } from '@mui/lab';
import EmployeeBooking from '../Booking/EmployeeBooking';
import Dashboard from '../Dashboard/Dashboard';
import { Mail } from '@mui/icons-material';
import { useDispatch, useSelector } from 'react-redux';
import { Badge, Grid, Tab, Box } from '@mui/material';
import EmListView from '../EmployeListView/EmListView';
import { employeeStyles } from 'pages/Shop/Dashboard/styles';

// const StyledTabs = styled((props) => (
//   <Tabs
//     {...props}
//     TabIndicatorProps={{ children: <span className="MuiTabs-indicatorSpan" /> }}
//   />
// ))({
//   '& .MuiTabs-indicator': {
//     display: 'flex',
//     justifyContent: 'center',
//     backgroundColor: 'transparent',
//   },
//   '& .MuiTabs-indicatorSpan': {
//     maxWidth: 40,
//     width: '100%',
//     backgroundColor: '#635ee7',
//   },
// });

const StyledTab = styled((props) => <Tab disableRipple {...props} />)(
  ({ theme }) => ({
    textTransform: 'none',
    fontWeight: theme.typography.fontWeightRegular,
    fontSize: theme.typography.pxToRem(15),
    minHeight: '50px',
    marginRight: theme.spacing(1),
    color: theme.palette.mode == 'dark' ? '#f5f5f5' : 'black',
    '&.Mui-selected': {
      color: '#d6a354 !important',
    },
    '&.Mui-focusVisible': {
      backgroundColor: 'rgba(100, 95, 228, 0.32)',
    },
    '&.MuiTabs-indicator': {
      display: 'none',
      left: '271px !important'
    }
  }),
);

export default function CustomTabs() {

  const classes = employeeStyles()
  const [value, setValue] = React.useState("0");
  const dispatch = useDispatch()
  const countData = useSelector((state) => state.auth.employeeTotalCount)
  const nData = useSelector((state) => state.auth.employeeBookingTotalCount)
  const profileData = useSelector((state) => state.auth.profile)

  const handleChange = (event, newValue) => {
    console.log('value', newValue)
    setValue(newValue);
  };

  return (
    <Box className={classes.grid}>
      <Box>
        <Grid container spacing={1} className={classes.cardgrid}>
          <Grid item xs={2} sm={2} md={4} ></Grid>
          <Grid item xs={12} sm={12} md={4}>
            {Object.keys(profileData).length > 0 && <EmListView {...profileData} />}
          </Grid>

          <Grid item xs={2} sm={2} md={4} ></Grid>
        </Grid>
        <TabContext value={value}>
          <TabList
            onChange={handleChange}
            aria-label="styled tabs example"
            centered
          >
            <StyledTab className='title_head' label="Dashboard" value="0" icon={<Badge color="secondary" badgeContent={countData}>
              <Mail />
            </Badge>} iconPosition="end" />
            <StyledTab className='title_head' label="Booking" value="1" icon={<Badge color="secondary" badgeContent={nData}>
              <Mail />
            </Badge>} iconPosition="end" />
          </TabList>
          {/* <Box sx={{ p: 3 }} /> */}
          <TabPanel sx={{ padding: '4px !important' }} value={"0"}>
            <Dashboard />
          </TabPanel>
          <TabPanel sx={{ padding: '4px !important' }} value={"1"} >
            <EmployeeBooking />
          </TabPanel>
        </TabContext>
      </Box>
    </Box>
  );
}
