import React, { useState, useEffect } from 'react'
import { Grid, TextField, Button, useTheme } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import 'pages/Registration/register.css'
import { keyLengthValidation } from 'config/KeyData';
import { makeStyles } from '@mui/styles'
import { contactCreate } from 'redux/action'


const styles = makeStyles((theme) => ({
  btn_form: {
    padding: '5px 30px',
    border: 'none',
    borderRadius: 'unset',
    background: theme.palette.mode == 'dark' ? 'white' : '#242424'
  }
}))

const ContactForm = () => {

  const theme = useTheme()
  const classes = styles(theme)

  const [data, setData] = useState({
    name: '',
    description: '',
    mobile: '',
  })

  const mainVal = useSelector(state => state.auth.created)
  const loading = useSelector(state => state.auth.loader)
  const [err, setErr] = useState({})
  const dispatch = useDispatch()

  useEffect(() => {

    if (mainVal) {
      setData({
        ...data,
        name: '',
        description: '',
        mobile: '',
      })
    }

  }, [mainVal])


  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    var check = contactValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(contactCreate(data))
  }

  return (
    <>
      <Grid container spacing={5}>
        <Grid item xs={12} sm={12} md={12}>
          <TextField
            name="name"
            type="text"
            value={data.name}
            variant="standard"
            onChange={handleChange}
            placeholder="Name"
            className='form_width'
          />
          {err.name && (<FormError data={err.name}></FormError>)}
        </Grid>
        <Grid item xs={12} sm={12} md={12}>
          <TextField
            variant="standard"
            onChange={handleChange}
            value={data.mobile}
            name="mobile"
            type="tel"
            placeholder="Contact Number"
            inputProps={{
              'aria-label': 'Contact Number',
              maxLength: keyLengthValidation.mobile
            }}
            className='form_width'
          />
          {err.mobile && (<FormError data={err.mobile}></FormError>)}
        </Grid>
        <Grid item xs={12} sm={12} md={12}>
          <TextField
            name="description"
            type="text"
            value={data.description}
            variant="standard"
            placeholder="tell us all about it"
            onChange={handleChange}
            className='form_width'
          />
          {err.description && (<FormError data={err.description}></FormError>)}
        </Grid>
        <Grid item xs={6} sm={6} md={6}>
          <Button id="sendBtn" type="button" className={classes.btn_form} disabled={loading ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Send</Button>
        </Grid>
      </Grid>
    </>
  )
}

export default ContactForm

const contactValidate = (data) => {
  let errors = {}

  if (!data.name) {
    errors["name"] = "Name is Required"
  }

  if (!data.mobile) {
    errors["mobile"] = "Contact Number is Required"
  }

  if (data.mobile) {
    if (!data["mobile"].match(/^[0-9]{10}$/)) {
      errors["mobile"] = "Please enter valid Contact no.";
    }
  }

  if (!data.description) {
    errors["description"] = "Description is Required"
  }
  return errors
}