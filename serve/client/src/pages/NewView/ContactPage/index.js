import React from 'react';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import NewAppbar from '../NewAppBar';
import ContactPage from './ContactPage'
import Appbar from 'components/Appbar';

const PageOneWrapper = styled('div')({
  backgroundColor: 'transparent',
  height: '100%',
  width: '100%',
  overflow: 'hidden',
  position: 'relative',
});

const Contact = () => {

  return (
    <PageOneWrapper className='contactWrapper'>
      <Grid container direction="column" wrap="nowrap" sx={{ height: '100%', overflowX: 'auto' }}>
        {/* <Grid item sx={{ zIndex: 1 }}>
          <Appbar />
        </Grid> */}
        <ContactPage />
      </Grid>
    </PageOneWrapper>
  )
};

export default Contact;
