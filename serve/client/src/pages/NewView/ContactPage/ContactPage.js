import React, { useState, useEffect } from 'react';
import { Grid, Typography, useMediaQuery } from '@mui/material';
import { makeStyles, useTheme } from '@mui/styles';
import ContactForm from './ContactForm'
import ToolbarTabs from 'components/Appbar/ToolbarTabs';
import { getAllContactPage } from 'redux/action';
import { useDispatch, useSelector } from 'react-redux';
import MobileToolbar from 'components/MobileToolbar/MobileToolbar';
import MobileLoader from 'pages/Shop/UsableComponent/MobileLoader';
import '../AboutPage/about.css'

const styles = makeStyles((theme) => ({
  title: {
    paddingBlock: '15px',
    fontSize: '2.5rem'
  },
  body: {
    color: theme.palette.mode == 'dark' ? 'white' : '#242424'
  },
  main: {
    color: 'white',
    [theme.breakpoints.up('md')]: {
      paddingInline: '200px',
    },
    [theme.breakpoints.up('sm')]: {
      paddingInline: '30px'
    },
    [theme.breakpoints.up('xs')]: {
      paddingInline: '30px'
    }
  },
  form: {
    paddingBlock: '30px'
  },
  body_desc: {
    fontFamily: 'gamja'
  }
}))

const ContactPage = () => {
  const theme = useTheme()
  const matches = useMediaQuery(theme.breakpoints.up('md'));
  const classes = styles(theme);
  const [data, setData] = useState({})
  const [flag, setFlag] = useState(false)
  const contactData = useSelector((state) => state.auth.contactPageList)
  const dispatch = useDispatch()

  useEffect(() => {

    if (Object.keys(contactData).length == 0) {
      dispatch(getAllContactPage())
    } else {
      setData(contactData.contact)
      setFlag(true)
    }

  }, [contactData])

  return (
    <Grid container justifyContent="center"
      sx={(theme) => ({
        color: 'white',
        [theme.breakpoints.up('md')]: {
          paddingInline: '320px',
        },
        [theme.breakpoints.up('sm')]: {
          paddingInline: '30px'
        },
        [theme.breakpoints.up('xs')]: {
          paddingInline: '30px'
        }
      })}
    >
      {flag ? (Object.keys(data).length > 0) ? <Grid item xs={12} md={12} sm={12} className={`${classes.body} ${matches ? "content-body-view" : 'content-body-view-mobile page-margin'}`}>
        <Grid container spacing={1}>
          <Grid item xs={12} sm={6} md={7} className='leftPannel'>
            <Typography variant="h1" component="h4" className={classes.title + ' title_head'}>
              {data?.title}
            </Typography>
            <Typography variant='body' className=''
              sx={(theme) => ({
                [theme.breakpoints.up('md')]: {
                  lineHeight: '2.5rem'
                },
                [theme.breakpoints.up('sm')]: {
                  lineHeight: 1
                },
                [theme.breakpoints.up('xs')]: {
                  lineHeight: 1
                }

              })} >
              {data?.description}
            </Typography>
            <Grid className='contactFrom' item xs={12} sm={12} md={12}>
              <ContactForm />
            </Grid>
          </Grid>

          <Grid className='contactInfo' item xs={12} md={5} sm={6} sx={(theme) => ({
            [theme.breakpoints.up('md')]: {
              paddingLeft: '8px'
            },
            [theme.breakpoints.up('sm')]: {
              paddingLeft: '15px'
            },
            [theme.breakpoints.up('xs')]: {
              paddingLeft: '15px'
            }
          })}>
            <Typography variant="h1" component="h4" className={classes.title + ' title_head'}>
              {data?.title_small}
            </Typography>
            <div className="suppourt">
              <div className='title_font email'>
                <div className='icon'>
                  <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"><title /><g data-name="mail email e-mail letter" id="mail_email_e-mail_letter"><path d="M28,13a1,1,0,0,0-1,1v8a1,1,0,0,1-1,1H6a1,1,0,0,1-1-1V14a1,1,0,0,0-2,0v8a3,3,0,0,0,.88,2.12A3,3,0,0,0,6,25H26a3,3,0,0,0,2.12-.88A3,3,0,0,0,29,22V14A1,1,0,0,0,28,13Z" /><path d="M15.4,18.8a1,1,0,0,0,1.2,0L28.41,9.94a1,1,0,0,0,.3-1.23,3.06,3.06,0,0,0-.59-.83A3,3,0,0,0,26,7H6a3,3,0,0,0-2.12.88,3.06,3.06,0,0,0-.59.83,1,1,0,0,0,.3,1.23ZM6,9H26a.9.9,0,0,1,.28,0L16,16.75,5.72,9A.9.9,0,0,1,6,9Z" /></g></svg>
                </div>
                <span className='emailText'>
                  <label>Email</label>
                  {/* {data?.email} */}
                  <a className='email-a' href={'mailto:' + data?.email}>{data?.email}</a>
                </span>
              </div>

              <div className='title_font phone'>
                <div className='icon'>
                  <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><title /><g data-name="1" id="_1"><path d="M348.73,450.06a198.63,198.63,0,0,1-46.4-5.85c-52.43-12.65-106.42-44.74-152-90.36s-77.71-99.62-90.36-152C46.65,146.75,56.15,99.61,86.69,69.07l8.72-8.72a42.2,42.2,0,0,1,59.62,0l50.11,50.1a42.18,42.18,0,0,1,0,59.62l-29.6,29.59c14.19,24.9,33.49,49.82,56.3,72.63s47.75,42.12,72.64,56.31L334.07,299a42.15,42.15,0,0,1,59.62,0l50.1,50.1a42.16,42.16,0,0,1,0,59.61l-8.73,8.72C413.53,439,383.73,450.06,348.73,450.06ZM125.22,78a12,12,0,0,0-8.59,3.56l-8.73,8.72c-22.87,22.87-29.55,60-18.81,104.49,11.37,47.13,40.64,96.1,82.41,137.86s90.73,71,137.87,82.41c44.5,10.74,81.61,4.06,104.48-18.81l8.72-8.72a12.16,12.16,0,0,0,0-17.19l-50.09-50.1a12.16,12.16,0,0,0-17.19,0l-37.51,37.51a15,15,0,0,1-17.5,2.72c-30.75-15.9-61.75-39.05-89.65-66.95s-51-58.88-66.94-89.63a15,15,0,0,1,2.71-17.5l37.52-37.51a12.16,12.16,0,0,0,0-17.19l-50.1-50.11A12.07,12.07,0,0,0,125.22,78Z" /><path d="M364.75,269.73a15,15,0,0,1-15-15,99.37,99.37,0,0,0-99.25-99.26,15,15,0,0,1,0-30c71.27,0,129.25,58,129.25,129.26A15,15,0,0,1,364.75,269.73Z" /><path d="M428.15,269.73a15,15,0,0,1-15-15c0-89.69-73-162.66-162.65-162.66a15,15,0,0,1,0-30c106.23,0,192.65,86.43,192.65,192.66A15,15,0,0,1,428.15,269.73Z" /></g></svg>
                </div>
                <span className='phoneTxt'>
                  <label>Phone</label>
                  {/* +91{data?.mobile} */}
                  <a className='mobile-a' href={'tel:' + '+91' + data?.mobile}>+91{data?.mobile}</a>
                </span>
              </div>
            </div>

          </Grid>
        </Grid>

      </Grid> : <div></div> : <Grid item xs={12} md={12} sm={12} className={classes.body}> <MobileLoader /> </Grid>}
    </Grid>
  )
}

export default ContactPage