import React from 'react';
import { Typography, Card, CardMedia, Grid, Box, CardContent, Chip, Dialog, useTheme } from '@mui/material';
import './offer.css'
import { useRecoilState, useSetRecoilState } from 'recoil';
import { mentorState, serviceState, topicsState } from 'store';
import { useNavigate } from 'react-router-dom';
import { ServiceButton } from 'components/common';
import DatePicker from 'components/DaterPicker';
import { LOADERCLOSE } from 'redux/action/types';
import { useDispatch, useSelector } from 'react-redux';
import { dayOfWeek, getDateChange } from 'config/KeyData';
import { DayEnumType } from 'types';
import { range } from 'utils/helper';

interface ServiceCardProps {
  topic: any;
  count: any;
  locationData: any;
  travel: any;
}
const ServiceCard: React.FC<ServiceCardProps> = ({ topic, count, locationData, travel }) => {
  const { motivation, name, description, image, _id, accept, waiting_time, waiting_number, waiting_key, color, value, type, location, tags, shop, shop_id, offer_id, lattitude, longitude, seat } = topic;

  const dayName: DayEnumType = dayOfWeek[new Date().getDay()];
  const { start_hour, end_hour, available } = shop.time_slot[dayName];
  const limitRange = available == true ? range(start_hour, end_hour, 1) : []

  const theme = useTheme()
  const [open, setOpen] = React.useState(false);
  const [key, setKey] = React.useState(false);
  const [show, setShow] = React.useState(false);
  const [motivate, setMotivate] = useRecoilState(serviceState);
  const setTopic = useSetRecoilState(topicsState);
  const [mentorData, setMentorData] = useRecoilState(mentorState);
  const history = useNavigate();
  const dispatch = useDispatch();
  const topicText = '#f5f5f5';

  const handleSearch = (item: any) => {
    // setTopic([item._id])
    // setMotivate({
    //   label: item.motivation,
    //   value: item.motivation
    // })
    history('/user/' + shop_id, { state: { id: item.motivation } })
  }

  const handleClose = () => {
    setOpen(false)
    setKey(false)
    dispatch({ type: LOADERCLOSE })
  }

  const handleCurrent = () => {
    // if (travel.length == 0) {
    //   if ("geolocation" in navigator) {
    //     console.log("Available");
    //   } else {
    //     console.log("Not Available");
    //   }
    //   return
    // }
    setOpen(true)
    setKey(true)
    setMentorData(shop)
  }

  const handleBook = () => {
    setOpen(true)
    setKey(false)
  }

  const handleShow = (val: any) => {
    setShow(val)
  }

  return (
    <>
      <Card sx={{ borderRadius: '4px' }} className="service-card" id="product-card" >
        <div className="cardImg">
          <div className='offer-Card' style={{ background: color }} >
            <span>{value}</span>
          </div>
          <CardMedia
            component="img"
            sx={{ width: '100%', objectFit: 'unset', borderRadius: 'none', border: 0 }}
            image={image}
            alt={name}
            onClick={() => { handleSearch({ _id, motivation }) }}
          />
        </div>

        <Box >
          <CardContent sx={{ padding: '8px' }}>
            <Grid container spacing={1} sx={{ display: { xs: 'none', md: 'flex', sm: 'none' } }}>
              <Grid
                item
                xs={12}
                sm={12}
                md={12}
                sx={{ color: theme.palette.mode == 'dark' ? 'white' : 'black' }}>
                <Typography className='bookingTitle' variant="h6" sx={{ fontWeight: 800 }} onClick={() => { handleSearch({ _id, motivation }) }}>
                  {name} (<span className={`${(!accept || accept == null || accept == undefined) ? 'open-btn-red' : (accept == true && limitRange.includes(new Date().getHours()) == true) ? 'open-btn-green' : 'open-btn-red'}`} >{(!accept || accept == null || accept == undefined) ? 'close' : (accept == true && limitRange.includes(new Date().getHours()) == true) ? 'open' : 'close'}</span>)
                </Typography>
                <Typography variant="h6" sx={{ fontWeight: 500 }} onClick={() => { handleSearch({ _id, motivation }) }}>
                  {motivation}
                </Typography>
              </Grid>
              {/* <Grid item xs={12} sm={12} md={12}>
                <Typography variant="h6" sx={{ fontWeight: 500 }} onClick={() => { handleSearch({ _id, motivation }) }}>
                  {motivation}
                </Typography>
              </Grid> */}
              <Grid className="userWating" item xs={12} sm={12} md={12}>
                <Typography
                  variant="body2" textAlign="center">
                  {count.length > 0 && <>
                    {count.filter((cc: any) => cc._id == _id && cc.shop_id == shop_id && cc.seat == seat)[0]?.count == 0 ? <span className='waiting'>No waiting</span> : <><span style={{ color: 'red' }}>{count.filter((cc: any) => cc._id == _id && cc.shop_id == shop_id && cc.seat == seat)[0]?.count}</span> users waiting in queue</>}</>}
                </Typography>
              </Grid>
              <Grid item xs={6} sm={6} md={6}>
                <Typography style={{ fontWeight: 'bold' }}>Seat Number</Typography>
              </Grid>
              <Grid item xs={6} sm={6} md={6}>
                <Typography variant="body2">{seat}</Typography>
              </Grid>
              <Grid item xs={6} sm={6} md={6}>
                <Typography style={{ fontWeight: 'bold' }}>Service Time</Typography>
              </Grid>
              <Grid item xs={6} sm={6} md={6}>
                <Typography className='waiting' variant="body2">{(waiting_number < 60) ? waiting_number + ' ' + waiting_key : getDateChange(waiting_number)}</Typography>
              </Grid>
              {count.length > 0 && <>
                <Grid item xs={6} sm={6} md={6}>
                  <Typography style={{ fontWeight: 'bold' }}>Waiting Time</Typography>
                </Grid>
                <Grid item xs={6} sm={6} md={6}>
                  <Typography className='waiting' variant="body2" >{count.filter((cc: any) => cc._id == _id && cc.shop_id == shop_id && cc.seat == seat)[0]?.time}</Typography>
                </Grid>
              </>}
              {travel.length > 0 && travel.filter((cc: any) => cc._id == shop_id).length > 0 && <>
                <Grid item xs={6} sm={6} md={6}>
                  <Typography style={{ fontWeight: 'bold' }}>Travelling Time</Typography>
                </Grid>
                <Grid item xs={6} sm={6} md={6}>
                  <Typography className='travelling' variant="body2">{travel.filter((cc: any) => cc._id == shop_id)[0]?.duration}</Typography>
                </Grid>
              </>}
              <Grid item xs={12} sm={12} md={12} className="book-btn" id="bookBtn" >
                <ServiceButton variant="contained" disabled={(!accept || accept == null || accept == undefined) ? false : (accept == true && limitRange.includes(new Date().getHours())) ? false : true} onClick={() => { handleCurrent() }}>
                  Book
                </ServiceButton>
              </Grid>
            </Grid>

            <Grid container spacing={1} sx={{ display: { xs: 'flex', md: 'none', sm: 'flex' } }}>
              <Grid
                item
                xs={12}
                sm={12}
                md={12}
                sx={{ color: theme.palette.mode == 'dark' ? 'white' : 'black' }}>
                <Typography variant="h6" sx={{ fontWeight: 600, fontSize: '1rem' }} onClick={() => { handleSearch({ _id, motivation }) }}>
                  {name} (<span className={`${(!accept || accept == null || accept == undefined) ? 'open-btn-red' : (accept == true && limitRange.includes(new Date().getHours()) == true) ? 'open-btn-green' : 'open-btn-red'}`} >{(!accept || accept == null || accept == undefined) ? 'close' : (accept == true && limitRange.includes(new Date().getHours()) == true) ? 'open' : 'close'}</span>)
                </Typography>
              </Grid>
              <Grid item xs={12} sm={12}>
                <Typography variant="h6" sx={{ fontWeight: 300, fontSize: '1rem' }} onClick={() => { handleSearch({ _id, motivation }) }}>
                  {motivation}
                </Typography>
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <Typography
                  variant="body2" textAlign="center">
                  {count.length > 0 && <>
                    {count.filter((cc: any) => cc._id == _id && cc.shop_id == shop_id && cc.seat == seat)[0]?.count == 0 ? <span className='waiting'>No waiting</span> : <><span style={{ color: 'red' }}>{count.filter((cc: any) => cc._id == _id && cc.shop_id == shop_id && cc.seat == seat)[0]?.count}</span> users waiting in queue</>}</>}
                </Typography>
              </Grid>
              {/* <Grid item xs={12} sm={12} textAlign="center">
                <Chip
                  style={{
                    color: topicText,
                  }}
                  size="small"
                  label={`Service Time is ${waiting_number + ' ' + waiting_key}`}
                />
              </Grid> */}
              <Grid item xs={12} sm={12} md={12} sx={{
                display: 'flex',
                justifyContent: 'space-between',
                fontSize: '10px'
              }}>
                <div style={{ fontWeight: 'bold' }}>Seat Number</div>
                <div>{seat}</div>
              </Grid>
              <Grid item xs={12} sm={12} md={12} sx={{
                display: 'flex',
                justifyContent: 'space-between',
                fontSize: '10px'
              }}>
                <div style={{ fontWeight: 'bold' }}>Service Time</div>
                <div className='waiting'>{(waiting_number < 60) ? waiting_number + ' ' + waiting_key : getDateChange(waiting_number)}</div>
              </Grid>

              {count.length > 0 && <Grid item xs={12} sm={12} md={12} sx={{
                display: 'flex',
                justifyContent: 'space-between',
                fontSize: '10px'
              }}>
                <div style={{ fontWeight: 'bold' }}>Waiting Time</div>
                <div className='waiting'>{count.filter((cc: any) => cc._id == _id && cc.shop_id == shop_id && cc.seat == seat)[0]?.time}</div>
              </Grid>}
              {travel.length > 0 && travel.filter((cc: any) => cc._id == shop_id).length > 0 && <>
                <Grid item xs={12} sm={12} md={12} sx={{
                  display: 'flex',
                  justifyContent: 'space-between',
                }}>
                  <div style={{ fontWeight: 'bold', fontSize: '10px' }}>Travelling Time</div>
                  <div style={{ fontSize: '9px' }} className='travelling'>{travel.filter((cc: any) => cc._id == shop_id)[0]?.duration}</div>
                </Grid>
              </>}
              <Grid item xs={12} sm={12} md={12} className="book-btn" >
                <ServiceButton variant="contained" disabled={(!accept || accept == null || accept == undefined) ? false : (accept == true && limitRange.includes(new Date().getHours()) == true) ? false : true} onClick={() => { handleCurrent() }} >
                  Book
                </ServiceButton>
              </Grid>
            </Grid>
          </CardContent>
        </Box>
      </Card>
      <Dialog open={open} className="bookingPopup"
        disableEscapeKeyDown={true} onClose={() => {
          if (show == true) {

          } else { handleClose() }
        }}
        sx={(theme) => ({
          [theme.breakpoints.up('sm')]: {
            minHeight: '770px !important'
          },
          [theme.breakpoints.up('xs')]: {
            minHeight: '450px !important'
          },
          [theme.breakpoints.up('md')]: {
            minHeight: '770px !important'
          },
        })}
      >
        {open == true && key == true && <DatePicker service={{
          label: motivation, value: _id, labelType: name, tagVal: tags, location: (travel.length > 0) ? {
            ...locationData,
            distance: travel.filter((cc: any) => cc._id == shop_id)[0]?.distance,
            distanceVal: travel.filter((cc: any) => cc._id == shop_id)[0]?.distanceVal,
            duration: travel.filter((cc: any) => cc._id == shop_id)[0]?.duration,
            durationVal: travel.filter((cc: any) => cc._id == shop_id)[0]?.durationVal,
          } : { ...locationData }
        }} close={handleClose} some={true} handleView={handleShow} />}
        {open == true && key == false && <DatePicker service={{
          label: motivation, value: _id, labelType: name, tagVal: tags, location: (travel.length > 0) ? {
            ...locationData,
            distance: travel.filter((cc: any) => cc._id == shop_id)[0]?.distance,
            distanceVal: travel.filter((cc: any) => cc._id == shop_id)[0]?.distanceVal,
            duration: travel.filter((cc: any) => cc._id == shop_id)[0]?.duration,
            durationVal: travel.filter((cc: any) => cc._id == shop_id)[0]?.durationVal,
          } : { ...locationData }
        }} close={handleClose} some={false} handleView={handleShow} />}
      </Dialog>
    </>
  );
};

export default React.memo(ServiceCard);
