import React, { useState, useEffect } from 'react'
import Carousel from "react-elastic-carousel";
import { useDispatch, useSelector } from 'react-redux';
import { getAlServiceOffers } from 'redux/action';
import Loader from 'react-loader-spinner';
import { Grid, InputBase, Paper, Box, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import '../Carousel/carousel.css'
import ServiceCard from './ServiceCard';
import Geocode from 'react-geocode';
import SocketContext from 'config/socket';
import { useContext } from 'react';
import { ACTIVE_EMPLOYEE_LIST } from 'redux/action/types';
Geocode.setApiKey('AIzaSyCqiCKV0cm-EpHp2hev4XkD5wG9JGxFbVs');
Geocode.enableDebug();

const StyledBox = React.memo(styled(Box)({
  width: '100%',
  height: '15vh',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',

  '& svg': {
    fill: 'lightgray',
  },
}));

const breakPoints = [
  { width: 1, itemsToShow: 1 },
  { width: 550, itemsToShow: 1 },
  { width: 768, itemsToShow: 3 },
  { width: 1200, itemsToShow: 4 },
];

const ServiceOffer = () => {

  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const shopCateData = useSelector((state) => state.auth.serviceOfferList)
  const [locData, setLocData] = useState([])
  const [count, setCount] = useState([])
  const [userD, setUserD] = useState({
    lattitude: '',
    longitude: '',
    address: ''
  })
  const dispatch = useDispatch()
  const context = useContext(SocketContext)

  useEffect(() => {


    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      if (localStorage.getItem('auth-token') !== null) {
        context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
      }
    })

    context.socket.on('offerValue', data => {
      console.log('offer', data)
      setCount(data)
    })

    context.socket.on('locationValue', data => {
      console.log('loc val', data)
      setLocData(data)
    })

    context.socket.on('shopEmployeeData', data => {
      //console.log('shop employee Data', data)
      dispatch({ type: ACTIVE_EMPLOYEE_LIST, payload: { data: data } })
    })

    context.socket.on('disconnect', () => {
      context.socket.removeAllListeners()
    })

    return () => {
      context.socket.removeAllListeners()
    }

  }, [])

  useEffect(() => {
    if (shopCateData.length == 0) {
      dispatch(getAlServiceOffers())
    } else {
      setElement(shopCateData.offer)
      setFlag(true)
    }
  }, [shopCateData])

  useEffect(() => {
    if (context.socket.connected == true && count.length == 0) {
      context.socket.emit('offer', 'hello')
      context.socket.emit('shopEmployee', 'hello')
    }

  }, [count, context])

  useEffect(() => {

    if ("geolocation" in navigator) {
      console.log("Available");
    } else {
      console.log("Not Available");
    }

    navigator.geolocation.getCurrentPosition(function (position) {
      console.log('latitude', position.coords.latitude, 'longitude', position.coords.longitude)
      setUserD({ ...userD, lattitude: position.coords.latitude, longitude: position.coords.longitude })
      context.socket.emit('locationGlobal', { lattitude: position.coords.latitude, longitude: position.coords.longitude })
      Geocode.fromLatLng(position.coords.latitude, position.coords.longitude).then(
        (response) => {
          const address = response.results[0].formatted_address;
          console.log(address, 'latitude', position.coords.latitude, 'longitude', position.coords.longitude);
          setUserD({ ...userD, address: address, lattitude: position.coords.latitude, longitude: position.coords.longitude })
        },
        (error) => {
          console.error('Location Err', error);
        }
      );
    },
      function (error) {
        console.error("Error Code = " + error.code + " - " + error.message);
      });

  }, [])


  return (
    <>
      {flag ? element.length > 0 ? <React.Fragment>
        <Typography className="title_head latestNew" variant="h5" component="h1" sx={{ color: 'white', paddingLeft: '21px', paddingBlock: '15px' }}>Latest Offers</Typography>
        <Carousel className='service-carousel' enableSwipe={true} enableMouseSwipe={true} breakPoints={breakPoints}>
          {element.map((cc, i) => {
            return (
              <ServiceCard key={cc._id + i} topic={{ ...cc.service_id, color: cc.color, value: cc.value, shop: cc.shop, offer_id: cc._id }} count={count} locationData={userD} travel={locData} />
            )
          })}
        </Carousel>
      </React.Fragment> : <></> : <StyledBox>
        <Loader type="ThreeDots" color="#00BFFF" height={80} width={80} />
      </StyledBox>}
    </>
  )
}

export default ServiceOffer