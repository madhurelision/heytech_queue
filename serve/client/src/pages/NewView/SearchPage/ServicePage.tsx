import React, { useEffect, useState } from 'react';
import { Grid, Paper } from '@mui/material';
import { styled } from '@mui/material/styles';
import { ShopSelectN as Select } from '../../../components/common/index';
import InfiniteScroll from 'react-infinite-scroll-component';
import { categoryState, serviceState, tabIndexState, topicsState } from '../../../store/local';
import { useRecoilState, useSetRecoilState } from 'recoil';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useDispatch, useSelector } from 'react-redux';
import { getAllTypeActiveService } from 'redux/action';
import ServiceActiveSearchComponent from 'pages/Shop/UsableComponent/ServiceActiveSearchComponent';
import NewServiceCard from '../NewServiceCard';
import { useParams } from 'react-router-dom';


var mainValue: any = [{
  value: 'All',
  label: 'All',
}]

const SearchWrapper = React.memo(styled(Grid)({
  marginBottom: '15px',
  '.search_wrapper': {
    display: 'flex',
    alignItems: 'center',
  },
}));


const CardContainer = React.memo(styled(Grid)({
  // Display: 'flex',
  // flexDirection: 'row',
  // justifyContent: 'space-around',
  display: 'grid',
  gridTemplateColumns: 'repeat(auto-fill, 320px)',
  justifyContent: 'space-between',
  marginTop: '3rem',
}));

const ServicePage = () => {
  const [motivation, setMotivation] = useRecoilState(serviceState);
  const setTabIndex = useSetRecoilState(tabIndexState);
  const setTopic = useSetRecoilState(topicsState);
  const [category, setCategory] = useRecoilState(categoryState);
  const [value, setValue] = useState({ label: 'All', value: 'All' })
  // const [items, setItems] = useState(topics.slice(0, LEN));

  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('md'));

  // @ts-ignore
  const motivationValue = value ? value?.value : 'All';
  // @ts-ignore
  const categoryValue = category?.value;
  const [data, setData] = useState([])
  const [element, setElement] = useState<[]>([])
  const [flag, setFlag] = useState(false)
  const serviceData = useSelector((state: any) => state.auth.newActiveServiceList)
  const moreData = useSelector((state: any) => state.auth.newServiceMore)
  const checkService = useSelector((state: any) => state.auth)
  const dispatch = useDispatch()
  const { id } = useParams()

  useEffect(() => {
    if (serviceData.length == 0 && checkService.serviceRequestFailed == false) {
      dispatch(getAllTypeActiveService({ type: id }, 0, 20))
    } else {
      setData(mainValue.concat([...new Set(serviceData.filter((dd: any) => dd.type == id).map((cc: any) => cc.motivation))].map((ss: any) => {
        return {
          label: ss,
          value: ss
        }
      })))
      setElement(serviceData.filter((dd: any) => dd.type == id))
      setFlag(true)
    }
  }, [serviceData])

  const filterTopics = (topics: [], motivation: string) => {
    if (motivation === 'All' || motivation === null) return topics;
    return element.filter((main: any) => main.motivation === motivation);
  };

  const handleMotivation = (val: any) => {
    setElement(serviceData.filter((dd: any) => dd.type == id))
    setValue(val)
    //setTopic(val.value)
  }

  const handleView = (e: any, ccd: any) => {
    if (ccd == null || ccd == undefined) {
      setElement(serviceData.filter((dd: any) => dd.type == id))
    } else {
      setElement(serviceData.filter((cc: any) => cc._id == ccd._id))
    }
  }

  return (
    <div className="mobile_padding" style={{ padding: '0rem 2rem' }}>
      <SearchWrapper justifyContent='center' container spacing={2}>
        <Grid item xs={12} sm={4} sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }}>
          <Paper
            className="searchBy"
            sx={{ display: 'flex', minWidth: '300px' }}>
            <ServiceActiveSearchComponent handleView={handleView} />
          </Paper>
        </Grid>
        <Grid className="searchBy" item xs={12} sm={4} sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
          <ServiceActiveSearchComponent handleView={handleView} />
        </Grid>
        <Grid className="filterOuter" item xs={12} sm={4} lg={3} >
          <Paper
            sx={{ display: 'flex', minWidth: '300px' }}>
            <Select
              menuPlacement="auto"
              name="Motivation"
              sx={{ fontSize: '20px' }}
              //options={motivationOptions}
              options={data}
              value={value}
              onChange={(e) => { handleMotivation(e) }}
              isSearchable={matches}
              classNamePrefix="select"
              placeholder={<span>Filter by Services</span>}
            />
          </Paper>
        </Grid>
      </SearchWrapper>
      <InfiniteScroll
        className="searchServices"
        dataLength={element.length}
        next={() => { dispatch(getAllTypeActiveService({ type: id }, element.length, 20)) }}
        hasMore={moreData}
        loader={<h4 className="loading">Loading...</h4>}>
        <Grid container spacing={2}>
          {filterTopics(element, motivationValue).map((item: any) => (
            <Grid item xs={12} sm={4} md={3} className="blockCards"
              key={item._id}
              onClick={() => {
                setTopic([item._id]);
                setMotivation({
                  label: item.motivation,
                  value: item.motivation
                });
                setTabIndex('1');
              }}>
              <NewServiceCard topic={item} />
            </Grid>
          ))}
        </Grid>
      </InfiniteScroll>
      
    </div>

    
  );
};

export default ServicePage;
