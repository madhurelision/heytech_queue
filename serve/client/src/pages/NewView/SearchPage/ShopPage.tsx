import React, { useEffect, useState } from 'react';
import { Grid, Paper, Box, Dialog, DialogTitle, Card, CardContent, IconButton, Button, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import { BookingButton, ShopSelect as Select, ShopSelectN } from '../../../components/common/index';
import { useRecoilState } from 'recoil';
import { categoryState, serviceState, expertState, topicsState } from '../../../store/local';
import { SERVER_URL } from '../../../config.keys';
import axios from 'axios';
import Loader from 'react-loader-spinner';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useDispatch, useSelector } from 'react-redux';
import { getAllBanner, getAllExpertise, getAllTypeActiveService } from 'redux/action';
import SearchComponent from 'pages/Shop/UsableComponent/SearchComponent';
import NoData from 'pages/Shop/UsableComponent/NoData'
import { SERVICE_REQ_FAILED, SHOP_LIST } from 'redux/action/types';
import NewUserCard from '../NewUserCard';
import { Navigate, useParams } from 'react-router-dom';
import { makeStyles } from '@mui/styles'
import { Close } from '@mui/icons-material';
var mainValue: any = [{
  value: 'All',
  label: 'All',
}]

const selectStyles = makeStyles(() => ({
  select__control: {
    borderTopLeftRadius: '0px',
    borderBottomLeftRadius: '0px',
  }
}))

const GridWrapper = React.memo(styled(Grid)({
  '.search_wrapper': {
    display: 'flex',
    alignItems: 'center',
  },
}));

const StyledBox = React.memo(styled(Box)({
  width: '100%',
  height: '15vh',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',

  '& svg': {
    fill: 'lightgray',
  },
}));
const getMentors = async (expertise: string, topic: any[], type: any) => {
  const expertise_ = expertise === undefined ? 'All' : expertise;
  const topics_ = topic.length === 0 ? 'All' : topic;
  const type_ = type === undefined ? 'All' : type;
  // const distances_ = (distance == 'All' || distance == 'AnyWhere') ? 'All' : distance;

  const { data: response } = await axios.get(
    `${SERVER_URL}/api/get-shops`,
    {
      params: {
        expertise: expertise_,
        topic: topics_,
        type: type_,
        position: 'All',
        distance: 'All'
        // skip: skip,
        // limit: limit
      },
    },
  );
  // @ts-ignore
  return response;
  // return shuffleArray(response);
};

// @ts-ignore
const RenderCards = ({
  isLoading,
  data,
  element
}: {
  isLoading: boolean;
  data: any[];
  element: any[];
}) => {
  if (isLoading || typeof data === 'undefined') return <div />;

  const users = data.slice(0, 50);
  return (
    <Grid container spacing={2} sx={{ marginTop: '1rem' }}>
      {users.map((user, index) => (
        <Grid item xs={12} sm={6} md={3} key={index}>
          <NewUserCard
            key={index}
            // @ts-ignore
            user={user}
            value={element}
          />
        </Grid>
      ))}
    </Grid>
  );
};

interface ShopPageProps {
  handleBtn: any;
  view: any;
  mainId: any;
}

const ShopPage: React.FC<ShopPageProps> = ({ handleBtn, view, mainId }) => {

  const classes = selectStyles()
  const [expertise, setExpertise] = useRecoilState(expertState);
  const [motivation, setMotivation] = useRecoilState(serviceState);
  const [category, setCategory] = useRecoilState(categoryState);
  // @ts-ignore
  const expertiseValue = expertise?.value;
  // @ts-ignore
  const categoryValue = category?.value;
  const [topic, setTopic] = useRecoilState(topicsState);
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('md'));
  const [element, setElement] = useState([])
  const [value, setValue] = useState([])
  const [flag, setFlag] = useState(false)
  const [main, setMain] = useState(true)
  const [show, setShow] = useState(false)
  const expertiseData = useSelector((state: any) => state.auth.expertiseList)
  const checkService = useSelector((state: any) => state.auth)
  const serviceData = useSelector((state: any) => state.auth.newActiveServiceList)
  const shopData = useSelector((state: any) => state.auth.shopList)
  const dispatch = useDispatch()
  const [banner, setBanner] = React.useState<any>([]);
  const bannerData = useSelector((state: any) => state.auth.bannerList)
  const [userD, setUserD] = useState({
    lattitude: '',
    longitude: ''
  })
  const [distance, setDistance] = useState({
    label: 'All',
    value: 'All'
  })
  // @ts-ignore
  const distanceValue = distance?.value;

  const { id } = useParams()

  useEffect(() => {
    if (bannerData.length == 0) {
      dispatch(getAllBanner())
    } else {
      setBanner(bannerData.banner)
    }
  }, [bannerData])

  useEffect(() => {
    if (expertiseData.length == 0) {
      dispatch(getAllExpertise())
    }
  }, [expertiseData])

  const handleMotivation = (val: any) => {
    setMotivation(val)
    setTopic(serviceData.filter((cc: any) => cc.motivation == val.label).map((dd: any) => dd._id))
    setShow(false)
  }

  const handleNear = (val: any) => {
    setDistance(val)
    setShow(false)
  }

  const handleView = (e: any, ccd: any) => {
    if (ccd == null || ccd == undefined) {
      setValue(shopData.filter((cc: any) => cc.type == categoryValue))
    } else {
      setValue(shopData.filter((cc: any) => cc._id == ccd._id))
    }
  }


  const handleOpen = () => {
    setShow(!show)
  }

  const handleClose = () => {
    setShow(false)
  }

  useEffect(() => {
    if (serviceData.length == 0 && checkService.serviceRequestFailed == false) {
      dispatch(getAllTypeActiveService({ type: 'All' }, 0, 20))
    } else {
      setElement(serviceData)
      setFlag(true)
    }
  }, [serviceData])

  useEffect(() => {
    dispatch(getAllTypeActiveService({ type: id }, 0, 20))
  }, [])

  useEffect(() => {
    console.log('request Created')
    getMentors(expertiseValue, topic, view == true ? mainId : id).then((cc) => {
      setValue(cc)
      setMain(false)
      dispatch({ type: SHOP_LIST, payload: { data: cc } })
    }).catch((err) => {
      console.log('main Err', err)
      if (!err.response || err.response === null || err.response === undefined) {
        dispatch({ type: SERVICE_REQ_FAILED, payload: err.message })
      } else {
        dispatch({ type: SERVICE_REQ_FAILED, payload: err.response.data.message })
      }
    })

  }, [expertiseValue, topic, categoryValue, view, mainId])

  // useEffect(() => {

  //   if ("geolocation" in navigator) {
  //     console.log("Available");
  //   } else {
  //     console.log("Not Available");
  //   }

  //   navigator.geolocation.getCurrentPosition(function (position: any) {
  //     console.log('position', position.coords)
  //     setUserD({ ...userD, 'lattitude': position.coords.latitude, 'longitude': position.coords.longitude })
  //   },
  //     function (error) {
  //       console.error("Error Code = " + error.code + " - " + error.message);
  //     });

  // }, [])

  if (typeof id === 'undefined') return <Navigate to="/global" />;

  const content =
    main === false ? (value.length == 0 ? <NoData /> :

      <RenderCards
        isLoading={main}
        // @ts-ignore
        data={value}
        element={banner}
      />
    ) : (
      <StyledBox>
        <Loader type="ThreeDots" color="#00BFFF" height={80} width={80} />
      </StyledBox>
    );

  return (
    <>
      <GridWrapper className='filterOuter' justifyContent="center" container spacing={1} >
        <Grid className='searchFil' item xs={8} sm={8} md={6} sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }}>
          <SearchComponent handleView={handleView} />
        </Grid>
        <Grid item xs={4} sm={4} md={6} sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }}>
          <BookingButton sx={{ padding: '8px 35px !important' }} variant="contained" onClick={handleOpen} >Filter</BookingButton>
        </Grid>
        <Grid className='searchFil' item xs={12} sm={12} md={6} sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
          <SearchComponent handleView={handleView} />
        </Grid>

        {/* <Grid item xs={12} sm={12} md={4}>
          <Paper
            sx={{ display: 'flex', minWidth: '300px' }}>
            <Select
              name="Expertise"
              menuPlacement="auto"
              sx={{ fontSize: '20px' }}
              options={view == true ? mainValue.concat(expertiseData) : mainValue.concat(expertiseData.filter((dd: any) => dd.type == id))}
              value={expertise}
              onChange={setExpertise}
              isSearchable={true}
              classNamePrefix="select"
              placeholder={<span>Filter by Expertise</span>}
            />
          </Paper>
        </Grid> */}
        <Grid item xs={12} sm={12} md={6} sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
          <Paper
            sx={{ display: 'flex', minWidth: '300px' }}>
            <ShopSelectN
              menuPlacement="auto"
              name="Motivation"
              sx={{ fontSize: '20px' }}
              options={view == true ? mainValue.concat([...new Set(element.map((cc: any) => cc.motivation))].map((ss: any) => {
                return {
                  label: ss,
                  value: ss
                }
              })) : mainValue.concat([...new Set(element.filter((dd: any) => dd.type == id).map((cc: any) => cc.motivation))].map((ss: any) => {
                return {
                  label: ss,
                  value: ss
                }
              }))}
              value={motivation}
              onChange={(e) => { handleMotivation(e) }}
              isSearchable={true}
              classNamePrefix="select"
              placeholder={<span>Filter by Services</span>}
            />
          </Paper>
        </Grid>
        {/* {userD.longitude != '' && userD.lattitude !== '' && <Grid item xs={12} sm={12} md={3} sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
          <Paper
            sx={{ display: 'flex', minWidth: '300px' }}>
            <ShopSelectN
              menuPlacement="auto"
              name="Motivation"
              sx={{ fontSize: '20px' }}
              //options={motivationOptions}
              options={[
                {
                  label: 'All',
                  value: 'All'
                },
                {
                  label: '10 Km',
                  value: 10000
                },
                {
                  label: '50 Km',
                  value: 50000
                },
                {
                  label: '100 Km',
                  value: 100000
                },
                {
                  label: '500 Km',
                  value: 500000
                },
                {
                  label: 'AnyWhere',
                  value: 'AnyWhere'
                }
              ]}
              value={distance}
              onChange={(e) => { handleNear(e) }}
              isSearchable={true}
              classNamePrefix="select"
              placeholder={<span style={{ display: 'flex' }} >
                Filter by Nearby</span>}
            />
          </Paper>
        </Grid>} */}
      </GridWrapper>
      {content}
      {/* <StyledBox>
        <Loader type="ThreeDots" color="#00BFFF" height={80} width={80} />
      </StyledBox> */}
      <Dialog
        open={show}
        onClose={handleClose}
        fullWidth
        aria-labelledby="responsive-dialog-title"
      >
        {/* <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Filter</Box>
          <Box>
            <IconButton onClick={handleClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle> */}
        <Grid>
          <Card>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Box flexGrow={1} ><Typography className='title_head' variant="h5" component="div">
                      Filter
                    </Typography> </Box>
                    <Box>
                      <IconButton onClick={handleClose}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                {/* <Grid item xs={12} sm={12} md={3}>
          <Paper
            sx={{ display: 'flex', minWidth: '300px' }}>
            <Select
              name="Expertise"
              menuPlacement="auto"
              sx={{ fontSize: '20px' }}
              options={mainValue.concat(expertiseData)}
              value={expertise}
              onChange={setExpertise}
              isSearchable={true}
              classNamePrefix="select"
              placeholder={<span style={{ display: 'flex' }} >
                Filter by Expertise</span>}
            />
          </Paper>
        </Grid> */}
                <Grid item xs={12} sm={12} md={12}>
                  <Paper
                    sx={{ display: 'flex', minWidth: '300px' }}>
                    <Select
                      menuPlacement="auto"
                      name="Motivation"
                      sx={{ fontSize: '20px' }}
                      menuPortalTarget={document.body}
                      styles={{ menuPortal: (base) => ({ ...base, zIndex: 99999 }), }}
                      //options={motivationOptions}
                      options={element}
                      value={motivation}
                      onChange={(e) => { handleMotivation(e) }}
                      isSearchable={true}
                      classNamePrefix="select"
                      placeholder={<span style={{ display: 'flex' }} >
                        {/* <SearchBtn sx={{ color: 'black !important', fontSize: '30px' }} /> */}
                        Filter by Services</span>}
                    />
                  </Paper>
                </Grid>

                {/* {userD.longitude != '' && userD.lattitude !== '' && <Grid item xs={12} sm={12} md={12} sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
                  <ShopSelectN
                    menuPlacement="auto"
                    name="Motivation"
                    sx={{ fontSize: '20px' }}
                    menuPortalTarget={document.body}
                    styles={{ menuPortal: (base) => ({ ...base, zIndex: 99999 }), }}
                    options={[
                      {
                        label: 'All',
                        value: 'All'
                      },
                      {
                        label: '10 Km',
                        value: 10000
                      },
                      {
                        label: '50 Km',
                        value: 50000
                      },
                      {
                        label: '100 Km',
                        value: 100000
                      },
                      {
                        label: '500 Km',
                        value: 500000
                      },
                      {
                        label: 'AnyWhere',
                        value: 'AnyWhere'
                      }
                    ]}
                    value={distance}
                    onChange={(e) => { handleNear(e) }}
                    isSearchable={true}
                    classNamePrefix="select"
                    placeholder={<span style={{ display: 'flex' }} >
                      Filter by Nearby</span>}
                  />
                </Grid>} */}
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Dialog>
    </>
  );
};

export default React.memo(ShopPage);
