import React, { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Grid from '@mui/material/Grid';
import { styled, useTheme } from '@mui/material/styles';
import ShopPage from './ShopPage';
import ServicePage from './ServicePage';
import { useRecoilState } from 'recoil';
import { globalTabIndexState, tabIndexState } from '../../../store/local';
import NewAppbar from '../NewAppBar';
import ToolbarTabs from '../NewAppBar/ToolbarTabs';
import { Navigate, useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getAllShopCategory } from 'redux/action';
import { ImageButton } from 'components/common';
import MobileToolbar from 'components/MobileToolbar/MobileToolbar';

interface TabPanelProps {
  children?: React.ReactNode;
  index: string;
  value: string;
}

const a11yProps = (index: string) => ({
  id: `simple-tab-${index}`,
  'aria-controls': `simple-tabpanel-${index}`,
});

const TabPanel = React.memo((props: TabPanelProps) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}>
      {value === index && <div>{children}</div>}
    </div>
  );
});


const Wrapper = React.memo(styled('div')({
  color: '#f5f5f5',
  minHeight: '100vh',
  '& .Mui-selected': {
    background: '#d6a354 !important',
    color: '#fff !important',
  },
  '.MuiTab-root': {
    fontSize: '20px',
    fontWeight: 'normal !important',
    letterSpacing: '2px',
  },

  '.Tab_Box': {
    borderBottom: 1,
    borderColor: 'transparent',
    paddingInline: '2rem',
    paddingBlock: '1rem',
    textAlign: 'center',
    '.MuiButtonBase-root-MuiTab-root.Mui-selected': {
      '& .Mui-selected': {
        color: '#d6a354 !important',
      },
    },
  },
  '& .MuiTabs-indicator': {
    display: 'none',
    justifyContent: 'center',
    color: '#C31818',
    backgroundColor: '#C31818',
  },
}));

const NewSearchPage = () => {

  const { id } = useParams()
  const theme = useTheme()
  const [tabIndex, setTabIndex] = useRecoilState(globalTabIndexState);
  const [check, setCheck] = useState(false)
  const [main, setMain] = useState({ label: 'All', value: 'All' })

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setTabIndex(newValue);
  };

  const [element, setElement] = useState<any>([])
  const shopCateData = useSelector((state: any) => state.auth.shopCategoryList)
  const dispatch = useDispatch()
  const history = useNavigate()

  const handleReset = () => {
    const check1 = element.filter((cc: any) => cc.name.trim().toLowerCase().includes('barber'))
    if (check1.length > 0) {
      setMain({
        ...main,
        label: check1[0].name,
        value: check1[0].value,
      })
    }
    setCheck(!check)
  }

  useEffect(() => {

    if (typeof id === 'undefined') {
      history('/global')
    }

  }, [])

  useEffect(() => {
    if (shopCateData.length == 0) {
      dispatch(getAllShopCategory())
    } else {
      setElement(shopCateData.category)
    }
  }, [shopCateData])

  return (
    <>
      <Wrapper className='innerContainer' style={(theme.palette.mode == 'dark') ? { backgroundColor: '#242424' } : { backgroundColor: 'white' }}>
        {/* <NewAppbar /> */}
        <Box sx={{ width: '100%' }}>
          <Box className="Tab_Box">
            <Tabs className="insideTab"
              value={tabIndex}
              onChange={handleChange}
              aria-label="Mentor-Topic Tabs">
              <Tab
                className="title_head"
                label={check == true ? main.label : element.filter((cc: any) => cc._id == id)[0]?.name}
                value="1"
                sx={{ typography: 'h4' }}
                {...a11yProps('1')}
              />
              <Tab
                className="title_head"
                label="Services"
                value="2"
                sx={{ typography: 'h4' }}
                {...a11yProps('2')}
              />
              {check == false && <ImageButton className="tab-btn-reset" onClick={() => { handleReset() }}>Reset</ImageButton>}
            </Tabs>
          </Box>
          <TabPanel index="1" value={tabIndex}>
            <Box className='mobile_padding' sx={{ padding: '0px 32px 32px 32px' }}>
              <ShopPage handleBtn={handleReset} view={check} mainId={main.value} />
            </Box>
          </TabPanel>
          <TabPanel index="2" value={tabIndex}>
            <ServicePage />
          </TabPanel>
        </Box>
        {/* <Grid
          item
          sx={{
            display: { xs: "block", md: "none", sm: "block" },
            position: 'fixed',
            bottom: 0,
            padding: '10px 10px 0px 10px',
            width: '100%',
          }}
        >
          <MobileToolbar />
        </Grid> */}
        
      </Wrapper>
    </>
  );
};

export default React.memo(NewSearchPage);
