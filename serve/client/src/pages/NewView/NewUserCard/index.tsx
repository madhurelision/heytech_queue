import { Paper, styled, Grid, IconButton } from '@mui/material';
import BookmarksRoundedIcon from '@mui/icons-material/BookmarksRounded';
import React, { useEffect, useState } from 'react';
import { commaString, range } from 'utils/helper';
import { MentorSchemaType, DayEnumType } from 'types';
import { Link } from 'components/common';
import { Favorite, Star } from '@mui/icons-material';
import { red, yellow } from '@mui/material/colors';
import { dayOfWeek } from 'config/KeyData';


// const Wrapper = React.memo(styled(Paper)`
//   //height: 400px;
//   width: 100%;
//   position: relative;
//   border-radius: 16px;
//   //margin: 1rem;
//   cursor: pointer;
// `);

const Wrapper = styled('div')`
  min-height: 400px;
  cursor: pointer;
  width: 100%;
  background-color: #B21515;
  color: #f5f5f5;
  cursor:pointer;
  background-position: center;
  background-size: cover;
  position: relative;
  border-radius: 16px;
  transition: all 0.8s cubic-bezier(0.32, 1.32, 0.42, 0.68);
  &:hover {
    background-color: #d6a354;
    transform: scale(1.05);
  }
  `

const StyledImage = React.memo(styled('img')`
  //display: flex;
  width: 100%;
  //height: 100%;
  //object-fit: contain;
  object-position: center center;
  background-color: #ccc;
  border-radius: 16px;
`);

const StyledDiv = React.memo(styled('div')
  `
display: flex;
background-color: #ccc;
overflow: hidden;
position: relative;
max-width: 100%;
margin: 20px auto;
border-radius: 16px;
.image {
  //position: absolute;
  width: 100%;
  height: 100%;
  opacity: 1;

  
  &.thumb {
    opacity: 1;
    filter: blur(10px);
    transition: opacity 1s ease-out;
    position: absolute;
    &.isLoaded {
      opacity: 0;
    }
  }

  &.isLoaded {
    transition: opacity 1s ease-out;
    opacity: 1;
  }

}
`)

// const AbsoluteGrid = React.memo(styled(Grid)`
//   padding: 5px;
//   background: rgb(0, 0, 0, 0.4);
//   border-radius: 16px;
//   position: absolute;
//   flex-direction: column-reverse;
//   width: 100%;
//   height: 100%;
//   top: 0px;
//   left: 0px;

//   .MuiSvgIcon-root {
//     padding-right: 4px;
//   }

//   .MuiGrid-item {
//     padding: 4px;
//     // display: flex;
//     // align-content: center;
//   }

//   .UserCard_text {
//     font-size: 24px;
//     font-weight: 700;
//     padding: 8px px;
//   }
//   .UserCard_topics {
//     text-overflow: ellipsis;
//   }
//   .UserCard_banner{
//     position: absolute;
//     top: 0px;
//     right: 0px;
//    color:'#ffff';
//     border-top-right-radius: 7px;
//     box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
//   }
// `);

const AbsoluteGrid = React.memo(styled(Grid)(({ theme }) => ({
  padding: '5px',
  background: 'rgb(0, 0, 0, 0.4)',
  borderRadius: '16px',
  position: 'absolute',
  flexDirection: 'column-reverse',
  width: '100%',
  height: '100%',
  top: '0px',
  left: '0px',
  '.MuiSvgIcon-root': {
    paddingRight: '4px',
  },
  '.MuiGrid-item': {
    padding: '4px',
    display: 'flex',
    alignContent: 'center',
  },
  '.UserCard_text': {
    fontSize: '24px',
    fontWeight: '700',
    padding: '8px px',
    [theme.breakpoints.up('xs')]: {
      fontSize: '22px',
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '22px',
    },
  },
  '.UserCard_topics': {
    textOverflow: 'ellipsis',
  },
  '.UserCard_banner': {
    position: 'absolute',
    top: '5px',
    right: '5px',
    borderTopRightRadius: '7px',
    boxShadow: '0 2px 6px rgba(0, 0, 0, 0.3)',
  },
  '.UserCard_banner.coming': {
    background: '#d91010',
  },
})))

const NewUserCard = ({ user, value }: { user: Partial<MentorSchemaType>, value: any[] }) => {

  // const [isLoaded, setIsLoaded] = useState(false);

  // const handleOnLoad = () => {
  //   setIsLoaded(true);
  // };

  const {
    first_name,
    last_name,
    shop_name,
    company,
    job_title,
    expertise,
    image_link,
    _id,
    like,
    totalShop,
    value: element,
    open,
    time_slot
  } = user;
  const name = (shop_name == null || shop_name == undefined) ? `${first_name} ${last_name}` : shop_name;
  // @ts-ignore
  const banner: any[] = element
  const dayName: DayEnumType = dayOfWeek[new Date().getDay()];
  //@ts-ignore
  const { start_hour, end_hour, available } = time_slot[dayName];
  const limit = available == true ? range(start_hour, end_hour, 1) : []
  return (
    <Link to={`/user/${_id}`}>
      <Wrapper
        style={{
          backgroundImage: `url(${image_link})`,
        }}
      >
        <AbsoluteGrid container>
          {/* <Grid item sx={{ display: 'flex' }}>
            <div>
              <BookmarksRoundedIcon />
              <span className="UserCard_topics">{commaString(expertise)}</span>
            </div>
            <div style={{ display: 'flex' }}>
              <div><Favorite fontSize="medium" sx={{ color: red[500] }} />{(like == null || like == undefined || like.length == 0) ? 0 : like.length}</div>
              <div><Star fontSize="medium" sx={{ color: yellow[500] }} />{totalShop}</div>
            </div>
          </Grid> */}
          {/* <Grid item>
            <div style={{ display: 'flex' }}>
              <div><Favorite fontSize="medium" sx={{ color: red[500] }} />{(like == null || like == undefined || like.length == 0) ? 0 : like.length}</div>
              <div><Star fontSize="medium" sx={{ color: yellow[500] }} />{totalShop}</div>
            </div>
          </Grid> */}
          <Grid item sx={{ justifyContent: 'space-between' }}>
            <div>
              <BookmarksRoundedIcon />
              <span className="UserCard_topics">{commaString(expertise)}</span>
            </div>
            <div style={{ display: 'flex' }}>
              <div style={{ display: 'flex' }}><Favorite fontSize="medium" sx={{ color: red[500] }} />{(like == null || like == undefined || like.length == 0) ? 0 : like.length}</div>
              <div style={{ display: 'flex' }}><Star fontSize="medium" sx={{ color: yellow[500] }} />{totalShop}</div>
            </div>
          </Grid>
          {/* <Grid item>
            <WorkRoundedIcon />
            <span>{job_title}</span>
          </Grid> */}
          {/* <Grid item sx={{ display: 'flex' }}>
            <div><Favorite fontSize="medium" sx={{ color: red[500] }} />{(like == null || like == undefined || like.length == 0) ? 0 : like.length}</div>
            <div><Star fontSize="medium" sx={{ color: yellow[500] }} />{totalShop}</div>
          </Grid> */}
          <Grid item>{company}</Grid>
          <Grid item className="UserCard_text">
            {name} (<span className={`${(!open || open == null || open == undefined) ? 'open-btn-red' : (open == true && limit.includes(new Date().getHours()) == true) ? 'open-btn-green' : 'open-btn-red'}`} >{(!open || open == null || open == undefined) ? 'close' : (open == true && limit.includes(new Date().getHours()) == true) ? 'open' : 'close'}</span>)
          </Grid>
          {banner.length > 0 && <Grid item className={`UserCard_banner ${banner[0]?.name?.toLowerCase()}`} >

            {banner[0]?.name}
          </Grid>}
        </AbsoluteGrid>
      </Wrapper>
    </Link>
  );
};

export default React.memo(NewUserCard);
