import React, { useState, useEffect } from 'react';
import { Grid, Typography, useMediaQuery, useTheme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import ToolbarTabs from 'components/Appbar/ToolbarTabs';
import { getAllAboutPage } from 'redux/action';
import MobileLoader from 'pages/Shop/UsableComponent/MobileLoader';
import { useDispatch, useSelector } from 'react-redux';
import MobileToolbar from 'components/MobileToolbar/MobileToolbar';
import './about.css'

const styles = makeStyles((theme) => ({
  title: {
    paddingBlock: '15px',
    fontSize: '2.5rem'
  },
  body: {
    color: theme.palette.mode == 'dark' ? 'white' : '#242424'
  },
  body_desc: {
    fontFamily: 'gamja',
  }
}))

const AboutPage = () => {

  const theme = useTheme()
  const matches = useMediaQuery(theme.breakpoints.up('md'));
  const classes = styles(theme);
  const [data, setData] = useState({})
  const [flag, setFlag] = useState(false)
  const aboutData = useSelector((state) => state.auth.aboutPageList)
  const dispatch = useDispatch()

  useEffect(() => {

    if (Object.keys(aboutData).length == 0) {
      dispatch(getAllAboutPage())
    } else {
      setData(aboutData.about)
      setFlag(true)
    }

  }, [aboutData])

  return (
    <Grid className='aboutWrapper' container justifyContent="center"
      sx={(theme) => ({
        color: 'white',
        [theme.breakpoints.up('md')]: {
          paddingInline: '200px',
        },
        [theme.breakpoints.up('sm')]: {
          paddingInline: '30px'
        },
        [theme.breakpoints.up('xs')]: {
          paddingInline: '30px'
        }
      })}
    >
      {flag ? (Object.keys(data).length > 0) ? <Grid item xs={12} md={12} sm={12} className={`${classes.body} ${matches ? "content-body-view" : 'content-body-view-mobile'}`}>

        <div className="flexBOx">
          <div className='left'>
            <Typography variant="h1" component="h4" className={classes.title + ' title_head'}>
              {data?.title}
            </Typography>
            <Typography variant='body' className='title_font leftPannel' sx={(theme) => ({
              [theme.breakpoints.up('md')]: {
                lineHeight: '2.5rem'
              },
              [theme.breakpoints.up('sm')]: {
                lineHeight: 1
              },
              [theme.breakpoints.up('xs')]: {
                lineHeight: 1
              }

            })}>
              {data?.description}
            </Typography>
          </div>

        </div>

      </Grid> : <div></div> : <Grid item xs={12} md={12} sm={12} className={classes.body}> <MobileLoader /> </Grid>}
    </Grid>
  )
}

export default AboutPage