import React from 'react';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import { useSelector } from 'react-redux';
import NewAppbar from '../NewAppBar';
import AboutPage from './AboutPage'
import Appbar from 'components/Appbar';

const PageOneWrapper = styled('div')({
  backgroundColor: 'transparent',
  height: '100%',
  width: '100%',
  overflow: 'hidden',
  position: 'relative',
});

const About = () => {

  return (
    <PageOneWrapper className='aboutMain'>
      <Grid container direction="column" wrap="nowrap" sx={{ height: '100%', overflowX: 'auto' }}>
        {/* <Grid item sx={{ zIndex: 1 }}>
          <Appbar />
        </Grid> */}
        <AboutPage />
      </Grid>
    </PageOneWrapper>
  )
};

export default About;
