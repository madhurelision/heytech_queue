import React, { useState, useEffect } from 'react'
import Item from './Item'
import Carousel from "react-elastic-carousel";
import { useRecoilState } from 'recoil';
import { categoryState } from 'store';
import { useDispatch, useSelector } from 'react-redux';
import { getAllShopCategory } from 'redux/action';
import Loader from 'react-loader-spinner';
import { Grid, InputBase, Paper, Box } from '@mui/material';
import { styled } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom'
import './carousel.css'

const StyledBox = React.memo(styled(Box)({
  width: '100%',
  height: '15vh',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',

  '& svg': {
    fill: 'lightgray',
  },
}));

const breakPoints = [
  { width: 1, itemsToShow: 1, itemsToScroll: 1 },
  { width: 550, itemsToShow: 1 },
  { width: 768, itemsToShow: 3 },
  { width: 1200, itemsToShow: 4 },
];

const CarouselItem = () => {

  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const [category, setCategory] = useRecoilState(categoryState);
  const shopCateData = useSelector((state) => state.auth.shopCategoryList)
  const dispatch = useDispatch()
  const history = useNavigate()

  useEffect(() => {
    if (shopCateData.length == 0) {
      dispatch(getAllShopCategory())
    } else {
      setElement(shopCateData.category)
      setFlag(true)
    }
  }, [shopCateData])

  const handleCategory = (val) => {
    setCategory({
      label: val.name,
      value: val._id
    })
    history('/global/search/' + val._id)
  }

  return (
    <>
      {flag ? element.length > 0 ? <Carousel className="category-carousel" enableSwipe={true} enableMouseSwipe={true} breakPoints={breakPoints}>
        {element.map((cc) => {
          return (
            <Item key={cc._id} className="card-item" style={{
              backgroundImage: `url(${cc.image})`,
            }} onClick={() => { handleCategory(cc) }} >{cc.name}({cc.count})</Item>
          )
        })}
      </Carousel> : <></> : <StyledBox>
        <Loader type="ThreeDots" color="#d6a354" height={80} width={80} />
      </StyledBox>}
    </>
  )
}

export default CarouselItem