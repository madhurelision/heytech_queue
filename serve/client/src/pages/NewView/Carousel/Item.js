import styled from "styled-components";

export default styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  text-align:center;
  //height: 250px;
  //width: 100%;
  background-color: #B21515;
  color: #f5f5f5;
  //margin: 0 15px;
  //font-size: 4em;
  cursor:pointer;
  background-position: center;
  background-size: cover;
  transition: all 0.8s cubic-bezier(0.32, 1.32, 0.42, 0.68);
  &:hover {
    background-color: #d6a354;
    transform: scale(1.05);
  }
  border-radius: 16px;
  font-family:fredoka;
`;