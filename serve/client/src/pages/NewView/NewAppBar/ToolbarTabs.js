import React from 'react';
import { Grid, Typography } from '@mui/material';
import { useLocation, useNavigate } from 'react-router-dom';
import { makeStyles } from '@mui/styles'

const style = makeStyles(() => ({
  grid: {
    color: '#ffff',
    cursor: 'pointer'
  },
  selectGrid: {
    color: '#d6a354',
    cursor: 'pointer'
  }
}))

const ToolbarTabs = () => {

  const history = useNavigate()
  const classes = style()
  const location = useLocation()

  return (
    <Grid container sx={{ color: 'white', justifyContent: 'center' }}>
      <Grid item xs={4} sm={4} md={4} className={(location.pathname == "/about") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/about')
      }}>
        <Typography className={classes.font}>About</Typography>
      </Grid>
      <Grid item xs={4} sm={4} md={4} className={(location.pathname == "/contact") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/contact')
      }}>
        <Typography className={classes.font}>Contact Us</Typography>
      </Grid>
    </Grid>
  )
}

export default ToolbarTabs