import React, { useState, useEffect } from "react";
import { Dialog, DialogTitle, IconButton, Box, Stack, Toolbar as MaterialToolbar } from "@mui/material";
import { Link, StyledButton as Button, ImageButton } from "components/common";
import Login from "pages/Login/Login";
import { Close, Login as LoginBtn, AccountCircle } from "@mui/icons-material";
import CustomerLogin from "pages/Login/CustomerLogin";
import { getAllAdminPage } from "redux/action";
import { useDispatch, useSelector } from "react-redux";

const Toolbar = () => {
  const [open, setOpen] = useState(false);
  const [view, setView] = useState(false);
  const [show, setShow] = useState(false);
  const handleClose = () => {
    setOpen(false);
    setShow(false);
  };

  const handleOpen = (check: any) => {
    setOpen(true);
    setView(check)
  };

  // @ts-ignore
  const [data, setData] = useState<any>({})
  const adminD = useSelector((state: any) => state.auth.adminPageList)
  const dispatch = useDispatch()

  useEffect(() => {

    if (Object.keys(adminD).length == 0) {
      dispatch(getAllAdminPage())
    } else {
      setData(adminD.admin)
    }

  }, [adminD])


  const handleShow = (val: any) => {
    setShow(val)
  }


  return (
    <div>
      <MaterialToolbar>
        <Stack direction="row" spacing={3} style={{ width: "100%" }}>
          <Box sx={{ flexGrow: 1 }}>
            <Link to="/">
              <ImageButton className="title_head">
                {(data.name !== null || data.name !== undefined) ? data.name : "HeyBarber"}
              </ImageButton>
            </Link>
          </Box>
          <Stack
            direction="row"
            spacing={3}
            sx={{ display: { xs: "none", md: "block", sm: "none" } }}
          >

            <ImageButton
              className="title_head"
              onClick={() => {
                handleOpen(true);
              }}
            >
              Shop
            </ImageButton>
            <ImageButton
              className="title_head"
              onClick={() => {
                handleOpen(false);
              }}
            >
              Customer
            </ImageButton>
            <Link to="/search">
              <ImageButton className="title_head">Find</ImageButton>
            </Link>
            {/* <IconButton title="Login">
              <AccountCircle fontSize="large" sx={{ color: '#fff !important' }} />
            </IconButton> */}
          </Stack>
          <Stack
            direction="row"
            // spacing={3}
            sx={{ display: { xs: "block", md: "none", sm: "block" } }}
          >
            {/* <ImageButton
              onClick={() => {
                handleOpen(true);
              }}
            >
              Shop Login
            </ImageButton>
            <ImageButton
              onClick={() => {
                handleOpen(false);
              }}
            >
              Customer Login
            </ImageButton> */}

            <IconButton title="Login" onClick={() => {
              handleOpen(true);
            }}>
              <AccountCircle fontSize="large" sx={{ color: '#fff !important' }} />
            </IconButton>
            {/* <IconButton title="Customer Login" onClick={() => {
              handleOpen(false);
            }}>
              <LoginBtn sx={{ color: '#C31818 !important' }} />
            </IconButton> */}
          </Stack>
        </Stack>
      </MaterialToolbar>
      <Dialog
        open={open}
        onClose={() => {
          if (show == true) {

          } else { handleClose() }
        }}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          <Box display="flex" alignItems="center">
            <Box>
              <IconButton onClick={handleClose}>
                <Close />
              </IconButton>
            </Box>
          </Box>
        </DialogTitle>
        {view == true && <Login handleClose={handleClose} handleView={handleShow} />}
        {view == false && <CustomerLogin handleClose={handleClose} handleView={handleShow} />}
      </Dialog>
    </div>
  );
};

export default Toolbar;
