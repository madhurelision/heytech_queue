import { Grid } from '@mui/material';
import React, { useState, useEffect } from 'react';
import { styled, Typography } from '@mui/material';
import { categoryState, serviceState, topicsState } from '../../../store/local';
import { useRecoilState } from 'recoil';
import { NewReactSelect } from '../../../components/common/index';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useDispatch, useSelector } from 'react-redux'
import { getAllShopCategory } from '../../../redux/action/index'
import { useNavigate } from 'react-router-dom';
import { Search as SearchBtn } from '@mui/icons-material'

const StyledGrid = React.memo(styled(Grid)({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-around',
  width: '100%',
}));


var mainValue = [{
  value: 'All',
  label: 'All',
}]

const Seach = () => {

  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('md'));
  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const [category, setCategory] = useRecoilState(categoryState);
  const [motivation, setMotivation] = useRecoilState(serviceState);
  const [topic, setTopic] = useRecoilState(topicsState);
  const shopCateData = useSelector((state) => state.auth.shopCategoryList)
  const serviceData = useSelector((state) => state.auth.newActiveServiceList)
  const shopData = useSelector((state) => state.auth.shopList)
  const dispatch = useDispatch()
  const history = useNavigate()

  useEffect(() => {
    if (shopCateData.length == 0) {
      dispatch(getAllShopCategory())
    } else {
      // setElement(mainValue.concat(shopCateData.category.map((cc) => {
      //   return {
      //     label: cc.name,
      //     value: cc._id,
      //     type: 'category',
      //   }
      // })).concat(shopData.map((dd) => {
      //   return {
      //     label: dd.shop_name,
      //     value: dd.type,
      //     type: 'shop'
      //   }
      // })).concat(serviceData.map((ss) => {
      //   return {
      //     label: ss.name,
      //     value: ss.type,
      //     type: 'service'
      //   }
      // })))
      setElement([
        {
          label: 'All',
          options: mainValue
        },
        {
          label: 'Shop',
          options: shopData.map((dd) => {
            return {
              label: dd.shop_name,
              value: dd.type,
              type: 'shop'
            }
          })
        },
        {
          label: 'Service',
          options: serviceData.map((ss) => {
            return {
              label: ss.name,
              value: ss.type,
              type: 'service'
            }
          })
        },
        {
          label: 'Category',
          options: shopCateData.category.map((cc) => {
            return {
              label: cc.name,
              value: cc._id,
              type: 'category',
            }
          })
        },
      ])
      setFlag(true)
    }
  }, [shopCateData])

  const handleCategory = (val) => {
    if (val.label == 'All') {
      setCategory(null)
      setTopic([])
      setMotivation(null)
      return
    }
    setTopic([])
    setMotivation(null)
    setCategory(val)
    history('/global/search/' + val.value)
  }

  return (
    <>
      <Grid container item justifyContent="center" sx={{ display: { xs: 'none', md: 'flex', sm: 'none' } }}>
        <StyledGrid item xs={12} sm={12} md={6} >
          <NewReactSelect
            menuPlacement="auto"
            name="ShopCategory"
            sx={{ fontSize: '20px', padding: '8px 8px', color: theme.palette.mode == 'dark' ? 'white' : 'black' }}
            options={element}
            value={category}
            onChange={handleCategory}
            isSearchable={matches}
            classNamePrefix="select"
            placeholder={<span><SearchBtn sx={{ color: 'black !important', fontSize: '45px' }} /></span>}
          />
        </StyledGrid>
      </Grid>
      <Grid container item justifyContent="center" sx={{ display: { xs: 'flex', md: 'none', sm: 'flex' } }}>
        <Grid className="globalSearchContent" item xs={12} sm={12} md={6} >
          <Typography className="title_head" sx={{ color: theme.palette.mode == 'dark' ? 'white' : 'black' }}>Search Your Shop</Typography>
          <NewReactSelect
            menuPlacement="auto"
            name="ShopCategory"
            sx={{ fontSize: '20px', color: theme.palette.mode == 'dark' ? 'white' : 'black' }}
            options={element}
            value={category}
            onChange={handleCategory}
            isSearchable={matches}
            classNamePrefix="select"
            placeholder={<span><SearchBtn sx={{ color: 'black !important', fontSize: '45px' }} /></span>}
          />
        </Grid>
      </Grid>
    </>
  );
};

export default React.memo(Seach);
