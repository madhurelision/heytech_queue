import React from 'react';
import Appbar from '../../../components/Appbar/index';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import { useSelector } from 'react-redux';
import RefreshPage from 'pages/RefreshPage/RefreshPage';
import HomePage from './HomePage';
import NewAppbar from '../NewAppBar';
import CarouselItem from '../Carousel';

const PageOneWrapper = styled('div')({
  backgroundColor: 'transparent',
  height: '100%',
  width: '100%',
  overflow: 'hidden',
  position: 'relative',
});

const NewHome = () => {

  const checkService = useSelector((state) => state.auth);

  return (
    <>
      <PageOneWrapper className='mainWrapper'>
        <Grid container direction="column" wrap="nowrap" sx={{ height: '100%', overflowX: 'auto' }}>
          {/* <Grid item sx={{ zIndex: 1 }}>
            <NewAppbar />
          </Grid> */}
          <HomePage />
          {/* <Grid item sx={{ display: 'flex', flexGrow: 1 }}> */}
          {/* {(checkService.serviceRequestFailed == false || checkService.expertiseRequestFailed == false) ? <Hero /> : <RefreshPage />} */}
          {/* <HomePage />
          </Grid> */}
          {/* <Grid item sx={{ display: 'flex', flexGrow: 1 }}>
            <CarouselItem />
          </Grid> */}
        </Grid>
      </PageOneWrapper>
    </>
  )
};

export default NewHome;
