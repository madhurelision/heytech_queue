import React from 'react';
import { Grid, Typography, styled } from '@mui/material';
import Searchbox from './Search';
import ToolbarTabs from '../NewAppBar/ToolbarTabs';
import CarouselItem from '../Carousel';
import ServiceOffer from '../ServiceOffer/ServiceOffer';
import MobileToolbar from 'components/MobileToolbar/MobileToolbar';

const StyledGrid = React.memo(styled(Grid)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignContent: 'center',
  color: theme.palette.mode == 'dark' ? '#f5f5f5' : 'black',
  paddingLeft: '2rem',

  '& .MuiTypography-root': {
    diplay: 'inline-flex',
    [theme.breakpoints.up('xs')]: {
      fontSize: '9vw',
      flexWrap: 'nowrap',
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '10vw',
      flexWrap: 'nowrap',
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '9vw',
      flexWrap: 'nowrap',
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '8vw',
      flexWrap: 'nowrap',
    },
    fontFamily: 'inter',
    fontWeight: '800',
    opacity: '0.9',
  },
})));

const HomePage = () => (
  <Grid container>
    {/* <StyledGrid
      item
      sm={12}
      lg={12}
      md={12}
      xs={12}
      style={{ width: '100%' }}>
      <Typography>Search.</Typography>
      <Typography>Your.</Typography>
      <Typography>Shop.</Typography>
    </StyledGrid> */}
    <StyledGrid
      className="globalSearchContent"
      item
      sm={12}
      lg={12}
      md={12}
      xs={12}
      sx={{ display: { xs: 'none', md: 'block', lg: 'block', sm: 'none' } }}
      style={{ textAlign: 'center' }}>
      <Typography className="title_head_home">Search Your Shop</Typography>
    </StyledGrid>

    <Grid
      item
      sm={12}
      lg={12}
      md={12}
      xs={12}
      sx={{ display: { xs: 'none', md: 'block', lg: 'block', sm: 'none' } }}
    >
      <Searchbox />
    </Grid>

    <Grid
      item
      sm={12}
      lg={12}
      md={12}
      xs={12}
      sx={{ padding: '20px', display: { xs: 'flex', md: 'none', lg: 'none', sm: 'flex' } }}>
      {/* <Typography variant="h5" component="h1" sx={{ color: 'white' }}>Search Your Shop</Typography> */}
      <Searchbox />
    </Grid>

    <Grid className="searchSlider"
      item
      sm={12}
      lg={12}
      md={12}
      xs={12}>
      <CarouselItem />
    </Grid>

    <Grid
      item
      sm={12}
      lg={12}
      md={12}
      xs={12}>
      <ServiceOffer />
    </Grid>
  </Grid>
);

export default React.memo(HomePage);
