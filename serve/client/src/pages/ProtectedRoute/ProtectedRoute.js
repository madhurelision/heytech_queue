import React, { useEffect } from 'react'
import { Navigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux'
import { shopProfileData, customerProfileData, employeeProfileData } from '../../redux/action/index'

const ProtectedRoute = ({ children }) => {

  const dispatch = useDispatch()
  const user = useSelector(state => state.auth.isAuthenticated)

  useEffect(() => {

    if (localStorage.getItem('auth-token') && user == false && localStorage.getItem('type') == 'shop') {
      dispatch(shopProfileData())
    } else if (localStorage.getItem('auth-token') && user == false && localStorage.getItem('type') == 'customer') {
      dispatch(customerProfileData())
    } else if (localStorage.getItem('auth-token') && user == false && localStorage.getItem('type') == 'employee') {
      dispatch(employeeProfileData())
    }

  }, [user])

  return (localStorage.getItem('auth-token') && localStorage.getItem('type') == 'shop') ? children : (localStorage.getItem('auth-token') && localStorage.getItem('type') == 'customer') ? children : (localStorage.getItem('auth-token') && localStorage.getItem('type') == 'employee') ? children : (!localStorage.getItem('auth-token') && user == false && !localStorage.getItem('type')) ? <Navigate to="/" /> : <Navigate to="/" />
}

export default ProtectedRoute
