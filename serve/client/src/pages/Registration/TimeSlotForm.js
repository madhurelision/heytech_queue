import React, { useEffect, useState, useCallback } from 'react'
import { TextField, Grid, Card, Typography, Checkbox, FormControlLabel, CardContent } from '@mui/material'
import './register.css'
import FormError from './FormError';
import { useNavigate, useParams } from 'react-router';
import { tableStyles } from 'pages/Shop/Dashboard/styles';
import { BookingButton } from "components/common";
import { useDispatch, useSelector } from 'react-redux';
import { shopProfileTimeSlot } from 'redux/action';

const TimeSlotForm = ({ handleNext }) => {

  const mainCheckU = useSelector((state) => state.auth.created)
  const [err, setErr] = useState({})
  const { id } = useParams();
  const dispatch = useDispatch()


  useEffect(() => {

    if (mainCheckU) {
      handleNext()
    }
  }, [mainCheckU])

  const [data, setData] = useState({
    time_slot: {
      monday: {
        start_hour: 9,
        end_hour: 21,
        available: true
      },
      tuesday: {
        start_hour: 9,
        end_hour: 21,
        available: true
      },
      wednesday: {
        start_hour: 9,
        end_hour: 21,
        available: true
      },
      thursday: {
        start_hour: 9,
        end_hour: 21,
        available: true
      },
      friday: {
        start_hour: 9,
        end_hour: 21,
        available: true
      },
      saturday: {
        start_hour: 9,
        end_hour: 21,
        available: true
      },
      sunday: {
        start_hour: 9,
        end_hour: 21,
        available: true
      },
    }
  })

  const handleStartData = (e) => {
    console.log('Data, ', e.target.name, 'value', e.target.value)
    setData({ ...data, time_slot: { ...data.time_slot, [e.target.id]: { ...data.time_slot[e.target.id], [e.target.name]: e.target.value } } })
  }

  const handleEndData = (e) => {
    console.log('Data, ', e.target.name, 'value', e.target.value)
    setData({ ...data, time_slot: { ...data.time_slot, [e.target.id]: { ...data.time_slot[e.target.id], [e.target.name]: e.target.value } } })
  }

  const handleCheck = (e) => {
    if (e.target.checked) {
      setData({ ...data, time_slot: { ...data.time_slot, [e.target.name]: { ...data.time_slot[e.target.name], available: e.target.checked } } })
      setErr({ ...err, time_slot: '' })
    } else {
      setData({ ...data, time_slot: { ...data.time_slot, [e.target.name]: { ...data.time_slot[e.target.name], available: e.target.checked } } })
      setErr({ ...err, time_slot: '' })
    }
  }


  const handleSubmit = () => {
    var check = timeSlotValidation(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(shopProfileTimeSlot(id, {
      time_slot: JSON.stringify(data.time_slot),
      time_zone: new Date().getTimezoneOffset(),
      time_val: window.Intl.DateTimeFormat().resolvedOptions().timeZone
    }))
  }

  const classes = tableStyles()

  return (
    <Grid sx={{ paddingBlock: '15px' }}>
      <Card >
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12}>
                <Typography variant="h5" component="div" align="center">
                  Step 3: Add TimeSlot
                </Typography>
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <Typography variant="h6" component="h6" align="left">
                  Time Slot (24 Hour Format)
                </Typography>
              </Grid>
              <Grid item xs={4} sm={4} md={2}>
                <FormControlLabel name="monday" control={<Checkbox checked={data.time_slot.monday.available} />} label="Monday" onChange={handleCheck} />
              </Grid>
              <Grid item xs={4} sm={4} md={5}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.monday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.monday.start_hour}
                  label="Start Time"
                  id="monday"
                  name="start_hour"
                  onChange={handleStartData}
                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={5}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.monday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.monday.end_hour}
                  label="End Time"
                  id="monday"
                  name="end_hour"
                  onChange={handleEndData}
                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={2}>
                <FormControlLabel name="tuesday" control={<Checkbox checked={data.time_slot.tuesday.available} />} label="Tuesday" onChange={handleCheck} />
              </Grid>
              <Grid item xs={4} sm={4} md={5}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.tuesday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.tuesday.start_hour}
                  label="Start Time"
                  id="tuesday"
                  name="start_hour"
                  onChange={handleStartData}
                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={5}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.tuesday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.tuesday.end_hour}
                  label="End Time"
                  id="tuesday"
                  name="end_hour"
                  onChange={handleEndData}
                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={2}>
                <FormControlLabel name="wednesday" control={<Checkbox checked={data.time_slot.wednesday.available} />} label="Wednesday" onChange={handleCheck} />
              </Grid>
              <Grid item xs={4} sm={4} md={5}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.wednesday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.wednesday.start_hour}
                  label="Start Time"
                  id="wednesday"
                  name="start_hour"
                  onChange={handleStartData}
                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={5}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.wednesday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.wednesday.end_hour}
                  label="End Time"
                  id="wednesday"
                  name="end_hour"
                  onChange={handleEndData}
                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={2}>
                <FormControlLabel name="thursday" control={<Checkbox checked={data.time_slot.thursday.available} />} label="Thursday" onChange={handleCheck} />
              </Grid>
              <Grid item xs={4} sm={4} md={5}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.thursday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.thursday.start_hour}
                  label="Start Time"
                  id="thursday"
                  name="start_hour"
                  onChange={handleStartData}
                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={5}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.thursday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.thursday.end_hour}
                  label="End Time"
                  id="thursday"
                  name="end_hour"
                  onChange={handleEndData}
                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={2}>
                <FormControlLabel name="friday" control={<Checkbox checked={data.time_slot.friday.available} />} label="Friday" onChange={handleCheck} />
              </Grid>
              <Grid item xs={4} sm={4} md={5}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.friday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.friday.start_hour}
                  label="Start Time"
                  id="friday"
                  name="start_hour"
                  onChange={handleStartData}
                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={5}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.friday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.friday.end_hour}
                  label="End Time"
                  id="friday"
                  name="end_hour"
                  onChange={handleEndData}
                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={2}>
                <FormControlLabel name="saturday" control={<Checkbox checked={data.time_slot.saturday.available} />} label="Saturday" onChange={handleCheck} />
              </Grid>
              <Grid item xs={4} sm={4} md={5}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.saturday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.saturday.start_hour}
                  label="Start Time"
                  id="saturday"
                  name="start_hour"
                  onChange={handleStartData}
                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={5}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.saturday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.saturday.end_hour}
                  label="End Time"
                  id="saturday"
                  name="end_hour"
                  onChange={handleEndData}
                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={2}>
                <FormControlLabel name="sunday" control={<Checkbox checked={data.time_slot.sunday.available} />} label="Sunday" onChange={handleCheck} />
              </Grid>
              <Grid item xs={4} sm={4} md={5}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.sunday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.sunday.start_hour}
                  label="Start Time"
                  id="sunday"
                  name="start_hour"
                  onChange={handleStartData}
                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={5}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.sunday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.sunday.end_hour}
                  label="End Time"
                  id="sunday"
                  name="end_hour"
                  onChange={handleEndData}
                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              {err.time_slot && (<FormError data={err.time_slot}></FormError>)}
            </Grid>
            <Grid id="submitBtn" container spacing={2}>
              <Grid item xs={12} align="right">
                <BookingButton type="button" variant="contained" onClick={() => { handleSubmit() }} >Next</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default TimeSlotForm


const timeSlotValidation = (data) => {

  let errors = {}

  if (data.time_slot.monday.available == false && data.time_slot.tuesday.available == false && data.time_slot.wednesday.available == false && data.time_slot.thursday.available == false && data.time_slot.friday.available == false && data.time_slot.saturday.available == false && data.time_slot.sunday.available == false) {
    errors['time_slot'] = "Minimum one time slot is Required";
  }

  return errors
}