import React, { useEffect, useState } from 'react'
import { TextField, Grid, Card, Typography, CardContent } from '@mui/material'
import './register.css'
import FormError from './FormError';
import { useNavigate, useParams } from 'react-router';
import { BookingButton } from "components/common";
import { useDispatch, useSelector } from 'react-redux';

const PaymentForm = ({ handleNext }) => {

  const mainCheckU = useSelector((state) => state.auth.created)
  const [data, setData] = useState({
    upi_id: ''
  })
  const [err, setErr] = useState({})
  const { id } = useParams();
  const dispatch = useDispatch();

  useEffect(() => {

    if (mainCheckU) {
      handleNext()
    }
  }, [mainCheckU])

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    var check = paymentValidation(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
  }

  return (
    <Grid sx={{ paddingBlock: '15px' }}>
      <Card >
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12}>
                <Typography variant="h5" component="div" align="center">
                  Step 3: Payment
                </Typography>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  type="text"
                  value={data.upi_id}
                  label="Upi Id"
                  name="upi_id"
                  id="upi_id"
                  onChange={handleChange}
                />
                {err.upi_id && (<FormError data={err.upi_id}></FormError>)}
              </Grid>
            </Grid>
            <Grid id="submitBtn" container spacing={2}>
              <Grid item xs={12} align="right">
                <BookingButton type="button" variant="contained" onClick={() => { handleSubmit() }} >Next</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default PaymentForm


const paymentValidation = (data) => {

  let errors = {}

  if (data.upi_id) {
    errors['upi_id'] = "Upi Id is Required";
  }

  return errors
}