import * as React from 'react';
import { Box, Stepper, Step, StepLabel, Button, Typography, AppBar, Stack, Grid, Toolbar, useTheme, StepConnector, stepConnectorClasses, styled } from '@mui/material';
import { Link, StyledButton as Btn } from "components/common";
import { useNavigate } from 'react-router-dom';
import { tableStyles } from 'pages/Shop/Dashboard/styles';
import ServicePageRegister from 'pages/ServicePage/ServicePageRegister';
import Registration from './Registration'
import CompletedPage from 'pages/ServicePage/CompletePage'
import { Check } from '@mui/icons-material';
import PropTypes from 'prop-types';
import TimeSlotForm from './TimeSlotForm';
import ShopKYC from './ShopKYC';

const QontoConnector = styled(StepConnector)(({ theme }) => ({
  [`&.${stepConnectorClasses.alternativeLabel}`]: {
    top: 10,
    left: 'calc(-50% + 16px)',
    right: 'calc(50% + 16px)',
  },
  [`&.${stepConnectorClasses.active}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      borderColor: '#784af4',
    },
  },
  [`&.${stepConnectorClasses.completed}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      borderColor: '#784af4',
    },
  },
  [`& .${stepConnectorClasses.line}`]: {
    borderColor: theme.palette.mode === 'dark' ? theme.palette.grey[800] : '#eaeaf0',
    borderTopWidth: 3,
    borderRadius: 1,
  },
}));

const QontoStepIconRoot = styled('div')(({ theme, ownerState }) => ({
  color: theme.palette.mode === 'dark' ? theme.palette.grey[700] : '#eaeaf0',
  display: 'flex',
  height: 22,
  alignItems: 'center',
  ...(ownerState.active && {
    color: '#784af4',
  }),
  '& .QontoStepIcon-completedIcon': {
    color: '#784af4',
    zIndex: 1,
    fontSize: 18,
  },
  '& .QontoStepIcon-circle': {
    width: 8,
    height: 8,
    borderRadius: '50%',
    backgroundColor: 'currentColor',
  },
}));

function QontoStepIcon(props) {
  const { active, completed, className } = props;

  return (
    <QontoStepIconRoot ownerState={{ active }} className={className}>
      {completed ? (
        <Check className="QontoStepIcon-completedIcon" />
      ) : (
        <div className="QontoStepIcon-circle" />
      )}
    </QontoStepIconRoot>
  );
}

QontoStepIcon.propTypes = {
  /**
   * Whether this step is active.
   * @default false
   */
  active: PropTypes.bool,
  className: PropTypes.string,
  /**
   * Mark the step as completed. Is passed to child components.
   * @default false
   */
  completed: PropTypes.bool,
};

const steps = ['Profile Registration', 'Add Services', "Add TimeSlot", "Shop Kyc", 'Completed'];

export default function StepperForm() {

  const theme = useTheme()
  const [activeStep, setActiveStep] = React.useState(0);
  const [skipped, setSkipped] = React.useState(new Set());

  const history = useNavigate()
  const classes = tableStyles()

  const isStepSkipped = (step) => {
    return skipped.has(step);
  };

  const handleNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };


  const handleLogButton = () => {
    history('/')
    localStorage.removeItem('registration-token')
  }

  return (
    <Grid style={{ paddingBlock: '10px', paddingInline: '6px' }}>
      <AppBar
        variant="outlined"
        position="relative"
        elevation={0}
        sx={{ padding: '8px', backgroundColor: theme.palette.mode == 'dark' ? '#242424' : 'white' }}>
        <Toolbar>
          <Stack direction="row" spacing={3} style={{ width: "100%" }}>
            <Box sx={{ flexGrow: 1 }}>
              <Link to="/">
                <Btn>{"HeyBarber"} </Btn>
              </Link>
            </Box>
            <Stack
              direction="row"
              spacing={3}
            >
              <Btn onClick={() => { handleLogButton() }} >{"Logout"}</Btn>
            </Stack>
          </Stack>
        </Toolbar>
      </AppBar>
      {/* <Box sx={{ width: '100%', marginTop: '10px' }}>
      </Box> */}

      <Stack sx={{ width: '100%', overflow: 'hidden' }} spacing={4}>
        <Stepper activeStep={activeStep} sx={(theme) => ({
          [theme.breakpoints.up('sm')]: {
            marginInline: '0% !important',
            paddingBlock: '15px'
          },
          [theme.breakpoints.up('xs')]: {
            marginInline: '0% !important',
            paddingBlock: '15px'
          },
          [theme.breakpoints.up('md')]: {
            marginInline: '10% !important',
            paddingBlock: '15px'
          },
        })} connector={<QontoConnector />} >
          {steps.map((label, index) => {
            const stepProps = {};
            const labelProps = {};
            if (isStepSkipped(index)) {
              stepProps.completed = false;
            }
            return (
              <Step key={label} {...stepProps}>
                <StepLabel StepIconComponent={QontoStepIcon} {...labelProps}>{label}</StepLabel>
              </Step>
            );
          })}
        </Stepper>
      </Stack>
      <Button onClick={handleNext}>
        {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
      </Button>
      <Button onClick={handleBack}>
        {'Back'}
      </Button>
      {activeStep == 0 && <Registration handleNext={handleNext} />}
      {activeStep == 1 && <ServicePageRegister handleNext={handleNext} />}
      {activeStep == 2 && <TimeSlotForm handleNext={handleNext} />}
      {activeStep == 3 && <ShopKYC handleNext={handleNext} />}
      {activeStep == 4 && <CompletedPage handleNext={handleNext} />}
    </Grid>
  );
}
