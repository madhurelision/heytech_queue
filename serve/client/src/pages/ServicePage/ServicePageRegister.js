import React, { useEffect, useState } from 'react';
import { AppBar, Autocomplete, Box, Button, Card, CardContent, Dialog, Grid, IconButton, Stack, TextField, Toolbar, Typography, CardMedia, TableBody, TableCell, TableRow, Table, TableHead, Paper, TableContainer, Collapse, DialogTitle, Chip } from '@mui/material';
import FormError from 'pages/Registration/FormError';
import CreateService from 'pages/Shop/Services/CreateService';
import { useNavigate, useParams } from 'react-router-dom';
import { getAllService, serviceAllClone, serviceRegistrationClone, shopProfileService } from 'redux/action';
import { useDispatch, useSelector } from 'react-redux';
import { BookingButton } from "components/common";
import { tableStyles } from 'pages/Shop/Dashboard/styles';
import { AddCircleOutline, Close, Edit, KeyboardArrowDown, KeyboardArrowUp, Cancel, FileCopy } from '@mui/icons-material';
import ImageBox from 'pages/Shop/UsableComponent/ImageBox';
import EditService from 'pages/Shop/Services/EditService';

const ServicePageRegister = ({ handleNext }) => {

  const serviceType = useSelector((state) => state.auth.serviceList)
  const mainCheckU = useSelector((state) => state.auth.updated)
  const checkTest = useSelector((state) => state.auth.created)
  const loading = useSelector(state => state.auth.loader)
  const createdService = useSelector(state => state.auth.complete)
  const serviceD = useSelector(state => state.auth.serviceSelect);
  const cloneService = useSelector(state => state.auth.clone)
  const registrationD = useSelector((state) => state.auth.registrationData)
  const dispatch = useDispatch()
  const history = useNavigate()


  const classes = tableStyles()
  const [element, setElement] = useState([])
  const [ids, setIds] = useState([])
  const [flag, setFlag] = useState(false)
  const [view, setView] = useState(false)
  const [clone, setClone] = useState('')
  const [show, setShow] = useState('')
  const [err, setErr] = useState({})
  const [data, setData] = useState({
    topics: []
  })
  const [value, setValue] = useState({
    open: false,
    name: '',
    image: '',
    description: '',
    motivation: '',
    emojiIcon: '',
    waiting_number: '',
    waiting_key: '',
    waiting_time: '',
    tags: '',
    seat: [],
    price: '',
    _id: ''
  })

  const { id } = useParams();

  useEffect(() => {
    if (serviceType.length == 0) {
      dispatch(getAllService(0, 20))
    } else {
      setElement(serviceType)
      setFlag(true)
    }
  }, [serviceType])

  useEffect(() => {

    if (mainCheckU == true) {
      handleNext()
    }
  }, [mainCheckU])


  useEffect(() => {
    if (checkTest == true) {
      var testArr = [...data.topics];
      if (serviceD.length > 0) {
        testArr.push(serviceD[0])
        console.log('testArr', testArr, 'service Craeted', serviceD, 'clone', clone)
        setData({ ...data, topics: testArr })
        setErr({ ...err, topics: '' })
      }
    }
  }, [checkTest, serviceD])

  useEffect(() => {
    if (cloneService == true) {
      var cloneArr = [...data.topics];
      console.log('mains Ids', ids, 'check', cloneArr)
      cloneArr = cloneArr.filter((cc) => !ids.includes(cc._id))
      if (serviceD.length > 0) {
        cloneArr.push(...serviceD)
        console.log('cloneArr', cloneArr, 'service Craeted', serviceD, 'clone', clone)
        setData({ ...data, topics: cloneArr })
        setErr({ ...err, topics: '' })
        setIds([])
      }
    }
  }, [cloneService, serviceD])

  useEffect(() => {
    if (createdService == true) {
      var mainArr = [...data.topics];
      mainArr = mainArr.filter((cc) => cc._id !== clone);
      if (serviceD.length > 0) {
        mainArr.push(serviceD[0])
        console.log('mainArr', mainArr, 'service Craeted', serviceD, 'clone', clone)
        setData({ ...data, topics: mainArr })
        setErr({ ...err, topics: '' })
        setClone("")
      }
    }
  }, [createdService, serviceD])

  const handleService = (e, n, r) => {
    console.log('check', r, 'new', n, 'rest', r)
    setData({ ...data, topics: n })
    setErr({ ...err, topics: '' })
  }

  const handleSubmit = () => {
    var mainCheck = data.topics.filter((cc) => cc.user_id == id)
    var check = checkService({ topics: data.topics, check: mainCheck })
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    //console.log('check Id Matched', data.topics.filter((cc) => cc.user_id == id))
    dispatch(shopProfileService(id, { topics: mainCheck.map((cc) => cc._id) }))
  }

  const handleClose = (ss) => {
    console.log('make Change', ss)
    setView(!view)
  }

  const handleRemoveButton = (val) => {
    var mainArr = [...data.topics];
    mainArr = mainArr.filter((cc) => cc._id !== val);
    setData({ ...data, topics: mainArr })
  }

  const handleCloneButton = (value) => {
    setClone(value)
    dispatch(serviceRegistrationClone(value))
  }

  const handleCloneAllBtn = () => {
    var arrIds = data.topics.filter((cc) => cc.user_id !== id)
    console.log('all Ids Which clone aree meant', arrIds)
    setIds(arrIds.map((cc) => cc._id))
    if (arrIds.length > 0) {
      dispatch(serviceAllClone({ id: arrIds.map((cc) => cc._id) }))
    } else {
      setErr({ ...err, topics: 'Select Topics that not created by you' })
    }
  }

  const handleEdit = (val) => {
    setValue({
      ...value,
      open: true,
      name: val.name,
      image: val.image,
      description: val.description,
      motivation: val.motivation,
      emojiIcon: val.emojiIcon,
      waiting_number: val.waiting_number,
      waiting_key: val.waiting_key,
      waiting_time: val.waiting_time,
      seat: val.seat,
      tags: val.tags,
      price: val.price,
      seat: (val.seat == null || val.seat == undefined) ? 1 : val.seat,
      _id: val._id
    })
    setClone(val._id)
  }

  const handleEditClose = (ss) => {
    console.log('make Change', ss)
    setValue({ ...value, open: false })
  }

  return (
    <Grid className={classes.grid}>
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12}>
                <Typography variant="h5" component="div" align="center">
                  Step 2: Add Service
                </Typography>
                <IconButton title="Create Service" onClick={() => { setView(!view) }} >
                  <AddCircleOutline />
                </IconButton>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Autocomplete
                  multiple={true}
                  id="multiple-limit-tags"
                  options={element.filter((cc) => cc.type == registrationD.type)}
                  disabled={loading}
                  getOptionLabel={(option) => (option.user_name == null || option.user_name == undefined) ? option.name : option.name + ' ' + (`${option.user_name}`)}
                  isOptionEqualToValue={(option, value) => option._id === value._id}
                  value={data.topics}
                  onChange={(e, n, r) => {
                    handleService(e, n, r)
                  }}
                  disableCloseOnSelect={true}
                  renderTags={(tagValue, getTagProps) =>
                    tagValue.map((option, index) => (
                      <Chip
                        label={option.name}
                        {...getTagProps({ index })}
                      />
                    ))
                  }
                  filterSelectedOptions={true}
                  renderInput={(params) => (
                    <TextField {...params} label="Services" placeholder="Add Services" disabled={true} />
                  )}
                />
                {err.topics && (<FormError data={err.topics}></FormError>)}
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ display: { xs: 'none', sm: 'none', md: 'block' }, marginTop: '5px' }}>
              <Grid item xs={12} align="right" >
                <BookingButton type="button" className="margin-right" disabled={loading || data.topics.length == 0 || data.topics.filter((cc) => cc.user_id !== id).length == 0} variant="contained" onClick={() => { handleCloneAllBtn() }}>Copy All Service</BookingButton>
                <BookingButton type="button" className="margin-right" disabled={loading} variant="contained" onClick={handleNext}>Skip</BookingButton>
                <BookingButton type="button" disabled={loading} variant="contained" onClick={() => { handleSubmit() }}>Next</BookingButton>
              </Grid>
            </Grid>
            <Grid container spacing={1} sx={{ display: { xs: 'flex', sm: 'flex', md: 'none' }, marginTop: '5px' }}>
              <Grid item xs={6} sm={6}>
                <BookingButton type="button" disabled={loading || data.topics.length == 0 || data.topics.filter((cc) => cc.user_id !== id).length == 0} variant="contained" onClick={() => { handleCloneAllBtn() }}>Copy All Service</BookingButton>
              </Grid>
              <Grid item xs={3} sm={3}>
                <BookingButton type="button" disabled={loading} variant="contained" onClick={handleNext}>Skip</BookingButton>
              </Grid>
              <Grid item xs={3} sm={3}>
                <BookingButton type="button" disabled={loading} variant="contained" onClick={() => { handleSubmit() }}>Next</BookingButton>
              </Grid>
            </Grid>
          </form>

          {
            data.topics.length > 0 && <>
              <Grid container sx={{ marginTop: '20px' }}>
                <Grid item xs={12} md={12} sm={12} sx={{ border: '1px solid' }}>
                  <Card sx={{ display: { xs: "none", md: "block", sm: "none" } }}>
                    <CardContent>
                      <TableContainer component={Paper} >
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                          <TableHead>
                            <TableRow>
                              <TableCell></TableCell>
                              <TableCell>Name</TableCell>
                              <TableCell>Motivation</TableCell>
                              <TableCell>Emoji Icon</TableCell>
                              <TableCell>Service Time</TableCell>
                              <TableCell>Seat</TableCell>
                              <TableCell>Image</TableCell>
                              <TableCell>Description</TableCell>
                              <TableCell>Remove</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {data.topics.map((row) => (
                              <TableRow
                                key={row._id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                style={(row.user_id !== id) ? { backgroundColor: 'grey' } : {}}
                              >
                                <TableCell>
                                  {row.user_id !== id && <div style={{ textAlign: 'center' }}>
                                    <div>{"To Select Service 'click' Copy."}</div>
                                    <BookingButton type="button" disabled={loading} variant="contained" onClick={() => { handleCloneButton(row._id) }}>Copy</BookingButton>
                                  </div>
                                  }
                                  {row.user_id == id &&
                                    <IconButton title="Edit" onClick={() => { handleEdit(row) }}>
                                      <Edit color="primary" />
                                    </IconButton>
                                  }
                                </TableCell>
                                <TableCell component="th" scope="row">
                                  {row.name}
                                </TableCell>
                                <TableCell>{row.motivation}</TableCell>
                                <TableCell>{row.emojiIcon}</TableCell>
                                <TableCell>{row?.waiting_number + ' ' + row.waiting_key}</TableCell>
                                <TableCell>{row?.seat.join()}</TableCell>
                                <TableCell>
                                  <ImageBox image={row.image} />
                                </TableCell>
                                <TableCell>{row.description}</TableCell>
                                <TableCell>
                                  {/* <Button type="button" disabled={loading} variant="contained" onClick={() => { handleRemoveButton(row._id) }} color="primary">Remove</Button> */}
                                  <IconButton title="Delete" disabled={loading} onClick={() => { handleRemoveButton(row._id) }}>
                                    <Cancel color="warning" />
                                  </IconButton>
                                </TableCell>
                              </TableRow>
                            ))}
                          </TableBody>
                        </Table>
                      </TableContainer>
                    </CardContent>
                  </Card>
                  {/* Mobile View */}
                  <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "block" } }} >
                    <Table size="small" aria-label="simple table">
                      <TableBody>
                        {data.topics.map((row) => (
                          <React.Fragment key={row._id}>
                            <TableRow
                              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                              style={(row.user_id !== id) ? { backgroundColor: 'grey' } : {}}
                            >
                              <TableCell style={{ display: 'flex', justifyContent: 'center', flexDirection: 'column' }}>
                                <Card sx={{ borderRadius: '18px' }} >
                                  <CardMedia
                                    component="img"
                                    sx={{ width: '100%', objectFit: 'unset', borderRadius: '18px', border: 1 }}
                                    image={row.image}
                                    alt={row.name}
                                  />
                                  <Box>
                                    <CardContent>
                                      <Grid container spacing={2}>
                                        <Grid item xs={6} sm={6} >
                                          <div>Name</div>
                                          <div style={{ fontSize: '1.125rem' }}>{row.name}</div>
                                        </Grid>
                                        <Grid item xs={6} sm={6} >
                                          <div>Waiting Time</div>
                                          <div style={{ fontSize: '1.125rem' }}>{row?.waiting_number + ' ' + row.waiting_key}</div>
                                        </Grid>
                                        {row.user_id !== id && <Grid item xs={12} sm={12}>
                                          <div style={{ textAlign: 'center' }}>
                                            <div>{"To Select Service 'click' Copy."}</div>
                                          </div>
                                        </Grid>}
                                        <Grid item xs={6} sm={6}>
                                          {row.user_id !== id &&
                                            <Button variant="contained" disabled={loading} sx={{ textTransform: 'capitalize', fontSize: '0.75rem', background: '#d6a354' }} startIcon={<FileCopy color='primary' />} onClick={() => { handleCloneButton(row._id) }} >Copy</Button>
                                          }

                                          {row.user_id == id && <Button variant="contained" disabled={loading} sx={{ textTransform: 'capitalize', fontSize: '0.75rem', background: '#d6a354' }} startIcon={<Edit color='primary' />} onClick={() => { handleEdit(row) }} >Edit</Button>
                                          }
                                        </Grid>
                                        <Grid item xs={6} sm={6}>
                                          <Button variant="contained" disabled={loading} sx={{ textTransform: 'capitalize', fontSize: '0.75rem', background: '#d6a354' }} startIcon={<Cancel color='warning' />} onClick={() => { handleRemoveButton(row._id) }} >
                                            Delete
                                          </Button>
                                        </Grid>
                                      </Grid>
                                    </CardContent>
                                  </Box>
                                </Card>
                                {
                                  show == row._id ? <IconButton
                                    aria-label="expand row"
                                    size="small"
                                    onClick={() => setShow('')}
                                  >
                                    <KeyboardArrowUp />
                                  </IconButton> : <IconButton
                                    aria-label="expand row"
                                    size="small"
                                    onClick={() => setShow(row._id)}
                                  >
                                    <KeyboardArrowDown />
                                  </IconButton>
                                }
                              </TableCell>
                            </TableRow>
                            <TableRow>
                              <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                                <Collapse in={show == row._id} timeout="auto" unmountOnExit>
                                  <Box sx={{ margin: 1 }}>
                                    <Grid container spacing={2}>
                                      <Grid item xs={6} sm={6}>
                                        <div>Motivation</div>
                                        <div style={{ fontSize: '1.125rem' }}>{row.motivation}</div>
                                      </Grid>
                                      <Grid item xs={6} sm={6}>
                                        <div>Description</div>
                                        <div style={{ fontSize: '1.125rem' }}>{row.description}</div>
                                      </Grid>
                                      <Grid item xs={6} sm={6}>
                                        <div>Emoji Icom</div>
                                        <div style={{ fontSize: '1.125rem' }}>{row.emojiIcon}</div>
                                      </Grid>
                                      <Grid item xs={6} sm={6}>
                                        <div>Seat</div>
                                        <div style={{ fontSize: '1.125rem' }}>{row?.seat.join()}</div>
                                      </Grid>
                                    </Grid>
                                  </Box>
                                </Collapse>
                              </TableCell>
                            </TableRow>
                          </React.Fragment>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                </Grid>
              </Grid>
            </>
          }
        </CardContent>
      </Card>
      <Dialog
        open={view}
        onClose={handleClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <CreateService setId={handleClose} view={false} />
      </Dialog>
      <Dialog
        open={value.open}
        onClose={handleEditClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <EditService allvalue={value} setId={handleEditClose} show={false} />
      </Dialog>
    </Grid>
  )
}

export default ServicePageRegister

const checkService = (data) => {
  let errors = {}

  if (!data.topics) {
    errors['topics'] = "Services is Required";
  }

  if (data.topics) {
    if (data.topics.length == 0) {
      errors['topics'] = "Services is Required ";
    }
    if (data.topics.length > 0 && data.check.length == 0) {
      errors['topics'] = "Services is Required either clone it and create then select it";
    }
  }

  return errors
}