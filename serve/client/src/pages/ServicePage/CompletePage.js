import React from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { tableStyles } from 'pages/Shop/Dashboard/styles';
import { Card, CardContent, Grid, Typography, Button } from '@mui/material';
import { BookingButton } from "components/common";

const CompletePage = () => {

  const linkToggle = useSelector(state => state.auth.link);
  const history = useNavigate()
  const classes = tableStyles()

  const handleLogButton = () => {
    history(linkToggle == "" ? '/' : linkToggle)
    localStorage.removeItem('registration-token')
  }

  const handleButton = () => {
    history("/")
    localStorage.removeItem('registration-token')
  }

  return (
    <Grid className={classes.grid}>
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12}>
                <Typography variant="h5" component="div" align="center">
                  Registration Successfully Completed
                </Typography>
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }}>
              <Grid item xs={12} align="center">
                <BookingButton type="button" sx={{ marginRight: '2px' }} variant="contained" onClick={() => { handleLogButton() }}>Home</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default CompletePage