import React, { useEffect, useState, useContext } from 'react'
import { Grid, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, AppBar, Toolbar, Typography, useTheme } from '@mui/material'
import moment from 'moment'
import { DetailStyles } from 'pages/Shop/Dashboard/styles'
import Test from 'pages/Clock/Test'
import StatusBox from 'pages/Shop/UsableComponent/StatusBox'
import TooltipBox from 'pages/Shop/UsableComponent/TooltipBox'
import { useDispatch, useSelector } from 'react-redux'
import { getSingleBooking } from 'redux/action'
import { useParams, useNavigate } from 'react-router'
import TableLoader from 'pages/Shop/UsableComponent/TableLoader'
import NoData from 'pages/Shop/UsableComponent/NoData'
import Geocode from 'react-geocode';
import SocketContext from 'config/socket';
import { getDateChange } from 'config/KeyData'
import DateTimer from 'pages/Shop/UsableComponent/DateTimer'
Geocode.setApiKey('AIzaSyCqiCKV0cm-EpHp2hev4XkD5wG9JGxFbVs');
Geocode.enableDebug();

const BookingDetailPage = () => {

  const bookingData = useSelector((state) => state.auth.singleBooking)
  const dispatch = useDispatch()
  const theme = useTheme()
  const [flag, setFlag] = useState(false)
  const [element, setElement] = useState({})
  const classes = DetailStyles()
  const history = useNavigate()
  const { id } = useParams()
  const [userD, setUserD] = useState({
    lattitude: '',
    longitude: '',
    address: ''
  })
  const context = useContext(SocketContext)

  useEffect(() => {

    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      if (localStorage.getItem('auth-token') !== null) {
        context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
      }
    })

    context.socket.on('disconnect', () => {
      context.socket.removeAllListeners()
    })

    return () => {
      context.socket.removeAllListeners()
    }

  }, [])

  useEffect(() => {

    if ("geolocation" in navigator) {
      console.log("Available");
    } else {
      console.log("Not Available");
    }

    navigator.geolocation.getCurrentPosition(function (position) {
      console.log('latitude', position.coords.latitude, 'longitude', position.coords.longitude)
      setUserD({ ...userD, lattitude: position.coords.latitude, longitude: position.coords.longitude })
      context.socket.emit('bookingLocation', { lattitude: position.coords.latitude, longitude: position.coords.longitude }, id)
      Geocode.fromLatLng(position.coords.latitude, position.coords.longitude).then(
        (response) => {
          const address = response.results[0].formatted_address;
          console.log(address, 'latitude', position.coords.latitude, 'longitude', position.coords.longitude);
          setUserD({ ...userD, address: address, lattitude: position.coords.latitude, longitude: position.coords.longitude })
        },
        (error) => {
          console.error('Location Err', error);
        }
      );
    },
      function (error) {
        console.error("Error Code = " + error.code + " - " + error.message);
      });

  }, [])

  useEffect(() => {

    if (id.length > 24 || id.length < 24) {
      history('/')
    }

    if (Object.keys(bookingData).length == 0) {
      dispatch(getSingleBooking(id))
    } else {
      setFlag(true)
      setElement(bookingData.detail)
    }

  }, [bookingData])

  return (
    <Grid container spacing={2} className={classes.grid} sx={{ color: 'white' }}>
      <AppBar sx={{ position: 'relative', backgroundColor: theme.palette.mode == 'dark' ? '#242424' : '#252525' }}>
        <Toolbar>
          <Typography className='title_head' sx={(theme) => ({
            [theme.breakpoints.up('sm')]: {
              ml: 2, flex: 1, fontSize: '1.5rem'
            },
            [theme.breakpoints.up('xs')]: {
              ml: 2, flex: 1, fontSize: '1rem'
            },
            [theme.breakpoints.up('md')]: {
              ml: 2, flex: 1, fontSize: '1.5rem'
            },
            [theme.breakpoints.up('lg')]: {
              ml: 2, flex: 1, fontSize: '2.5rem'
            }
          })} variant="h6" component="div">
            Booking Detail
          </Typography>
          <Typography className='title_head' sx={(theme) => ({
            [theme.breakpoints.up('sm')]: {
              ml: 2, flex: 1, fontSize: '1.5rem'
            },
            [theme.breakpoints.up('xs')]: {
              ml: 2, flex: 1, fontSize: '1rem'
            },
            [theme.breakpoints.up('md')]: {
              ml: 2, flex: 1, fontSize: '1.5rem'
            },
            [theme.breakpoints.up('lg')]: {
              ml: 2, flex: 1, fontSize: '2.5rem'
            }
          })} variant="h6" component="div">
            <DateTimer />
          </Typography>
        </Toolbar>
      </AppBar>
      {flag ? Object.keys(element).length > 0 ? <>
        {element.waiting_verify == true && element.end_time !== null && <Grid item xs={12} md={10} sm={12}>
          {(element.waiting_verify == true && element.end_time !== null) ? <Test maxTime={parseInt(element.waiting_time) * 60} maxDate={new Date(element.end_time).setSeconds(parseInt((element.durationVal !== null && element.durationVal !== undefined) ? element.durationVal : 0))} /> : null}
        </Grid>}
        <Grid item xs={12} md={4} sm={12}>
          <TableContainer component={Paper}>
            <Table size="small" aria-label="simple table">
              <TableBody>
                <TableRow>
                  <TableCell></TableCell>
                  <TableCell></TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Size</TableCell>
                  <TableCell>{(!element.size || element.size == null || element.size == undefined) ? '----' : element.size}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Number</TableCell>
                  <TableCell>{(!element.mobile || element.mobile == null || element.mobile == undefined) ? '-----' : element.mobile}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Appointment Date</TableCell>
                  <TableCell>{moment(element.appointment_date).format('LL')}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Appointment Time</TableCell>
                  <TableCell>{moment(element.appointment_date).calendar()}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Waiting Time</TableCell>
                  <TableCell>{(!element.waiting_number || element.waiting_number == null || element.waiting_number == undefined) ? '--------' : (element?.waiting_number < 60) ? element?.waiting_number + ' ' + element.waiting_key : getDateChange(element.waiting_number)}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Reach Time</TableCell>
                  <TableCell>{(!element.end_time || element.end_time == null || element.end_time == undefined) ? '----' : moment(new Date(element.end_time).setSeconds(parseInt((element.durationVal !== null && element.durationVal !== undefined) ? element.durationVal : 0))).calendar()}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Shop Name</TableCell>
                  <TableCell>{element?.shop_name}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Shop Email</TableCell>
                  <TableCell>{element.shop_email}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Service Name</TableCell>
                  <TableCell>{element?.service_id.name}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Seat</TableCell>
                  <TableCell>{element?.seat}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Service Time</TableCell>
                  <TableCell>{(element?.service_id.waiting_number < 60) ? element?.service_id.waiting_number + ' ' + element.service_id.waiting_key : getDateChange(element.service_id.waiting_number)}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Service End Time</TableCell>
                  <TableCell> {(element.queue == true) ? (element?.approximate_number < 60) ? element?.approximate_number + ' ' + element.approximate_key : getDateChange(element.approximate_number) : '------'}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Status</TableCell>
                  <TableCell><StatusBox value={element.status} /></TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Description</TableCell>
                  <TableCell sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
                    <TooltipBox title="hover" data={element.description} />
                  </TableCell>
                  <TableCell sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }}>
                    {element.description}
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
        {(element?.shop_id?.lattitude !== null && element?.shop_id?.longitude !== null && element?.shop_id?.lattitude !== undefined && element?.shop_id?.longitude !== undefined && element?.shop_id?.lattitude !== "" && element?.shop_id?.longitude !== "") && <Grid item xs={12} md={12} sm={12} className="page-margin">
          <iframe width="100%" height="300" frameBorder="0" scrolling="no" marginHeight="0" marginWidth="0"
            src={`https://maps.google.com/maps?q=${element.shop_id.lattitude},${element.shop_id.longitude}&z=15&output=embed`}
          ></iframe>
        </Grid>}
      </> : <NoData /> : <Grid item xs={12} sm={12} md={10}>
        <TableLoader />
      </Grid>}
    </Grid>
  )
}

export default BookingDetailPage
