import React, { useEffect, useState } from 'react';
import { Grid, InputBase, Paper } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { styled } from '@mui/material/styles';
import { ShopSelectN as Select } from '../../components/common/index';
// import { motivationOptions, shuffleTopics as topics } from '../../data/index';
import TopicCard from '../../components/TopicCard/index';
import InfiniteScroll from 'react-infinite-scroll-component';
import { motivationState, tabIndexState, topicState } from '../../store/local';
import { useRecoilState, useSetRecoilState } from 'recoil';
// import { Topic } from '../../types';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useDispatch, useSelector } from 'react-redux';
import { getAllActiveBarberService, getAllActiveService, getAllService } from 'redux/action';
import ServiceSearchComponent from 'pages/Shop/UsableComponent/ServiceSearchComponent';
import ServiceActiveSearchComponent from 'pages/Shop/UsableComponent/ServiceActiveSearchComponent';


var mainValue: any = [{
  value: 'All',
  label: 'All',
}]

const LEN = 8;

const SearchWrapper = React.memo(styled(Grid)({
  marginBottom: '15px',
  '.search_wrapper': {
    display: 'flex',
    alignItems: 'center',
  },
}));

const TextAreaWrapper = React.memo(styled(Paper)({
  marginLeft: '16px',
  padding: '2px 4px',
  display: 'flex',
  alignItems: 'center',
  backgroundColor: 'black !important',
  width: 400,

  '.Search_Input': {
    fontSize: '20px',
    padding: '0px 6px',
    width: '100%',
  },
}));

const CardContainer = React.memo(styled(Grid)({
  // Display: 'flex',
  // flexDirection: 'row',
  // justifyContent: 'space-around',
  display: 'flex',
  gridTemplateColumns: 'repeat(auto-fill, 320px)',
  justifyContent: 'space-between',
  marginTop: '3rem',
}));

const MentorsPage = () => {
  const [motivation, setMotivation] = useRecoilState(motivationState);
  const setTabIndex = useSetRecoilState(tabIndexState);
  const setTopic = useSetRecoilState(topicState);
  const [value, setValue] = useState({ label: 'All', value: 'All' })
  // const [items, setItems] = useState(topics.slice(0, LEN));

  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('md'));

  // @ts-ignore
  const motivationValue = value ? value?.value : 'All';

  // const fetchMoreData = () => {
  //   const n = items.length;
  //   setTimeout(() => {
  //     setItems(topics.slice(0, n + LEN));
  //   }, 300);
  // };

  //////  
  const [data, setData] = useState([])
  const [element, setElement] = useState<[]>([])
  const [flag, setFlag] = useState(false)
  const serviceData = useSelector((state: any) => state.auth.barberServiceList)
  const moreData = useSelector((state: any) => state.auth.serviceMore)
  const checkService = useSelector((state: any) => state.auth)
  const dispatch = useDispatch()

  useEffect(() => {
    if (serviceData.length == 0 && checkService.serviceRequestFailed == false) {
      dispatch(getAllActiveBarberService(0, 20))
    } else {
      setData(mainValue.concat([...new Set(serviceData.map((cc: any) => cc.motivation))].map((ss: any) => {
        return {
          label: ss,
          value: ss
        }
      })))
      setElement(serviceData)
      setFlag(true)
    }
  }, [serviceData])

  const filterTopics = (topics: [], motivation: string) => {
    if (motivation === 'All' || motivation === null) return topics;
    return element.filter((main: any) => main.motivation === motivation);
  };

  const handleMotivation = (val: any) => {
    setElement(serviceData)
    setValue(val)
    //setTopic(val.value)
  }

  const handleView = (e: any, ccd: any) => {
    if (ccd == null || ccd == undefined) {
      setElement(serviceData)
    } else {
      setElement(serviceData.filter((cc: any) => cc._id == ccd._id))
    }
  }

  return (
    <div className="mobile_padding" style={{ padding: '0rem 2rem' }}>
      <SearchWrapper justifyContent='center' container spacing={2}>
        <Grid item xs={12} sm={4} sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }}>
          <Paper className="searchBy"
            sx={{ display: 'flex', minWidth: '300px' }}>
            <ServiceActiveSearchComponent handleView={handleView} />
          </Paper>
        </Grid>
        <Grid className="searchBy" item xs={12} sm={3} sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
          <ServiceActiveSearchComponent handleView={handleView} />
        </Grid>
        <Grid className="filterOuter" item xs={12} sm={3} >
          <Paper
            sx={{ display: 'flex', minWidth: '300px' }}>
            <Select
              menuPlacement="auto"
              name="Motivation"
              sx={{ fontSize: '20px' }}
              //options={motivationOptions}
              options={data}
              value={value}
              onChange={(e) => { handleMotivation(e) }}
              isSearchable={matches}
              classNamePrefix="select"
              placeholder={<span>Filter by Services</span>}
            />
          </Paper>
        </Grid>
      </SearchWrapper>
      <InfiniteScroll className="searchServices"
        //dataLength={Math.min(topics.length, element.length)}
        dataLength={element.length}
        next={() => { dispatch(getAllActiveBarberService(element.length, 10)) }}
        // hasMore={filterTopics(element:[], motivationValue).length < filterTopics(element:[], motivationValue).length}
        hasMore={moreData}
        loader={<h4 className="loading">Loading...</h4>}>
        <Grid container spacing={2}>
          {filterTopics(element, motivationValue).map((item: any) => (
            <Grid item xs={12} sm={4} md={3} className="blockCards"
              key={item._id}
              onClick={() => {
                setTopic([item._id]);
                setMotivation({
                  label: item.motivation,
                  value: item.motivation
                });
                setTabIndex('1');
              }}>
              <TopicCard topic={item} />
            </Grid>
          ))}
        </Grid>
      </InfiniteScroll>
    </div>
  );
};

export default MentorsPage;
