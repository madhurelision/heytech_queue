import React, { useEffect, useState } from 'react';
import { Grid, InputBase, Paper, Box, Dialog, Card, CardContent, DialogTitle, IconButton, Button, Typography } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { styled } from '@mui/material/styles';
import { BookingButton, ReactSelect, ShopSelect as Select, ShopSelectN } from '../../components/common/index';
import { expertiseOptions } from '../../data/index';
import UserCard from '../../components/UserCard/index';
import { useRecoilState, useRecoilValue } from 'recoil';
import { expertiseState, motivationState, topicState } from '../../store/local';
import { SERVER_URL } from '../../config.keys';
import axios from 'axios';
import { MentorSchemaType } from '../../types';
import { useQuery } from 'react-query';
import { shuffleArray } from '../../utils/helper';
import Loader from 'react-loader-spinner';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useDispatch, useSelector } from 'react-redux';
import { getAllActiveBarberExpertise, getAllActiveBarberService, getAllActiveService, getAllBanner, getAllExpertise, getAllService } from 'redux/action';
import SearchComponent from 'pages/Shop/UsableComponent/SearchComponent';
import NoData from 'pages/Shop/UsableComponent/NoData'
import { SERVICE_REQ_FAILED, SHOP_LIST } from 'redux/action/types';
import { Close, Search as SearchBtn } from '@mui/icons-material'
import Geocode from 'react-geocode';
Geocode.setApiKey('AIzaSyCqiCKV0cm-EpHp2hev4XkD5wG9JGxFbVs');
Geocode.enableDebug();
var mainValue: any = [{
  value: 'All',
  label: 'All',
}]

const GridWrapper = React.memo(styled(Grid)({
  '.search_wrapper': {
    display: 'flex',
    alignItems: 'center',
  },
}));

const StyledBox = React.memo(styled(Box)({
  width: '100%',
  height: '15vh',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',

  '& svg': {
    fill: 'lightgray',
  },
}));

const TextAreaWrapper = React.memo(styled(Paper)({
  marginLeft: '16px',
  padding: '2px 4px',
  display: 'flex',
  alignItems: 'center',
  backgroundColor: 'black !important',
  width: 400,

  '.Search_Input': {
    fontSize: '20px',
    padding: '0px 6px',
    width: '100%',
  },
}));

const CardContainer = React.memo(styled(Grid)({
  display: 'grid',
  gridTemplateColumns: 'repeat(auto-fill, 300px)',
  justifyContent: 'space-between',
  marginTop: '3rem',
}));

const getMentors = async (expertise: string, topic: any[], position: any[], distance: any) => {
  const expertise_ = expertise === undefined ? 'All' : expertise;
  const topics_ = topic.length === 0 ? 'All' : topic;
  const distances_ = (distance == 'All' || distance == 'AnyWhere') ? 'All' : distance;
  const { data: response } = await axios.get(
    `${SERVER_URL}/api/get-shops`,
    {
      params: {
        expertise: expertise_,
        topic: topics_,
        type: '6242d0c9f0addae2c2328a59',
        position: position,
        distance: distances_
      },
    },
  );
  // @ts-ignore
  return response;
  // return shuffleArray(response);
};

// @ts-ignore
const RenderCards = ({
  isLoading,
  data,
  element
}: {
  isLoading: boolean;
  data: any[];
  element: any[];
}) => {
  if (isLoading || typeof data === 'undefined') return <div />;

  const users = data.slice(0, 50);
  return (
    <Grid container spacing={2} sx={{ marginTop: '1rem' }}>
      {users.map((user, index) => (
        <Grid item xs={12} sm={6} md={3} key={index}>
          <UserCard
            key={index}
            // @ts-ignore
            user={user}
            value={element}
          />
        </Grid>
      ))}
    </Grid>
  );
};

const MentorsPage = () => {
  console.log('Mentor Page Console');
  const [expertise, setExpertise] = useRecoilState(expertiseState);
  const [motivation, setMotivation] = useRecoilState(motivationState);
  // @ts-ignore
  const expertiseValue = expertise?.value;
  const [topic, setTopic] = useRecoilState(topicState);
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('md'));
  const [element, setElement] = useState([])
  const [value, setValue] = useState([])
  const [view, setView] = useState(false)
  const [flag, setFlag] = useState(false)
  const [main, setMain] = useState(true)
  const expertiseData = useSelector((state: any) => state.auth.barberExpertiseList)
  const checkService = useSelector((state: any) => state.auth)
  const serviceData = useSelector((state: any) => state.auth.barberServiceList)
  const shopData = useSelector((state: any) => state.auth.shopList)
  const dispatch = useDispatch()
  const [banner, setBanner] = React.useState<any>([]);
  const bannerData = useSelector((state: any) => state.auth.bannerList)
  const [userD, setUserD] = useState({
    lattitude: '',
    longitude: ''
  })
  const [distance, setDistance] = useState({
    label: 'All',
    value: 'All'
  })
  // @ts-ignore
  const distanceValue = distance?.value;

  useEffect(() => {
    if (bannerData.length == 0) {
      dispatch(getAllBanner())
    } else {
      setBanner(bannerData.banner)
    }
  }, [bannerData])

  useEffect(() => {
    if (expertiseData.length == 0) {
      dispatch(getAllActiveBarberExpertise())
    }
  }, [expertiseData])

  const handleMotivation = (val: any) => {
    setMotivation(val)
    setTopic(serviceData.filter((cc: any) => cc.motivation == val.label).map((dd: any) => dd._id))
    setView(false)
  }

  const filterExpert = (val: string) => {
    if (val == 'All' || val == null || val == undefined) return expertiseData
    const mains = serviceData.filter((cc: any) => cc.motivation == val).map((dd: any) => dd.shop)
    return expertiseData.filter((cc: any) => mains.includes(cc.shop))
  };


  const handleNear = (val: any) => {
    setDistance(val)
    setView(false)
  }

  const handleView = (e: any, ccd: any) => {
    if (ccd == null || ccd == undefined) {
      setValue(shopData)
    } else {
      setValue(shopData.filter((cc: any) => cc._id == ccd._id))
    }
  }

  const handleOpen = (val: any) => {
    setView(!view)
  }

  const handleClose = () => {
    setView(false)
  }

  useEffect(() => {
    if (serviceData.length == 0 && checkService.serviceRequestFailed == false) {
      dispatch(getAllActiveBarberService(0, 20))
    } else {
      setElement(mainValue.concat([...new Set(serviceData.map((cc: any) => cc.motivation))].map((ss: any) => {
        return {
          label: ss,
          value: ss
        }
      })))
      setFlag(true)
    }
  }, [serviceData])

  useEffect(() => {
    console.log('request Created')
    getMentors(expertiseValue, topic, [userD.longitude, userD.lattitude], distanceValue).then((cc) => {
      setValue(cc)
      setMain(false)
      dispatch({ type: SHOP_LIST, payload: { data: cc } })
    }).catch((err) => {
      console.log('main Err', err)
      if (!err.response || err.response === null || err.response === undefined) {
        dispatch({ type: SERVICE_REQ_FAILED, payload: err.message })
      } else {
        dispatch({ type: SERVICE_REQ_FAILED, payload: err.response.data.message })
      }
    })

  }, [expertiseValue, topic, distanceValue, userD])

  useEffect(() => {

    if ("geolocation" in navigator) {
      console.log("Available");
    } else {
      console.log("Not Available");
    }

    navigator.geolocation.getCurrentPosition(function (position: any) {
      console.log('position', position.coords)
      setUserD({ ...userD, 'lattitude': position.coords.latitude, 'longitude': position.coords.longitude })
    },
      function (error) {
        console.error("Error Code = " + error.code + " - " + error.message);
      });

  }, [])

  // const { isLoading, data } = useQuery(['mentors', expertiseValue, topic], async () => {
  //   return await getMentors(expertiseValue, topic)
  // }, { enabled: true },
  // );

  // if (!isLoading && value !== data) setValue(data);

  const content =
    main === false ? (value.length == 0 ? <NoData /> :

      <RenderCards
        isLoading={main}
        // @ts-ignore
        data={value.sort((a: any, b: any) => b.open - a.open)}
        element={banner}
      />
    ) : (
      <StyledBox>
        <Loader type="ThreeDots" color="#00BFFF" height={80} width={80} />
      </StyledBox>
    );

  return (
    <>
      <GridWrapper className='filterOuter' justifyContent="center" container spacing={1} >
        <Grid className='searchFil' item xs={8} sm={8} md={6} sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }}>
          <SearchComponent handleView={handleView} />
        </Grid>
        <Grid className='searchFil' item xs={12} sm={12} md={3} sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
          <SearchComponent handleView={handleView} />
        </Grid>
        {/* <Grid item xs={12} sm={12} md={3} sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
          <Paper
            sx={{ display: 'flex', minWidth: '300px' }}>
            <Select
              name="Expertise"
              menuPlacement="auto"
              sx={{ fontSize: '20px' }}
              //@ts-ignore
              options={mainValue.concat(filterExpert(motivation?.label))}
              value={expertise}
              onChange={setExpertise}
              isSearchable={true}
              classNamePrefix="select"
              placeholder={<span style={{ display: 'flex' }} >
                Filter by Expertise</span>}
            />
          </Paper>
        </Grid> */}
        <Grid item xs={12} sm={12} md={3} sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
          <Paper
            sx={{ display: 'flex', minWidth: '300px' }}>
            <Select
              menuPlacement="auto"
              name="Motivation"
              sx={{ fontSize: '20px' }}
              //options={motivationOptions}
              options={element}
              value={motivation}
              onChange={(e) => { handleMotivation(e) }}
              isSearchable={true}
              classNamePrefix="select"
              placeholder={<span style={{ display: 'flex' }} >
                {/* <SearchBtn sx={{ color: 'black !important', fontSize: '30px' }} /> */}
                Filter by Services</span>}
            />
          </Paper>
        </Grid>

        {userD.longitude != '' && userD.lattitude !== '' && <Grid item xs={12} sm={12} md={3} sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
          <Paper
            sx={{ display: 'flex', minWidth: '300px' }}>
            <ShopSelectN
              menuPlacement="auto"
              name="Motivation"
              sx={{ fontSize: '20px' }}
              //options={motivationOptions}
              options={[
                {
                  label: 'All',
                  value: 'All'
                },
                {
                  label: '10 Km',
                  value: 10000
                },
                {
                  label: '50 Km',
                  value: 50000
                },
                {
                  label: '100 Km',
                  value: 100000
                },
                {
                  label: '500 Km',
                  value: 500000
                },
                {
                  label: '1000 Km',
                  value: 1000000
                },
                {
                  label: 'AnyWhere',
                  value: 'AnyWhere'
                }
              ]}
              value={distance}
              onChange={(e) => { handleNear(e) }}
              isSearchable={true}
              classNamePrefix="select"
              placeholder={<span style={{ display: 'flex' }} >
                {/* <SearchBtn sx={{ color: 'black !important', fontSize: '30px' }} /> */}
                Filter by Nearby</span>}
            />
          </Paper>
        </Grid>}
        <Grid item xs={4} sm={4} md={6} sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }}>
          <BookingButton sx={{ padding: '8px 35px !important' }} variant="contained" onClick={handleOpen} >Filter</BookingButton>
        </Grid>
      </GridWrapper>
      {content}
      {/* <StyledBox>
        <Loader type="ThreeDots" color="#00BFFF" height={80} width={80} />
      </StyledBox> */}
      <Dialog className="popupFilter"
        open={view}
        onClose={handleClose}
        fullWidth
        aria-labelledby="responsive-dialog-title"
      >
        <Grid>
          <Card>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Box flexGrow={1} ><Typography className='title_head' variant="h5" component="div">
                      Filter
                    </Typography> </Box>
                    <Box>
                      <IconButton onClick={handleClose}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                {/* <Grid item xs={12} sm={12} md={12}>
                  <Select
                    name="Expertise"
                    menuPlacement="auto"
                    sx={{ fontSize: '20px' }}
                    menuPortalTarget={document.body}
                    styles={{ menuPortal: (base) => ({ ...base, zIndex: 99999 }), }}
                    //@ts-ignore
                    options={mainValue.concat(filterExpert(motivation?.label))}
                    value={expertise}
                    onChange={setExpertise}
                    isSearchable={true}
                    classNamePrefix="select"
                    placeholder={<span style={{ display: 'flex' }} >
                      Filter by Expertise</span>}
                  />
                </Grid> */}
                <Grid item xs={12} sm={12} md={12}>
                  <Select
                    menuPlacement="auto"
                    name="Motivation"
                    sx={{ fontSize: '20px' }}
                    menuPortalTarget={document.body}
                    styles={{ menuPortal: (base) => ({ ...base, zIndex: 99999 }), }}
                    //options={motivationOptions}
                    options={element}
                    value={motivation}
                    onChange={(e) => { handleMotivation(e) }}
                    isSearchable={true}
                    classNamePrefix="select"
                    placeholder={<span style={{ display: 'flex' }} >
                      {/* <SearchBtn sx={{ color: 'black !important', fontSize: '30px' }} /> */}
                      Filter by Services</span>}
                  />
                </Grid>

                {userD.longitude != '' && userD.lattitude !== '' && <Grid item xs={12} sm={12} md={12}>
                  <ShopSelectN
                    menuPlacement="auto"
                    name="Motivation"
                    sx={{ fontSize: '20px' }}
                    menuPortalTarget={document.body}
                    styles={{ menuPortal: (base) => ({ ...base, zIndex: 99999 }), }}
                    //options={motivationOptions}
                    options={[
                      {
                        label: 'All',
                        value: 'All'
                      },
                      {
                        label: '10 Km',
                        value: 10000
                      },
                      {
                        label: '50 Km',
                        value: 50000
                      },
                      {
                        label: '100 Km',
                        value: 100000
                      },
                      {
                        label: '500 Km',
                        value: 500000
                      },
                      {
                        label: 'AnyWhere',
                        value: 'AnyWhere'
                      }
                    ]}
                    value={distance}
                    onChange={(e) => { handleNear(e) }}
                    isSearchable={true}
                    classNamePrefix="select"
                    placeholder={<span style={{ display: 'flex' }} >
                      {/* <SearchBtn sx={{ color: 'black !important', fontSize: '30px' }} /> */}
                      Filter by Nearby</span>}
                  />
                </Grid>}
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Dialog>
    </>
  );
};

export default React.memo(MentorsPage);
