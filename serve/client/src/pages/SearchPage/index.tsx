import React from 'react';
import Appbar from '../../components/Appbar/index';
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Grid from '@mui/material/Grid';
import { styled, useTheme } from '@mui/material/styles';
import MentorsPage from './MentorsPage';
import TopicsPage from './TopicsPage';
import { useRecoilState } from 'recoil';
import { tabIndexState } from '../../store/local';
import { useSelector } from 'react-redux';
import RefreshPage from 'pages/RefreshPage/RefreshPage';
import MobileToolbar from 'components/MobileToolbar/MobileToolbar';
import { useContext } from 'react';
import SocketContext from 'config/socket';
import { useEffect } from 'react';

interface TabPanelProps {
  children?: React.ReactNode;
  index: string;
  value: string;
}
// const StyledTabs = styled((props) => (
//   <Tabs
//     {...props}
//     TabIndicatorProps={{ children: <span className="MuiTabs-indicatorSpan" /> }}
//   />
// ))({
//   '& .MuiTabs-indicator': {
//     display: 'flex',
//     justifyContent: 'center',
//     backgroundColor: 'transparent',
//   },
//   '& .MuiTabs-indicatorSpan': {
//     maxWidth: 40,
//     width: '100%',
//     backgroundColor: '#635ee7',
//   },
// });

// const StyledTab = styled((props) => <Tab disableRipple {...props} />)(
//   ({ theme }) => ({
//     textTransform: 'none',
//     fontWeight: theme.typography.fontWeightRegular,
//     fontSize: theme.typography.pxToRem(15),
//     marginRight: theme.spacing(1),
//     color: 'rgba(255, 255, 255, 0.7)',
//     '&.Mui-selected': {
//       color: '#fff',
//     },
//     '&.Mui-focusVisible': {
//       backgroundColor: 'rgba(100, 95, 228, 0.32)',
//     },
//   }),
// );

const a11yProps = (index: string) => ({
  id: `simple-tab-${index}`,
  'aria-controls': `simple-tabpanel-${index}`,
});

const TabPanel = React.memo((props: TabPanelProps) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}>
      {value === index && <div>{children}</div>}
    </div>
  );
});


const Wrapper = React.memo(styled('div')({
  color: '#f5f5f5',
  minHeight: '100vh',
  '& .Mui-selected ': {
    background: '#d6a354 !important',
    color: '#fff !important',
  },
  '.MuiTab-root': {
    fontSize: '20px',
    fontWeight: 'normal !important',
    letterSpacing: '2px',
  },

  '.Tab_Box': {
    borderBottom: 1,
    borderColor: 'transparent',
    paddingInline: '2rem',
    paddingBlock: '1rem',
    textAlign: 'center',
    '.MuiButtonBase-root-MuiTab-root.Mui-selected': {
      '& .Mui-selected': {
        color: '#d6a354 !important',
      },
    }
  },
  '& .MuiTabs-indicator': {
    display: 'none',
    justifyContent: 'center',
    color: '#C31818',
    backgroundColor: '#C31818',
  },
}));

const SearchPage = () => {

  const theme = useTheme()
  const [tabIndex, setTabIndex] = useRecoilState(tabIndexState);

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setTabIndex(newValue);
  };

  // const context = useContext(SocketContext)

  // useEffect(() => {

  //   if (context.socket.connected == false) {
  //     context.socket = context.socketAuth(localStorage.getItem('auth-token'))
  //   }

  //   context.socket.on('connect', () => {
  //     console.log('Socket is connected')
  //     context.socket.emit('getExpertise', 'hello')
  //   })

  //   context.socket.on('expertise', data => {
  //     console.log('shop Expertise Data', data)
  //   })

  //   context.socket.on('disconnect', () => {
  //     context.socket.removeAllListeners()
  //   })

  //   return () => {
  //     context.socket.removeAllListeners()
  //   }
  // }, [])

  return (
    <>
      <Wrapper className='innerContainer' style={(theme.palette.mode == 'dark') ? { backgroundColor: '#242424' } : { backgroundColor: 'white' }}>
        {/* <Appbar /> */}
        {/* {(checkService.serviceRequestFailed == false || checkService.expertiseRequestFailed == false) ?
          <Box sx={{ width: '100%' }}>
            <Box className="Tab_Box">
              <Tabs
                value={tabIndex}
                onChange={handleChange}
                aria-label="Mentor-Topic Tabs">
                <Tab
                  label="Barbers"
                  value="1"
                  sx={{ typography: 'h4' }}
                  {...a11yProps('1')}
                />
                <Tab
                  label="Services"
                  value="2"
                  sx={{ typography: 'h4' }}
                  {...a11yProps('2')}
                />
              </Tabs>
            </Box>
            <TabPanel index="1" value={tabIndex}>
              <Box sx={{ padding: '0px 32px 32px 32px' }}>
                <MentorsPage />
              </Box>
            </TabPanel>
            <TabPanel index="2" value={tabIndex}>
              <TopicsPage />
            </TabPanel>
          </Box> : <RefreshPage />} */}
        <Box sx={{ width: '100%' }}>
          <Box className="Tab_Box">
            <Tabs className="insideTab"
              value={tabIndex}
              onChange={handleChange}
              aria-label="Mentor-Topic Tabs">
              <Tab
                label="Barbers"
                value="1"
                className="tab-btn-search title_head"
                sx={{ typography: 'h4' }}
                {...a11yProps('1')}
              />
              <Tab
                label="Services"
                value="2"
                className="tab-btn-search title_head"
                sx={{ typography: 'h4' }}
                {...a11yProps('2')}
              />
            </Tabs>
          </Box>
          <TabPanel index="1" value={tabIndex}>
            <Box className='mobile_padding' sx={{ padding: '0px 32px 32px 32px' }}>
              <MentorsPage />
            </Box>
          </TabPanel>
          <TabPanel index="2" value={tabIndex}>
            <TopicsPage />
          </TabPanel>
        </Box>
        {/* <Grid
          item
          sx={{
            display: { xs: "block", md: "none", sm: "block" },
            position: 'fixed',
            bottom: 0,
            padding: '10px 10px 0px 10px',
            width: '100%',
          }}
        >
          <MobileToolbar />
        </Grid> */}
      </Wrapper>
    </>
  );
};

export default React.memo(SearchPage);
