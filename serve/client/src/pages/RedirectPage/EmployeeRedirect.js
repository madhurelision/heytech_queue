import React, { useEffect } from 'react'
import { useNavigate, useParams } from 'react-router'

const EmployeeRedirect = () => {

  const { id } = useParams()

  const history = useNavigate()

  useEffect(() => {

    if (id.length != 0) {
      localStorage.setItem('auth-token', id)
      localStorage.setItem('type', 'employee')
      localStorage.setItem('isAuthenticated', true)
      history('/employee')
    } else {
      history('/')
    }


  }, [])

  return (
    <div>

    </div>
  )
}

export default EmployeeRedirect
