import React, { useEffect } from 'react'
import { useNavigate, useParams } from 'react-router'

const ShopRedirect = () => {

  const { id } = useParams()
  const history = useNavigate()

  useEffect(() => {

    if (id) {
      localStorage.setItem('auth-token', id)
      localStorage.setItem('type', 'shop')
      localStorage.setItem('isAuthenticated', true)
      history('/shop')
    } else {
      history('/')
    }


  }, [])

  return (
    <div>

    </div>
  )
}

export default ShopRedirect
