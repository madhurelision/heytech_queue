import React, { useEffect } from 'react'
import { useNavigate, useParams } from 'react-router'

const CustomerRedirect = () => {

  const { id } = useParams()

  const history = useNavigate()

  useEffect(() => {

    if (id.length != 0) {
      localStorage.setItem('auth-token', id)
      localStorage.setItem('type', 'customer')
      localStorage.setItem('isAuthenticated', true)
      history('/customer')
    } else {
      history('/')
    }


  }, [])

  return (
    <div>

    </div>
  )
}

export default CustomerRedirect
