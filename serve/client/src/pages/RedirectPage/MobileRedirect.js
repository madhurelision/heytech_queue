import React, { useEffect } from 'react'
import { useNavigate, useParams } from 'react-router'

const MobileRedirect = () => {

  const { id, token } = useParams()

  const history = useNavigate()

  useEffect(() => {

    if (token.length != 0) {
      localStorage.setItem('auth-token', token)
      localStorage.setItem('type', 'customer')
      localStorage.setItem('isAuthenticated', true)
      history('/user/' + id)
    } else {
      history('/')
    }


  }, [])

  return (
    <div>

    </div>
  )
}

export default MobileRedirect
