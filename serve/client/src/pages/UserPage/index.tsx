import React, { useContext, useEffect, useState } from 'react';
import { styled, Grid, Avatar, IconButton, Typography, Card, CardContent, Dialog, DialogTitle, DialogContent, useTheme, Button, Box } from '@mui/material';
import { Close, Favorite, Instagram, LinkedIn, ConnectWithoutContact } from '@mui/icons-material';
import { lightGreen } from '@mui/material/colors';
import PaginatedBookingCard from '../../components/PaginatedBookingCard/index';
import ShowMoreText from 'react-show-more-text';
import Divider from '@mui/material/Divider';
import { commaString } from '../../utils/helper';
import { BookingButton, ReactSelect as Select } from '../../components/common/index';
// import { motivationOptions } from '../../data/index';
import Appbar from '../../components/Appbar/index';
import axios from 'axios';
//import { MentorSchemaType } from '../../types';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useQuery } from 'react-query';
import { SERVER_URL } from '../../config.keys';
import Loader from '../../components/Loader/index';
// import { topics as topicData } from '../../data/index';
import { useRecoilState } from 'recoil';
import { mentorState } from '../../store/local';
import { useDispatch, useSelector } from 'react-redux';
import { getAllOffers, getAllService, shopUserDislike, shopUserLike } from 'redux/action';
import GalleryView from 'pages/Shop/Gallery/GalleryView';
import SocketContext, { socket } from 'config/socket';
import Testimonial from 'pages/Shop/UsableComponent/Testimonial';
import MobileToolbar from 'components/MobileToolbar/MobileToolbar';
import Geocode from 'react-geocode';
import ShopQueuePage from './ShopQueuePage';
import { ACTIVE_EMPLOYEE_LIST, FORM_OPEN } from 'redux/action/types';
import ShopQR from './ShopQR';
import CreateGallery from 'pages/Shop/Gallery/CreateGallery';
import PackageShop from './PackageShop';
Geocode.setApiKey('AIzaSyCqiCKV0cm-EpHp2hev4XkD5wG9JGxFbVs');
Geocode.enableDebug();

var mainValue: any = [{
  value: 'All',
  label: 'All',
}]


const Wrapper = React.memo(styled('div')`
  font-family: 'Circular Std';
  //min-height: 100vh;
  display: flex;
  flex-direction: column;
  /* height: 100%; */
  //width: '100vw';
  //background-color: #242424;
  color: #f5f5f5;
  background-color: 'transparent',
  height: '100%',
  width: '100%',
  over-flow: 'hidden',
  position: 'relative',
`);

const TextWrapper = React.memo(styled('div')(({ theme }) => ({

  width: '100%',
  '.show-more': {
    fontWeight: 400,
    color: theme.palette.mode == 'dark' ? '#d4d4d4' : 'black',
    fontSize: '16px',
  },
  '.show-more-anchor': {
    color: 'grey',
  },
  '.show-more-anchor:hover': {
    color: 'palevioletred',
  },
  '.MuiTypography-root': {
    fontFamily: 'Circular Std',
  },
})));

const Banner = React.memo(styled('div')`
  background-color: #7f5a83;
  // background-image: linear-gradient(120deg, #7f5a83 0%, #0d324d 74%);
  /* background-image: linear-gradient(90deg, #3512b2, #d18873); */

  width: 100%;
  height: 180px;
`);

const Container = React.memo(styled(Grid)`
  position: relative;
  display: flex;
  flex-direction: column;
  // max-width: 800px;
  /* background: #393939; */
  font-family: inter;
`);

const PhotoWrapper = React.memo(styled('div')`
  padding: 16px;
  display: flex;
  flex-direction: row;

  div {
    padding: 2px 8px;
  }
`);

const Photo = React.memo(styled('div')`
  width: 50px;
  .MuiAvatar-root {
    height: 100px;
    width: 100px;
    position: absolute;
    top: 0px;
    left: 0px;
    transform: translate(0, -50%);
  }
`);

const getMentor = async (id: string | undefined) => {
  const { data: response } = await axios.get(
    `${SERVER_URL}/api/get-mentor`,
    {
      params: {
        id,
      },
    },
  );
  return response;
};

// const getTopics = (topicNums: number[]) =>
//   topicNums.map((el) => topicData[Number(el)]);

const UserPage = () => {

  const theme = useTheme()
  const [isExpanded, setIsExpanded] = useState(false);
  const [openS, setOpenS] = useState(false);
  const [motivation, setMotivation] = useState('All');
  const [heart, setHeart] = useState<'error' | 'inherit'>('inherit');
  const [page, setPage] = useState(1);
  const [mentorData, setMentorData] = useRecoilState(mentorState);
  const socketOn = useSelector((state: any) => state.auth.socket)

  const [shopData, setShopData] = useState<{
    first_name: any;
    last_name: any;
    shop_name: any;
    image_link: any;
    description: any;
    instagram: any;
    job_title: any;
    company: any;
    expertise: any;
    language: any;
    location: any;
    topics: any;
    gallery: any;
    like: any;
    review: any;
    code: any;
    lattitude: any;
    longitude: any;
    seat: any;
    banner_link: any;
    open: any;
    queue: any;
    appointment: any;
    pack: any;
    user_information: any;
  }>({
    first_name: '',
    last_name: '',
    shop_name: '',
    image_link: '',
    description: '',
    instagram: '',
    location: '',
    job_title: '',
    company: '',
    expertise: [],
    language: [],
    topics: [],
    gallery: [],
    like: [],
    review: [],
    code: '',
    longitude: '',
    lattitude: '',
    seat: 0,
    banner_link: '',
    open: '',
    queue: '',
    appointment: '',
    pack: [],
    user_information: ''
  })
  const [checkData, setCheckData] = useState({})
  const [locData, setLocData] = useState({})
  const [userD, setUserD] = useState({
    lattitude: '',
    longitude: '',
    address: ''
  })
  const [element, setElement] = useState([])
  const [count, setCount] = useState<[]>([])
  const [booking, setBooking] = useState<[]>([])
  const [time, setTime] = useState<[]>([])
  const [mainT, setmainT] = useState('')
  const [flag, setFlag] = useState(false)
  const [check, setCheck] = useState(true)
  const [view, setView] = useState({
    open: false,
    image: ''
  })
  const serviceData = useSelector((state: any) => state.auth.serviceList)
  const userProfileData = useSelector((state: any) => state.auth.profile)
  const [offerD, setOfferD] = React.useState<any>([]);
  const offerData = useSelector((state: any) => state.auth.offerList)
  const cted = useSelector((state: any) => state.auth.updated)
  const uted = useSelector((state: any) => state.auth.created)
  const dispatch = useDispatch()
  const context = useContext(SocketContext)
  const { id } = useParams();
  const [socketCon, setSocketCon] = useState(false)
  const history = useLocation()

  useEffect(() => {
    console.log('yext history', history)
    //@ts-ignore
    if (history.state == null || history.state == undefined || history.state.id == "" || history.state.id == null || history.state.id == undefined) {
    } else {
      //@ts-ignore
      setMotivation(history.state.id)
    }

  }, [])

  useEffect(() => {

    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    if (context.socket.connected == true) {
      setSocketCon(true)
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      setSocketCon(true)
      if (localStorage.getItem('auth-token') !== null) {
        context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
      }
    })

    context.socket.on('userDisconnect', (data) => {
      console.log('Socket Err', data)
    })

    context.socket.on('userLocation', (data) => {
      // console.log('Sock Location', data)
      setLocData(data)
    })

    context.socket.on('shopValue', data => {

      if (data.data == id) {
        setCount(data.val)
      }

      //console.log('shop Socket Data', data)
      //setCount(data)
    })

    context.socket.on('bookingValue', data => {
      // console.log('shop Sockebokit Data', data)
      if (data.data == id) {
        setmainT(data.val)
      }
      //setmainT(data)
    })

    context.socket.on('shopQueueData', data => {
      //console.log('shop Sockebokit Data', data.val)
      if (data.data == id) {
        setBooking(data.val.booking)
      }
    })

    context.socket.on('shopEmployeeData', data => {
      //console.log('shop Sockebokit Data', data.val)
      dispatch({ type: ACTIVE_EMPLOYEE_LIST, payload: { data: data } })
    })

    context.socket.on('timeValue', data => {
      // console.log('shop time all Data', data)
      if (data.data == id) {
        setTime(data.val)
      }
    })

    context.socket.on('disconnect', () => {
      context.socket.removeAllListeners()
    })

    return () => {
      context.socket.removeAllListeners()
    }
  }, [])

  const getToken = (id: any) => {
    axios.get(
      `${SERVER_URL}/api/makeToken/` + id
    ).then((cc: any) => {
      //console.log('main Data', cc)
      context.socket.emit('userConnect', { token: cc.data.data, type: 'shop' })
    }).catch((err: Error) => {
      //console.log('main Data err', err)
    })
  }

  useEffect(() => {
    //console.log('socket connected check emit', context.socket.connected)
    if (socketCon == true) {
      // console.log('socket emittted after connected')
      context.socket.emit('shopQueue', id)
      //context.socket.emit('shopQueueTime', id)
      context.socket.emit('shopBookingQueue', id)
      context.socket.emit('shopEmployee', 'hello')
      context.socket.emit('shopServiceTime', id)
      setSocketCon(false)
    }

  }, [socketCon])

  useEffect(() => {
    if (serviceData.length == 0) {
      dispatch(getAllService(0, 20))
    } else {
      setElement(mainValue.concat(serviceData.map((cc: any) => {
        return {
          label: cc.motivation,
          value: cc._id
        }
      })))
      setFlag(true)
    }
  }, [serviceData])

  useEffect(() => {
    if (offerData.length == 0) {
      dispatch(getAllOffers())
    } else {
      setOfferD(offerData.offer.map((cc: any) => cc.service_id))
    }
  }, [offerData])

  const handleLikeButton = () => {
    dispatch(shopUserLike(id, { _id: userProfileData?._id }))
  }

  const handleDislikeButton = () => {
    dispatch(shopUserDislike(id, { _id: userProfileData?._id }))
  }

  const handleOpen = (item: any) => {
    setView({ ...view, open: true, image: item })
  }

  const handleClose = () => {
    setView({ ...view, open: false })
  }

  const handleOpenImage = (item: any) => {
    setOpenS(true)
  }

  const handleCloseImage = () => {
    setOpenS(false)
  }

  useEffect(() => {

    if (cted) {
      getMentor(id).then((cc) => {
        setMentorData(cc)
        setShopData(cc)
      }).catch((err) => {
        console.log('cc', err)
      })
    }

  }, [cted])


  useEffect(() => {

    if (uted) {
      getMentor(id).then((cc) => {
        setMentorData(cc)
        setShopData(cc)
      }).catch((err) => {
        console.log('cc', err)
      })
    }

  }, [uted])

  useEffect(() => {

    getMentor(id).then((cc) => {
      setMentorData(cc)
      setShopData(cc)
      setCheckData(cc)
      setCheck(false)
    }).catch((err) => {
      console.log('cc', err)
      setCheck(false)
    })

  }, [])

  useEffect(() => {

    if ("geolocation" in navigator) {
      console.log("Available");
    } else {
      console.log("Not Available");
    }

    navigator.geolocation.getCurrentPosition(function (position: any) {
      console.log('latitude', position.coords.latitude, 'longitude', position.coords.longitude)
      setUserD({ ...userD, 'lattitude': position.coords.latitude, 'longitude': position.coords.longitude })
      context.socket.emit('location', { lattitude: position.coords.latitude, longitude: position.coords.longitude }, id)

      if (localStorage.getItem('type') == 'customer') {
        context.socket.emit('customerLocation', { lattitude: position.coords.latitude, longitude: position.coords.longitude }, userProfileData?._id)
      }

      Geocode.fromLatLng(position.coords.latitude, position.coords.longitude).then(
        (response) => {
          const address = response.results[0].formatted_address;
          //console.log(address, 'latitude', position.coords.latitude, 'longitude', position.coords.longitude);
          //console.log('latitude', position.coords.latitude, 'longitude', position.coords.longitude);
          setUserD({ ...userD, 'address': address, 'lattitude': position.coords.latitude, 'longitude': position.coords.longitude })
        },
        (error) => {
          console.error('Location Err', error);
        }
      );
    },
      function (error) {
        console.error("Error Code = " + error.code + " - " + error.message);
      });

  }, [])

  //const { isLoading, data } = useQuery(['mentor', id], () => getMentor(id));

  if (typeof id === 'undefined') return <div />;

  if (check) return <Loader />;

  if (Object.keys(checkData).length == 0) return <div />;

  // if (!isLoading && mentorData !== data) setMentorData(data);

  const {
    first_name,
    last_name,
    shop_name,
    image_link,
    description,
    instagram,
    job_title,
    company,
    expertise,
    language,
    topics: topicNums,
    gallery,
    like,
    review,
    code,
    location,
    lattitude,
    longitude,
    seat,
    banner_link,
    open,
    queue,
    appointment,
    pack,
    user_information
  } = shopData;

  const name = (shop_name == null || shop_name == undefined) ? `${first_name} ${last_name}` : shop_name;
  const topics: [] = topicNums;
  // const job_title = 'Member of Technical Staff';
  // const company = 'Adobe';
  // const description = [
  //   'As an engineer with a design-focused MBA and experience in Entrepreneurship, I combine the perspectives from business, design, and technology to challenge conventional thinking about innovation and deliver critical & creative insights. I am a technology generalist with a deep interest and understanding of emerging technologies such as ML, chatbot, and Voice assistant technologies. I create experiential prototypes to tell stories about Future and digital experiences.',
  //   'Currently, I work as a Manager, Customer Experience at Questrade, leading a team of CX and Service Designers, managing and improving the customer experience for our existing offering, and designing and developing the new experiences through product and service innovation.',
  //   'I am passionate about innovation in financial services, transportation, and retail and actively write on LinkedIn and Medium on innovation, strategy, and Futures.',
  // ];
  // const expertise = ['Software Development', 'Product Management'];
  // const language = ['Hindi', 'English'];

  return (
    <>

      {/* <Appbar /> */}
      <Wrapper className={(open == false) ? 'colorMainC' : ''} >
        <div id="innerBanner">
          <div className='bannerAddress'>
            {userD.address !== "" && <div>{userD.address}</div>}
          </div>
          <Banner style={(banner_link == null || banner_link == undefined || banner_link == '') ? { backgroundImage: 'linear-gradient(120deg, #7f5a83 0%, #0d324d 74%)' } : { background: `url(${banner_link + '?tr=w-816'}) no-repeat center center fixed`, backgroundSize: 'cover' }} />
        </div>

        <Grid className="mainWrapperUser"
          container
          height="100%"
          justifyContent="center"
          sx={{ background: theme.palette.mode == 'dark' ? '#242424' : 'white', padding: '0rem 1rem' }}>

          <Container item md={8} xs={12} sm={12} className="page-margin">
            <PhotoWrapper>
              <Photo>
                <Avatar
                  src={image_link}
                  sx={{
                    bgcolor: lightGreen[500],
                    padding: '0px !important',
                    border: '5px solid #3E3E42',
                  }}>
                  {name.slice(0, 1)}
                </Avatar>
              </Photo>
              <span style={{ flexGrow: 1 }}></span>

              <div className="socialIcon">

                {/* <IconButton title="Connection">
                <ConnectWithoutContact style={(socketOn == true) ? { color: 'green' } : { color: 'red' }} />
              </IconButton>
              <div>
                <span style={(socketOn == true) ? { color: 'green' } : { color: 'red' }}>{(socketOn == true) ? 'online' : 'offline'}</span>
              </div> */}
                <IconButton
                  title="Instagram"
                  target="_blank"
                  href={instagram}
                  aria-label="instagram"
                  size="large"
                  color="primary">
                  <Instagram fontSize="inherit" />
                </IconButton>
                <IconButton className="favIcon" title="Like"

                  onClick={() => {
                    if (localStorage.getItem('type') == 'customer') {
                      like.includes(userProfileData?._id) ? handleDislikeButton() : handleLikeButton()
                    } else {
                      dispatch({ type: FORM_OPEN, payload: true })
                    }
                  }}
                  aria-label="add to wish list"
                  size="large"
                  // disabled={localStorage.getItem('type') == 'customer' ? false : true}
                  color={like.includes(userProfileData?._id) ? 'error' : (theme.palette.mode == 'dark') ? 'inherit' : 'primary'}>
                  <Favorite fontSize="inherit" />
                  <IconButton className="likeCount">{like.length}</IconButton>
                </IconButton>

              </div>


              <div className={`${socketOn == true ? 'socket-btn-green' : 'socket-btn-red'}`} >
                {(socketOn == true) ? 'online' : 'offline'}
              </div>
            </PhotoWrapper>

            <TextWrapper className="barberList" sx={{ color: theme.palette.mode == 'dark' ? 'white' : 'black' }}>
              <Typography variant="h5" fontWeight={700} sx={{ textTransform: 'capitalize' }}>
                Hi, I m {name} <span className='span-review'>Reviews ({review.length})</span>
              </Typography>
              <Typography fontWeight={600} sx={{ py: 1 }}>
                {job_title} at {company}
              </Typography>
              {open == false && <Typography className='socket-btn-red' fontWeight={600} sx={{ py: 1 }}>
                Shop is Close
              </Typography>}
              <Typography className={`${theme.palette.mode == 'dark' ? 'deatilLocaiton-dark' : 'deatilLocaiton-light'}`} fontWeight={400} sx={{ py: 1 }}>
                {/* {location}  */}
                {(userD.lattitude !== null && userD.longitude !== null || lattitude !== null || longitude !== null || userD.lattitude !== undefined && userD.longitude !== undefined || lattitude !== undefined || longitude !== undefined || userD.lattitude !== "" && userD.longitude !== "" || lattitude !== "" || longitude !== "") ? <a href={`https://www.google.com/maps/dir/?api=1&origin=${userD.lattitude},${userD.longitude}&destination=${lattitude},${longitude}`} target="_blank" >{location}</a> : location}
              </Typography>
              <ShowMoreText
                /* Default options */
                lines={3}
                className="show-more"
                more="Show more"
                less="Show less"
                anchorClass="show-more-anchor"
                onClick={() => setIsExpanded(!isExpanded)}
                expanded={false}
                truncatedEndingComponent={'... '}>
                {description}
                {/* {description.map((description: any, index: any) => (
                  <div key={index} style={{ padding: '8px, 0px' }}>
                    {description}
                  </div>
                ))} */}
              </ShowMoreText>
              <div className='scanCode'>
                <Grid item fontWeight={700} className="title_head">
                  QR Code
                </Grid>
                <Grid item>
                  <img src={code} width="100px" onClick={() => handleOpen(code)} />
                </Grid>
              </div>

            </TextWrapper>
            <Grid container sx={{ py: 2 }}>
              <Grid
                container
                direction="column"
                item
                xs={6}
                md={4}
                spacing={1}
                sx={{ color: theme.palette.mode == 'dark' ? 'white' : 'black' }}
              >
                <Grid item fontWeight={700} className="title_head">
                  Expertise
                </Grid>
                <Grid item>{commaString(expertise)}</Grid>
              </Grid>
              <Grid
                container
                direction="column"
                item
                xs={6}
                md={4}
                spacing={1}
                sx={{ color: theme.palette.mode == 'dark' ? 'white' : 'black' }}
              >
                <Grid item fontWeight={700} className="title_head">
                  Languages
                </Grid>
                <Grid item>{commaString(language)}</Grid>
              </Grid>
              <Grid
                container
                direction="column"
                item
                xs={12}
                md={4}
                spacing={1}
                sx={(theme) => ({
                  [theme.breakpoints.up('md')]: {
                    textAlign: 'end'
                  },
                  [theme.breakpoints.up('xs')]: {
                    textAlign: 'start',
                    marginTop: '2px'
                  },
                  [theme.breakpoints.up('sm')]: {
                    textAlign: 'start'
                  },
                  color: theme.palette.mode == 'dark' ? 'white' : 'black'
                })}
              >

              </Grid>
            </Grid>
            <Divider />
            {pack.length > 0 && <Grid item xs={12} md={12} sm={12} sx={{ paddingTop: '1rem' }}>
              <PackageShop pack={pack} id={user_information} />
            </Grid>}

            {/* adding select here */}
            <Grid item xs={12} md={4} sx={{ paddingTop: '1rem' }}>
              <div style={{ margin: '1rem 0rem' }}>
                <Select
                  menuPlacement="auto"
                  name="Topic"

                  // options={element}
                  options={
                    mainValue.concat([...new Set(topics.map((cc: any) => cc.motivation))].map((ss: any) => {
                      return {
                        label: ss,
                        value: ss
                      }
                    }))
                  }

                  //isMulti={true}
                  //closeMenuOnSelect={false}
                  // @ts-ignore
                  onChange={(e: any) => {
                    //console.log('Select', e)
                    setMotivation(e.value);
                    setPage(1);
                  }} // Value - label
                  isSearchable={true}
                  classNamePrefix="select"
                  placeholder={<span>Filter by Services</span>}
                />
              </div>
            </Grid>
            <Grid className="bookingCards" container width="100%">
              <PaginatedBookingCard
                motivation={motivation}
                topics={topics}
                page={page}
                setPage={setPage}
                value={count}
                wtime={mainT}
                location={locData}
                locationBook={{ ...locData, ...userD }}
                offer={offerD}
                stime={time}
              />
            </Grid>

            <Grid className='bookingContainer spaceBottom' container width="100%">
              <ShopQueuePage booking={booking} seat={seat} />
            </Grid>
            <Grid className='spaceBottom' container width="100%">
              <Grid item xs={12} md={12} sm={12}>
                <Card>
                  <Typography variant="h6" component="h4" align="center" sx={{ paddingTop: '16px' }} className="title_head booking_head">Reviews</Typography>
                  <CardContent>
                    {(!review || review == null || review == undefined || review.length == 0) ? <div className='shop-queue-tab no-recode'>No Review Found</div> : <Testimonial element={review} test={true} />}
                  </CardContent>
                </Card>
              </Grid>
            </Grid>
            <Grid container width="100%">
              <Grid item width="100%">
                <Card>
                  <Box display="flex" alignItems="center" sx={{ paddingTop: '16px', position: 'relative' }}>
                    <Typography flexGrow={1} variant="h6" component="h4" align="center" className="title_head booking_head" >Gallery</Typography>
                    {Object.keys(userProfileData).length > 0 && localStorage.getItem('type') == 'shop' && <Box style={{
                      position: 'absolute', right: 0
                    }}>
                      {id == userProfileData?._id && <BookingButton type="button" variant="contained" className='customer-btn' onClick={handleOpenImage} >Add Image</BookingButton>}
                    </Box>}
                  </Box>
                  <CardContent>
                    {(!gallery || gallery == null || gallery == undefined || gallery.length == 0) ? <div className='shop-queue-tab no-recode'>No Gallery Found</div> : <GalleryView element={gallery} />}
                  </CardContent>
                </Card>
              </Grid>
            </Grid>
          </Container>
        </Grid>
        <Dialog
          open={view.open}
          onClose={handleClose}
          fullWidth
        >
          {/* <img src={view.image} width="100%" height="50%" /> */}
          <ShopQR image={view.image} name={name} handleClose={handleClose} />
        </Dialog>
        <Dialog
          open={openS}
          onClose={handleCloseImage}
          fullWidth
          maxWidth="md"
          aria-labelledby="responsive-dialog-title"
        >
          <CreateGallery setId={handleCloseImage} />
        </Dialog>
        {/* 
        <Grid
          item
          sx={{
            display: { xs: "block", md: "none", sm: "block" },
            position: 'fixed',
            bottom: 0,
            padding: '10px 10px 0px 10px',
            width: '100%',
          }}
        >
          <MobileToolbar />
        </Grid> */}
      </Wrapper>
    </>
  );
};

export default UserPage;
