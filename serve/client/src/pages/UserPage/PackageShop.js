import React, { useEffect, useState } from 'react'
import Carousel from "react-elastic-carousel";
import { Dialog, IconButton } from '@mui/material'
import './package.css';
import Payment from 'pages/Payment/Payment';
import { useRecoilState } from 'recoil';
import { globalPackageState } from 'store';
import { VerticalAlignBottom } from '@mui/icons-material'
import { useDispatch, useSelector } from 'react-redux';
import { getShopPackageFeatureL, getSingleShopPackage } from 'redux/action';
import PackageShopFeature from './PackageShopFeature';
import { rupeeIndian } from '../../config/KeyData';

const breakPoints = [
  // { width: 1, itemsToShow: 1, itemsToScroll: 1 },
  // { width: 550, itemsToShow: 2 },
  // { width: 768, itemsToShow: 3 },
  // { width: 1200, itemsToShow: 4 },
  { width: 1, itemsToShow: 1, itemsToScroll: 1 },
  { width: 550, itemsToShow: 2 },
  { width: 850, itemsToShow: 3 },
  { width: 1150, itemsToShow: 4 },
  { width: 1450, itemsToShow: 5 },
  { width: 1750, itemsToShow: 6 },
];

const PackageShop = ({ pack, id }) => {

  const [show, setShow] = useState(false)
  const [element, setElement] = useState([])
  const [data, setData] = useState([])
  const [open, setOpen] = useState({
    id: '',
    type: '',
    open: false
  })
  const [catalogData, setCatalogData] = useRecoilState(globalPackageState);
  const featureD = useSelector((state) => state.auth.featurePackageList)
  const dispatch = useDispatch()

  useEffect(() => {

    if (featureD.length == 0) {
      //dispatch(getShopPackageFeatureL())
    } else {
      setElement(featureD)
    }

  }, [featureD])

  useEffect(() => {
    dispatch(getSingleShopPackage(id))
  }, [])

  const handleOpen = (cc) => {
    var price = parseInt(cc.price)
    if (price == 0 || price == null || price == undefined) {

    } else {
      setOpen({ ...open, id: cc._id, open: true, type: cc.view })
      setCatalogData(pack)
    }
  }

  const handleBtn = (pid) => {
    setData(element.filter((cc) => cc.package_id._id == pid))
    setShow(!show)
  }

  const handleCloseBtn = () => {
    setShow(false)
  }

  const handleClose = () => {
    setOpen({
      ...open,
      id: '', open: false
    })
  }

  return (
    <>
      <Carousel className="package-carousel new" enableSwipe={true} enableMouseSwipe={true} breakPoints={breakPoints}>
        {pack.map((cc) => <div className="package-card view-border" key={cc._id}>
          <div className="item">
            <div className="view">
              <div className='view-title-main' >
                <span className='view-title'>{cc.name}</span>
              </div>
              <p className='view-price'>
                {/* {'INR' + ' '} */}
                &#8377;{rupeeIndian.format(cc.price)}</p>
              <p className='view-month'>{cc.view}</p>
              <p className='view-desc'>
                {cc.description}
              </p>
              <div className='view-pay-main'>
                <span className='view-pay' onClick={() => { handleOpen(cc) }}>{cc.btn}</span>
              </div>
              <div className='view-list'>
                <span onClick={() => { handleBtn(cc._id) }}>Or see features <IconButton><VerticalAlignBottom /></IconButton></span>
              </div>
            </div>
          </div>
        </div>)}
      </Carousel>
      {/* {show == true && data.length > 0 && <PackageShopFeature mainData={data} handleBtn={handleCloseBtn} />} */}
      <Dialog
        open={open.open}
        onClose={handleClose}
        fullWidth
        aria-labelledby="responsive-dialog-title"
      >
        <Payment id={open.id} type={open.type} close={handleClose} />
      </Dialog>
      <Dialog
        open={show}
        onClose={handleCloseBtn}
        fullWidth
        aria-labelledby="responsive-dialog-title">
        <PackageShopFeature mainData={data} handleBtn={handleCloseBtn} />
      </Dialog>
    </>
  )
}

export default PackageShop