import React, { useState, useCallback } from 'react'
import { Card, CardContent, Grid, TableContainer, Typography, Table, TableBody, TableHead, TableCell, TableRow, Paper, IconButton, Collapse, Box, Dialog, DialogTitle } from '@mui/material'
import { getDateChange, SeatForm } from 'config/KeyData';
import { makeStyles } from '@mui/styles';
import moment from 'moment';
import BookStatus from 'pages/Shop/UsableComponent/BookStatus'
import { tableStyles } from 'pages/Shop/Dashboard/styles';
import singleImage from '../../assets/single.svg';
import doubleImage from '../../assets/double.svg';
import tripleImage from '../../assets/triple.svg';
import { Close } from '@mui/icons-material';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

const cardStyles = makeStyles(() => ({
  cardUi: {
    boxShadow: '0 4px 8px 0',
    background: 'white',
    color: '#252525',
    //textAlign: 'center',
    marginBottom: '10px',
    marginTop: '10px',
    border: '1px solid #252525',
    borderRadius: '5px',
    padding: '10px',
    //width: '70%',
    margin: 'auto',
    cursor: 'pointer',
  },
  cardTitle: {
    fontSize: '15px',
    fontWeight: 'bolder',
    textTransform: 'uppercase',
    borderBottom: '1px solid #ccc',
    padding: '0 0 5px',
    margin: '0 0 13px',
  },
  timeKey: {
    color: '#23F311',
  },
  waitKey: {
    color: '#d6a354'
  },
  cardKey: {
    fontWeight: 'bold',
    fontSize: '13px'
  },
  cardDiv: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  cardMKey: {
    fontWeight: 'bold',
    fontSize: '10px'
  },
}))

interface ShopQueuePageProps {
  booking: [];
  seat: any
}

const ShopQueuePage: React.FC<ShopQueuePageProps> = ({ booking, seat }) => {

  const classes = cardStyles()
  const classesn = tableStyles()
  const [view, setView] = useState<{
    open: any;
    data: any
  }>({
    open: false,
    data: {}
  })
  const userProfileData = useSelector((state: any) => state.auth.profile)
  const { id } = useParams();

  const handleTimerClose = useCallback((id) => {
    console.log('Timer Close', id)
  }, []);

  const handleOpen = (val: any) => {
    setView({
      ...view,
      data: val,
      open: true
    })
  }

  const handleClose = () => {
    setView({ ...view, open: false, data: {} })
  }

  return (
    <Grid className="bookingGrid" item xs={12} md={12} sm={12}>
      <Card>
        <Typography variant="h5" component="div" align="center" sx={{ paddingTop: '16px' }} className="title_head booking_head" >Bookings </Typography>
        <CardContent>
          <div className='bookingWrapper'>
            <div>
              {/* <TableHead>
                <TableRow>
                  {SeatForm(seat).map((cc) => {
                    var arr = booking.filter((dd: any) => parseInt(dd.seat) == cc)
                    return <TableCell key={cc}>
                      <div className='shop-queue-tab'>
                        <div className="seatNo">{"Seat " + cc}</div>
                        <div className="seatTime">
                          {
                            booking.length > 0 && arr.length > 0 && <span>{ //@ts-ignore 
                              moment(arr[arr.length - 1]?.approximate_date).calendar()}</span>}
                        </div>
                      </div>
                    </TableCell>
                  })}
                </TableRow>
              </TableHead> */}
              <div className='bookingSec'>
                {booking.length > 0 ? <>
                  {SeatForm(seat).map((cc, i) => {
                    var arr = booking.filter((dd: any) => parseInt(dd.seat) == cc)
                    return <div className="innerBooking" key={cc}>
                      <div className="seatTitle">
                        <div className='shop-queue-tab'>
                          <div className="seatNo">{"Seat " + cc}</div>
                          <div className="seatTime">
                            {
                              booking.length > 0 && arr.length > 0 && <span>{ //@ts-ignore 
                                moment(arr[arr.length - 1]?.approximate_date).calendar()}</span>}
                          </div>
                        </div>
                      </div>
                      {/* <TableCell key={cc + i} align="center" > */}
                      <div className="bookingCardCont">
                        {booking.filter((dd: any) => parseInt(dd.seat) == cc).map(function (ss: any, i) {
                          return <Box id="bookingCardView" key={ss._id} className={classes.cardUi} onClick={() => { handleOpen(ss) }}
                            sx={(theme) => ({
                              [theme.breakpoints.up('sm')]: {
                                width: '100%'
                              },
                              [theme.breakpoints.up('xs')]: {
                                width: '100%'
                              },
                              [theme.breakpoints.up('md')]: {
                                width: '70%'
                              },
                            })} >
                            <div className={classes.cardTitle} >{ss?.service_id?.name}</div>
                            {/* <div className={classes.cardDiv}>

                          return <TableCell key={ss._id}>
                            <Box className={classes.cardUi} onClick={() => { handleOpen(ss) }} style={{ width: '250px' }}
                            >
                              <div className={classes.cardTitle} >{ss?.service_id?.name}</div>
                              {/* <div className={classes.cardDiv}>

                              <div className={classes.cardKey}>Service end at</div>
                              <div style={{ fontSize: '9px' }}>{moment(ss.approximate_date).calendar()} <span className={classes.waitKey} >({(!ss.approximate_number || ss.approximate_number == null || ss.approximate_number == undefined) ? ss?.approximate_time : (ss?.approximate_number < 60) ? ss?.approximate_number + ' ' + ss.approximate_key : getDateChange(ss.approximate_number)})</span></div>
                            </div>
                            <div className={classes.cardDiv}>
                              <div className={classes.cardKey}>Waiting Time</div>
                              <div style={{ fontSize: '15px' }}>{(!ss.waiting_number || ss.waiting_number == null || ss.waiting_number == undefined) ? ss?.waiting_time : (ss?.waiting_number < 60) ? ss?.waiting_number + ' ' + ss.waiting_key : getDateChange(ss.waiting_number)}</div>
                            </div>

                            <div className={classes.cardDiv}>
                              <div className={classes.cardKey}>Status</div>
                              <div style={{ fontSize: '10px' }}><BookStatus value={ss.status} /></div>
                            </div> */}
                            <div className={classes.cardDiv}>
                              <div className={classes.cardKey}>No. of Users</div>
                              <div style={{ fontSize: '14px' }}>{ss.size}
                                {(ss.size == 1) && <img style={{ marginRight: '5px', marginLeft: '5px' }} src={singleImage} width="15px" height="15px" />}
                                {(ss.size == 2) && <img style={{ marginRight: '5px', marginLeft: '5px' }} src={doubleImage} width="15px" height="15px" />}
                                {(ss.size >= 3) && <img style={{ marginRight: '5px', marginLeft: '5px' }} src={tripleImage} width="15px" height="15px" />}</div>
                            </div>
                          </Box>
                        })}
                      </div>

                      {/* </TableCell> */}
                    </div>
                  })}
                </> : <div>
                  <div className='shop-queue-tab no-recode'>
                    No Data Found
                  </div>
                </div>}
              </div>
            </div>
          </div>

          {/* <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "block" } }}>
            <Table size="small" aria-label="simple table">
              <TableHead>
               
              </TableHead>
              <TableBody>
                {booking.length > 0 ? <>
                  {SeatForm(seat).map((cc, i) => {
                    var arr = booking.filter((dd: any) => parseInt(dd.seat) == cc)
                    return <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                      <TableCell key={cc}>
                        <div className='shop-queue-tab'>
                          <div>{"Seat " + cc}</div>
                          <div>
                            {
                              booking.length > 0 && arr.length > 0 && <span>{ //@ts-ignore 
                                moment(arr[arr.length - 1]?.approximate_date).calendar()}</span>}
                          </div>
                        </div>
                      </TableCell>
                      {booking.filter((dd: any) => parseInt(dd.seat) == cc).map(function (ss: any, i) {
                        return <TableCell>
                          <Box key={ss._id} className={classes.cardUi} onClick={() => { handleOpen(ss) }}
                            sx={{ width: '250px' }} >
                            <div className={classes.cardTitle} >{ss?.service_id?.name}</div>
                            
                            <div>
                              <div className={classes.cardMKey}>No. of Users</div>
                              <div style={{ fontSize: '14px' }}>{ss.size}
                                {(ss.size == 1) && <img style={{ marginRight: '5px', marginLeft: '5px' }} src={singleImage} width="15px" height="15px" />}
                                {(ss.size == 2) && <img style={{ marginRight: '5px', marginLeft: '5px' }} src={doubleImage} width="15px" height="15px" />}
                                {(ss.size >= 3) && <img style={{ marginRight: '5px', marginLeft: '5px' }} src={tripleImage} width="15px" height="15px" />}</div>
                            </div>
                          </Box>
                        </TableCell>
                      })}
                    </TableRow>
                  })}
                </> : <TableRow>
                  <TableCell className='shop-queue-tab' colSpan={12}>
                    No Data Found
                  </TableCell>
                </TableRow>}
              </TableBody>
            </Table>
          </TableContainer> */}
        </CardContent>
      </Card>
      <Dialog
        open={view.open}
        onClose={handleClose}
        fullWidth
        aria-labelledby="responsive-dialog-title"
      >
        <Grid>
          <Card>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} md={12} sm={12}><Box display="flex" alignItems="center">
                  <Box flexGrow={1} >
                    <Typography className='title_head' flexGrow={1} variant="h5" component="div">Booking Details</Typography>
                  </Box>
                  <Box>
                    <IconButton onClick={handleClose}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
                </Grid>
                <Grid item xs={6} sm={6} md={6} >Service Name</Grid>
                <Grid item xs={6} sm={6} md={6} >{view?.data?.service_id?.name}</Grid>
                <Grid item xs={6} sm={6} md={6} >Service end at</Grid>
                <Grid item xs={6} sm={6} md={6} >
                  {moment(view.data?.approximate_date).calendar()} <span>({(!view.data?.approximate_number || view.data?.approximate_number == null || view.data?.approximate_number == undefined) ? view?.data?.approximate_time : (view?.data?.approximate_number < 60) ? view?.data?.approximate_number + ' ' + view.data?.approximate_key : getDateChange(view.data?.approximate_number)})
                  </span>
                </Grid>
                {Object.keys(userProfileData).length > 0 && localStorage.getItem('type') == 'shop' && <>
                  {id == userProfileData?._id && <>
                    <Grid item xs={6} sm={6} md={6} >Waiting Time</Grid>
                    <Grid item xs={6} sm={6} md={6}>
                      {(!view.data?.waiting_number || view.data?.waiting_number == null || view.data?.waiting_number == undefined) ? view?.data?.waiting_time : (view?.data?.waiting_number < 60) ? view?.data?.waiting_number + ' ' + view.data?.waiting_key : getDateChange(view.data?.waiting_number)}
                    </Grid>
                  </>}
                </>}
                <Grid item xs={6} sm={6} md={6} >Service Time</Grid>
                <Grid item xs={6} sm={6} md={6}>
                  {(!view.data?.service_id?.waiting_number || view.data?.service_id?.waiting_number == null || view.data?.service_id?.waiting_number == undefined) ? view?.data?.service_id?.waiting_time : (view?.data?.service_id?.waiting_number < 60) ? view?.data?.service_id?.waiting_number + ' ' + view.data?.service_id?.waiting_key : getDateChange(view.data?.service_id?.waiting_number)}
                </Grid>
                <Grid item xs={6} sm={6} md={6} >Status</Grid>
                <Grid item xs={6} sm={6} md={6} ><BookStatus value={view.data?.status} /></Grid>
                <Grid item xs={6} sm={6} md={6} >No. of Users</Grid>
                <Grid item xs={6} sm={6} md={6} >{view.data?.size}
                  {(view.data?.size == 1) && <img style={{ marginRight: '5px', marginLeft: '5px', background: 'white' }} src={singleImage} width="15px" height="15px" />}
                  {(view.data?.size == 2) && <img style={{ marginRight: '5px', marginLeft: '5px', background: 'white' }} src={doubleImage} width="15px" height="15px" />}
                  {(view.data?.size >= 3) && <img style={{ marginRight: '5px', marginLeft: '5px', background: 'white' }} src={tripleImage} width="15px" height="15px" />}
                </Grid>
                {Object.keys(userProfileData).length > 0 && localStorage.getItem('type') == 'shop' && <>
                  {id == userProfileData?._id && <>
                    <Grid item xs={6} sm={6} md={6}>Reach Time</Grid>
                    <Grid item xs={6} sm={6} md={6}>{(!view.data?.end_time || view.data?.end_time == null || view.data?.end_time == undefined) ? '----' : moment(view.data?.end_time).calendar()}</Grid>
                  </>}
                </>}
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Dialog>
    </Grid>
  )
}

export default ShopQueuePage