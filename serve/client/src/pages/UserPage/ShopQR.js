import React from 'react';
import { Card, CardContent, Grid, TableContainer, Typography, Table, TableBody, TableHead, TableCell, TableRow, Paper, IconButton, Collapse, Box, Dialog, DialogTitle } from '@mui/material'
import { Close } from '@mui/icons-material';

const ShopQR = ({ image, name, handleClose }) => {

  const handleQrDown = (val, name) => {
    console.log('Image Click')
    var a = document.createElement("a"); //Create <a>
    a.href = val;
    a.download = `${name}.png`; //File name Here
    a.click();
  }

  return (
    <Grid>
      <Card>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={12}>
              <Box display="flex" alignItems="center">
                <Box flexGrow={1} >
                </Box>
                <Box>
                  <IconButton onClick={handleClose}>
                    <Close />
                  </IconButton>
                </Box>
              </Box>
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <div id="bannerQr">
                <div className="container">
                  <div className='innerPart'>
                    <div className='tilte'>
                      {name}
                    </div>
                    <div className='Clogo'>
                      COMPANY LOGO HERE
                    </div>
                    <div className='qrcodescanner' onClick={() => { handleQrDown(image, name) }}>
                      <img src={image} width="100px" />
                    </div>
                    <div className="barbar_logo">
                      <div className="logoMobile MuiBox-root css-oe0vnf"><a className="mainLogo css-144sflg" href="/"><svg enableBackground="new 0 0 512 512" id="Layer_1" version="1.1" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><g><path d="   M373.9,24l-1.2-8L162.9,391.7c-6.3,11.4-20.6,15.5-32.1,9.5c-0.2-0.1-0.3-0.2-0.5-0.3c-22.2-11.4-49.8-3.3-62.4,18.2   c-13.5,23-5.4,52.5,17.7,65.4c22.9,12.8,51.7,4.6,64.5-18.3l55.8-100c3.2-5.7,10.3-7.7,16-4.5h0c6.5,3.6,14.6,1.3,18.2-5.2   l108.4-194.2C372.1,120.3,381,71.6,373.9,24z M127.1,453.3c-5.7,10.1-18.5,13.8-28.6,8.1c-10.1-5.7-13.8-18.5-8.1-28.6   c5.7-10.1,18.5-13.8,28.6-8.1C129.2,430.4,132.8,443.2,127.1,453.3z M259.4,284.2c-6.6,0-11.9-5.3-11.9-11.9s5.3-11.9,11.9-11.9   c6.6,0,11.9,5.3,11.9,11.9S265.9,284.2,259.4,284.2z" fill="none" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" strokeWidth="10"></path><path d="   M444.6,425.4c-12.2-21.8-39.6-30.4-62-19.5c-0.2,0.1-0.3,0.2-0.5,0.3c-11.6,5.8-25.8,1.3-31.9-10.1l-53.8-102.8l-29.2,52.3   l6.4,13.6c3.5,6.5,11.6,9,18.1,5.6h0c5.7-3,12.9-0.9,15.9,4.9l53.7,101.2c12.3,23.1,41,31.9,64.1,19.6   C448.9,478,457.6,448.7,444.6,425.4z M413.1,467.2c-10.3,5.4-23,1.5-28.4-8.7c-5.4-10.3-1.5-23,8.7-28.4c10.3-5.4,23-1.5,28.4,8.7   C427.2,449,423.3,461.7,413.1,467.2z" fill="none" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" strokeWidth="10"></path><path d="   M216.8,255.2l29.2-52.4L148.5,16l-1.4,8c-8.1,47.5-0.3,96.3,22.3,138.8L216.8,255.2z" fill="none" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" strokeWidth="10"></path></g></svg><button className="MuiButtonBase-root css-1mjc1cl-MuiButtonBase-root" type="button">HeyBarber<span className="MuiTouchRipple-root css-8je8zh-MuiTouchRipple-root"></span></button></a></div>
                    </div>
                    <div className='insidetext'>
                      <h3>Scan and get in queue save your time & reach at perfect time</h3>
                      <div className="appTitle">
                        open to all online users just the app
                      </div>
                      <a href='#0'>click on Hey Barber to visit website</a>
                    </div>
                  </div>
                </div>
              </div>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default ShopQR