import React from 'react'
import { Grid, Card, CardContent, Box, Typography, IconButton } from '@mui/material'
import { Close } from '@mui/icons-material'
import './package.css';

const PackageShopFeature = ({ mainData, handleBtn }) => {

  return (
    <Grid >
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid className="creatPop" item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                    Package Shop Feature
                  </Typography>
                  <Box className="crossIcon">
                    <IconButton onClick={() => { handleBtn() }}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Grid className="course-table" item xs={12} sm={12} md={12}>

                <table>
                  <tr>
                    <th></th>
                    {mainData?.map((cc) => (
                      <td key={cc._id}>{cc.package_id.name}</td>
                    ))}
                  </tr>
                  <tr>
                    <th>{'Service'}</th>
                    {mainData?.map((cc) => (
                      <td key={cc._id}>
                        {cc.service.map((dd) => (
                          <div key={dd._id}>
                            {dd.name}
                          </div>
                        ))}
                      </td>
                    ))}
                  </tr>
                </table>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default PackageShopFeature