import React, { useEffect, useState, } from 'react'
import { TextField, Grid, Card, Typography, Dialog, DialogContent, CardContent, useTheme, FormGroup, useMediaQuery, Box, IconButton } from '@mui/material'
import '../../../pages/Registration/register.css'
import FormError from '../../../pages/Registration/FormError';
import { useSelector, useDispatch } from 'react-redux'
import NewImageCropper from 'pages/ImageCropper/NewImageCropper';
import { BookingButton } from "components/common";
import { shopProfileUpdateKYC } from 'redux/action';
import { Close } from '@mui/icons-material';

const ShopProfileKyc = ({ allvalue, handleNext }) => {

  const loading = useSelector(state => state.auth.loader)
  const mainCheckU = useSelector((state) => state.auth.created)
  const dispatch = useDispatch()

  const [data, setData] = useState({
    aadhar_link: '',
    aadharimage: allvalue.aadhar_link,
    gst_link: '',
    gstimage: allvalue.gst_link,
  })

  const [err, setErr] = useState({})
  const [image, setImage] = useState(allvalue.aadhar_link)
  const [banner, setBanner] = useState(allvalue.gst_link)
  const [open, setOpen] = useState({
    open: false,
    name: '',
    image: ''
  })


  useEffect(() => {

    if (mainCheckU == true) {
      handleNext()
    }
  }, [mainCheckU])

  const onHandleImage = (e) => {

    if (e.target.files == null || e.target.files == undefined) {
      return
    }

    const file = Math.round((e.target.files[0].size / 1024 * 1024));
    console.log('Image Size', e.target.files[0].size, 'Image Size Mb', file)

    if (file > 3145728) {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      setErr({ ...err, [e.target.name]: 'Image Size Should be less than 3 Mb' })
      return
    }

    const VIDEO_FORMATS = ["video/mp4", "video/mkv"];
    console.log('Image Console', e.target.name, e.target.files)
    console.log('Main', document.getElementsByName(e.target.name)[0].value)
    if (!VIDEO_FORMATS.includes(e.target.files[0].type)) {
      setData({ ...data, [e.target.name]: e.target.files[0] })
      setErr({ ...err, [e.target.name]: '' })
      setOpen({ ...open, open: true, name: e.target.name, image: URL.createObjectURL(e.target.files[0]) })
    } else {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      return
    }
  }


  const CrooppedImageN = (imgss) => {
    console.log('Cropped Image Called', imgss)
    var ext = (imgss.type == "image/jpeg") ? '.jpg' : (imgss.type == "image/png") ? '.png' : '.jpg'
    const values = blobToFile(imgss, open.name + ext)
    console.log('Cropped Image Called', values)
    setData({ ...data, [open.name]: values, aadharimage: URL.createObjectURL(values) })
    setOpen({ ...open, open: false })
    setImage(URL.createObjectURL(values))
  }

  const blobToFile = (theBlob, fileName) => {
    return new File([theBlob], fileName, { lastModified: new Date().getTime(), type: theBlob.type })
  }

  const handleClose = () => {
    document.getElementsByName(open.name)[0].value = ''
    setData({ ...data, [open.name]: '' })
    setOpen({ ...open, open: false })
  }

  const handleSubmit = () => {
    var check = kycValidation({ ...data })
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)

    var formData = new FormData()
    formData.append('image', data.aadhar_link)
    formData.append('aadharimage', data.aadharimage)
    formData.append('banner', data.gst_link)
    formData.append('gstimage', data.gstimage)
    dispatch(shopProfileUpdateKYC(formData))
  }

  const handleGst = (e) => {

    if (e.target.files == null || e.target.files == undefined) {
      return
    }

    const file = Math.round((e.target.files[0].size / 1024 * 1024));
    console.log('Image Size', e.target.files[0].size, 'Image Size Mb', file)
    console.log('Image Type', e.target.files[0].type, e.target.files[0].type.includes('pdf'))

    if (e.target.files[0].type.includes('pdf') == false) {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      setErr({ ...err, [e.target.name]: 'Only Pdf will be Uploaded' })
      setBanner(banner)
      return
    }

    if (file > 3145728) {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      setErr({ ...err, [e.target.name]: 'Pdf Size Should be less than 3 Mb' })
      setBanner(banner)
      return
    }

    setData({ ...data, [e.target.name]: e.target.files[0], gstimage: URL.createObjectURL(e.target.files[0]) })
    setErr({ ...err, [e.target.name]: '' })
    setBanner(URL.createObjectURL(e.target.files[0]))

  }

  return (
    <Grid>
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Box flexGrow={1} ><Typography className='title_head' variant="h5" component="div" align="center">
                    Shop Kyc
                  </Typography></Box>
                  <Box>
                    <IconButton onClick={handleNext}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField id="imageFile" type="file"
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }} name="aadhar_link" label="Aadhar Card" onChange={(e) => { onHandleImage(e) }} />

                {err.aadhar_link && (<FormError data={err.aadhar_link}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6} sx={(theme) => ({
                [theme.breakpoints.up('xs')]: {
                  textAlign: 'center'
                },
                [theme.breakpoints.up('sm')]: {
                  textAlign: 'center'
                }
              })} >
                <img src={image} className="image_tag" />
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField id="imageFile" type="file"
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }} inputProps={{ accept: '.pdf, .doc, .docx' }} name="gst_link" label="GST Certificate" onChange={(e) => { handleGst(e) }} />

                {err.gst_link && (<FormError data={err.gst_link}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6} sx={(theme) => ({
                [theme.breakpoints.up('xs')]: {
                  textAlign: 'center'
                },
                [theme.breakpoints.up('sm')]: {
                  textAlign: 'center'
                }
              })} >
                {banner !== '' && banner !== null && banner !== undefined && <object width="100%" height="400" data={banner} type="application/pdf"></object>}
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }} >
              <Grid item xs={12} align='right'>
                <BookingButton type="button" disabled={loading} variant="contained" onClick={() => { handleSubmit() }}>Update</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
      <Dialog open={open.open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description" maxWidth="md" fullWidth>
        <DialogContent>
          <NewImageCropper img={open.image} click={CrooppedImageN} ratio={16 / 9} />
        </DialogContent>
      </Dialog>
    </Grid>
  )
}

export default ShopProfileKyc

const kycValidation = (data) => {

  let errors = {}

  if (!data.aadharimage) {
    errors['aadhar_link'] = "Aadhar Card is Required";
  }

  if (!data.gstimage) {
    errors['gst_link'] = "GST Certificate is Required";
  }

  return errors
}