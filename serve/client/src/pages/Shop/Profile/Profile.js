import React, { useEffect, useState } from 'react'
import { Grid } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { shopProfileData } from 'redux/action'
import EditProfile from './EditProfile'
import TableLoader from '../UsableComponent/TableLoader'
import { tableStyles } from '../Dashboard/styles'

const Profile = () => {

  const [element, setElement] = useState({})
  const [flag, setFlag] = useState(false)
  const shopData = useSelector((state) => state.auth.profile)
  const mainVal = useSelector(state => state.auth.created)
  const dispatch = useDispatch()

  const classes = tableStyles()

  useEffect(() => {

    if (Object.keys(shopData).length == 0) {
      dispatch(shopProfileData())
    } else {
      setElement(shopData)
      setFlag(true)
    }

    if (mainVal == true) {
      dispatch(shopProfileData())
    }

  }, [shopData, mainVal])

  return (
    <Grid container spacing={2} className={classes.grid} sx={(theme) => ({
      [theme.breakpoints.up('sm')]: {
        marginBottom: '30px !important'
      },
      [theme.breakpoints.up('xs')]: {
        marginBottom: '30px !important'
      },
      [theme.breakpoints.up('md')]: {
        marginBottom: '0px !important'
      },
    })}>
      {flag ? <Grid item xs={12} sm={12} md={10} className="page-margin">
        <EditProfile allvalue={element} />
      </Grid> : <Grid item xs={12} sm={12} md={10}>
        <TableLoader />
      </Grid>}
    </Grid>
  )
}

export default Profile
