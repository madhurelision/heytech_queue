import React, { useEffect, useState } from 'react'
import { Grid, Button, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Typography, Card, CardContent, IconButton, Box, Collapse, Chip } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { getShopPackagePaymentList } from 'redux/action'
import { Refresh, KeyboardArrowDown, KeyboardArrowUp } from '@mui/icons-material'
import { LoadingButton } from '@mui/lab'
import TableLoader from 'pages/Shop/UsableComponent/TableLoader'
import NoData from 'pages/Shop/UsableComponent/NoData'
import { tableStyles } from 'pages/Shop/Dashboard/styles'
import ImageBox from 'pages/Shop/UsableComponent/ImageBox'
import TooltipBox from 'pages/Shop/UsableComponent/TooltipBox'
import MobileLoader from 'pages/Shop/UsableComponent/MobileLoader'
import 'pages/Shop/Queue/queue.css'
import moment from 'moment'
import NormalStatusBox from 'pages/Shop/UsableComponent/NormalStatusBox'

const PackagePayment = () => {

  const classes = tableStyles()
  const [element, setElement] = useState([])
  const [boxview, setBoxView] = useState('')
  const [flag, setFlag] = useState(false)
  const [view, setView] = useState('')
  const paymentData = useSelector((state) => state.auth.shopPackageList)
  const allCount = useSelector(state => state.auth.shopPackageTotal)
  const loading = useSelector((state) => state.auth.loader)
  const dispatch = useDispatch()

  useEffect(() => {

    if (paymentData.length == 0) {
      dispatch(getShopPackagePaymentList(0, 10))
    } else {
      setElement(paymentData.payment)
      setFlag(true)
    }

  }, [paymentData])

  const handleRefresh = () => {
    dispatch(getShopPackagePaymentList(0, element.length > 0 ? element.length : 10))
  }

  const fetchMoreData = () => {
    dispatch(getShopPackagePaymentList(element.length, 10))
  };

  const handleViewCard = (row) => {
    if (view == row._id) {
      setView("")
    } else {
      setView(row._id)
    }
  }

  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card>
          <CardContent>
            <Grid className="serviceListHead" container spacing={2}>
              <Grid item xs={12} sm={2} md={2} >
                <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh} >
                  Refresh
                </Button>
              </Grid>
              <Grid item xs={12} sm={8} md={8}>
                <Typography className='title_head' variant="h5" component="div" align="center">
                  Package Payment
                </Typography>
              </Grid>
              <Grid item xs={12} sm={2} md={2}></Grid>
            </Grid>
            <TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "block" } }}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Email</TableCell>
                    <TableCell>Amount</TableCell>
                    <TableCell>Payment Id</TableCell>
                    <TableCell>Payment Url</TableCell>
                    <TableCell>Payment Method</TableCell>
                    <TableCell>Status</TableCell>
                    <TableCell>Created At</TableCell>
                    <TableCell>Package Expiry</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? element.length > 0 ? element.map((item) => (
                    <React.Fragment key={item._id}>
                      <TableRow
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell>
                          {
                            boxview == item._id ? <IconButton
                              title='Close'
                              aria-label="close"
                              size="small"
                              onClick={() => setBoxView('')}
                            >
                              <KeyboardArrowUp />
                            </IconButton> : <IconButton
                              title='Open'
                              aria-label="open"
                              size="small"
                              onClick={() => setBoxView(item._id)}
                            >
                              <KeyboardArrowDown />
                            </IconButton>
                          }
                        </TableCell>
                        <TableCell>{item.email}</TableCell>
                        <TableCell>{item.Amount}</TableCell>
                        <TableCell>{item?.payment_method_id}</TableCell>
                        <TableCell>
                          <Chip href={item?.payment_url} component="a" label={"Click Me"} target="_blank" />
                        </TableCell>
                        <TableCell>{item?.payment_method}</TableCell>
                        <TableCell>
                          <NormalStatusBox label={item?.status} />
                        </TableCell>
                        <TableCell>{moment(item.createdAt).format('LLLL')}</TableCell>
                        <TableCell>{moment(item.endDate).format('LLLL')}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={12}>
                          <Collapse in={boxview == item._id} timeout="auto" unmountOnExit>
                            <Box style={{ margin: '3px' }}>
                              <Typography variant="h6" gutterBottom component="div">
                                Order Details
                              </Typography>
                              <Table size="small" aria-label="shopDetail">
                                <TableHead>
                                  <TableRow>
                                    <TableCell></TableCell>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Email</TableCell>
                                    <TableCell>Mobile</TableCell>
                                    <TableCell>Amount</TableCell>
                                  </TableRow>
                                </TableHead>
                                <TableBody>
                                  <TableRow>
                                    <TableCell></TableCell>
                                    <TableCell>{item.name}</TableCell>
                                    <TableCell>{item.email}</TableCell>
                                    <TableCell>{item.mobile}</TableCell>
                                    <TableCell>{item.Amount}</TableCell>
                                  </TableRow>
                                </TableBody>
                              </Table>
                            </Box>
                            <Box style={{ margin: '3px' }}>
                              <Typography variant="h6" gutterBottom component="div" className={classes.tableClass} >
                                Product Details
                              </Typography>
                              <Table size="small" aria-label="shopDetail">
                                <TableHead>
                                  <TableRow>
                                    <TableCell></TableCell>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Description</TableCell>
                                    <TableCell>Image</TableCell>
                                    <TableCell>Price</TableCell>
                                  </TableRow>
                                </TableHead>
                                <TableBody>
                                  <TableRow>
                                    <TableCell></TableCell>
                                    <TableCell >{item.product_id.title}</TableCell>
                                    <TableCell >
                                      <TooltipBox title="hover" data={item.product_id.description} />
                                    </TableCell>
                                    <TableCell >
                                      <ImageBox image={item.product_id.image} />
                                    </TableCell>
                                    <TableCell >{item.product_id.price}</TableCell>
                                  </TableRow>
                                </TableBody>
                              </Table>
                            </Box>
                          </Collapse>
                        </TableCell>
                      </TableRow>
                    </React.Fragment>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <TableLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            {/* Mobile View */}
            <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "none" } }} >
              <Table size="small" aria-label="simple table">
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <TableRow
                      key={row._id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell>
                        <Card className='employee-card' >
                          <Box>
                            <CardContent sx={{ paddingBottom: '0 !important' }}>
                              <Grid container spacing={1}>
                                <Grid className='employee-title' item xs={6} sm={6}>
                                  <div>Email</div>
                                  <div className='employee-body'>{row.email}</div>
                                </Grid>
                                <Grid className='employee-title' item xs={6} sm={6}>
                                  <div>Amount</div>
                                  <div className='employee-body'>{row?.Amount}</div>
                                </Grid>
                                <Grid className='employee-title' item xs={6} sm={6}>
                                  <div>Payment Id</div>
                                  <div className='employee-body'>{row?.payment_method_id} </div>
                                </Grid>
                                <Grid className='employee-title' item xs={6} sm={6}>
                                  <div>Payment Url</div>
                                  <div className='employee-body'>
                                    <Chip href={row?.payment_url} component="a" label={"Click Me"} target="_blank" />
                                  </div>
                                </Grid>
                                <Grid className="employee-title" item xs={6} sm={6}>
                                  <div>Payment Method</div>
                                  <div className="employee-body">
                                    {row?.payment_method}
                                  </div>
                                </Grid>
                                <Grid className="employee-title" item xs={6} sm={6}>
                                  <div>Status</div>
                                  <div className="employee-body">
                                    {row?.status}
                                  </div>
                                </Grid>
                              </Grid>
                            </CardContent>
                          </Box>
                        </Card>
                      </TableCell>
                    </TableRow>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <MobileLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
          </CardContent>
          {element.length >= 10 && <Grid container spacing={2}>
            {allCount != element.length && <Grid item xs={12} sm={12} md={12} textAlign="center">
              <LoadingButton loading={loading}
                loadingIndicator="Loading..."
                variant="outlined" disabled={loading} onClick={fetchMoreData} >View More</LoadingButton>
            </Grid>}
          </Grid>}
        </Card>
      </Grid>
    </Grid>
  )
}

export default PackagePayment
