import React, { useState } from 'react';
import { styled } from '@mui/material/styles';
import { TabContext, TabList, TabPanel } from '@mui/lab';
import { Tab, Box } from '@mui/material';
import { tableStyles } from 'pages/Shop/Dashboard/styles';
import Profile from './Profile';
import PackagePayment from './PackagePayment';

const StyledTab = styled((props) => <Tab disableRipple {...props} />)(
  ({ theme }) => ({
    textTransform: 'none',
    fontWeight: theme.typography.fontWeightRegular,
    fontSize: theme.typography.pxToRem(15),
    minHeight: '50px',
    marginRight: theme.spacing(1),
    color: theme.palette.mode == 'dark' ? '#f5f5f5' : 'black',
    '&.Mui-selected': {
      color: '#d6a354 !important',
    },
    '&.Mui-focusVisible': {
      backgroundColor: 'rgba(100, 95, 228, 0.32)',
    },
    '&.MuiTabs-indicator': {
      display: 'none',
      left: '271px !important'
    }
  }),
);

const ProfileTab = () => {

  const classes = tableStyles()
  const [value, setValue] = React.useState("0");

  const handleChange = (event, newValue) => {
    console.log('value', newValue)
    setValue(newValue);
  };

  return (
    <Box>
      <Box>
        <TabContext value={value}>
          <TabList
            onChange={handleChange}
            aria-label="styled tabs example"
            centered
          >
            <StyledTab className='title_head' label="Profile" value="0" iconPosition="end" />
            <StyledTab className='title_head' label="Package History" value="1" iconPosition="end" />
          </TabList>
          <TabPanel sx={{ padding: '4px !important' }} value={"0"}>
            <Profile />
          </TabPanel>
          <TabPanel sx={{ padding: '4px !important' }} value={"1"}>
            <PackagePayment />
          </TabPanel>
        </TabContext>
      </Box>
    </Box>
  );
}

export default ProfileTab