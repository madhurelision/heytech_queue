import React, { useState, useEffect, useCallback } from 'react'
import { Grid, Card, CardContent, TextField, Button, Box, IconButton, Typography } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import '../../Registration/register.css'
import { tableStyles } from 'pages/Shop/Dashboard/styles'
import { keyLengthValidation } from 'config/KeyData'
import { customerMobileCreateOtp, customerMobileVerifyOtp, shopMobileCreateOtp, shopMobileVerifyOtp } from 'redux/action'
import { BookingButton } from "components/common";
import ProgressTimer from '../UsableComponent/ProgressTimer'
import { Close, Password } from '@mui/icons-material'

const VerifyOtp = ({ allvalue, setId, status, handleView }) => {

  const [otp, setOtp] = useState('')

  const [err, setErr] = useState({})
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.created)
  const mainCheckU = useSelector((state) => state.auth.updated)
  const loading = useSelector(state => state.auth.loader)
  const [count, setCount] = useState(0)
  const [show, setShow] = useState({
    open: false,
    date: new Date().setSeconds(120)
  })

  useEffect(() => {

    if (mainCheckU) {
      setShow({ ...show, open: true, date: new Date().setSeconds(120) })
      handleView(true)
    }

  }, [mainCheckU])

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])


  const handleChange = (e) => {
    setOtp(e.target.value)
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    if (otp == null || otp == undefined || otp == "") {
      setErr({ ...err, otp: 'Otp is Required' })
      return
    }
    console.log('formData', otp)

    if (status == true) {
      dispatch(shopMobileVerifyOtp(allvalue?._id, { otp: otp }))
    } else {
      dispatch(customerMobileVerifyOtp(allvalue?._id, { otp: otp, mobile: allvalue.mobile }))
    }

  }

  const classes = tableStyles()

  const handleResendOtp = () => {
    setCount(count + 1)
    setShow({ ...show, open: true, date: new Date().setSeconds(120) })
    if (status == true) {
      dispatch(shopMobileCreateOtp(allvalue?._id, { mobile: allvalue?.mobile, resend: 'multiple' }))
    } else {
      dispatch(customerMobileCreateOtp(allvalue?._id, { mobile: allvalue?.mobile, resend: 'multiple' }))
    }
  }

  const handleCloseTimer = useCallback((val) => {
    setShow({ ...show, open: false })
  }, [show])

  return (
    <Grid>
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Typography className='title_head' flexGrow={1} variant="h5" component="div" align="center">
                    Verify Otp
                  </Typography>
                  <Box>
                    <IconButton onClick={setId}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Button sx={{ textTransform: 'capitalize' }} disabled={(loading || count == 2 || show.open)} startIcon={<Password />} onClick={() => { handleResendOtp() }} >
                Resend Otp
              </Button>
              {show.open == true && <ProgressTimer time={show.date} ma={'hello'} func={handleCloseTimer} show={false} />}
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="otp" type="text" value={otp} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} inputProps={{
                  maxLength: keyLengthValidation.otp
                }} onChange={handleChange} label="Otp" />
                {err.otp && (<FormError data={err.otp}></FormError>)}
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }}>
              <Grid item xs={12} align="right">
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Submit</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default VerifyOtp
