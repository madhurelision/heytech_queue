import React, { useEffect, useState, useCallback } from 'react'
import { TextField, Grid, Card, Chip, Typography, Dialog, DialogContent, Checkbox, FormControlLabel, CardContent, RadioGroup, Radio, FormLabel, FormControl, InputLabel, Select, MenuItem, Box, useTheme, FormGroup, useMediaQuery, Autocomplete } from '@mui/material'
import '../../../pages/Registration/register.css'
import FormError from '../../../pages/Registration/FormError';
import { useSelector, useDispatch } from 'react-redux'
import { shopMobileCreateOtp, shopProfileUpdate, shopQrcodeGenerate, getAllDescriptionTag, getAllShopCategory, openStatusUpdate, getAllExpertiseName } from 'redux/action';
import Geocode from 'react-geocode';
import { geocodeByPlaceId, getLatLng } from 'react-google-places-autocomplete';
import Location from '../UsableComponent/Location';
import SelectDataComp from '../UsableComponent/SelectDataComp';
import { keyLengthValidation, Languages, Seat } from 'config/KeyData';
import NewImageCropper from 'pages/ImageCropper/NewImageCropper';
import VerifyOtp from './VerifyOtp';
import { BookingButton } from "components/common";
import SwitchCompnent from '../UsableComponent/SwitchComponent';
import ShopProfileKyc from './ShopProfileKyc';
Geocode.setApiKey('AIzaSyCqiCKV0cm-EpHp2hev4XkD5wG9JGxFbVs');
Geocode.setLocationType("ROOFTOP");
Geocode.enableDebug();

const EditProfile = ({ allvalue }) => {

  const theme = useTheme()
  const matches = useMediaQuery(theme.breakpoints.up('md'));
  const mainCheckU = useSelector((state) => state.auth.updated)
  const loading = useSelector(state => state.auth.loader)
  const dispatch = useDispatch()

  const [data, setData] = useState({
    first_name: allvalue.first_name,
    last_name: allvalue.last_name,
    shop_name: allvalue.shop_name,
    image_link: '',
    email: allvalue.email,
    company: allvalue.company,
    description: allvalue.description,
    expertise: allvalue.expertise,
    language: allvalue.language,
    //topics: allvalue.topics,
    instagram: allvalue.instagram,
    time_slot: {
      monday: {
        start_hour: allvalue.time_slot?.monday?.start_hour,
        end_hour: allvalue.time_slot?.monday?.end_hour,
        available: allvalue.time_slot?.monday?.available
      },
      tuesday: {
        start_hour: allvalue.time_slot?.tuesday?.start_hour,
        end_hour: allvalue.time_slot?.tuesday?.end_hour,
        available: allvalue.time_slot?.tuesday?.available
      },
      wednesday: {
        start_hour: allvalue.time_slot?.wednesday?.start_hour,
        end_hour: allvalue.time_slot?.wednesday?.end_hour,
        available: allvalue.time_slot?.wednesday?.available
      },
      thursday: {
        start_hour: allvalue.time_slot?.thursday?.start_hour,
        end_hour: allvalue.time_slot?.thursday?.end_hour,
        available: allvalue.time_slot?.thursday?.available
      },
      friday: {
        start_hour: allvalue.time_slot?.friday?.start_hour,
        end_hour: allvalue.time_slot?.friday?.end_hour,
        available: allvalue.time_slot?.friday?.available
      },
      saturday: {
        start_hour: allvalue.time_slot?.saturday?.start_hour,
        end_hour: allvalue.time_slot?.saturday?.end_hour,
        available: allvalue.time_slot?.saturday?.available
      },
      sunday: {
        start_hour: allvalue.time_slot?.sunday?.start_hour,
        end_hour: allvalue.time_slot?.sunday?.end_hour,
        available: allvalue.time_slot?.sunday?.available
      },
    },
    waiting_number: allvalue.waiting_number,
    waiting_key: allvalue.waiting_key,
    waiting_time: allvalue.waiting_time,
    seat: allvalue.seat,
    oldimage: allvalue.image_link,
    location: allvalue.location,
    lattitude: allvalue.lattitude,
    longitude: allvalue.longitude,
    type: allvalue.type,
    open: allvalue.open,
    banner_link: '',
    bannerimage: allvalue.banner_link,
    time_zone: allvalue.time_zone,
    time_val: allvalue.time_val,
    upi_id: allvalue.upi_id
  })

  const [err, setErr] = useState({})
  const [btn, setBtn] = useState(true)
  const [view, setView] = useState(false)
  const [kyc, setKyc] = useState(false)
  const [show, setShow] = useState(false)
  const [mb, setMb] = useState(true)
  const [crop, setCrop] = useState(true)
  const [expertiseId, setExpertise] = useState('')
  const [element, setElement] = useState('')
  const [mobile, setMobile] = useState(allvalue.mobile)
  const [image, setImage] = useState(allvalue.image_link)
  const [banner, setBanner] = useState(allvalue.banner_link)
  const [open, setOpen] = useState({
    open: false,
    name: '',
    image: ''
  })
  const [tag, setTag] = useState([])
  const [category, setCategory] = useState([])
  const descriptionTagD = useSelector(state => state.auth.descriptionTagList)
  const shopCateData = useSelector((state) => state.auth.shopCategoryList)
  const [serveName, setServeName] = useState([])
  const expertiseNameD = useSelector(state => state.auth.expertiseNameList)


  useEffect(() => {

    if (expertiseNameD.length == 0) {
      dispatch(getAllExpertiseName())
    } else {
      setServeName(expertiseNameD.expertiseName)
    }

  }, [expertiseNameD])

  useEffect(() => {

    if (descriptionTagD.length == 0) {
      dispatch(getAllDescriptionTag())
    } else {
      setTag(descriptionTagD.tag)
    }

  }, [descriptionTagD])

  useEffect(() => {

    if (shopCateData.length == 0) {
      dispatch(getAllShopCategory())
    } else {
      setCategory(shopCateData.category)
    }

  }, [shopCateData])

  useEffect(() => {
    console.log('Hellp Updated', mainCheckU)
    if (mainCheckU == true) {
      console.log('Hellp Updated', mainCheckU)
      setView(true)
    }

  }, [mainCheckU])

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
    setBtn(false)
  }

  const addExpertise = () => {
    const check = checkExpertise({ name: element })
    setErr({ ...err, ...check })
    if (Object.keys(check).length > 0) return
    var main = [...data.expertise]
    var checked = main.filter((cc) => cc == element)

    if (checked.length > 0) {
      setElement('')
    } else {
      main.push(element)
      setData({ ...data, expertise: main })
      setErr({ ...err, expertise: '' })
      setElement('')
      setBtn(false)
    }
  }

  const handleEditChange = (e) => {
    if (expertiseId !== '') {
      var atV = [...data.expertise]
      console.log('val', atV[expertiseId])
      atV[expertiseId] = e.target.value
      setElement(e.target.value)
      setData({ ...data, expertise: atV })
      setBtn(false)
    } else {
      setElement(e.target.value)
      setErr({ ...err, name: '' })
    }
  }

  const handleRemove = (id) => {
    var arr = [...data.expertise]
    arr = arr.filter((cc, i) => i !== id)
    setData({ ...data, expertise: arr })
    setBtn(false)
  }

  const addNewExpet = () => {
    setElement('')
    setExpertise('')
  }

  const handleEdit = (val) => {
    setExpertise(val.i)
    setElement(val.value)
  }

  const onHandleImage = (e) => {

    if (e.target.files == null || e.target.files == undefined) {
      return
    }

    const file = Math.round((e.target.files[0].size / 1024 * 1024));
    console.log('Image Size', e.target.files[0].size, 'Image Size Mb', file)

    if (file > 5242880) {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      setErr({ ...err, [e.target.name]: 'Image Size Should be less than 5 Mb' })
      return
    }

    const VIDEO_FORMATS = ["video/mp4", "video/mkv"];
    console.log('Image Console', e.target.name, e.target.files)
    console.log('Main', document.getElementsByName(e.target.name)[0].value)
    if (!VIDEO_FORMATS.includes(e.target.files[0].type)) {
      setData({ ...data, [e.target.name]: e.target.files[0] })
      setErr({ ...err, [e.target.name]: '' })
      if (e.target.name == 'image_link') {
        setCrop(true)
      } else {
        setCrop(false)
      }
      setOpen({ ...open, open: true, name: e.target.name, image: URL.createObjectURL(e.target.files[0]) })
      setBtn(false)
    } else {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      setCrop(true)
      return
    }
  }


  const CrooppedImageN = (imgss) => {
    console.log('Cropped Image Called', imgss)
    var ext = (imgss.type == "image/jpeg") ? '.jpg' : (imgss.type == "image/png") ? '.png' : '.jpg'
    const values = blobToFile(imgss, open.name + ext)
    console.log('Cropped Image Called', values)
    if (open.name == 'image_link') {
      setData({ ...data, [open.name]: values, oldimage: URL.createObjectURL(values) })
      setOpen({ ...open, open: false })
      setImage(URL.createObjectURL(values))
    } else {
      setData({ ...data, [open.name]: values, bannerimage: URL.createObjectURL(values) })
      setOpen({ ...open, open: false })
      setBanner(URL.createObjectURL(values))
    }
  }

  const blobToFile = (theBlob, fileName) => {
    return new File([theBlob], fileName, { lastModified: new Date().getTime(), type: theBlob.type })
  }

  const handleClose = () => {
    document.getElementsByName(open.name)[0].value = ''
    setData({ ...data, [open.name]: '' })
    setOpen({ ...open, open: false })
  }

  const handleStartData = (e) => {
    console.log('Data, ', e.target.name, 'value', e.target.value)
    setData({
      ...data,
      time_slot: { ...data.time_slot, [e.target.id]: { ...data.time_slot[e.target.id], [e.target.name]: e.target.value } },
      time_zone: new Date().getTimezoneOffset(),
      time_val: window.Intl.DateTimeFormat().resolvedOptions().timeZone
    })
    // setData({
    //   ...data,
    //   time_zone: new Date().getTimezoneOffset(),
    //   time_val: window.Intl.DateTimeFormat().resolvedOptions().timeZone
    // })
    setBtn(false)
  }

  const handleEndData = (e) => {
    console.log('Data, ', e.target.name, 'value', e.target.value)
    setData({
      ...data,
      time_slot: { ...data.time_slot, [e.target.id]: { ...data.time_slot[e.target.id], [e.target.name]: e.target.value } },
      time_zone: new Date().getTimezoneOffset(),
      time_val: window.Intl.DateTimeFormat().resolvedOptions().timeZone
    })
    // setData({
    //   ...data,
    //   time_zone: new Date().getTimezoneOffset(),
    //   time_val: window.Intl.DateTimeFormat().resolvedOptions().timeZone
    // })
    setBtn(false)
  }

  const handleCheck = (e) => {
    if (e.target.checked) {
      setData({ ...data, time_slot: { ...data.time_slot, [e.target.name]: { ...data.time_slot[e.target.name], available: e.target.checked } } })
      setErr({ ...err, time_slot: '' })
      setBtn(false)
    } else {
      setData({ ...data, time_slot: { ...data.time_slot, [e.target.name]: { ...data.time_slot[e.target.name], available: e.target.checked } } })
      setErr({ ...err, time_slot: '' })
      setBtn(false)
    }
  }


  const handleSubmit = () => {
    var check = registrationValidation({ ...data })
    setErr(check)

    if (Object.keys(check).length > 0) {
      const hey = Object.keys(check)
      scroll(hey[0])
      return
    }
    console.log('formData', data)

    var formData = new FormData()
    formData.append('first_name', data.first_name)
    formData.append('last_name', data.last_name)
    formData.append('shop_name', data.shop_name)
    formData.append('email', data.email)
    formData.append('company', data.company)
    formData.append('description', data.description)
    formData.append('instagram', data.instagram)
    formData.append('image', data.image_link)
    formData.append('oldimage', data.oldimage)
    formData.append('banner', data.banner_link)
    formData.append('bannerimage', data.bannerimage)
    data.expertise.map((cc) => {
      formData.append('expertise', cc)
    })

    data.language.map((cc) => {
      formData.append('language', cc)
    })

    formData.append('time_slot', JSON.stringify(data.time_slot))
    formData.append('waiting_number', data.waiting_number)
    formData.append('waiting_key', data.waiting_key)
    formData.append('waiting_time', data.waiting_number + ' ' + data.waiting_key)
    formData.append('location', data.location)
    formData.append('lattitude', data.lattitude)
    formData.append('longitude', data.longitude)
    formData.append('seat', data.seat)
    formData.append('mobile', data.mobile)
    formData.append('type', data.type)
    formData.append('time_val', data.time_val)
    formData.append('time_zone', data.time_zone)
    formData.append('upi_id', data.upi_id)

    dispatch(shopProfileUpdate(formData))
  }

  const handleGenerate = () => {
    dispatch(shopQrcodeGenerate())
  }

  const handleBoxLocation = useCallback((val) => {
    geocodeByPlaceId(val.value.place_id).then((ss) => {
      getLatLng(ss[0]).then((dd) => {
        console.log('Location seleted', { name: val.label, latitude: dd.lat, longitude: dd.lng })
        setData({ ...data, location: val.label, longitude: dd.lng, lattitude: dd.lat })
        setErr({ ...err, location: '' })
        setBtn(false)
      })
    }).catch((err) => {
      console.log('Location SS Data Errorr', err)
    })
  }, [data])

  const handleLiveLocation = useCallback(() => {
    if ("geolocation" in navigator) {
      console.log("Available");
    } else {
      console.log("Not Available");
    }

    navigator.geolocation.getCurrentPosition(function (position) {
      Geocode.fromLatLng(position.coords.latitude, position.coords.longitude).then(
        (response) => {
          const address = response.results[0].formatted_address;
          console.log(address, 'latitude', position.coords.latitude, 'longitude', position.coords.longitude);
          setData({ ...data, location: address, longitude: position.coords.longitude, lattitude: position.coords.latitude })
          setErr({ ...err, location: '' })
          setBtn(false)
        },
        (error) => {
          console.error(error);
        }
      );
    },
      function (error) {
        console.error("Error Code = " + error.code + " - " + error.message);
      });
  }, [data])

  const handleLanguageChange = (event) => {
    const {
      target: { value },
    } = event;
    setData({ ...data, language: typeof value === 'string' ? value.split(',') : value });
    setErr({ ...err, language: '' })
    setBtn(false)
  };

  const handleTagBtn = (item) => {
    setData({ ...data, description: data.description.length == 0 ? data.description.concat(item) : data.description.concat(' ', item) })
    setErr({ ...err, description: '' })
    setBtn(false)
  }

  const handleMobile = (e) => {
    setMobile(e.target.value)
    setErr({ ...err, [e.target.name]: '' })
    setMb(false)
  }

  const AddMobile = () => {

    var check = checkMobile({ mobile: mobile })
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formmobile', mobile)

    if (allvalue.mobile == mobile) {
      console.log('mobile match', allvalue.mobile, mobile)
      setMb(true)
      return
    }
    dispatch(shopMobileCreateOtp(allvalue?.user_information, { mobile: mobile, resend: 'single' }))
  }

  const handleViewClose = () => {
    setView(false)
  }

  const handleChangeStatus = (e, id) => {
    if (allvalue.topics.length == 0 && e.target.checked == true) {

    } else {
      setData({ ...data, open: e.target.checked })
      dispatch(openStatusUpdate(id, { open: e.target.checked }))
    }
  }

  const handleKyc = () => {
    setKyc(false)
  }

  const handleShow = (val) => {
    setShow(val)
  }

  const scroll = (val) => {
    const section = document.querySelector(`#${val}`);
    section.scrollIntoView({ behavior: 'smooth', block: 'start' });
  };

  return (
    <Grid>
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              {/* {(allvalue.upi_id == null && allvalue.upi_id == undefined) && <Grid item xs={12} md={12} sm={12}>
                <div className="shop-queue-tab text-danger fs-3 fw-bold">
                  Upi Id is Required Please Add in Profile
                </div>
              </Grid>} */}
              <Grid item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center" flexDirection={matches ? 'row' : 'column'}>
                  <Box flexGrow={1}>
                    <Typography className='title_head' variant="h4" component="h2" align="center">
                      Edit Profile
                    </Typography>
                  </Box>
                  <Box>
                    <Typography variant="h6" component="h6" align="center">
                      Shop Status
                    </Typography>
                    <FormGroup>
                      <SwitchCompnent label="Open" value={data.open} handleChange={handleChangeStatus} row={allvalue._id} />
                    </FormGroup>
                  </Box>
                </Box>
              </Grid>

              <Grid item xs={12} sm={12} md={12}>
                <Typography variant="h6" component="h6" align="left">
                  General Information
                </Typography>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  type="text"
                  value={data.shop_name}
                  label="Shop Name"
                  name="shop_name"
                  id="shop_name"
                  onChange={handleChange}
                />
                {err.shop_name && (<FormError data={err.shop_name}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  type="email"
                  value={data.email}
                  label="Email"
                  name="email"
                  id="email"
                  disabled
                  onChange={handleChange}
                />
                {err.email && (<FormError data={err.email}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  type="text"
                  value={data.upi_id}
                  label="Upi Id"
                  name="upi_id"
                  id="upi_id"
                  onChange={handleChange}
                />
                {err.upi_id && (<FormError data={err.upi_id}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  type="text"
                  value={data.company}
                  label="Company"
                  name="company"
                  id="company"
                  onChange={handleChange}
                />
                {err.company && (<FormError data={err.company}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  value={data.instagram}
                  label="Intagram Link"
                  name="instagram"
                  id="instagram"
                  onChange={handleChange}
                />
                {err.instagram && (<FormError data={err.instagram}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6} id="language">
                <SelectDataComp title={"Languages"} data={Languages} value={data.language} handleChange={handleLanguageChange} />
                {err.language && (<FormError data={err.language}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <TextField name="location" id="location" type="text" value={data.location} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} disabled label="Location" />
                {err.location && (<FormError data={err.location}></FormError>)}
              </Grid>
              <Location handleLocation={handleLiveLocation} handleDropdown={handleBoxLocation} />
              <Grid item xs={12} sm={12} md={12}>
                <Typography variant="h6" component="h6" align="left">
                  Shop Information
                </Typography>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">Seat</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    value={data.seat}
                    label="Seat"
                    name="seat"
                    id="seat"
                    onChange={handleChange}
                  >
                    {Seat().map((cc) => {
                      return (
                        <MenuItem key={cc} value={cc}>{cc}</MenuItem>
                      )
                    })}
                  </Select>
                </FormControl>
                {err.seat && (<FormError data={err.seat}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <TextField name="mobile" id="mobile" type="tel" inputProps={{
                  maxLength: keyLengthValidation.mobile
                }} value={mobile} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleMobile} label="Mobile Number" />
                {err.mobile && (<FormError data={err.mobile}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={2}>
                <BookingButton type="button" variant="contained" disabled={(mobile == null || mobile == undefined || mobile == '' || mobile.length < 10 || mb)} onClick={() => { AddMobile() }} fullWidth >Add Mobile</BookingButton>
              </Grid>
              <Grid item xs={12} sm={12} md={5}>
                <TextField name="waiting_number" id="waiting_number" type="number" inputProps={{ min: "0", step: "1" }} value={data.waiting_number} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Minimum Waiting Time" />
                {err.waiting_number && (<FormError data={err.waiting_number}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={2}>
                <FormControl component="fieldset" fullWidth>
                  <FormLabel component="legend">Waiting Time</FormLabel>
                  <RadioGroup row aria-label="waitingtime" name="waiting_key" id="waiting_key" onChange={handleChange} value={data.waiting_key}>
                    <FormControlLabel value="min" control={<Radio />} label="Min" />
                    <FormControlLabel value="hour" control={<Radio />} label="Hour" />
                  </RadioGroup>
                </FormControl>
                {err.waiting_key && (<FormError data={err.waiting_key}></FormError>)}
              </Grid>

              <Grid item xs={12} sm={12} md={5}>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">Category</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    value={data.type}
                    label="Category"
                    name="type"
                    id="type"
                    onChange={handleChange}
                  >
                    {category.map((cc) => {
                      return (
                        <MenuItem style={{ textTransform: 'capitalize' }} key={cc._id} value={cc._id}>{cc.name}</MenuItem>
                      )
                    })}
                  </Select>
                </FormControl>
                {err.type && (<FormError data={err.type}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={12} id="expertise">
                {
                  (expertiseId !== '') && <BookingButton type="button" onClick={() => { addNewExpet() }} >Add Expertise</BookingButton>
                }
                {
                  data.expertise.length === 0 ? "No Expertise Found" : data.expertise.map((cc, i) => <Chip key={i} label={cc} className="shop-profile-expert-chip" onClick={() => { handleEdit({ i: i, value: cc }) }} onDelete={() => { handleRemove(i) }} />)
                }

                {err.expertise && (<FormError data={err.expertise}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                {/* <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  type="text"
                  value={element}
                  label="Expertise"
                  onChange={handleEditChange}
                /> */}
                <Autocomplete
                  options={serveName}
                  noOptionsText="Enter to create a new option"
                  getOptionLabel={(option) => option.name}
                  onInputChange={(e, newValue) => {
                    handleEditChange({ target: { value: newValue } })
                  }}
                  value={{ name: element }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label="Expertise"
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true
                      }}
                      onKeyDown={(e) => {
                        if (
                          e.key === "Enter" &&
                          serveName.findIndex((o) => o.name === element) === -1
                        ) {
                          setServeName((o) => o.concat({ name: element }));
                        }
                      }}
                    />
                  )}
                />
                {err.name && (<FormError data={err.name}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <BookingButton type="button" variant="contained" disabled={(expertiseId !== '' || element == '')} onClick={() => { addExpertise() }} fullWidth >Add</BookingButton>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Typography variant="body1" className="shop-image-title" component="div" align="left">
                  Description
                </Typography>
                <textarea name="description" id="description" value={data.description} cols="60" rows="10" onChange={handleChange} className={`description ${theme.palette.mode == 'dark' ? 'description-dark' : 'description-light'}`} ></textarea>
                {err.description && (<FormError data={err.description}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6} sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }}>
                {tag.length > 0 && <p className='shop-hint-title' >**Description Template</p>}
                {tag.length > 0 && <Grid container spacing={1}>
                  {tag.map((cc) => {
                    return (
                      <Grid item xs={12} key={cc._id}>
                        <Chip size="small" sx={{
                          height: '100%',
                          '.MuiChip-label': {
                            overflowWrap: 'break-word',
                            whiteSpace: 'normal',
                            textOverflow: 'clip'
                          }
                        }} onClick={() => { handleTagBtn(cc.type) }} label={cc.name} variant="outlined"></Chip>
                      </Grid>
                    )
                  })}
                </Grid>}
              </Grid>

              <Grid item xs={12} sm={12} md={4}>
                <Typography variant="body1" className="shop-image-title" component="div" align="left">
                  Qr Code
                </Typography>
                <BookingButton type="button" disabled={loading} variant="contained" onClick={() => { handleGenerate() }}>Generate QrCode</BookingButton>
              </Grid>
              <Grid item xs={12} sm={12} md={2} sx={(theme) => ({
                [theme.breakpoints.up('xs')]: {
                  textAlign: 'center'
                },
                [theme.breakpoints.up('sm')]: {
                  textAlign: 'center'
                }
              })}>
                <img src={allvalue.code} />
              </Grid>

              <Grid item xs={12} sm={12} md={12} sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
                {tag.length > 0 && <p className='shop-hint-title' >**Description Template</p>}
                {tag.length > 0 && <>
                  {tag.map((cc) => {
                    return (
                      <Chip key={cc._id} size="small" onClick={() => { handleTagBtn(cc.type) }} label={cc.name} variant="outlined"></Chip>
                    )
                  })}
                </>}
              </Grid>

              <Grid item xs={12} sm={12} md={12}>
                <Typography variant="h6" component="h6" align="left">
                  Profile Images
                </Typography>
              </Grid>
              <Grid item xs={12} sm={12} md={3} id="image_link">
                <TextField id="imageFile" type="file"
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }} name="image_link" label="Profile Image" onChange={(e) => { onHandleImage(e) }} />

                {err.image_link && (<FormError data={err.image_link}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={3} sx={(theme) => ({
                [theme.breakpoints.up('xs')]: {
                  textAlign: 'center'
                },
                [theme.breakpoints.up('sm')]: {
                  textAlign: 'center'
                }
              })} >
                <img src={image} className="image_tag" />
              </Grid>
              <Grid item xs={12} sm={12} md={3} id="banner_link">
                <TextField id="imageFile" type="file"
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }} name="banner_link" label="Profile Banner Image" onChange={(e) => { onHandleImage(e) }} />

                {err.banner_link && (<FormError data={err.banner_link}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={3} sx={(theme) => ({
                [theme.breakpoints.up('xs')]: {
                  textAlign: 'center'
                },
                [theme.breakpoints.up('sm')]: {
                  textAlign: 'center'
                }
              })} >
                <img src={banner} className="image_tag" />
              </Grid>
              {/* <Grid item xs={12} sm={12} md={12}>
                <Typography variant="h6" component="h6" align="left">
                  Shop KYC
                </Typography><BookingButton type="button" disabled={loading} variant="contained" onClick={() => { setKyc(true) }}>Shop Kyc</BookingButton>
              </Grid> */}
              <Grid item xs={12} sm={12} md={12} id="time_slot">
                <Typography variant="h6" component="h6" align="left">
                  Time Slot (24 Hour Format)
                </Typography>
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <FormControlLabel name="monday" control={<Checkbox checked={data.time_slot.monday.available} />} label="Monday" onChange={handleCheck} />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.monday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.monday.start_hour}
                  label="Start Time"
                  id="monday"
                  name="start_hour"
                  onChange={handleStartData}

                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.monday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.monday.end_hour}
                  label="End Time"
                  id="monday"
                  name="end_hour"
                  onChange={handleEndData}

                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <FormControlLabel name="tuesday" control={<Checkbox checked={data.time_slot.tuesday.available} />} label="Tuesday" onChange={handleCheck} />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.tuesday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.tuesday.start_hour}
                  label="Start Time"
                  id="tuesday"
                  name="start_hour"
                  onChange={handleStartData}

                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.tuesday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.tuesday.end_hour}
                  label="End Time"
                  id="tuesday"
                  name="end_hour"
                  onChange={handleEndData}

                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <FormControlLabel name="wednesday" control={<Checkbox checked={data.time_slot.wednesday.available} />} label="Wednesday" onChange={handleCheck} />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.wednesday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.wednesday.start_hour}
                  label="Start Time"
                  id="wednesday"
                  name="start_hour"
                  onChange={handleStartData}

                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.wednesday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.wednesday.end_hour}
                  label="End Time"
                  id="wednesday"
                  name="end_hour"
                  onChange={handleEndData}

                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <FormControlLabel name="thursday" control={<Checkbox checked={data.time_slot.thursday.available} />} label="Thursday" onChange={handleCheck} />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.thursday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.thursday.start_hour}
                  label="Start Time"
                  id="thursday"
                  name="start_hour"
                  onChange={handleStartData}

                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.thursday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.thursday.end_hour}
                  label="End Time"
                  id="thursday"
                  name="end_hour"
                  onChange={handleEndData}

                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <FormControlLabel name="friday" control={<Checkbox checked={data.time_slot.friday.available} />} label="Friday" onChange={handleCheck} />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.friday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.friday.start_hour}
                  label="Start Time"
                  id="friday"
                  name="start_hour"
                  onChange={handleStartData}

                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.friday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.friday.end_hour}
                  label="End Time"
                  id="friday"
                  name="end_hour"
                  onChange={handleEndData}

                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <FormControlLabel name="saturday" control={<Checkbox checked={data.time_slot.saturday.available} />} label="Saturday" onChange={handleCheck} />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.saturday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.saturday.start_hour}
                  label="Start Time"
                  id="saturday"
                  name="start_hour"
                  onChange={handleStartData}

                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.saturday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.saturday.end_hour}
                  label="End Time"
                  id="saturday"
                  name="end_hour"
                  onChange={handleEndData}

                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <FormControlLabel name="sunday" control={<Checkbox checked={data.time_slot.sunday.available} />} label="Sunday" onChange={handleCheck} />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.sunday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.sunday.start_hour}
                  label="Start Time"
                  id="sunday"
                  name="start_hour"
                  onChange={handleStartData}

                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              <Grid item xs={4} sm={4} md={4}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  disabled={data.time_slot.sunday.available == true ? false : true}
                  type="number"
                  value={data.time_slot.sunday.end_hour}
                  label="End Time"
                  id="sunday"
                  name="end_hour"
                  onChange={handleEndData}

                  inputProps={{
                    min: 0, step: 1, max: 24
                  }}
                />
              </Grid>
              {err.time_slot && (<FormError data={err.time_slot}></FormError>)}
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }} >
              <Grid item xs={12} align={matches ? 'right' : 'center'}>
                <BookingButton type="button" disabled={(loading || btn == true) ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Update</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
      <Dialog open={open.open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description" maxWidth="md" fullWidth>
        <DialogContent>
          <NewImageCropper img={open.image} click={CrooppedImageN} ratio={crop == true ? 9 / 16 : 16 / 9} />
        </DialogContent>
      </Dialog>
      <Dialog open={view}
        disableEscapeKeyDown={true}
        onClose={handleViewClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description" maxWidth="md" fullWidth>
        <VerifyOtp allvalue={{ _id: allvalue?.user_information, mobile: mobile }} setId={handleViewClose} status={true} handleView={handleShow} />
      </Dialog>
      <Dialog open={kyc}
        onClose={handleKyc}
        disableEscapeKeyDown={true}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description" maxWidth="md" fullWidth>
        <ShopProfileKyc allvalue={allvalue} handleNext={handleKyc} />
      </Dialog>
    </Grid>
  )
}

export default EditProfile

const registrationValidation = (data) => {

  let errors = {}

  if (!data.oldimage) {
    errors['image_link'] = "Profile Picture is Required";
  }

  if (!data.shop_name) {
    errors['shop_name'] = "Shop Name is Required";
  }

  // if (!data.last_name) {
  //   errors['last_name'] = "Last Name is Required";
  // }

  if (!data.email) {
    errors['email'] = "Email is Required";
  }

  if (!data.description) {
    errors['description'] = "Description is Required";
  }

  if (!data.company) {
    errors['company'] = "Company is Required";
  }

  // if (!data.instagram) {
  //   errors['instagram'] = "Instagram is Required";
  // }

  if (!data.language) {
    errors['language'] = "Language is Required";
  }

  if (data.language) {
    if (data.language.length == 0) {
      errors['language'] = "Language is Required";
    }
  }

  // if (!data.topics) {
  //   errors['topics'] = "Services is Required";
  // }

  // if (data.topics) {
  //   if (data.topics.length == 0) {
  //     errors['topics'] = "Services is Required";
  //   }
  //   if (data.topics.length > 0 && data.check.length == 0) {
  //     errors['topics'] = "Please Select Own Service or clone it or create it from services Tab";
  //   }
  // }

  if (!data.expertise) {
    errors['expertise'] = "Expertise is Required";
  }

  if (data.expertise) {
    if (data.expertise.length == 0) {
      errors['expertise'] = "Expertise is Required";
    }
  }

  if (data.time_slot.monday.available == false && data.time_slot.tuesday.available == false && data.time_slot.wednesday.available == false && data.time_slot.thursday.available == false && data.time_slot.friday.available == false && data.time_slot.saturday.available == false && data.time_slot.sunday.available == false) {
    errors['time_slot'] = "Minimum one time slot is Required";
  }

  if (!data.waiting_number) {
    errors["waiting_number"] = "Waiting Time is Required"
  }
  if (!data.waiting_key) {
    errors["waiting_key"] = "Waiting Key is Required"
  }

  if (!data.location) {
    errors["location"] = "Location is Required"
  }

  if (!data.seat) {
    errors["seat"] = "Seat is Required"
  }

  if (!data.type) {
    errors["type"] = "Category is Required"
  }

  if (!data.upi_id) {
    errors["upi_id"] = "Upi Id is Required"
  }

  return errors
}

const checkExpertise = (data) => {
  let errors = {}
  if (!data.name) {
    errors['name'] = "Name is Required";
  }


  return errors
}

const checkMobile = (data) => {
  let errors = {}

  if (!data.mobile) {
    errors['mobile'] = "Mobile Number is Required";
  }

  if (data.mobile) {
    if (!data["mobile"].match(/^[0-9]{10}$/)) {
      errors["mobile"] = "Please enter valid mobile no.";
    }
  }
  return errors
}