import React, { useState } from 'react'
import EmployeeList from './EmployeeList';
import CreateEmployee from './CreateEmployee';

const EmployeePage = () => {

  const [id, setId] = useState(0)

  switch (id) {
    case 0:
      return <EmployeeList setId={setId} />
    case 1:
      return <CreateEmployee setId={setId} />
    default:
      return <EmployeeList setId={setId} />
  }
}

export default EmployeePage
