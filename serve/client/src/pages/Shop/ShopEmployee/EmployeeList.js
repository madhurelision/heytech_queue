import React, { useEffect, useState } from 'react'
import { Grid, Button, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Typography, Card, CardContent, IconButton, Dialog, DialogTitle, DialogContent, Box, FormGroup, DialogActions } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { employeeDelete, employeeStatusUpdate, getShopEmployee } from 'redux/action'
import { AddCircleOutline, Close, Edit, Refresh, Delete, MoreVert } from '@mui/icons-material'
import TableLoader from '../UsableComponent/TableLoader'
import NoData from '../UsableComponent/NoData'
import { tableStyles } from '../Dashboard/styles'
import SwitchCompnent from '../UsableComponent/SwitchComponent'
import EditEmployee from './EditEmployee'
import { BookingButton } from "components/common";
import MobileLoader from '../UsableComponent/MobileLoader'
import '../Queue/queue.css'
import MainCalendar from '../UsableComponent/CustomCalendar/MainCalendar'

const EmployeeList = ({ setId }) => {

  const classes = tableStyles()
  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const [view, setView] = useState('')
  const employeeData = useSelector((state) => state.auth.shopEmployee)
  const cted = useSelector((state) => state.auth.created)
  const loading = useSelector((state) => state.auth.loader)
  const dispatch = useDispatch()
  const [data, setData] = useState({
    open: false,
    name: '',
    type: '',
    mobile: '',
    _id: ''
  })

  const [cancel, setCancel] = useState({
    open: false,
    _id: ''
  })

  const [expert, setExpert] = useState({
    open: false,
    data: {}
  })

  useEffect(() => {

    if (employeeData.length == 0) {
      dispatch(getShopEmployee())
    } else {
      setElement(employeeData.employee)
      setFlag(true)
    }

    if (cted) {
      setCancel({ ...cancel, open: false })
      dispatch(getShopEmployee())
    }

  }, [employeeData, cted])

  const handleEdit = (val) => {
    setData({
      ...data,
      open: true,
      name: val.name,
      type: val.type,
      mobile: val.mobile,
      _id: val._id
    })
  }

  const handleClose = () => {
    setData({ ...data, open: false })
  }

  const handleRefresh = () => {
    dispatch(getShopEmployee())
  }


  const handleChangeStatus = (e, id) => {
    console.log('ket', e.target.checked, 'id', id)
    dispatch(employeeStatusUpdate(id, { status: e.target.checked }))
  }

  const handleDelete = (id) => {
    dispatch(employeeDelete(id))
  }

  const handleDeleteOpen = (item) => {
    setCancel({
      ...cancel,
      open: true,
      _id: item
    })
  }

  const handleDeleteClose = () => {
    setCancel({ ...cancel, open: false })
  }

  const handleViewOpen = (item) => {
    setExpert({
      ...expert,
      open: true,
      data: item
    })
  }

  const handleViewClose = () => {
    setExpert({ ...expert, open: false })
  }

  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card>
          <CardContent>
            <Grid container spacing={2} sx={{ marginBottom: '20px' }}>
              <Grid item xs={12} sm={2} md={2} >
                <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh} >
                  Refresh
                </Button>
              </Grid>
              <Grid item xs={12} sm={8} md={8} >
                <Typography className='title_head' variant="h5" component="div" align="center">
                  Employee List
                </Typography>
              </Grid>
              <Grid item xs={12} sm={2} md={2} >
                <Button className='customer-btn' startIcon={<AddCircleOutline color='primary' />} onClick={() => { setId(1) }} >
                  Create
                </Button>
              </Grid>
            </Grid>
            <TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "block" } }} >
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>Type</TableCell>
                    <TableCell>Number</TableCell>
                    <TableCell>Employee Id</TableCell>
                    <TableCell>Status</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <TableRow
                      key={row._id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell>
                        <IconButton title="Details" onClick={() => { handleViewOpen(row) }} >
                          <MoreVert color="info" />
                        </IconButton>
                        <IconButton title="Edit" onClick={() => { handleEdit(row) }}>
                          <Edit color='primary' />
                        </IconButton>
                        <IconButton title="Delete" onClick={() => { handleDeleteOpen(row._id) }}>
                          <Delete color='warning' />
                        </IconButton>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {row.name}
                      </TableCell>
                      <TableCell>{row.type}</TableCell>
                      <TableCell>{row.mobile}</TableCell>
                      <TableCell>{row?.employee_id}</TableCell>
                      <TableCell>
                        <FormGroup>
                          <SwitchCompnent label="Activate" value={row.status} handleChange={handleChangeStatus} row={row._id} />
                        </FormGroup>
                      </TableCell>
                    </TableRow>
                  )) : <TableRow>
                    <TableCell colSpan="6">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="6">
                      <TableLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            {/* Mobile View */}
            <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "none" } }} >
              <Table size="small" aria-label="simple table">
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <React.Fragment key={row._id}>
                      <TableRow
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell>
                          <Card className='employee-card' >
                            <Box>
                              <CardContent sx={{ paddingBottom: '0 !important' }}>
                                <Grid container spacing={1}>
                                  <Grid className='employee-title' item xs={6} sm={6}>
                                    <div>Name</div>
                                    <div className='employee-body'>{row.name}</div>
                                  </Grid>
                                  <Grid className='employee-title' item xs={6} sm={6}>
                                    <div>Type</div>
                                    <div className='employee-body'>{row?.type}</div>
                                  </Grid>
                                  <Grid className='employee-title' item xs={6} sm={6}>
                                    <div>Mobile</div>
                                    <div className='employee-body'>{row?.mobile} </div>
                                  </Grid>
                                  <Grid className='employee-title' item xs={6} sm={6}>
                                    <div>Employee Id</div>
                                    <div className='employee-body'>{row?.employee_id}</div>
                                  </Grid>
                                  <Grid item xs={12} sm={12} className="div-flex">
                                    <Button className='customer-btn' startIcon={<Edit color='primary' />} onClick={() => { handleEdit(row) }} >
                                      Edit
                                    </Button>
                                    <Button className='customer-btn' startIcon={<Delete color='warning' />} onClick={() => { handleDeleteOpen(row._id) }} >
                                      Delete
                                    </Button>
                                    <Button className='customer-btn' startIcon={<MoreVert color="info" />} onClick={() => { handleViewOpen(row) }} >
                                      Details
                                    </Button>
                                    <FormGroup>
                                      <SwitchCompnent label="Activate" value={row.status} handleChange={handleChangeStatus} row={row._id} />
                                    </FormGroup>
                                  </Grid>
                                </Grid>
                              </CardContent>
                            </Box>
                          </Card>
                        </TableCell>
                      </TableRow>
                    </React.Fragment>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <MobileLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
          </CardContent>
        </Card>
      </Grid>
      <Dialog
        open={data.open}
        onClose={handleClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <EditEmployee allvalue={data} setId={handleClose} />
      </Dialog>
      <Dialog
        open={cancel.open}
        onClose={handleDeleteClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Delete Employee</Box>
          <Box>
            <IconButton onClick={handleDeleteClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          Are You Sure You want to Delete??
        </DialogContent>
        <DialogActions>
          <BookingButton fullWidth variant="contained" disabled={loading} onClick={() => { handleDelete(cancel._id) }} >Yes</BookingButton>
          <BookingButton fullWidth variant="contained" disabled={loading} onClick={handleDeleteClose} >No</BookingButton>
        </DialogActions>
      </Dialog>
      <Dialog
        open={expert.open}
        onClose={handleViewClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <Grid>
          <Card>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Box flexGrow={1}>
                      <Typography className='title_head' flexGrow={1} variant="h5" component="div">Employee Details</Typography>
                    </Box>
                    <Box>
                      <IconButton onClick={handleViewClose}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                {Object.keys(expert.data).length > 0 && <>
                  <Grid item xs={12} sm={6} md={6}>{expert?.data.name}</Grid>
                  <Grid item xs={12} sm={6} md={6}>{expert?.data.type}</Grid>
                  <Grid item xs={12} sm={6} md={6}>{expert?.data.mobile}</Grid>
                  <Grid item xs={12} sm={6} md={6}>{expert?.data?.employee_id}</Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <MainCalendar arr={expert?.data?.log} />
                  </Grid>
                </>}
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Dialog>
    </Grid>
  )
}

export default EmployeeList
