import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Button, Box, IconButton, Typography } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import { keyLengthValidation } from 'config/KeyData';
import { employeeShopUpdate } from 'redux/action';
import { BookingButton } from "components/common";
import { Close } from '@mui/icons-material';

const EditEmployee = ({ allvalue, setId }) => {

  const [data, setData] = useState({
    name: allvalue.name,
    mobile: allvalue.mobile,
    type: allvalue.type,
  })

  const [err, setErr] = useState({})
  const [btn, setBtn] = useState(true)
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.created)
  const loading = useSelector(state => state.auth.loader)

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])


  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
    setBtn(false)
  }


  const handleSubmit = () => {
    var check = employeeValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(employeeShopUpdate(allvalue?._id, data))
  }


  return (
    <Grid >
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}><Grid className="creatPop" item xs={12} sm={12} md={12}>
              <Box display="flex" alignItems="center">
                <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                  Edit Employee
                </Typography>
                <Box className="crossIcon">
                  <IconButton onClick={() => { setId() }}>
                    <Close />
                  </IconButton>
                </Box>
              </Box>
            </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="name" type="text" value={data.name} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Name" />
                {err.name && (<FormError data={err.name}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="type" type="text" value={data.type} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Type" />
                {err.type && (<FormError data={err.type}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="mobile" type="tel" inputProps={{
                  maxLength: keyLengthValidation.mobile
                }} value={data.mobile} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Mobile Number" />
                {err.mobile && (<FormError data={err.mobile}></FormError>)}
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }}>
              <Grid item xs={6} align="left">
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId() }}>Close</BookingButton>
              </Grid>
              <Grid item xs={6} align="right">
                <BookingButton type="button" disabled={(loading || btn) ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Update</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default EditEmployee

const employeeValidate = (data) => {
  let errors = {}

  if (!data.name) {
    errors["name"] = "Name is Required"
  }

  if (!data.type) {
    errors["type"] = "Type is Required"
  }

  if (!data.mobile) {
    errors['mobile'] = "Mobile Number is Required";
  }

  // if (data.mobile) {
  //   if (!data["mobile"].match(/^[0-9]{10}$/)) {
  //     errors["mobile"] = "Please enter valid mobile no.";
  //   }
  // }
  return errors
}