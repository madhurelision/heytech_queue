import React from 'react';
import { TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Paper, IconButton } from '@mui/material';
import { Delete, Add, Remove } from '@mui/icons-material'

const TableData = ({ arr, handleRemove, check, handleAdd, handleMinus }) => {
  return (
    <TableContainer container={Paper} sx={{ height: '25vh', overflowY: 'scroll', position: 'relative' }}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead sx={{ position: 'sticky', top: 0, backgroundColor: 'black', zIndex: 1000 }}>
          <TableRow>
            <TableCell>#</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>Quantity</TableCell>
            <TableCell>Subtotal</TableCell>
            {check == false && <TableCell>Action</TableCell>}
          </TableRow>
        </TableHead>
        <TableBody>
          {arr.length == 0 ? <TableRow>
            <TableCell colSpan="5">No Data Found</TableCell>
          </TableRow> : <>{arr.map((row, i) => (
            <TableRow
              key={row._id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell>
                {i + 1}
              </TableCell>
              <TableCell>
                {row.name}
              </TableCell>
              <TableCell>
                {check == false && <IconButton disabled={row.stock == 1} title="Minus" onClick={() => { handleMinus(i, row._id) }}>
                  <Remove />
                </IconButton>}
                {row.stock}
                {check == false && <IconButton title="Add" onClick={() => { handleAdd(i, row._id) }}>
                  <Add />
                </IconButton>}
              </TableCell>
              <TableCell>
                {row.stock * row.price}
              </TableCell>
              {check == false && <TableCell>
                <IconButton title="Delete" onClick={() => { handleRemove(row._id) }}>
                  <Delete color='warning' />
                </IconButton>
              </TableCell>}
            </TableRow>
          ))}</>

          }
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default TableData