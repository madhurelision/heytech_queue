import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, Typography, Box, IconButton } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { Close } from '@mui/icons-material'
import { BookingButton } from "components/common";
import { useParams } from 'react-router-dom';
import { getAllSinglePayment } from 'redux/action';
import TableLoader from 'pages/Shop/UsableComponent/TableLoader'
import NoData from 'pages/Shop/UsableComponent/NoData'

const UpiForm = () => {

  const [data, setData] = useState({})
  const [flag, setFlag] = useState(false)
  const payData = useSelector(state => state.auth.payList)
  const { id } = useParams();

  const dispatch = useDispatch()

  useEffect(() => {

    if (Object.keys(payData).length == 0) {
      dispatch(getAllSinglePayment(id))
    } else {
      setData(payData.pay)
      setFlag(true)
    }

  }, [payData])

  const handleBtn = () => {
    const deepLink = `upi://pay?pa=${data?.shop_id?.upi}&pn=${data?.shop_id?.name}&am=${data?.total}&cu=INR`
  }

  return (
    <Grid>
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid className="creatPop" item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                    Upi Form
                  </Typography>
                </Box>
              </Grid>
            </Grid>
            {/* {flag ? Object.keys(data).length > 0 ? <></> : <NoData /> : <Grid item xs={12} sm={12} md={10}>
        <TableLoader />
      </Grid>} */}
            <NoData />
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default UpiForm