import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Dialog, DialogContent, Typography, DialogTitle, Box, IconButton, useTheme, useMediaQuery, MenuItem, FormControl, InputLabel, Select, Checkbox, CardMedia } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import '../../Registration/register.css'
import { Close } from '@mui/icons-material'
import { BookingButton } from "components/common";
import { getShopService } from 'redux/action'
import TableData from './TableData'
import { uniqueServiceElements } from 'config/uniqueElements'
import CustomerSearch from '../UsableComponent/CustomerSearch'

const ServiceForm = ({ allvalue, setId }) => {

  const theme = useTheme()
  const [data, setData] = useState({
    customer_id: allvalue.customer,
    type: '',
    data: allvalue.service,
    order_tax: 0,
    discount: 0,
    test: allvalue.test
  })

  const [err, setErr] = useState({})
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.created)
  const loading = useSelector(state => state.auth.loader)
  const serviceData = useSelector((state) => state.auth.shopService)
  const customerData = useSelector((state) => state.auth.customerList)
  const [service, setService] = useState([])
  const [val, setVal] = useState('All')

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])

  useEffect(() => {
    if (serviceData.length == 0) {
      dispatch(getShopService(0, 10))
    } else {
      setService(serviceData.service)

    }
  }, [serviceData])

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    var check = serviceValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    var check = { ...data, service_data: data.data, service: data.data.map((cc) => cc._id), product_id: allvalue._id, shop_id: allvalue?.shop_id, amount: data.data.reduce((cc, val) => { cc = cc + (val.stock * val.price); return cc }, 0), total: sumTotal(data.order_tax, data.discount) }

    // if (data.type == 'Cash') {

    // } else if (data.type == 'UPI') {

    // } else if (data.type == 'Package') {

    // } else {

    // }

  }

  const handleRemove = (id) => {
    const ss = data.data.filter((cc) => cc._id !== id)
    setData({ ...data, data: ss, service: '' })
  }

  const handleService = (e) => {
    const main = service.filter((cc) => cc._id == e.target.value)
    var mft = data.data
    const ss = data.data.filter((cc) => cc._id == e.target.value)
    if (ss.length == 0) {
      mft.push({ _id: main[0]?._id, stock: 1, price: 100, name: main[0]?.name })
      setData({ ...data, data: mft })
    } else {

    }
  }

  const handleServiceC = (id) => {
    const main = service.filter((cc) => cc._id == id)
    var mft = data.data
    const ss = data.data.filter((cc) => cc._id == id)
    const mains = data.data.findIndex((e) => e._id == id)

    if (ss.length == 0) {
      mft.push({ _id: main[0]?._id, stock: 1, price: 100, name: main[0]?.name })
      setData({ ...data, data: mft })
    } else {
      var atV = [...data.data]
      console.log('val', atV[mains])
      atV[mains] = { ...atV[mains], stock: ss[0]?.stock + 1 }
      setData({ ...data, data: atV })
    }
  }

  const filterTopics = (motivation) => {
    if (motivation == 'All') {
      return service
    }
    return uniqueServiceElements(service).filter((main) => main.motivation === motivation);
  };

  const handleView = (e, ccd) => {
    if (ccd == null || ccd == undefined) {
      setData({ ...data, customer_id: '', test: '' })
    } else {
      const ss = customerData.customer.filter((cc) => cc._id == ccd._id)
      setData({ ...data, customer_id: ss[0]?._id, test: ss[0] })
    }
  }

  const sumTotal = (order_tax, ss) => {
    const GTotal = data.data.reduce((cc, val) => { cc = cc + (val.stock * val.price); return cc }, 0)
    var tax, discount
    if (parseInt(order_tax) !== null) {
      if (GTotal * parseInt(order_tax) > 0) {
        tax = parseInt((GTotal * parseInt(order_tax)) / 100)
      } else {
        tax = 0
      }
    }
    if (parseInt(ss) !== null) {
      if (GTotal * parseInt(ss) > 0) {
        discount = parseInt((GTotal * parseInt(ss)) / 100)
      } else {
        discount = 0
      }
    }
    return GTotal + tax - discount
  }

  const getTax = (order_tax) => {
    const GTotal = data.data.reduce((cc, val) => { cc = cc + (val.stock * val.price); return cc }, 0)
    var tax = 0
    if (parseInt(order_tax) !== null) {
      if (GTotal * parseInt(order_tax) > 0) {
        tax = parseInt((GTotal * parseInt(order_tax)) / 100)
      } else {
        tax = 0
      }
    }
    return tax
  }

  return (
    <Grid >
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid className="creatPop" item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                    Service Form
                  </Typography>
                  <Box className="crossIcon">
                    <IconButton onClick={() => { setId() }}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={12} md={12}>
                    <CustomerSearch val={data.test} handleView={handleView} />
                    {err.customer && (<FormError data={err.customer}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <FormControl fullWidth>
                      <InputLabel id="demo-simple-select-label" shrink={true} >Service</InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={data.service}
                        label="Service"
                        name="service"
                        onChange={handleService}
                      >
                        {service.map((cc) => {
                          return (
                            <MenuItem style={{ textTransform: 'capitalize' }} key={cc._id} value={cc._id}>{cc.name}</MenuItem>
                          )
                        })}
                      </Select>
                    </FormControl>
                    {err.service && (<FormError data={err.service}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <TableData arr={data.data} handleRemove={handleRemove} check={false} />
                  </Grid>
                  <Grid item xs={12} sm={12} md={4}>
                    <TextField name="order_tax" type="number" inputProps={{ min: 1, step: 1 }} value={data.order_tax} fullWidth
                      variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Order Tax" />
                    {err.order_tax && (<FormError data={err.order_tax}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={4}>
                    <TextField name="discount" type="number" inputProps={{ min: 1, step: 1 }} value={data.discount} fullWidth
                      variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Discount" />
                    {err.discount && (<FormError data={err.discount}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={4}>
                    <FormControl fullWidth>
                      <InputLabel id="demo-simple-select-label" shrink={true} >Payment Type</InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={data.type}
                        label="Payment Type"
                        name="type"
                        onChange={handleChange}
                      >
                        {['Cash', 'UPI', 'Package'].map((cc) => {
                          return (
                            <MenuItem key={cc} value={cc}>{cc}</MenuItem>
                          )
                        })}
                      </Select>
                    </FormControl>
                    {err.type && (<FormError data={err.type}></FormError>)}
                  </Grid>
                  <Grid item xs={6} sm={6} md={6}>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                      <div>Grand Total:  &#8377;{sumTotal(data.order_tax, data.discount)}</div>
                      <div>Tax: &#8377;{getTax(data.order_tax)}</div>
                    </div>
                  </Grid>
                  <Grid item xs={6} sm={6} md={6} align="right">
                    <BookingButton type="button" variant="contained" onClick={() => { handleSubmit() }} >Pay</BookingButton>
                  </Grid>

                </Grid>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Grid container spacing={2} sx={{ marginBottom: '15px' }}>
                  <Grid item xs={12} sm={12} md={12}>
                    <div className='payment-tab'>
                      {[<div className={val == 'All' ? 'selected-payment-tab' : 'payment-tab-div'} onClick={() => { setVal('All') }}>All</div>].concat([...new Set(service.map((cc) => cc.motivation))].map((ss) =>
                        <div className={ss == val ? 'selected-payment-tab' : 'payment-tab-div'} key={ss} onClick={() => { setVal(ss) }} >{ss}</div>
                      ))}
                    </div>
                  </Grid>
                </Grid>
                <Grid container spacing={2} sx={{ overflowY: 'scroll', height: '58vh' }}>
                  {filterTopics(val).map((row) => <Grid item key={row._id} xs={12} sm={6} md={4}>
                    <Card className={data.data.filter((cc) => cc._id == row._id).length > 0 ? 'card-book-highlight' : "card-book"} sx={{ borderRadius: '18px' }} onClick={() => { handleServiceC(row._id) }} >
                      <CardMedia
                        component="img"
                        sx={{ width: '100%', objectFit: 'unset', borderRadius: '18px', border: 1 }}
                        image={row.image}
                        alt={row.name}
                      />
                      <Box>
                        <CardContent>
                          <Grid container spacing={2}>
                            <Grid item xs={12} sm={12} md={12}>
                              <div className='customer-font'>{row.name}</div>
                            </Grid>
                          </Grid>
                        </CardContent>
                      </Box>
                    </Card>
                  </Grid>)}
                </Grid>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default ServiceForm

const serviceValidate = (data) => {
  let errors = {}

  if (!data.customer_id) {
    errors["customer_id"] = "Customer is Required"
  }

  if (!data.type) {
    errors["type"] = "Type is Required"
  }

  if (!data.data) {
    errors["service"] = "Service is Required"
  }

  if (data.data.length == 0) {
    errors["service"] = "Service is Required"
  }

  return errors
}