import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Dialog, DialogContent, Typography, DialogTitle, Box, IconButton, useTheme, useMediaQuery, MenuItem, FormControl, InputLabel, Select, Checkbox, CardMedia } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import '../../Registration/register.css'
import { Close } from '@mui/icons-material'
import { BookingButton } from "components/common";
import TableData from './TableData'
import { uniqueServiceElements } from 'config/uniqueElements'
import { getShopCategory, getShopVendor, getShopProduct } from 'redux/action/inventory'

const ProductForm = ({ allvalue, setId }) => {

  const theme = useTheme()
  const [data, setData] = useState({
    vendor: allvalue.vendor,
    product: '',
    data: allvalue.product,
    order_tax: 0,
    discount: 0
  })

  const [err, setErr] = useState({})
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.created)
  const loading = useSelector(state => state.auth.loader)
  const categoryData = useSelector((state) => state.auth.inventoryCategoryList)
  const vendorData = useSelector((state) => state.auth.inventoryVendorList)
  const productData = useSelector((state) => state.auth.inventoryProductList)
  const [category, setCategory] = useState([])
  const [vendor, setVendor] = useState([])
  const [product, setProduct] = useState([])
  const [val, setVal] = useState('All')

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])


  useEffect(() => {

    if (productData.length == 0) {
      dispatch(getShopProduct(0, 10))
    } else {
      setProduct(productData.product)
    }

  }, [productData])

  useEffect(() => {
    if (categoryData.length == 0) {
      dispatch(getShopCategory(0, 10))
    } else {
      setCategory(categoryData.category)
    }
  }, [categoryData])

  useEffect(() => {
    if (vendorData.length == 0) {
      dispatch(getShopVendor(0, 10))
    } else {
      setVendor(vendorData.vendor)
    }
  }, [vendorData])

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    var check = productValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
  }

  const handleRemove = (id) => {
    const ss = data.data.filter((cc) => cc._id !== id)
    setData({ ...data, data: ss, product: '' })
  }

  const handleProduct = (e) => {
    const main = product.filter((cc) => cc._id == e.target.value)
    var mft = data.data
    const ss = data.data.filter((cc) => cc._id == e.target.value)
    if (ss.length == 0) {
      mft.push({ _id: main[0]?._id, stock: 1, price: main[0]?.price, name: main[0]?.name })
      setData({ ...data, data: mft })
    } else {

    }
  }

  const handleProductC = (id) => {
    const main = product.filter((cc) => cc._id == id)
    var mft = data.data
    const ss = data.data.filter((cc) => cc._id == id)
    const mains = data.data.findIndex((e) => e._id == id)

    if (main[0].mainstock !== 0) {
      if (ss.length == 0) {
        mft.push({ _id: main[0]?._id, stock: 1, price: main[0]?.price, name: main[0]?.name })
        setData({ ...data, data: mft })
      } else {
        var atV = [...data.data]
        console.log('val', atV[mains])
        atV[mains] = { ...atV[mains], stock: ss[0]?.stock + 1 }
        setData({ ...data, data: atV })
      }
    }
  }

  const filterTopics = (motivation) => {
    if (motivation == 'All') {
      return product
    }
    return uniqueServiceElements(product).filter((main) => main.category._id === motivation);
  };

  const sumTotal = (order_tax, ss) => {
    const GTotal = data.data.reduce((cc, val) => { cc = cc + (val.stock * val.price); return cc }, 0)
    var tax, discount
    if (parseInt(order_tax) !== null) {
      if (GTotal * parseInt(order_tax) > 0) {
        tax = parseInt((GTotal * parseInt(order_tax)) / 100)
      } else {
        tax = 0
      }
    }
    if (parseInt(ss) !== null) {
      if (GTotal * parseInt(ss) > 0) {
        discount = parseInt((GTotal * parseInt(ss)) / 100)
      } else {
        discount = 0
      }
    }
    return GTotal + tax - discount
  }

  const getTax = (order_tax) => {
    const GTotal = data.data.reduce((cc, val) => { cc = cc + (val.stock * val.price); return cc }, 0)
    var tax = 0
    if (parseInt(order_tax) !== null) {
      if (GTotal * parseInt(order_tax) > 0) {
        tax = parseInt((GTotal * parseInt(order_tax)) / 100)
      } else {
        tax = 0
      }
    }
    return tax
  }

  return (
    <Grid >
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid className="creatPop" item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                    Product Form
                  </Typography>
                  <Box className="crossIcon">
                    <IconButton onClick={() => { setId() }}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={12} md={12}>
                    <FormControl fullWidth>
                      <InputLabel id="demo-simple-select-label" shrink={true} >Vendor</InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={data.vendor}
                        label="Vendor"
                        name="vendor"
                        onChange={handleChange}
                      >
                        {vendor.map((cc) => {
                          return (
                            <MenuItem style={{ textTransform: 'capitalize' }} key={cc._id} value={cc._id}>{cc.name}</MenuItem>
                          )
                        })}
                      </Select>
                    </FormControl>
                    {err.vendor && (<FormError data={err.vendor}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <FormControl fullWidth>
                      <InputLabel id="demo-simple-select-label" shrink={true} >Product</InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={data.product}
                        label="Product"
                        name="product"
                        onChange={handleProduct}
                      >
                        {product.map((cc) => {
                          return (
                            <MenuItem style={{ textTransform: 'capitalize' }} key={cc._id} value={cc._id}>{cc.name}</MenuItem>
                          )
                        })}
                      </Select>
                    </FormControl>
                    {err.product && (<FormError data={err.product}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <TableData arr={data.data} handleRemove={handleRemove} check={false} />
                  </Grid>
                  <Grid item xs={12} sm={12} md={6}>
                    <TextField name="order_tax" type="number" inputProps={{ min: 1, step: 1 }} value={data.order_tax} fullWidth
                      variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Order Tax" />
                    {err.order_tax && (<FormError data={err.order_tax}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={6}>
                    <TextField name="discount" type="number" inputProps={{ min: 1, step: 1 }} value={data.discount} fullWidth
                      variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Discount" />
                    {err.discount && (<FormError data={err.discount}></FormError>)}
                  </Grid>
                  <Grid item xs={6} sm={6} md={6}>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                      <div>Grand Total:  &#8377;{sumTotal(data.order_tax, data.discount)}</div>
                      <div>Tax: &#8377;{getTax(data.order_tax)}</div>
                    </div>
                  </Grid>
                  <Grid item xs={6} sm={6} md={6} align="right">
                    <BookingButton type="button" variant="contained" onClick={() => { handleSubmit() }} >Pay</BookingButton>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} sm={12} md={6} sx={{ marginBottom: '15px' }}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={12} md={12}>
                    <div className='payment-tab'>
                      {[<div className={val == 'All' ? 'selected-payment-tab' : 'payment-tab-div'} onClick={() => { setVal('All') }}>All</div>].concat(category.map((ss) =>
                        <div className={ss == val ? 'selected-payment-tab' : 'payment-tab-div'} key={ss._id} onClick={() => { setVal(ss._id) }} >{ss.name}</div>
                      ))}
                    </div>
                  </Grid>
                </Grid>
                <Grid container spacing={2} sx={{ overflowY: 'scroll', height: '58vh' }}>
                  {filterTopics(val).map((row) => <Grid item key={row._id} xs={12} sm={6} md={4}>
                    <Card className={data.data.filter((cc) => cc._id == row._id).length > 0 ? 'card-book-highlight' : "card-book"} sx={{ borderRadius: '18px' }} onClick={() => {
                      if (row.mainstock !== 0) {
                        handleProductC(row._id)
                      }
                    }} >
                      <CardMedia
                        component="img"
                        sx={{ width: '100%', objectFit: 'unset', borderRadius: '18px', border: 1 }}
                        image={row.image}
                        alt={row.name}
                      />
                      <Box>
                        <CardContent>
                          <Grid container spacing={2}>
                            <Grid item xs={12} sm={12} md={12}>
                              <div className='customer-font'>{row.name}</div>
                            </Grid>
                            <Grid item xs={12} sm={12} md={12}>
                              <div className='customer-font'> &#8377;{row.price}</div>
                            </Grid>
                            <Grid item xs={12} sm={12} md={12}>
                              <div className='customer-font'> {row.mainstock}{'pcs'}</div>
                            </Grid>
                          </Grid>
                        </CardContent>
                      </Box>
                    </Card>
                  </Grid>)}
                </Grid>
              </Grid>
            </Grid>
            {/* <Grid container spacing={2} sx={{ marginTop: '5px' }}>
              <Grid item xs={6} align="left">
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId(1) }} >Close</BookingButton>
              </Grid>
              <Grid item xs={6} align="right">
                <BookingButton type="button" disabled={(loading || btn) ? true : false} variant="contained" onClick={() => { handleSubmit() }} >Submit</BookingButton>
              </Grid>
            </Grid> */}
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default ProductForm

const productValidate = (data) => {
  let errors = {}

  if (!data.vendor) {
    errors["vendor"] = "Vendor is Required"
  }

  if (!data.data) {
    errors["product"] = "Product is Required"
  }

  if (data.data.length == 0) {
    errors["product"] = "Product is Required"
  }

  return errors
}