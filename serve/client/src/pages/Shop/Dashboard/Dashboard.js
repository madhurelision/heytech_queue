import React, { useContext, useEffect, useState, useCallback, useRef } from 'react'
import { Grid, Button, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Typography, Card, CardContent, CardHeader, IconButton, Dialog, DialogTitle, DialogContent, Box, DialogActions, Collapse, FormGroup, FormControlLabel, Switch, useTheme, Menu, MenuItem } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import TableLoader from '../UsableComponent/TableLoader'
import NoData from '../UsableComponent/NoData'
import { LoadingButton } from '@mui/lab'
import { appointmentStatusUpdate, bookingEmployeeAdd, getShopCurrentBooking, shopCancelBooking, shopProfileData, shopCompleteBooking, getShopEmployee, getShopService } from 'redux/action'
import WaitingForm from './WaitingForm'
import { tableStyles } from './styles'
import StatusBox from '../UsableComponent/StatusBox'
import DialogBox from './DialogBox'
import moment from 'moment'
import ShopAppointmentView from './ShopAppointmentView'
import EmailBox from '../UsableComponent/EmailBox'
import ProgressTimer from '../UsableComponent/ProgressTimer'
import { useNavigate } from 'react-router'
import SocketContext from 'config/socket'
import { Cancel, Close, Edit, MoreVert, Refresh, WhatsApp, KeyboardArrowDown, KeyboardArrowUp, Star, AddCircleOutline, ViewList, GridView } from '@mui/icons-material'
import { ACTIVE_EMPLOYEE_LIST, LOADERCLOSE, SHOP_COUNT, SOCKET_SHOP_BOOKING_REMOVE, SOCKET_SHOP_BOOKING_UPDATE, SOCKET_SHOP_NOTIFICATION, SOCKET_SHOP_QUEUE_REMOVE, SOCKET_SHOP_QUEUE_UPDATE } from 'redux/action/types'
import TooltipBox from '../UsableComponent/TooltipBox'
import ListCard from '../UsableComponent/ListCard'
import { ChangeNumber, handleWhatsapp, getDateChange } from 'config/KeyData'
import SwitchCompnent from '../UsableComponent/SwitchComponent';
import DatePicker from 'components/DaterPicker';
import { useRecoilState } from 'recoil'
import { mentorState } from 'store'
import EmployeeSelect from '../UsableComponent/EmployeeSelect'
import MobileLoader from '../UsableComponent/MobileLoader'
import RemarkForm from '../Queue/RemarkForm'
import RMark from '@mui/icons-material/MarkunreadMailbox';
import NewTimer from '../UsableComponent/NewTimer'
import '../Queue/queue.css'
import InfiniteScroll from 'react-infinite-scroll-component'

var DailogData
const Dashboard = ({ setValue }) => {

  const theme = useTheme()
  const [element, setElement] = useState([])
  const [box, setBox] = useState([])
  const [flag, setFlag] = useState(false)
  const [open, setOpen] = useState(false)
  const [form, setForm] = useState(false)
  const [show, setShow] = useState(false)
  const [view, setView] = useState(true)
  const [boxview, setBoxView] = useState('')
  const bookingData = useSelector((state) => state.auth.shopCurrentBooking)
  const allCount = useSelector(state => state.auth.listABookshopCount)
  const profileData = useSelector((state) => state.auth.profile);
  const serviceData = useSelector((state) => state.auth.shopService)
  const employeeData = useSelector((state) => state.auth.shopEmployee)
  const moreData = useSelector(state => state.auth.shopAppointmentMore)
  const cted = useSelector((state) => state.auth.open)
  const mainV = useSelector((state) => state.auth.updated);
  const loading = useSelector((state) => state.auth.loader)
  const [mentorData, setMentorData] = useRecoilState(mentorState);
  const dispatch = useDispatch()
  const srollerLoad = useRef(null);
  const [data, setData] = useState({
    open: false,
    waiting_time: '',
    _id: '',
    appointment_date: '',
    appointment_time: '',
    waiting_number: '',
    waiting_key: ''
  })

  const [remark, setRemark] = useState({
    open: false,
    remark: '',
    _id: '',
  })

  const [cancel, setCancel] = useState({
    open: false,
    _id: ''
  })


  const [book, setBook] = useState({
    open: false,
    data: {}
  })

  const [appointment, setAppointment] = useState({
    open: false,
    value: false
  })


  const classes = tableStyles()
  const history = useNavigate()


  const context = useContext(SocketContext)


  useEffect(() => {

    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
    })

    context.socket.on('userDisconnect', (data) => {
      console.log('Socket Err', data)
    })

    context.socket.on('shopCount', (data) => {
      //console.log('Shop Count Data', data)
      dispatch({ type: SHOP_COUNT, payload: data })
    })

    context.socket.on('shopBooking', (data) => {
      //console.log('Shop Booking Data', data)
      dispatch({ type: SOCKET_SHOP_BOOKING_UPDATE, payload: [data] })
      if (data.queue == true) {
        dispatch({ type: SOCKET_SHOP_QUEUE_UPDATE, payload: [data] })
      }
    })

    context.socket.on('shopBookingRemove', (data) => {
      //console.log('Shop Booking Remove', data)
      dispatch({ type: SOCKET_SHOP_BOOKING_REMOVE, payload: data })
      if (data.queue == true) {
        dispatch({ type: SOCKET_SHOP_QUEUE_REMOVE, payload: data })
      }
    })

    context.socket.on('notificationShopCount', (data) => {
      //console.log('notificationCount', data)
      dispatch({ type: SOCKET_SHOP_NOTIFICATION, payload: data })
    })

    context.socket.on('shopEmployeeData', data => {
      //console.log('shop Sockebokit Data', data.val)
      dispatch({ type: ACTIVE_EMPLOYEE_LIST, payload: { data: data } })
    })

    context.socket.on('disconnect', () => {
      console.log('socket is disconnect')
    })

    return () => {
      context.socket.removeListener(['shopCount', 'shopBookingRemove', 'shopBooking', 'shopEmployeeData'])
    }

  }, [])

  useEffect(() => {

    if (bookingData.length == 0) {
      dispatch(getShopCurrentBooking(0, 10))
    } else {
      setElement(bookingData.booking)
      setFlag(true)
    }
  }, [bookingData])


  useEffect(() => {

    if (cted) {
      setCancel({ ...cancel, open: false })
      setBook({ ...book, open: false })
      dispatch(getShopCurrentBooking(0, 10))
    }

    if (mainV) {
      setAppointment({ ...appointment, open: false, value: false })
      dispatch(shopProfileData())
    }

  }, [cted, mainV])

  useEffect(() => {

    if (employeeData.length == 0) {
      dispatch(getShopEmployee())
    }
  }, [employeeData])

  useEffect(() => {

    if (serviceData.length == 0) {
      dispatch(getShopService(0, 10))
    }
  }, [serviceData])

  // const handleObserver = useCallback((entries) => {
  //   const target = entries[0];
  //   console.log('Observer', target)
  //   if (target.isIntersecting) {
  //     console.log('test', allCount, 'ele', element.length, 'cond', allCount !== null && allCount != element.length)
  //     // if (allCount !== null && allCount != element.length) {
  //     //   dispatch(getShopCurrentBooking(element.length, 10))
  //     // }
  //   }
  // }, [element, allCount]);

  // useEffect(() => {
  //   const option = {
  //     root: null,
  //     rootMargin: "20px",
  //     threshold: 0
  //   };
  //   const observer = new IntersectionObserver(handleObserver, option);
  //   if (srollerLoad.current) observer.observe(srollerLoad.current);
  // }, [handleObserver]);


  const handleEdit = (val) => {
    setData({
      ...data,
      open: true,
      waiting_time: val.waiting_time,
      status: val.status,
      _id: val._id,
      appointment_date: val.appointment_date,
      appointment_time: val.appointment_time,
      waiting_number: val.waiting_number,
      waiting_key: val.waiting_key,
    })
  }

  const handleClose = () => {
    setData({ ...data, open: false })
  }

  const handleDialogClose = () => {
    setOpen(false)
  }

  const handleDialogOpen = (item) => {
    DailogData = <ShopAppointmentView item={item} />
    setOpen(true)
  }

  const handleRefresh = () => {
    dispatch(getShopCurrentBooking(0, 10))
  }

  const handleCancel = (item) => {
    dispatch(shopCancelBooking(item))
  }

  // const handleWhatsapp = (mobile, url) => {
  //   var a = document.createElement('a')
  //   a.target = "_blank"
  //   a.href = `https://wa.me/${mobile}?text=${url}`
  //   a.click()
  //   console.log('link', `https://wa.me/${mobile}?text=${url}`)
  // }

  const handleCancelOpen = (item) => {
    setCancel({
      ...cancel,
      open: true,
      _id: item
    })
  }

  const handleCancelClose = () => {
    setCancel({ ...cancel, open: false })
  }

  const handleTimerClose = useCallback((id) => {
    console.log('Timer Close', id)
    var arr = [...box]
    arr.push(id)
    setBox(arr)
  }, [box.length]);


  const handleChangeStatusOpen = (e, id) => {
    setAppointment({ ...appointment, open: true, value: e.target.checked })
  }

  const handleChangeStatus = (val) => {
    dispatch(appointmentStatusUpdate(profileData._id, { appointment: val }))
  }

  const handleChangeStatusClose = () => {
    setAppointment({ ...appointment, open: false, value: false })
  }

  const handleFormOpen = () => {
    setForm(true)
    setMentorData(profileData)
  }

  const handleFormClose = () => {
    setForm(false)
    setShow(false)
    dispatch({ type: LOADERCLOSE })
  }

  const handleAssign = (e, id) => {
    console.log('Check Assign', e.target.value, id)
    dispatch(bookingEmployeeAdd(id, { assign: e.target.value }))
    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }
  }

  const handleRemarkEdit = (val) => {
    setRemark({
      ...remark,
      open: true,
      remark: val.remark,
      _id: val._id,
    })
  }

  const handleRemarkClose = () => {
    setRemark({ ...remark, open: false })
  }

  const handleViewCard = (row) => {
    setBook({
      ...book,
      open: true,
      data: row,
    })
  }

  const handleBookingClose = () => {
    setBook({ ...book, open: false })
  }


  const fetchMoreData = () => {
    dispatch(getShopCurrentBooking(element.length, 10))
  };


  const handleshopComplete = () => {
    dispatch(shopCompleteBooking({ status: false }))
    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }
    if (context.socket.connected == true) {
      context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
    }
  }


  const handleShow = (val) => {
    setShow(val)
  }
  
  const [anchorEl, setAnchorEl] = React.useState(null);
  const menuBtn = Boolean(anchorEl);
  const handleMenuClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card>
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={4} sm={3} md={2}>
              <Button
                  id="demo-positioned-button"
                  aria-controls={menuBtn ? 'demo-positioned-menu' : undefined}
                  aria-haspopup="true"
                  aria-expanded={menuBtn ? 'true' : undefined}
                  onClick={handleMenuClick}
                >
                  Menu
                </Button>
              <Menu
                  id="demo-positioned-menu"
                  aria-labelledby="demo-positioned-button"
                  anchorEl={anchorEl}
                  open={menuBtn}
                  onClose={handleMenuClose}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                  }}
                >
                  <MenuItem onClick={handleMenuClose}>
                <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh}>
                  Refresh
                </Button>
                  </MenuItem>
                  <MenuItem onClick={handleMenuClose}>
                <Button className='customer-btn' disabled={Object.keys(profileData).length == 0} startIcon={<AddCircleOutline color='primary' />} onClick={handleFormOpen}>
                  Appointment
                </Button>
                  </MenuItem>
                  {element.length > 0 && <MenuItem onClick={handleMenuClose}>
                  <Button className='customer-btn' startIcon={view ? <GridView color='primary' /> : <ViewList color='primary' />} onClick={() => { setView(!view) }} sx={{ display: { xs: "none", md: 'inline-flex', sm: 'inline-flex' } }}>
                  {view ? 'Grid' : 'List'}
                </Button>
                  </MenuItem>}

                </Menu>
                {/* {element.length > 0 && <Button className='customer-btn' startIcon={<CheckCircle color='primary' />} onClick={handleshopComplete} >
                  Complete Booking
                </Button>} */}
              </Grid>
              <Grid item xs={3} sm={6} md={8}>
                <Typography className='title_head' variant="h5" component="div" align="center" sx={{ display: { xs: 'none', md: 'block', sm: 'block' } }}>
                  Appointment List
                </Typography>
              </Grid>
              <Grid item xs={5} sm={3} md={2}>
                {Object.keys(profileData).length > 0 && <FormGroup>
                  <SwitchCompnent label="Appointment" value={profileData.appointment} handleChange={handleChangeStatusOpen} row={profileData._id} />
                </FormGroup>}
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <Typography className='title_head' sx={{ display: { xs: 'block', md: 'none', sm: 'none' } }} variant="h5" component="h2" align="center">
                  Appointment List
                </Typography>
              </Grid>
            </Grid>
            <InfiniteScroll className="searchServices"
              dataLength={element.length}
              next={() => { dispatch(getShopCurrentBooking(element.length, 10)) }}
              hasMore={moreData}
              loader={<h4 className="loading">Loading...</h4>}>
              <TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "block" } }}>
                {view == true && <Table sx={{ minWidth: 750 }} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell></TableCell>
                      <TableCell></TableCell>
                      <TableCell>Booking Id</TableCell>
                      <TableCell>Booking Type</TableCell>
                      <TableCell>Timer</TableCell>
                      <TableCell>Appointment Date</TableCell>
                      <TableCell>Appointment Time</TableCell>
                      <TableCell>Waiting Time</TableCell>
                      <TableCell>Service Name</TableCell>
                      <TableCell>User Size</TableCell>
                      <TableCell>Assign</TableCell>
                      <TableCell>Status</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {flag ? element.length > 0 ? element.map((row) => (
                      <React.Fragment
                        key={row._id}>
                        <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>

                          <TableCell>
                            {
                              boxview == row._id ? <IconButton
                                title='Open'
                                aria-label="open"
                                size="small"
                                onClick={() => setBoxView('')}
                              >
                                <KeyboardArrowUp />
                              </IconButton> : <IconButton
                                title='Close'
                                aria-label="close"
                                size="small"
                                onClick={() => setBoxView(row._id)}
                              >
                                <KeyboardArrowDown />
                              </IconButton>
                            }
                          </TableCell>
                          <TableCell>
                            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                              <IconButton title="Add Waiting Time" onClick={() => {
                                if (row.queue == true) {
                                  setValue("1")
                                } else {
                                  handleEdit(row)
                                }
                              }}>
                                <Edit color="primary" />
                              </IconButton>
                              <IconButton title="Add Remark" onClick={() => { handleRemarkEdit(row) }}>
                                <RMark color="primary" />
                              </IconButton>
                              <IconButton title="Details" onClick={() => { handleDialogOpen(row) }} >
                                <MoreVert color="info" />
                              </IconButton>
                              {(new Date(row.end_time) > new Date()) && <IconButton title="Cancel Booking" onClick={() => { handleCancelOpen(row._id) }} >
                                <Cancel color="disabled" />
                              </IconButton>}
                            </div>
                          </TableCell>
                          <TableCell>
                            {'#' + row?.bookingid}
                          </TableCell>
                          <TableCell>
                            {row.queue == true ? 'Queue' : 'Appointment'}
                          </TableCell>
                          <TableCell>
                            {row.waiting_verify == true ? <ProgressTimer time={new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))} ma={row._id} func={handleTimerClose} show={true} /> : '.......'}
                            {row.waiting_verify == true ? <NewTimer main={new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))} /> : ''}
                          </TableCell>
                          <TableCell >{moment(row.appointment_date).format('LL')}</TableCell>
                          <TableCell >{moment(row.appointment_date).calendar()}</TableCell>
                          <TableCell >{(!row.waiting_number || row.waiting_number == null || row.waiting_number == undefined) ? row?.waiting_time : (row?.waiting_number < 60) ? row?.waiting_number + ' ' + row.waiting_key : getDateChange(row.waiting_number)}</TableCell>
                          <TableCell >{row?.service_id?.name}</TableCell>
                          <TableCell  >{row?.size}</TableCell>
                          <TableCell>
                            <EmployeeSelect value={row.assign == null || row.assign == undefined ? '' : row.assign} _id={row._id} handleChange={handleAssign} />
                          </TableCell>
                          <TableCell >
                            <StatusBox value={row.status} />
                          </TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell className='customer-tablecell' colSpan={12}>
                            <Collapse in={boxview == row._id} timeout="auto" unmountOnExit>
                              <Box sx={{ margin: '3px' }}>
                                <Typography variant="h4" gutterBottom component="div" sx={{ fontSize: '1.1rem' }}>
                                  User Details
                                </Typography>
                                <Table size="small" aria-label="shopDetail">
                                  <TableHead>
                                    <TableRow>
                                      <TableCell></TableCell>
                                      <TableCell>Created By</TableCell>
                                      <TableCell>Description</TableCell>
                                      <TableCell>User Rating</TableCell>
                                      <TableCell>User Name</TableCell>
                                      <TableCell>User Number</TableCell>
                                      <TableCell>User Email</TableCell>
                                    </TableRow>
                                  </TableHead>
                                  <TableBody>
                                    <TableRow>
                                      <TableCell></TableCell>
                                      <TableCell>{(row.create_type == null || row.create_type == undefined) ? "........" : (row.create_type == "shop") ? row?.shop[0]?.shop_name + ' ' + row.create_type : (row.create_type == "customer") ? row?.customer[0]?.first_name + ' ' + row.create_type : (row.create_type == "employee") ? row?.employee[0]?.name + ' ' + row.create_type : row.create_type}</TableCell>
                                      <TableCell>
                                        <TooltipBox title="hover" data={row.description} />
                                      </TableCell>
                                      <TableCell>
                                        <Star color="warning" />{row.totalUser}
                                      </TableCell>
                                      <TableCell component="th" scope="row">
                                        {row?.user_id?.name}
                                      </TableCell>
                                      <TableCell>
                                        {(!row.mobile || row.mobile == null || row.mobile == undefined) ? '-----' : <IconButton title={row.mobile} onClick={() => { handleWhatsapp(row.mobile, `Hello Mr.${row?.user_id.name}`) }}>
                                          <WhatsApp />
                                        </IconButton>}
                                      </TableCell>
                                      <TableCell>
                                        <EmailBox title={row.user_email} />
                                      </TableCell>
                                    </TableRow>
                                  </TableBody>
                                </Table>
                              </Box>
                            </Collapse>
                          </TableCell>
                        </TableRow>
                      </React.Fragment>
                    )) : <TableRow>
                      <TableCell colSpan="12">
                        <NoData />
                      </TableCell>
                    </TableRow> : <TableRow>
                      <TableCell colSpan="12">
                        <TableLoader />
                      </TableCell>
                    </TableRow>}
                  </TableBody>
                </Table>}
                {view == false && <Table size="small" aria-label="simple table">
                  <Grid container spacing={2}>
                    {flag ? element.length > 0 ? element.map((row) => (
                      <Grid item xs={12} sm={6} md={4} key={row._id}>
                        <ListCard {...row} handleClick={handleViewCard} />
                      </Grid>
                    )) : <Grid item xs={12} sm={12} md={12}>
                      <NoData />
                    </Grid> : <Grid item xs={12} sm={12} md={12}>
                      <MobileLoader />
                    </Grid>}
                  </Grid>
                </Table>}
              </TableContainer>

              {/* Mobile View */}
              <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "none" } }}>
                <Table size="small" aria-label="simple table">
                  <TableBody className={`${theme.palette.mode == 'dark' ? 'dark-table' : 'light-table'}`}>
                    {flag ? element.length > 0 ? element.map((row) => (
                      <TableRow key={row._id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                        <TableCell className='customer-table'>
                          <ListCard {...row} handleClick={handleViewCard} />
                        </TableCell>
                      </TableRow>
                    )) : <TableRow>
                      <TableCell colSpan="12">
                        <NoData />
                      </TableCell>
                    </TableRow> : <TableRow>
                      <TableCell colSpan="12">
                        <MobileLoader />
                      </TableCell>
                    </TableRow>}
                  </TableBody>
                </Table>
              </TableContainer>
            </InfiniteScroll>
          </CardContent>
          {/* <div ref={srollerLoad} /> */}

          {/* {element.length > 9 && <Grid container spacing={2}>
            {allCount != element.length && <Grid item xs={12} sm={12} md={12} textAlign="center">
              <LoadingButton loading={loading}
                loadingIndicator="Loading..."
                variant="outlined" disabled={loading} onClick={fetchMoreData}>View More</LoadingButton>
            </Grid>}
          </Grid>} */}
        </Card>
      </Grid>
      <Dialog
        open={data.open}
        onClose={handleClose}
        fullWidth
        aria-labelledby="responsive-dialog-title"
      >
        <WaitingForm allvalue={data} setId={handleClose} />
      </Dialog>
      <DialogBox open={open} handleClose={handleDialogClose} title={'Appointment Detail'} data={DailogData} />
      <Dialog
        open={cancel.open}
        onClose={handleCancelClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Cancel Booking</Box>
          <Box>
            <IconButton onClick={handleCancelClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          Are You Sure You want to Cancel??
        </DialogContent>
        <DialogActions>
          <Button fullWidth variant="contained" color="primary" disabled={loading} onClick={() => { handleCancel(cancel._id) }} >Yes</Button>
          <Button fullWidth variant="contained" color="primary" disabled={loading} onClick={handleCancelClose} >No</Button>
        </DialogActions>
      </Dialog>
      <Dialog open={form} className="bookingPopup"
        disableEscapeKeyDown={true} onClose={() => {
          if (show == true) {

          } else { handleFormClose() }
        }}
        sx={(theme) => ({
          [theme.breakpoints.up('sm')]: {
            minHeight: '770px !important'
          },
          [theme.breakpoints.up('xs')]: {
            minHeight: '450px !important'
          },
          [theme.breakpoints.up('md')]: {
            minHeight: '770px !important'
          },
        })}
      >
        <DatePicker service={{
          label: '', value: '', labelType: '', tagVal: '', location: {
            lattitude: '',
            longitude: '',
            address: ''
          }
        }} close={handleFormClose} some={false} handleView={handleShow} />
      </Dialog>

      <Dialog
        open={remark.open}
        onClose={handleRemarkClose}
        fullWidth
        aria-labelledby="responsive-dialog-title"
      >
        <RemarkForm allvalue={remark} setId={handleRemarkClose} />
      </Dialog>
      <Dialog
        open={book.open}
        onClose={handleBookingClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <Grid>
          <Card>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Box flexGrow={1} >
                      <Typography className='title_head loginHead' variant="h4" component="h2" align="center">
                        Booking Details
                      </Typography>
                    </Box>
                    <Box>
                      <IconButton onClick={handleBookingClose}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>

                <Grid item xs={6} sm={6}>
                  <div>{book.data.description}</div>
                </Grid>
                <Grid item xs={6} sm={6} >
                  <div>Waiting Time</div>
                  <div className='customer-font'>
                    {(!book.data.waiting_number || book.data.waiting_number == null || book.data.waiting_number == undefined) ? book.data?.waiting_time : (book.data?.waiting_number < 60) ? book.data?.waiting_number + ' ' + book.data.waiting_key : getDateChange(book.data.waiting_number)}
                  </div>
                </Grid>
                <Grid item xs={6} sm={6} >
                  <div>Service Time</div>
                  <div className='customer-font'>
                    {(book.data.queue == true) ? (book.data?.approximate_number < 60) ? book.data?.approximate_number + ' ' + book.data.approximate_key : getDateChange(book.data.approximate_number) : '------'}
                  </div>
                </Grid>
                <Grid item xs={6} sm={6} >
                  <div>Created By</div>
                  <div className='customer-font'>
                    {(book.data.create_type == null || book.data.create_type == undefined) ? "........" : (book.data.create_type == "shop") ? book.data?.shop[0]?.shop_name + ' ' + book.data.create_type : (book.data.create_type == "customer") ? book.data?.customer[0]?.name + ' ' + book.data.create_type : (book.data.create_type == "employee") ? book.data?.employee[0]?.name + ' ' + book.data.create_type : book.data.create_type}
                  </div>
                </Grid>
                <Grid item xs={6} sm={6}>
                  <div>
                    <EmployeeSelect value={book.data.assign == null || book.data.assign == undefined ? '' : book.data.assign} _id={book.data._id} handleChange={handleAssign} />
                  </div>
                </Grid>
                <Grid item xs={12} sm={12}>

                  <span className='customer-fonts'>
                    {book.data.waiting_verify == true ? <ProgressTimer time={new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0))} ma={book.data._id} func={handleTimerClose} show={true} /> : '.......'}
                    {book.data.waiting_verify == true ? <NewTimer main={new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0))} /> : ''}
                  </span>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <div>{book.data?.user_id?.name} <Star color="warning" />{book.data.totalUser}</div>
                  <div>{book.data.user_email}</div>
                  <div>{ChangeNumber(book.data.mobile)}</div>
                </Grid>
                <Grid item xs={12} sm={12}>
                  {(!book.data.mobile || book.data.mobile == null || book.data.mobile == undefined) ? '-----' : <IconButton title={book.data.mobile} onClick={() => { handleWhatsapp(book.data.mobile, `Hello Mr.${book.data?.user_id.name}`) }} >
                    <WhatsApp color="success" />
                  </IconButton>}
                  <IconButton title="Add Waiting Time" onClick={() => {
                    if (book.data.queue == true) {
                      setValue("1")
                    } else {
                      handleEdit(book.data)
                    }
                  }}>
                    <Edit color="primary" />
                  </IconButton>
                  <IconButton title="Add Remark" onClick={() => {
                    handleRemarkEdit(book.data)
                  }}>
                    <RMark color="primary" />
                  </IconButton>
                  <IconButton title="Details" onClick={() => { handleDialogOpen(book.data) }} >
                    <MoreVert color="info" />
                  </IconButton>
                  {(new Date(book.data.end_time) > new Date()) && <IconButton title="Cancel Booking" onClick={() => { handleCancelOpen(book.data._id) }} >
                    <Cancel color="disabled" />
                  </IconButton>}
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Dialog>
      <Dialog
        open={appointment.open}
        onClose={handleChangeStatusClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Appointment Status</Box>
          <Box>
            <IconButton onClick={handleChangeStatusClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          {appointment.value == true && 'Are You Sure You Want Open Shop Appointment ??'}
          {appointment.value == false && 'Are You Sure You Want Close Shop Appointment ??'}
        </DialogContent>
        <DialogActions>
          <Button fullWidth variant="contained" color="primary" disabled={loading} onClick={() => { handleChangeStatus(appointment.value) }} >Yes</Button>
          <Button fullWidth variant="contained" color="primary" disabled={loading} onClick={handleChangeStatusClose} >No</Button>
        </DialogActions>
      </Dialog>
    </Grid>
  )
}

export default Dashboard
