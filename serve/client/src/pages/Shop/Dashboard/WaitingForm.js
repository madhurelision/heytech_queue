import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Autocomplete, Button, FormControlLabel, Radio, RadioGroup, FormControl, FormLabel, Box, Typography, IconButton } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from '../../Registration/FormError'
import { bookingTimeUpdate } from 'redux/action'
import moment from 'moment'
import { BookingButton } from "components/common";
import { Close } from '@mui/icons-material'

const StatusElement = ['accepted', 'cancelled', 'waiting']

const TimeDuration = [
  "min", "hour"
]

const WaitingForm = ({ allvalue, setId }) => {

  const [data, setData] = useState({
    waiting_time: allvalue.waiting_time,
    number: parseInt((!allvalue.waiting_number || allvalue.waiting_number == null || allvalue.waiting_number == undefined) ? 0 : allvalue.waiting_number),
    key: (allvalue.waiting_key == 'min') ? 'min' : 'hour',
    status: allvalue.status,
    end_time: ''
  })


  const [btn, setBtn] = useState(true)
  const [err, setErr] = useState({})
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.open)
  const loading = useSelector(state => state.auth.loader)

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])

  const handleSubmit = () => {
    var check = waitingValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    var main_t = createDate(allvalue.appointment_date, parseInt(data.number), data.key)
    // console.log('main_t', main_t)
    dispatch(bookingTimeUpdate(allvalue?._id, {
      waiting_time: data.number + ' ' + data.key,
      waiting_number: data.number,
      waiting_key: data.key,
      status: data.status,
      end_time: main_t
    }))
  }

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
    setBtn(false)
  }

  return (
    <Grid >
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Box flexGrow={1} >
                    <Typography className='title_head loginHead' variant="h4" component="h2" align="center">
                      Booking Waiting Data
                    </Typography>
                  </Box>
                  <Box>
                    <IconButton onClick={setId}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <span>Preferred Date: {moment(allvalue?.appointment_date).format('LL')}</span>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <span>Preferred Time: {moment(allvalue?.appointment_date).calendar()}</span>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField label="Waiting Time" name="number" onChange={handleChange} value={data.number} InputLabelProps={{ shrink: true }} fullWidth placeholder="Enter Waiting Time" type="number" inputProps={{
                  min: '1', step: "1",
                }} />
                {err.number && (<FormError data={err.number}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                {/* <Autocomplete
                  limitTags={2}
                  id="multiple-limit-tags"
                  options={TimeDuration}
                  getOptionLabel={(option) => option}
                  isOptionEqualToValue={(option, value) => option === value}
                  value={data.key}
                  onChange={(e, n, r) => {
                    console.log('check', r, 'new', n, 'rest', r)
                    setData({ ...data, key: n })
                    setErr({ ...err, key: '' })
                    setBtn(false)
                  }}
                  filterSelectedOptions={true}
                  renderInput={(params) => (
                    <TextField {...params} label="Waiting Time" placeholder="Add Waiting Time" />
                  )}
                /> */}
                <FormControl component="fieldset">
                  <FormLabel component="legend">Waiting Time</FormLabel>
                  <RadioGroup row aria-label="waitingtime" name="key" onChange={handleChange} value={data.key}>
                    <FormControlLabel value='min' control={<Radio />} label="Min" />
                    <FormControlLabel value='hour' control={<Radio />} label="Hour" />
                  </RadioGroup>
                </FormControl>
                {err.key && (<FormError data={err.key}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                {/* <Autocomplete
                  limitTags={2}
                  id="multiple-limit-tags"
                  options={StatusElement}
                  getOptionLabel={(option) => option}
                  isOptionEqualToValue={(option, value) => option === value}
                  value={data.status}
                  onChange={(e, n, r) => {
                    console.log('check', r, 'new', n, 'rest', r)
                    setData({ ...data, status: n })
                    setErr({ ...err, status: '' })
                    setBtn(false)
                  }}
                  filterSelectedOptions={true}
                  renderInput={(params) => (
                    <TextField {...params} label="Status" placeholder="Status" />
                  )}
                /> */}
                <FormControl component="fieldset">
                  <FormLabel component="legend">Status</FormLabel>
                  <RadioGroup row aria-label="status" name="status" onChange={handleChange} value={data.status}>
                    <FormControlLabel disabled={data.status == 'accepted'} value='waiting' control={<Radio />} label="waiting" />
                    <FormControlLabel disabled={data.status == 'accepted'} value='accepted' control={<Radio />} label="accepted" />
                  </RadioGroup>
                </FormControl>
                {err.status && (<FormError data={err.status}></FormError>)}
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }}>
              <Grid item xs={6} align="left">
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId() }}>Close</BookingButton>
              </Grid>
              <Grid item xs={6} align="right">
                <BookingButton type="button" disabled={(loading || btn) ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Submit</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default WaitingForm


const waitingValidate = (data) => {
  let errors = {}

  if (!data.number) {
    errors["number"] = "Waiting Time is Required"
  }

  if (!data.key) {
    errors["key"] = "Waiting key is Required"
  }

  if (!data.status) {
    errors["status"] = "Status is Required"
  }

  return errors
}

const createDate = (date, time, key) => {
  var now = new Date(date);
  if (key == 'min') {
    now.setMinutes(now.getMinutes() + time);
  } else {
    now.setMinutes(now.getMinutes() + time * 60)
  }
  now.setMilliseconds(0);
  return now
}