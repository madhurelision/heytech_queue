import React from 'react'
import { Grid, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Rating, List, ListItem, ListItemText, ListItemAvatar, Divider, Avatar, Typography } from '@mui/material'
import { DetailStyles } from './styles'
import StatusBox from '../UsableComponent/StatusBox'
import moment from 'moment'
import Test from 'pages/Clock/Test'
import { useLocation } from 'react-router'
import TooltipBox from '../UsableComponent/TooltipBox'
import { getDateChange } from 'config/KeyData'

const ShopAppointmentView = ({ item }) => {

  const classes = DetailStyles()
  const location = useLocation()

  return (
    <Grid container spacing={2} className={classes.grid}>
      {(location.pathname == '/shop' || location.pathname == '/shop/queue' || location.pathname == '/shop/appoinment' || location.pathname == '/employee' || location.pathname == '/employee/booking') && <Grid item xs={12} md={10} sm={12}>
        {(item.waiting_verify == true && item.end_time !== null) ? <Test maxTime={parseInt(item.waiting_time) * 60} maxDate={new Date(item.end_time).setSeconds(parseInt((item.durationVal !== null && item.durationVal !== undefined) ? item.durationVal : 0))} /> : null}
      </Grid>}
      <Grid item xs={12} md={4} sm={12}>
        <TableContainer component={Paper}>
          <Table size="small" aria-label="simple table">
            <TableBody>
              <TableRow>
                <TableCell></TableCell>
                <TableCell></TableCell>
              </TableRow>
              <TableRow>
                <TableCell>User Size</TableCell>
                <TableCell>{(!item.size || item.size == null || item.size == undefined) ? '----' : item.size}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Appointment Date</TableCell>
                <TableCell>{moment(item.appointment_date).format('LL')}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Appointment Time</TableCell>
                <TableCell>{moment(item.appointment_date).calendar()}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Waiting Time</TableCell>
                <TableCell>{(!item.waiting_number || item.waiting_number == null || item.waiting_number == undefined) ? '---------' : (item?.waiting_number < 60) ? item?.waiting_number + ' ' + item.waiting_key : getDateChange(item.waiting_number)}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Reach Time</TableCell>
                <TableCell>{(!item.end_time || item.end_time == null || item.end_time == undefined) ? '----' : moment(new Date(item.end_time).setSeconds(parseInt((item.durationVal !== null && item.durationVal !== undefined) ? item.durationVal : 0))).calendar()}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>User Name</TableCell>
                <TableCell>{item?.user_id.name}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>User Number</TableCell>
                <TableCell>{(!item.mobile || item.mobile == null || item.mobile == undefined) ? '-----' : item.mobile}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>User Email</TableCell>
                <TableCell>{item.user_email}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Service Name</TableCell>
                <TableCell>{item?.service_id.name}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Seat</TableCell>
                <TableCell>{item?.seat}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Service Time</TableCell>
                <TableCell> {(item?.service_id?.waiting_number < 60) ? item?.service_id?.waiting_number + ' ' + item?.service_id?.waiting_key : getDateChange(item?.service_id?.waiting_number)}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Service End Time</TableCell>
                <TableCell> {(item.queue == true) ? (item?.approximate_number < 60) ? item?.approximate_number + ' ' + item.approximate_key : getDateChange(item.approximate_number) : '------'}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Status</TableCell>
                <TableCell><StatusBox value={item.status} /></TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Description</TableCell>
                <TableCell sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
                  <TooltipBox title="hover" data={item.description} />
                </TableCell>
                <TableCell sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }}>
                  {item.description}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      {(location.pathname == '/shop/history') && <Grid container width="100%" >
        <Grid item xs={12} md={4} sm={12} textAlign="center"></Grid>
        <Grid item xs={12} md={4} sm={12} textAlign="center">
          {item.review.length > 0 && item.shop_review == true ? <List>
            <h2>Reviews</h2>
            {
              item.review.map((row) => (
                (row.shop_review == true) ? <React.Fragment key={row._id}>
                  <ListItem alignItems="flex-start">
                    <ListItemAvatar>
                      <Avatar alt={"Notify"} src={row?.shop_image}>
                        {row?.shop_name.slice(0, 1)}
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                      primary={row?.shop_name}
                      secondary={
                        <React.Fragment>
                          <Typography
                            sx={{ display: 'inline' }}
                            component="span"
                            variant="body2"
                            color="text.primary"
                          >
                            <Rating name="read-only" value={row.rating} readOnly />
                          </Typography>
                          {" — "} {row?.description}
                        </React.Fragment>
                      }
                    />
                  </ListItem>
                  <Divider variant="inset" component="li" />
                </React.Fragment> : <React.Fragment>
                  <ListItem alignItems="flex-start">
                    <ListItemAvatar>
                      <Avatar alt={"Notify"} src={row?.image}>
                        {row?.user_name.slice(0, 1)}
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                      primary={row?.user_name}
                      secondary={
                        <React.Fragment>
                          <Typography
                            sx={{ display: 'inline' }}
                            component="span"
                            variant="body2"
                            color="text.primary"
                          >
                            <Rating name="read-only" value={row.rating} readOnly />
                          </Typography>
                          {" — "} {row?.description}
                        </React.Fragment>
                      }
                    />
                  </ListItem>
                  <Divider variant="inset" component="li" />
                </React.Fragment>
              ))
            }
          </List> : <div style={{ textAlign: 'center' }}>No Review Found</div>}
        </Grid>
      </Grid>}
    </Grid>
  )
}

export default ShopAppointmentView
