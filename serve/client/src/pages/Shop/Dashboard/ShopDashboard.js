import React, { useState } from 'react';
import { styled } from '@mui/material/styles';
import { TabContext, TabList, TabPanel } from '@mui/lab';
import { Mail, BarChart } from '@mui/icons-material';
import { useSelector } from 'react-redux';
import { Badge, Tab, Box } from '@mui/material';
import Dashboard from 'pages/Shop/Dashboard/Dashboard';
import ShopQueue from 'pages/Shop/Queue/ShopQueue';
import { tableStyles } from 'pages/Shop/Dashboard/styles';
import ShopQueueTable from '../Queue/ShopQueueTable';
import Insight from '../Insight/Insight';

const StyledTab = styled((props) => <Tab disableRipple {...props} />)(
  ({ theme }) => ({
    textTransform: 'none',
    fontWeight: theme.typography.fontWeightRegular,
    fontSize: theme.typography.pxToRem(15),
    minHeight: '50px',
    marginRight: theme.spacing(1),
    color: theme.palette.mode == 'dark' ? '#f5f5f5' : 'black',
    '&.Mui-selected': {
      color: '#d6a354 !important',
    },
    '&.Mui-focusVisible': {
      backgroundColor: 'rgba(100, 95, 228, 0.32)',
    },
    '&.MuiTabs-indicator': {
      display: 'none',
      left: '271px !important'
    }
  }),
);

const ShopDashboard = () => {

  const classes = tableStyles()
  const [value, setValue] = React.useState("0");
  const countData = useSelector((state) => state.auth);
  const [data, setData] = useState('All')

  const handleChange = (event, newValue) => {
    console.log('value', newValue)
    setValue(newValue);
  };

  const handleBtn = (val) => {
    setData(val)
  }

  return (
    <Box className={classes.tabgrid}>
      <Box>
        <TabContext value={value}>
          <TabList
            onChange={handleChange}
            aria-label="styled tabs example"
            centered
          >
            <StyledTab className='title_head' label="Queue" value="0" icon={<Badge color="secondary" badgeContent={countData?.shopQueueCount}>
              <Mail />
            </Badge>} iconPosition="end" />
            <StyledTab className='title_head' label="Appointment" value="1" icon={<Badge color="secondary" badgeContent={countData?.shopBookingCount}>
              <Mail />
            </Badge>} iconPosition="end" />
            <StyledTab className='title_head' label="Insight" value="2" icon={<Badge color="secondary">
              <BarChart />
            </Badge>} iconPosition="end" />
          </TabList>
          {/* <Box sx={{ p: 3 }} /> */}
          <TabPanel sx={{ padding: '4px !important' }} value={"0"}>
            <ShopQueueTable data={data} handleBtn={handleBtn} />
            <ShopQueue shopVal={data} />
          </TabPanel>
          <TabPanel sx={{ padding: '4px !important' }} value={"1"}>
            <Dashboard setValue={setValue} />
          </TabPanel>
          <TabPanel sx={{ padding: '4px !important' }} value={"2"}>
            <Insight />
          </TabPanel>
        </TabContext>
      </Box>
    </Box>
  );
}

export default ShopDashboard