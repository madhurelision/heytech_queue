import { Close } from '@mui/icons-material';
import { AppBar, Dialog, Slide, Toolbar, IconButton, List, ListItem, Button, Typography, Divider, ListItemText, DialogContent, DialogTitle, useTheme } from '@mui/material';
import React from 'react'
import moment from 'moment'
import DateTimer from '../UsableComponent/DateTimer';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const DialogBox = ({ open, handleClose, title, data }) => {

  const theme = useTheme()

  return (
    <Dialog
      fullScreen
      open={open}
      onClose={handleClose}
      TransitionComponent={Transition}
    >
      <AppBar sx={{ position: 'relative', backgroundColor: theme.palette.mode == 'dark' ? '#242424' : '#252525' }}>
        <Toolbar>
          <Typography className='title_head' sx={(theme) => ({
            [theme.breakpoints.up('sm')]: {
              ml: 2, flex: 1, fontSize: '1.5rem'
            },
            [theme.breakpoints.up('xs')]: {
              ml: 2, flex: 1, fontSize: '1rem'
            },
            [theme.breakpoints.up('md')]: {
              ml: 2, flex: 1, fontSize: '1.5rem'
            },
            [theme.breakpoints.up('lg')]: {
              ml: 2, flex: 1, fontSize: '2.5rem'
            }
          })} variant="h6" component="div">
            {title}
          </Typography>
          <Typography className='title_head' sx={(theme) => ({
            [theme.breakpoints.up('sm')]: {
              ml: 2, flex: 1, fontSize: '1.5rem'
            },
            [theme.breakpoints.up('xs')]: {
              ml: 2, flex: 1, fontSize: '1rem'
            },
            [theme.breakpoints.up('md')]: {
              ml: 2, flex: 1, fontSize: '1.5rem'
            },
            [theme.breakpoints.up('lg')]: {
              ml: 2, flex: 1, fontSize: '2.5rem'
            }
          })} variant="h6" component="div">
            <DateTimer />
          </Typography>
          <IconButton
            edge="end"
            color="inherit"
            onClick={handleClose}
            aria-label="close"
          >
            <Close />
          </IconButton>
        </Toolbar>
      </AppBar>
      {/* <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
        <Box flexGrow={1} >{title}</Box>
        <Box>
          <IconButton onClick={handleClose}>
            <Close />
          </IconButton>
        </Box>
      </Box></DialogTitle> */}
      <DialogContent>
        {data}
      </DialogContent>
    </Dialog>
  )
}

export default DialogBox
