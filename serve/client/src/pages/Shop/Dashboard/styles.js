import { makeStyles } from '@mui/styles'

export const tableStyles = makeStyles(() => ({
  grid: {
    paddingInline: '15px',
    paddingBlock: '15px',
    justifyContent: 'center',
    overflowX: 'auto',
    height: '100%'
  },
  gridS: {
    justifyContent: 'center',
    overflowX: 'auto',
    height: '100%'
  },
  mobilegrid: {
    backgroundColor: '#342f2f !important'
  },
  cardTitle: {
    fontSize: '0.75rem'
  },
  cardValue: {
    fontSize: '1.0rem'
  },
  tabgrid: {
    justifyContent: 'center',
    overflowX: 'auto',
    height: '100%'
  },
  ssgrid: {
    paddingInline: '15px',
    paddingBlock: '15px',
    justifyContent: 'center',
  },
}))

export const statusStyles = makeStyles(() => ({
  waiting: {
    fontSize: '1.2rem !important',
    color: 'red !important',
    backgroundColor: 'null !important'
  },
  accepted: {
    fontSize: '16px !important',
    color: '#19b819 !important',
    backgroundColor: 'null !important',
    padding: '0 0 14px 0'
  },
  running: {
    fontSize: '1.2rem !important',
    color: '#19b819 !important',
    backgroundColor: 'null !important'
  },
  cancelled: {
    fontSize: '1.2rem !important',
    color: 'red !important',
    backgroundColor: 'null !important'
  },
  completed: {
    fontSize: '1.2rem !important',
    color: '#19b819 !important',
    backgroundColor: 'null !important'
  },
  expired: {
    fontSize: '1.2rem !important',
    color: 'red !important',
    backgroundColor: 'null !important'
  }
}))

export const CircularTimer = makeStyles(() => ({
  timer: {
    position: 'absolute',
    top: '94px',
    left: '44%'
  }
}))

export const DetailStyles = makeStyles(() => ({
  grid: {
    paddingInline: '15px',
    paddingBlock: '15px',
    justifyContent: 'center',
    overflowX: 'auto',
  }
}))

export const employeeStyles = makeStyles(() => ({
  grid: {
    justifyContent: 'center',
    overflowX: 'auto',
    height: '100%'
  },
  mobilegrid: {
    backgroundColor: '#342f2f !important'
  },
  cardgrid: {
    paddingInline: '15px',
    justifyContent: 'center',
    overflowX: 'auto',
    height: '100%'
  },
}))

export const tabStyles = makeStyles((theme) => ({
  font: {
    fontSize: '10px'
  },
  grid: {
    color: theme.palette.mode == 'dark' ? '#ffff' : 'black',
    cursor: 'pointer',
    '&:hover': {
      color: '#d6a354',
    }
  },
  selectGrid: {
    color: '#d6a354',
    cursor: 'pointer',
    '&:hover': {
      color: '#d6a354',
    }
  }
}))

export const bookingStatusStyles = makeStyles(() => ({
  waiting: {
    color: 'red !important',
    backgroundColor: 'null !important'
  },
  accepted: {
    color: '#19b819 !important',
    backgroundColor: 'null !important'
  },
  running: {
    color: '#19b819 !important',
    backgroundColor: 'null !important'
  },
  cancelled: {
    color: 'red !important',
    backgroundColor: 'null !important'
  },
  completed: {
    color: '#19b819 !important',
    backgroundColor: 'null !important'
  },
  expired: {
    color: 'red !important',
    backgroundColor: 'null !important'
  }
}))