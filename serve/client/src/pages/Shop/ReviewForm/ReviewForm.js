import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, Button, FormLabel, RadioGroup, Radio, FormControl, FormControlLabel, Typography, Chip, Rating, useTheme, useMediaQuery, Box, IconButton } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import '../../Registration/register.css'
import { customerUserReview, getAllReviewTag, shopUserReview } from 'redux/action'
import { ReviewTag } from 'config/KeyData'
import { Star, Close } from '@mui/icons-material'
import { yellow } from '@mui/material/colors'
import { BookingButton } from "components/common";

const ReviewForm = ({ allvalue, setId, type }) => {

  const theme = useTheme()
  const matches = useMediaQuery(theme.breakpoints.up('md'));
  const [data, setData] = useState({
    rating: '',
    description: '',
    shop_id: allvalue.shop_id,
    user_id: allvalue.user_id,
    shop_email: allvalue.shop_email,
    shop_name: allvalue.shop_name,
    user_name: allvalue.user_name,
    user_email: allvalue.user_email,
    booking_id: allvalue.booking_id,
    service_id: allvalue.service_id,
    image: allvalue.image,
    shop_image: allvalue.shop_image,
    mobile: allvalue.mobile,
  })


  const [err, setErr] = useState({})
  const [tag, setTag] = useState([])
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.updated)
  const loading = useSelector(state => state.auth.loader)
  const reviewTagD = useSelector(state => state.auth.reviewTagList)

  useEffect(() => {

    if (reviewTagD.length == 0) {
      dispatch(getAllReviewTag())
    } else {
      setTag(reviewTagD.review)
    }

  }, [reviewTagD])

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])


  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    var check = reviewValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    if (type == true) {
      dispatch(customerUserReview(data))
    } else {
      dispatch(shopUserReview(data))
    }
  }

  const handleTagBtn = (item) => {
    setData({ ...data, description: data.description.length == 0 ? data.description.concat(item) : data.description.concat(' ', item) })
    setErr({ ...err, description: '' })
  }

  return (
    <Grid >
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Box flexGrow={1} >
                    <Typography className='title_head loginHead' variant="h4" component="h2" align="center">
                      Review Form
                    </Typography>
                  </Box>
                  <Box>
                    <IconButton onClick={setId}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                {/* <FormControl component="fieldset">
                  <FormLabel component="legend">Rating</FormLabel>
                  <RadioGroup row aria-label="rating" name="rating" onChange={handleChange} value={data.rating}>
                    <FormControlLabel value="1" control={<Radio />} label={<Star sx={{ color: yellow[500], fontSize: '1.1rem' }} />} />
                    <FormControlLabel value="2" control={<Radio />} label={<><Star sx={{ color: yellow[500], fontSize: '1.1rem' }} /><Star sx={{ color: yellow[500], fontSize: '1.1rem' }} /></>} />
                    <FormControlLabel value="3" control={<Radio />} label={<><Star sx={{ color: yellow[500], fontSize: '1.1rem' }} /><Star sx={{ color: yellow[500], fontSize: '1.1rem' }} /><Star sx={{ color: yellow[500], fontSize: '1.1rem' }} /></>} />
                    <FormControlLabel value="4" control={<Radio />} label={<><Star sx={{ color: yellow[500], fontSize: '1.1rem' }} /><Star sx={{ color: yellow[500], fontSize: '1.1rem' }} /><Star sx={{ color: yellow[500], fontSize: '1.1rem' }} /><Star sx={{ color: yellow[500], fontSize: '1.1rem' }} /></>} />
                    <FormControlLabel value="5" control={<Radio />} label={<><Star sx={{ color: yellow[500], fontSize: '1.1rem' }} /><Star sx={{ color: yellow[500], fontSize: '1.1rem' }} /><Star sx={{ color: yellow[500], fontSize: '1.1rem' }} /><Star sx={{ color: yellow[500], fontSize: '1.1rem' }} /><Star sx={{ color: yellow[500], fontSize: '1.1rem' }} /></>} />
                  </RadioGroup>
                </FormControl> */}
                <div>Rating</div>
                <Rating name="rating" onChange={handleChange} value={parseInt(data.rating)} size="large" />
                {err.rating && (<FormError data={err.rating}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <div>Description</div>
                <textarea name="description" value={data.description} cols="40" rows="6" className={`description ${theme.palette.mode == 'dark' ? 'description-dark' : 'description-light'}`} onChange={handleChange}></textarea>
                {err.description && (<FormError data={err.description}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={12} sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
                {tag.length > 0 && <>
                  {tag.map((cc) => {
                    return (
                      <Chip key={cc._id} size="small" onClick={() => { handleTagBtn(cc.type) }} label={cc.name} variant="outlined"></Chip>
                    )
                  })}
                </>}
              </Grid>
              <Grid item xs={12} sm={12} md={12} sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }}>
                {tag.length > 0 && <Grid container spacing={1}>
                  {tag.map((cc) => {
                    return (
                      <Grid key={cc._id} item xs={6}>
                        <Chip size="small" sx={{
                          height: '100%',
                          '.MuiChip-label': {
                            overflowWrap: 'break-word',
                            whiteSpace: 'normal',
                            textOverflow: 'clip'
                          }
                        }} onClick={() => { handleTagBtn(cc.type) }} label={cc.name} variant="outlined"></Chip>
                      </Grid>
                    )
                  })}
                </Grid>}
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }}>
              <Grid item xs={6} align="left">
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId() }}>Close</BookingButton>
              </Grid>
              <Grid item xs={6} align="right">
                <BookingButton type="button" disabled={(loading) ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Submit</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default ReviewForm

const reviewValidate = (data) => {
  let errors = {}

  if (!data.rating) {
    errors["rating"] = "Rating is Required"
  }

  if (!data.description) {
    errors["description"] = "Description is Required"
  }
  return errors
}