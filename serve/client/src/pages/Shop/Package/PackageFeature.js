import React, { useState } from 'react'
import CreatePackageFeature from './CreatePackageFeature'
import PackageFeatureList from './PackageFeatureList'

const PackageFeature = () => {

  const [id, setId] = useState(0)

  switch (id) {
    case 0:
      return <PackageFeatureList setId={setId} />
    case 1:
      return <CreatePackageFeature setId={setId} />
    default:
      return <PackageFeatureList setId={setId} />
  }
}

export default PackageFeature
