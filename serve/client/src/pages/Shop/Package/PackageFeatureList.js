import React, { useEffect, useState } from 'react'
import { Grid, Button, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Typography, Card, CardContent, IconButton, Dialog, DialogTitle, DialogContent, Box, FormGroup, Collapse, CardMedia, DialogActions, Stack, Chip, Checkbox } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { getShopFeature, featureDelete } from 'redux/action'
import { AddCircleOutline, Close, DesignServices, Edit, Refresh, KeyboardArrowDown, KeyboardArrowUp, Delete, CheckCircleOutline, HighlightOff } from '@mui/icons-material'
import { LoadingButton } from '@mui/lab'
import EditPackageFeature from './EditPackageFeature'
import TableLoader from '../UsableComponent/TableLoader'
import NoData from '../UsableComponent/NoData'
import { tableStyles } from '../Dashboard/styles'
import ImageBox from '../UsableComponent/ImageBox'
import TooltipBox from '../UsableComponent/TooltipBox'
import { AntSwitch, BookingButton } from "components/common";
import MobileLoader from '../UsableComponent/MobileLoader'
import '../Queue/queue.css'
import CreatePackageFeature from './CreatePackageFeature'

const PackageFeatureList = ({ setId }) => {

  const classes = tableStyles()
  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const [form, setForm] = useState(false)
  const packageData = useSelector((state) => state.auth.packageFeatureList)
  const allCount = useSelector(state => state.auth.packageFeatureTotal)
  const cted = useSelector((state) => state.auth.created)
  const loading = useSelector((state) => state.auth.loader)
  const dispatch = useDispatch()
  const [data, setData] = useState({
    open: false,
    package_id: '',
    service: '',
    _id: ''
  })

  const [cancel, setCancel] = useState({
    open: false,
    _id: ''
  })

  useEffect(() => {

    if (packageData.length == 0) {
      dispatch(getShopFeature(0, 10))
    } else {
      setElement(packageData.feature)
      setFlag(true)
    }

    if (cted) {
      setCancel({ ...cancel, open: false })
      dispatch(getShopFeature(0, element.length == 0 ? 10 : element.length))
    }

  }, [packageData, cted])

  const handleEdit = (val) => {
    setData({
      ...data,
      open: true,
      package_id: val.package_id._id,
      service: val.service,
      _id: val._id
    })
  }

  const handleClose = () => {
    setData({ ...data, open: false })
  }

  const handleRefresh = () => {
    dispatch(getShopFeature(0, element.length > 0 ? element.length : 10))
  }

  const handleDeleteOpen = (val) => {
    setCancel({
      ...cancel,
      open: true,
      _id: val
    })
  }

  const handleDeleteClose = () => {
    setCancel({ ...cancel, open: false })
  }

  const handleDelete = (row) => {
    dispatch(featureDelete(row))
  }

  const fetchMoreData = () => {
    dispatch(getShopFeature(element.length, 10))
  };

  const handleFormClose = () => {
    setForm(false)
  }

  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card>
          <CardContent>

            <Grid className="serviceListHead" container spacing={2}>
              <Grid item xs={12} sm={2} md={2} >
                <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh} >
                  Refresh
                </Button>
              </Grid>
              <Grid item xs={12} sm={7} md={8}>
                <Typography className='title_head' variant="h5" component="div" align="center">
                  Package Feature List
                </Typography>
              </Grid>
              <Grid item xs={12} sm={3} md={2}>
                <Button className='customer-btn' startIcon={<AddCircleOutline color='primary' />} onClick={() => { setForm(true) }} >
                  Create
                </Button>
              </Grid>
            </Grid>
            <TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "block" } }}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Package Id</TableCell>
                    <TableCell>Service</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <TableRow
                      key={row._id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell>
                        <div style={{ display: 'flex' }}>
                          <IconButton title="Edit" onClick={() => { handleEdit(row) }}>
                            <Edit color='primary' />
                          </IconButton>
                          <IconButton title="Delete" onClick={() => { handleDeleteOpen(row._id) }}>
                            <Delete color='warning' />
                          </IconButton>
                        </div>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {row.package_id.name}
                      </TableCell>
                      <TableCell>{row.service.map((cc) => <Chip key={cc._id} label={cc.name} />)}</TableCell>
                    </TableRow>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <TableLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            {/* Mobile View */}
            <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "none" } }} >
              <Table size="small" aria-label="simple table">
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <React.Fragment key={row._id}>
                      <TableRow
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell className='customer-table'>
                          <Card sx={{ borderRadius: '18px' }}>
                            <CardMedia
                              component="img"
                              sx={{ width: '100%', objectFit: 'unset', borderRadius: '18px', border: 1 }}
                              image={row.package_id.image}
                              alt={row.package_id.name}
                            />
                            <Box>
                              <CardContent>
                                <Grid container spacing={2}>
                                  <Grid item xs={6} sm={6} >
                                    <div>Package Name</div>
                                    <div className='customer-font'>{row.package_id.name}</div>
                                  </Grid>
                                  <Grid item xs={6} sm={6} >
                                    <div>Service</div>
                                    <div className='customer-font'>
                                      {row.service.map((cc) => <Chip key={cc._id} label={cc.name} />)}
                                    </div>
                                  </Grid>
                                </Grid>
                              </CardContent>
                            </Box>
                          </Card>
                        </TableCell>
                      </TableRow>
                    </React.Fragment>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <MobileLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
          </CardContent>
          {element.length >= 10 && <Grid container spacing={2}>
            {allCount != element.length && <Grid item xs={12} sm={12} md={12} textAlign="center">
              <LoadingButton loading={loading}
                loadingIndicator="Loading..."
                variant="outlined" disabled={loading} onClick={fetchMoreData} >View More</LoadingButton>
            </Grid>}
          </Grid>}
        </Card>
      </Grid>
      <Dialog
        open={data.open}
        onClose={handleClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <EditPackageFeature allvalue={data} setId={handleClose} />
      </Dialog>
      <Dialog
        open={cancel.open}
        onClose={handleDeleteClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Delete Feature</Box>
          <Box>
            <IconButton onClick={handleDeleteClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          Are You Sure You want to Delete??
        </DialogContent>
        <DialogActions>
          <BookingButton fullWidth variant="contained" disabled={loading} onClick={() => { handleDelete(cancel._id) }} >Yes</BookingButton>
          <BookingButton fullWidth variant="contained" disabled={loading} onClick={handleDeleteClose} >No</BookingButton>
        </DialogActions>
      </Dialog>
      <Dialog
        open={form}
        onClose={() => {
          if (loading == true) {

          } else {
            handleFormClose()
          }
        }}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <CreatePackageFeature setId={handleFormClose} />
      </Dialog>
    </Grid>
  )
}

export default PackageFeatureList
