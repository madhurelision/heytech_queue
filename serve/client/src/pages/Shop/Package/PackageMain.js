import React, { useState } from 'react'
import CreatePackage from './CreatePackage'
import PackageList from './PackageList'

const PackageMain = () => {

  const [id, setId] = useState(0)

  switch (id) {
    case 0:
      return <PackageList setId={setId} />
    case 1:
      return <CreatePackage setId={setId} />
    default:
      return <PackageList setId={setId} />
  }
}

export default PackageMain
