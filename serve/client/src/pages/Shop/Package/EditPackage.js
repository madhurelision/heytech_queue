import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Dialog, DialogContent, Typography, DialogTitle, Box, IconButton, useTheme, useMediaQuery, MenuItem, FormControl, InputLabel, Select, Checkbox } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import '../../Registration/register.css'
import { getShopService, packageupdate, } from 'redux/action'
import NewImageCropper from '../../ImageCropper/NewImageCropper'
import { Close } from '@mui/icons-material'
import { BookingButton } from "components/common";
import { MonthType } from 'config/KeyData'
import EditPackageFeature from './EditPackageFeature'
import Autocomplete from '@mui/material/Autocomplete';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckBoxIcon from '@mui/icons-material/CheckBox';

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

const EditPackage = ({ allvalue, setId }) => {

  const theme = useTheme()
  const [data, setData] = useState({
    name: allvalue.name,
    image: '',
    description: allvalue.description,
    price: allvalue.price,
    view: allvalue.view,
    btn: allvalue.btn,
    oldimage: allvalue.image,
    service: allvalue?.service,
  })
  const [open, setOpen] = useState({
    open: false,
    name: '',
    image: ''
  })
  const [val, setVal] = useState({
    open: false,
    package_id: allvalue?.package_id,
    service: allvalue?.service,
    _id: allvalue?.feature_id
  })

  const handleEditFClose = () => {
    setVal({ ...val, open: false })
  }

  const [image, setImage] = useState(allvalue.image)
  const [err, setErr] = useState({})
  const [view, setView] = useState(false)
  const [btn, setBtn] = useState(true)
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.created)
  const checkVal = useSelector(state => state.auth.complete)
  const loading = useSelector(state => state.auth.loader)
  const galleryData = useSelector((state) => state.auth.shopGallery)
  const [main, setMain] = useState([])
  const serviceData = useSelector((state) => state.auth.shopService)

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

    if (checkVal) {
      setId(0)
    }

  }, [mainVal, checkVal])


  useEffect(() => {
    if (serviceData.length == 0) {
      dispatch(getShopService(0, 10))
    } else {
      setMain(serviceData.service)
    }
  }, [serviceData])

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
    setBtn(false)
  }

  const onHandleImage = (e) => {

    if (e.target.files == null || e.target.files == undefined) {
      return
    }

    const file = Math.round((e.target.files[0].size / 1024 * 1024));
    console.log('Image Size', e.target.files[0].size, 'Image Size Mb', file)

    if (file > 512000) {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      setErr({ ...err, [e.target.name]: 'Image Size Should be less than 500 kb' })
      return
    }

    const VIDEO_FORMATS = ["video/mp4", "video/mkv"];
    console.log('Image Console', e.target.name, e.target.files)
    console.log('Main', document.getElementsByName(e.target.name)[0].value)
    if (!VIDEO_FORMATS.includes(e.target.files[0].type)) {
      setData({ ...data, [e.target.name]: e.target.files[0] })
      setErr({ ...err, [e.target.name]: '' })
      setOpen({ ...open, open: true, name: e.target.name, image: URL.createObjectURL(e.target.files[0]) })
      setBtn(false)
    } else {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      return
    }
  }


  const CrooppedImageN = (imgss) => {
    console.log('Cropped Image Called', imgss)
    var ext = (imgss.type == "image/jpeg") ? '.jpg' : (imgss.type == "image/png") ? '.png' : '.jpg'
    const values = blobToFile(imgss, open.name + ext)
    console.log('Cropped Image Called', values)
    setData({ ...data, [open.name]: values })
    setOpen({ ...open, open: false })
    setImage(URL.createObjectURL(values))
  }

  const blobToFile = (theBlob, fileName) => {
    return new File([theBlob], fileName, { lastModified: new Date().getTime(), type: theBlob.type })
  }

  const handleClose = () => {
    document.getElementsByName(open.name)[0].value = ''
    setData({ ...data, [open.name]: '' })
    setOpen({ ...open, open: false })
  }

  const handleSubmit = () => {
    var check = packageValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)

    var formData = new FormData()
    formData.append('name', data.name)
    formData.append('price', data.price)
    formData.append('image', data.image)
    formData.append('oldimage', data.oldimage)
    formData.append('description', data.description)
    formData.append('view', data.view)
    formData.append('btn', data.btn)

    dispatch(packageupdate(allvalue?._id, formData))
  }

  const selectImage = (item) => {
    setData({ ...data, oldimage: item })
    setImage(item)
    setBtn(false)
    setView(false)
  }

  const handleViewClose = () => {
    setView(!view)
  }

  return (
    <Grid >
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid className="creatPop" item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                    Edit Package
                  </Typography>
                  <Box className="crossIcon">
                    <IconButton onClick={() => { setId() }}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="name" type="text" value={data.name} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Name" style={{ marginBottom: '10px' }} />
                {err.name && (<FormError data={err.name}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="price" inputProps={{ min: "0", step: "1" }} type="number"
                  value={data.price} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Price" />
                {err.price && (<FormError data={err.price}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="btn" type="text" value={data.btn} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Button Name" style={{ marginBottom: '10px' }} />
                {err.btn && (<FormError data={err.btn}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <div>
                  <FormControl fullWidth variant="outlined">
                    <InputLabel id="demo-simple-select-label" shrink={true}>Duration</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={data.view}
                      label="Type"
                      name="view"
                      onChange={handleChange}
                    >
                      {MonthType.map((cc) => {
                        return (
                          <MenuItem key={cc} value={cc}>{cc}</MenuItem>
                        )
                      })}
                    </Select>
                  </FormControl>
                </div>
                {err.view && (<FormError data={err.view}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <Typography variant="body1" className="shop-image-title" component="div" align="left">
                  Description
                </Typography>
                <textarea name="description" value={data.description} cols="40" rows="10" className={`description ${theme.palette.mode == 'dark' ? 'description-dark' : 'description-light'}`} onChange={handleChange}></textarea>
                {err.description && (<FormError data={err.description}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="image" type="file" fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={onHandleImage} label="Image" />
                {err.image && (<FormError data={err.image}></FormError>)}
                {galleryData.length > 0 || galleryData.gallery != null && <BookingButton type="button" variant="contained" className="shop-image-select-pr-btn" onClick={() => { setView(!view) }} >Select Images</BookingButton>}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Autocomplete
                  multiple
                  id="checkboxes-tags-demo"
                  options={main}
                  value={data.service}
                  disableCloseOnSelect
                  getOptionLabel={(option) => option.name}
                  // filterSelectedOptions={true}
                  isOptionEqualToValue={(option, value) => option._id === value._id}
                  renderOption={(props, option, { selected }) => (
                    <li {...props}>
                      <Checkbox
                        icon={icon}
                        checkedIcon={checkedIcon}
                        style={{ marginRight: 8 }}
                        checked={selected}
                      />
                      {option.name}
                    </li>
                  )}
                  // onChange={(e, n, r) => {
                  //   console.log('check', r, 'new', n, 'rest', r)
                  //   setData({ ...data, service: n })
                  //   setErr({ ...err, service: '' })
                  // }}
                  renderInput={(params) => (
                    <TextField {...params} label="Features" placeholder="Service" />
                  )}
                />
                <BookingButton type="button" className="shop-image-select-pr-btn" variant="contained" onClick={() => { setVal({ ...val, open: true }) }} >Edit</BookingButton>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <img src={image} width="100%" height="100%" />
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }}>
              <Grid item xs={6} align="left">
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId(1) }} >Close</BookingButton>
              </Grid>
              <Grid item xs={6} align="right">
                <BookingButton type="button" disabled={(loading || btn) ? true : false} variant="contained" onClick={() => { handleSubmit() }} >Submit</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
      <Dialog open={open.open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description" maxWidth="md" fullWidth>
        <DialogContent>
          <NewImageCropper img={open.image} click={CrooppedImageN} ratio={16 / 9} />
        </DialogContent>
      </Dialog>

      <Dialog open={view}
        onClose={handleViewClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description" maxWidth="md" fullWidth>
        <DialogTitle id="form-dialog-title">
          <Box display="flex" alignItems="center">
            <Box flexGrow={1} >Select Images</Box>
            <Box>
              <IconButton onClick={handleViewClose}>
                <Close />
              </IconButton>
            </Box>
          </Box></DialogTitle>
        <DialogContent>
          {galleryData.length > 0 || galleryData.gallery != null && <Grid container spacing={2}>
            {galleryData.gallery.map((cc) => {
              return (
                <Grid item xs={4} md={4} sm={4} key={cc._id}>
                  <img
                    src={cc.image}
                    alt="Gallery"
                    loading="lazy"
                    width="100%" height="100%"
                    style={{ cursor: 'pointer' }}
                    onClick={() => { selectImage(cc.image) }}
                  />
                </Grid>
              )
            })}
          </Grid>}
        </DialogContent>
      </Dialog>
      <Dialog
        open={val.open}
        onClose={handleEditFClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <EditPackageFeature allvalue={val} setId={handleEditFClose} />
      </Dialog>
    </Grid>
  )
}

export default EditPackage

const packageValidate = (data) => {
  let errors = {}

  if (!data.name) {
    errors["name"] = "Name is Required"
  }

  if (!data.price) {
    errors["price"] = "Price is Required"
  }

  if (!data.oldimage) {
    errors["image"] = "Image is Required"
  }

  if (!data.description) {
    errors["description"] = "Description is Required"
  }
  return errors
}