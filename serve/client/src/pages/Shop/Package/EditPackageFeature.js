import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { featureUpdate, getShopPackage, getShopService } from 'redux/action'
import { Grid, Card, CardContent, TextField, Dialog, DialogContent, Typography, IconButton, Box, DialogTitle, useTheme, FormControl, Select, MenuItem, InputLabel, Checkbox } from '@mui/material'
import FormError from 'pages/Registration/FormError'
import '../../Registration/register.css'
import { tableStyles } from '../Dashboard/styles'
import { Close } from '@mui/icons-material';
import Autocomplete from '@mui/material/Autocomplete';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import { BookingButton } from "components/common";

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

const EditPackageFeature = ({ allvalue, setId }) => {

  const [element, setElement] = useState([])
  const [main, setMain] = useState([])
  const [data, setData] = useState({
    package_id: allvalue.package_id,
    service: allvalue.service
  })
  const packageData = useSelector((state) => state.auth.packageList)
  const serviceData = useSelector((state) => state.auth.shopService)
  const [err, setErr] = useState({})
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.created)
  const loading = useSelector(state => state.auth.loader)
  const classes = tableStyles()

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])


  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  useEffect(() => {
    if (packageData.length == 0) {
      dispatch(getShopPackage(0, 10))
    } else {
      setElement(packageData.package)
    }
  }, [packageData])

  useEffect(() => {
    if (serviceData.length == 0) {
      dispatch(getShopService(0, 10))
    } else {
      setMain(serviceData.service)
    }
  }, [serviceData])



  const handleSubmit = () => {
    var check = featureValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(featureUpdate(allvalue._id, { ...data, service: data.service.map((cc) => cc._id) }))
  }

  return (
    <Grid container spacing={2} className={classes.gridS} >
      <Grid item xs={12} sm={12} md={12}>
        <Card>
          <CardContent className="createPackage">
            <form>
              <Grid container spacing={2}>
                <Grid className="creatPop" item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                      Edit Package Feature
                    </Typography>
                    <Box className="crossIcon">
                      <IconButton onClick={() => { setId(0) }}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                {/* <Grid item xs={12} sm={12} md={6}>
                  <div>
                    <FormControl fullWidth variant="outlined">
                      <InputLabel id="demo-simple-select-label" shrink={true}>Package Id</InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={data.package_id}
                        label="Package ID"
                        name="package_id"
                        onChange={handleChange}
                      >
                        {element.map((cc) => {
                          return (
                            <MenuItem key={cc._id} value={cc._id}>{cc.name}</MenuItem>
                          )
                        })}
                      </Select>
                    </FormControl>
                  </div>
                  {err.package_id && (<FormError data={err.package_id}></FormError>)}
                </Grid> */}
                <Grid item xs={12} sm={12} md={12}>
                  <Autocomplete
                    multiple
                    id="checkboxes-tags-demo"
                    options={main}
                    value={data.service}
                    disableCloseOnSelect
                    isOptionEqualToValue={(option, value) => option._id === value._id}
                    getOptionLabel={(option) => option.name}
                    // filterSelectedOptions={true}
                    renderOption={(props, option, { selected }) => (
                      <li {...props}>
                        <Checkbox
                          icon={icon}
                          checkedIcon={checkedIcon}
                          style={{ marginRight: 8 }}
                          checked={selected}
                        />
                        {option.name}
                      </li>
                    )}
                    onChange={(e, n, r) => {
                      console.log('check', r, 'new', n, 'rest', r)
                      setData({ ...data, service: n })
                      setErr({ ...err, service: '' })
                    }}
                    renderInput={(params) => (
                      <TextField {...params} label="Services" placeholder="Service" />
                    )}
                  />
                  {err.service && (<FormError data={err.service}></FormError>)}
                </Grid>
              </Grid>
              <Grid container spacing={2} sx={{ marginTop: '5px' }} >
                <Grid item xs={6} align="left">
                  <BookingButton className='formSubmit-btn' type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId(0) }}>Back</BookingButton>
                </Grid>
                <Grid item xs={6} align="right">
                  <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Submit</BookingButton>
                </Grid>
              </Grid>
            </form>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default EditPackageFeature


const featureValidate = (data) => {
  let errors = {}

  if (!data.package_id) {
    errors["package_id"] = "Package Id is Required"
  }
  if (!data.service) {
    errors["service"] = "service is Required"
  }

  if (data.service) {
    if (data.service.length == 0) {
      errors['service'] = "service is Required";
    }
  }

  return errors
}