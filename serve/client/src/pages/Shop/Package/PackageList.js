import React, { useEffect, useState } from 'react'
import { Grid, Button, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Typography, Card, CardContent, IconButton, Dialog, DialogTitle, DialogContent, Box, FormGroup, Collapse, CardMedia, DialogActions, Stack, Chip, Checkbox } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { getShopGallery, getShopPackage, getShopService, packageDelete } from 'redux/action'
import { AddCircleOutline, Close, DesignServices, Edit, Refresh, KeyboardArrowDown, KeyboardArrowUp, Delete, CheckCircleOutline, HighlightOff } from '@mui/icons-material'
import { LoadingButton } from '@mui/lab'
import EditPackage from './EditPackage'
import TableLoader from '../UsableComponent/TableLoader'
import NoData from '../UsableComponent/NoData'
import { tableStyles } from '../Dashboard/styles'
import ImageBox from '../UsableComponent/ImageBox'
import TooltipBox from '../UsableComponent/TooltipBox'
import { AntSwitch, BookingButton } from "components/common";
import MobileLoader from '../UsableComponent/MobileLoader'
import '../Queue/queue.css'
import CreatePackage from './CreatePackage'

const PackageList = ({ setId }) => {

  const classes = tableStyles()
  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const [form, setForm] = useState(false)
  const [view, setView] = useState('')
  const packageData = useSelector((state) => state.auth.packageList)
  const galleryData = useSelector((state) => state.auth.shopGallery)
  const serviceData = useSelector((state) => state.auth.shopService)
  const allCount = useSelector(state => state.auth.packageTotal)
  const cted = useSelector((state) => state.auth.created)
  const loading = useSelector((state) => state.auth.loader)
  const dispatch = useDispatch()
  const [data, setData] = useState({
    open: false,
    name: '',
    image: '',
    description: '',
    price: '',
    view: '',
    btn: '',
    service: [],
    package_id: '',
    feature_id: '',
    _id: ''
  })

  const [cancel, setCancel] = useState({
    open: false,
    _id: ''
  })

  useEffect(() => {

    if (packageData.length == 0) {
      dispatch(getShopPackage(0, 10))
    } else {
      setElement(packageData.package)
      setFlag(true)
    }

    if (cted) {
      setCancel({ ...cancel, open: false })
      dispatch(getShopPackage(0, element.length == 0 ? 10 : element.length))
    }

  }, [packageData, cted])

  useEffect(() => {

    if (galleryData.length == 0) {
      dispatch(getShopGallery())
    }
  }, [galleryData])

  useEffect(() => {
    if (serviceData.length == 0) {
      dispatch(getShopService(0, 10))
    }
  }, [serviceData])

  const handleEdit = (val) => {
    setData({
      ...data,
      open: true,
      name: val.name,
      image: val.image,
      description: val.description,
      price: val.price,
      view: val.view,
      btn: val.btn,
      _id: val._id,
      service: val?.feature?.service,
      package_id: val?.feature?.package_id,
      feature_id: val?.feature?._id
    })
  }

  const handleClose = () => {
    setData({ ...data, open: false })
  }

  const handleRefresh = () => {
    dispatch(getShopPackage(0, element.length > 0 ? element.length : 10))
  }

  const handleDeleteOpen = (val) => {
    setCancel({
      ...cancel,
      open: true,
      _id: val
    })
  }

  const handleDeleteClose = () => {
    setCancel({ ...cancel, open: false })
  }

  const handleDelete = (row) => {
    dispatch(packageDelete(row))
  }

  const fetchMoreData = () => {
    dispatch(getShopPackage(element.length, 10))
  };

  const handleViewCard = (row) => {
    if (view == row._id) {
      setView("")
    } else {
      setView(row._id)
    }
  }

  const handleFormClose = () => {
    setForm(false)
  }

  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card>
          <CardContent>

            <Grid className="serviceListHead" container spacing={2}>
              <Grid item xs={12} sm={2} md={2} >
                <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh} >
                  Refresh
                </Button>
              </Grid>
              <Grid item xs={12} sm={7} md={8}>
                <Typography className='title_head' variant="h5" component="div" align="center">
                  Package List
                </Typography>
              </Grid>
              <Grid item xs={12} sm={3} md={2}>
                <Button className='customer-btn' startIcon={<AddCircleOutline color='primary' />} onClick={() => { setForm(true) }} >
                  Create
                </Button>
              </Grid>
            </Grid>
            <TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "block" } }}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>Price</TableCell>
                    <TableCell>Type</TableCell>
                    <TableCell>Button Name</TableCell>
                    <TableCell>Image</TableCell>
                    <TableCell>Description</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <TableRow
                      key={row._id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell>
                        <div style={{ display: 'flex' }}>
                          <IconButton title="Edit" onClick={() => { handleEdit(row) }}>
                            <Edit color='primary' />
                          </IconButton>
                          <IconButton title="Delete" onClick={() => { handleDeleteOpen(row._id) }}>
                            <Delete color='warning' />
                          </IconButton>
                        </div>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {row.name}
                      </TableCell>
                      <TableCell>{row.price}</TableCell>
                      <TableCell>{row.view}</TableCell>
                      <TableCell>{row.btn}</TableCell>
                      <TableCell>
                        <ImageBox image={row.image} />
                      </TableCell>
                      <TableCell>
                        <TooltipBox title="hover" data={row.description} />
                      </TableCell>
                    </TableRow>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <TableLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            {/* Mobile View */}
            <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "none" } }} >
              <Table size="small" aria-label="simple table">
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <React.Fragment key={row._id}>
                      <TableRow
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell className='customer-table'>
                          <Card sx={{ borderRadius: '18px' }} onClick={() => { handleViewCard(row) }}>
                            <CardMedia
                              component="img"
                              sx={{ width: '100%', objectFit: 'unset', borderRadius: '18px', border: 1 }}
                              image={row.image}
                              alt={row.name}
                            />
                            <Box>
                              <CardContent>
                                <Grid container spacing={2}>
                                  <Grid item xs={6} sm={6} >
                                    <div>Name</div>
                                    <div className='customer-font'>{row.name}</div>
                                  </Grid>
                                </Grid>
                              </CardContent>
                            </Box>
                          </Card>
                          {
                            view == row._id ? <IconButton
                              aria-label="expand row"
                              size="small"
                              onClick={() => setView('')}
                            >
                              <KeyboardArrowUp />
                            </IconButton> : <IconButton
                              aria-label="expand row"
                              size="small"
                              onClick={() => setView(row._id)}
                            >
                              <KeyboardArrowDown />
                            </IconButton>
                          }
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className='customer-tablecell' colSpan={6}>
                          <Collapse in={view == row._id} timeout="auto" unmountOnExit>
                            <Box sx={{ margin: 1 }}>
                              <Grid container spacing={2}>
                                <Grid item xs={6} sm={6}>
                                  <div>Price</div>
                                  <div className='customer-font'>{row.price}</div>
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                  <div>Type</div>
                                  <div className='customer-font'>{row.view}</div>
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                  <div>Button Name</div>
                                  <div className='customer-font'>{row.btn}</div>
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                  <div>Description</div>
                                  <div className='customer-font'>{row.description}</div>
                                </Grid>
                                <Grid item xs={4} sm={4}>
                                  <Button className='customer-btn' startIcon={<Edit color='primary' />} onClick={() => { handleEdit(row) }} >
                                    Edit
                                  </Button>
                                </Grid>
                                <Grid item xs={4} sm={4}>

                                  <Button className='customer-btn' startIcon={<Delete color='warning' />} onClick={() => { handleDeleteOpen(row._id) }} >
                                    Delete
                                  </Button>
                                </Grid>
                              </Grid>
                            </Box>
                          </Collapse>
                        </TableCell>
                      </TableRow>
                    </React.Fragment>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <MobileLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
          </CardContent>
          {element.length >= 10 && <Grid container spacing={2}>
            {allCount != element.length && <Grid item xs={12} sm={12} md={12} textAlign="center">
              <LoadingButton loading={loading}
                loadingIndicator="Loading..."
                variant="outlined" disabled={loading} onClick={fetchMoreData} >View More</LoadingButton>
            </Grid>}
          </Grid>}
        </Card>
      </Grid>
      <Dialog
        open={data.open}
        onClose={handleClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <EditPackage allvalue={data} setId={handleClose} />
      </Dialog>
      <Dialog
        open={cancel.open}
        onClose={handleDeleteClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Delete Package</Box>
          <Box>
            <IconButton onClick={handleDeleteClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          Are You Sure You want to Delete??
        </DialogContent>
        <DialogActions>
          <BookingButton fullWidth variant="contained" disabled={loading} onClick={() => { handleDelete(cancel._id) }} >Yes</BookingButton>
          <BookingButton fullWidth variant="contained" disabled={loading} onClick={handleDeleteClose} >No</BookingButton>
        </DialogActions>
      </Dialog>
      <Dialog
        open={form}
        onClose={() => {
          if (loading == true) {

          } else {
            handleFormClose()
          }
        }}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <CreatePackage setId={handleFormClose} />
      </Dialog>
    </Grid>
  )
}

export default PackageList
