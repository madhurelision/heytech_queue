import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Typography, Box, IconButton, useTheme, FormControl, FormControlLabel, Radio, RadioGroup, FormLabel, Chip, InputLabel, MenuItem, Select } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import 'pages/Registration/register.css'
import { Close } from '@mui/icons-material'
import { BookingButton } from "components/common";
import { getShopProduct, getShopTag, productShopStock } from 'redux/action/inventory'

const StockForm = ({ allvalue, setId }) => {

  const theme = useTheme()
  const [data, setData] = useState({
    comment: '',
    stock: 1,
    state: '',
    _id: allvalue?._id,
    newStock: allvalue?.newStock
  })

  const [err, setErr] = useState({})
  const [btn, setBtn] = useState(true)
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.created)
  const loading = useSelector(state => state.auth.loader)
  const tagData = useSelector((state) => state.auth.inventoryTagList)
  const productData = useSelector((state) => state.auth.inventoryProductList)
  const [tag, setTag] = useState([])
  const [product, setProduct] = useState([])


  useEffect(() => {
    if (tagData.length == 0) {
      dispatch(getShopTag())
    } else {
      setTag(tagData.tag)
    }
  }, [tagData])


  useEffect(() => {

    if (productData.length == 0) {
      dispatch(getShopProduct(0, 10))
    } else {
      setProduct(productData.product)
    }
  }, [productData])


  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
    setBtn(false)
  }

  const handleSubmit = () => {
    var check = stockValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    var val = (data.state == 'tradein' || data.state == 'adjustin') ? +data.stock : -data.stock
    dispatch(productShopStock(data._id, { stock: val, comment: data.comment, state: data.state }))
  }

  const handleTagBtn = (item) => {
    setData({ ...data, comment: data.comment.length == 0 ? data.comment.concat(item) : data.comment.concat(' ', item) })
    setErr({ ...err, comment: '' })
  }

  const handleProduct = (e) => {
    var ss = product.filter((cc) => cc._id == e.target.value)
    if (ss.length > 0) {
      setData({ ...data, _id: ss[0]?._id, newStock: ss[0]?.mainstock })
      setErr({ ...err, _id: '' })
    }
  }

  return (
    <Grid >
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid className="creatPop" item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                    Stock Update
                  </Typography>
                  <Box className="crossIcon">
                    <IconButton onClick={() => { setId() }}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label" shrink={true} >Product</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={data._id}
                    label="Product"
                    name="_id"
                    onChange={handleProduct}
                  >
                    {product.map((cc) => {
                      return (
                        <MenuItem style={{ textTransform: 'capitalize' }} key={cc._id} value={cc._id}>{cc.name}</MenuItem>
                      )
                    })}
                  </Select>
                </FormControl>
                {err._id && (<FormError data={err._id}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <TextField name="newStock" type="number" inputProps={{ min: 1, step: 1 }} value={data.newStock} fullWidth
                  variant="outlined" disabled InputLabelProps={{ shrink: true }} onChange={handleChange} label="Current Stock" />
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <TextField name="stock" type="number" inputProps={{ min: 1, step: 1 }} value={data.stock} fullWidth
                  variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Stock" />
                {err.stock && (<FormError data={err.stock}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                {/* <FormControl>
                  <FormLabel id="demo-row-radio-buttons-group-label">State</FormLabel>
                  <RadioGroup
                    row
                    aria-labelledby="demo-row-radio-buttons-group-label"
                    name='state'
                    value={data.state}
                    onChange={handleChange}
                  >
                    <FormControlLabel value="tradein" control={<Radio />} label="Trade In" />
                    <FormControlLabel value="tradeout" control={<Radio />} label="Trade Out" />
                    <FormControlLabel value="adjustin" control={<Radio />} label="Adjust In" />
                    <FormControlLabel value="adjustout" control={<Radio />} label="Adjust Out" />
                  </RadioGroup>
                </FormControl> */}
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label" shrink={true} >Adjustment Type</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={data.state}
                    label="Adjustment Type"
                    name="state"
                    onChange={handleChange}
                  >
                    <MenuItem value="tradein">  Trade In</MenuItem>
                    <MenuItem value="tradeout" > Trade Out</MenuItem>
                    <MenuItem value="adjustin" > Adjust In</MenuItem>
                    <MenuItem value="adjustout"  >Adjust Out</MenuItem>
                  </Select>
                </FormControl>
                {err.state && (<FormError data={err.state}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <Typography variant="body1" className="shop-image-title" component="div" align="left">
                  Comment
                </Typography>
                <textarea name="comment" value={data.comment} cols="40" rows="10" className={`description ${theme.palette.mode == 'dark' ? 'description-dark' : 'description-light'}`} onChange={handleChange}></textarea>
                {err.comment && (<FormError data={err.comment}></FormError>)}
              </Grid>

              <Grid item xs={12} sm={12} md={12} sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }}>
                {tag.length > 0 && <p className='shop-hint-title' >**Comment Template</p>}
                {tag.length > 0 && <Grid container spacing={1}>
                  {tag.map((cc) => {
                    return (
                      <Grid item xs={6} key={cc._id}>
                        <Chip size="small" sx={{
                          height: '100%',
                          '.MuiChip-label': {
                            overflowWrap: 'break-word',
                            whiteSpace: 'normal',
                            textOverflow: 'clip'
                          }
                        }} onClick={() => { handleTagBtn(cc.type) }} label={cc.name} variant="outlined"></Chip>
                      </Grid>
                    )
                  })}
                </Grid>}
              </Grid>
              <Grid item xs={12} sm={12} md={12} sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
                {tag.length > 0 && <p className='shop-hint-title' >**Comment Template</p>}
                {tag.length > 0 && <>
                  {tag.map((cc) => {
                    return (
                      <Chip key={cc._id} size="small" onClick={() => { handleTagBtn(cc.type) }} label={cc.name} variant="outlined"></Chip>
                    )
                  })}
                </>}
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }}>
              <Grid item xs={6} align="left">
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId(1) }} >Close</BookingButton>
              </Grid>
              <Grid item xs={6} align="right">
                <BookingButton type="button" disabled={(loading || btn) ? true : false} variant="contained" onClick={() => { handleSubmit() }} >Submit</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default StockForm

const stockValidate = (data) => {
  let errors = {}

  if (!data.stock) {
    errors["stock"] = "Stock is Required"
  }

  if (data.stock == "0") {
    errors["stock"] = "Stock Must be Greater Than Zero"
  }

  if (data.stock == 0) {
    errors["stock"] = "Stock Must be Greater Than Zero"
  }

  if (!data.comment) {
    errors["comment"] = "Comment is Required"
  }


  if (!data.state) {
    errors["state"] = "Adjustment Type is Required"
  }

  if (!data._id) {
    errors["_id"] = "Product is Required"
  }

  return errors
}