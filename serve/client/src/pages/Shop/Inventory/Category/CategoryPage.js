import React, { useState } from 'react'
import CreateCategory from './CreateCategory'
import CategoryList from './CategoryList'

const CategoryPage = () => {

  const [id, setId] = useState(0)

  switch (id) {
    case 0:
      return <CategoryList setId={setId} />
    case 1:
      return <CreateCategory setId={setId} />
    default:
      return <CategoryList setId={setId} />
  }
}

export default CategoryPage
