import React, { useEffect, useState } from 'react'
import { Grid, Button, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Typography, Card, CardContent, IconButton, Dialog, DialogTitle, DialogContent, Box, DialogActions, } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { AddCircleOutline, Close, Edit, Refresh, Delete, } from '@mui/icons-material'
import { LoadingButton } from '@mui/lab'
import EditVendor from './EditVendor'
import TableLoader from 'pages/Shop/UsableComponent/TableLoader'
import NoData from 'pages/Shop/UsableComponent/NoData'
import { tableStyles } from 'pages/Shop/Dashboard/styles'
import { BookingButton } from "components/common";
import MobileLoader from 'pages/Shop/UsableComponent/MobileLoader'
import 'pages/Shop/Queue/queue.css'
import CreateVendor from './CreateVendor'
import { vendorShopDelete, getShopVendor, getShopVendorType } from 'redux/action/inventory'
import CreateVendorType from './CreateVendorType'

const VendorList = ({ setId }) => {

  const classes = tableStyles()
  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const [form, setForm] = useState(false)
  const [open, setOpen] = useState(false)
  const vendorData = useSelector((state) => state.auth.inventoryVendorList)
  const vendorTypeData = useSelector((state) => state.auth.inventoryVendorTypeList)
  const allCount = useSelector(state => state.auth.inventoryVendorTotal)
  const cted = useSelector((state) => state.auth.created)
  const uted = useSelector((state) => state.auth.updated)
  const loading = useSelector((state) => state.auth.loader)
  const dispatch = useDispatch()
  const [data, setData] = useState({
    open: false,
    name: '',
    mobile: '',
    location: '',
    type: '',
    company: '',
    _id: ''
  })

  const [cancel, setCancel] = useState({
    open: false,
    _id: ''
  })

  useEffect(() => {

    if (vendorData.length == 0) {
      dispatch(getShopVendor(0, 10))
    } else {
      setElement(vendorData.vendor)
      setFlag(true)
    }

    if (cted) {
      setCancel({ ...cancel, open: false })
      dispatch(getShopVendor(0, element.length == 0 ? 10 : element.length))
    }

    if (uted) {
      dispatch(getShopVendorType())
    }

  }, [vendorData, cted, uted])


  useEffect(() => {

    if (vendorTypeData.length == 0) {
      dispatch(getShopVendorType())
    }
  }, [vendorTypeData])

  const handleEdit = (val) => {
    setData({
      ...data,
      open: true,
      name: val.name,
      mobile: val?.mobile,
      location: val?.location,
      type: val?.type?._id,
      company: val?.company,
      _id: val._id,
    })
  }

  const handleClose = () => {
    setData({ ...data, open: false })
  }

  const handleRefresh = () => {
    dispatch(getShopVendor(0, element.length > 0 ? element.length : 10))
  }

  const handleDeleteOpen = (val) => {
    setCancel({
      ...cancel,
      open: true,
      _id: val
    })
  }

  const handleDeleteClose = () => {
    setCancel({ ...cancel, open: false })
  }

  const handleDelete = (row) => {
    dispatch(vendorShopDelete(row))
  }

  const fetchMoreData = () => {
    dispatch(getShopVendor(element.length, 10))
  };

  const handleFormClose = () => {
    setForm(false)
  }

  const handleOpenClose = () => {
    setOpen(false)
  }

  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card>
          <CardContent>

            <Grid className="serviceListHead" container spacing={2}>
              <Grid item xs={12} sm={2} md={2} >
                <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh} >
                  Refresh
                </Button>
              </Grid>
              <Grid item xs={12} sm={7} md={8}>
                <Typography className='title_head' variant="h5" component="div" align="center">
                  Vendor List
                </Typography>
              </Grid>
              <Grid item xs={12} sm={3} md={2}>
                <Button className='customer-btn' startIcon={<AddCircleOutline color='primary' />} onClick={() => { setOpen(true) }} >
                  Add
                </Button>
                <Button className='customer-btn' startIcon={<AddCircleOutline color='primary' />} onClick={() => { setForm(true) }} >
                  Create
                </Button>
              </Grid>
            </Grid>
            <TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "block" } }}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>Contact</TableCell>
                    <TableCell>Type</TableCell>
                    <TableCell>Company</TableCell>
                    <TableCell>Location</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <TableRow
                      key={row._id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell>
                        <div style={{ display: 'flex' }}>
                          <IconButton title="Edit" onClick={() => { handleEdit(row) }}>
                            <Edit color='primary' />
                          </IconButton>
                          <IconButton title="Delete" onClick={() => { handleDeleteOpen(row._id) }}>
                            <Delete color='warning' />
                          </IconButton>
                        </div>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {row.name}
                      </TableCell>
                      <TableCell>
                        {row?.mobile}
                      </TableCell>
                      <TableCell>
                        {row?.type?.name}
                      </TableCell>
                      <TableCell>
                        {row?.company}
                      </TableCell>
                      <TableCell>
                        {row?.location}
                      </TableCell>
                    </TableRow>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <TableLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            {/* Mobile View */}
            <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "none" } }} >
              <Table size="small" aria-label="simple table">
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <React.Fragment key={row._id}>
                      <TableRow
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell>
                          <Card className='employee-card' >
                            <Box>
                              <CardContent sx={{ paddingBottom: '0 !important' }}>
                                <Grid container spacing={1}>
                                  <Grid className='employee-title' item xs={6} sm={6}>
                                    <div>Name</div>
                                    <div className='employee-body'>{row.name}</div>
                                  </Grid>
                                  <Grid className='employee-title' item xs={6} sm={6}>
                                    <div>Mobile</div>
                                    <div className='employee-body'>{row?.mobile}</div>
                                  </Grid>
                                  <Grid className='employee-title' item xs={6} sm={6}>
                                    <div>Type</div>
                                    <div className='employee-body'>{row?.type?.name}</div>
                                  </Grid>
                                  <Grid className='employee-title' item xs={6} sm={6}>
                                    <div>Company</div>
                                    <div className='employee-body'>{row?.company}</div>
                                  </Grid>
                                  <Grid className='employee-title' item xs={6} sm={6}>
                                    <div>Location</div>
                                    <div className='employee-body'>{row?.location}</div>
                                  </Grid>
                                  <Grid item xs={12} sm={12} className="div-flex">
                                    <Button className='customer-btn' startIcon={<Edit color='primary' />} onClick={() => { handleEdit(row) }} >
                                      Edit
                                    </Button>
                                    <Button className='customer-btn' startIcon={<Delete color='warning' />} onClick={() => { handleDeleteOpen(row._id) }} >
                                      Delete
                                    </Button>
                                  </Grid>
                                </Grid>
                              </CardContent>
                            </Box>
                          </Card>
                        </TableCell>
                      </TableRow>
                    </React.Fragment>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <MobileLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
          </CardContent>
          {element.length >= 10 && <Grid container spacing={2}>
            {allCount != element.length && <Grid item xs={12} sm={12} md={12} textAlign="center">
              <LoadingButton loading={loading}
                loadingIndicator="Loading..."
                variant="outlined" disabled={loading} onClick={fetchMoreData} >View More</LoadingButton>
            </Grid>}
          </Grid>}
        </Card>
      </Grid>
      <Dialog
        open={data.open}
        onClose={handleClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <EditVendor allvalue={data} setId={handleClose} />
      </Dialog>
      <Dialog
        open={cancel.open}
        onClose={handleDeleteClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Delete Vendor</Box>
          <Box>
            <IconButton onClick={handleDeleteClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          Are You Sure You want to Delete??
        </DialogContent>
        <DialogActions>
          <BookingButton fullWidth variant="contained" disabled={loading} onClick={() => { handleDelete(cancel._id) }} >Yes</BookingButton>
          <BookingButton fullWidth variant="contained" disabled={loading} onClick={handleDeleteClose} >No</BookingButton>
        </DialogActions>
      </Dialog>
      <Dialog
        open={form}
        onClose={() => {
          if (loading == true) {

          } else {
            handleFormClose()
          }
        }}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <CreateVendor setId={handleFormClose} />
      </Dialog>
      <Dialog
        open={open}
        onClose={() => {
          if (loading == true) {

          } else {
            handleOpenClose()
          }
        }}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <CreateVendorType setId={handleOpenClose} />
      </Dialog>
    </Grid>
  )
}

export default VendorList
