import React, { useState } from 'react'
import CreateVendor from './CreateVendor'
import VendorList from './VendorList'

const VendorPage = () => {

  const [id, setId] = useState(0)

  switch (id) {
    case 0:
      return <VendorList setId={setId} />
    case 1:
      return <CreateVendor setId={setId} />
    default:
      return <VendorList setId={setId} />
  }
}

export default VendorPage
