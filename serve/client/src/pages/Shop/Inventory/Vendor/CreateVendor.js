import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Typography, IconButton, Box, FormControl, Select, MenuItem, InputLabel } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import 'pages/Registration/register.css'
import { tableStyles } from 'pages/Shop/Dashboard/styles'
import { Close } from '@mui/icons-material'
import { BookingButton } from "components/common";
import { getShopVendorType, vendorShopCreate } from 'redux/action/inventory'
import { keyLengthValidation } from 'config/KeyData'

const CreateVendor = ({ setId }) => {

  const [data, setData] = useState({
    name: '',
    mobile: '',
    location: '',
    type: '',
    company: ''
  })

  const [err, setErr] = useState({})
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.created)
  const loading = useSelector(state => state.auth.loader)
  const vendorTypeData = useSelector((state) => state.auth.inventoryVendorTypeList)
  const [vendorType, setVendorType] = useState([])

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])


  useEffect(() => {
    if (vendorTypeData.length == 0) {
      dispatch(getShopVendorType())
    } else {
      setVendorType(vendorTypeData.type)
    }
  }, [vendorTypeData])

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    var check = vendorValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(vendorShopCreate(data))
  }

  const classes = tableStyles()

  return (
    <Grid container spacing={2} className={classes.gridS} >
      <Grid item xs={12} sm={12} md={12}>
        <Card>
          <CardContent className="createBrand">
            <form>
              <Grid container spacing={2}>
                <Grid className="creatPop" item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                      Create Vendor
                    </Typography>
                    <Box className="crossIcon">
                      <IconButton onClick={() => { setId(0) }}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <TextField name="name" type="text" value={data.name} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Name" />
                  {err.name && (<FormError data={err.name}></FormError>)}
                </Grid>

                <Grid item xs={12} sm={12} md={6}>
                  <TextField
                    variant="outlined"
                    onChange={handleChange}
                    value={data.mobile}
                    InputLabelProps={{ shrink: true }}
                    name="mobile"
                    type="tel"
                    label="Contact Number"
                    inputProps={{
                      'aria-label': 'Contact Number',
                      maxLength: keyLengthValidation.mobile
                    }}
                    fullWidth
                  />
                  {err.mobile && (<FormError data={err.mobile}></FormError>)}
                </Grid>

                <Grid item xs={12} sm={12} md={6}>
                  <TextField name="location" type="text" value={data.location} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Location" />
                  {err.location && (<FormError data={err.location}></FormError>)}
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                  <TextField name="company" type="text" value={data.company} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Company" />
                  {err.company && (<FormError data={err.company}></FormError>)}
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                  <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label" shrink={true} >Type</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={data.type}
                      label="Type"
                      name="type"
                      onChange={handleChange}
                    >
                      {vendorType.map((cc) => {
                        return (
                          <MenuItem style={{ textTransform: 'capitalize' }} key={cc._id} value={cc._id}>{cc.name}</MenuItem>
                        )
                      })}
                    </Select>
                  </FormControl>
                  {err.type && (<FormError data={err.type}></FormError>)}
                </Grid>
              </Grid>
              <Grid container spacing={2} sx={{ marginTop: '5px' }} >
                <Grid item xs={6} align="left">
                  <BookingButton className='formSubmit-btn' type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId(0) }}>Back</BookingButton>
                </Grid>
                <Grid item xs={6} align="right">
                  <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Submit</BookingButton>
                </Grid>
              </Grid>
            </form>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default CreateVendor

const vendorValidate = (data) => {
  let errors = {}

  if (!data.name) {
    errors["name"] = "Name is Required"
  }

  if (!data.mobile) {
    errors["mobile"] = "Contact Number is Required"
  }

  if (data.mobile) {
    if (!data["mobile"].match(/^[0-9]{10}$/)) {
      errors["mobile"] = "Please enter valid Contact no.";
    }
  }

  if (!data.location) {
    errors["location"] = "Location is Required"
  }

  if (!data.type) {
    errors["type"] = "Type is Required"
  }

  if (!data.company) {
    errors["company"] = "Company is Required"
  }

  return errors
}