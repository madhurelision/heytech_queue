import React, { useEffect, useState } from 'react'
import { Grid, Button, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Typography, Card, CardContent, IconButton, Dialog, DialogTitle, DialogContent, Box, FormGroup, Collapse, CardMedia, DialogActions, Stack, Chip, Checkbox } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { AddCircleOutline, Close, Edit, Refresh, KeyboardArrowDown, KeyboardArrowUp, Delete } from '@mui/icons-material'
import { LoadingButton } from '@mui/lab'
import EditProduct from './Product/EditProduct'
import TableLoader from 'pages/Shop/UsableComponent/TableLoader'
import NoData from 'pages/Shop/UsableComponent/NoData'
import { tableStyles } from 'pages/Shop/Dashboard/styles'
import ImageBox from 'pages/Shop/UsableComponent/ImageBox'
import TooltipBox from 'pages/Shop/UsableComponent/TooltipBox'
import { AntSwitch, BookingButton } from "components/common";
import MobileLoader from 'pages/Shop/UsableComponent/MobileLoader'
import 'pages/Shop/Queue/queue.css'
import CreateProduct from './Product/CreateProduct'
import { productShopDelete, getShopProduct, getShopBrand, getShopCategory, getShopVendor, getShopTag } from 'redux/action/inventory'
import { getShopGallery, getShopService } from 'redux/action'
import StockForm from './StockForm'
import ProductShopSearch from '../UsableComponent/ProductShopSearch'

const InventoryMain = () => {

  const classes = tableStyles()
  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const [form, setForm] = useState(false)
  const [view, setView] = useState('')
  const productData = useSelector((state) => state.auth.inventoryProductList)
  // const categoryData = useSelector((state) => state.auth.inventoryCategoryList)
  // const brandData = useSelector((state) => state.auth.inventoryBrandList)
  // const vendorData = useSelector((state) => state.auth.inventoryVendorList)
  const tagData = useSelector((state) => state.auth.inventoryTagList)
  const galleryData = useSelector((state) => state.auth.shopGallery)
  const serviceData = useSelector((state) => state.auth.shopService)
  const allCount = useSelector(state => state.auth.inventoryProductTotal)
  const cted = useSelector((state) => state.auth.created)
  const loading = useSelector((state) => state.auth.loader)
  const dispatch = useDispatch()
  const [data, setData] = useState({
    open: false,
    name: '',
    image: '',
    description: '',
    price: '',
    stock: '',
    service: '',
    brand: '',
    category: '',
    expiry: '',
    _id: ''
  })

  const [cancel, setCancel] = useState({
    open: false,
    _id: ''
  })

  const [stock, setStock] = useState({
    open: false,
    stock: '',
    newStock: '',
    _id: ''
  })

  useEffect(() => {

    if (productData.length == 0) {
      dispatch(getShopProduct(0, 10))
    } else {
      setElement(productData.product)
      setFlag(true)
    }

    if (cted) {
      setCancel({ ...cancel, open: false })
      dispatch(getShopProduct(0, element.length > 0 ? element.length : 10))
    }

  }, [productData, cted])

  // useEffect(() => {

  //   if (categoryData.length == 0) {
  //     dispatch(getShopCategory(0, 10))
  //   }

  // }, [categoryData])


  // useEffect(() => {

  //   if (brandData.length == 0) {
  //     dispatch(getShopBrand(0, 10))
  //   }
  // }, [brandData])

  // useEffect(() => {

  //   if (vendorData.length == 0) {
  //     dispatch(getShopVendor(0, 10))
  //   }
  // }, [vendorData])

  useEffect(() => {

    if (tagData.length == 0) {
      dispatch(getShopTag())
    }
  }, [tagData])

  useEffect(() => {

    if (serviceData.length == 0) {
      dispatch(getShopService(0, 10))
    }
  }, [serviceData])

  useEffect(() => {

    if (galleryData.length == 0) {
      dispatch(getShopGallery())
    }
  }, [galleryData])

  const handleEdit = (val) => {
    setData({
      ...data,
      open: true,
      name: val.name,
      image: val.image,
      description: val.description,
      stock: val.stock,
      price: val.price,
      service: val.service?._id,
      brand: val.brand?._id,
      category: val.category?._id,
      expiry: val.expiry,
      _id: val._id
    })
  }

  const handleClose = () => {
    setData({ ...data, open: false })
  }

  const handleRefresh = () => {
    dispatch(getShopProduct(0, element.length > 0 ? element.length : 10))
  }

  const handleDeleteOpen = (val) => {
    setCancel({
      ...cancel,
      open: true,
      _id: val
    })
  }

  const handleDeleteClose = () => {
    setCancel({ ...cancel, open: false })
  }

  const handleDelete = (row) => {
    dispatch(productShopDelete(row))
  }

  const fetchMoreData = () => {
    dispatch(getShopProduct(element.length, 10))
  };

  const handleViewCard = (row) => {
    if (view == row._id) {
      setView("")
    } else {
      setView(row._id)
    }
  }

  const handleFormClose = () => {
    setForm(false)
  }

  const handleStockEdit = (val) => {
    setStock({
      ...stock,
      open: true,
      stock: val.stock,
      _id: val._id,
      newStock: val.mainstock
    })
  }

  const handleStockClose = () => {
    setStock({ ...stock, open: false })
  }

  const [value, setValue] = useState('');

  const handleView = (e, ccd) => {
    if (ccd == null || ccd == undefined) {
      setElement(productData.service)
      setValue("")
    } else {
      setElement(productData.product.filter((cc) => cc._id == ccd._id))
      setValue("h")
    }
  }

  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card>
          <CardContent>
            <Grid className="serviceListHead" container spacing={2}>
              <Grid item xs={12} sm={2} md={2} >
                <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh} >
                  Refresh
                </Button>
              </Grid>
              <Grid item xs={12} sm={7} md={8}>
                <Typography className='title_head' variant="h5" component="div" align="center">
                  Dashboard
                </Typography>
              </Grid>
              <Grid item xs={12} sm={3} md={2}>
              </Grid>
              {/* <Grid item xs={12} sm={3} md={2}>
                <Button className='customer-btn' startIcon={<AddCircleOutline color='primary' />} onClick={() => { setForm(true) }} >
                  Create
                </Button>
              </Grid> */}
              {element.length > 0 && <Grid item xs={12} sm={6} md={6} style={{ paddingBlock: '20px' }}>
                <ProductShopSearch handleView={handleView} />
              </Grid>}
            </Grid>
            <TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "block" } }}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>Total Stock</TableCell>
                    <TableCell>New Stock</TableCell>
                    <TableCell>Price</TableCell>
                    <TableCell>Service</TableCell>
                    <TableCell>Brand</TableCell>
                    <TableCell>Volume</TableCell>
                    <TableCell>Category</TableCell>
                    <TableCell>Image</TableCell>
                    <TableCell>Description</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <TableRow
                      key={row._id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      onClick={() => { handleStockEdit(row) }}
                    >
                      <TableCell> </TableCell>
                      <TableCell component="th" scope="row">
                        {row.name}
                      </TableCell>
                      <TableCell>{row.stock}</TableCell>
                      <TableCell>{(row.mainstock == 0) ? <span className="socket-btn-red">{'Out of Stock'}</span> : row.mainstock}</TableCell>
                      <TableCell>{row.price}</TableCell>
                      <TableCell>{row.service.map((cc) => <Chip key={cc._id} className="service_Data_margin" label={cc.name} />)}</TableCell>
                      <TableCell>{row.brand?.name}</TableCell>
                      <TableCell>{row?.volume + ' ' + row?.volume_type}</TableCell>
                      <TableCell>{row.category?.name}</TableCell>
                      <TableCell>
                        <ImageBox image={row.image} />
                      </TableCell>
                      <TableCell>
                        <TooltipBox title="hover" data={row.description} />
                      </TableCell>
                    </TableRow>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <TableLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            {/* Mobile View */}
            <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "none" } }} >
              <Table size="small" aria-label="simple table">
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <React.Fragment key={row._id}>
                      <TableRow
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell className='customer-table'>
                          <Card sx={{ borderRadius: '18px' }} onClick={() => { handleViewCard(row) }}>
                            <CardMedia
                              component="img"
                              sx={{ width: '100%', objectFit: 'unset', borderRadius: '18px', border: 1 }}
                              image={row.image}
                              alt={row.name}
                            />
                            <Box>
                              <CardContent>
                                <Grid container spacing={2}>
                                  <Grid item xs={6} sm={6} >
                                    <div>Name</div>
                                    <div className='customer-font'>{row.name}</div>
                                  </Grid>
                                  <Grid item xs={6} sm={6} >
                                    <div>Service</div>
                                    <div className='customer-font'>{row.service.map((cc) => <Chip key={cc._id} className="service_Data_margin" label={cc.name} />)}</div>
                                  </Grid>
                                </Grid>
                              </CardContent>
                            </Box>
                          </Card>
                          {
                            view == row._id ? <IconButton
                              aria-label="expand row"
                              size="small"
                              onClick={() => setView('')}
                            >
                              <KeyboardArrowUp />
                            </IconButton> : <IconButton
                              aria-label="expand row"
                              size="small"
                              onClick={() => setView(row._id)}
                            >
                              <KeyboardArrowDown />
                            </IconButton>
                          }
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className='customer-tablecell' colSpan={6}>
                          <Collapse in={view == row._id} timeout="auto" unmountOnExit>
                            <Box sx={{ margin: 1 }}>
                              <Grid container spacing={2}>
                                <Grid item xs={6} sm={6}>
                                  <div>Total Stock</div>
                                  <div className='customer-font'>{row.stock}</div>
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                  <div>New Stock</div>
                                  <div className='customer-font'>{(row.mainstock == 0) ? <span className="socket-btn-red">{'Out of Stock'}</span> : row.mainstock}</div>
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                  <div>Price</div>
                                  <div className='customer-font'>{row.price}</div>
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                  <div>Volume</div>
                                  <div className='customer-font'>{row?.volume + ' ' + row?.volume_type}</div>
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                  <div>Brand</div>
                                  <div className='customer-font'>{row.brand?.name}</div>
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                  <div>Category</div>
                                  <div className='customer-font'>{row.category?.name}</div>
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                  <div>Description</div>
                                  <div className='customer-font'>{row.description}</div>
                                </Grid>
                              </Grid>
                            </Box>
                          </Collapse>
                        </TableCell>
                      </TableRow>
                    </React.Fragment>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <MobileLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
          </CardContent>
          {element.length > 10 && (value == null || value == undefined || value == "") && <Grid container spacing={2}>
            {allCount != element.length && <Grid item xs={12} sm={12} md={12} textAlign="center">
              <LoadingButton loading={loading}
                loadingIndicator="Loading..."
                variant="outlined" disabled={loading} onClick={fetchMoreData} >View More</LoadingButton>
            </Grid>}
          </Grid>}
        </Card>
      </Grid>
      <Dialog
        open={data.open}
        onClose={handleClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <EditProduct allvalue={data} setId={handleClose} />
      </Dialog>
      <Dialog
        open={cancel.open}
        onClose={handleDeleteClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Delete Product</Box>
          <Box>
            <IconButton onClick={handleDeleteClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          Are You Sure You want to Delete??
        </DialogContent>
        <DialogActions>
          <BookingButton fullWidth variant="contained" disabled={loading} onClick={() => { handleDelete(cancel._id) }} >Yes</BookingButton>
          <BookingButton fullWidth variant="contained" disabled={loading} onClick={handleDeleteClose} >No</BookingButton>
        </DialogActions>
      </Dialog>
      <Dialog
        open={form}
        onClose={() => {
          if (loading == true) {

          } else {
            handleFormClose()
          }
        }}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <CreateProduct setId={handleFormClose} />
      </Dialog>
      <Dialog
        open={stock.open}
        onClose={handleStockClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <StockForm allvalue={stock} setId={handleStockClose} />
      </Dialog>
    </Grid>
  )
}

export default InventoryMain
