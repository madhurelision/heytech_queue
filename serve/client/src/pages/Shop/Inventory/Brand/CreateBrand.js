import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Typography, IconButton, Box } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import 'pages/Registration/register.css'
import { tableStyles } from 'pages/Shop/Dashboard/styles'
import { Close } from '@mui/icons-material'
import { BookingButton } from "components/common";
import { brandShopCreate } from 'redux/action/inventory'

const CreateBrand = ({ setId }) => {

  const [data, setData] = useState({
    name: '',
    type: ''
  })

  const [err, setErr] = useState({})
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.created)
  const loading = useSelector(state => state.auth.loader)

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])


  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    var check = brandValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(brandShopCreate(data))
  }

  const classes = tableStyles()

  return (
    <Grid container spacing={2} className={classes.gridS} >
      <Grid item xs={12} sm={12} md={12}>
        <Card>
          <CardContent className="createBrand">
            <form>
              <Grid container spacing={2}>
                <Grid className="creatPop" item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                      Create Brand
                    </Typography>
                    <Box className="crossIcon">
                      <IconButton onClick={() => { setId(0) }}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                  <TextField name="name" type="text" value={data.name} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Name" style={{ marginBottom: '10px' }} />
                  {err.name && (<FormError data={err.name}></FormError>)}
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                  <TextField name="type" type="text" value={data.type} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Type" />
                  {err.type && (<FormError data={err.type}></FormError>)}
                </Grid>
              </Grid>
              <Grid container spacing={2} sx={{ marginTop: '5px' }} >
                <Grid item xs={6} align="left">
                  <BookingButton className='formSubmit-btn' type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId(0) }}>Back</BookingButton>
                </Grid>
                <Grid item xs={6} align="right">
                  <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Submit</BookingButton>
                </Grid>
              </Grid>
            </form>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default CreateBrand

const brandValidate = (data) => {
  let errors = {}

  if (!data.name) {
    errors["name"] = "Name is Required"
  }

  if (!data.type) {
    errors["type"] = "Type is Required"
  }

  return errors
}