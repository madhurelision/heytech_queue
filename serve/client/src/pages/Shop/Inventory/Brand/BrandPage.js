import React, { useState } from 'react'
import CreateBrand from './CreateBrand'
import BrandList from './BrandList'

const BrandPage = () => {

  const [id, setId] = useState(0)

  switch (id) {
    case 0:
      return <BrandList setId={setId} />
    case 1:
      return <CreateBrand setId={setId} />
    default:
      return <BrandList setId={setId} />
  }
}

export default BrandPage
