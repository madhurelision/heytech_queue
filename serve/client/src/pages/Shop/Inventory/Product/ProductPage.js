import React, { useState } from 'react'
import CreateProduct from './CreateProduct'
import ProductList from './ProductList'

const ProductPage = () => {

  const [id, setId] = useState(0)

  switch (id) {
    case 0:
      return <ProductList setId={setId} />
    case 1:
      return <CreateProduct setId={setId} />
    default:
      return <ProductList setId={setId} />
  }
}

export default ProductPage
