import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Dialog, DialogContent, Typography, Box, IconButton, useTheme, InputLabel, Select, MenuItem, FormControl, Checkbox } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import 'pages/Registration/register.css'
import NewImageCropper from 'pages/ImageCropper/NewImageCropper'
import { Close } from '@mui/icons-material'
import { BookingButton } from "components/common";
import { getShopBrand, getShopCategory, getShopVendor, productShopUpdate, getShopUnit } from 'redux/action/inventory'
import { getShopService } from 'redux/action'
import { DesktopDatePicker, LocalizationProvider, MobileDatePicker } from '@mui/lab'
import AdapterDateFns from '@mui/lab/AdapterDateFns'
import Autocomplete from '@mui/material/Autocomplete';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckBoxIcon from '@mui/icons-material/CheckBox';

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

const EditProduct = ({ allvalue, setId }) => {

  const theme = useTheme()
  const [data, setData] = useState({
    name: allvalue.name,
    image: '',
    description: allvalue.description,
    oldimage: allvalue.image,
    stock: allvalue.stock,
    price: allvalue.price,
    service: allvalue.service,
    brand: allvalue.brand,
    category: allvalue.category,
    expiry: allvalue.expiry,
    vendor: allvalue?.vendor,
    volume: allvalue?.volume,
    volume_type: allvalue?.volume_type,
  })
  const [open, setOpen] = useState({
    open: false,
    name: '',
    image: ''
  })

  const [image, setImage] = useState(allvalue.image)
  const [err, setErr] = useState({})
  const [btn, setBtn] = useState(true)
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.created)
  const loading = useSelector(state => state.auth.loader)
  const categoryData = useSelector((state) => state.auth.inventoryCategoryList)
  const brandData = useSelector((state) => state.auth.inventoryBrandList)
  const vendorData = useSelector((state) => state.auth.inventoryVendorList)
  const unitData = useSelector((state) => state.auth.inventoryUnitList)
  const serviceData = useSelector((state) => state.auth.shopService)
  const [category, setCategory] = useState([])
  const [service, setService] = useState([])
  const [brand, setBrand] = useState([])
  const [vendor, setVendor] = useState([])
  const [unit, setUnit] = useState([])

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])


  useEffect(() => {
    if (categoryData.length == 0) {
      dispatch(getShopCategory(0, 10))
    } else {
      setCategory(categoryData.category)
    }
  }, [categoryData])

  useEffect(() => {
    if (serviceData.length == 0) {
      dispatch(getShopService(0, 10))
    } else {
      setService(serviceData.service)
    }
  }, [serviceData])

  useEffect(() => {
    if (brandData.length == 0) {
      dispatch(getShopBrand(0, 10))
    } else {
      setBrand(brandData.brand)
    }
  }, [brandData])

  useEffect(() => {
    if (vendorData.length == 0) {
      dispatch(getShopVendor(0, 10))
    } else {
      setVendor(vendorData.vendor)
    }
  }, [vendorData])

  useEffect(() => {
    if (unitData.length == 0) {
      dispatch(getShopUnit())
    } else {
      setUnit(unitData.unit)
    }
  }, [unitData])

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
    setBtn(false)
  }

  const onHandleImage = (e) => {

    if (e.target.files == null || e.target.files == undefined) {
      return
    }

    const file = Math.round((e.target.files[0].size / 1024 * 1024));
    console.log('Image Size', e.target.files[0].size, 'Image Size Mb', file)

    if (file > 512000) {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      setErr({ ...err, [e.target.name]: 'Image Size Should be less than 500 kb' })
      return
    }

    const VIDEO_FORMATS = ["video/mp4", "video/mkv"];
    console.log('Image Console', e.target.name, e.target.files)
    console.log('Main', document.getElementsByName(e.target.name)[0].value)
    if (!VIDEO_FORMATS.includes(e.target.files[0].type)) {
      setData({ ...data, [e.target.name]: e.target.files[0] })
      setErr({ ...err, [e.target.name]: '' })
      setOpen({ ...open, open: true, name: e.target.name, image: URL.createObjectURL(e.target.files[0]) })
      setBtn(false)
    } else {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      return
    }
  }


  const CrooppedImageN = (imgss) => {
    console.log('Cropped Image Called', imgss)
    var ext = (imgss.type == "image/jpeg") ? '.jpg' : (imgss.type == "image/png") ? '.png' : '.jpg'
    const values = blobToFile(imgss, open.name + ext)
    console.log('Cropped Image Called', values)
    setData({ ...data, [open.name]: values })
    setOpen({ ...open, open: false })
    setImage(URL.createObjectURL(values))
  }

  const blobToFile = (theBlob, fileName) => {
    return new File([theBlob], fileName, { lastModified: new Date().getTime(), type: theBlob.type })
  }

  const handleClose = () => {
    document.getElementsByName(open.name)[0].value = ''
    setData({ ...data, [open.name]: '' })
    setOpen({ ...open, open: false })
  }

  const handleSubmit = () => {
    var check = productValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)

    var formData = new FormData()
    formData.append('name', data.name)
    formData.append('image', data.image)
    formData.append('oldimage', data.oldimage)
    formData.append('description', data.description)
    formData.append('stock', data.stock)
    formData.append('price', data.price)
    data.service.map((cc) => {
      formData.append('service', cc._id)
    })
    formData.append('brand', data.brand)
    formData.append('category', data.category)
    formData.append('expiry', data.expiry)
    formData.append('vendor', data.vendor)
    formData.append('volume', data.volume)
    formData.append('volume_type', data.volume_type)

    dispatch(productShopUpdate(allvalue?._id, formData))
  }

  const handleDate = (newValue) => {
    console.log('new date', newValue)
    setData({ ...data, expiry: newValue });
    setErr({ ...err, expiry: '' })
    setBtn(false)
  };

  return (
    <Grid >
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid className="creatPop" item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                    Edit Product
                  </Typography>
                  <Box className="crossIcon">
                    <IconButton onClick={() => { setId() }}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="name" type="text" value={data.name} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Name" style={{ marginBottom: '10px' }} />
                {err.name && (<FormError data={err.name}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="stock" type="number" inputProps={{ min: 1, step: 1 }} value={data.stock} fullWidth
                  variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Stock" />
                {err.stock && (<FormError data={err.stock}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="price" type="number" inputProps={{ min: 1, step: 1 }} value={data.price} fullWidth
                  variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Price" />
                {err.price && (<FormError data={err.price}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <DesktopDatePicker
                    label="Expiry"
                    inputFormat="MM/dd/yyyy"
                    value={data.expiry}
                    onChange={handleDate}
                    minDate={new Date()}
                    renderInput={(params) => <TextField {...params} fullWidth sx={{ display: { xs: 'none', sm: 'block', md: 'block' } }} />}
                  />
                  <MobileDatePicker
                    label="Expiry"
                    inputFormat="MM/dd/yyyy"
                    value={data.expiry}
                    onChange={handleDate}
                    minDate={new Date()}
                    renderInput={(params) => <TextField {...params} fullWidth sx={{ display: { xs: 'block', sm: 'none', md: 'none' } }} />}
                  />
                </LocalizationProvider>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="volume" type="number" inputProps={{ min: 1, step: 1 }} value={data.volume} fullWidth
                  variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Volume" />
                {err.volume && (<FormError data={err.volume}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label" shrink={true} >Volume Type</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={data.volume_type}
                    label="Volume Type"
                    name="volume_type"
                    onChange={handleChange}
                  >
                    {unit.map((cc) => {
                      return (
                        <MenuItem style={{ textTransform: 'capitalize' }} key={cc._id} value={cc.type}>{cc.name}</MenuItem>
                      )
                    })}
                  </Select>
                </FormControl>
                {err.volume_type && (<FormError data={err.volume_type}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Autocomplete
                  multiple
                  id="checkboxes-tags-demo"
                  options={service}
                  value={data.service}
                  disableCloseOnSelect
                  isOptionEqualToValue={(option, value) => option._id === value._id}
                  getOptionLabel={(option) => option.name}
                  renderOption={(props, option, { selected }) => (
                    <li {...props}>
                      <Checkbox
                        icon={icon}
                        checkedIcon={checkedIcon}
                        style={{ marginRight: 8 }}
                        checked={selected}
                      />
                      {option.name}
                    </li>
                  )}
                  onChange={(e, n, r) => {
                    console.log('check', r, 'new', n, 'rest', r)
                    setData({ ...data, service: n })
                    setErr({ ...err, service: '' })
                    setBtn(false)
                  }}
                  renderInput={(params) => (
                    <TextField {...params} label="Services" placeholder="Service" />
                  )}
                />
                {err.service && (<FormError data={err.service}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label" shrink={true} >Brand</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={data.brand}
                    label="Brand"
                    name="brand"
                    onChange={handleChange}
                  >
                    {brand.map((cc) => {
                      return (
                        <MenuItem style={{ textTransform: 'capitalize' }} key={cc._id} value={cc._id}>{cc.name}</MenuItem>
                      )
                    })}
                  </Select>
                </FormControl>
                {err.brand && (<FormError data={err.brand}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label" shrink={true} >Category</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={data.category}
                    label="Category"
                    name="category"
                    onChange={handleChange}
                  >
                    {category.map((cc) => {
                      return (
                        <MenuItem style={{ textTransform: 'capitalize' }} key={cc._id} value={cc._id}>{cc.name}</MenuItem>
                      )
                    })}
                  </Select>
                </FormControl>
                {err.category && (<FormError data={err.category}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label" shrink={true} >Vendor</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={data.vendor}
                    label="Vendor"
                    name="vendor"
                    onChange={handleChange}
                  >
                    {vendor.map((cc) => {
                      return (
                        <MenuItem style={{ textTransform: 'capitalize' }} key={cc._id} value={cc._id}>{cc.name}</MenuItem>
                      )
                    })}
                  </Select>
                </FormControl>
                {err.vendor && (<FormError data={err.vendor}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <Typography variant="body1" className="shop-image-title" component="div" align="left">
                  Description
                </Typography>
                <textarea name="description" value={data.description} cols="40" rows="10" className={`description ${theme.palette.mode == 'dark' ? 'description-dark' : 'description-light'}`} onChange={handleChange}></textarea>
                {err.description && (<FormError data={err.description}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="image" type="file" fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={onHandleImage} label="Image" />
                {err.image && (<FormError data={err.image}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <img src={image} width="100%" height="100%" />
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }}>
              <Grid item xs={6} align="left">
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId(1) }} >Close</BookingButton>
              </Grid>
              <Grid item xs={6} align="right">
                <BookingButton type="button" disabled={(loading || btn) ? true : false} variant="contained" onClick={() => { handleSubmit() }} >Submit</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
      <Dialog open={open.open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description" maxWidth="md" fullWidth>
        <DialogContent>
          <NewImageCropper img={open.image} click={CrooppedImageN} ratio={16 / 9} />
        </DialogContent>
      </Dialog>
    </Grid>
  )
}

export default EditProduct

const productValidate = (data) => {
  let errors = {}

  if (!data.name) {
    errors["name"] = "Name is Required"
  }

  if (!data.stock) {
    errors["stock"] = "Stock is Required"
  }

  if (!data.oldimage) {
    errors["image"] = "Image is Required"
  }
  if (!data.description) {
    errors["description"] = "Description is Required"
  }
  if (!data.price) {
    errors["price"] = "Price is Required"
  }
  if (!data.brand) {
    errors["brand"] = "Brand is Required"
  }
  if (!data.category) {
    errors["category"] = "Category is Required"
  }
  if (!data.service) {
    errors["service"] = "Service is Required"
  }
  if (!data.vendor) {
    errors["vendor"] = "Vendor is Required"
  }
  if (!data.expiry) {
    errors["expiry"] = "Expiry is Required"
  }
  if (data.stock == "0") {
    errors["stock"] = "Stock Must be Greater Than Zero"
  }

  if (data.stock == 0) {
    errors["stock"] = "Stock Must be Greater Than Zero"
  }

  if (!data.volume) {
    errors["volume"] = "Volume is Required"
  }

  if (!data.volume_type) {
    errors["volume_type"] = "Volume Type is Required"
  }

  if (data.volume == "0") {
    errors["volume"] = "Volume Must be Greater Than Zero"
  }

  if (data.volume == 0) {
    errors["volume"] = "Volume Must be Greater Than Zero"
  }

  if (data.service) {
    if (data.service.length == 0) {
      errors['service'] = "service is Required";
    }
  }

  return errors
}