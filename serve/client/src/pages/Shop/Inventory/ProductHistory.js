import React, { useEffect, useState } from 'react'
import { Grid, Button, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Typography, Card, CardContent, IconButton, Dialog, DialogTitle, DialogContent, Box, FormGroup, Collapse, CardMedia, DialogActions, Stack, Chip, Checkbox } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { AddCircleOutline, Close, DesignServices, Edit, Refresh, KeyboardArrowDown, KeyboardArrowUp, Delete, CheckCircleOutline, HighlightOff } from '@mui/icons-material'
import { LoadingButton } from '@mui/lab'
import TableLoader from 'pages/Shop/UsableComponent/TableLoader'
import NoData from 'pages/Shop/UsableComponent/NoData'
import { tableStyles } from 'pages/Shop/Dashboard/styles'
import { AntSwitch, BookingButton } from "components/common";
import MobileLoader from 'pages/Shop/UsableComponent/MobileLoader'
import 'pages/Shop/Queue/queue.css'
import { brandShopDelete, getShopStock } from 'redux/action/inventory'
import TooltipBox from '../UsableComponent/TooltipBox'

const ProductHistory = ({ setId }) => {

  const classes = tableStyles()
  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const [form, setForm] = useState(false)
  const stockData = useSelector((state) => state.auth.inventoryStockList)
  const allCount = useSelector(state => state.auth.inventoryStockTotal)
  const cted = useSelector((state) => state.auth.created)
  const loading = useSelector((state) => state.auth.loader)
  const dispatch = useDispatch()
  const [data, setData] = useState({
    open: false,
    val: '',
  })

  const [cancel, setCancel] = useState({
    open: false,
    _id: ''
  })

  useEffect(() => {

    if (stockData.length == 0) {
      dispatch(getShopStock(0, 10))
    } else {
      setElement(stockData.stock)
      setFlag(true)
    }

    if (cted) {
      setCancel({ ...cancel, open: false })
      dispatch(getShopStock(0, element.length == 0 ? 10 : element.length))
    }

  }, [stockData, cted])

  const handleEdit = (val) => {
    setData({
      ...data,
      open: true,
      val: val,
    })
  }

  const handleClose = () => {
    setData({ ...data, open: false })
  }

  const handleRefresh = () => {
    dispatch(getShopStock(0, element.length > 0 ? element.length : 10))
  }

  const handleDeleteOpen = (val) => {
    setCancel({
      ...cancel,
      open: true,
      _id: val
    })
  }

  const handleDeleteClose = () => {
    setCancel({ ...cancel, open: false })
  }

  const handleDelete = (row) => {
    dispatch(brandShopDelete(row))
  }

  const fetchMoreData = () => {
    dispatch(getShopStock(element.length, 10))
  };

  const handleFormClose = () => {
    setForm(false)
  }

  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card>
          <CardContent>
            <Grid className="serviceListHead" container spacing={2}>
              <Grid item xs={12} sm={2} md={2} >
                <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh} >
                  Refresh
                </Button>
              </Grid>
              <Grid item xs={12} sm={7} md={8}>
                <Typography className='title_head' variant="h5" component="div" align="center">
                  Product History
                </Typography>
              </Grid>
              <Grid item xs={12} sm={3} md={2}>
              </Grid>
            </Grid>
            <TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "block" } }}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Product Name</TableCell>
                    <TableCell>Stock</TableCell>
                    <TableCell>State</TableCell>
                    <TableCell>Comment</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <TableRow
                      key={row._id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      onClick={() => { handleEdit(row) }}
                    >
                      <TableCell>
                        {/* <div style={{ display: 'flex' }}>
                          <IconButton title="Edit" onClick={() => { handleEdit(row) }}>
                            <Edit color='primary' />
                          </IconButton>
                          <IconButton title="Delete" onClick={() => { handleDeleteOpen(row._id) }}>
                            <Delete color='warning' />
                          </IconButton>
                        </div> */}
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {row?.product_id?.name}
                      </TableCell>
                      <TableCell>{row.stock}</TableCell>
                      <TableCell>{row.state}</TableCell>
                      <TableCell>
                        <TooltipBox title="hover" data={row.comment} />
                      </TableCell>
                    </TableRow>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <TableLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            {/* Mobile View */}
            <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "none" } }} >
              <Table size="small" aria-label="simple table">
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <React.Fragment key={row._id}>
                      <TableRow
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell>
                          <Card className='employee-card' >
                            <Box>
                              <CardContent sx={{ paddingBottom: '0 !important' }}>
                                <Grid container spacing={1}>
                                  <Grid className='employee-title' item xs={6} sm={6}>
                                    <div>Product Name</div>
                                    <div className='employee-body'>{row?.product_id?.name}</div>
                                  </Grid>
                                  <Grid className='employee-title' item xs={6} sm={6}>
                                    <div>Stock</div>
                                    <div className='employee-body'>{row?.stock}</div>
                                  </Grid>
                                  <Grid className='employee-title' item xs={6} sm={6}>
                                    <div>State</div>
                                    <div className='employee-body'>{row?.state}</div>
                                  </Grid>
                                  <Grid className='employee-title' item xs={6} sm={6}>
                                    <div>Comment</div>
                                    <div className='employee-body'>{row?.comment}</div>
                                  </Grid>
                                  {/* <Grid item xs={12} sm={12} className="div-flex">
                                    <Button className='customer-btn' startIcon={<Edit color='primary' />} onClick={() => { handleEdit(row) }} >
                                      Edit
                                    </Button>
                                    <Button className='customer-btn' startIcon={<Delete color='warning' />} onClick={() => { handleDeleteOpen(row._id) }} >
                                      Delete
                                    </Button>
                                  </Grid> */}
                                </Grid>
                              </CardContent>
                            </Box>
                          </Card>
                        </TableCell>
                      </TableRow>
                    </React.Fragment>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <MobileLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
          </CardContent>
          {element.length >= 10 && <Grid container spacing={2}>
            {allCount != element.length && <Grid item xs={12} sm={12} md={12} textAlign="center">
              <LoadingButton loading={loading}
                loadingIndicator="Loading..."
                variant="outlined" disabled={loading} onClick={fetchMoreData} >View More</LoadingButton>
            </Grid>}
          </Grid>}
        </Card>
      </Grid>
      <Dialog
        open={cancel.open}
        onClose={handleDeleteClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Delete History</Box>
          <Box>
            <IconButton onClick={handleDeleteClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          Are You Sure You want to Delete??
        </DialogContent>
        <DialogActions>
          <BookingButton fullWidth variant="contained" disabled={loading} onClick={() => { handleDelete(cancel._id) }} >Yes</BookingButton>
          <BookingButton fullWidth variant="contained" disabled={loading} onClick={handleDeleteClose} >No</BookingButton>
        </DialogActions>
      </Dialog>
      <Dialog
        open={data.open}
        onClose={handleClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <Grid>
          <Card>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Box flexGrow={1} >
                      <Typography className='title_head loginHead' variant="h4" component="h2" align="center">
                        Product Details
                      </Typography>
                    </Box>
                    <Box>
                      <IconButton onClick={handleClose}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                <Grid item xs={6} sm={6}>
                  <div>Product Name</div>
                  <div>{data?.val?.product_id?.name}</div>
                </Grid>
                <Grid item xs={6} sm={6}>
                  <div>Stock</div>
                  <div>{data?.val?.stock}</div>
                </Grid>
                <Grid item xs={6} sm={6}>
                  <div>State</div>
                  <div>{data?.val?.state}</div>
                </Grid>
                <Grid item xs={6} sm={6}>
                  <div>Comment</div>
                  <div>{data?.val?.comment}</div>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Dialog>
    </Grid>
  )
}

export default ProductHistory
