import React, { useState, useContext, useEffect } from 'react'
import { Grid, TableContainer, Table, TableBody, TableHead, TableCell, TableRow, Paper, useTheme, useMediaQuery } from '@mui/material'
import { SeatForm } from 'config/KeyData';
import { useSelector } from 'react-redux'
import moment from 'moment';
import SocketContext from 'config/socket';
import { tableStyles } from '../Dashboard/styles';
import './queue.css';

const ShopQueueTable = ({ data, handleBtn }) => {

  const [booking, setBooking] = useState([])
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('md'));
  const profileData = useSelector((state) => state.auth.profile);
  const context = useContext(SocketContext)

  useEffect(() => {

    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
    })

    context.socket.on('userDisconnect', (data) => {
      console.log('Socket Err', data)
    })

    context.socket.on('shopQueueCount', data => {
      //console.log('shop Sockebokit Data', data.val)
      if (data.data == profileData.user_information) {
        setBooking(data.val.booking)
      }
    })

    context.socket.on('disconnect', () => {
      console.log('socket is disconnect')
    })

    return () => {
      context.socket.removeListener(['shopQueueCount'])
    }

  }, [])

  useEffect(() => {

    if (Object.keys(profileData).length > 0) {
      context.socket.emit('shopQueueGet', profileData.user_information)
    }

  }, [profileData])

  const classes = tableStyles()


  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} >
        {(Object.keys(profileData).length > 0) &&
          <TableContainer component={Paper}>
            <Table size="small" aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell className={`${(data == 'All') ? 'queue-color ' : 'queue-cursor '}` + 'queue-tableHeadMob'}
                    onClick={() => { handleBtn('All') }}>All</TableCell>
                  {SeatForm(profileData.seat).map((cc) => {
                    var arr = booking.filter((dd) => parseInt(dd.seat) == cc)
                    return <TableCell className={`${(data == cc) ? 'queue-color ' : 'queue-cursor '}` + 'queue-tableHeadMob'} key={cc} onClick={() => { handleBtn(cc) }}>
                      <div className='queue-center'>
                        <div>{cc}</div>
                        {booking.length > 0 && arr.length > 0 && <div>{moment(arr[arr.length - 1]?.approximate_date).calendar()}</div>}
                      </div>
                    </TableCell>
                  })}
                </TableRow>
              </TableHead>
              <TableBody>
                {booking.length > 0 ? <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                  <TableCell onClick={() => { handleBtn('All') }}>{booking.length}</TableCell>
                  {SeatForm(profileData.seat).map((cc, i) => {
                    return <TableCell key={cc + i} align="center" onClick={() => { handleBtn(cc) }}>
                      <div className={`${(booking.filter((dd) => parseInt(dd.seat) == cc).length > 0) ? 'queue-highlight' : 'queue-cursor'}`}>
                        <div>Queue Size</div>
                        {booking.filter((dd) => parseInt(dd.seat) == cc).length}
                      </div>
                    </TableCell>
                  })}
                </TableRow> : <TableRow>
                  <TableCell className='shop-queue-tab' colSpan={12}>
                    No Data Found
                  </TableCell>
                </TableRow>}
              </TableBody>
            </Table>
          </TableContainer>}
      </Grid>
    </Grid>
  )
}

export default ShopQueueTable