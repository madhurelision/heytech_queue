import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, FormControlLabel, Radio, RadioGroup, FormControl, FormLabel, Box, IconButton, Typography } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from '../../Registration/FormError'
import { bookingTimeUpdate, QueuebookingTimeUpdate } from 'redux/action'
import moment from 'moment'
import { BookingButton } from "components/common";
import { Close } from '@mui/icons-material'

const QueueForm = ({ allvalue, setId }) => {

  const [data, setData] = useState({
    waiting_time: allvalue.waiting_time,
    approximate_time: allvalue.approximate_time,
    number: allvalue.waiting_number,
    key: (allvalue.waiting_key == 'min') ? 'min' : 'hour',
    name: allvalue.approximate_number,
    namekey: (allvalue.approximate_key == 'min') ? 'min' : 'hour',
    status: allvalue.status,
    end_time: ''
  })


  const [btn, setBtn] = useState(true)
  const [err, setErr] = useState({})
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.open)
  const loading = useSelector(state => state.auth.loader)

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])

  const handleSubmit = () => {
    var check = waitingValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    // console.log('waiting time check', allvalue.waiting_time, data.number + ' ' + data.key, allvalue.waiting_time == data.number + ' ' + data.key)
    // console.log('approximate time', allvalue.approximate_time, data.name + ' ' + data.namekey, allvalue.approximate_time == data.name + ' ' + data.namekey)
    //var main_t = createDate(allvalue.appointment_date, parseInt(data.number), data.key)
    var main_t = (allvalue.waiting_number + ' ' + allvalue.waiting_key == data.number + ' ' + data.key == true) ? allvalue.end_time : createDate(allvalue.appointment_date, parseInt(data.number), data.key);
    var main_tes = (allvalue.approximate_number + ' ' + allvalue.approximate_key == data.name + ' ' + data.namekey == true) ? allvalue.approximate_date : createDate(allvalue.waiting_date, parseInt(data.name), data.namekey)
    // console.log('both time', main_t, 'appe', main_tes)
    var mT = (allvalue.waiting_key == 'min' && data.key == 'min') ? parseInt(data.number) - parseInt(allvalue.waiting_number) : (allvalue.waiting_key == 'hour' && data.key == 'min') ? parseInt(data.number) - (parseInt(allvalue.waiting_number) * 60) : (allvalue.waiting_key == 'min' && data.key == 'hour') ? (parseInt(data.number) * 60) - parseInt(allvalue.waiting_number) : parseInt(data.number) - parseInt(allvalue.waiting_number);

    var cT = (allvalue.approximate_key == 'min' && data.namekey == 'min') ? parseInt(data.name) - parseInt(allvalue.approximate_number) : (allvalue.approximate_key == 'hour' && data.key == 'min') ? parseInt(data.name) - (parseInt(allvalue.approximate_number) * 60) : (allvalue.approximate_key == 'min' && data.key == 'hour') ? (parseInt(data.name) * 60) - parseInt(allvalue.approximate_number) : parseInt(data.name) - parseInt(allvalue.approximate_number);


    dispatch(QueuebookingTimeUpdate(allvalue?._id, {
      //waiting_time: { time: data.number, key: data.key },
      waiting_number: data.number,
      waiting_key: data.key,
      //approximate_time: { time: data.name, key: data.namekey },
      approximate_number: data.name,
      approximate_key: data.namekey,
      status: data.status,
      end_time: main_t,
      approximate_date: main_tes,
      difference_waiting: mT,
      difference_approx: cT
    }))
  }

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
    setBtn(false)
  }

  return (
    <Grid >
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Box flexGrow={1} >
                    <Typography className='title_head loginHead' variant="h4" component="h2" align="center">
                      Queue Waiting Data
                    </Typography>
                  </Box>
                  <Box>
                    <IconButton onClick={setId}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <span>Preferred Date: {moment(allvalue?.appointment_date).format('LL')}</span>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <span>Preferred Time: {moment(allvalue?.appointment_date).calendar()}</span>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField label="Waiting Time" name="number" onChange={handleChange} value={data.number} InputLabelProps={{ shrink: true }} fullWidth placeholder="Enter Waiting Time" type="number" inputProps={{
                  min: 1, step: 1,
                }} />
                {err.number && (<FormError data={err.number}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">Waiting Time</FormLabel>
                  <RadioGroup row aria-label="waitingtime" name="key" onChange={handleChange} value={data.key}>
                    <FormControlLabel value='min' control={<Radio />} label="Min" />
                    <FormControlLabel value='hour' control={<Radio />} label="Hour" />
                  </RadioGroup>
                </FormControl>
                {err.key && (<FormError data={err.key}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField label="Approximate Time" name="name" onChange={handleChange} value={data.name} InputLabelProps={{ shrink: true }} fullWidth placeholder="Enter Approximate Time" type="number" inputProps={{
                  min: 1, step: 1,
                }} />
                {err.name && (<FormError data={err.name}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">Approximate Time</FormLabel>
                  <RadioGroup row aria-label="approximatetime" name="namekey" onChange={handleChange} value={data.namekey}>
                    <FormControlLabel value='min' control={<Radio />} label="Min" />
                    <FormControlLabel value='hour' control={<Radio />} label="Hour" />
                  </RadioGroup>
                </FormControl>
                {err.namekey && (<FormError data={err.namekey}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">Status</FormLabel>
                  <RadioGroup row aria-label="status" name="status" onChange={handleChange} value={data.status}>
                    <FormControlLabel disabled={data.status == 'accepted' && (new Date(allvalue.end_time) < new Date())} value='waiting' control={<Radio />} label="waiting" />
                    <FormControlLabel disabled={data.status == 'accepted' && (new Date(allvalue.end_time) < new Date())} value='accepted' control={<Radio />} label="accepted" />
                  </RadioGroup>
                </FormControl>
                {err.status && (<FormError data={err.status}></FormError>)}
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }} >
              <Grid item xs={6} align="left">
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId() }}>Close</BookingButton>
              </Grid>
              <Grid item xs={6} align="right">
                <BookingButton type="button" disabled={(loading || btn) ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Submit</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default QueueForm


const waitingValidate = (data) => {
  let errors = {}

  if (!data.number) {
    errors["number"] = "Waiting Time is Required"
  }

  if (!data.key) {
    errors["key"] = "Waiting key is Required"
  }

  if (!data.name) {
    errors["name"] = "Approximate Time is Required"
  }

  if (!data.namekey) {
    errors["namekey"] = "Approximate key is Required"
  }

  if (!data.status) {
    errors["status"] = "Status is Required"
  }

  return errors
}

const createDate = (date, time, key) => {
  var now = new Date(date);
  if (key == 'min') {
    now.setMinutes(now.getMinutes() + time);
  } else {
    now.setMinutes(now.getMinutes() + time * 60)
  }
  now.setMilliseconds(0);
  return now
}