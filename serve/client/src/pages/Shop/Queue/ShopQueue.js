import React, { useContext, useEffect, useState, useCallback, useRef } from 'react'
import { Grid, Button, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Typography, Card, CardContent, IconButton, Dialog, DialogTitle, DialogContent, Box, DialogActions, FormGroup, Collapse, useThemeProps, useTheme, Menu, MenuItem } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import TableLoader from '../UsableComponent/TableLoader'
import NoData from '../UsableComponent/NoData'
import { bookingEmployeeAdd, bookingSeatChangeUpdate, getShopQueue, queueStatusUpdate, shopCancelBooking, shopProfileData, shopCompleteBooking, getShopEmployee, getShopService } from 'redux/action'
import StatusBox from '../UsableComponent/StatusBox'
import moment from 'moment'
import QueueForm from './QueueForm'
import DialogBox from '../Dashboard/DialogBox'
import ShopAppointmentView from '../Dashboard/ShopAppointmentView'
import { tableStyles } from '../Dashboard/styles'
import EmailBox from '../UsableComponent/EmailBox'
import ProgressTimer from '../UsableComponent/ProgressTimer'
import SocketContext from 'config/socket'
import { Cancel, Close, Edit, MoreVert, Refresh, WhatsApp, KeyboardArrowUp, KeyboardArrowDown, Star, AddCircleOutline, CheckCircle, ViewList, GridView } from '@mui/icons-material'
import { LoadingButton } from '@mui/lab'
import { ACTIVE_EMPLOYEE_LIST, LOADERCLOSE, SHOP_COUNT, SHOP_TOTAL, SOCKET_SHOP_BOOKING_REMOVE, SOCKET_SHOP_BOOKING_UPDATE, SOCKET_SHOP_NOTIFICATION, SOCKET_SHOP_QUEUE_REMOVE, SOCKET_SHOP_QUEUE_UPDATE } from 'redux/action/types'
import TooltipBox from '../UsableComponent/TooltipBox'
import ListCard from '../UsableComponent/ListCard'
import { ChangeNumber, handleWhatsapp, getDateChange, dateMinus } from 'config/KeyData'
import SwitchCompnent from '../UsableComponent/SwitchComponent'
import DatePicker from 'components/DaterPicker';
import { useRecoilState } from 'recoil'
import { mentorState } from 'store'
import EmployeeSelect from '../UsableComponent/EmployeeSelect'
import MobileLoader from '../UsableComponent/MobileLoader'
import RemarkForm from './RemarkForm'
import RMark from '@mui/icons-material/MarkunreadMailbox';
import SeatSelect from '../UsableComponent/SeatSelect'
import NewTimer from '../UsableComponent/NewTimer'
import '../Queue/queue.css'
import InfiniteScroll from 'react-infinite-scroll-component'
import TestTimer from '../UsableComponent/TestTimer'
import ServiceForm from '../PaymentPage/ServiceShopForm'

var DailogData
const ShopQueue = ({ shopVal }) => {

  const theme = useTheme()
  const [element, setElement] = useState([]);
  const [box, setBox] = useState([]);
  const [flag, setFlag] = useState(false);
  const [open, setOpen] = useState(false);
  const [form, setForm] = useState(false)
  const [show, setShow] = useState(false)
  const [complete, setComplete] = useState(false)
  const [view, setView] = useState(true);
  const [boxview, setBoxView] = useState('');
  const bookingData = useSelector((state) => state.auth.shopQueueBooking);
  const serviceData = useSelector((state) => state.auth.shopService)
  const profileData = useSelector((state) => state.auth.profile);
  const allCount = useSelector(state => state.auth.listQBookshopCount)
  const moreData = useSelector(state => state.auth.shopQueueMore)
  const employeeData = useSelector((state) => state.auth.shopEmployee)
  const [mentorData, setMentorData] = useRecoilState(mentorState);
  const cted = useSelector((state) => state.auth.open);
  const mainV = useSelector((state) => state.auth.updated);
  const loading = useSelector((state) => state.auth.loader);
  const dispatch = useDispatch();
  const srollerLoad = useRef(null);
  const [data, setData] = useState({
    open: false,
    waiting_time: '',
    approximate_time: '',
    _id: '',
    appointment_date: '',
    appointment_time: '',
    waiting_number: '',
    waiting_key: '',
    approximate_number: '',
    approximate_key: '',
    end_time: '',
    approximate_date: '',
    waiting_date: ''
  })

  const [remark, setRemark] = useState({
    open: false,
    remark: '',
    _id: '',
  })

  const [payment, setPayment] = useState({
    open: false,
    customer: '',
    test: {},
    service: [],
    _id: '',
    shop_id: '',
    upi_id: '',
  })

  const [cancel, setCancel] = useState({
    open: false,
    _id: ''
  })

  const [book, setBook] = useState({
    open: false,
    data: {}
  })

  const [queue, setQueue] = useState({
    open: false,
    value: false
  })

  const classes = tableStyles()


  const context = useContext(SocketContext)


  useEffect(() => {

    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
    })

    context.socket.on('userDisconnect', (data) => {
      console.log('Socket Err', data)
    })

    context.socket.on('shopCount', (data) => {
      //console.log('Shop Count Data', data)
      dispatch({ type: SHOP_COUNT, payload: data })
    })

    context.socket.on('shopBooking', (data) => {
      // console.log('Shop Booking Data', data)
      dispatch({ type: SOCKET_SHOP_BOOKING_UPDATE, payload: [data] })
      if (data.queue == true) {
        dispatch({ type: SOCKET_SHOP_QUEUE_UPDATE, payload: [data] })
      }
    })

    context.socket.on('shopBookingRemove', (data) => {
      //console.log('Shop Booking Remove', data)
      dispatch({ type: SOCKET_SHOP_BOOKING_REMOVE, payload: data })
      if (data.queue == true) {
        dispatch({ type: SOCKET_SHOP_QUEUE_REMOVE, payload: data })
      }
    })

    context.socket.on('notificationShopCount', (data) => {
      //console.log('notificationCount', data)
      dispatch({ type: SOCKET_SHOP_NOTIFICATION, payload: data })
    })


    context.socket.on('shopEmployeeData', data => {
      //console.log('shop Sockebokit Data', data.val)
      dispatch({ type: ACTIVE_EMPLOYEE_LIST, payload: { data: data } })
    })

    context.socket.on('shopTotalCount', data => {
      //console.log('shop Sockebokit Data', data.val)
      if (data.data == profileData.user_information) {
        dispatch({ type: SHOP_TOTAL, payload: { data: data.val } })
      }
    })

    context.socket.on('disconnect', () => {
      console.log('socket is disconnect')
    })

    return () => {
      context.socket.removeListener(['shopCount', 'shopBookingRemove', 'shopBooking', 'shopEmployeeData'])
    }

  }, [])

  useEffect(() => {

    if (bookingData.length == 0) {
      dispatch(getShopQueue(0, 10))
    } else {
      setElement(bookingData.booking)
      setFlag(true)
    }

  }, [bookingData])

  useEffect(() => {

    if (cted) {
      setCancel({ ...cancel, open: false })
      setBook({ ...book, open: false })
      setComplete(false)
      dispatch(getShopQueue(0, 10))
    }

    if (mainV) {
      setQueue({ ...queue, open: false, value: false })
      dispatch(shopProfileData())
    }

  }, [cted, mainV])


  useEffect(() => {

    if (employeeData.length == 0) {
      dispatch(getShopEmployee())
    }
  }, [employeeData])

  useEffect(() => {

    if (serviceData.length == 0) {
      dispatch(getShopService(0, 10))
    }
  }, [serviceData])

  const handleEdit = (val) => {
    setData({
      ...data,
      open: true,
      waiting_time: val.waiting_time,
      approximate_time: val.approximate_time,
      status: val.status,
      _id: val._id,
      appointment_date: val.appointment_date,
      appointment_time: val.appointment_time,
      waiting_number: val.waiting_number,
      waiting_key: val.waiting_key,
      approximate_number: val.approximate_number,
      approximate_key: val.approximate_key,
      end_time: val.end_time,
      approximate_date: val.approximate_date,
      waiting_date: val.waiting_date
    })
  }

  const handleClose = () => {
    setData({ ...data, open: false })
  }

  const handleDialogClose = () => {
    setOpen(false)
  }

  const handleRemarkEdit = (val) => {
    setRemark({
      ...remark,
      open: true,
      remark: val.remark,
      _id: val._id,
    })
  }

  const handlePaymentM = (val) => {
    setPayment({
      ...payment,
      open: true,
      customer: val?.user_id?._id,
      shop_id: val?.shop_id?._id,
      test: val?.user_id,
      upi_id: val?.upi_id,
      service: [{ ...val.service_id, stock: 1, mark: true }],
      _id: val._id,
    })
  }

  const handlePayClose = () => {
    setPayment({ ...payment, open: false })
  }

  const handleRemarkClose = () => {
    setRemark({ ...remark, open: false })
  }

  const handleDialogOpen = (item) => {
    DailogData = <ShopAppointmentView item={item} />
    setOpen(true)
  }

  const handleRefresh = () => {
    dispatch(getShopQueue(0, 10))
  }

  const handleCancel = (item) => {
    dispatch(shopCancelBooking(item))
  }

  const handleCancelOpen = (item) => {
    setCancel({
      ...cancel,
      open: true,
      _id: item
    })
  }

  const handleCancelClose = () => {
    setCancel({ ...cancel, open: false })
  }

  const handleCompleteOpen = () => {
    setComplete(true)
  }

  const handleCompleteClose = () => {
    setComplete(false)
  }

  const handleChangeStatusOpen = (e, id) => {
    setQueue({ ...queue, open: true, value: e.target.checked })
  }

  const handleChangeStatus = (val) => {
    dispatch(queueStatusUpdate(profileData._id, { queue: val }))
  }

  const handleChangeStatusClose = () => {
    setQueue({ ...queue, open: false, value: false })
  }

  const handleTimerClose = useCallback((id) => {
    console.log('Timer Close', id)
    var arr = [...box]
    arr.push(id)
    setBox(arr)
  }, [box.length]);

  const handleFormOpen = () => {
    setForm(true)
    setMentorData(profileData)
  }

  const handleFormClose = () => {
    setForm(false)
    setShow(false)
    dispatch({ type: LOADERCLOSE })
  }

  const handleAssign = (e, id) => {
    console.log('Check Assign', e.target.value, id)
    dispatch(bookingEmployeeAdd(id, { assign: e.target.value }))
    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    if (context.socket.connected == true) {
      context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
    }
  }

  const handleSeat = (e, id) => {
    console.log('Check seat', e.target.value, id)
    dispatch(bookingSeatChangeUpdate(id, { seat: e.target.value }))
    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }
    if (context.socket.connected == true) {
      context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
    }
  }

  const handleElement = (element, val) => {

    if (val == 'All') { return element }

    return element.filter((cc) => cc.seat == val)
  }

  const handleViewCard = (row) => {

    setBook({
      ...book,
      open: true,
      data: row,
    })
  }

  const handleBookingClose = () => {
    setBook({ ...book, open: false })
  }

  const fetchMoreData = () => {
    dispatch(getShopQueue(element.length, 10))
  };

  const handleshopComplete = () => {
    dispatch(shopCompleteBooking({ status: true }))
    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }
    if (context.socket.connected == true) {
      context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
    }
  }


  const handleShow = (val) => {
    setShow(val)
  }

  const [anchorEl, setAnchorEl] = React.useState(null);
  const menuBtn = Boolean(anchorEl);
  const handleMenuClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card>
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={4} sm={2} md={2}>
                {/* <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh}>
                  Refresh
                </Button>
                <Button className='customer-btn' disabled={Object.keys(profileData).length == 0} startIcon={<AddCircleOutline color='primary' />} onClick={handleFormOpen}>
                  Queue
                </Button>
                {element.length > 0 && <Button style={{ padding: '6px 0px', letterSpacing: 0 }} className='customer-btn' startIcon={<CheckCircle color='primary' />} onClick={handleCompleteOpen}>
                  Complete All Booking
                </Button>}
                {element.length > 0 && <Button className='customer-btn' startIcon={view ? <GridView color='primary' /> : <ViewList color='primary' />} onClick={() => { setView(!view) }} sx={{ display: { xs: "none", md: 'inline-flex', sm: 'inline-flex' } }}>
                  {view ? 'Grid' : 'List'}
                </Button>} */}
                <Button
                  id="demo-positioned-button"
                  aria-controls={menuBtn ? 'demo-positioned-menu' : undefined}
                  aria-haspopup="true"
                  aria-expanded={menuBtn ? 'true' : undefined}
                  onClick={handleMenuClick}
                >
                  Menu
                </Button>
                <Menu
                  id="demo-positioned-menu"
                  aria-labelledby="demo-positioned-button"
                  anchorEl={anchorEl}
                  open={menuBtn}
                  onClose={handleMenuClose}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                  }}
                >
                  <MenuItem onClick={handleMenuClose}>
                    <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh}>
                      Refresh
                    </Button>
                  </MenuItem>
                  <MenuItem onClick={handleMenuClose}>
                    <Button className='customer-btn' disabled={Object.keys(profileData).length == 0} startIcon={<AddCircleOutline color='primary' />} onClick={handleFormOpen}>
                      Queue
                    </Button>
                  </MenuItem>
                  {element.length > 0 && <MenuItem onClick={handleMenuClose}>
                    <Button style={{ padding: '6px 0px', letterSpacing: 0 }} className='customer-btn' startIcon={<CheckCircle color='primary' />} onClick={handleCompleteOpen}>
                      Complete All Booking
                    </Button>
                  </MenuItem>}
                  {element.length > 0 && <MenuItem onClick={handleMenuClose}>
                    <Button className='customer-btn' startIcon={view ? <GridView color='primary' /> : <ViewList color='primary' />} onClick={() => { setView(!view) }} sx={{ display: { xs: "none", md: 'inline-flex', sm: 'inline-flex' } }}>
                      {view ? 'Grid' : 'List'}
                    </Button>
                  </MenuItem>}
                </Menu>
              </Grid>
              <Grid item xs={4} sm={8} md={8}>
                <Typography className='title_head' sx={{ display: { xs: 'none', md: 'block', sm: 'block' } }} variant="h5" component="div" align="center">
                  Queue List
                </Typography>
              </Grid>
              <Grid item xs={4} sm={2} md={2}>
                {Object.keys(profileData).length > 0 && <FormGroup>
                  <SwitchCompnent label="Queue" value={profileData.queue} handleChange={handleChangeStatusOpen} row={profileData._id} />
                </FormGroup>}
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <Typography className='title_head' sx={{ display: { xs: 'block', md: 'none', sm: 'none' } }} variant="h5" component="h2" align="center">
                  Queue List
                </Typography>
              </Grid>
              {/* <Grid item xs={12} sm={12} md={12}>
                <TestTimer main={new Date('2022-08-19T08:37:32.000Z')} second={new Date('2022-08-19T07:37:32.000Z')} />
              </Grid> */}
            </Grid>
            <InfiniteScroll className="searchServices"
              dataLength={element.length}
              next={() => { dispatch(getShopQueue(element.length, 10)) }}
              hasMore={moreData}
              loader={<h4 className="loading">Loading...</h4>}>
              <TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "block" } }}>
                {view == true && <Table sx={{ minWidth: 800 }} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell></TableCell>
                      <TableCell></TableCell>
                      <TableCell>Booking Id</TableCell>
                      <TableCell>Status</TableCell>
                      <TableCell>Timer</TableCell>
                      <TableCell>Reach Date</TableCell>
                      <TableCell>Reach Time</TableCell>
                      <TableCell>Waiting Time</TableCell>
                      <TableCell>Seat</TableCell>
                      <TableCell>Service Time</TableCell>
                      <TableCell>Service Name</TableCell>
                      <TableCell>User Size</TableCell>
                      <TableCell>Travelling Time</TableCell>
                      <TableCell>Appointment Date</TableCell>
                      <TableCell>Appointment Time</TableCell>
                      <TableCell>Assign</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {flag ? element.length > 0 ? handleElement(element, shopVal).map((row) => (
                      <React.Fragment
                        key={row._id}>
                        <TableRow
                          sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                          <TableCell>
                            {
                              boxview == row._id ? <IconButton
                                title='Open'
                                aria-label="open"
                                size="small"
                                onClick={() => setBoxView('')}
                              >
                                <KeyboardArrowUp />
                              </IconButton> : <IconButton
                                title='Close'
                                aria-label="close"
                                size="small"
                                onClick={() => setBoxView(row._id)}
                              >
                                <KeyboardArrowDown />
                              </IconButton>
                            }
                          </TableCell>
                          <TableCell>
                            <div className='div-flex'>
                              {row.waiting_verify == true ? <>
                                {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) > new Date() &&
                                  <IconButton title="Add Waiting Time" onClick={() => { handleEdit(row) }}>
                                    <Edit color="primary" />
                                  </IconButton>}
                              </> : <></>}
                              {row.waiting_verify == false && <IconButton title="Add Waiting Time" onClick={() => { handleEdit(row) }}>
                                <Edit color="primary" />
                              </IconButton>}
                              <IconButton title="Add Remark" onClick={() => { handleRemarkEdit(row) }}>
                                <RMark color="primary" />
                              </IconButton>
                              <IconButton title="Details" onClick={() => { handleDialogOpen(row) }} >
                                <MoreVert color="info" />
                              </IconButton>
                              {(new Date(row.end_time) > new Date()) && <IconButton title="Cancel Booking" onClick={() => { handleCancelOpen(row._id) }} >
                                <Cancel color="disabled" />
                              </IconButton>}
                              {row.payment.length == 0 && <Button className='customer-btn' onClick={() => { handlePaymentM(row) }}>
                                Pay
                              </Button>}
                            </div>
                          </TableCell>
                          <TableCell >
                            {'#' + row?.bookingid}
                          </TableCell>
                          <TableCell>
                            {row.waiting_verify == true ? <>
                              {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) > new Date() && <>
                                {'Waiting'}
                              </>}
                              {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) < new Date() && new Date(row.approximate_date) > new Date() && <>
                                {'Serving'}
                              </>}
                              {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) < new Date() && new Date(row.approximate_date) < new Date() && <>
                                {'Finished'}
                              </>}
                            </> : '--------'}
                          </TableCell>
                          <TableCell >
                            {/* {row.waiting_verify == true ? <ProgressTimer time={new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))} ma={row._id} func={handleTimerClose} show={true} /> : '.......'} */}
                            {row.waiting_verify == true ? <>
                              {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) > new Date() && <>
                                <ProgressTimer time={new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))} ma={row._id} func={handleTimerClose} show={true} />
                                <NewTimer main={new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))} />
                              </>}
                              {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) < new Date() && new Date(row.approximate_date) > new Date() && <>
                                <ProgressTimer time={new Date(row.approximate_date)} ma={row._id} func={handleTimerClose} show={true} />
                                <NewTimer main={new Date(row.approximate_date)} />
                              </>}
                              {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) < new Date() && new Date(row.approximate_date) < new Date() && <>
                                <ProgressTimer time={new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))} ma={row._id} func={handleTimerClose} show={true} />
                                <NewTimer main={new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))} />
                              </>}
                            </> : '--------'}
                            {/* {row.waiting_verify == true ? <NewTimer main={new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))} /> : ''} */}
                          </TableCell>
                          <TableCell>{(!row.end_time || row.end_time == null || row.end_time == undefined) ? '----' : moment(new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))).format('LL')}</TableCell>
                          <TableCell>{(!row.end_time || row.end_time == null || row.end_time == undefined) ? '----' : moment(new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))).calendar()}</TableCell>
                          <TableCell >{(!row.waiting_number || row.waiting_number == null || row.waiting_number == undefined) ? row?.waiting_time : (row?.waiting_number < 60) ? row?.waiting_number + ' ' + row.waiting_key : getDateChange(row.waiting_number)}</TableCell>
                          <TableCell >

                            {row.waiting_verify == true ? <>
                              {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) > new Date() &&
                                <SeatSelect value={row?.seat} _id={row._id} handleChange={handleSeat} seat={row?.service_id?.seat} />}

                              {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) < new Date() && new Date(row.approximate_date) > new Date() && <>
                                {row?.seat}
                              </>}
                              {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) < new Date() && new Date(row.approximate_date) < new Date() && <>
                                {row?.seat}
                              </>}
                            </> : <></>}
                            {row.waiting_verify == false && <SeatSelect value={row?.seat} _id={row._id} handleChange={handleSeat} seat={row?.service_id?.seat} />}

                          </TableCell>
                          <TableCell >{(!row.approximate_number || row.approximate_number == null || row.approximate_number == undefined) ? row?.approximate_time : (row?.approximate_number < 60) ? row?.approximate_number + ' ' + row.approximate_key : getDateChange(row.approximate_number)}</TableCell>
                          <TableCell>{row?.service_id.name}</TableCell>
                          <TableCell>{row?.size}</TableCell>
                          <TableCell>{row?.duration}</TableCell>
                          <TableCell >{moment(row.appointment_date).format('LL')}</TableCell>
                          <TableCell >{moment(row.appointment_date).calendar()}</TableCell>
                          <TableCell>

                            {row.waiting_verify == true ? <>
                              {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) > new Date() &&
                                <EmployeeSelect value={row?.assign} _id={row._id} handleChange={handleAssign} />}

                              {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) < new Date() && new Date(row.approximate_date) > new Date() && <>
                                <EmployeeSelect value={book.data?.assign} _id={book.data._id} handleChange={() => { }} />
                              </>}
                              {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) < new Date() && new Date(row.approximate_date) < new Date() && <>
                                <EmployeeSelect value={book.data?.assign} _id={book.data._id} handleChange={() => { }} />
                              </>}
                            </> : <></>}
                            {row.waiting_verify == false && <EmployeeSelect value={row?.assign} _id={row._id} handleChange={handleAssign} />}
                          </TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell className='customer-tablecell' colSpan={16}>
                            <Collapse in={boxview == row._id} timeout="auto" unmountOnExit>
                              <Box sx={{ margin: '3px' }}>
                                <Typography variant="h6" gutterBottom component="div" sx={{ fontSize: '1.1rem' }}>
                                  User Details
                                </Typography>
                                <Table size="small" aria-label="shopDetail">
                                  <TableHead>
                                    <TableRow>
                                      <TableCell></TableCell>
                                      <TableCell>Create By</TableCell>
                                      <TableCell>Description</TableCell>
                                      <TableCell>User Rating</TableCell>
                                      <TableCell>User Name</TableCell>
                                      <TableCell>User Number</TableCell>
                                      <TableCell>User Email</TableCell>
                                    </TableRow>
                                  </TableHead>
                                  <TableBody>
                                    <TableRow>
                                      <TableCell></TableCell>
                                      <TableCell>{(row.create_type == null || row.create_type == undefined) ? "........" : (row.create_type == "shop") ? row?.shop[0]?.shop_name + ' ' + row.create_type : (row.create_type == "customer") ? row?.customer[0]?.name + ' ' + row.create_type : (row.create_type == "employee") ? row?.employee[0]?.name + ' ' + row.create_type : row.create_type}</TableCell>
                                      <TableCell >
                                        <TooltipBox title="hover" data={row.description} />
                                      </TableCell>
                                      <TableCell>
                                        <Star color="warning" />{row.totalUser}
                                      </TableCell>
                                      <TableCell component="th" scope="row">
                                        {row?.user_id?.name}
                                      </TableCell>
                                      <TableCell >{(!row.mobile || row.mobile == null || row.mobile == undefined) ? '-----' : <IconButton title={row.mobile} onClick={() => { handleWhatsapp(row.mobile, `Hello Mr.${row?.user_id.name}`) }} >
                                        <WhatsApp />
                                      </IconButton>}</TableCell>
                                      <TableCell >
                                        <EmailBox title={row.user_email} />
                                      </TableCell>
                                    </TableRow>
                                  </TableBody>
                                </Table>
                              </Box>
                            </Collapse>
                          </TableCell>
                        </TableRow>
                      </React.Fragment>
                    )) : <TableRow>
                      <TableCell colSpan="16">
                        <NoData />
                      </TableCell>
                    </TableRow> : <TableRow>
                      <TableCell colSpan="16">
                        <TableLoader />
                      </TableCell>
                    </TableRow>}
                  </TableBody>
                </Table>}
                {view == false && <Table size="small" aria-label="simple table">
                  <Grid container spacing={2}>
                    {flag ? element.length > 0 ? handleElement(element, shopVal).map((row) => (
                      <Grid
                        item xs={12} sm={6} md={4}
                        key={row._id}
                      >
                        <ListCard {...row} handleClick={handleViewCard} />
                      </Grid>
                    )) : <Grid item xs={12} sm={12} md={12}>
                      <NoData />
                    </Grid> : <Grid item xs={12} sm={12} md={12}>
                      <MobileLoader />
                    </Grid>}
                  </Grid>
                </Table>}
              </TableContainer>
              {/* Mobile View */}
              <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "none" } }} >
                <Table size="small" aria-label="simple table">
                  <TableBody className={`${theme.palette.mode == 'dark' ? 'dark-table' : 'light-table'}`} >
                    {flag ? element.length > 0 ? handleElement(element, shopVal).map((row) => (
                      <TableRow
                        key={row._id}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell className='customer-table' >
                          <ListCard {...row} handleClick={handleViewCard} />
                        </TableCell>
                      </TableRow>
                    )) : <TableRow>
                      <TableCell colSpan="12">
                        <NoData />
                      </TableCell>
                    </TableRow> : <TableRow>
                      <TableCell colSpan="12">
                        <MobileLoader />
                      </TableCell>
                    </TableRow>}
                  </TableBody>
                </Table>
              </TableContainer>
            </InfiniteScroll>
          </CardContent>
        </Card>
      </Grid>
      <Dialog
        open={data.open}
        onClose={handleClose}
        fullWidth
        aria-labelledby="responsive-dialog-title"
      >
        <QueueForm allvalue={data} setId={handleClose} />
      </Dialog>
      <DialogBox open={open} handleClose={handleDialogClose} title={'Queue Detail'} data={DailogData} />
      <Dialog
        open={cancel.open}
        onClose={handleCancelClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Cancel Booking</Box>
          <Box>
            <IconButton onClick={handleCancelClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          Are You Sure You want to Cancel??
        </DialogContent>
        <DialogActions>
          <Button fullWidth variant="contained" color="primary" disabled={loading} onClick={() => { handleCancel(cancel._id) }} >Yes</Button>
          <Button fullWidth variant="contained" color="primary" disabled={loading} onClick={handleCancelClose} >No</Button>
        </DialogActions>
      </Dialog>
      <Dialog className="bookingPopup" open={form}
        disableEscapeKeyDown={true} onClose={() => {
          if (show == true) {

          } else { handleFormClose() }
        }}
        sx={(theme) => ({
          [theme.breakpoints.up('sm')]: {
            minHeight: '770px !important'
          },
          [theme.breakpoints.up('xs')]: {
            minHeight: '450px !important'
          },
          [theme.breakpoints.up('md')]: {
            minHeight: '770px !important'
          },
        })}
      >
        <DatePicker service={{
          label: '', value: '', labelType: '', tagVal: '', location: {
            lattitude: '',
            longitude: '',
            address: ''
          }
        }} close={handleFormClose} some={true} handleView={handleShow} />
      </Dialog>

      <Dialog
        open={remark.open}
        onClose={handleRemarkClose}
        fullWidth
        aria-labelledby="responsive-dialog-title"
      >
        <RemarkForm allvalue={remark} setId={handleRemarkClose} />
      </Dialog>

      <Dialog
        open={payment.open}
        onClose={handlePayClose}
        fullWidth
        maxWidth="lg"
        aria-labelledby="responsive-dialog-title"
      >
        <ServiceForm allvalue={payment} setId={handlePayClose} />
      </Dialog>
      <Dialog
        open={book.open}
        onClose={handleBookingClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <Grid>
          <Card>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Box flexGrow={1} >
                      <Typography className='title_head loginHead' variant="h4" component="h2" align="center">
                        Booking Details
                      </Typography>
                    </Box>
                    <Box>
                      <IconButton onClick={handleBookingClose}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>

                <Grid item xs={6} sm={6}>
                  <div>{book.data.description}</div>
                </Grid>
                <Grid item xs={6} sm={6} >
                  <div>Waiting Time</div>
                  <div className='customer-font'>
                    {(!book.data.waiting_number || book.data.waiting_number == null || book.data.waiting_number == undefined) ? book.data?.waiting_time : (book.data?.waiting_number < 60) ? book.data?.waiting_number + ' ' + book.data.waiting_key : getDateChange(book.data.waiting_number)}
                  </div>
                </Grid>
                <Grid item xs={6} sm={6} >
                  <div>Reach Date</div>
                  <div className='customer-font'>
                    {(!book.data.end_time || book.data.end_time == null || book.data.end_time == undefined) ? '----' : moment(new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0))).format('LL')}
                  </div>
                </Grid>
                <Grid item xs={6} sm={6} >
                  <div>Reach Time</div>
                  <div className='customer-font'>
                    {(!book.data.end_time || book.data.end_time == null || book.data.end_time == undefined) ? '----' : moment(new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0))).calendar()}
                  </div>
                </Grid>
                <Grid item xs={6} sm={6} >
                  <div>Service Time</div>
                  <div className='customer-font'>
                    {(!book.data.approximate_number || book.data.approximate_number == null || book.data.approximate_number == undefined) ? book.data?.approximate_time : (book.data?.approximate_number < 60) ? book.data?.approximate_number + ' ' + book.data.approximate_key : getDateChange(book.data.approximate_number)}
                  </div>
                </Grid>
                <Grid item xs={6} sm={6} >
                  <div>Travelling Time</div>
                  <div className='customer-font'>
                    {book.data?.duration}
                  </div>
                </Grid>

                <Grid item xs={6} sm={6} >
                  <div>Created By</div>
                  <div className='customer-font'>
                    {(book.data.create_type == null || book.data.create_type == undefined) ? "........" : (book.data.create_type == "shop") ? book.data?.shop[0]?.shop_name + ' ' + book.data.create_type : (book.data.create_type == "customer") ? book.data?.customer[0]?.name + ' ' + book.data.create_type : (book.data.create_type == "employee") ? book.data?.employee[0]?.name + ' ' + book.data.create_type : book.data.create_type}
                  </div>
                </Grid>
                <Grid item xs={6} sm={6}>
                  <div>
                    {book.data.waiting_verify == true ? <>
                      {new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0)) > new Date() &&
                        <EmployeeSelect value={book.data?.assign} _id={book.data._id} handleChange={handleAssign} />
                      }

                      {new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0)) < new Date() && new Date(book.data.approximate_date) > new Date() && <>
                        <EmployeeSelect value={book.data?.assign} _id={book.data._id} handleChange={() => { }} />

                      </>}
                      {new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0)) < new Date() && new Date(book.data.approximate_date) < new Date() && <>
                        <EmployeeSelect value={book.data?.assign} _id={book.data._id} handleChange={() => { }} />

                      </>}
                    </> : <></>}
                    {book.data.waiting_verify == false && <EmployeeSelect value={book.data?.assign} _id={book.data._id} handleChange={handleAssign} />
                    }
                  </div>
                </Grid>
                <Grid item xs={6} sm={6}>
                  <div>
                    {book.data.waiting_verify == true ? <>
                      {new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0)) > new Date() &&
                        <SeatSelect value={book.data?.seat} _id={book.data._id} handleChange={handleSeat} seat={book.data?.service_id?.seat} />}
                      {new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0)) < new Date() && new Date(book.data.approximate_date) > new Date() && <>
                        {book.data?.seat}
                      </>}
                      {new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0)) < new Date() && new Date(book.data.approximate_date) < new Date() && <>
                        {book.data?.seat}
                      </>}
                    </> : <></>}
                    {book.data.waiting_verify == false && <SeatSelect value={book.data?.seat} _id={book.data._id} handleChange={handleSeat} seat={book.data?.service_id?.seat} />}
                  </div>
                </Grid>
                <Grid item xs={12} sm={12}>

                  <span className='customer-fonts'>
                    {/* {book.data.waiting_verify == true ? <ProgressTimer time={new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0))} ma={book.data._id} func={handleTimerClose} show={true} /> : '.......'}

                    {book.data.waiting_verify == true ? <NewTimer main={new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0))} /> : ''} */}
                    {book.data.waiting_verify == true ? <>
                      {new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0)) > new Date() && <>
                        <ProgressTimer time={new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0))} ma={book.data._id} func={handleTimerClose} show={true} />
                        {'Waiting'}
                        <NewTimer main={new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0))} />
                      </>}
                      {new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0)) < new Date() && new Date(book.data.approximate_date) > new Date() && <>
                        <ProgressTimer time={new Date(book.data.approximate_date)} ma={book.data._id} func={handleTimerClose} show={true} />
                        {'Serving'}
                        <NewTimer main={new Date(book.data.approximate_date)} />
                      </>}
                      {new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0)) < new Date() && new Date(book.data.approximate_date) < new Date() && <>
                        <ProgressTimer time={new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0))} ma={book.data._id} func={handleTimerClose} show={true} />
                        {'Finished'}
                        <NewTimer main={new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0))} />
                      </>}
                    </> : '--------'}
                  </span>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <div>{book.data?.user_id?.name} <Star color="warning" />{book.data.totalUser}</div>
                  <div>{book.data.user_email}</div>
                  <div>{ChangeNumber(book.data.mobile)}</div>
                </Grid>
                <Grid item xs={12} sm={12}>
                  {(!book.data.mobile || book.data.mobile == null || book.data.mobile == undefined) ? '-----' : <IconButton title={book.data.mobile} onClick={() => { handleWhatsapp(book.data.mobile, `Hello Mr.${book.data?.user_id.name}`) }} >
                    <WhatsApp color="success" />
                  </IconButton>}
                  {book.data.waiting_verify == true ? <>
                    {new Date(book.data.end_time).setSeconds(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? book.data.durationVal : 0)) > new Date() &&

                      <IconButton title="Add Waiting Time" onClick={() => {
                        handleEdit(book.data)
                      }}>
                        <Edit color="primary" />
                      </IconButton>}
                  </> : <></>}
                  {book.data.waiting_verify == false &&
                    <IconButton title="Add Waiting Time" onClick={() => {
                      handleEdit(book.data)
                    }}>
                      <Edit color="primary" />
                    </IconButton>}
                  <IconButton title="Add Remark" onClick={() => {
                    handleRemarkEdit(book.data)
                  }}>
                    <RMark color="primary" />
                  </IconButton>
                  <IconButton title="Details" onClick={() => { handleDialogOpen(book.data) }} >
                    <MoreVert color="info" />
                  </IconButton>
                  {(new Date(book.data.end_time) > new Date()) && <IconButton title="Cancel Booking" onClick={() => { handleCancelOpen(book.data._id) }} >
                    <Cancel color="disabled" />
                  </IconButton>}

                  <Button className='customer-btn' onClick={() => { handlePaymentM(book.data) }}>
                    Pay
                  </Button>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Dialog>
      <Dialog
        open={complete}
        onClose={handleCompleteClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Complete All Bookings</Box>
          <Box>
            <IconButton onClick={handleCompleteClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          Complete Your All Booking Sure??
        </DialogContent>
        <DialogActions>
          <Button fullWidth variant="contained" color="primary" disabled={loading} onClick={handleshopComplete} >Yes</Button>
          <Button fullWidth variant="contained" color="primary" disabled={loading} onClick={handleCompleteClose} >No</Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={queue.open}
        onClose={handleChangeStatusClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Queue Status</Box>
          <Box>
            <IconButton onClick={handleChangeStatusClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          {queue.value == true && 'Are You Sure You Want Open Shop Queue ??'}
          {queue.value == false && 'Are You Sure You Want Close Shop Queue ??'}
        </DialogContent>
        <DialogActions>
          <Button fullWidth variant="contained" color="primary" disabled={loading} onClick={() => { handleChangeStatus(queue.value) }} >Yes</Button>
          <Button fullWidth variant="contained" color="primary" disabled={loading} onClick={handleChangeStatusClose} >No</Button>
        </DialogActions>
      </Dialog>
    </Grid>
  )
}

export default ShopQueue
