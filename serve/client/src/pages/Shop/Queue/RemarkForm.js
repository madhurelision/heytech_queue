import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Box, IconButton, Typography } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from '../../Registration/FormError'
import { bookingRemarkUpdate } from 'redux/action'
import { BookingButton } from "components/common";
import { Close } from '@mui/icons-material'

const RemarkForm = ({ allvalue, setId }) => {

  const [data, setData] = useState({
    remark: allvalue.remark,
  })

  const [btn, setBtn] = useState(true)
  const [err, setErr] = useState({})
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.open)
  const loading = useSelector(state => state.auth.loader)

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])

  const handleSubmit = () => {
    var check = remarkValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(bookingRemarkUpdate(allvalue?._id, data))
  }

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
    setBtn(false)
  }

  return (
    <Grid >
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Box flexGrow={1} >
                    <Typography className='title_head loginHead' variant="h4" component="h2" align="center">
                      Add Remark
                    </Typography>
                  </Box>
                  <Box>
                    <IconButton onClick={setId}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField label="Remark" name="remark" onChange={handleChange} value={data.remark} InputLabelProps={{ shrink: true }} fullWidth placeholder="Enter Remark" type="remark" />
                {err.remark && (<FormError data={err.remark}></FormError>)}
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }} >
              <Grid item xs={6} align="left">
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId() }}>Close</BookingButton>
              </Grid>
              <Grid item xs={6} align="right">
                <BookingButton type="button" disabled={(loading || btn) ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Submit</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default RemarkForm


const remarkValidate = (data) => {
  let errors = {}

  if (!data.remark) {
    errors["remark"] = "Remark is Required"
  }


  return errors
}