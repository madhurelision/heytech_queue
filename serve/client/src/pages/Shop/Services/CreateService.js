import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Autocomplete, Dialog, DialogContent, Button, Typography, FormLabel, RadioGroup, Radio, FormControl, FormControlLabel, IconButton, Box, DialogTitle, Chip, Select, InputLabel, MenuItem, useTheme, useMediaQuery } from '@mui/material'
import ImageCropper from '../../ImageCropper/ImageCropper'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import '../../Registration/register.css'
import { getAllServiceName, getAllServiceTag, getAllTagService, serviceCreate, serviceRegistrationCreate } from 'redux/action'
import { tableStyles } from '../Dashboard/styles'
import { EmogiItem, Seat, SeatForm, ServiceTag } from 'config/KeyData';
import NewImageCropper from 'pages/ImageCropper/NewImageCropper'
import { Close } from '@mui/icons-material'
import { BookingButton } from "components/common";
import SelectDataComp from '../UsableComponent/SelectDataComp'

const CreateService = ({ setId, show }) => {

  const theme = useTheme()
  const matches = useMediaQuery(theme.breakpoints.up('md'));
  const [data, setData] = useState({
    name: '',
    image: '',
    description: '',
    motivation: '',
    emojiIcon: '',
    waiting_number: '',
    waiting_key: 'min',
    oldimage: '',
    tags: '',
    seat: [],
    price: ''
  })
  const [open, setOpen] = useState({
    open: false,
    name: '',
    image: ''
  })

  const [image, setImage] = useState()
  const [view, setView] = useState(false)
  const [err, setErr] = useState({})
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.created)
  const loading = useSelector(state => state.auth.loader)
  const galleryData = useSelector((state) => state.auth.shopGallery)
  const profileD = useSelector((state) => state.auth.profile)
  const registrationD = useSelector((state) => state.auth.registrationData)
  const [tag, setTag] = useState([])
  const [tags, setTags] = useState([])
  const [serveName, setServeName] = useState([])
  const serviceTagD = useSelector(state => state.auth.serviceTagList)
  const tagServerD = useSelector(state => state.auth.tagServiceList)
  const serviceNameD = useSelector(state => state.auth.serviceNameList)

  useEffect(() => {

    if (serviceTagD.length == 0) {
      dispatch(getAllServiceTag())
    } else {
      setTag(serviceTagD.tag)
    }

  }, [serviceTagD])

  useEffect(() => {

    if (tagServerD.length == 0) {
      dispatch(getAllTagService())
    } else {
      setTags(tagServerD.tag)
    }

  }, [tagServerD])

  useEffect(() => {

    if (serviceNameD.length == 0) {
      dispatch(getAllServiceName())
    } else {
      setServeName(serviceNameD.serviceName)
    }

  }, [serviceNameD])

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])


  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const onHandleImage = (e) => {

    if (e.target.files == null || e.target.files == undefined) {
      return
    }

    const file = Math.round((e.target.files[0].size / 1024 * 1024));
    console.log('Image Size', e.target.files[0].size, 'Image Size Mb', file)

    if (file > 512000) {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      setErr({ ...err, [e.target.name]: 'Image Size Should be less than 500 kb' })
      return
    }

    const VIDEO_FORMATS = ["video/mp4", "video/mkv"];
    console.log('Image Console', e.target.name, e.target.files)
    console.log('Main', document.getElementsByName(e.target.name)[0].value)
    if (!VIDEO_FORMATS.includes(e.target.files[0].type)) {
      setData({ ...data, [e.target.name]: e.target.files[0] })
      setErr({ ...err, [e.target.name]: '' })
      setOpen({ ...open, open: true, name: e.target.name, image: URL.createObjectURL(e.target.files[0]) })
    } else {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      return
    }
  }


  const CrooppedImageN = (imgss) => {
    console.log('Cropped Image Called', imgss)
    var ext = (imgss.type == "image/jpeg") ? '.jpg' : (imgss.type == "image/png") ? '.png' : '.jpg'
    const values = blobToFile(imgss, open.name + ext)
    console.log('Cropped Image Called', values)
    setData({ ...data, [open.name]: values, oldimage: URL.createObjectURL(values) })
    setOpen({ ...open, open: false })
    setImage(URL.createObjectURL(values))
  }

  const blobToFile = (theBlob, fileName) => {
    return new File([theBlob], fileName, { lastModified: new Date().getTime(), type: theBlob.type })
  }

  const handleClose = () => {
    document.getElementsByName(open.name)[0].value = ''
    setData({ ...data, [open.name]: '', oldimage: '' })
    setOpen({ ...open, open: false })
  }

  const handleSubmit = () => {
    var check = serviceValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)

    var formData = new FormData()
    formData.append('name', data.name)
    formData.append('motivation', data.motivation)
    formData.append('emojiIcon', data.emojiIcon)
    formData.append('image', data.image)
    formData.append('oldimage', data.oldimage)
    formData.append('description', data.description)
    formData.append('waiting_number', data.waiting_number)
    formData.append('waiting_key', data.waiting_key)
    formData.append('waiting_time', data.waiting_number + ' ' + data.waiting_key)
    data.seat.map((cc) => {
      formData.append('seat', cc)
    })
    formData.append('tags', data.tags)
    formData.append('price', data.price)

    if (show == true) {

      dispatch(serviceCreate(formData))
    } else {

      dispatch(serviceRegistrationCreate(formData))
    }
  }

  const selectImage = (item) => {
    setData({ ...data, oldimage: item })
    setImage(item)
    setView(false)
  }

  const handleViewClose = () => {
    setView(!view)
  }

  const classes = tableStyles()

  const handleTagBtn = (item) => {
    setData({ ...data, description: data.description.length == 0 ? data.description.concat(item) : data.description.concat(' ', item) })
    setErr({ ...err, description: '' })
  }

  const handleSeatChange = (event) => {
    const {
      target: { value },
    } = event;
    setData({ ...data, seat: typeof value === 'string' ? value.split(',') : value });
    setErr({ ...err, seat: '' })
  }

  return (
    <Grid container spacing={2} className={classes.gridS} >
      <Grid item xs={12} sm={12} md={12}>
        <Card>
          <CardContent className="createService">
            <form>
              <Grid container spacing={2}>
                <Grid className="creatPop" item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                      Create Services
                    </Typography>
                    <Box className="crossIcon">
                      <IconButton onClick={() => { setId(0) }}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                  {/* <TextField name="name" type="text" value={data.name} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Name" style={{ marginBottom: '10px' }} /> */}
                  <Autocomplete
                    options={serveName}
                    noOptionsText="Enter to create a new option"
                    getOptionLabel={(option) => option.name}
                    onInputChange={(e, newValue) => {
                      setData({ ...data, name: newValue });
                    }}
                    value={{ name: data.name }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Name"
                        variant="outlined" InputLabelProps={{
                          shrink: true
                        }}
                        onKeyDown={(e) => {
                          if (
                            e.key === "Enter" &&
                            serveName.findIndex((o) => o.name === data.name) === -1
                          ) {
                            setServeName((o) => o.concat({ name: data.name }));
                          }
                        }}
                      />
                    )}
                  />
                  {/* <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">Names</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={data.name}
                      label="Names"
                      name="name"
                      onChange={handleChange}
                    >
                      {serveName.map((cc) => {
                        return (
                          <MenuItem style={{ textTransform: 'capitalize' }} key={cc._id} value={cc.name}>{cc.name}</MenuItem>
                        )
                      })}
                    </Select>
                  </FormControl> */}
                  {err.name && (<FormError data={err.name}></FormError>)}
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                  <TextField name="motivation" type="text" value={data.motivation} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Motivation" />
                  {err.motivation && (<FormError data={err.motivation}></FormError>)}
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                  <TextField name="price" type="number" inputProps={{ min: 1, step: 1 }} value={data.price} fullWidth
                    variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Price" />
                  {err.price && (<FormError data={err.price}></FormError>)}
                </Grid>
                <Grid item xs={12} sm={12} md={4}>
                  <TextField name="waiting_number" type="number" inputProps={{ min: 0, step: 1 }} value={data.waiting_number} fullWidth
                    //onKeyUp={allowOnlyNumericsOrDigits} 
                    variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Service Time" />
                  {err.waiting_number && (<FormError data={err.waiting_number}></FormError>)}
                </Grid>
                <Grid item xs={12} sm={12} md={4}>
                  <FormControl component="fieldset">
                    <FormLabel component="legend">Service Time</FormLabel>
                    <RadioGroup row aria-label="waitingtime" name="waiting_key" onChange={handleChange} value={data.waiting_key}>
                      <FormControlLabel value="min" control={<Radio />} label="Min" />
                      <FormControlLabel value="hour" control={<Radio />} label="Hour" />
                    </RadioGroup>
                  </FormControl>
                  {err.waiting_key && (<FormError data={err.waiting_key}></FormError>)}
                </Grid>

                <Grid item xs={12} sm={12} md={4}>
                  <Autocomplete
                    limitTags={2}
                    id="multiple-limit-tags"
                    options={EmogiItem}
                    getOptionLabel={(option) => option}
                    isOptionEqualToValue={(option, value) => option === value}
                    value={data.emojiIcon}
                    onChange={(e, n, r) => {
                      console.log('check', r, 'new', n, 'rest', r)
                      setData({ ...data, emojiIcon: n })
                      setErr({ ...err, emojiIcon: '' })
                    }}
                    filterSelectedOptions={true}
                    renderInput={(params) => (
                      <TextField {...params} InputLabelProps={{ shrink: true }} label="Emoji Icon" placeholder="Add Emoji" />
                    )}
                  />
                  {err.emojiIcon && (<FormError data={err.emojiIcon}></FormError>)}
                </Grid>

                <Grid item xs={12} sm={12} md={6}>
                  <FormControl fullWidth>
                    <SelectDataComp title={"Seat"} data={SeatForm((profileD.seat == null || profileD.seat == undefined) ? (registrationD.seat == null || registrationD.seat == undefined) ? 1 : registrationD.seat : profileD.seat)} value={data.seat} handleChange={handleSeatChange} />
                  </FormControl>
                  {err.seat && (<FormError data={err.seat}></FormError>)}
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                  <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label" shrink={true} >Tags</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={data.tags}
                      label="Tags"
                      name="tags"
                      onChange={handleChange}
                    >
                      {tags.map((cc) => {
                        return (
                          <MenuItem style={{ textTransform: 'capitalize' }} key={cc._id} value={cc._id}>{cc.name}</MenuItem>
                        )
                      })}
                    </Select>
                  </FormControl>
                  {err.tags && (<FormError data={err.tags}></FormError>)}
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Typography variant="body1" className="shop-image-title" component="div" align="left">
                    Description
                  </Typography>
                  <textarea name="description" value={data.description} cols="70" rows="10" className={`description ${theme.palette.mode == 'dark' ? 'description-dark' : 'description-light'}`} onChange={handleChange}></textarea>
                  {err.description && (<FormError data={err.description}></FormError>)}
                </Grid>
                <Grid item xs={12} sm={12} md={12} sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }}>
                  {tag.length > 0 && <p className='shop-hint-title' >**Description Template</p>}
                  {tag.length > 0 && <Grid container spacing={1}>
                    {tag.map((cc) => {
                      return (
                        <Grid item xs={6} key={cc._id}>
                          <Chip size="small" sx={{
                            height: '100%',
                            '.MuiChip-label': {
                              overflowWrap: 'break-word',
                              whiteSpace: 'normal',
                              textOverflow: 'clip'
                            }
                          }} onClick={() => { handleTagBtn(cc.type) }} label={cc.name} variant="outlined"></Chip>
                        </Grid>
                      )
                    })}
                  </Grid>}
                </Grid>
                <Grid item xs={12} sm={12} md={12} sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
                  {tag.length > 0 && <p className='shop-hint-title' >**Description Template</p>}
                  {tag.length > 0 && <>
                    {tag.map((cc) => {
                      return (
                        <Chip key={cc._id} size="small" onClick={() => { handleTagBtn(cc.type) }} label={cc.name} variant="outlined"></Chip>
                      )
                    })}
                  </>}
                </Grid>
                <Grid item xs={12} sm={8} md={6}>
                  <TextField name="image" type="file" fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={onHandleImage} label="Image" />
                  {err.image && (<FormError data={err.image}></FormError>)}
                  {galleryData.length > 0 || galleryData.gallery != null && <BookingButton type="button" variant="contained" className="shop-image-select-pr-btn" onClick={() => { setView(!view) }}>Select Images</BookingButton>}
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                  <img src={image} width="100%" height="100%" />
                </Grid>
              </Grid>
              <Grid container spacing={2} sx={{ marginTop: '5px' }} >
                {show == false && <Grid item xs={6} align="left">
                  <BookingButton className='formSubmit-btn' type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId(0) }}>Back</BookingButton>
                </Grid>}
                <Grid item xs={show == true ? 12 : 6} align="right">
                  {show == true && <BookingButton className='formSubmit-btn' type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId(0) }}>Back</BookingButton>}
                  <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Submit</BookingButton>
                </Grid>
              </Grid>
            </form>
          </CardContent>
        </Card>
        <Dialog open={open.open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description" maxWidth="md" fullWidth>
          <DialogContent>
            <NewImageCropper img={open.image} click={CrooppedImageN} ratio={16 / 9} />
          </DialogContent>
        </Dialog>
        <Dialog open={view}
          onClose={handleViewClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description" maxWidth="md" fullWidth>
          <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
            <Box flexGrow={1} >Select Images</Box>
            <Box>
              <IconButton onClick={handleViewClose}>
                <Close />
              </IconButton>
            </Box>
          </Box></DialogTitle>
          <DialogContent>
            {galleryData.length > 0 || galleryData.gallery != null && <Grid container spacing={2}>
              {galleryData.gallery.map((cc) => {
                return (
                  <Grid item xs={4} md={4} sm={4} key={cc._id}>
                    <img
                      src={cc.image}
                      alt="Gallery"
                      loading="lazy"
                      width="100%" height="100%"
                      style={{ cursor: 'pointer' }}
                      onClick={() => { selectImage(cc.image) }}
                    />
                  </Grid>
                )
              })}
            </Grid>}
          </DialogContent>
        </Dialog>
      </Grid>
    </Grid>
  )
}

export default CreateService

const serviceValidate = (data) => {
  let errors = {}

  if (!data.name) {
    errors["name"] = "Name is Required"
  }

  if (!data.motivation) {
    errors["motivation"] = "Motivation is Required"
  }

  if (!data.oldimage) {
    errors["image"] = "Image is Required"
  }

  if (!data.description) {
    errors["description"] = "Description is Required"
  }
  if (!data.tags) {
    errors["tags"] = "Tags is Required"
  }
  if (!data.emojiIcon) {
    errors["emojiIcon"] = "Emoji Icon is Required"
  }
  if (!data.waiting_number) {
    errors["waiting_number"] = "Service Time is Required"
  }
  if (!data.waiting_key) {
    errors["waiting_key"] = "Service Key is Required"
  }
  if (!data.seat) {
    errors["seat"] = "Seat is Required"
  }

  if (data.seat) {
    if (data.seat.length == 0) {
      errors['seat'] = "Seat is Required";
    }
  }

  if (!data.price) {
    errors["price"] = "Price is Required"
  }

  if (data.price == "0") {
    errors["price"] = "Price Must be Greater Than Zero"
  }

  if (data.price == 0) {
    errors["price"] = "Price Must be Greater Than Zero"
  }

  return errors
}