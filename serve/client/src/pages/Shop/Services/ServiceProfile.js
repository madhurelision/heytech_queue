import React, { useEffect, useState } from 'react';
import { AppBar, Autocomplete, Box, Button, Card, CardContent, Dialog, Grid, IconButton, Stack, TextField, Toolbar, Typography, CardMedia, TableBody, TableCell, TableRow, Table, TableHead, Paper, TableContainer, Collapse, DialogTitle, Chip, Checkbox, DialogActions, ListItem, ListItemButton, ListItemText, Divider, List } from '@mui/material';
import FormError from 'pages/Registration/FormError';
import { useNavigate } from 'react-router-dom';
import { getAllService, serviceAllShopClone, serviceClone, shopProfileUpdateService } from 'redux/action';
import { useDispatch, useSelector } from 'react-redux';
import { BookingButton } from "components/common";
import { tableStyles } from 'pages/Shop/Dashboard/styles';
import { AddCircleOutline, Close, Edit, KeyboardArrowDown, KeyboardArrowUp, Cancel, FileCopy } from '@mui/icons-material';
import ImageBox from 'pages/Shop/UsableComponent/ImageBox';
import ServiceSearchComponent from '../UsableComponent/ServiceSearchComponent';
import TooltipBox from '../UsableComponent/TooltipBox';
import { getDateChange } from 'config/KeyData';
import { LoadingButton } from '@mui/lab';

const ServiceProfile = ({ setId }) => {

  const serviceType = useSelector((state) => state.auth.serviceList)
  const profileD = useSelector((state) => state.auth.profile)
  const mainCheckU = useSelector((state) => state.auth.updated)
  const checkTest = useSelector((state) => state.auth.created)
  const loading = useSelector(state => state.auth.loader)
  const createdService = useSelector(state => state.auth.complete)
  const serviceD = useSelector(state => state.auth.serviceSelect);
  const cloneService = useSelector(state => state.auth.clone)
  const total = useSelector((state) => state.auth.serviceListTotal);
  const shopCateData = useSelector((state) => state.auth.shopCategoryList)
  const dispatch = useDispatch()
  const history = useNavigate()
  const [val, setVal] = useState(10)
  const [item, setItem] = useState([]);
  const [cr, setCr] = useState("");


  const classes = tableStyles()
  const [element, setElement] = useState([])
  const [category, setCategory] = useState([])
  const [arr, setArr] = useState([])
  const [ids, setIds] = useState([])
  const [flag, setFlag] = useState(false)
  const [btn, setBtn] = useState(true)
  const [open, setOpen] = useState(false)
  const [clone, setClone] = useState('')
  const [show, setShow] = useState('')
  const [err, setErr] = useState({})
  const [data, setData] = useState({
    topics: []
  })
  const [value, setValue] = useState({
    open: false,
    name: '',
    image: '',
    description: '',
    motivation: '',
    emojiIcon: '',
    waiting_number: '',
    waiting_key: '',
    waiting_time: '',
    tags: '',
    seat: [],
    _id: '',
    price: ''
  })

  useEffect(() => {
    if (serviceType.length == 0) {
      dispatch(getAllService(0, 20))
    } else {
      setElement(serviceType)
      setItem(serviceType.filter((cc) => cc.user_id !== profileD.user_information).slice(0, val))
      setFlag(true)
    }
  }, [serviceType])

  useEffect(() => {

    if (mainCheckU == true) {
      setId(0)
    }
  }, [mainCheckU])


  useEffect(() => {
    if (checkTest == true) {
      var testArr = [...data.topics];
      if (serviceD.length > 0) {
        testArr.push(serviceD[0])
        console.log('testArr', testArr, 'service Craeted', serviceD, 'clone', clone)
        setData({ ...data, topics: testArr })
        setErr({ ...err, topics: '' })
      }
    }
  }, [checkTest, serviceD])

  useEffect(() => {
    if (cloneService == true) {
      var cloneArr = [...data.topics];
      console.log('mains Ids', ids, 'check', cloneArr)
      cloneArr = cloneArr.filter((cc) => !ids.includes(cc._id))
      if (serviceD.length > 0) {
        cloneArr.push(...serviceD)
        console.log('cloneArr', cloneArr, 'service Craeted', serviceD, 'clone', clone)
        setData({ ...data, topics: cloneArr })
        setErr({ ...err, topics: '' })
        setIds([])
        setArr([])
        setBtn(false)
      }
    }
  }, [cloneService, serviceD])

  useEffect(() => {
    if (createdService == true) {
      var mainArr = [...data.topics];
      var aisArr = [...arr];
      mainArr = mainArr.filter((cc) => cc._id !== clone);
      aisArr = aisArr.filter((cc) => cc !== clone);
      if (serviceD.length > 0) {
        mainArr.push(serviceD[0])
        console.log('mainArr', mainArr, 'service Craeted', serviceD, 'clone', clone)
        setData({ ...data, topics: mainArr })
        setErr({ ...err, topics: '' })
        setClone("")
        setArr(aisArr)
        setBtn(false)
      }
    }
  }, [createdService, serviceD])


  useEffect(() => {
    if (shopCateData.length == 0) {
    } else {
      setCategory(shopCateData.category)
    }
  }, [shopCateData])

  const handleService = (e, n, r) => {
    console.log('check', r, 'new', n, 'rest', r)
    setData({ ...data, topics: n })
    setErr({ ...err, topics: '' })
    setBtn(false)
  }

  const handleSubmit = () => {
    var mainCheck = data.topics.filter((cc) => cc.user_id == profileD.user_information)
    var check = checkService({ topics: data.topics, check: mainCheck })
    setErr(check)
    var test = [...profileD.topics].concat(mainCheck)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(shopProfileUpdateService(profileD.user_information, { topics: test.map((cc) => cc._id) }))
  }

  const handleRemoveButton = (val) => {
    var mainArr = [...data.topics];
    mainArr = mainArr.filter((cc) => cc._id !== val);
    setData({ ...data, topics: mainArr })
    setArr(mainArr.map((cc) => cc._id))
  }

  const handleCloneButton = (value) => {
    setClone(value)
    dispatch(serviceClone(value))
  }

  const handleCloneAllBtn = () => {
    var arrIds = data.topics.filter((cc) => cc.user_id !== profileD.user_information)
    console.log('all Ids Which clone aree meant', arrIds)
    var mainCheck = data.topics.filter((cc) => cc.user_id == profileD.user_information)
    var test = [...profileD.topics].concat(mainCheck)
    setIds(arrIds.map((cc) => cc._id))
    if (arrIds.length > 0) {
      dispatch(serviceAllShopClone({ id: arrIds.map((cc) => cc._id), topics: test.map((cc) => cc._id) }))
    } else {
      setErr({ ...err, topics: 'Select Topics that not created by you' })
    }
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleOpen = () => {
    setElement(serviceType)
    setItem(serviceType.filter((cc) => cc.user_id !== profileD.user_information).slice(0, val))
    setOpen(true)
  }

  const handleSelect = (e, item) => {
    var selectArr = [...arr]
    var selectT = [...data.topics]
    if (e.target.checked == true) {
      selectArr.push(item._id);
      selectT.push(item)
      setArr(selectArr)
      setData({ ...data, topics: selectT })
    } else {
      selectArr = selectArr.filter((cc) => cc !== item._id);
      selectT = selectT.filter((cc) => cc._id !== item._id);
      setArr(selectArr)
      setData({ ...data, topics: selectT })
    }
  }

  const handleView = (e, ccd) => {
    if (ccd == null || ccd == undefined) {
      setItem(serviceType.filter((cc) => cc.user_id !== profileD.user_information).slice(0, val))
      setCr("")
    } else {
      setItem(serviceType.filter((cc) => cc._id == ccd._id))
      setCr("h")
    }
  }

  const fetchMoreData = () => {
    dispatch(getAllService(element.length, 10))
    const n = item.length;
    setVal(n + 10)
  };

  return (
    <Grid container spacing={2} className={classes.gridS}>
      <Grid item xs={12} sm={12} md={12}>
        <Card>
          <CardContent>
            <form>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Typography flexGrow={1} variant="h5" component="h2" align="center">
                      Service Profile Update
                    </Typography>
                    <Box>
                      <IconButton onClick={() => { setId(0) }}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <BookingButton type="button" disabled={loading} variant="contained" fullWidth onClick={() => { handleOpen() }}>Select Services</BookingButton>
                  {err.topics && (<FormError data={err.topics}></FormError>)}
                </Grid>
                {btn == true && <Grid item xs={12} sm={6} md={6}>
                  <BookingButton type="button" disabled={loading || data.topics.length == 0} variant="contained" fullWidth onClick={() => { handleCloneAllBtn() }}>Copy And Update</BookingButton>
                </Grid>}
                {btn == false && <Grid item xs={12} sm={6} md={6}>
                  <BookingButton type="button" disabled={loading || btn} variant="contained" fullWidth onClick={() => { handleSubmit() }}>Update</BookingButton>
                </Grid>}
              </Grid>
            </form>

            {
              data.topics.length > 0 && <>
                <Grid container sx={{ marginTop: '20px' }}>
                  <Grid item xs={12} md={12} sm={12} sx={{ border: '1px solid' }}>
                    <Card sx={{ display: { xs: "none", md: "block", sm: "block" } }}>
                      <CardContent>
                        <TableContainer component={Paper} >
                          <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                              <TableRow>
                                <TableCell></TableCell>
                                <TableCell>Name</TableCell>
                                <TableCell>Motivation</TableCell>
                                <TableCell>Emoji Icon</TableCell>
                                <TableCell>Service Time</TableCell>
                                <TableCell>Seat</TableCell>
                                <TableCell>Image</TableCell>
                                <TableCell>Description</TableCell>
                                <TableCell>Shop</TableCell>
                                <TableCell>Remove</TableCell>
                              </TableRow>
                            </TableHead>
                            <TableBody>
                              {data.topics.map((row) => (
                                <TableRow
                                  key={row._id}
                                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                  style={(row.user_id !== profileD.user_information) ? { backgroundColor: 'grey' } : {}}
                                >
                                  <TableCell>
                                    {row.user_id !== profileD.user_information && <div style={{ textAlign: 'center' }}>
                                      <div>{"To Select Service 'click' Copy."}</div>
                                      <BookingButton type="button" disabled={loading} variant="contained" onClick={() => { handleCloneButton(row._id) }}>Copy</BookingButton>
                                    </div>
                                    }
                                    {/* {row.user_id == profileD.user_information &&
                                    <IconButton title="Edit" onClick={() => { handleEdit(row) }}>
                                      <Edit color="primary" />
                                    </IconButton>
                                  } */}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.name}
                                  </TableCell>
                                  <TableCell>{row.motivation}</TableCell>
                                  <TableCell>{row.emojiIcon}</TableCell>
                                  <TableCell>{row?.waiting_number + ' ' + row.waiting_key}</TableCell>
                                  <TableCell>{row?.seat.join()}</TableCell>
                                  <TableCell>
                                    <ImageBox image={row.image} />
                                  </TableCell>
                                  <TableCell>{row.description}</TableCell>
                                  <TableCell>{row.user_id == profileD.user_information ? profileD?.shop_name : row?.user_name}</TableCell>
                                  <TableCell>
                                    <IconButton title="Delete" disabled={loading} onClick={() => { handleRemoveButton(row._id) }}>
                                      <Cancel color="warning" />
                                    </IconButton>
                                  </TableCell>
                                </TableRow>
                              ))}
                            </TableBody>
                          </Table>
                        </TableContainer>
                      </CardContent>
                    </Card>
                    {/* Mobile View */}
                    <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "none" } }} >
                      <Table size="small" aria-label="simple table">
                        <TableBody>
                          {data.topics.map((row) => (
                            <React.Fragment key={row._id}>
                              <TableRow
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                style={(row.user_id !== profileD.user_information) ? { backgroundColor: 'grey' } : {}}
                              >
                                <TableCell style={{ display: 'flex', justifyContent: 'center', flexDirection: 'column' }}>
                                  <Card sx={{ borderRadius: '18px' }} >
                                    <CardMedia
                                      component="img"
                                      sx={{ width: '100%', objectFit: 'unset', borderRadius: '18px', border: 1 }}
                                      image={row.image}
                                      alt={row.name}
                                    />
                                    <Box>
                                      <CardContent>
                                        <Grid container spacing={2}>
                                          <Grid item xs={6} sm={6} >
                                            <div>Name</div>
                                            <div style={{ fontSize: '1.125rem' }}>{row.name}</div>
                                          </Grid>
                                          <Grid item xs={6} sm={6} >
                                            <div>Waiting Time</div>
                                            <div style={{ fontSize: '1.125rem' }}>{row?.waiting_number + ' ' + row.waiting_key}</div>
                                          </Grid>
                                          <Grid item xs={6} sm={6}>
                                            {row.user_id !== profileD.user_information && <div style={{ textAlign: 'center' }}>
                                              <div>{"To Select Service 'click' Copy."}</div>
                                              <Button disabled={loading} sx={{ textTransform: 'capitalize', fontSize: '0.75rem' }} startIcon={<FileCopy color='primary' />} onClick={() => { handleCloneButton(row._id) }} >Copy</Button>
                                            </div>
                                            }
                                          </Grid>
                                          <Grid item xs={6} sm={6}>

                                            <Button disabled={loading} sx={{ textTransform: 'capitalize', fontSize: '0.75rem' }} startIcon={<Cancel color='warning' />} onClick={() => { handleRemoveButton(row._id) }} >
                                              Delete
                                            </Button>
                                          </Grid>
                                        </Grid>
                                      </CardContent>
                                    </Box>
                                  </Card>
                                  {
                                    show == row._id ? <IconButton
                                      aria-label="expand row"
                                      size="small"
                                      onClick={() => setShow('')}
                                    >
                                      <KeyboardArrowUp />
                                    </IconButton> : <IconButton
                                      aria-label="expand row"
                                      size="small"
                                      onClick={() => setShow(row._id)}
                                    >
                                      <KeyboardArrowDown />
                                    </IconButton>
                                  }
                                </TableCell>
                              </TableRow>
                              <TableRow>
                                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                                  <Collapse in={show == row._id} timeout="auto" unmountOnExit>
                                    <Box sx={{ margin: 1 }}>
                                      <Grid container spacing={2}>
                                        <Grid item xs={6} sm={6}>
                                          <div>Motivation</div>
                                          <div style={{ fontSize: '1.125rem' }}>{row.motivation}</div>
                                        </Grid>
                                        <Grid item xs={6} sm={6}>
                                          <div>Description</div>
                                          <div style={{ fontSize: '1.125rem' }}>{row.description}</div>
                                        </Grid>
                                        <Grid item xs={6} sm={6}>
                                          <div>Emoji Icom</div>
                                          <div style={{ fontSize: '1.125rem' }}>{row.emojiIcon}</div>
                                        </Grid>
                                        <Grid item xs={6} sm={6}>
                                          <div>Seat</div>
                                          <div style={{ fontSize: '1.125rem' }}>{row?.seat.join()}</div>
                                        </Grid>
                                        <Grid item xs={4} sm={4}>
                                          <div>Shop</div>
                                          <div style={{ fontSize: '1.125rem' }}>{row.user_id == profileD.user_information ? profileD?.shop_name : row?.user_name}</div>
                                        </Grid>
                                      </Grid>
                                    </Box>
                                  </Collapse>
                                </TableCell>
                              </TableRow>
                            </React.Fragment>
                          ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
                  </Grid>
                </Grid>
              </>
            }
          </CardContent>
        </Card>

        <Dialog
          open={open}
          onClose={handleClose}
          // fullScreen
          maxWidth="md"
          aria-labelledby="responsive-dialog-title"
        >
          <Grid>
            <Card>
              <CardContent>
                <Grid container spacing={2}>
                  <Grid item xs={12} md={12} sm={12}>
                    <Box display="flex" alignItems="center">
                      <Box flexGrow={1} >
                        <Typography variant='h5' component="div">
                          Select Service ({arr.length})
                        </Typography>
                      </Box>
                      <Box>
                        <IconButton onClick={handleClose}>
                          <Close />
                        </IconButton>
                      </Box>
                    </Box>
                  </Grid>
                  <Grid item xs={12} md={3} sm={12}></Grid>
                  <Grid item xs={12} md={6} sm={12}>
                    <ServiceSearchComponent handleView={handleView} status={true} />
                  </Grid>
                  <Grid item xs={12} md={3} sm={12}></Grid>
                  <Grid item xs={12} sm={2} md={2}>
                    <List>
                      {category.map((cc) => <React.Fragment key={cc._id}>
                        <ListItem disablePadding>
                          <ListItemButton>
                            <ListItemText primary={cc.name} />
                          </ListItemButton>
                        </ListItem>
                        <Divider />
                      </React.Fragment>
                      )}
                    </List>
                  </Grid>
                  <Grid item xs={12} md={10} sm={10}>
                    <TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "block" } }}>
                      <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead>
                          <TableRow>
                            <TableCell></TableCell>
                            <TableCell>Name</TableCell>
                            <TableCell>Motivation</TableCell>
                            <TableCell>Emoji Icon</TableCell>
                            <TableCell>Service Time</TableCell>
                            {/* <TableCell>Seat</TableCell> */}
                            <TableCell>Image</TableCell>
                            <TableCell>Description</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {item.map((row) => (
                            <TableRow
                              key={row._id}
                              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                              style={arr.includes(row._id) ? { backgroundColor: 'grey' } : {}}
                            >
                              <TableCell>
                                {/* <Checkbox
                                  color="primary"
                                  inputProps={{ 'aria-label': 'secondary checkbox' }}
                                  onClick={(e) => { handleSelect(e, row) }}
                                  id={row._id}
                                  name={row._id}
                                  value={row._id}
                                  checked={arr.includes(row._id) ? true : false}
                                /> */}
                                {arr.includes(row._id) ? <BookingButton variant="contained" disabled={loading} onClick={() => { handleSelect({ target: { checked: false } }, row) }} >Remove</BookingButton> : <BookingButton variant="contained" disabled={loading} onClick={() => { handleSelect({ target: { checked: true } }, row) }} >Add</BookingButton>}
                              </TableCell>
                              <TableCell component="th" scope="row">
                                {row.name}
                              </TableCell>
                              <TableCell>{row.motivation}</TableCell>
                              <TableCell>{row.emojiIcon}</TableCell>
                              <TableCell>{(row?.waiting_number < 60) ? row?.waiting_number + ' ' + row?.waiting_key : getDateChange(row?.waiting_number)}</TableCell>
                              {/* <TableCell>
                                <div style={{ display: 'flex' }}>
                                  {row?.seat?.map((cc) => {
                                    return <Chip key={cc} label={cc} />
                                  })}
                                </div>
                              </TableCell> */}
                              <TableCell>
                                <ImageBox image={row.image} />
                              </TableCell>
                              <TableCell>
                                <TooltipBox title="hover" data={row.description} />
                              </TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
                    {/* Mobile */}
                    <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "none" } }} >
                      <Table size="small" aria-label="simple table">
                        <TableBody>
                          {item.map((row) => (
                            <TableRow
                              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                              key={row._id}
                            >
                              <TableCell className='customer-table'>
                                <Card sx={{
                                  borderRadius: '18px'
                                }} style={arr.includes(row._id) ? { backgroundColor: 'red' } : {}}>
                                  <CardMedia
                                    component="img"
                                    sx={{ width: '100%', objectFit: 'unset', borderRadius: '18px', border: 1 }}
                                    image={row.image}
                                    alt={row.name}
                                  />
                                  <Box>
                                    <CardContent>
                                      <Grid container spacing={2}>
                                        <Grid item xs={6} sm={6} >
                                          <div style={{ fontSize: '0.75rem' }}>Name</div>
                                          <div style={{ fontSize: '1.0rem' }}>{row.name}</div>
                                        </Grid>
                                        <Grid item xs={6} sm={6} >
                                          <div style={{ fontSize: '0.75rem' }}>Waiting Time</div>
                                          <div style={{ fontSize: '1.0rem' }}>{row?.waiting_number + ' ' + row.waiting_key}</div>
                                        </Grid>
                                        <Grid item xs={6} sm={6}>
                                          <div style={{ fontSize: '0.75rem' }}>Motivation</div>
                                          <div style={{ fontSize: '1.0rem' }}>{row.motivation}</div>
                                        </Grid>
                                        <Grid item xs={6} sm={6}>
                                          <div style={{ fontSize: '0.75rem' }}>Description</div>
                                          <div style={{ fontSize: '1.0rem' }}>{row.description}</div>
                                        </Grid>
                                        <Grid item xs={4} sm={4}>
                                          <div style={{ fontSize: '0.75rem' }}>Emoji Icom</div>
                                          <div style={{ fontSize: '1.0rem' }}>{row.emojiIcon}</div>
                                        </Grid>
                                        {/* <Grid item xs={4} sm={4}>
                                          <div style={{ fontSize: '0.75rem' }}>Seat</div>
                                          <div style={{ fontSize: '1.0rem' }}>{row?.seat.join()}</div>
                                        </Grid> */}
                                        <Grid item xs={4} sm={4}>
                                          <div style={{ fontSize: '0.75rem' }}>Shop</div>
                                          <div style={{ fontSize: '1.0rem' }}>{row?.user_name}</div>
                                        </Grid>
                                        <Grid item xs={12} sm={12} textAlign="center">
                                          {/* <Checkbox
                                            color="primary"
                                            inputProps={{ 'aria-label': 'secondary checkbox' }}
                                            onClick={(e) => { handleSelect(e, row) }}
                                            id={row._id}
                                            name={row._id}
                                            value={row._id}
                                            checked={arr.includes(row._id) ? true : false}
                                          /> */}
                                          {arr.includes(row._id) ? <BookingButton variant="contained" disabled={loading} onClick={() => { handleSelect({ target: { checked: false } }, row) }} >Remove</BookingButton> : <BookingButton variant="contained" disabled={loading} onClick={() => { handleSelect({ target: { checked: true } }, row) }} >Add</BookingButton>}
                                        </Grid>
                                      </Grid>
                                    </CardContent>
                                  </Box>
                                </Card>
                              </TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      </Table>
                    </TableContainer>

                    {/* {element.map((row) => {
                        return (
                          <Grid item xs={12} md={4} sm={6} key={row._id} sx={{ display: 'flex', justifyContent: 'center' }}>
                            <Checkbox
                              color="primary"
                              inputProps={{ 'aria-label': 'secondary checkbox' }}
                              onClick={(e) => { handleSelect(e, row) }}
                              id={row._id}
                              name={row._id}
                              value={row._id}
                              checked={arr.includes(row._id) ? true : false}
                            />
                            <Card sx={(theme) => ({
                              borderRadius: '18px',
                              [theme.breakpoints.up('sm')]: {
                                width: '300px !important'
                              },
                              [theme.breakpoints.up('xs')]: {
                                width: '300px !important'
                              },
                              [theme.breakpoints.up('md')]: {
                                width: '100% !important'
                              },
                            })} style={arr.includes(row._id) ? { backgroundColor: 'red' } : {}}>
                              <CardMedia
                                component="img"
                                sx={{ width: '100%', objectFit: 'unset', borderRadius: '18px', border: 1 }}
                                image={row.image}
                                alt={row.name}
                              />
                              <Box>
                                <CardContent>
                                  <Grid container spacing={2}>
                                    <Grid item xs={6} sm={6} >
                                      <div style={{ fontSize: '0.75rem' }}>Name</div>
                                      <div style={{ fontSize: '1.0rem' }}>{row.name}</div>
                                    </Grid>
                                    <Grid item xs={6} sm={6} >
                                      <div style={{ fontSize: '0.75rem' }}>Waiting Time</div>
                                      <div style={{ fontSize: '1.0rem' }}>{row?.waiting_number + ' ' + row.waiting_key}</div>
                                    </Grid>
                                    <Grid item xs={6} sm={6}>
                                      <div style={{ fontSize: '0.75rem' }}>Motivation</div>
                                      <div style={{ fontSize: '1.0rem' }}>{row.motivation}</div>
                                    </Grid>
                                    <Grid item xs={6} sm={6}>
                                      <div style={{ fontSize: '0.75rem' }}>Description</div>
                                      <div style={{ fontSize: '1.0rem' }}>{row.description}</div>
                                    </Grid>
                                    <Grid item xs={4} sm={4}>
                                      <div style={{ fontSize: '0.75rem' }}>Emoji Icom</div>
                                      <div style={{ fontSize: '1.0rem' }}>{row.emojiIcon}</div>
                                    </Grid>
                                    <Grid item xs={4} sm={4}>
                                      <div style={{ fontSize: '0.75rem' }}>Seat</div>
                                      <div style={{ fontSize: '1.0rem' }}>{row?.seat.join()}</div>
                                    </Grid>
                                    <Grid item xs={4} sm={4}>
                                      <div style={{ fontSize: '0.75rem' }}>Shop</div>
                                      <div style={{ fontSize: '1.0rem' }}>{row?.user_name}</div>
                                    </Grid>
                                  </Grid>
                                </CardContent>
                              </Box>
                            </Card>
                          </Grid>
                        )
                      })} */}
                  </Grid>
                  {element.length > 0 && (cr == null || cr == undefined || cr == "") && <Grid container spacing={2}>
                    {total != element.length && <Grid item xs={12} sm={12} md={12} textAlign="center">
                      <LoadingButton loading={loading}
                        loadingIndicator="Loading..."
                        variant="outlined" disabled={loading} onClick={fetchMoreData} >View More</LoadingButton>
                    </Grid>}
                  </Grid>}
                </Grid>
              </CardContent>
            </Card>
          </Grid>
          <DialogActions>
            <BookingButton variant="contained" disabled={loading} onClick={() => { handleClose() }} >Close</BookingButton>
          </DialogActions>
        </Dialog>
      </Grid>
    </Grid>
  )
}

export default ServiceProfile

const checkService = (data) => {
  let errors = {}

  if (!data.topics) {
    errors['topics'] = "Services is Required";
  }

  if (data.topics) {
    if (data.topics.length == 0) {
      errors['topics'] = "Services is Required ";
    }
    if (data.topics.length > 0 && data.check.length == 0) {
      errors['topics'] = "Services is Required either copy it and create then select it";
    }
  }

  return errors
}