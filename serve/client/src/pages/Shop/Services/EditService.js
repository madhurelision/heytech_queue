import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Autocomplete, Dialog, DialogContent, Button, Typography, FormLabel, RadioGroup, Radio, FormControl, FormControlLabel, DialogTitle, Box, IconButton, Chip, MenuItem, Select, InputLabel, useTheme, useMediaQuery } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import '../../Registration/register.css'
import { getAllServiceTag, serviceRegistrationUpdate, serviceupdate, getAllTagService, getAllServiceName } from 'redux/action'
import { EmogiItem, Seat, SeatForm, ServiceTag } from 'config/KeyData';
import NewImageCropper from '../../ImageCropper/NewImageCropper'
import { Close } from '@mui/icons-material'
import { BookingButton } from "components/common";
import SelectDataComp from '../UsableComponent/SelectDataComp'

const EditService = ({ allvalue, setId, show }) => {

  const theme = useTheme()
  const matches = useMediaQuery(theme.breakpoints.up('md'));
  const [data, setData] = useState({
    name: allvalue.name,
    image: '',
    description: allvalue.description,
    motivation: allvalue.motivation,
    emojiIcon: allvalue.emojiIcon,
    oldimage: allvalue.image,
    waiting_number: allvalue.waiting_number,
    waiting_key: allvalue.waiting_key,
    waiting_time: allvalue.waiting_time,
    seat: allvalue.seat,
    tags: allvalue.tags,
    price: allvalue.price,
  })
  const [open, setOpen] = useState({
    open: false,
    name: '',
    image: ''
  })

  const [image, setImage] = useState(allvalue.image)
  const [err, setErr] = useState({})
  const [view, setView] = useState(false)
  const [btn, setBtn] = useState(true)
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.created)
  const checkVal = useSelector(state => state.auth.complete)
  const loading = useSelector(state => state.auth.loader)
  const galleryData = useSelector((state) => state.auth.shopGallery)
  const profileD = useSelector((state) => state.auth.profile)
  const registrationD = useSelector((state) => state.auth.registrationData)
  const [tag, setTag] = useState([])
  const [tags, setTags] = useState([])
  const serviceTagD = useSelector(state => state.auth.serviceTagList)
  const tagServerD = useSelector(state => state.auth.tagServiceList)
  const [serveName, setServeName] = useState([{ name: allvalue.name }])
  const serviceNameD = useSelector(state => state.auth.serviceNameList)

  useEffect(() => {

    if (serviceTagD.length == 0) {
      dispatch(getAllServiceTag())
    } else {
      setTag(serviceTagD.tag)
    }

  }, [serviceTagD])

  useEffect(() => {

    if (tagServerD.length == 0) {
      dispatch(getAllTagService())
    } else {
      setTags(tagServerD.tag)
    }

  }, [tagServerD])


  useEffect(() => {

    if (serviceNameD.length == 0) {
      dispatch(getAllServiceName())
    } else {
      setServeName(serveName.concat(serviceNameD.serviceName))
    }

  }, [serviceNameD])

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

    if (checkVal) {
      setId(0)
    }

  }, [mainVal, checkVal])


  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
    setBtn(false)
  }

  const onHandleImage = (e) => {

    if (e.target.files == null || e.target.files == undefined) {
      return
    }

    const file = Math.round((e.target.files[0].size / 1024 * 1024));
    console.log('Image Size', e.target.files[0].size, 'Image Size Mb', file)

    if (file > 512000) {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      setErr({ ...err, [e.target.name]: 'Image Size Should be less than 500 kb' })
      return
    }

    const VIDEO_FORMATS = ["video/mp4", "video/mkv"];
    console.log('Image Console', e.target.name, e.target.files)
    console.log('Main', document.getElementsByName(e.target.name)[0].value)
    if (!VIDEO_FORMATS.includes(e.target.files[0].type)) {
      setData({ ...data, [e.target.name]: e.target.files[0] })
      setErr({ ...err, [e.target.name]: '' })
      setOpen({ ...open, open: true, name: e.target.name, image: URL.createObjectURL(e.target.files[0]) })
      setBtn(false)
    } else {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      return
    }
  }


  const CrooppedImageN = (imgss) => {
    console.log('Cropped Image Called', imgss)
    var ext = (imgss.type == "image/jpeg") ? '.jpg' : (imgss.type == "image/png") ? '.png' : '.jpg'
    const values = blobToFile(imgss, open.name + ext)
    console.log('Cropped Image Called', values)
    setData({ ...data, [open.name]: values })
    setOpen({ ...open, open: false })
    setImage(URL.createObjectURL(values))
  }

  const blobToFile = (theBlob, fileName) => {
    return new File([theBlob], fileName, { lastModified: new Date().getTime(), type: theBlob.type })
  }

  const handleClose = () => {
    document.getElementsByName(open.name)[0].value = ''
    setData({ ...data, [open.name]: '' })
    setOpen({ ...open, open: false })
  }

  const handleSubmit = () => {
    var check = serviceValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)

    var formData = new FormData()
    formData.append('name', data.name)
    formData.append('motivation', data.motivation)
    formData.append('emojiIcon', data.emojiIcon)
    formData.append('image', data.image)
    formData.append('oldimage', data.oldimage)
    formData.append('description', data.description)
    formData.append('waiting_number', data.waiting_number)
    formData.append('waiting_key', data.waiting_key)
    formData.append('waiting_time', data.waiting_number + ' ' + data.waiting_key)
    data.seat.map((cc) => {
      formData.append('seat', cc)
    })
    formData.append('tags', data.tags)
    formData.append('price', data.price)

    if (show == true) {

      dispatch(serviceupdate(allvalue?._id, formData))
    } else {

      dispatch(serviceRegistrationUpdate(allvalue?._id, formData))
    }
  }

  // const allowOnlyNumericsOrDigits = (e) => {
  //   const charCode = e.which ? e.which : e.keyCode;

  //   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
  //     setErr({ ...err, waiting_number: 'OOPs! Only numeric values or digits allowed' });
  //   }
  // }


  const selectImage = (item) => {
    setData({ ...data, oldimage: item })
    setImage(item)
    setBtn(false)
    setView(false)
  }

  const handleViewClose = () => {
    setView(!view)
  }

  const handleTagBtn = (item) => {
    setData({ ...data, description: data.description.length == 0 ? data.description.concat(item) : data.description.concat(' ', item) })
    setErr({ ...err, description: '' })
    setBtn(false)
  }

  const handleSeatChange = (event) => {
    const {
      target: { value },
    } = event;
    setData({ ...data, seat: typeof value === 'string' ? value.split(',') : value });
    setErr({ ...err, seat: '' })
    setBtn(false)
  }

  return (
    <Grid >
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid className="creatPop" item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                    Edit Service
                  </Typography>
                  <Box className="crossIcon">
                    <IconButton onClick={() => { setId() }}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Autocomplete
                  options={serveName}
                  noOptionsText="Enter to create a new option"
                  getOptionLabel={(option) => option.name}
                  onInputChange={(e, newValue) => {
                    setData({ ...data, name: newValue });
                  }}
                  value={{ name: data.name }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label="Name"
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true
                      }}
                      onKeyDown={(e) => {
                        if (
                          e.key === "Enter" &&
                          serveName.findIndex((o) => o.name === data.name) === -1
                        ) {
                          setServeName((o) => o.concat({ name: data.name }));
                        }
                      }}
                    />
                  )}
                />
                {/* <TextField name="name" type="text" value={data.name} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Name" style={{ marginBottom: '10px' }} />

                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">Names</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={data.name}
                    label="Names"
                    name="name"
                    onChange={handleChange}
                  >
                    {serveName.map((cc) => {
                      return (
                        <MenuItem style={{ textTransform: 'capitalize' }} key={cc._id} value={cc.name}>{cc.name}</MenuItem>
                      )
                    })}
                  </Select>
                </FormControl> */}
                {err.name && (<FormError data={err.name}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="motivation" type="text" value={data.motivation} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Motivation" />
                {err.motivation && (<FormError data={err.motivation}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="price" type="number" inputProps={{ min: 1, step: 1 }} value={data.price} fullWidth
                  variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Price" />
                {err.price && (<FormError data={err.price}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <TextField name="waiting_number" inputProps={{ min: "0", step: "1" }} type="number"
                  // onKeyPress={(event) => { return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57 }} 
                  //onKeyUp={allowOnlyNumericsOrDigits}
                  value={data.waiting_number} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Service Time" />
                {err.waiting_number && (<FormError data={err.waiting_number}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">Service Time</FormLabel>
                  <RadioGroup row aria-label="waitingtime" name="waiting_key" onChange={handleChange} value={data.waiting_key}>
                    <FormControlLabel value="min" control={<Radio />} label="Min" />
                    <FormControlLabel value="hour" control={<Radio />} label="Hour" />
                  </RadioGroup>
                </FormControl>
                {err.waiting_key && (<FormError data={err.waiting_key}></FormError>)}
              </Grid>

              <Grid item xs={12} sm={12} md={4}>
                <Autocomplete
                  limitTags={2}
                  id="multiple-limit-tags"
                  options={EmogiItem}
                  getOptionLabel={(option) => option}
                  isOptionEqualToValue={(option, value) => option === value}
                  value={data.emojiIcon}
                  onChange={(e, n, r) => {
                    console.log('check', r, 'new', n, 'rest', r)
                    setData({ ...data, emojiIcon: n })
                    setErr({ ...err, emojiIcon: '' })
                    setBtn(false)
                  }}
                  filterSelectedOptions={true}
                  renderInput={(params) => (
                    <TextField {...params} InputLabelProps={{ shrink: true }} label="Emoji Icon" placeholder="Add Emoji" />
                  )}
                />
                {err.emojiIcon && (<FormError data={err.emojiIcon}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                {/* <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">Seat</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={data.seat}
                    label="Seat"
                    name="seat"
                    onChange={handleChange}
                  >
                    {SeatForm((profileD.seat == null || profileD.seat == undefined) ? (registrationD.seat == null || registrationD.seat == undefined) ? 1 : registrationD.seat : profileD.seat).map((cc) => {
                      return (
                        <MenuItem key={cc} value={cc}>{cc}</MenuItem>
                      )
                    })}
                  </Select>
                </FormControl> */}
                <SelectDataComp title={"Seat"} data={SeatForm((profileD.seat == null || profileD.seat == undefined) ? (registrationD.seat == null || registrationD.seat == undefined) ? 1 : registrationD.seat : profileD.seat)} value={data.seat} handleChange={handleSeatChange} />
                {err.seat && (<FormError data={err.seat}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label" shrink={true}>Tags</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={data.tags}
                    label="Tags"
                    name="tags"
                    onChange={handleChange}
                  >
                    {tags.map((cc) => {
                      return (
                        <MenuItem style={{ textTransform: 'capitalize' }} key={cc._id} value={cc._id}>{cc.name}</MenuItem>
                      )
                    })}
                  </Select>
                </FormControl>
                {err.tags && (<FormError data={err.tags}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <Typography variant="body1" className="shop-image-title" component="div" align="left">
                  Description
                </Typography>
                <textarea name="description" value={data.description} cols="40" rows="10" className={`description ${theme.palette.mode == 'dark' ? 'description-dark' : 'description-light'}`} onChange={handleChange}></textarea>
                {err.description && (<FormError data={err.description}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={12} sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }}>
                {tag.length > 0 && <p className='shop-hint-title' >**Description Template</p>}
                {tag.length > 0 && <Grid container spacing={1}>
                  {tag.map((cc) => {
                    return (
                      <Grid item xs={6} key={cc._id}>
                        <Chip key={cc._id} size="small" sx={{
                          height: '100%',
                          '.MuiChip-label': {
                            overflowWrap: 'break-word',
                            whiteSpace: 'normal',
                            textOverflow: 'clip'
                          }
                        }} onClick={() => { handleTagBtn(cc.type) }} label={cc.name} variant="outlined"></Chip>
                      </Grid>
                    )
                  })}
                </Grid>}
              </Grid>
              <Grid item xs={12} sm={12} md={12} sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
                {tag.length > 0 && <p className='shop-hint-title' >**Description Template</p>}
                {/* {ServiceTag.map((cc) => {
                  return (
                    <Chip key={cc.type} size="small" onClick={() => { handleTagBtn(cc.name) }} label={cc.type} variant="outlined"></Chip>
                  )
                })} */}
                {tag.length > 0 && <>
                  {tag.map((cc) => {
                    return (
                      <Chip key={cc._id} size="small" onClick={() => { handleTagBtn(cc.type) }} label={cc.name} variant="outlined"></Chip>
                    )
                  })}
                </>}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="image" type="file" fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={onHandleImage} label="Image" />
                {err.image && (<FormError data={err.image}></FormError>)}
                {galleryData.length > 0 || galleryData.gallery != null && <BookingButton type="button" variant="contained" className="shop-image-select-pr-btn" onClick={() => { setView(!view) }} >Select Images</BookingButton>}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <img src={image} width="100%" height="100%" />
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }}>
              <Grid item xs={6} align="left">
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId(1) }} >Close</BookingButton>
              </Grid>
              <Grid item xs={6} align="right">
                <BookingButton type="button" disabled={(loading || btn) ? true : false} variant="contained" onClick={() => { handleSubmit() }} >Submit</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
      <Dialog open={open.open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description" maxWidth="md" fullWidth>
        <DialogContent>
          <NewImageCropper img={open.image} click={CrooppedImageN} ratio={16 / 9} />
        </DialogContent>
      </Dialog>

      <Dialog open={view}
        onClose={handleViewClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description" maxWidth="md" fullWidth>
        <DialogTitle id="form-dialog-title">
          <Box display="flex" alignItems="center">
            <Box flexGrow={1} >Select Images</Box>
            <Box>
              <IconButton onClick={handleViewClose}>
                <Close />
              </IconButton>
            </Box>
          </Box></DialogTitle>
        <DialogContent>
          {galleryData.length > 0 || galleryData.gallery != null && <Grid container spacing={2}>
            {galleryData.gallery.map((cc) => {
              return (
                <Grid item xs={4} md={4} sm={4} key={cc._id}>
                  <img
                    src={cc.image}
                    alt="Gallery"
                    loading="lazy"
                    width="100%" height="100%"
                    style={{ cursor: 'pointer' }}
                    onClick={() => { selectImage(cc.image) }}
                  />
                </Grid>
              )
            })}
          </Grid>}
        </DialogContent>
      </Dialog>
    </Grid>
  )
}

export default EditService

const serviceValidate = (data) => {
  let errors = {}

  if (!data.name) {
    errors["name"] = "Name is Required"
  }

  if (!data.motivation) {
    errors["motivation"] = "Motivation is Required"
  }

  if (!data.oldimage) {
    errors["image"] = "Image is Required"
  }
  if (!data.tags) {
    errors["tags"] = "Tags is Required"
  }

  if (!data.description) {
    errors["description"] = "Description is Required"
  }
  if (!data.emojiIcon) {
    errors["emojiIcon"] = "Emoji Icon is Required"
  }
  if (!data.waiting_number) {
    errors["waiting_number"] = "Service Time is Required"
  }
  if (!data.waiting_key) {
    errors["waiting_key"] = "Service Key is Required"
  }
  if (!data.seat) {
    errors["seat"] = "Seat is Required"
  }

  if (data.seat) {
    if (data.seat.length == 0) {
      errors['seat'] = "Seat is Required";
    }
  }

  if (!data.price) {
    errors["price"] = "Price is Required"
  }

  if (data.price == "0") {
    errors["price"] = "Price Must be Greater Than Zero"
  }

  if (data.price == 0) {
    errors["price"] = "Price Must be Greater Than Zero"
  }

  return errors
}