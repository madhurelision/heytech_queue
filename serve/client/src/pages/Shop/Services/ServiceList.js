import React, { useEffect, useState } from 'react'
import { Grid, Button, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Typography, Card, CardContent, IconButton, Dialog, DialogTitle, DialogContent, Box, FormGroup, Collapse, CardMedia, DialogActions, Stack, Chip, Checkbox } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { getAllShopCategory, getShopGallery, getShopService, serviceDelete, serviceStatusUpdate, shopProfileData, shopProfileUpdateService } from 'redux/action'
import { AddCircleOutline, Close, DesignServices, Edit, Refresh, KeyboardArrowDown, KeyboardArrowUp, Delete, CheckCircleOutline, HighlightOff } from '@mui/icons-material'
import { LoadingButton } from '@mui/lab'
import EditService from './EditService'
import TableLoader from '../UsableComponent/TableLoader'
import NoData from '../UsableComponent/NoData'
import { tableStyles } from '../Dashboard/styles'
import ImageBox from '../UsableComponent/ImageBox'
import SwitchCompnent from '../UsableComponent/SwitchComponent'
import TooltipBox from '../UsableComponent/TooltipBox'
import { AntSwitch, BookingButton } from "components/common";
import { getDateChange } from 'config/KeyData'
import ServiceShopSearch from '../UsableComponent/ServiceShopSearch'
import MobileLoader from '../UsableComponent/MobileLoader'
import '../Queue/queue.css'
import ServiceProfile from './ServiceProfile'
import CreateService from './CreateService'

const ServiceList = ({ setId }) => {

  const classes = tableStyles()
  const profileD = useSelector((state) => state.auth.profile)
  const [element, setElement] = useState([])
  const [arr, setArr] = useState([])
  const [item, setItem] = useState([])
  const [flag, setFlag] = useState(false)
  const [sort, setSort] = useState(false)
  const [form, setForm] = useState(false)
  const [show, setShow] = useState(false)
  const [view, setView] = useState('')
  const serviceData = useSelector((state) => state.auth.shopService)
  const galleryData = useSelector((state) => state.auth.shopGallery)
  const shopCateData = useSelector((state) => state.auth.shopCategoryList)
  const allCount = useSelector(state => state.auth.serviceCount)
  const cted = useSelector((state) => state.auth.created)
  const mainT = useSelector((state) => state.auth.updated)
  const loading = useSelector((state) => state.auth.loader)
  const dispatch = useDispatch()
  const [data, setData] = useState({
    open: false,
    name: '',
    image: '',
    description: '',
    motivation: '',
    emojiIcon: '',
    waiting_number: '',
    waiting_key: '',
    waiting_time: '',
    seat: '',
    tags: '',
    price: '',
    _id: ''
  })

  const [cancel, setCancel] = useState({
    open: false,
    _id: ''
  })

  useEffect(() => {

    if (serviceData.length == 0) {
      dispatch(getShopService(0, 10))
    } else {
      setElement(serviceData.service)
      setFlag(true)
    }

    if (cted) {
      setCancel({ ...cancel, open: false })
      dispatch(getShopService(0, element.length))
    }

  }, [serviceData, cted])


  useEffect(() => {
    if (shopCateData.length == 0) {
      dispatch(getAllShopCategory())
    }
  }, [shopCateData])

  useEffect(() => {

    if (Object.keys(profileD).length == 0) {
      dispatch(shopProfileData())
    } else {
      setArr(profileD?.topics?.map((cc) => cc._id))
    }

    if (mainT == true) {
      dispatch(shopProfileData())
      setItem([])
    }

    return () => {
      setItem([])
    }
  }, [profileD, mainT])

  useEffect(() => {

    if (galleryData.length == 0) {
      dispatch(getShopGallery())
    }
  }, [galleryData])

  const handleEdit = (val) => {
    setData({
      ...data,
      open: true,
      name: val.name,
      image: val.image,
      description: val.description,
      motivation: val.motivation,
      emojiIcon: val.emojiIcon,
      waiting_number: val.waiting_number,
      waiting_key: val.waiting_key,
      waiting_time: val.waiting_time,
      seat: (val.seat == null || val.seat == undefined) ? 1 : val.seat,
      tags: val.tags,
      price: val.price,
      _id: val._id
    })
  }

  const handleClose = () => {
    setData({ ...data, open: false })
  }

  const handleRefresh = () => {
    dispatch(getShopService(0, element.length > 0 ? element.length : 10))
  }


  const handleChangeStatus = (e, id) => {
    console.log('ket', e.target.checked, 'id', id)
    dispatch(serviceStatusUpdate(id, { accept: e.target.checked }))
  }

  const handleDeleteOpen = (val) => {
    setCancel({
      ...cancel,
      open: true,
      _id: val
    })
  }

  const handleDeleteClose = () => {
    setCancel({ ...cancel, open: false })
  }

  const handleDelete = (row) => {
    dispatch(serviceDelete(row))
  }

  const handleSelect = (e, id) => {
    var selectArr = [...arr]
    if (e.target.checked == true) {
      selectArr.push(id);
      setArr(selectArr)
      dispatch(shopProfileUpdateService(profileD.user_information, { topics: selectArr }))
    } else {
      selectArr = selectArr.filter((cc) => cc !== id);
      setArr(selectArr)
      dispatch(shopProfileUpdateService(profileD.user_information, { topics: selectArr }))
    }
  }

  const handleMainSelect = (e, id) => {
    var selectArr = [...item]
    if (e.target.checked == true) {
      selectArr.push(id);
      setItem(selectArr)
    } else {
      selectArr = selectArr.filter((cc) => cc !== id);
      setItem(selectArr)
    }
  }

  const handleAddButton = () => {
    var m = [...new Set(arr.concat(item))]
    dispatch(shopProfileUpdateService(profileD.user_information, { topics: m }))
  }

  const handleRemoveButton = () => {
    var c = arr.filter(val => !item.some(itemToBeRemoved => itemToBeRemoved === val))
    dispatch(shopProfileUpdateService(profileD.user_information, { topics: c }))
  }

  const handleSelectAll = (e) => {
    if (e.target.checked == true) {
      setItem(element.map((cc) => cc._id))
    } else {
      setItem([])
    }
  }


  const [value, setValue] = useState('');

  const handleChange = (e) => {
    setValue(e.target.value)
  }

  const fetchMoreData = () => {
    dispatch(getShopService(element.length, 10))
  };

  const handleBtn = () => {

    if (value == null || value == undefined || value == "") {
      setElement(serviceData.service)
      return
    }
    setElement(serviceData.service.reduce(function (newVal, data) {
      if (data.name.includes(value)) {
        return newVal.concat(data);
      } else if (data.description.includes(value)) {
        return newVal.concat(data)
      } else if (data.motivation.includes(value)) {
        return newVal.concat(data)
      } else {
        return newVal
      }
    }, []))
  }

  const handleView = (e, ccd) => {
    if (ccd == null || ccd == undefined) {
      setElement(serviceData.service)
      setValue("")
    } else {
      setElement(serviceData.service.filter((cc) => cc._id == ccd._id))
      setValue("h")
    }
  }

  const handleSeatSort = (sort) => {
    if (element.length == 0) {
      return
    }
    if (sort == false) {
      const serviceSort = serviceData.service.sort((a, b) => {
        return b.seat - a.seat
      })
      setElement(serviceSort)
      setSort(true)
    } else {
      const servicesSort = serviceData.service.sort((a, b) => {
        return a.seat - b.seat
      })
      setElement(servicesSort)
      setSort(false)
    }
  }

  const handleServiceSort = (sort) => {
    if (element.length == 0) {
      return
    }
    if (sort == false) {
      const serviceSort = serviceData.service.sort((a, b) => {
        return arr.includes(b._id) - arr.includes(a._id)
      })
      setElement(serviceSort)
      setSort(true)
    } else {
      const servicesSort = serviceData.service.sort((a, b) => {
        return arr.includes(a._id) - arr.includes(b._id)
      })
      setElement(servicesSort)
      setSort(false)
    }
  }


  const handleViewCard = (row) => {
    if (view == row._id) {
      setView("")
    } else {
      setView(row._id)
    }
  }

  const handleFormClose = () => {
    setForm(false)
  }

  const handleProfileClose = () => {
    setShow(false)
  }


  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card>
          <CardContent>

            <Grid className="serviceListHead" container spacing={2}>
              {Object.keys(profileD).length > 0 && profileD.topics.length == 0 && <Grid item xs={12} md={12} sm={12}>
                <div className="shop-queue-tab text-danger fs-3 fw-bold">
                  Service is Required Please Select in Profile or Create Service
                </div>
              </Grid>}
              <Grid item xs={12} sm={2} md={2} >
                <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh} >
                  Refresh
                </Button>
              </Grid>
              <Grid item xs={12} sm={7} md={8}>
                <Typography className='title_head' variant="h5" component="div" align="center">
                  Service List ({(element.length == 0) ? 0 : allCount})
                </Typography>
              </Grid>
              <Grid className="listBtn" item xs={12} sm={3} md={2}>
                <Button className='customer-btn' startIcon={<DesignServices color='primary' />} onClick={() => { setShow(true) }}>
                  Add
                </Button>

                <Button className='customer-btn' startIcon={<AddCircleOutline color='primary' />} onClick={() => { setForm(true) }} >
                  Create
                </Button>
              </Grid>
              {element.length > 0 && <Grid item xs={12} sm={6} md={6} style={{ paddingBlock: '20px' }}>
                <ServiceShopSearch handleView={handleView} />
              </Grid>}
              {element.length > 0 && item.length > 0 && <Grid item xs={12} sm={6} md={6} style={{ paddingBlock: '20px' }}>
                {/* <BookingButton type="button" disabled={loading} variant="contained" className='customer-btn formSubmit-btn' onClick={() => { handleAddButton() }}> Activate</BookingButton>
                <BookingButton type="button" disabled={loading} variant="contained" className='customer-btn' onClick={() => { handleRemoveButton() }}>Deactivate</BookingButton> */}
                <IconButton className="view-grid-main" disabled={loading} title={"Activate"} onClick={() => { handleAddButton() }}>
                  <CheckCircleOutline color='primary' className='locationBtn-font' />
                  <span style={{ fontSize: '12px' }}>Activate</span>
                </IconButton>
                <IconButton className="view-grid-main" disabled={loading} title={"Deactivate"} onClick={() => { handleRemoveButton() }}>
                  <HighlightOff color='warning' className='locationBtn-font' />
                  <span style={{ fontSize: '12px' }}>Deactivate</span>
                </IconButton>
              </Grid>}
            </Grid>
            <TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "block" } }}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>
                      {element.length > 0 && <Checkbox
                        indeterminate={item.length > 0 && item.length < element.length}
                        checked={element.length > 0 && item.length === element.length}
                        onChange={handleSelectAll}
                        inputProps={{ 'aria-label': 'select all desserts' }}
                      />}
                    </TableCell>
                    <TableCell></TableCell>
                    <TableCell onClick={() => { handleServiceSort(sort) }}>Activate / Deactivate</TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>Motivation</TableCell>
                    <TableCell>Emoji Icon</TableCell>
                    <TableCell>Service Time</TableCell>
                    <TableCell onClick={() => { handleSeatSort(sort) }} >Seat</TableCell>
                    <TableCell>Price</TableCell>
                    <TableCell>Image</TableCell>
                    <TableCell>Description</TableCell>
                    <TableCell>Booking Status</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <TableRow
                      key={row._id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell>
                        <Checkbox
                          disabled={loading}
                          id={row._id}
                          name={row._id}
                          value={row._id}
                          checked={item.includes(row._id) ? true : false}
                          onClick={(e) => { handleMainSelect(e, row._id) }}
                          inputProps={{ 'aria-label': 'ant design' }}
                        />
                      </TableCell>
                      <TableCell>
                        <div style={{ display: 'flex' }}>
                          <IconButton title="Edit" onClick={() => { handleEdit(row) }}>
                            <Edit color='primary' />
                          </IconButton>
                          <IconButton title="Delete" onClick={() => { handleDeleteOpen(row._id) }}>
                            <Delete color='warning' />
                          </IconButton>
                        </div>
                      </TableCell>
                      <TableCell>
                        <FormGroup>
                          <Stack direction="row" spacing={1} alignItems="center">
                            <Typography>Deactivate</Typography>
                            <AntSwitch
                              disabled={loading}
                              id={row._id}
                              name={row._id}
                              value={row._id}
                              checked={arr.includes(row._id) ? true : false}
                              onClick={(e) => { handleSelect(e, row._id) }}
                              inputProps={{ 'aria-label': 'ant design' }} />
                            <Typography>Activate</Typography>
                          </Stack>
                        </FormGroup>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {row.name}
                      </TableCell>
                      <TableCell>{row.motivation}</TableCell>
                      <TableCell>{row.emojiIcon}</TableCell>
                      <TableCell>{(row?.waiting_number < 60) ? row?.waiting_number + ' ' + row?.waiting_key : getDateChange(row?.waiting_number)}</TableCell>
                      <TableCell>
                        <div style={{ display: 'flex' }}>
                          {row?.seat?.map((cc) => {
                            return <Chip key={cc} label={cc} />
                          })}
                        </div>
                      </TableCell>
                      <TableCell>
                        {row?.price}
                      </TableCell>
                      <TableCell>
                        <ImageBox image={row.image} />
                      </TableCell>
                      <TableCell>
                        <TooltipBox title="hover" data={row.description} />

                      </TableCell>
                      <TableCell>
                        <FormGroup>
                          <SwitchCompnent label="Accept" value={row.accept} handleChange={handleChangeStatus} row={row._id} />
                        </FormGroup>
                      </TableCell>
                    </TableRow>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <TableLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            {/* Mobile View */}
            <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "none" } }} >
              <Table size="small" aria-label="simple table">
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <React.Fragment key={row._id}>
                      <TableRow
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell className='customer-table'>
                          <Card sx={{ borderRadius: '18px' }} onClick={() => { handleViewCard(row) }}>
                            <CardMedia
                              component="img"
                              sx={{ width: '100%', objectFit: 'unset', borderRadius: '18px', border: 1 }}
                              image={row.image}
                              alt={row.name}
                            />
                            <Box>
                              <CardContent>
                                <Grid container spacing={2}>
                                  <Grid item xs={6} sm={6} >
                                    <div>Name</div>
                                    <div className='customer-font'>{row.name}</div>
                                  </Grid>
                                  <Grid item xs={6} sm={6} >
                                    <div>Waiting Time</div>
                                    <div className='customer-font'>{(row?.waiting_number < 60) ? row?.waiting_number + ' ' + row?.waiting_key : getDateChange(row?.waiting_number)}</div>
                                  </Grid>
                                </Grid>
                              </CardContent>
                            </Box>
                          </Card>
                          {
                            view == row._id ? <IconButton
                              aria-label="expand row"
                              size="small"
                              onClick={() => setView('')}
                            >
                              <KeyboardArrowUp />
                            </IconButton> : <IconButton
                              aria-label="expand row"
                              size="small"
                              onClick={() => setView(row._id)}
                            >
                              <KeyboardArrowDown />
                            </IconButton>
                          }
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className='customer-tablecell' colSpan={6}>
                          <Collapse in={view == row._id} timeout="auto" unmountOnExit>
                            <Box sx={{ margin: 1 }}>
                              <Grid container spacing={2}>
                                <Grid item xs={6} sm={6}>
                                  <div>Motivation</div>
                                  <div className='customer-font'>{row.motivation}</div>
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                  <div>Description</div>
                                  <div className='customer-font'>{row.description}</div>
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                  <div>Emoji Icom</div>
                                  <div className='customer-font'>{row.emojiIcon}</div>
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                  <div>Price</div>
                                  <div className='customer-font'>{row?.price}</div>
                                </Grid>
                                <Grid item xs={6} sm={6}>
                                  <div>Seat</div>
                                  <div className='customer-font'>{row?.seat?.map((cc) => {
                                    return <Chip key={cc} label={cc} />
                                  })}</div>
                                </Grid>
                                <Grid item xs={4} sm={4}>
                                  <Button className='customer-btn' startIcon={<Edit color='primary' />} onClick={() => { handleEdit(row) }} >
                                    Edit
                                  </Button>
                                </Grid>
                                <Grid item xs={4} sm={4}>

                                  <Button className='customer-btn' startIcon={<Delete color='warning' />} onClick={() => { handleDeleteOpen(row._id) }} >
                                    Delete
                                  </Button>
                                </Grid>
                                <Grid item xs={4} sm={4}>
                                  <SwitchCompnent label="Accept" value={row.accept} handleChange={handleChangeStatus} row={row._id} />

                                </Grid>
                                <Grid item xs={4} sm={4}>
                                  <FormGroup>
                                    <Stack direction="row" spacing={1} alignItems="center">
                                      <Typography>Deactivate</Typography>
                                      <AntSwitch
                                        disabled={loading}
                                        id={row._id}
                                        name={row._id}
                                        value={row._id}
                                        checked={arr.includes(row._id) ? true : false}
                                        onClick={(e) => { handleSelect(e, row._id) }}
                                        inputProps={{ 'aria-label': 'ant design' }} />
                                      <Typography>Activate</Typography>
                                    </Stack>
                                  </FormGroup>
                                </Grid>
                                <Grid item xs={12} sm={12} textAlign="center">
                                  <Checkbox
                                    disabled={loading}
                                    id={row._id}
                                    name={row._id}
                                    value={row._id}
                                    checked={item.includes(row._id) ? true : false}
                                    onClick={(e) => { handleMainSelect(e, row._id) }}
                                    inputProps={{ 'aria-label': 'ant design' }}
                                  />
                                </Grid>
                              </Grid>
                            </Box>
                          </Collapse>
                        </TableCell>
                      </TableRow>
                    </React.Fragment>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <MobileLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
          </CardContent>
          {element.length >= 10 && (value == null || value == undefined || value == "") && <Grid container spacing={2}>
            {allCount != element.length && <Grid item xs={12} sm={12} md={12} textAlign="center">
              <LoadingButton loading={loading}
                loadingIndicator="Loading..."
                variant="outlined" disabled={loading} onClick={fetchMoreData} >View More</LoadingButton>
            </Grid>}
          </Grid>}
        </Card>
      </Grid>
      <Dialog
        open={data.open}
        onClose={handleClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <EditService allvalue={data} setId={handleClose} show={true} />
      </Dialog>
      <Dialog
        open={cancel.open}
        onClose={handleDeleteClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Delete Service</Box>
          <Box>
            <IconButton onClick={handleDeleteClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        <DialogContent>
          Are You Sure You want to Delete??
        </DialogContent>
        <DialogActions>
          <BookingButton fullWidth variant="contained" disabled={loading} onClick={() => { handleDelete(cancel._id) }} >Yes</BookingButton>
          <BookingButton fullWidth variant="contained" disabled={loading} onClick={handleDeleteClose} >No</BookingButton>
        </DialogActions>
      </Dialog>
      <Dialog
        open={form}
        onClose={() => {
          if (loading == true) {

          } else {
            handleFormClose()
          }
        }}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <CreateService setId={handleFormClose} show={true} />
      </Dialog>
      <Dialog
        open={show}
        onClose={() => {
          if (loading == true) {

          } else {
            handleProfileClose()
          }
        }}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <ServiceProfile setId={handleProfileClose} />
      </Dialog>
    </Grid>
  )
}

export default ServiceList
