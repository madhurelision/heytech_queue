import React, { useState } from 'react'
import CreateService from './CreateService'
import ServiceList from './ServiceList'
import ServiceProfile from './ServiceProfile'

const ServicePage = () => {

  const [id, setId] = useState(0)

  switch (id) {
    case 0:
      return <ServiceList setId={setId} />
    case 1:
      return <CreateService setId={setId} show={true} />
    case 2:
      return <ServiceProfile setId={setId} />
    default:
      return <ServiceList setId={setId} />
  }
}

export default ServicePage
