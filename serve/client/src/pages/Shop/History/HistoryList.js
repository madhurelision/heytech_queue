import React, { useContext, useEffect, useState } from 'react'
import { Grid, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Typography, Card, CardContent, DialogTitle, DialogContent, Box, Dialog, IconButton, Button, TextField, Stack, Collapse } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import TableLoader from '../UsableComponent/TableLoader'
import NoData from '../UsableComponent/NoData'
import { getShopBooking, getShopQueryBooking, getShopQueryBookingUpdate } from 'redux/action'
import { tableStyles } from '../Dashboard/styles'
import StatusBox from '../UsableComponent/StatusBox'
import moment from 'moment'
import DialogBox from '../Dashboard/DialogBox'
import ShopAppointmentView from '../Dashboard/ShopAppointmentView'
import EmailBox from '../UsableComponent/EmailBox'
import ReviewForm from '../ReviewForm/ReviewForm'
import { Close, Edit, KeyboardArrowDown, MoreVert, PersonSearch, Refresh, Reviews, Search } from '@mui/icons-material'
import { DesktopDatePicker, LocalizationProvider, MobileDatePicker } from '@mui/lab'
import SocketContext from 'config/socket'
import { SHOP_COUNT, SOCKET_SHOP_NOTIFICATION } from 'redux/action/types'
import TooltipBox from '../UsableComponent/TooltipBox'
import AdapterDateFns from '@mui/lab/AdapterDateFns'
import KeyboardArrowUp from '@mui/icons-material/KeyboardArrowUp'
import { ChangeNumber, getDateChange, keyLengthValidation } from 'config/KeyData'
import HistoryCard from '../UsableComponent/HistoryCard'
import { LoadingButton } from '@mui/lab'
import MobileLoader from '../UsableComponent/MobileLoader'
import '../Queue/queue.css'

var DailogData
const HistoryList = () => {

  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const [open, setOpen] = useState(false)
  const [view, setView] = useState('')
  const bookingData = useSelector((state) => state.auth.shopBooking)
  const allCount = useSelector(state => state.auth.shopHistoryCount)
  const cted = useSelector((state) => state.auth.updated)
  const loading = useSelector((state) => state.auth.loader)
  const dispatch = useDispatch()
  const classes = tableStyles()
  const [data, setData] = useState({
    open: false,
    shop_id: '',
    user_id: '',
    shop_email: '',
    shop_name: '',
    user_name: '',
    user_email: '',
    booking_id: '',
    service_id: '',
    image: '',
    shop_image: '',
    mobile: ''
  })

  const [value, setValue] = useState({
    start_date: new Date(),
    end_date: new Date(),
    search: ''
  });


  const [book, setBook] = useState({
    open: false,
    data: {}
  })


  const context = useContext(SocketContext)


  useEffect(() => {

    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
    })

    context.socket.on('userDisconnect', (data) => {
      console.log('Socket Err', data)
    })

    context.socket.on('shopCount', (data) => {
      //console.log('Shop Count Data', data)
      dispatch({ type: SHOP_COUNT, payload: data })
    })

    context.socket.on('notificationShopCount', (data) => {
      //console.log('notificationCount', data)
      dispatch({ type: SOCKET_SHOP_NOTIFICATION, payload: data })
    })

    context.socket.on('disconnect', () => {
      console.log('socket is disconnect')
    })

    return () => {
      context.socket.removeListener(['shopCount'])
    }
  }, [])

  useEffect(() => {

    if (bookingData.length == 0) {
      dispatch(getShopQueryBooking({ startDate: value.start_date, endDate: value.end_date }, 0, 10))
    } else {
      setElement(bookingData.booking)
      setFlag(true)
    }

    if (cted) {
      dispatch(getShopQueryBookingUpdate(data.booking_id))
    }

  }, [bookingData, cted])


  const handleDialogClose = () => {
    setOpen(false)
  }

  const handleDialogOpen = (item) => {
    DailogData = <ShopAppointmentView item={item} />
    setOpen(true)
  }

  const handleClose = () => {
    setData({ ...data, open: false })
  }

  const handleOpen = (item) => {
    setData({
      ...data,
      open: true,
      shop_id: item.shop_id.user_information,
      user_id: item.user_id._id,
      shop_email: item.shop_email,
      shop_name: item.shop_name,
      user_name: item.user_id.name,
      user_email: item.user_email,
      booking_id: item._id,
      service_id: item.service_id._id,
      image: item.user_id.image_link,
      shop_image: item.shop_id.image_link,
      mobile: item.mobile
    })
  }

  const handleRefresh = () => {
    dispatch(getShopQueryBooking({ startDate: value.start_date, endDate: value.end_date }, 0, 10))
  }

  const handleStartDate = (newValue) => {
    console.log('new date', newValue)
    setValue({ ...value, start_date: newValue });
    dispatch(getShopQueryBooking({ startDate: newValue, endDate: value.end_date }, 0, 10))
  };

  const handleEndDate = (newValue) => {
    console.log('new date', newValue)
    setValue({ ...value, end_date: newValue });
    dispatch(getShopQueryBooking({ startDate: value.start_date, endDate: newValue }, 0, 10))
  };

  const handleSearch = () => {
    console.log('Search Data', value.start_date, value.end_date)
    dispatch(getShopQueryBooking({ startDate: value.start_date, endDate: value.end_date }, 0, 10))
  }

  const handleChange = (e) => {
    setValue({ ...value, [e.target.name]: e.target.value })
  }

  const handleBtn = () => {

    if (value.search == null || value.search == undefined || value.search == "") {
      setElement(bookingData.booking)
      return
    }
    setElement(bookingData.booking.reduce(function (newVal, data) {
      if (data.user_id.name.includes(value.search)) {
        return newVal.concat(data);
      } else if (data.description.includes(value.search)) {
        return newVal.concat(data)
      } else if (data.service_id.name.includes(value.search)) {
        return newVal.concat(data)
      } else if (data.mobile == parseInt(value.search)) {
        return newVal.concat(data)
      } else if (data.user_email.includes(value.search)) {
        return newVal.concat(data)
      } else if (data.status.includes(value.search)) {
        return newVal.concat(data)
      } else {
        return newVal
      }
    }, []))
  }


  const fetchMoreData = () => {
    dispatch(getShopQueryBooking({ startDate: value.start_date, endDate: value.end_date }, element.length, 10))
  };


  const handleViewCard = (row) => {
    setBook({
      ...book,
      open: true,
      data: row,
    })
  }

  const handleBookingClose = () => {
    setBook({ ...book, open: false })
  }

  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card>
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={2} md={2} >
                {/* <IconButton title="Refresh" disabled={loading} onClick={handleRefresh} >
                  <Refresh />
                </IconButton> */}
                <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh} >
                  Refresh
                </Button>
              </Grid>
              <Grid item xs={12} sm={8} md={8} >
                <Typography className='title_head' variant="h5" component="div" align="center">
                  History List
                </Typography>
              </Grid>
              <Grid item xs={12} sm={2} md={2} ></Grid>

              <Grid item xs={5} sm={5} md={5}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <DesktopDatePicker
                    label="Start Date"
                    inputFormat="MM/dd/yyyy"
                    value={value.start_date}
                    onChange={handleStartDate}
                    maxDate={new Date()}
                    renderInput={(params) => <TextField {...params} fullWidth sx={{ display: { xs: 'none', sm: 'block', md: 'block' } }} />}
                  />
                  <MobileDatePicker
                    label="Start Date"
                    inputFormat="MM/dd/yyyy"
                    value={value.start_date}
                    onChange={handleStartDate}
                    maxDate={new Date()}
                    renderInput={(params) => <TextField {...params} fullWidth sx={{ display: { xs: 'block', sm: 'none', md: 'none' } }} />}
                  />
                </LocalizationProvider>
              </Grid>
              <Grid item xs={5} sm={5} md={5}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <DesktopDatePicker
                    label="End Date"
                    inputFormat="MM/dd/yyyy"
                    value={value.end_date}
                    onChange={handleEndDate}
                    minDate={value.start_date}
                    maxDate={new Date()}
                    renderInput={(params) => <TextField {...params} fullWidth sx={{ display: { xs: 'none', sm: 'block', md: 'block' } }} />}
                  />
                  <MobileDatePicker
                    label="End Date"
                    inputFormat="MM/dd/yyyy"
                    value={value.end_date}
                    onChange={handleEndDate}
                    minDate={value.start_date}
                    maxDate={new Date()}
                    renderInput={(params) => <TextField {...params} fullWidth sx={{ display: { xs: 'block', sm: 'none', md: 'none' } }} />}
                  />
                </LocalizationProvider>
              </Grid>
              <Grid item xs={2} sm={2} md={2} justifyContent="center" alignItems="center" textAlign="center" >
                <IconButton title="Find" disabled={loading} onClick={handleSearch} sx={{ height: '100%' }} >
                  <Search />
                </IconButton>
              </Grid>
              <Grid item xs={6} sm={6} md={6} >
                <TextField label="Search By Name Service" placeholder="Search By Name" name="search" value={value.search} onChange={handleChange} fullWidth />
              </Grid>
              <Grid item xs={6} sm={6} md={6} >
                <IconButton title="Search" disabled={loading} onClick={handleBtn} sx={{ height: '100%' }} >
                  <PersonSearch />
                </IconButton>
              </Grid>
            </Grid>
            {/* Desktop View */}
            <TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "block" } }} >
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Review / Details</TableCell>
                    <TableCell>Appointment Date</TableCell>
                    <TableCell>Appointment Time</TableCell>
                    <TableCell>Service Name</TableCell>
                    <TableCell>Description</TableCell>
                    <TableCell>User Name</TableCell>
                    <TableCell>User Number</TableCell>
                    <TableCell>User Email</TableCell>
                    <TableCell>Status</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <TableRow
                      key={row._id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell>
                        {(row.status == 'completed' && row.shop_review == false) && <IconButton title="Review" onClick={() => { handleOpen(row) }}>
                          <Reviews />
                        </IconButton>}
                        <IconButton title="Details" onClick={() => { handleDialogOpen(row) }} >
                          <MoreVert />
                        </IconButton>
                      </TableCell>
                      <TableCell >{moment(row.appointment_date).format('LL')}</TableCell>
                      <TableCell >{moment(row.appointment_date).calendar()}</TableCell>
                      <TableCell >{row?.service_id.name}</TableCell>
                      <TableCell >
                        <TooltipBox title="hover" data={row.description} />
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {row?.user_id.name}
                      </TableCell>
                      <TableCell >{ChangeNumber(row.mobile)}</TableCell>
                      <TableCell >
                        <EmailBox title={row.user_email} />
                      </TableCell>
                      <TableCell >
                        <StatusBox value={row.status} />
                      </TableCell>
                    </TableRow>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <TableLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>

            {/* Mobile View */}
            <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "none" } }} >
              <Table size="small" aria-label="simple table">
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <React.Fragment key={row._id}>
                      <TableRow
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell className='customer-table'>
                          <HistoryCard {...row} handleOpen={handleOpen} handleDialogOpen={handleDialogOpen} handleClick={handleViewCard} />
                          {/* {
                            view == row._id ? <IconButton
                              aria-label="expand row"
                              size="small"
                              onClick={() => setView('')}
                            >
                              <KeyboardArrowUp />
                            </IconButton> : <IconButton
                              aria-label="expand row"
                              size="small"
                              onClick={() => setView(row._id)}
                            >
                              <KeyboardArrowDown />
                            </IconButton>
                          } */}
                        </TableCell>
                      </TableRow>
                      {/* <TableRow>
                        <TableCell className='customer-tablecell' colSpan={6}>
                          <Collapse in={view == row._id} timeout="auto" unmountOnExit>
                            <Box sx={{ margin: 1 }}>
                              <Grid container spacing={2}>
                                <Grid item xs={6} sm={6}>
                                  <div>{row.description}</div>
                                </Grid>
                                <Grid item xs={6} sm={6} >
                                  <div>Waiting Time</div>
                                  <div className='customer-font'>
                                    {(!row.waiting_number || row.waiting_number == null || row.waiting_number == undefined) ? row?.waiting_time : (row?.waiting_number < 60) ? row?.waiting_number + ' ' + row.waiting_key : getDateChange(row.waiting_number)}
                                  </div>
                                </Grid>
                                <Grid item xs={6} sm={6} >
                                  <div>Service Time</div>
                                  <div className='customer-font'>
                                    {(!row.approximate_number || row.approximate_number == null || row.approximate_number == undefined) ? row?.approximate_time : (row?.approximate_number < 60) ? row?.approximate_number + ' ' + row.approximate_key : getDateChange(row.approximate_number)}
                                  </div>
                                </Grid>
                                <Grid item xs={12} sm={12}>
                                  <div>{row?.user_id?.first_name}</div>
                                  <div>{row.user_email}</div>
                                  <div>{ChangeNumber(row.mobile)}</div>
                                </Grid>
                              </Grid>
                            </Box>
                          </Collapse>
                        </TableCell>
                      </TableRow> */}
                    </React.Fragment>
                  )) : <TableRow>
                    <TableCell colSpan="6">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="6">
                      <MobileLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
          </CardContent>
          {element.length >= 10 && (value.search == null || value.search == undefined || value.search == "") && allCount !== null && <Grid container spacing={2}>
            {allCount != element.length && <Grid item xs={12} sm={12} md={12} textAlign="center">
              <LoadingButton loading={loading}
                loadingIndicator="Loading..."
                variant="outlined" disabled={loading} onClick={fetchMoreData} >View More</LoadingButton>
            </Grid>}
          </Grid>}
        </Card>
      </Grid>
      <DialogBox open={open} handleClose={handleDialogClose} title={'History Detail'} data={DailogData} />
      <Dialog
        open={data.open}
        onClose={handleClose}
        sx={(theme) => ({
          [theme.breakpoints.up('sm')]: {
            minHeight: '750px !important'
          },
          [theme.breakpoints.up('xs')]: {
            minHeight: '450px !important'
          },
          [theme.breakpoints.up('md')]: {
            minHeight: '800px !important'
          },
        })}
        aria-labelledby="responsive-dialog-title"
      >
        <ReviewForm allvalue={data} setId={handleClose} type={false} />
      </Dialog>
      <Dialog
        open={book.open}
        onClose={handleBookingClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <Grid>
          <Card>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Box flexGrow={1} >
                      <Typography className='title_head loginHead' variant="h4" component="h2" align="center">
                        Booking Details
                      </Typography>
                    </Box>
                    <Box>
                      <IconButton onClick={handleBookingClose}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>

                <Grid item xs={6} sm={6}>
                  <div>{book.data.description}</div>
                </Grid>
                <Grid item xs={6} sm={6} >
                  <div>Waiting Time</div>
                  <div className='customer-font'>
                    {(!book.data.waiting_number || book.data.waiting_number == null || book.data.waiting_number == undefined) ? book.data?.waiting_time : (book.data?.waiting_number < 60) ? book.data?.waiting_number + ' ' + book.data.waiting_key : getDateChange(book.data.waiting_number)}
                  </div>
                </Grid>
                <Grid item xs={6} sm={6} >
                  <div>Service Time</div>
                  <div className='customer-font'>
                    {(!book.data.approximate_number || book.data.approximate_number == null || book.data.approximate_number == undefined) ? book.data?.approximate_time : (book.data?.approximate_number < 60) ? book.data?.approximate_number + ' ' + book.data.approximate_key : getDateChange(book.data.approximate_number)}
                  </div>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <div>{book.data?.user_id?.name}</div>
                  <div>{book.data.user_email}</div>
                  <div>{ChangeNumber(book.data.mobile)}</div>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Dialog>
    </Grid>
  )
}

export default HistoryList
