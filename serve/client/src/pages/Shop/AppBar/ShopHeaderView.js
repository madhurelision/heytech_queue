import React from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { useLocation, useNavigate } from 'react-router-dom'
import { LOGOUT } from 'redux/action/types';
import { Badge, IconButton } from '@mui/material'
import { Mail, Menu } from '@mui/icons-material';
import './header.css';
import { removeItemStore } from 'config/localClient'

const ShopHeaderView = () => {

  const history = useNavigate()
  const location = useLocation()
  const dispatch = useDispatch()
  const countData = useSelector((state) => state.auth)
  const handleLogout = () => {
    dispatch({ type: LOGOUT })
    history('/')
    removeItemStore()
  }

  console.log('location Header', location)
  return (
    <div class="phone">
      <div class="menu">
        <div class="options">
          <div class={`option ${location.pathname == '/shop/appoinment' ? 'selected' : ''}`} onClick={() => {
            history('/shop/appoinment');
            document.querySelector('.phone').classList.toggle('active')
          }}>Appointment
            <Badge sx={{ marginLeft: '5px', }} color="secondary" badgeContent={countData?.shopBookingCount}>
              <Mail />
            </Badge>
          </div>
          <div class={`option ${location.pathname == '/shop' ? 'selected' : ''}`} onClick={() => {
            history('/shop');
            document.querySelector('.phone').classList.toggle('active')
          }} >Dashboard
            <Badge sx={{ marginLeft: '5px' }} color="secondary" badgeContent={countData?.shopQueueCount}>
              <Mail />
            </Badge>
          </div>
          <div class={`option ${location.pathname == '/shop/profile' ? 'selected' : ''}`} onClick={() => {
            history('/shop/profile');
            document.querySelector('.phone').classList.toggle('active')
          }}>Profile</div>
          <div class={`option ${location.pathname == '/shop/service' ? 'selected' : ''}`} onClick={() => {
            history('/shop/service');
            document.querySelector('.phone').classList.toggle('active')
          }}>Service</div>
          <div class={`option ${location.pathname == '/shop/gallery' ? 'selected' : ''}`} onClick={() => {
            history('/shop/gallery');
            document.querySelector('.phone').classList.toggle('active')
          }}>Gallery</div>
          <div class={`option ${location.pathname == '/shop/history' ? 'selected' : ''}`} onClick={() => {
            history('/shop/history');
            document.querySelector('.phone').classList.toggle('active')
          }}>History</div>
          <div class="option" onClick={() => { handleLogout() }}>Logout</div>
        </div>
      </div>
      {/* <svg class="x" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 220 400" height="400" width="220">
        <g class="top-bars" stroke-width="4">
          <path class="bar bar1" d="M 178,20 H 202" />
          <path class="bar bar2" d="M 178,29 H 202" />
        </g>
      </svg> */}
      <IconButton onClick={() => {
        document.querySelector('.phone').classList.toggle('active')
      }}>
        <Menu />
      </IconButton>
      {/* <div class="menu-click-area" onClick={() => {
        document.querySelector('.phone').classList.toggle('active')
      }}></div> */}
    </div>
  );
};

export default ShopHeaderView;
