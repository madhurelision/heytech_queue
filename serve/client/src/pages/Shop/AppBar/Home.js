import React from 'react'
import { Grid } from '@mui/material'
import MaterialAppBar from '@mui/material/AppBar';
import { styled } from '@mui/material/styles';
import ShopHeader from './ShopHeader';
import { Outlet } from 'react-router-dom'
import ScrollTop from 'components/ScrollToTop';
import ShopTabs from './ShopTabs';

const PageOneWrapper = styled('div')({
  backgroundColor: 'transparent',
  height: '100%',
  width: '100%',
  overflow: 'hidden',
  position: 'relative',
});


const Home = ({ children }) => {
  return (
    <PageOneWrapper>
      <Grid container direction="column" wrap="nowrap" sx={{ height: '100%' }}>
        {/* <Grid item >
          <MaterialAppBar
            variant="outlined"
            position="relative"
            elevation={0}
            sx={{ padding: '8px', backgroundColor: '#242424' }}>
            <ShopHeader />
          </MaterialAppBar>
          <div id="back-to-top-anchor" />
          <ScrollTop />
        </Grid> */}
        {children}
        <Outlet />
        {/* <Grid item
          sx={{
            display: { xs: "block", md: "none", sm: "block" },
            position: 'fixed',
            bottom: 0,
            padding: '10px 10px 0px 10px',
            width: '100%',
          }}>
          <ShopTabs />
        </Grid> */}
      </Grid>
    </PageOneWrapper>
  )
}

export default Home
