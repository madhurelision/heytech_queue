import React, { useEffect, useContext, useState } from 'react';
import MaterialAppBar from '@mui/material/AppBar';
import { Badge, IconButton } from '@mui/material'
import { Close, Mail, Menu, Notifications, PowerSettingsNew } from '@mui/icons-material';
import ScrollTop from 'components/ScrollToTop';
import MaterialToolbar from '@mui/material/Toolbar';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import { ImageButton, Link, StyledButton as Button } from '../../../components/common/index';
import { useNavigate, useLocation } from 'react-router';
import { LOGOUT, SHOP_COUNT, SOCKET_SHOP_BOOKING_REMOVE, SOCKET_SHOP_BOOKING_UPDATE, SOCKET_SHOP_NOTIFICATION, SOCKET_SHOP_QUEUE_REMOVE, SOCKET_SHOP_QUEUE_UPDATE } from 'redux/action/types';
import { useDispatch, useSelector } from 'react-redux';
import SocketContext, { socketAuth, socket } from 'config/socket';
import ShopNotificationView from '../Notification/ShopNotificationView';
import { ServiceSnackbar } from '../UsableComponent/SnackBar';
import { getAllAdminPage, shopLogoutBtn } from 'redux/action';
import { removeItemStore } from 'config/localClient'
import ToggleButton from '../UsableComponent/ToggleButton';

const ShopHeader = () => {
  const history = useNavigate()
  const dispatch = useDispatch()
  const countData = useSelector((state) => state.auth)
  const profileData = useSelector((state) => state.auth.profile);
  const logout = useSelector((state) => state.auth.logout);
  const loading = useSelector((state) => state.auth.loader);
  const [open, setOpen] = useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [menuFlag, setMenuFlag] = useState(false);
  const location = useLocation()
  const handleMenuFlag = () => {
    setMenuFlag(!menuFlag);
  };

  const context = useContext(SocketContext)

  const handleLogout = () => {
    dispatch({ type: LOGOUT })
    context.socket.emit('logout')
    history('/')
    removeItemStore()
  }

  useEffect(() => {
    if (logout == true) {
      handleLogout()
    }
  }, [logout])

  const handleBtnLog = () => {
    dispatch(shopLogoutBtn())
  }

  const handleNOff = () => {
    setOpen(false)
  }

  const [data, setData] = useState({})
  const adminD = useSelector((state) => state.auth.adminPageList)

  useEffect(() => {

    if (Object.keys(adminD).length == 0) {
      dispatch(getAllAdminPage())
    } else {
      setData(adminD.admin)
    }

  }, [adminD])

  useEffect(() => {

    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
    })

    context.socket.on('userDisconnect', (data) => {
      console.log('Socket Err', data)
    })

    context.socket.on('shopCount', (data) => {
      //console.log('Shop Count Data', data)
      dispatch({ type: SHOP_COUNT, payload: data })
    })

    context.socket.on('shopBooking', (data) => {
      // console.log('Shop Booking Data', data)
      dispatch({ type: SOCKET_SHOP_BOOKING_UPDATE, payload: [data] })
      if (data.queue == true) {
        dispatch({ type: SOCKET_SHOP_QUEUE_UPDATE, payload: [data] })
      }
    })

    context.socket.on('shopBookingRemove', (data) => {
      //console.log('Shop Booking Remove', data)
      dispatch({ type: SOCKET_SHOP_BOOKING_REMOVE, payload: data })
      if (data.queue == true) {
        dispatch({ type: SOCKET_SHOP_QUEUE_REMOVE, payload: data })
      }
    })

    context.socket.on('notificationShopCount', (data) => {
      //console.log('notificationCount', data)
      dispatch({ type: SOCKET_SHOP_NOTIFICATION, payload: data })
    })

    context.socket.on('disconnect', () => {
      console.log('socket is disconnect')
    })

    return () => {
      context.socket.removeListener(['shopCount', 'shopBookingRemove', 'shopBooking'])
    }

  }, [])

  return (
    <div id="mainHeader">
      <MaterialToolbar>
        <Stack className="mobileContainer" direction="row" alignItems="center" justifyContent="space-between" spacing={3} style={{ width: '100%' }}>
          <Box className="logoMobile" sx={{ p: 2, border: 'none' }}>
            <Link to="/" className="mainLogo">
              <svg enableBackground="new 0 0 512 512" id="Layer_1" version="1.1" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><g><path d="   M373.9,24l-1.2-8L162.9,391.7c-6.3,11.4-20.6,15.5-32.1,9.5c-0.2-0.1-0.3-0.2-0.5-0.3c-22.2-11.4-49.8-3.3-62.4,18.2   c-13.5,23-5.4,52.5,17.7,65.4c22.9,12.8,51.7,4.6,64.5-18.3l55.8-100c3.2-5.7,10.3-7.7,16-4.5h0c6.5,3.6,14.6,1.3,18.2-5.2   l108.4-194.2C372.1,120.3,381,71.6,373.9,24z M127.1,453.3c-5.7,10.1-18.5,13.8-28.6,8.1c-10.1-5.7-13.8-18.5-8.1-28.6   c5.7-10.1,18.5-13.8,28.6-8.1C129.2,430.4,132.8,443.2,127.1,453.3z M259.4,284.2c-6.6,0-11.9-5.3-11.9-11.9s5.3-11.9,11.9-11.9   c6.6,0,11.9,5.3,11.9,11.9S265.9,284.2,259.4,284.2z" fill="none" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" strokeWidth="10" /><path d="   M444.6,425.4c-12.2-21.8-39.6-30.4-62-19.5c-0.2,0.1-0.3,0.2-0.5,0.3c-11.6,5.8-25.8,1.3-31.9-10.1l-53.8-102.8l-29.2,52.3   l6.4,13.6c3.5,6.5,11.6,9,18.1,5.6h0c5.7-3,12.9-0.9,15.9,4.9l53.7,101.2c12.3,23.1,41,31.9,64.1,19.6   C448.9,478,457.6,448.7,444.6,425.4z M413.1,467.2c-10.3,5.4-23,1.5-28.4-8.7c-5.4-10.3-1.5-23,8.7-28.4c10.3-5.4,23-1.5,28.4,8.7   C427.2,449,423.3,461.7,413.1,467.2z" fill="none" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" strokeWidth="10" /><path d="   M216.8,255.2l29.2-52.4L148.5,16l-1.4,8c-8.1,47.5-0.3,96.3,22.3,138.8L216.8,255.2z" fill="none" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" strokeWidth="10" /></g></svg>
              <ImageButton className={(location.pathname == "/") ? 'activeLink' : ''} >{(data.name !== null || data.name !== undefined) ? data.name : "HeyBarber"}</ImageButton>
            </Link>
          </Box>
          <Stack
            className="headerMenu"
            direction="row" alignItems="center" justifyContent="end"
            // spacing={1}
            sx={{ display: { xs: "none", md: "block", sm: "none" } }}
          >
            {/* <Link to="/shop/appoinment">
              <ImageButton >Appointment
                <Badge sx={{ marginLeft: '5px', }} color="secondary" badgeContent={countData?.shopBookingCount}>
                  <Mail />
                </Badge>
              </ImageButton>
            </Link> */}
            <Link to={`/user/${profileData._id}`}>
              <ImageButton className={(location.pathname == `/user/${profileData._id}`) ? 'activeLink' : ''} >Shop
              </ImageButton>
            </Link>
            <Link to="/shop">
              <ImageButton className={(location.pathname == "/shop") ? 'activeLink' : ''}  >Dashboard
                {/* <Badge sx={{ marginLeft: '5px' }} color="secondary" badgeContent={countData?.shopQueueCount}>
                  <Mail />
                </Badge> */}
              </ImageButton>
            </Link>
            <Link to="/shop/profile">
              <ImageButton className={(location.pathname == "/shop/profile") ? 'activeLink' : ''}>Profile</ImageButton>
            </Link>
            <Link to="/shop/service">
              <ImageButton className={(location.pathname == "/shop/service") ? 'activeLink' : ''} >Service</ImageButton>
            </Link>
            <Link to="/shop/gallery">
              <ImageButton className={(location.pathname == "/shop/gallery") ? 'activeLink' : ''} >Gallery</ImageButton>
            </Link>
            <Link to="/shop/employee">
              <ImageButton className={(location.pathname == "/shop/employee") ? 'activeLink' : ''}>Employee</ImageButton>
            </Link>
            {/* <Link to="/shop/insight">
              <ImageButton className={(location.pathname == "/shop/insight") ? 'activeLink' : ''}>Insight</ImageButton>
            </Link> */}
            <Link to="/shop/inventory">
              <ImageButton className={(location.pathname == "/shop/inventory") ? 'activeLink' : ''} >Inventory</ImageButton>
            </Link>
            <Link to="/shop/package">
              <ImageButton className={(location.pathname == "/shop/package") ? 'activeLink' : ''} >Package</ImageButton>
            </Link>
            <Link to="/shop/history">
              <ImageButton className={(location.pathname == "/shop/history") ? 'activeLink' : ''}>History</ImageButton>
            </Link>
            <Link to="/about">
              <ImageButton className={(location.pathname == "/about") ? 'activeLink' : ''} >About</ImageButton>
            </Link>
            <Link to="/contact">
              <ImageButton className={(location.pathname == "/contact") ? 'activeLink' : ''} >Contact Us</ImageButton>
            </Link>
            <ImageButton onClick={(event) => {
              setAnchorEl(event.currentTarget);
              setOpen(!open)
              setMenuFlag(!menuFlag)
            }}  ><Badge sx={{ marginLeft: '5px' }} color="secondary" badgeContent={countData?.shopNotificationCount}>
                <Notifications />
              </Badge>
            </ImageButton>
            <ImageButton disabled={loading} onClick={() => { handleBtnLog() }}>Logout</ImageButton>
            <ToggleButton />
          </Stack>
          <Stack
            className="mobileBtns"
            direction="row"
            //spacing={3}
            sx={{ display: { xs: "block", md: "none", sm: "block" } }}
          >
            <IconButton title="Notification" onClick={(event) => {
              setAnchorEl(event.currentTarget);
              setOpen(!open)
            }}>
              <Badge color="secondary" badgeContent={countData?.shopNotificationCount}>
                <Notifications />
              </Badge>
            </IconButton>
            <IconButton disabled={loading} title="Logout" onClick={handleBtnLog}>
              <PowerSettingsNew />
            </IconButton>
            <ToggleButton />
            {/* <IconButton onClick={handleMenuFlag}>
              {menuFlag == true ? <Close /> : <Menu />}
            </IconButton>
            {menuFlag == true ? (
              <div style={{
                position: 'absolute',
                top: 48,
                left: 0,
                right: 0,
                bottom: 0,
                zIndex: 100,
                backgroundColor: 'black',
                height: 'fit-content'
              }} >
                <Button style={{ width: "100%" }} onClick={() => {
                  history('/shop/appoinment');
                  setMenuFlag(!menuFlag)
                }}>Appointment
                  <Badge sx={{ marginLeft: '5px', }} color="secondary" badgeContent={countData?.shopBookingCount}>
                    <Mail />
                  </Badge>
                </Button>
                <Button style={{ width: "100%" }} onClick={() => {
                  history('/shop');
                  setMenuFlag(!menuFlag)
                }} >Dashboard
                  <Badge sx={{ marginLeft: '5px' }} color="secondary" badgeContent={countData?.shopQueueCount}>
                    <Mail />
                  </Badge>
                </Button>
                <Button style={{ width: "100%" }} onClick={() => {
                  history('/shop/profile');
                  setMenuFlag(!menuFlag)
                }}>Profile</Button>
                <Button style={{ width: "100%" }} onClick={() => {
                  history('/shop/service');
                  setMenuFlag(!menuFlag)
                }}>Service</Button>
                <Button style={{ width: "100%" }} onClick={() => {
                  history('/shop/gallery');
                  setMenuFlag(!menuFlag)
                }}>Gallery</Button>
                <Button style={{ width: "100%" }} onClick={() => {
                  history('/shop/employee');
                  setMenuFlag(!menuFlag)
                }}>Employee</Button>
                <Button style={{ width: "100%" }} onClick={() => {
                  history('/shop/history');
                  setMenuFlag(!menuFlag)
                }}>History</Button>
                <Button style={{ width: "100%" }} onClick={(event) => {
                  setAnchorEl(event.currentTarget);
                  setOpen(!open)
                  setMenuFlag(!menuFlag)
                }} >Notification <Badge sx={{ marginLeft: '5px' }} color="secondary" badgeContent={countData?.shopNotificationCount}>
                    <Notifications />
                  </Badge>
                </Button>
                <Button style={{ width: "100%" }} onClick={() => { handleLogout() }}>Logout</Button>
              </div>
            ) : (
              <span></span>
            )} */}
          </Stack>
        </Stack>
        <ShopNotificationView open={open} anchorEl={anchorEl} handleClose={handleNOff} />
        {/* {Object.keys(profileData).length > 0 && profileData.topics.length == 0 && <ServiceSnackbar open={true} message={"Service is Required Please Select in Profile or Create Service"} />} */}
      </MaterialToolbar>
    </div>
  )
}

export default ShopHeader;
