import React from 'react';
import { Grid, Typography, useTheme } from '@mui/material';
import { AssignmentInd, BarChart, CleaningServices, ContactMail, History, Info, Mail, ManageAccounts } from '@mui/icons-material';
import { useLocation, useNavigate } from 'react-router-dom';
import { tabStyles } from '../Dashboard/styles';

const ShopTabs = () => {

  const theme = useTheme()
  const history = useNavigate();
  const classes = tabStyles(theme)
  const location = useLocation()

  return (
    <Grid id="fixedToolbar" className="shop-mobile-tab" container sx={{ color: theme.palette.mode == 'dark' ? 'white' : 'black', justifyContent: 'center', backdropFilter: 'blur(10px)' }}>
      {/* <Grid item xs={2} sm={2} md={2} textAlign="center" className={(location.pathname !== "/shop") ? classes.grid : classes.selectGrid} onClick={(e) => {
        history('/shop')
      }}>
        <Mail fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Dashboard</Typography>
      </Grid>
      <Grid item xs={2} sm={2} md={2} textAlign="center" className={(location.pathname !== "/shop/profile") ? classes.grid : classes.selectGrid} onClick={() => {
        history('/shop/profile')
      }}>
        <ManageAccounts fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Profile</Typography>
      </Grid>
      <Grid item xs={2} sm={2} md={2} textAlign="center" className={(location.pathname !== "/shop/service") ? classes.grid : classes.selectGrid} onClick={() => {
        history('/shop/service')
      }}>
        <CleaningServices fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Service</Typography>
      </Grid>
      <Grid item xs={2} sm={2} md={2} textAlign="center" className={(location.pathname !== "/shop/employee") ? classes.grid : classes.selectGrid} onClick={() => {
        history('/shop/employee')
      }}>
        <AssignmentInd fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Employee</Typography>
      </Grid>
      <Grid item xs={2} sm={2} md={2} textAlign="center" className={(location.pathname !== "/shop/history") ? classes.grid : classes.selectGrid} onClick={() => {
        history('/shop/history')
      }}>
        <History fontSize="large" />
        <Typography className={classes.font + ' title_head'} >History</Typography>
      </Grid>
      <Grid item xs={1} sm={1} md={1} textAlign="center" className={(location.pathname == "/about") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/about')
      }}>
        <Info fontSize="large" />
        <Typography className={classes.font + ' title_head'} >About</Typography>
      </Grid>
      <Grid item xs={1} sm={1} md={1} textAlign="center" className={(location.pathname == "/contact") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/contact')
      }}>
        <ContactMail fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Contact Us</Typography>
      </Grid> */}

      <div className={(location.pathname !== "/shop") ? classes.grid : classes.selectGrid} onClick={(e) => {
        history('/shop')
      }}>
        <Mail fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Dashboard</Typography>
      </div>
      <div className={(location.pathname !== "/shop/profile") ? classes.grid : classes.selectGrid} onClick={() => {
        history('/shop/profile')
      }}>
        <ManageAccounts fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Profile</Typography>
      </div>
      <div className={(location.pathname !== "/shop/service") ? classes.grid : classes.selectGrid} onClick={() => {
        history('/shop/service')
      }}>
        <CleaningServices fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Service</Typography>
      </div>
      <div className={(location.pathname !== "/shop/employee") ? classes.grid : classes.selectGrid} onClick={() => {
        history('/shop/employee')
      }}>
        <AssignmentInd fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Employee</Typography>
      </div>
      <div className={(location.pathname !== "/shop/inventory") ? classes.grid : classes.selectGrid} onClick={() => {
        history('/shop/inventory')
      }}>
        <AssignmentInd fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Inventory</Typography>
      </div>
      <div className={(location.pathname !== "/shop/package") ? classes.grid : classes.selectGrid} onClick={() => {
        history('/shop/package')
      }}>
        <AssignmentInd fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Package</Typography>
      </div>
      {/* <div className={(location.pathname !== "/shop/insight") ? classes.grid : classes.selectGrid} onClick={() => {
        history('/shop/insight')
      }}>
        <BarChart fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Insight</Typography>
      </div> */}
      <div className={(location.pathname !== "/shop/history") ? classes.grid : classes.selectGrid} onClick={() => {
        history('/shop/history')
      }}>
        <History fontSize="large" />
        <Typography className={classes.font + ' title_head'} >History</Typography>
      </div>
      <div className={(location.pathname == "/about") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/about')
      }}>
        <Info fontSize="large" />
        <Typography className={classes.font + ' title_head'} >About</Typography>
      </div>
      <div className={(location.pathname == "/contact") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/contact')
      }}>
        <ContactMail fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Contact Us</Typography>
      </div>

    </Grid>
  )
}

export default ShopTabs