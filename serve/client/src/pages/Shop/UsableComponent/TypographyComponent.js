import React from 'react';
import { Typography } from '@mui/material'


const TypographyComponent = (props) => {
  return (
    <Typography {...props} sx={(theme) => ({
      [theme.breakpoints.up('sm')]: {
        fontSize: '1.5rem !important'
      },
      [theme.breakpoints.up('xs')]: {
        fontSize: '1.5rem !important'
      },
      [theme.breakpoints.up('md')]: {
        fontSize: '2.125rem !important'
      },
    })}>
      {props.children}
    </Typography>
  )
}

export default TypographyComponent