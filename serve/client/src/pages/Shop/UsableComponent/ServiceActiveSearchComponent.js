import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { TextField, Autocomplete, styled, InputAdornment } from "@mui/material";
import { getAllActiveServiceRegexMatch, getAllServiceRegexMatch, getAllShopTextMatch } from "redux/action";
import { NewSearchBarber as SearchBarber } from "components/common";
import { useParams } from "react-router-dom";
import { Search as SearchBtn } from '@mui/icons-material'

var welc = false

const ServiceActiveSearchComponent = ({ handleView }) => {

  const [element, setElement] = useState([])
  const [category, setCategory] = useState('')
  const [open, setOpen] = useState(false)
  const serviceData = useSelector((state) => state.auth.activeServiceList)
  const profileD = useSelector((state) => state.auth.profile)
  const dispatch = useDispatch()
  const { id } = useParams()

  useEffect(() => {

    if (serviceData.length > 0) {
      setElement(serviceData)
    }

  }, [serviceData])


  const [flag, setFlag] = useState(false)

  const handleButton = (val) => {
    if (val.length > 2) {
      setFlag(true)
      welc = true
    }
    if (val == "" || val == null || val.length == 0) {
      setFlag(false)
      welc = false
    }
    if (welc) {
      setCategory(val)
      dispatch(getAllActiveServiceRegexMatch(val))
    } else {
      setCategory(val)
      setOpen(false)
    }
  }

  return (
    <>
      <SearchBarber
        id="asynchronous-demo"
        open={open}
        onOpen={
          () => {
            if (element.length == 0) {

            } else {
              setOpen(true);
            }
          }
        }
        onClose={
          () => {
            setOpen(false);
            handleButton('');
          }
        }
        isOptionEqualToValue={(option, value) => option.name === value.name}
        getOptionLabel={(option) => option.name + ' ' + option.motivation}
        options={(id == null || id == undefined || id.length == 0) ? element.filter((cc) => cc.type == '6242d0c9f0addae2c2328a59') : element.filter((cc) => cc.type == id)}
        loading={element.length == 0 ? true : false}
        fullWidth
        onChange={handleView}
        renderInput={(params) => (
          <TextField
            {...params}
            //label="Search By Name & Motivation"
            placeholder="Search By Name & Motivation"
            variant="outlined"
            value={category}
            onChange={(e) => {
              setOpen(true)
              console.log('value', e.target.value)
              handleButton(e.target.value);
            }}
            InputProps={{
              ...params.InputProps,
              startAdornment: (
                <>
                  <InputAdornment position="start"><SearchBtn sx={{ color: 'black !important', fontSize: '30px' }} /></InputAdornment>
                  {params.InputProps.startAdornment}
                </>
              ),
              endAdornment: (
                <React.Fragment>
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
            fullWidth
          />
        )}
      />
    </>
  )
}

export default ServiceActiveSearchComponent
