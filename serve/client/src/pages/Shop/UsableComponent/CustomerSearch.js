import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { Autocomplete, TextField } from "@mui/material";
import { getAllCustomer, getAllCustomerRegexMatch } from "redux/action";

var welc = false

const CustomerSearch = ({ val, handleView }) => {

  const [element, setElement] = useState([])
  const [category, setCategory] = useState('')
  const [open, setOpen] = useState(false)
  const customerData = useSelector((state) => state.auth.customerList)
  const dispatch = useDispatch()

  useEffect(() => {

    if (customerData.length == 0) {
      dispatch(getAllCustomer(0, 10))
    } else {
      setElement(customerData.customer)
    }

  }, [customerData])


  const [flag, setFlag] = useState(false)

  const handleButton = (val) => {
    if (val.length > 2) {
      setFlag(true)
      welc = true
    }
    if (val == "" || val == null || val.length == 0) {
      setFlag(false)
      welc = false
    }
    if (welc) {
      setCategory(val)
      dispatch(getAllCustomerRegexMatch(val))
    } else {
      setCategory(val)
      setOpen(false)
    }
  }

  return (
    <>
      <Autocomplete
        id="asynchronous-demo"
        open={open}
        onOpen={
          () => {
            if (element.length == 0) {

            } else {
              setOpen(true);
            }
          }
        }
        onClose={
          () => {
            setOpen(false);
            handleButton('');
          }
        }
        isOptionEqualToValue={(option, value) => option.name === value.name || option.mobile === value.mobile}
        getOptionLabel={(option) => option.name}
        options={element}
        value={val}
        loading={element.length == 0 ? true : false}
        fullWidth
        onChange={handleView}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Search By Name & Mobile"
            variant="outlined"
            InputLabelProps={{ shrink: true }}
            value={category}
            onChange={(e) => {
              setOpen(true)
              console.log('value', e.target.value)
              handleButton(e.target.value);
            }}
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
            fullWidth
          />
        )}
      />
    </>
  )
}

export default CustomerSearch
