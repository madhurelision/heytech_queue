import React from 'react'
import { IconButton } from '@mui/material'
import Email from '@mui/icons-material/Email'

const EmailBox = ({ title }) => {
  return (
    <IconButton title={title}>
      <Email />
    </IconButton>
  )
}

export default EmailBox
