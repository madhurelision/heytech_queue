import React from 'react'
import { Grid, styled } from '@mui/material';

const TopGrid = styled(Grid)({
  justifyContent: 'center',
  alignItems: 'center',
  height: '120px',
  width: '210px',
  borderRadius: '16px',
  backgroundColor: '#388e3c',
  padding: '1rem 1rem',
  color: 'black',
  zIndex: 1,
  backgroundPosition: 'center',
  backgroundSize: 'cover'
});

const ImageBox = ({ image }) => {
  return (
    <TopGrid container item
      // sx={{ backgroundColor: background }}
      sx={{ backgroundImage: `url(${image})` }}
    />
  )
}

export default ImageBox
