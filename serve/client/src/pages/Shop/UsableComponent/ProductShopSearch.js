import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { TextField } from "@mui/material";
import { SearchBarber } from "components/common";
import { getAllProductRegexMatch } from "redux/action/inventory";

var welc = false

const ProductShopSearch = ({ handleView }) => {

  const [element, setElement] = useState([])
  const [category, setCategory] = useState('')
  const [open, setOpen] = useState(false)
  const productData = useSelector((state) => state.auth.inventoryProductList)
  const dispatch = useDispatch()

  useEffect(() => {

    if (productData.length == 0) {
    } else {
      setElement(productData.product)
    }

  }, [productData])


  const [flag, setFlag] = useState(false)

  const handleButton = (val) => {
    if (val.length > 2) {
      setFlag(true)
      welc = true
    }
    if (val == "" || val == null || val.length == 0) {
      setFlag(false)
      welc = false
    }
    if (welc) {
      setCategory(val)
      dispatch(getAllProductRegexMatch(val))
    } else {
      setCategory(val)
      setOpen(false)
    }
  }

  return (
    <>
      <SearchBarber
        id="asynchronous-demo"
        open={open}
        onOpen={
          () => {
            if (element.length == 0) {

            } else {
              setOpen(true);
            }
          }
        }
        onClose={
          () => {
            setOpen(false);
            handleButton('');
          }
        }
        isOptionEqualToValue={(option, value) => option.name === value.name}
        getOptionLabel={(option) => option.name}
        options={element}
        loading={element.length == 0 ? true : false}
        fullWidth
        onChange={handleView}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Search By Name"
            variant="outlined"
            value={category}
            onChange={(e) => {
              setOpen(true)
              console.log('value', e.target.value)
              handleButton(e.target.value);
            }}
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
            fullWidth
          />
        )}
      />
    </>
  )
}

export default ProductShopSearch
