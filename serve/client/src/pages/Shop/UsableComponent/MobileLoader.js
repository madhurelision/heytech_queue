import React from 'react';
import { styled, Box, Skeleton } from '@mui/material';


const MobileLoader = () => {
  return (
    <Box>
      <Skeleton />
      <Skeleton animation={false} />
      <Skeleton animation="wave" />
    </Box>
  )
}

export default MobileLoader