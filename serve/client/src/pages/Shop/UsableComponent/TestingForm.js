import React, { useContext, useEffect, useState } from 'react';
import axios from 'axios';
import { SERVER_URL } from '../../../config.keys';
import SocketContext from 'config/socket';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useRecoilState } from 'recoil';
import { mentorState } from 'store';
import SliderForm from './SliderForm';
import { ACTIVE_EMPLOYEE_LIST } from 'redux/action/types';
import Loader from '../../../components/Loader/index';


const getMentor = async (id) => {
  const { data: response } = await axios.get(
    `${SERVER_URL}/api/get-mentor`,
    {
      params: {
        id,
      },
    },
  );
  return response;
};

const TestingForm = () => {

  const [mentorData, setMentorData] = useRecoilState(mentorState);

  const [shopData, setShopData] = useState({
    first_name: '',
    last_name: '',
    shop_name: '',
    image_link: '',
    description: '',
    instagram: '',
    location: '',
    job_title: '',
    company: '',
    expertise: [],
    language: [],
    topics: [],
    gallery: [],
    like: [],
    review: [],
    code: '',
    longitude: '',
    lattitude: '',
    seat: 0,
    banner_link: '',
    open: '',
    queue: '',
    appointment: '',
    pack: [],
    user_information: ''
  })
  const [checkData, setCheckData] = useState({})
  const dispatch = useDispatch()
  const context = useContext(SocketContext)
  const { id } = useParams();
  const [socketCon, setSocketCon] = useState(false)
  const [check, setCheck] = useState(true)

  useEffect(() => {

    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    if (context.socket.connected == true) {
      setSocketCon(true)
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      setSocketCon(true)
      if (localStorage.getItem('auth-token') !== null) {
        context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
      }
    })

    context.socket.on('userDisconnect', (data) => {
      console.log('Socket Err', data)
    })


    context.socket.on('shopEmployeeData', data => {
      //console.log('shop Sockebokit Data', data.val)
      dispatch({ type: ACTIVE_EMPLOYEE_LIST, payload: { data: data } })
    })


    context.socket.on('disconnect', () => {
      context.socket.removeAllListeners()
    })

    return () => {
      context.socket.removeAllListeners()
    }
  }, [])


  useEffect(() => {
    if (socketCon == true) {
      context.socket.emit('shopEmployee', 'hello')
      setSocketCon(false)
    }

  }, [socketCon])

  useEffect(() => {

    getMentor(id).then((cc) => {
      setMentorData(cc)
      setShopData(cc)
      setCheckData(cc)
      setCheck(false)
    }).catch((err) => {
      console.log('cc', err)
      setCheck(false)
    })

  }, [])

  if (typeof id === 'undefined') return <div />;

  if (check) return <Loader />;

  if (Object.keys(checkData).length == 0) return <div />;

  return (
    <SliderForm />
  )
}

export default TestingForm