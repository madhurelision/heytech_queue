import React, { useEffect, useState } from "react";
import { ProgressBar } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import { dateMinus } from "config/KeyData";

function NewTimer({ main }) {
  const [secondsLeft, setSecondsLeft] = useState(dateMinus(main, new Date()));
  const [time, setTime] = useState()
  const [timer, setTimer] = useState();
  const [progress, setProgress] = useState(100)

  const start = (mainV) => {

    var start = Date.now()
    var diff = 0
    var duration = mainV
    const timer = setInterval(() => {
      setSecondsLeft((secondsLeft) => secondsLeft - 1);

      var car = duration - diff;
      var v = car / duration * 100;
      var tim = Math.round(v) + 2;
      setProgress(tim)
      diff = duration - (((Date.now() - start) / 1000) | 0);
      var minutes = (diff / 60) | 0;
      var seconds = (diff % 60) | 0;

      minutes = minutes < 10 ? "0" + minutes : minutes;
      seconds = seconds < 10 ? "0" + seconds : seconds;
      setTime(minutes + ":" + seconds)


      if (secondsLeft === 0) {
        clearInterval(timer);
        setProgress(100)
      }

      if (diff <= 0) {
        clearInterval(timer);
      }
    }, 1000);
    setTimer(timer);
  };

  useEffect(() => {
    if (secondsLeft === 0) {
      clearInterval(timer);
    }
  }, [secondsLeft, timer]);

  useEffect(() => {
    return () => clearInterval(timer);
  }, [timer]);

  useEffect(() => {
    start(dateMinus(main, new Date()))
  }, [])

  const getTimeRemaining = (e) => {
    var mainV = new Date(e)
    mainV.setSeconds(mainV.getSeconds())
    //console.log('result first date', e, Date.parse(mainV), Date.parse(new Date()))
    const total = Date.parse(mainV) - Date.parse(new Date());
    const seconds = Math.floor((total / 1000) % 60);
    const minutes = Math.floor((total / 1000 / 60) % 60);
    const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
    const days = Math.floor((total / (1000 * 60 * 60 * 24)))
    if (total > 0) {
      return days + ':' +
        (hours > 9 ? hours : '0' + hours) + ':' +
        (minutes > 9 ? minutes : '0' + minutes) + ':'
        + (seconds > 9 ? seconds : '0' + seconds)
    } else {
      return '00:00:00:00'
    }
  }

  return (
    <div className="App">
      <div style={{ display: 'none' }}>{getTimeRemaining(main)}</div>
      <ProgressBar animated now={progress} />
    </div>
  );
}

export default React.memo(NewTimer)