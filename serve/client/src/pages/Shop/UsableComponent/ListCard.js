import React from 'react'
import { Card, CardContent, CardMedia, Box, Typography, Grid } from '@mui/material'
import ProgressTimer from './ProgressTimer'
import moment from 'moment'
import StatusBox from './StatusBox'

const ListCard = (props) => {
  return (
    <Card className='bookingCUsIMg card-book' sx={{ borderRadius: '18px' }} onClick={() => { props.handleClick(props) }}>
      <CardMedia
        component="img"
        sx={{ width: '100%', objectFit: 'unset', borderRadius: '0' }}
        image={props?.service_id?.image}
        alt={props?.service_id?.name}
      />
      <Box>
        <CardContent className="paddingBottom">
          <Grid container spacing={2}>
            <Grid className="block" item xs={6} sm={6} >
              <span>
                {props.queue == true ? 'Queue' : 'Appointment'}
              </span>
            </Grid>
            <Grid className="labeling" item xs={6} sm={6} >
              <span>
                {'#' + props?.bookingid}
              </span>
            </Grid>
            <Grid className="acceptBlock" item xs={6} sm={6}>
              <Typography className="block" variant='h4' component="div" style={{ fontSize: '1.125rem' }}>
                {props?.service_id?.name}
              </Typography>
              <div className="block">
                {props.size + ' ' + 'users'}
              </div>
              <div className="block">
                {'Seat No' + ': ' + props?.seat}
              </div>
            </Grid>
            <Grid className="acceptBlock" item xs={6} sm={6} sx={{ fontSize: '0.85rem !important' }}>
              <StatusBox className="labeling" value={props.status} />
              <div className="labeling">
                {moment(props.appointment_date).format('LL')}
              </div>
              <div className="labeling">
                {moment(props.appointment_date).calendar()}
              </div>
            </Grid>
          </Grid>
        </CardContent>
      </Box>
    </Card>
  )
}

export default ListCard