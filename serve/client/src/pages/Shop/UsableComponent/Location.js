import React, { useState } from 'react'
import GooglePlacesAutocomplete, { geocodeByPlaceId, getLatLng } from 'react-google-places-autocomplete';
import { Grid, Button, useTheme, IconButton } from '@mui/material';
import { BookingButton } from "components/common";
import { ShareLocation } from '@mui/icons-material';

const Location = (props) => {

  const theme = useTheme()
  const { handleLocation, handleDropdown } = props

  const [wel, setWel] = useState()

  const hanldeMenuLocation = (data) => {
    setWel(data)
    handleDropdown(data)
    console.log('Location Data', data)
    geocodeByPlaceId(data.value.place_id).then((ss) => {
      console.log('Location SS Data', ss)
      getLatLng(ss[0]).then((dd) => {
        console.log('Location seleted', { name: data.label, latitude: dd.lat, longitude: dd.lng })
        setWel(null)
      })
    }).catch((err) => {
      console.log('Location SS Data Errorr', err)
    })
  }

  return (
    <>
      <Grid id="locationInput" className={`${(theme.palette.mode == 'dark') ? 'dark-loc' : 'light-loc'}`} item xs={10} sm={10} md={10}>
        <GooglePlacesAutocomplete
          apiKey="AIzaSyCqiCKV0cm-EpHp2hev4XkD5wG9JGxFbVs"
          selectProps={{
            onChange: (d) => { hanldeMenuLocation(d) },
            placeholder: "Type in an address",
            isSearchable: true,
            value: wel,
            styles: {
              menu: (provided, state) => ({
                ...provided,
                borderBottom: '1px dotted pink',
                padding: 20,
                zIndex: '16',
                backgroundColor: theme.palette.mode == 'dark' ? '#121212 !important' : '#fff !important',
                color: theme.palette.mode == 'dark' ? '#6c757d !important' : '#121212 !important',
              })
            }
          }}
        />
      </Grid>
      <Grid item xs={2} sm={2} md={2}>
        {/* <BookingButton type="button" variant="contained" onClick={handleLocation} >Get Current</BookingButton> */}
        <IconButton title="Get Location" onClick={handleLocation}>
          <ShareLocation className='locationBtn-font' />
        </IconButton>
      </Grid>
    </>
  )
}

export default Location
