import React, { useEffect, useState } from 'react';
import {
  Chip,
  Typography,
} from '@mui/material';
import { keyLengthValidation } from 'config/KeyData';
import './sliderForm.css'
import { useRecoilValue } from 'recoil';
import { mentorState } from 'store';
import { getAllTagService } from 'redux/action';
import { useDispatch, useSelector } from 'react-redux';
import { uniqueServiceElements } from 'config/uniqueElements';

const SliderForm = () => {
  const { first_name, last_name, email, _id, topics, user_information, waiting_time, shop_name } = useRecoilValue(mentorState);

  const [step, setStep] = useState(0)
  const [view, setView] = useState(0)
  const [val, setVal] = useState(0)
  const [otp, setOtp] = useState('')
  const dispatch = useDispatch()

  const [element, setElement] = useState({
    user_id: '',
    user_email: '',
    service_id: '',
    appointment_date: '',
    appointment_time: '',
    shop_email: '',
    shop_name: '',
    shop_id: '',
    description: '',
    size: '',
    mobile: '',
    time: '',
    location: '',
    assign: '',
    s_type: ''
  })
  const [err, setErr] = useState({})

  useEffect(() => {
    if (view == 1) {

      focusMain('otp')
    }
  }, [view])

  const handleChange = (e) => {
    setElement({ ...element, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const focusMain = (val) => {
    const section = document.getElementById(`${val}`);
    section.focus()
  };

  const handleSubmit = () => {
    var check = validateForm(element)
    setErr(check)
    if (Object.keys(check).length > 0) {
      const hey = Object.keys(check)
      focusMain(hey[0])
      return
    }
    setView(1)
    console.log('formData', element)

  }

  const handleStep = (n) => {
    setStep(n)
    if (n <= 6) {
      var x = document.getElementsByClassName("input-box");
      focusMain(x[n].id)
    } else {
      setStep(0)
      focusMain('input-button')
    }
  }

  const handleKeyDown = (e, m) => {
    if (e.key === 'Enter') {
      // 👇️ your logic here
      handleStep(m + 1)
    }
  };

  const handleOtp = () => {
    if (otp == null || otp == undefined || otp == "") {
      setErr({ ...err, otp: 'Otp is Required' })
      focusMain('otp')
      setVal(0)
      return
    }
    setView(2)
    setVal(0)
  }

  const handleOtpChange = (e) => {
    setOtp(e.target.value);
    setErr({ ...err, otp: '' });
  }

  const handleClose = () => {
    setView(0)
    setOtp('')
    setElement({
      ...element,
      user_id: '',
      user_email: '',
      service_id: '',
      appointment_date: '',
      appointment_time: '',
      shop_email: '',
      shop_name: '',
      shop_id: '',
      description: '',
      size: '',
      mobile: '',
      time: '',
      location: '',
      assign: ''
    })
  }

  const main = (e) => {
    if (e.key === 'Enter') {
      focusMain('otp-button')
      setVal(1)
    }
  }


  const tagS = useSelector((state) => state.auth.tagServiceList)
  const employeeD = useSelector((state) => state.auth.activeEmployeeList)
  const [tag, setTag] = useState([])
  const [employee, setEmployee] = useState([])
  const [tagK, setTagK] = useState('')

  useEffect(() => {

    if (tagS.length == 0) {
      dispatch(getAllTagService())
    } else {
      setTag(tagS.tag)
    }

  }, [tagS])

  useEffect(() => {

    if (employeeD.length == 0) {
    } else {
      setEmployee(employeeD.employee)
    }

  }, [employeeD])

  ////main Fn  
  const filterTopics = (motivation) => {
    return uniqueServiceElements(topics).filter((main) => main.motivation === motivation);
  };

  const filterTagTopics = (motivation) => {
    return tag.filter((main) => main._id === motivation);
  };

  const filterEmployee = (emp) => {
    return employee.filter((main) => main.user_id === emp);
  };


  const handleOption = (e) => {
    var ssMai = filterTopics(e.target.value)
    setElement({ ...element, service_id: ssMai[0]?._id, s_type: e.target.value })
    setTagK(ssMai[0]?.tags)
  }

  const handleViewOption = (e) => {
    setElement({ ...element, service_id: e.target.value })
    var ssMai = filterTopics(e.target.value)
    setTagK(ssMai[0]?.tags)
    setErr({ ...err, service_id: '' })
  }

  const handleTagBtn = (item) => {
    setElement({ ...element, description: element.description.length == 0 ? element.description.concat(item) : element.description.concat(' ', item) })
    setErr({ ...err, description: '' })
  }

  return (
    <form className='main-f' autoComplete="off">
      {view == 0 && <>
        <input className={`input-box ${step == 0 ? 'test-box' : ''}`}
          value={element.user_email}
          onChange={handleChange} onKeyDown={(e) => { handleKeyDown(e, 0) }} id="user_email" type="email" name="user_email" placeholder="Enter User Email" required />
        <label for="user_email">
          <span className={`label-text  ${step == 0 ? 'main-lebel' : ''}`}>User Email</span>
          <span className="nav-dot" onClick={() => { handleStep(0) }}></span>
          <div className="signup-button-trigger" onClick={() => { handleStep(0) }}>Book</div>
        </label>
        <input className={`input-box ${step == 1 ? 'test-box' : ''}`}
          value={element.size}
          onChange={handleChange} min="0" max={keyLengthValidation.size} step="1" onKeyDown={(e) => { handleKeyDown(e, 1) }} id="size" type="number" name="size" placeholder="User Size" required />
        <label for="size">
          <span className={`label-text  ${step == 1 ? 'main-lebel' : ''}`}>User Size</span>
          <span className="nav-dot" onClick={() => { handleStep(1) }}></span>
        </label>
        <input className={`input-box ${step == 2 ? 'test-box' : ''}`}
          value={element.mobile}
          onChange={handleChange} maxLength={keyLengthValidation.mobile} onKeyDown={(e) => { handleKeyDown(e, 2) }} id="mobile" type="tel" name="mobile" placeholder="Enter Mobile Number" required />
        <label for="mobile">
          <span className={`label-text  ${step == 2 ? 'main-lebel' : ''}`}>Mobile Number</span>
          <span className="nav-dot" onClick={() => { handleStep(2) }}></span>
        </label>
        <textarea
          name="description"
          className={`input-box ${step == 3 ? 'test-box' : ''}`}
          style={{ height: '150px' }}
          onChange={handleChange}
          value={element.description}
          onKeyDown={(e) => { handleKeyDown(e, 3) }}
          id="description"
          cols="70"
          rows="10"
          placeholder="Tips on getting booking accepted, Keep your questions specific, Share your goal for session "></textarea>
        <label for="description">
          <span className={`label-textarea ${step == 3 ? 'main-lebel' : ''}`}>Description</span>
          <span className="nav-dot" onClick={() => { handleStep(3) }}></span>
        </label>
        <select
          className={`input-box ${step == 4 ? 'test-box' : ''}`}
          onChange={handleChange}
          value={element.assign}
          id="assign"
          name="assign"
          onKeyDown={(e) => { handleKeyDown(e, 4) }}
        >
          <option value="">Select Employee</option>
          {filterEmployee(user_information).map((cc) => <option key={cc._id} value={cc._id}>{cc.name}</option>)}
          {/* <option value="1">One</option>
          <option value="2">Two</option>
          <option value="3">Three</option>
          <option value="4">Four</option> */}
        </select>
        <label for="assign">
          <span className={`label-text  ${step == 4 ? 'main-lebel' : ''}`}>Select Employee</span>
          <span className="nav-dot" onClick={() => { handleStep(4) }}></span>
        </label>
        <select
          className={`input-box ${step == 5 ? 'test-box' : ''}`}
          id="input-6"
          name="s_type"
          onChange={handleOption}
          value={element.s_type}
          onKeyDown={(e) => { handleKeyDown(e, 5) }}
        >
          <option value="">Select Service Type</option>
          {[...new Set(topics.map((cc) => cc.motivation))].map((ss) => <option key={ss} value={ss}>{ss}</option>)}
        </select>
        <label for="input-6">
          <span className={`label-text  ${step == 5 ? 'main-lebel' : ''}`}>Select Service Type</span>
          <span className="nav-dot" onClick={() => { handleStep(5) }}></span>
        </label>
        <select
          className={`input-box ${step == 6 ? 'test-box' : ''}`}
          id="service_id"
          onChange={handleViewOption}
          value={element.service_id}
          onKeyDown={(e) => { handleKeyDown(e, 6) }}
          name="service_id">
          <option value="">Select Service</option>
          {filterTopics(element.s_type).map((cc) => <option key={cc._id} value={cc._id}>{cc.name}</option>)}
          {/* <option value="1">One</option>
          <option value="2">Two</option>
          <option value="3">Three</option>
          <option value="4">Four</option> */}
        </select>
        <label for="service_id">
          <span className={`label-text  ${step == 6 ? 'main-lebel' : ''}`}>Select Service</span>
          <span className="nav-dot" onClick={() => { handleStep(6) }}></span>
        </label>
        <button id="input-button" className='input-button' type="button" onClick={() => { handleSubmit() }}>Booking</button>
        {step > 0 && <span className='lab-back' onClick={() => { handleStep(step - 1) }}>Back</span>}
        {step <= 6 && <span className='lab-next' onClick={() => { handleStep(step + 1) }}>Next</span>}
        {(step == 3 || step == 4) && <span className='lab-skip' onClick={() => { handleStep(step + 1) }}>Skip</span>}
        {
          (tagK !== "" || tagK !== null || tagK !== undefined) && step == 3 && <>
            {filterTagTopics(tagK).map((cc, i) => {
              return <div className="description-yags" key={i}>
                {cc.tag.map((dd, i) => {
                  return (
                    <Chip key={dd} size="small" onClick={() => { handleTagBtn(dd) }} label={dd} variant="outlined"></Chip>
                  )
                })}
              </div>
            })}
          </>
        }
        {/* <div className="signup-button">Book</div> */}
      </>}
      {view == 1 && <>
        <input className={`input-box otp-box ${val == 0 ? 'test-box' : ''}`}
          value={otp}
          onChange={handleOtpChange} onKeyDown={main} maxLength={keyLengthValidation.otp} id="otp" type="tel" name="otp" placeholder="Enter Otp" required />
        <label for="otp">
          <span className="label-text">Enter Otp</span>
        </label>
        <button id="otp-button" className='otp-button' type="button" onClick={() => { handleOtp() }}>Verify</button>
      </>}
      {view == 2 && <>
        <Typography style={{ color: 'white', textAlign: 'center' }} variant="h5">
          Booking Completed Successfully
        </Typography>
        <button id="close-button" className='otp-button otp-btn' type="button" onClick={() => { handleClose() }}>Close</button>
      </>}
    </form>
  )
}

export default SliderForm



const validateForm = (val) => {
  let errors = {}

  if (!val.service_id) {
    errors['service_id'] = "Service is Required";
  }

  if (!val.description) {
    errors['description'] = "Description is Required";
  }

  if (!val.user_email && !val.mobile) {
    errors['user_email'] = "User Email is Required";
    errors['mobile'] = "Mobile Number is Required";
  }

  if (val.user_email) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    if (!pattern.test(val["user_email"])) {
      errors["user_email"] = "Please enter valid email.";
    }
  }

  if (!val.size) {
    errors['size'] = "User Size is Required";
  }

  if (val.size) {
    if (parseInt(val.size) > keyLengthValidation.size) {
      errors['size'] = `User Size must be less then or equal to ${keyLengthValidation.size}`;
    }
  }

  if (!val.user_email) {
    if (!val.mobile) {
      errors['mobile'] = "Mobile Number is Required";
    }

    if (val.mobile) {
      if (!val["mobile"].match(/^[0-9]{10}$/)) {
        errors["mobile"] = "Please enter valid mobile no.";
      }
    }
  }

  return errors
}