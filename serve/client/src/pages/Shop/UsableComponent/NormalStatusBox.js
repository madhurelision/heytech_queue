import React from 'react'
import { Chip } from '@mui/material'

const NormalStatusBox = ({ label }) => {
  return (
    <Chip style={(label == true) ? { color: 'white', backgroundColor: 'green' } : { color: 'white', backgroundColor: 'red' }} label={(label == true) ? 'true' : 'false'} />
  )
}

export default React.memo(NormalStatusBox)
