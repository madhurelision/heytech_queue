import React from 'react'
import { Chip, Typography } from '@mui/material'
import { bookingStatusStyles } from '../Dashboard/styles'

const BookStatus = ({ value }) => {

  const classes = bookingStatusStyles()

  return (
    <>
      <Typography variant='h4' component="div" className={classes[value]} style={{ textTransform: 'capitalize', fontSize: '0.95rem' }}>
        {value}
      </Typography>
    </>
  )
}

export default BookStatus
