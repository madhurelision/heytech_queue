import React, { useState, useEffect } from 'react'
import { FormControl, InputLabel, MenuItem, Select } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import TableLoader from './TableLoader'


const EmployeeSelect = ({ value, handleChange, _id }) => {

  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const employeeData = useSelector((state) => state.auth.shopEmployee)

  useEffect(() => {

    if (employeeData.length == 0) {
    } else {
      setElement(employeeData.employee)
      setFlag(true)
    }

  }, [employeeData])

  return (
    <>
      {flag ? (element.length == 0 ? <div>No Employee</div> : <FormControl variant="outlined" fullWidth style={(value == null || value == undefined || value == "") ? { width: '100px' } : {}} >
        <InputLabel id="demo-simple-select-label" shrink={true} >Assign</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={value}
          label="Assign"
          name="Assign"
          onChange={(e) => { handleChange(e, _id) }}
        >
          <MenuItem value="">{"None"}</MenuItem>
          {element.map((cc) => {
            return (
              <MenuItem key={cc._id} value={cc._id}>{cc.name}</MenuItem>
            )
          })}
        </Select>
      </FormControl>) : <><TableLoader /></>}
    </>
  )
}


export default EmployeeSelect