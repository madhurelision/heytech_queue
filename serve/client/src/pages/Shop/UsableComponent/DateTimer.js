import React, { useEffect, useState } from 'react'
import moment from 'moment'

const DateTimer = () => {

  const [date, setDate] = useState(new Date());

  useEffect(() => {
    const timer = setInterval(() => setDate(new Date()), 1000);

    return () => {
      clearInterval(timer)
    }
  }, [])

  return (
    <>
      {moment().format('MMMM Do YYYY, h:mm:ss a')}
    </>
  )
}

export default DateTimer