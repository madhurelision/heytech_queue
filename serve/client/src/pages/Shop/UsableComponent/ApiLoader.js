import React from 'react'
import {
  Box,
  CircularProgress,
  LinearProgress
} from '@mui/material'
import { makeStyles } from '@mui/styles'

const useStyles = makeStyles((theme) => ({
  ss: {
    // '& > * + *': {
    //   marginLeft: theme.spacing(2),
    // },
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    zIndex: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    position: 'fixed'
  },
  circle: {
    position: 'fixed',
    top: '50%',
    left: '50%',
    margin: '-50px 0px 0px -50px',
    zIndex: 50
  }
}))

const ApiLoader = () => {
  const classes = useStyles()
  return (
    <>
      <Box sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }} className={classes.ss}>
        <LinearProgress />
      </Box>
      <Box sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }} className={classes.circle}>
        <CircularProgress size={80} />
      </Box>
    </>
  )
}

export default React.memo(ApiLoader)
