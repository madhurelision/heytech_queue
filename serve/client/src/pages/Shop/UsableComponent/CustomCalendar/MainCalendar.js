import React, { useState, useEffect } from 'react';
import { styled } from '@mui/material'
import datesGenerator from './dateGenerator'
import moment from 'moment'
import './calendar.css';

const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']

const Container = styled('div')`
  width: 100%;
  border: 1px solid black;
  margin: 0 auto;
  /* box-shadow: 10px 10px 0px black; */
`

const MonthText = styled('div')`
  font-size: 26px;
  font-weight: bold;
  text-align: center;
`
function isSameDay(d1, d2) {
  return d1.getFullYear() === d2.getFullYear() &&
    d1.getDate() === d2.getDate() &&
    d1.getMonth() === d2.getMonth();
}

const MainCalendar = ({ arr }) => {
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [dates, setDates] = useState([]);
  const [calendar, setCalendar] = useState({
    month: selectedDate.getMonth(),
    year: selectedDate.getFullYear(),
  });


  useEffect(() => {
    const body = {
      month: calendar.month,
      year: calendar.year
    };
    const { dates, nextMonth, nextYear, previousMonth, previousYear } = datesGenerator(body);

    setDates([...dates]);
    setCalendar({
      ...calendar,
      nextMonth,
      nextYear,
      previousMonth,
      previousYear
    });
  }, [])

  const onClickNext = () => {
    const body = { month: calendar.nextMonth, year: calendar.nextYear };
    const { dates, nextMonth, nextYear, previousMonth, previousYear } = datesGenerator(body);

    setDates([...dates]);
    setCalendar({
      ...calendar,
      month: calendar.nextMonth,
      year: calendar.nextYear,
      nextMonth,
      nextYear,
      previousMonth,
      previousYear
    });
  }

  const onClickPrevious = () => {
    const body = { month: calendar.previousMonth, year: calendar.previousYear };
    const { dates, nextMonth, nextYear, previousMonth, previousYear } = datesGenerator(body);

    setDates([...dates]);
    setCalendar({
      ...calendar,
      month: calendar.previousMonth,
      year: calendar.previousYear,
      nextMonth,
      nextYear,
      previousMonth,
      previousYear
    });
  }

  const onSelectDate = (date) => {
    setSelectedDate(new Date(date.year, date.month, date.date))
  }

  return (
    <div className='cal-main'>
      <Container>
        <div className='p10'>
          <div onClick={onClickPrevious} className="cal-prev-btn"> <span className="cal-color">Previous</span> </div>
          <div onClick={onClickNext} className="cal-next-btn"> <span className="cal-color">Next</span> </div>
        </div>
        <MonthText>{months[calendar.month]}</MonthText>
        <div>
          <div>
            <table className='cal-table'>
              <tbody>
                <tr>
                  {days.map((day) => (
                    <td key={day} className="p15"><div className='cal-day'>{day}</div></td>
                  ))}
                </tr>

                {dates.length > 0 && dates.map((week) => (
                  <tr key={JSON.stringify(week[0])}>
                    {week.map((each) => (
                      <td key={JSON.stringify(each)} className="p15">
                        <div className='cal-day'>
                          {each.date}
                          {arr.map((cc) => {
                            if (isSameDay(new Date(each.year, each.month, each.date), new Date(cc.createdAt))) {
                              return <div key={cc._id} >
                                <span className={`calendar-font ${(cc.type == 'Login') ? 'calendar_green' : 'calendar_red'}`}>{cc.type} {moment(cc.createdAt).format('LT')}</span>
                              </div>
                            }
                          })}
                        </div>
                      </td>
                    ))}
                  </tr>
                ))}
              </tbody>
            </table>
          </div>

        </div>
        {/* <div style={{ padding: 10 }}>
          Selected Date: {selectedDate.toLocaleString()}
        </div> */}
      </Container >
    </div >
  );
}

export default MainCalendar;