import React from 'react'
import { Card, CardContent, CardMedia, Box, Typography, Grid } from '@mui/material'
import moment from 'moment'
import StatusBox from './StatusBox'

const ShopQueueCard = (props) => {
  return (
    <Card sx={{ borderRadius: '18px' }} >
      <CardMedia
        component="img"
        sx={{ width: '100%', objectFit: 'unset', borderRadius: '18px', border: 1 }}
        image={props?.service_id?.image}
        alt={props?.service_id?.name}
      />
      <Box>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={6} sm={6} >
              <span>
                {props.queue == true ? 'Queue' : 'Appointment'}
              </span>
            </Grid>
            <Grid item xs={6} sm={6} >
              <span>
                {'#' + props?.bookingid}
              </span>
            </Grid>
            <Grid item xs={6} sm={6}>
              <Typography variant='h4' component="div" style={{ fontSize: '1.125rem' }}>
                {props?.service_id?.name}
              </Typography>
              <div>
                {props.size + ' ' + 'users'}
              </div>
              <div>
                {'Seat No' + ': ' + props?.seat}
              </div>
            </Grid>
            <Grid item xs={6} sm={6} sx={{ fontSize: '0.85rem !important' }}>
              <StatusBox value={props.status} />
              <div>
                {moment(props.appointment_date).format('LL')}
              </div>
              <div>
                {moment(props.appointment_date).calendar()}
              </div>
            </Grid>
          </Grid>
        </CardContent>
      </Box>
    </Card>
  )
}

export default ShopQueueCard