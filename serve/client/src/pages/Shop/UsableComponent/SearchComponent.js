import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { TextField, Autocomplete, styled, InputAdornment } from "@mui/material";
import { getAllShopRegexMatch, getAllShopTextMatch } from "redux/action";
import { NewSearchBarber as SearchBarber } from "components/common";
import { useParams } from "react-router-dom";
import { Search as SearchBtn } from '@mui/icons-material'

var welc = false

const SearchComponent = ({ handleView }) => {

  const [element, setElement] = useState([])
  const [category, setCategory] = useState('')
  const [open, setOpen] = useState(false)
  const shopData = useSelector((state) => state.auth.shopList)
  const dispatch = useDispatch()
  const { id } = useParams()

  useEffect(() => {

    if (shopData.length > 0) {
      setElement(shopData)
    }

  }, [shopData])


  const [flag, setFlag] = useState(false)

  const handleButton = (val) => {
    if (val.length > 2) {
      setFlag(true)
      welc = true
    }
    if (val == "" || val == null || val.length == 0) {
      setFlag(false)
      welc = false
    }
    if (welc) {
      setCategory(val)
      dispatch(getAllShopRegexMatch(val))
    } else {
      setCategory(val)
      setOpen(false)
    }
  }

  const handleSelect = () => {

  }

  return (
    <>
      {/* <Autocomplete
        id="asynchronous-demo"
        open={open}
        onOpen={
          () => {
            if (element.length == 0) {

            } else {
              setOpen(true);
            }
          }
        }
        onClose={
          () => {
            setOpen(false);
            handleButton('');
          }
        }
        style={{ marginBottom: '5px' }}
        isOptionEqualToValue={(option, value) => option.first_name === value.first_name}
        getOptionLabel={(option) => option.first_name + ' ' + option.last_name}
        options={element}
        loading={element.length == 0 ? true : false}
        fullWidth
        renderInput={(params) => (
          <TextField
            {...params}
            label="Search By Name & Expertise"
            variant="outlined"
            value={category}
            onChange={(e) => {
              setOpen(true)
              console.log('value', e.target.value)
              handleButton(e.target.value);
            }}
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
            fullWidth
          />
        )}
      /> */}
      <SearchBarber
        id="asynchronous-demo"
        open={open}
        onOpen={
          () => {
            if (element.length == 0) {

            } else {
              setOpen(true);
            }
          }
        }
        onClose={
          () => {
            setOpen(false);
            handleButton('');
          }
        }
        isOptionEqualToValue={(option, value) => option.shop_name === value.shop_name}
        getOptionLabel={(option) => option.shop_name}
        options={(id == null || id == undefined || id.length == 0) ? element.filter((cc) => cc.type == '6242d0c9f0addae2c2328a59') : element.filter((cc) => cc.type == id)}
        loading={element.length == 0 ? true : false}
        fullWidth
        onChange={handleView}
        renderInput={(params) => (
          <TextField
            {...params}
            //label={"Search By Shop"}
            variant="outlined"
            placeholder="Search by shop"
            value={category}
            onChange={(e) => {
              setOpen(true)
              console.log('value', e.target.value)
              handleButton(e.target.value);
            }}
            InputProps={{
              ...params.InputProps,
              startAdornment: (
                <>
                  <InputAdornment position="start"><SearchBtn sx={{ color: 'black !important', display: 'none', fontSize: '30px' }} /></InputAdornment>
                  {params.InputProps.startAdornment}
                </>
              ),
              endAdornment: (
                <React.Fragment>
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
            fullWidth
          />
        )}
      />
    </>
  )
}

export default SearchComponent
