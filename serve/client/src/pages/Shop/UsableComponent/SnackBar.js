import React from 'react'
import { Snackbar, Alert } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { LOADERCLOSE } from 'redux/action/types'
import { useNavigate } from 'react-router-dom'

const SuccessSnackbar = ({ open, handleClose }) => {
  const dispatch = useDispatch()
  const msg = useSelector(state => state.auth.message)
  return (
    <Snackbar anchorOrigin={{
      vertical: 'top', horizontal: 'right'
    }} open={open} autoHideDuration={2000} onClose={() => { dispatch({ type: LOADERCLOSE }) }}>
      <Alert onClose={() => { dispatch({ type: LOADERCLOSE }) }} elevation={6} variant="filled" severity="success">
        {(msg == "" || msg == null || msg == undefined) ? 'Request Submitted' : msg}
      </Alert>
    </Snackbar>
  )
}

const FailedSnackbar = ({ open, handleClose }) => {
  const dispatch = useDispatch()
  const msg = useSelector(state => state.auth.message)

  return (
    <Snackbar anchorOrigin={{
      vertical: 'top', horizontal: 'right'
    }} open={open} autoHideDuration={2000} onClose={() => { dispatch({ type: LOADERCLOSE }) }}>
      <Alert onClose={() => { dispatch({ type: LOADERCLOSE }) }} elevation={6} variant="filled" severity="error">
        {(msg == "" || msg == null || msg == undefined) ? 'Request Failed' : msg}
      </Alert>
    </Snackbar>
  )
}

const ServiceSnackbar = ({ open, message }) => {

  const history = useNavigate()

  return (
    <Snackbar anchorOrigin={{
      vertical: 'top', horizontal: 'right'
    }} open={open} autoHideDuration={2000} onClose={() => { history('/shop/service') }}>
      <Alert onClose={() => { history('/shop/service') }} elevation={6} variant="filled" severity="error">
        {message}
      </Alert>
    </Snackbar>
  )
}

export { SuccessSnackbar, FailedSnackbar, ServiceSnackbar }
