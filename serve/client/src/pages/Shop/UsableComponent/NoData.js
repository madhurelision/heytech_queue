import React from "react";
import { makeStyles } from "@mui/styles";
import { Container, Grid } from '@mui/material';
import nodataicon from "../../../assets/Nodata1.svg";
import NoDataGif from "../../../assets/NoData.png";
import './NoData.css'

const useStyles = makeStyles((theme) => ({
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    height: '100%'
  }
}));


// const NoData = () => {
//   const classes = useStyles();
//   return (
//     <Container component="main" maxWidth="xs">

//       <div className={classes.paper}>
//         <img src={NoDataGif} alt="No data found...." />
//       </div>
//     </Container>
//   );
// };
const NoData = () => {
  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={12} md={12} textAlign="center">
        <img src={NoDataGif} alt="No data found...." className="noimageContent" />
      </Grid>
    </Grid>
  );
};
export default NoData;
