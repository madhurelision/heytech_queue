import React, { useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Carousel } from 'react-bootstrap'
import { Rating, useTheme } from '@mui/material'
import './testimonial.css'
import { ReviewBack } from 'config/KeyData';

const Testimonial = ({ element, test }) => {

  const theme = useTheme()

  return (
    <>
      {(test == true) ? <Carousel className='check-carousel' variant={theme.palette.mode == 'dark' ? 'light' : 'dark'} >

        {
          element.map((cc) => {
            return (
              <Carousel.Item key={cc._id}>
                <img
                  className="d-block w-100"
                  src={theme.palette.mode == 'dark' ? ReviewBack.black : ReviewBack.white}
                  alt="First slide"
                />
                <Carousel.Caption className='mainImgCont' >
                  <Rating name="read-only" value={cc.rating} readOnly />
                  <h5>{cc.user_name}</h5>
                  <p>{cc.description}</p>
                </Carousel.Caption>
              </Carousel.Item>
            )
          })
        }
      </Carousel> : <Carousel variant={theme.palette.mode == 'dark' ? 'light' : 'dark'}>

        {
          element.map((cc) =>
            (cc.shop_review == true) ? (<Carousel.Item key={cc._id}>
              <img
                className="d-block w-100"
                src={theme.palette.mode == 'dark' ? ReviewBack.black : ReviewBack.white}
                alt="First slide"
              />
              <Carousel.Caption className='mainImgCont' >
                <Rating name="read-only" value={cc.rating} readOnly />
                {/* <h5>{cc.user_email}</h5> */}
                <h5>{cc.shop_name}</h5>
                <p>{cc.description}</p>
              </Carousel.Caption>
            </Carousel.Item>) : (<Carousel.Item key={cc._id}>
              <img
                className="d-block w-100"
                src={theme.palette.mode == 'dark' ? ReviewBack.black : ReviewBack.white}
                alt="First slide"
              />
              <Carousel.Caption className='mainImgCont' >
                <Rating name="read-only" value={cc.rating} readOnly />
                {/* <h5>{cc.user_email}</h5> */}
                <h5>{cc.user_name}</h5>
                <p>{cc.description}</p>
              </Carousel.Caption>
            </Carousel.Item>)
          )
        }
      </Carousel>}
    </>
  )
};

export default Testimonial;
