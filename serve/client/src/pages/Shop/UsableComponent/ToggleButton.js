import { FormControlLabel } from '@mui/material'
import { MaterialUISwitch } from 'components/common'
import { ToggleContext } from 'pages/routes'
import React, { useContext } from 'react'

const ToggleButton = () => {

  const { show, setShow } = useContext(ToggleContext)

  const handleBtn = (data) => {
    localStorage.setItem('theme', data)
    setShow(data)
  }

  return (
    <FormControlLabel
      control={<MaterialUISwitch sx={{ m: 1 }} checked={(show == 'dark') ? true : false} onChange={() => {
        handleBtn(show == 'dark' ? 'light' : 'dark')
      }} />}
      label=""
    />
  )
}

export default ToggleButton