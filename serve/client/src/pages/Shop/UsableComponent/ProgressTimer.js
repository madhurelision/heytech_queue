import React, { useState, useEffect, useRef } from 'react'
import { CircularProgress, LinearProgress } from '@mui/material'
import NewTimer from './NewTimer';
import { dateMinus } from 'config/KeyData';

const ProgressTimer = ({ time, ma, func, show }) => {

  const Ref = useRef(null);
  const [progress, setProgress] = useState(0)

  const getTimeRemaining = (e) => {
    var mainV = new Date(e)
    mainV.setSeconds(mainV.getSeconds())
    //console.log('result first date', e, Date.parse(mainV), Date.parse(new Date()))
    const total = Date.parse(mainV) - Date.parse(new Date());
    const seconds = Math.floor((total / 1000) % 60);
    const minutes = Math.floor((total / 1000 / 60) % 60);
    const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
    const days = Math.floor((total / (1000 * 60 * 60 * 24)))
    if (total > 0) {
      return (show == true) ? days + ':' +
        (hours > 9 ? hours : '0' + hours) + ':' +
        (minutes > 9 ? minutes : '0' + minutes) + ':'
        + (seconds > 9 ? seconds : '0' + seconds) : (minutes > 9 ? minutes : '0' + minutes) + ':'
      + (seconds > 9 ? seconds : '0' + seconds)
    } else {
      return (show == true) ? '00:00:00:00' : '00:00'
    }
  }

  const getTotal = (e) => {
    return Date.parse(e) - Date.parse(new Date());
  }

  const getPercent = (e) => {
    //const total = Date.parse(new Date()) - Date.parse(e);
    //console.log('Percentage', e, 'New D', Date.parse(new Date()), 'test', (e / 1000000), 'cent', 100 - (e / 100000))
    //console.log('MainSSSSS', 100 - total / 1000000)
    if (e > 0) {
      setProgress(100 - (e / 100000));
    } else {
      clearInterval(Ref.current)
      Ref.current = null
      setProgress(100)
      func(ma)
    }
  }

  const clearTimer = (e) => {
    //console.log('main', e)
    setProgress(0);

    if (Ref.current) clearInterval(Ref.current);
    const id = setInterval(() => {
      var cc = getTotal(e)
      getPercent(cc);
      cc = null
    }, 1000)
    Ref.current = id;
  }

  const getDeadTime = () => {
    let deadline = new Date(time);
    deadline.setSeconds(deadline.getSeconds());
    return deadline;
  }

  useEffect(() => {
    clearTimer(getDeadTime());
    return () => {
      window.clearInterval(Ref.current)
      Ref.current = null
      setProgress(0)
    }
  }, []);

  return (
    <div>
      {getTimeRemaining(time)}
      {/* {show == true && <NewTimer main={time} />} */}
    </div>
  )
}

export default React.memo(ProgressTimer)
