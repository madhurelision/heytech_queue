import React from 'react'
import { Chip, Typography } from '@mui/material'
import { statusStyles } from '../Dashboard/styles'

const StatusBox = ({ value }) => {

  const classes = statusStyles()

  return (
    <>
      <Typography variant='h4' component="div" className={classes[value]} style={{ textTransform: 'capitalize' }}>
        {value}
      </Typography>
    </>
  )
}

export default StatusBox
