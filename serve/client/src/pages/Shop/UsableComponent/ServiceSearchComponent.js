import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { TextField, Autocomplete, styled } from "@mui/material";
import { getAllServiceRegexMatch, getAllShopTextMatch } from "redux/action";
import { SearchBarber } from "components/common";

var welc = false

const ServiceSearchComponent = ({ handleView, status }) => {

  const [element, setElement] = useState([])
  const [category, setCategory] = useState('')
  const [open, setOpen] = useState(false)
  const serviceData = useSelector((state) => state.auth.serviceList)
  const profileD = useSelector((state) => state.auth.profile)
  const dispatch = useDispatch()

  useEffect(() => {

    if (serviceData.length > 0) {
      setElement(serviceData)
    }

  }, [serviceData])


  const [flag, setFlag] = useState(false)

  const handleButton = (val) => {
    if (val.length > 2) {
      setFlag(true)
      welc = true
    }
    if (val == "" || val == null || val.length == 0) {
      setFlag(false)
      welc = false
    }
    if (welc) {
      setCategory(val)
      dispatch(getAllServiceRegexMatch(val))
    } else {
      setCategory(val)
      setOpen(false)
    }
  }

  return (
    <>
      <SearchBarber
        id="asynchronous-demo"
        open={open}
        onOpen={
          () => {
            if (element.length == 0) {

            } else {
              setOpen(true);
            }
          }
        }
        onClose={
          () => {
            setOpen(false);
            handleButton('');
          }
        }
        isOptionEqualToValue={(option, value) => option.name === value.name}
        getOptionLabel={(option) => option.name + ' ' + option.motivation}
        options={status == false ? element : element.filter((cc) => cc.user_id !== profileD.user_information)}
        loading={element.length == 0 ? true : false}
        fullWidth
        onChange={handleView}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Search By Name & Motivation"
            variant="outlined"
            value={category}
            onChange={(e) => {
              setOpen(true)
              console.log('value', e.target.value)
              handleButton(e.target.value);
            }}
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
            fullWidth
          />
        )}
      />
    </>
  )
}

export default ServiceSearchComponent
