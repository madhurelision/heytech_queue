import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { TextField } from "@mui/material";
import { getAllServiceShopRegexMatch } from "redux/action";
import { SearchBarber } from "components/common";

var welc = false

const ServiceShopSearch = ({ handleView }) => {

  const [element, setElement] = useState([])
  const [category, setCategory] = useState('')
  const [open, setOpen] = useState(false)
  const serviceData = useSelector((state) => state.auth.shopService)
  const dispatch = useDispatch()

  useEffect(() => {

    if (serviceData.length == 0) {
    } else {
      setElement(serviceData.service)
    }

  }, [serviceData])


  const [flag, setFlag] = useState(false)

  const handleButton = (val) => {
    if (val.length > 2) {
      setFlag(true)
      welc = true
    }
    if (val == "" || val == null || val.length == 0) {
      setFlag(false)
      welc = false
    }
    if (welc) {
      setCategory(val)
      dispatch(getAllServiceShopRegexMatch(val))
    } else {
      setCategory(val)
      setOpen(false)
    }
  }

  return (
    <>
      <SearchBarber
        id="asynchronous-demo"
        open={open}
        onOpen={
          () => {
            if (element.length == 0) {

            } else {
              setOpen(true);
            }
          }
        }
        onClose={
          () => {
            setOpen(false);
            handleButton('');
          }
        }
        isOptionEqualToValue={(option, value) => option.name === value.name}
        getOptionLabel={(option) => option.name + ' ' + option.motivation}
        options={element}
        loading={element.length == 0 ? true : false}
        fullWidth
        onChange={handleView}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Search By Name & Motivation"
            variant="outlined"
            value={category}
            onChange={(e) => {
              setOpen(true)
              console.log('value', e.target.value)
              handleButton(e.target.value);
            }}
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
            fullWidth
          />
        )}
      />
    </>
  )
}

export default ServiceShopSearch
