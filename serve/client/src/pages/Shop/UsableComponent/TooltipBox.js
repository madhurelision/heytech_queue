import React from 'react'
import { Fade, Tooltip, IconButton } from '@mui/material'
import { Description } from '@mui/icons-material'

const TooltipBox = ({ title, data }) => {
  return (
    <Tooltip
      TransitionComponent={Fade}
      TransitionProps={{ timeout: 600 }}
      title={data}
    >
      <IconButton>
        <Description />
      </IconButton>
    </Tooltip>
  )
}

export default TooltipBox
