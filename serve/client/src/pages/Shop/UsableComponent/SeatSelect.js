import React from 'react'
import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';


const SeatSelect = ({ value, handleChange, _id, seat }) => {


  return (
    <FormControl fullWidth style={(value == null || value == undefined || value == "") ? { width: '100px' } : {}} >
      <InputLabel id="demo-simple-select-label">Seat</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={value}
        label="Seat"
        name="Seat"
        onChange={(e) => { handleChange(e, _id) }}
      >
        {seat.map((cc) => {
          return (
            <MenuItem key={cc} value={cc}>{cc}</MenuItem>
          )
        })}
      </Select>
    </FormControl>
  )
}


export default SeatSelect