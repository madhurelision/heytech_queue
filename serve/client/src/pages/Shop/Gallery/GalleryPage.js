import React, { useState } from 'react'
import CreateGallery from './CreateGallery'
import GalleryList from './GalleryList'

const GalleryPage = () => {

  const [id, setId] = useState(0)

  switch (id) {
    case 0:
      return <GalleryList setId={setId} />
    case 1:
      return <CreateGallery setId={setId} />
    default:
      return <GalleryList setId={setId} />
  }
}

export default GalleryPage
