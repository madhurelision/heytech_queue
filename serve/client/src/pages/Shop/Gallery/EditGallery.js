import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Dialog, DialogContent, Button, IconButton, Typography, Box } from '@mui/material'
import ImageCropper from '../../ImageCropper/ImageCropper'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import '../../Registration/register.css'
import { galleryUpdate } from 'redux/action'
import NewImageCropper from 'pages/ImageCropper/NewImageCropper'
import { BookingButton } from "components/common";
import { Close } from '@mui/icons-material'

const EditGallery = ({ allvalue, setId }) => {

  const [data, setData] = useState({
    image: '',
    oldimage: allvalue.image,
  })

  const [open, setOpen] = useState({
    open: false,
    name: '',
    image: ''
  })

  const [image, setImage] = useState(allvalue.image)
  const [err, setErr] = useState({})
  const [btn, setBtn] = useState(true)
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.created)
  const loading = useSelector(state => state.auth.loader)

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])

  const onHandleImage = (e) => {

    if (e.target.files == null || e.target.files == undefined) {
      return
    }

    const file = Math.round((e.target.files[0].size / 1024 * 1024));
    console.log('Image Size', e.target.files[0].size, 'Image Size Mb', file)

    if (file > 512000) {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      setErr({ ...err, [e.target.name]: 'Image Size Should be less than 500 kb' })
      return
    }

    const VIDEO_FORMATS = ["video/mp4", "video/mkv"];
    console.log('Image Console', e.target.name, e.target.files)
    console.log('Main', document.getElementsByName(e.target.name)[0].value)
    if (!VIDEO_FORMATS.includes(e.target.files[0].type)) {
      setData({ ...data, [e.target.name]: e.target.files[0] })
      setErr({ ...err, [e.target.name]: '' })
      setOpen({ ...open, open: true, name: e.target.name, image: URL.createObjectURL(e.target.files[0]) })
      setBtn(false)
    } else {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      return
    }
  }


  const CrooppedImageN = (imgss) => {
    console.log('Cropped Image Called', imgss)
    var ext = (imgss.type == "image/jpeg") ? '.jpg' : (imgss.type == "image/png") ? '.png' : '.jpg'
    const values = blobToFile(imgss, open.name + ext)
    console.log('Cropped Image Called', values)
    setData({ ...data, [open.name]: values })
    setOpen({ ...open, open: false })
    setImage(URL.createObjectURL(values))
  }

  const blobToFile = (theBlob, fileName) => {
    return new File([theBlob], fileName, { lastModified: new Date().getTime(), type: theBlob.type })
  }

  const handleClose = () => {
    document.getElementsByName(open.name)[0].value = ''
    setData({ ...data, [open.name]: '' })
    setOpen({ ...open, open: false })
    setBtn(true)
  }

  const handleSubmit = () => {
    var check = galleryValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)

    var formData = new FormData()
    formData.append('image', data.image)
    formData.append('oldimage', data.oldimage)
    dispatch(galleryUpdate(allvalue?._id, formData))
  }

  return (
    <Grid >
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}><Grid className="creatPop" item xs={12} sm={12} md={12}>
              <Box display="flex" alignItems="center">
                <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                  Edit Gallery
                </Typography>
                <Box className="crossIcon">
                  <IconButton onClick={() => { setId(0) }}>
                    <Close />
                  </IconButton>
                </Box>
              </Box>
            </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="image" type="file" fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={onHandleImage} label="Image" />
                {err.image && (<FormError data={err.image}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <img src={image} width="100%" height="100%" />
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }}>
              <Grid item xs={6} align="left">
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId() }}>Close</BookingButton>
              </Grid>
              <Grid item xs={6} align="right">
                <BookingButton type="button" disabled={(loading || btn) ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Submit</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
      <Dialog open={open.open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description" maxWidth="md" fullWidth>
        <DialogContent>
          <NewImageCropper img={open.image} click={CrooppedImageN} ratio={16 / 9} />
        </DialogContent>
      </Dialog>
    </Grid>
  )
}

export default EditGallery

const galleryValidate = (data) => {
  let errors = {}

  if (!data.oldimage) {
    errors["image"] = "Image is Required"
  }

  return errors
}