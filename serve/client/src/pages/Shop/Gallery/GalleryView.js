import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import './gallery.css'
import { Close, KeyboardArrowLeftOutlined, KeyboardArrowRightOutlined } from '@mui/icons-material';
import { Dialog, DialogContent, DialogTitle, Grid, IconButton, ImageList, ImageListItem, Box } from '@mui/material';

const GalleryView = ({ element }) => {

  const [index, setIndex] = useState(0);
  const [open, setOpen] = useState(false);

  const next = () => {
    setIndex((i) => (i + 1) % element.length);
  };

  const prev = () => {
    setIndex(
      (i) => (((i - 1) % element.length) + element.length) % element.length
    );
  };

  const handleOpen = (i) => {
    setIndex(i)
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  return (
    <div>
      {/* {
        element.map((cc, i) => {
          return (
            <div key={i} className="col-lg-4 col-md-12 mb-4 mb-lg-0" onClick={() => { handleOpen(i) }}>
              <img
                src={cc.image}
                className="w-100 shadow-1-strong rounded mb-4"
                alt="Queue"
                height="200px"
              />
            </div>
          )
        })
      } */}

      <ImageList sx={(theme) => ({
        [theme.breakpoints.up('md')]: {
          width: '100%', gap: '0px !important',
          gridTemplateColumns: 'repeat(4, 1fr) !important'
        },
        [theme.breakpoints.up('xs')]: {
          width: '100%', gap: '0px !important',
          gridTemplateColumns: 'repeat(2, 1fr) !important'
        },
        [theme.breakpoints.up('sm')]: {
          width: '100%',
          gap: '0px !important',
          gridTemplateColumns: 'repeat(4, 1fr) !important'
        }
      })}>
        {element.map((item, i) => (
          <ImageListItem key={item._id}>
            <img
              src={`${item.image}?w=164&h=164&fit=crop&auto=format`}
              srcSet={`${item.image}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
              alt="Gallery"
              loading="lazy"
              onClick={() => { handleOpen(i) }}
            />
          </ImageListItem>
        ))}
      </ImageList>

      {/* {open && (
        <div className="galleryModal">
          <div className="closeBtn">
            <IconButton title="Close" onClick={handleClose}>
              <Close />
            </IconButton>
          </div>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <div>
              <IconButton title="Left" onClick={prev}>
                <KeyboardArrowLeftOutlined />
              </IconButton>
            </div>
            <div style={{ padding: '100px' }}>
              <img src={element[index].image} alt="Queue" className="modalImage" />
            </div>
            <div>
              <IconButton title="Right" onClick={next}>
                <KeyboardArrowRightOutlined />
              </IconButton>
            </div>
          </div>
        </div>
      )} */}
      <Dialog
        open={open}
        onClose={handleClose}
        fullScreen
      >
        <DialogTitle>
          <Box display="flex" alignItems="center">
            <Box flexGrow={1} ></Box>
            <Box>
              <IconButton
                edge="end"
                color="inherit"
                onClick={handleClose}
                aria-label="close"

              >
                <Close />
              </IconButton>
            </Box>
          </Box>
        </DialogTitle>
        <DialogContent sx={{ overflow: "hidden" }}>
          <Grid container spacing={16} alignItems="center"
            justifyContent="center">
            <Grid item xs={12} sm={12} md={1} textAlign="start"
            // sx={(theme) => ({
            //   [theme.breakpoints.up('sm')]: {
            //     padding: '100px !important'
            //   },
            //   [theme.breakpoints.up('xs')]: {
            //     padding: '100px !important'
            //   },
            //   [theme.breakpoints.up('md')]: {
            //     padding: '0px !important'
            //   },
            //   [theme.breakpoints.up('lg')]: {
            //     padding: '0px !important'
            //   }
            // })}
            >

            </Grid>
            <Grid item xs={12} sm={12} md={10}>
              <Grid container spacing={0} alignItems="center"
                justifyContent="center" >
                <Grid item xs={2} sm={2} md={1} sx={{ position: 'absolute', left: '6%' }} textAlign="start" >
                  <IconButton title="Left" onClick={prev}>
                    <KeyboardArrowLeftOutlined />
                  </IconButton>
                </Grid>
                <Grid item xs={12} sm={12} md={10} >
                  <img src={element[index].image} alt="Gallery" className='modalImage' />
                </Grid>
                <Grid item xs={2} sm={2} md={1} sx={{ position: 'absolute', right: '6%' }} textAlign="end">
                  <IconButton title="Right" onClick={next}>
                    <KeyboardArrowRightOutlined />
                  </IconButton>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={12} md={1} textAlign="end">

            </Grid>
          </Grid>


        </DialogContent>
      </Dialog>
    </div>
  )
}

export default GalleryView
