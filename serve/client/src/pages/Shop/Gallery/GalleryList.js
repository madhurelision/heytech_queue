import React, { useEffect, useState } from 'react'
import { Grid, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Typography, Card, CardContent, Button, IconButton, DialogTitle, Dialog, Box, CardMedia } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { galleryDelete, getShopGallery } from 'redux/action'
import TableLoader from '../UsableComponent/TableLoader'
import NoData from '../UsableComponent/NoData'
import { tableStyles } from '../Dashboard/styles'
import { AddCircleOutline, Close, Edit, GridView, Link, Refresh, ViewList, Delete } from '@mui/icons-material'
import GalleryView from './GalleryView'
import ImageBox from '../UsableComponent/ImageBox'
import EditGallery from './EditGallery'
import UrlForm from './UrlForm'
import MobileLoader from '../UsableComponent/MobileLoader'
import moment from 'moment'
import '../Queue/queue.css'

const GalleryList = ({ setId }) => {

  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const [view, setView] = useState(true)
  const [open, setOpen] = useState(false)
  const [drop, setDrop] = useState('')
  const galleryData = useSelector((state) => state.auth.shopGallery)
  const cted = useSelector((state) => state.auth.created)
  const loading = useSelector((state) => state.auth.loader)
  const dispatch = useDispatch()
  const [data, setData] = useState({
    open: false,
    image: '',
    _id: ''
  })


  const classes = tableStyles()

  useEffect(() => {

    if (galleryData.length == 0) {
      dispatch(getShopGallery())
    } else {
      setElement(galleryData.gallery)
      setFlag(true)
    }

    if (cted) {
      dispatch(getShopGallery())
    }
  }, [galleryData, cted])

  const handleRefresh = () => {
    dispatch(getShopGallery())
  }


  const handleEdit = (val) => {
    setData({
      ...data,
      open: true,
      image: val.image,
      _id: val._id
    })
  }

  const handleClose = () => {
    setData({ ...data, open: false })
  }

  const handleCloseBox = () => {
    setOpen(false)
  }

  const handleDelete = (item) => {
    dispatch(galleryDelete(item))
  }

  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card>
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={2} sm={2} md={2} >
                <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh} >
                  Refresh
                </Button>
              </Grid>
              <Grid item xs={8} sm={8} md={8} >
                <Typography className='title_head' sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }} variant="h4" component="h2" align="center">
                  Gallery List
                </Typography>
                <Typography className='title_head' sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }} variant="h5" component="h2" align="center">
                  Gallery List
                </Typography>
              </Grid>
              <Grid item xs={2} sm={2} md={2} ></Grid>
            </Grid>
            <Button className='customer-btn' startIcon={view ? <GridView color='primary' /> : <ViewList color='primary' />} onClick={() => { setView(!view) }} >
              {view ? 'Grid' : 'List'}
            </Button>
            <Button className='customer-btn' startIcon={<AddCircleOutline color='primary' />} onClick={() => { setId(1) }} >
              Create
            </Button>
            <Button className='customer-btn' startIcon={<Link color='primary' />} onClick={() => { setOpen(true) }} >
              Gallery
            </Button>
            {view ? <>{flag ? element.length > 0 ? <GalleryView element={element} /> : <NoData /> : <TableLoader />}</> : <><TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "none" } }}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Image</TableCell>
                    <TableCell>Created At</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <TableRow
                      key={row._id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell>
                        <IconButton title="Edit" onClick={() => { handleEdit(row) }}>
                          <Edit color='primary' />
                        </IconButton>

                        <IconButton title="Delete" onClick={() => { handleDelete(row._id) }}>
                          <Delete color="warning" />
                        </IconButton>
                      </TableCell>
                      <TableCell>
                        <ImageBox image={row.image} />
                      </TableCell>
                      <TableCell>{moment(row.createdAt).format('LLLL')}</TableCell>
                    </TableRow>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <TableLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
              {/* Mobile View */}
              <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "block" } }} >
                <Table size="small" aria-label="simple table">
                  <TableBody>
                    {flag ? element.length > 0 ? element.map((row) => (
                      <React.Fragment key={row._id}>
                        <TableRow
                          sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                          <TableCell>
                            <Card sx={{ borderRadius: '18px' }} >
                              <CardMedia
                                component="img"
                                sx={{ width: '100%', objectFit: 'unset', borderRadius: '18px', border: 1 }}
                                image={row.image}
                                alt={row.name}
                              />
                              <Box>
                                <CardContent>
                                  <Grid container spacing={2}>
                                    <Grid item xs={12} sm={12}>
                                      <IconButton title="Edit" onClick={() => { handleEdit(row) }}>
                                        <Edit color="primary" />
                                      </IconButton>
                                      <IconButton title="Delete" onClick={() => { handleDelete(row._id) }}>
                                        <Delete color="warning" />
                                      </IconButton>
                                    </Grid>
                                  </Grid>
                                </CardContent>
                              </Box>
                            </Card>
                          </TableCell>
                        </TableRow>
                      </React.Fragment>
                    )) : <TableRow>
                      <TableCell colSpan="12">
                        <NoData />
                      </TableCell>
                    </TableRow> : <TableRow>
                      <TableCell colSpan="12">
                        <MobileLoader />
                      </TableCell>
                    </TableRow>}
                  </TableBody>
                </Table>
              </TableContainer>
            </>}
          </CardContent>
        </Card>
      </Grid>
      <Dialog
        open={data.open}
        onClose={handleClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <EditGallery allvalue={data} setId={handleClose} />
      </Dialog>
      <Dialog
        open={open}
        onClose={handleCloseBox}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <UrlForm setId={handleCloseBox} />
      </Dialog>
    </Grid>
  )
}

export default GalleryList
