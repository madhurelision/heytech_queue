import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Button, Box, Typography, IconButton, } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import '../../Registration/register.css'
import { tableStyles } from '../Dashboard/styles'
import { BookingButton } from "components/common";
import { Close } from '@mui/icons-material'

const UrlForm = ({ setId }) => {

  const [data, setData] = useState({
    url: '',
  })

  const [err, setErr] = useState({})
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.open)
  const loading = useSelector(state => state.auth.loader)

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])


  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    // var check = serviceValidate(data)
    // setErr(check)

    // if (Object.keys(check).length > 0) return
    console.log('formData', data)

  }

  const classes = tableStyles()

  return (
    <Grid>
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid className="creatPop" item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                    Gallery Url
                  </Typography>
                  <Box className="crossIcon">
                    <IconButton onClick={() => { setId() }}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="url" type="text" value={data.url} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Url" />
                {err.url && (<FormError data={err.url}></FormError>)}
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }} >
              <Grid item xs={6} align="left">
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId() }}>Close</BookingButton>
              </Grid>
              <Grid item xs={6} align="right">
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Find</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default UrlForm
