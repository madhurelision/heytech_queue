import React, { useState, useContext, useEffect } from 'react'
import { Grid, TableContainer, Table, TableBody, TableHead, TableCell, TableRow, Paper, useTheme, useMediaQuery } from '@mui/material'
import { useSelector, useDispatch } from 'react-redux'
import moment from 'moment';
import { getShopDaily } from 'redux/action'
import { ChartCustom } from '../UsableComponent/CustomChart/ChartCustom';

const DailyVisitor = () => {

  const dispatch = useDispatch()
  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const [queue, setQueue] = useState({
    start_date: moment(new Date()).subtract(7, 'days').toDate(),
    end_date: new Date(),
  });
  const dailyData = useSelector((state) => state.auth.shopDaily);

  useEffect(() => {

    if (dailyData.length == 0) {
      dispatch(getShopDaily({ startDate: queue.start_date, endDate: queue.end_date }))
    } else {
      setElement(dailyData.booking)
      setFlag(true)
    }

  }, [dailyData])

  return (
    <> <ChartCustom data={{
      labels: element.map((cc) => moment(cc.date).format('LL')),
      datasets: [
        {
          type: 'bar',
          label: 'Accepted',
          backgroundColor: 'rgb(0, 128, 0)',
          borderColor: 'white',
          borderWidth: 2,
          fill: false,
          data: element.map((cc) => cc.value.verify),
        },
        {
          type: 'bar',
          label: 'Not Accepted',
          backgroundColor: 'rgb(255, 0, 0)',
          borderColor: 'white',
          borderWidth: 2,
          fill: false,
          data: element.map((cc) => cc.value.notVerify),
        },
        {
          type: 'bar',
          label: 'Total',
          backgroundColor: 'rgb(75, 192, 192)',
          data: element.map((cc) => cc.value.total),
          borderColor: 'white',
          borderWidth: 2,
        },
        {
          type: 'bar',
          label: 'Completed',
          backgroundColor: 'rgb(255, 99, 132)',
          borderColor: 'white',
          borderWidth: 2,
          fill: false,
          data: element.map((cc) => cc.value.completed),
        },
        {
          type: 'bar',
          label: 'Expired',
          backgroundColor: 'rgb(0, 0, 255)',
          borderColor: 'white',
          borderWidth: 2,
          fill: false,
          data: element.map((cc) => cc.value.expired),
        },
      ]
    }
    } type="line" title="Daily Visitor" />
    </>
  )
}

export default DailyVisitor