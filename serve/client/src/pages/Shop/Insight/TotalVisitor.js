import React, { useState, useContext, useEffect } from 'react'
import { Grid, TableContainer, Table, TableBody, TableHead, TableCell, TableRow, Paper, useTheme, useMediaQuery } from '@mui/material'
import { useSelector, useDispatch } from 'react-redux'
import { getShopTotal } from 'redux/action'
import { ChartCustom } from '../UsableComponent/CustomChart/ChartCustom'

const TotalVisitor = () => {

  const dispatch = useDispatch()
  const [element, setElement] = useState({
    total:0, verify:0, notVerify:0, completed:0, expired:0
  })
  const [flag, setFlag] = useState(false)
  const totalData = useSelector((state) => state.auth.shopTotal);

  useEffect(() => {

    if (totalData.length == 0) {
      dispatch(getShopTotal())
    } else {
      setElement(totalData.booking)
      setFlag(true)
    }

  }, [totalData])

  return (
    <>
      <ChartCustom data={
        {
          labels: ["Total", "Accepted", "Not Accepted", "Completed", "Expired"],
          datasets: [
            {
              label: '# of Votes',
              data: [element.total, element.verify, element.notVerify, element.completed, element.expired],
              backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
              ],
              borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
              ],
              borderWidth: 1,
            },
          ]
        }
      } type="doughnut" title="Total Visitor" /> 
    </>
  )
}

export default TotalVisitor