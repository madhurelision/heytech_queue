import React, { useState, useContext, useEffect } from 'react'
import { Grid, TableContainer, Table, TableBody, TableHead, TableCell, TableRow, Paper, useTheme, useMediaQuery } from '@mui/material'
import { getMyColor, SeatForm } from 'config/KeyData';
import { useSelector } from 'react-redux'
import moment from 'moment';
import SocketContext from 'config/socket';
import { tableStyles } from '../Dashboard/styles';
import { ChartCustom } from '../UsableComponent/CustomChart/ChartCustom'
import '../Queue/queue.css';

const WaitingTimeChart = () => {

  const [booking, setBooking] = useState([])
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('md'));
  const profileData = useSelector((state) => state.auth.profile);
  const context = useContext(SocketContext)

  useEffect(() => {

    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
    })

    context.socket.on('userDisconnect', (data) => {
      console.log('Socket Err', data)
    })

    context.socket.on('shopQueueCount', data => {
      //console.log('shop Sockebokit Data', data.val)
      if (data.data == profileData.user_information) {
        setBooking(data.val.booking)
      }
    })

    context.socket.on('disconnect', () => {
      console.log('socket is disconnect')
    })

    return () => {
      context.socket.removeListener(['shopQueueCount'])
    }

  }, [])

  useEffect(() => {

    if (Object.keys(profileData).length > 0) {
      context.socket.emit('shopQueueGet', profileData.user_information)
    }

  }, [profileData])

  const classes = tableStyles()


  return (
    <>
      {Object.keys(profileData).length > 0 ? <ChartCustom data={
        {
          //labels: SeatForm(profileData.seat).map((cc) => cc),
          datasets: SeatForm(profileData.seat).map((cc) => {
            return {
              label: cc,
              data: booking.filter((dd) => parseInt(dd.seat) == cc).map((ss) => {
                return {
                  x: moment(ss.approximate_date).calendar(),
                  y: ss.size
                }
              }),
              borderColor: getMyColor(),
              borderWidth: 2,
            }
          })
        }
      } type="line" title="Waiting Time" label={{
        scales: {
          xAxes: [
            {
              type: "time",
              time: {
                displayFormats: {
                  quarter: 'MMM YYYY'
                }
              }
            }
          ],
        },
        maintainAspectRatio: false,
        plugins: {
          legend: {
            position: 'bottom',
          },
          title: {
            display: true,
            text: "Waiting Time",
          },
        },
      }} /> : <></>}
    </>
  )
}

export default WaitingTimeChart