import React, { useContext, useEffect, useState } from 'react'
import { Grid, Card, CardContent, Button } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { getShopDaily, getShopTotal } from 'redux/action'
import { tableStyles } from '../Dashboard/styles'
import moment from 'moment'
import { Refresh } from '@mui/icons-material'
import SocketContext from 'config/socket'
import { SHOP_TOTAL } from 'redux/action/types';
import DailyVisitor from './DailyVisitor'
import TotalVisitor from './TotalVisitor'
import WaitingTimeChart from './WaitingTimeChart'

const Insight = () => {

  const dispatch = useDispatch()
  const classes = tableStyles()

  const context = useContext(SocketContext)
  const profileData = useSelector((state) => state.auth.profile);
  const loading = useSelector((state) => state.auth.loader)


  useEffect(() => {

    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
    })

    context.socket.on('userDisconnect', (data) => {
      console.log('Socket Err', data)
    })


    context.socket.on('shopTotalCount', data => {
      //console.log('shop Sockebokit Data', data.val)
      if (data.data == profileData.user_information) {
        dispatch({ type: SHOP_TOTAL, payload: { data: data.val } })
      }
    })

    context.socket.on('disconnect', () => {
      console.log('socket is disconnect')
    })

    return () => {
      context.socket.removeListener(['shopCount'])
    }
  }, [])

  const handleRefresh = () => {
    dispatch(getShopTotal())
    dispatch(getShopDaily({
      startDate: moment(new Date()).subtract(7, 'days').toDate(),
      endDate: new Date(),
    }))
    if (Object.keys(profileData).length > 0) {
      context.socket.emit('shopQueueGet', profileData.user_information)
    }
  }

  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card>
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12}>
              <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh} >
                  Refresh
                </Button>
              </Grid>
              <Grid item xs={12} sm={4} md={4} >
                <TotalVisitor />
              </Grid>
              <Grid item xs={12} sm={8} md={8} >
                <DailyVisitor />
              </Grid>
              <Grid item xs={12} sm={12} md={12} >
                <WaitingTimeChart />
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default Insight
