import React from 'react';
import { useRecoilState } from 'recoil';
import { globalPackageState } from 'store';
import { Grid } from '@mui/material';
import { rupeeIndian } from 'config/KeyData';

const Confirmation = ({
  name,
  email,
  address_1,
  address_2,
  city,
  country,
  state,
  pincode,
  product_id,
  pay_type
}) => {

  const [catalogData, setCatalogData] = useRecoilState(globalPackageState);


  const billingListItems = [
    {
      id: 0,
      label: 'Full name:',
      value: `${name}`,
    },
    {
      id: 1,
      label: 'Email:',
      value: email,
    },
    {
      id: 2,
      label: 'Address:',
      value: `${address_1},${address_2}, ${city}, ${state}, ${country}, ${pincode}`,
    },
  ];

  return (
    <>
      {billingListItems.map(({ id, label, value }) => (
        <Grid item xs={12} sm={12} md={6} key={id}>
          <div>{label}</div>
          <div>{value}</div>
        </Grid>
      ))}
      {catalogData.filter((cc) => cc._id == product_id).map((cc) => (
        <React.Fragment key={cc.name}>
          <Grid item xs={12} sm={12} md={6}>
            <div>Product Title</div>
            <div>{cc.name}</div>
          </Grid>
          <Grid item xs={12} sm={12} md={6}>
            <div>Product Price</div>
            <div>
              {/* {'INR' + ' '} */}
              &#8377;
              {rupeeIndian.format(cc.price)}
            </div>
          </Grid>
        </React.Fragment>
      ))}
    </>
  )
}

export default Confirmation