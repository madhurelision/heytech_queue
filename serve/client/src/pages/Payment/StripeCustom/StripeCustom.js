import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { toast } from 'react-toastify';
import 'bootstrap/dist/css/bootstrap.min.css';
import ReactCreditCards from '../ReactCreditCard/ReactCreditCards';
import { shopPackagePayment } from 'redux/action';
import { Grid } from '@mui/material'

const getDropList = () => {
  const year = new Date().getFullYear();
  return (
    Array.from(new Array(50), (v, i) =>
      <option key={i} value={year + i}>{year + i}</option>
    )
  );
};

const StripeForm = ({ main, pid, close }) => {


  const dispatch = useDispatch();
  const Loading = useSelector((state) => state.auth.loader);
  const openVal = useSelector((state) => state.auth.paymentDone);

  const [element, setElement] = useState({
    cvc: '',
    message: '',
    expYear: '',
    expMonth: '',
    cardNumber: '',
    formProcess: false,
    name: '',
    focus: ''
  })
  const packId = pid

  useEffect(() => {
    loadStripe(main)
  }, [])

  useEffect(() => {

    if (openVal) {
      close()
    }

  }, [openVal])

  const loadStripe = (main) => {

    if (!window.document.getElementById('stripe-script')) {
      var s = window.document.createElement("script");
      s.id = "stripe-script";
      s.type = "text/javascript";
      s.src = "https://js.stripe.com/v2/";
      s.onload = () => {
        console.log('Stripe', window['Stripe'])
        window['Stripe'].setPublishableKey(main);
      }
      window.document.body.appendChild(s);
    }
  }

  const handleChange = (e) => {
    setElement({ ...element, [e.target.name]: e.target.value })
  }

  const pay = (e) => {

    e.preventDefault();

    setElement({ ...element, formProcess: true })

    window.Stripe.card.createToken({
      number: element.cardNumber,
      exp_month: element.expMonth,
      exp_year: element.expYear,
      cvc: element.cvc,
      name: packId.name,
      //email: packId?.email,
      //phone: packId?.mobile,
      address_line1: packId?.address_1,
      address_line2: packId?.address_2,
      address_city: packId?.city,
      address_state: packId?.state,
      address_country: 'IN',
      address_zip: packId?.pincode,
    }, (status, response) => {


      if (status === 200) {

        console.log('stripe response', response)

        dispatch(shopPackagePayment({
          pay_id: response.id,
          ...packId
        }))
        setElement({ ...element, formProcess: false })

      } else {
        toast.error(response.error.message)
        setElement({ ...element, formProcess: false })
      }
    });
  }

  return (
    <>
      <Grid item xs={12} md={12} sm={12}>
        <ReactCreditCards name={packId?.name} number={element.cardNumber} expiry={element.expMonth + '/' + element.expYear} cvc={element.cvc} focused={element.focus} />
      </Grid>
      <Grid item xs={12} md={12} sm={12}>
        <div className="form-group">
          <label>CARD NUMBER</label>
          <div className="input-group">
            <input type="text" className="form-control contact-input" placeholder="Valid Card Number" name="cardNumber" maxLength="16" onChange={handleChange} onFocus={e => setElement({ ...element, focus: e.target.name })} />
            <span className="input-group-addon"><span className="fa fa-credit-card"></span></span>
          </div>
        </div>
      </Grid>
      <Grid item xs={12} md={4} sm={12}>
        <div className="form-group">
          <label><span className="hidden-xs">EXPIRATION</span> DATE</label>
          <select name="expMonth" className="form-control contact-input" onChange={handleChange} onFocus={e => setElement({ ...element, focus: e.target.name })}>
            <option value="">Select Month</option>
            <option value="01">01</option>
            <option value="02">02</option>
            <option value="03">03</option>
            <option value="04">04</option>
            <option value="05">05</option>
            <option value="06">06</option>
            <option value="07">07</option>
            <option value="08">08</option>
            <option value="09">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
          </select>
        </div>
      </Grid>
      <Grid item xs={12} md={4} sm={12}>
        <div className="form-group">
          <label></label>
          <select name="expYear" className="form-control contact-input" onChange={handleChange} onFocus={e => setElement({ ...element, focus: e.target.name })}>
            <option value="">Select Year</option>
            {getDropList()}
          </select>
        </div>
      </Grid>
      <Grid item xs={12} md={4} sm={12}>
        <div className="form-group">
          <label>CVV CODE</label>
          <input type="text" name="cvc" className="form-control contact-input" placeholder="CVC" maxLength="4" onChange={handleChange} onFocus={e => setElement({ ...element, focus: e.target.name })} />
        </div>
      </Grid>
      <Grid item xs={12} md={12} sm={12}>
        {(element.formProcess || Loading) ? (
          <span className="btn btn-warning btn-lg btn-block">Please wait...</span>
        ) : (
          <button type="button" className="btn btn-warning btn-lg btn-block" onClick={(e) => pay(e)}>Process payment</button>
        )}
      </Grid>
    </>
  )
}


const StripeCustom = ({ id, close }) => {

  const StripeDetails = useSelector(state => state.auth.stripe)

  let content = <div>No Payment Method Found</div>;

  if (Object.keys(StripeDetails).length) {
    if (StripeDetails.key === "") {
      content = <div>No Payment Method Found</div>;
    } else {
      content = <StripeForm main={StripeDetails.key} pid={id} close={close} />
    }
  }



  return (
    <>
      {content}
    </>
  )
}

export default StripeCustom

