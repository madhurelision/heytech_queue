import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Button, Typography, IconButton, Box } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import { tableStyles } from '../Shop/Dashboard/styles'
import { keyLengthValidation } from 'config/KeyData';
import { Close, TaskAlt } from '@mui/icons-material'
import { BookingButton } from "components/common";
import Confirmation from './Confirmation'
import StripeCustom from './StripeCustom/StripeCustom'
import { LOADERCLOSE } from 'redux/action/types'

const Payment = ({ id, type, close }) => {

  const classes = tableStyles()
  const loading = useSelector(state => state.auth.loader)
  const [data, setData] = useState({
    name: '',
    email: '',
    mobile: '',
    address_1: '',
    address_2: '',
    city: '',
    pincode: '',
    country: '',
    state: '',
    product_id: id,
    pay_type: type
  })
  const dispatch = useDispatch()

  const [err, setErr] = useState({})
  const [view, setView] = useState(1)

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    var cc = checkForm(data)
    setErr(cc)

    if (Object.keys(cc).length > 0) return
    setView(2)
  }

  const handleClose = () => {
    setView(4)
    dispatch({
      type: LOADERCLOSE
    })
  }

  const handleBack = (id) => {
    setView(id)
  }

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={12} md={12}>
        <Card>
          <CardContent>
            <form>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Typography className='title_head' flexGrow={1} variant="h5" component="div" align="center">
                      {(view == 1 || view == 2) ? 'Billing Details' : view == 3 ? 'Card Details' : view == 4 ? 'Payment Successful' : 'Billing Details'}
                    </Typography>
                    <Box>
                      <IconButton onClick={() => { close() }}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                {view == 1 && <>
                  <Grid item xs={12} sm={12} md={6}>
                    <TextField name="name" type="text" value={data.name} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Name" />
                    {err.name && (<FormError data={err.name}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={6}>
                    <TextField name="email" type="email" value={data.email} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Email" />
                    {err.email && (<FormError data={err.email}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={6}>
                    <TextField name="mobile" type="tel" inputProps={{
                      maxLength: keyLengthValidation.mobile
                    }} value={data.mobile} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Mobile Number" />
                    {err.mobile && (<FormError data={err.mobile}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={6}>
                    <TextField name="address_1" type="text" value={data.address_1} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Address 1" />
                    {err.address_1 && (<FormError data={err.address_1}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={6}>
                    <TextField name="address_2" type="text" value={data.address_2} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Address 2" />
                    {err.address_2 && (<FormError data={err.address_2}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={6}>
                    <TextField name="state" type="text" value={data.state} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="State" />
                    {err.state && (<FormError data={err.state}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={6}>
                    <TextField name="country" type="text" value={data.country} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Country" />
                    {err.country && (<FormError data={err.country}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={6}>
                    <TextField name="city" type="text" value={data.city} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="City" />
                    {err.city && (<FormError data={err.city}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={6}>
                    <TextField name="pincode" type="text" value={data.pincode} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Pincode" />
                    {err.pincode && (<FormError data={err.pincode}></FormError>)}
                  </Grid>
                </>}
                {view == 2 && <Confirmation {...data} />}
                {view == 3 && <StripeCustom id={data} close={handleClose} />}
                {view == 4 && <Grid item xs={12} sm={12} md={12}>
                  <div><i className='fa fa-check payment-sucess-icon'></i></div>
                  <Box display="flex" alignItems="center">
                    <Typography className='title_head' flexGrow={1} variant="h5" component="div" align="center">
                      <IconButton>
                        <TaskAlt fontSize='large' />
                      </IconButton>
                      Thank You!!!!
                    </Typography>
                    <Box>
                    </Box>
                  </Box>
                  {/* <button className='payment-btn' onClick={close} >Close</button> */}
                  {/* <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={close}>Close</BookingButton> */}
                </Grid>}
              </Grid>
              <Grid container spacing={2} sx={{ marginTop: '5px' }} >
                {view == 1 && <Grid item xs={12} align="right">
                  <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Pay</BookingButton>
                </Grid>}
                {view == 2 && <Grid item xs={12} align="left">
                  <BookingButton className='formSubmit-btn' type="button" disabled={loading ? true : false} variant="contained" onClick={() => { handleBack(1) }}>Back</BookingButton>
                  <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { handleBack(3) }}>Next</BookingButton>
                </Grid>}
              </Grid>
            </form>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default Payment

const checkForm = (data) => {
  let errors = {}

  if (!data.name) {
    errors['name'] = "Name is Required";
  }

  if (!data.email) {
    errors['email'] = "Email is Required";
  }

  if (typeof data["email"] !== "undefined") {
    //regular expression for email validation
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    if (!pattern.test(data["email"])) {
      errors["email"] = "*Please enter valid email-ID.";
    }
  }

  if (!data.mobile) {
    errors['mobile'] = "Mobile is Required";
  }

  if (!data.address_1) {
    errors['address_1'] = "Address 1 is Required";
  }

  if (!data.address_2) {
    errors['address_2'] = "Address 2 is Required";
  }

  if (!data.country) {
    errors['country'] = "Country is Required";
  }
  if (!data.state) {
    errors['state'] = "State is Required";
  }
  if (!data.city) {
    errors['city'] = "City is Required";
  }
  if (!data.pincode) {
    errors['pincode'] = "Pincode is Required";
  }

  return errors
}