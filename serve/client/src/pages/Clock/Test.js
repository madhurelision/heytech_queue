import { CircularProgress, useTheme } from '@mui/material';
import { CircularTimer } from 'pages/Shop/Dashboard/styles';
import React, { useState, useRef, useEffect } from 'react'
import './clock.css'

function secondsToHms(d) {
  d = Number(d);
  var h = Math.floor(d / 3600);
  var m = Math.floor(d % 3600 / 60);
  var s = Math.floor(d % 3600 % 60);

  var hDisplay = h > 0 ? h : "";
  var mDisplay = m > 0 ? m : "";
  var sDisplay = s > 0 ? s : "";
  //return hDisplay + ':' + mDisplay + ':' + sDisplay;
  return (hDisplay > 9 ? hDisplay : '0' + hDisplay) + ':' +
    (mDisplay > 9 ? mDisplay : '0' + mDisplay) + ':'
    + (sDisplay > 9 ? sDisplay : '0' + sDisplay)
}

const Test = ({ maxTime, maxDate }) => {

  // We need ref in this, because we are dealing
  // with JS setInterval to keep track of it and
  // stop it when needed
  const theme = useTheme()
  const Ref = useRef(null);

  // The state for our timer
  const [timer, setTimer] = useState('00:00:00');
  const [progress, setProgress] = useState()


  const getTimeRemaining = (e) => {
    //console.log('result first date', Date.parse(e), Date.parse(new Date()))
    const total = Date.parse(e) - Date.parse(new Date());
    const seconds = Math.floor((total / 1000) % 60);
    const minutes = Math.floor((total / 1000 / 60) % 60);
    const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
    const days = Math.floor((total / (1000 * 60 * 60 * 24)))
    //console.log('hour', hours, 'min', minutes, 'sec', seconds)
    return {
      total, hours, minutes, seconds, days
    };
  }


  const startTimer = (e) => {
    let { total, hours, minutes, seconds, days }
      = getTimeRemaining(e);
    if (total >= 0) {

      // update the timer
      // check if less than 10 then we need to
      // add '0' at the begining of the variable
      setTimer(
        days + ':' +
        (hours > 9 ? hours : '0' + hours) + ':' +
        (minutes > 9 ? minutes : '0' + minutes) + ':'
        + (seconds > 9 ? seconds : '0' + seconds)
      )
    }
  }


  const clearTimer = (e) => {
    //console.log('main', e)
    setTimer('00:00:00:00');
    if (Ref.current) clearInterval(Ref.current);
    const id = setInterval(() => {
      startTimer(e);
    }, 1000)
    Ref.current = id;
  }

  const getDeadTime = () => {
    let deadline = new Date(maxDate);
    // console.log('deadline time', deadline, 'maxTime', maxTime, 'max Date', maxDate)
    // console.log('deadline second', deadline.getSeconds(), ' a', maxTime)
    deadline.setSeconds(deadline.getSeconds());
    return deadline;
  }

  // We can use useEffect so that when the component
  // mount the timer will start as soon as possible

  // We put empty array to act as componentDid
  // mount only
  useEffect(() => {
    clearTimer(getDeadTime());
    return () => {
      window.clearInterval(Ref.current)
      Ref.current = null
      setTimer('00:00:00')
    }
  }, []);

  // Another way to call the clearTimer() to start
  // the countdown is via action event from the
  // button first we create function to be called
  // by the button
  const onClickReset = () => {
    clearTimer(getDeadTime());
  }

  // const getPercent = () => {
  //   let now = new Date();
  //   let diff = new Date(maxDate);
  //   //return 100 - ((Date.parse(new Date(maxDate)) - Date.parse(new Date())) / new Date(maxDate));
  //   console.log('%%%%%%', Math.round((diff - now) * 100 / now))
  //   return Math.round(diff / now * 100)
  //   //return Math.round(((diff / new Date(maxDate)) * 100000000 * 2) / 100)
  // }

  const classes = CircularTimer()

  return (
    <div className={`timer-div ${theme.palette.mode == 'dark' ? 'timer-dark' : 'timer-light'}`} >
      <div><h1 className="timer-clock">{timer}</h1></div>
      <div className="timer-name">Days Hours Min Sec</div>
      {/* <div className="circular-div">
        <CircularProgress variant="determinate" value={getPercent()} size={180} />
      </div> */}
    </div>
  )
}

export default Test
