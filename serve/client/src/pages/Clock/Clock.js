import React, { useState } from 'react'
import { CircularProgress } from '@mui/material'
import moment from 'moment'
import './clock.css'

const Clock = ({ time, maxtime }) => {

  const getTime = () => {
    return moment.utc(time).format('mm.ss');
  }

  const getPercent = () => {
    return 100 - ((maxtime - time) / maxtime * 100);
  }

  return (
    <div className="mainClock">
      <div className="sample">
        <div className="clock">
          {getTime()}
          <div className="circular">
            <CircularProgress mode="determinate" value={getPercent()} size={5} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Clock
