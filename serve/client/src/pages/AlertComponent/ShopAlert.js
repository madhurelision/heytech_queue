import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import Alert from 'pages/AlertComponent/Alert';

const ShopAlert = () => {

  const profileData = useSelector((state) => state.auth.profile);
  const auth = useSelector((state) => state.auth.isAuthenticated)

  return (
    <>
      {auth == true && localStorage.getItem('type') == 'shop' && <>
        {Object.keys(profileData).length > 0 && profileData.upi_id == null && <div>
          <Alert style={{ textAlign: 'center' }} show={true} variant={'danger'} data={'Upi Id is Required Please Add in Profile'} />
        </div>}
      </>}
    </>
  )
}

export default ShopAlert