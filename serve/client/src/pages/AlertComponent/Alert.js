import React from 'react';
import AlertComp from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';

const Alert = ({ show, variant, data, style }) => {

  const history = useNavigate();

  return (
    <AlertComp show={show} key={variant} variant={variant}>
      <div className="d-flex justify-content-around">
        <div>{data}</div>
        <Button onClick={() => { history('/shop/profile') }} variant="outline-danger">
          Add
        </Button>
      </div>
    </AlertComp>
  )
}

export default Alert