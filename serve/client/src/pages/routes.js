import React, { lazy, Suspense, useEffect, createContext, useState } from 'react';
import { Route, Routes, Navigate, Outlet, useLocation } from 'react-router-dom';
import Loader from '../components/Loader/index';
import usePageTracking from '../utils/hooks/use-page-tracking';
import getTheme from '../utils/hooks/theme';
import analytics from 'config/analytics';
import { ThemeProvider } from '@mui/material/styles';
import useRouteEmit from 'config/useRouteEmit';
import ToolbarMain from 'components/Appbar/ToolbarMain'
import SocketContext, { socket, socketAuth } from 'config/socket';
import { useSelector } from 'react-redux';
import Package from './Shop/Package/Package';
import SliderForm from './Shop/UsableComponent/SliderForm';
import TestingForm from './Shop/UsableComponent/TestingForm';
import Inventory from './Shop/Inventory/Inventory';
import UpiForm from './Shop/PaymentPage/UpiForm';
import CustomerPackage from './Customer/Package/CustomerPackage';
import CustomerPTab from './Customer/Profile/CustomerPTab';
import ShopAlert from './AlertComponent/ShopAlert';
import ProfileTab from './Shop/Profile/ProfileTab';
import Insight from './Shop/Insight/Insight';
const ErrorPage = lazy(() => import('./404ErrorPage'));
const Landing = lazy(() => import('./Landing/index'));
const UserPage = lazy(() => import('./UserPage/index'));
const SearchPage = lazy(() => import('./SearchPage/index'));
const Registration = lazy(() => import('./Registration/Registration'));
const Login = lazy(() => import('./Login/Login'));
const CustomerRedirect = lazy(() => import('./RedirectPage/CustomerRedirect'));
const ShopRedirect = lazy(() => import('./RedirectPage/ShopRedirect'));
const ProtectedRoute = lazy(() => import('./ProtectedRoute/ProtectedRoute'));
const Home = lazy(() => import('./Shop/AppBar/Home'));
const Dashboard = lazy(() => import('./Shop/Dashboard/Dashboard'));
const CustomerHome = lazy(() => import('./Customer/AppBar/CustomerHome'));
const CustomerBookingList = lazy(() => import('./Customer/Booking/CustomerBookingList'));
const CustomerProfile = lazy(() => import('./Customer/Profile/CustomerProfile'));
const Profile = lazy(() => import('./Shop/Profile/Profile'));
const ServicePage = lazy(() => import('./Shop/Services/ServicePage'));
const HistoryList = lazy(() => import('./Shop/History/HistoryList'));
const CustomerHistory = lazy(() => import('./Customer/History/CustomerHistory'));
const BookingDetailPage = lazy(() => import('./BookingDetailPage/BookingDetailPage'));
const ShopQueue = lazy(() => import('./Shop/Queue/ShopQueue'));
const GalleryPage = lazy(() => import('./Shop/Gallery/GalleryPage'));
const MobileRedirect = lazy(() => import('./RedirectPage/MobileRedirect'));
const Whatsapp = lazy(() => import('./Whatsapp/Whatsapp'));
const EmployeePage = lazy(() => import('./Shop/ShopEmployee/EmployeePage'));
const EmployeeLogin = lazy(() => import('./Login/EmployeeLogin'));
const EmployeeHome = lazy(() => import('./Employee/AppBar/EmployeeHome'));
const StepperForm = lazy(() => import('./Registration/StepperForm'));
const CustomTabs = lazy(() => import('./Employee/UsableComponent/CustomTab'));
const EmployeeRedirect = lazy(() => import('./RedirectPage/EmployeeRedirect'));
const ShopDashboard = lazy(() => import('./Shop/Dashboard/ShopDashboard'));
const NewHome = lazy(() => import('./NewView/HomePage'));
const NewSearchPage = lazy(() => import('./NewView/SearchPage'))
const About = lazy(() => import('./NewView/AboutPage'));
const Contact = lazy(() => import('./NewView/ContactPage'));
const Banner = lazy(() => import('./Banner/Banner'));

// const VideoCall = lazy(() => import('./VideoCall/index'));

export const ToggleContext = createContext()

const Main = ({ children }) => {
  return (
    <>
      {children}
      <Outlet />
    </>
  )
}


analytics.track('buttonClicked')


const App = () => {

  let location = useLocation()
  const [show, setShow] = useState(localStorage.getItem('theme') ? localStorage.getItem('theme') : 'dark')

  useEffect(() => {

    analytics.page()

  }, [location])

  useEffect(() => {

    if (show == 'dark') {
      document.body.classList.remove('toogle-dark')
    } else {
      document.body.classList.add('toogle-dark')
    }
  }, [show])

  useRouteEmit()

  return (
    <ToggleContext.Provider value={{ show, setShow }}>
      <ThemeProvider theme={getTheme(show)}>
        <ToolbarMain>
          <ShopAlert />
          <Suspense fallback={<Loader />}>
            <Routes>
              <Route path="/" element={<SocketContext.Provider value={{ socket, socketAuth }}><Main /></SocketContext.Provider>}>
                <Route index element={<Landing />} />
                <Route path="search" element={<SearchPage />} />
                <Route path="user/:id" element={<UserPage />} />
                <Route path="register/:id" element={<StepperForm />} />
                {/* <Route path="servicePage/:id" element={<ServicePageRegister />} /> */}
                <Route path="booking/:id" element={<BookingDetailPage />} />
                <Route path="payment/:id" element={<UpiForm />} />
                <Route path="employeelogin" element={<EmployeeLogin />} />
                <Route path="whatsapp" element={<Whatsapp />} />
                <Route path="banner" element={<Banner />} />
                {/* <Route path="test" element={<SliderForm />} /> */}
                <Route path="user/newform/:id" element={<TestingForm />} />

                {/* Shop */}
                <Route path="shoplogin/:id" element={<ShopRedirect />} />
                <Route path="shop" element={<ProtectedRoute><Home /></ProtectedRoute>} >
                  <Route index element={<ShopDashboard />} />
                  <Route path="service" element={<ServicePage />} />
                  {/* <Route path="queue" element={<ShopQueue />} /> */}
                  {/* <Route path="appoinment" element={<Dashboard />} /> */}
                  <Route path="profile" element={<ProfileTab />} />
                  <Route path="gallery" element={<GalleryPage />} />
                  <Route path="history" element={<HistoryList />} />
                  <Route path="package" element={<Package />} />
                  <Route path="employee" element={<EmployeePage />} />
                  <Route path="inventory" element={<Inventory />} />
                  <Route path="insight" element={<Insight />} />
                  {/* <Route path="check" element={<ShopDashboard />} /> */}
                  <Route path="*" element={<Navigate to='/' />} />
                </Route>

                {/* Customer Login */}
                <Route path="userlogin/:id" element={<CustomerRedirect />} />
                <Route path="customer" element={<ProtectedRoute><CustomerHome /></ProtectedRoute>} >
                  <Route index element={<CustomerBookingList />} />
                  <Route path="profile" element={<CustomerPTab />} />
                  <Route path="history" element={<CustomerHistory />} />
                  <Route path="*" element={<Navigate to='/' />} />
                </Route>

                {/* Employee Login */}
                <Route path="loginemployee/:id" element={<EmployeeRedirect />} />
                <Route path="employee" element={<ProtectedRoute><EmployeeHome /></ProtectedRoute>} >
                  <Route index element={<CustomTabs />} />
                  {/* <Route path="booking" element={<EmployeeBooking />} /> */}
                  <Route path="*" element={<Navigate to='/' />} />
                </Route>
                {/* <Route path="step" element={<CustomTabs />} /> */}

                {/* Mobile Reedirect */}
                <Route path="mobile/:id/:token" element={<MobileRedirect />} />

                {/* Test */}
                <Route path="global" element={<NewHome />} />
                <Route path="global/search/:id" element={<NewSearchPage />} />
                <Route path='about' element={<About />} />
                <Route path='contact' element={<Contact />} />
                <Route path='error' element={<ErrorPage />} />
              </Route>
            </Routes>
            {/* <Routes>
              <Route path="/room" element={<VideoCall />} />
              <Route path="/room/:id" element={<VideoCall />} />
            </Routes> */}
          </Suspense>
        </ToolbarMain>
      </ThemeProvider>
    </ToggleContext.Provider>
  );
};

export default App;
