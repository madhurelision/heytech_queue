import React, { useState } from 'react'
import { TextField, Grid, Card, Button, Typography, CardContent, } from '@mui/material'
import FormError from 'pages/Registration/FormError';
import { keyLengthValidation } from 'config/KeyData';
import { BookingButton } from "components/common";

const Whatsapp = () => {

  const [data, setData] = useState({
    link: 'https://heybarber.herokuapp.com',
    number: ''
  })
  const [err, setErr] = useState({})

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    var check = checkWhatsapp(data)
    setErr(check)
    console.log('check', check)

    if (Object.keys(check).length > 0) return

    var a = document.createElement('a')
    a.target = "_blank"
    a.href = `https://wa.me/+91${data.number}?text=${data.link}`
    a.click()
    console.log('link', `https://wa.me/${data.number}?text=${data.link}`)
  }

  return (<Grid>
    <Card>
      <CardContent>
        <form>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={12}>
              <Typography variant="h5" component="div" align="center">
                Whatsapp
              </Typography>
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              <TextField
                fullWidth
                InputLabelProps={{
                  shrink: true
                }}
                type="text"
                value={data.link}
                label="Link"
                name="link"
                onChange={handleChange}
              />
              {err.link && (<FormError data={err.link}></FormError>)}
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              <TextField name="number" type="tel" inputProps={{
                maxLength: keyLengthValidation.mobile
              }} value={data.number} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} label="Mobile Number"
                onChange={handleChange}
              />
              {err.number && (<FormError data={err.number}></FormError>)}
            </Grid>
          </Grid>
          <Grid container spacing={2} sx={{ marginTop: '5px' }} >
            <Grid item xs={12} align="right">
              <BookingButton type="button" variant="contained" onClick={() => { handleSubmit() }} >Submit</BookingButton>
            </Grid>
          </Grid>
        </form>
      </CardContent>
    </Card>
  </Grid>
  )
}

export default Whatsapp


const checkWhatsapp = (data) => {
  let errors = {}

  if (!data.link) {
    errors['link'] = "Link is Required";
  }

  if (!data.number) {
    errors['number'] = "Mobile Number is Required";
  }

  if (data.number) {
    if (!data["number"].match(/^[0-9]{10}$/)) {
      errors["number"] = "Please enter valid mobile no.";
    }
  }
  return errors
}