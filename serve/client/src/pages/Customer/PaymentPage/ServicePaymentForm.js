import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Dialog, DialogContent, Typography, DialogTitle, Box, IconButton, useTheme, useMediaQuery, MenuItem, FormControl, InputLabel, Select, Checkbox, CardMedia } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import 'pages/Registration/register.css'
import { Close } from '@mui/icons-material'
import { BookingButton } from "components/common";
import TableData from 'pages/Shop/PaymentPage/TableData'
import { customerMakePackagePay } from 'redux/action/paymentAction'
import { getCustomerPackagePaymentList } from 'redux/action'

const ServicePaymentForm = ({ allvalue, setId }) => {

  const [data, setData] = useState({
    customer_id: allvalue.customer,
    type: '',
    data: allvalue.service,
    order_tax: 0,
    discount: 0,
    row: allvalue.service_id,
    qty: 1
  })

  const [shop, setShop] = useState(allvalue.shop_id)

  const [err, setErr] = useState({})
  const dispatch = useDispatch()
  const paymentData = useSelector((state) => state.auth.customerPackageList)
  const mainVal = useSelector(state => state.auth.created)
  const loading = useSelector(state => state.auth.loader)
  const [element, setElement] = useState([])

  useEffect(() => {

    if (paymentData.length == 0) {
      dispatch(getCustomerPackagePaymentList(0, 10))
    } else {
      setElement(paymentData.payment)
    }

  }, [paymentData])

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])


  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    var check = paymentValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    var check = { ...data, service_data: data.data, booking_id: allvalue._id }
    dispatch(customerMakePackagePay(check))
  }

  const handleBtn = () => {
    const deepLink = `upi://pay?pa=${shop?.upi_id}&pn=${shop?.shop_name}&am=${sumTotal(data.order_tax, data.discount)}&cu=INR`
    var a = document.createElement('a')
    a.target = "_blank"
    a.href = deepLink
    a.click()
  }

  const handleRemove = (id) => {
    const ss = data.data.filter((cc) => cc._id !== id)
    setData({ ...data, data: ss, service: '' })
  }

  const handleServiceC = (main) => {
    var mft = data.data
    const ss = data.data.filter((cc) => cc._id == main._id)

    if (ss.length == 0) {
      mft.push({ ...main, stock: 1 })
      setData({ ...data, data: mft })
    } else {
      var atV = [...data.data]
      console.log('val', atV[0])
      atV[0] = { ...atV[0], stock: ss[0]?.stock + 1 }
      setData({ ...data, data: atV, qty: ss[0]?.stock + 1 })
    }
  }

  const sumTotal = (order_tax, ss) => {
    const GTotal = data.data.reduce((cc, val) => { cc = cc + (val.stock * val.price); return cc }, 0)
    var tax, discount
    if (parseInt(order_tax) !== null) {
      if (GTotal * parseInt(order_tax) > 0) {
        tax = parseInt((GTotal * parseInt(order_tax)) / 100)
      } else {
        tax = 0
      }
    }
    if (parseInt(ss) !== null) {
      if (GTotal * parseInt(ss) > 0) {
        discount = parseInt((GTotal * parseInt(ss)) / 100)
      } else {
        discount = 0
      }
    }
    return GTotal + tax - discount
  }

  const getTax = (order_tax) => {
    const GTotal = data.data.reduce((cc, val) => { cc = cc + (val.stock * val.price); return cc }, 0)
    var tax = 0
    if (parseInt(order_tax) !== null) {
      if (GTotal * parseInt(order_tax) > 0) {
        tax = parseInt((GTotal * parseInt(order_tax)) / 100)
      } else {
        tax = 0
      }
    }
    return tax
  }

  const filterMethod = () => {
    return element.filter((main) => new Date(main.endDate) > new Date());
  };

  return (
    <Grid >
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid className="creatPop" item xs={12} sm={12} md={12}>
                <Box display="flex" alignItems="center">
                  <Typography className='title_head loginHead' flexGrow={1} variant="h4" component="h2" align="center">
                    Payment Form
                  </Typography>
                  <Box className="crossIcon">
                    <IconButton onClick={() => { setId() }}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={12} md={12}>
                    <TableData arr={data.data} handleRemove={handleRemove} check={true} />
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <FormControl fullWidth>
                      <InputLabel id="demo-simple-select-label" shrink={true} >Payment Type</InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={data.type}
                        label="Payment Type"
                        name="type"
                        onChange={handleChange}
                      >
                        {(shop.upi_id !== null && shop.upi_id !== undefined) && <MenuItem value={'UPI'}>{'UPI'}</MenuItem>}
                        {(filterMethod().length > 0) && <MenuItem value={'Package'}>{'Package'}</MenuItem>}
                      </Select>
                    </FormControl>
                    {err.type && (<FormError data={err.type}></FormError>)}
                  </Grid>
                  <Grid item xs={6} sm={6} md={6}>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                      <div>Grand Total:  &#8377;{sumTotal(data.order_tax, data.discount)}</div>
                      <div>Tax: &#8377;{getTax(data.order_tax)}</div>
                    </div>
                  </Grid>
                  <Grid item xs={6} sm={6} md={6} align="right">
                    {(data.type == '' || data.type == 'Package') && <BookingButton disabled={loading} type="button" variant="contained" onClick={() => { handleSubmit() }} >Pay</BookingButton>}
                    {(data.type == 'UPI') && <BookingButton disabled={loading} type="button" variant="contained" onClick={() => { handleBtn() }} >Pay</BookingButton>}
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={12} md={12}>
                    <Card className="card-book-highlight" sx={{ borderRadius: '18px' }} onClick={() => { handleServiceC(data.row) }} >
                      <CardMedia
                        component="img"
                        sx={{ width: '100%', objectFit: 'unset', borderRadius: '18px', border: 1 }}
                        image={data?.row?.image}
                        alt={data?.row?.name}
                      />
                      <Box>
                        <CardContent>
                          <Grid container spacing={2}>
                            <Grid item xs={12} sm={12} md={12}>
                              <div className='customer-font'>{data?.row?.name}</div>
                            </Grid>
                          </Grid>
                        </CardContent>
                      </Box>
                    </Card>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default ServicePaymentForm

const paymentValidate = (data) => {
  let errors = {}

  if (!data.customer_id) {
    errors["customer_id"] = "Customer is Required"
  }

  if (!data.type) {
    errors["type"] = "Type is Required"
  }

  if (!data.data) {
    errors["service"] = "Service is Required"
  }

  if (data.data.length == 0) {
    errors["service"] = "Service is Required"
  }

  return errors
}