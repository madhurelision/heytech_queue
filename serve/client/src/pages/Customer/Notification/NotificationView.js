import React, { useEffect, useState } from 'react';
import { Popper, Fade, Paper, Typography, ClickAwayListener, Card, CardContent, Grid, Chip, Stack, List, ListItem, ListItemAvatar, Avatar, ListItemText, Divider, CardActions, Button, IconButton, useTheme, useMediaQuery } from '@mui/material'
import { getAllCustomerNotification, getAllViewNotification } from 'redux/action';
import TableLoader from 'pages/Shop/UsableComponent/TableLoader';
import { useDispatch, useSelector } from 'react-redux';
import { Close } from '@mui/icons-material';
import { BookingButton } from "components/common";
import { LoadingButton } from '@mui/lab'
import { useNavigate } from 'react-router-dom';

const NotificationView = ({ open, anchorEl, handleClose }) => {


  const [element, setElement] = useState([]);
  const [val, setVal] = useState(10)
  const [item, setItem] = useState([]);
  const [flag, setFlag] = useState(false);
  const notifyData = useSelector((state) => state.auth.customerNotification);
  const nData = useSelector((state) => state.auth.customerNotificationCount);
  const total = useSelector((state) => state.auth.customerNotificationTotal);
  const cted = useSelector((state) => state.auth.notify);
  const loading = useSelector((state) => state.auth.loader);
  const dispatch = useDispatch();
  const theme = useTheme();
  const matchesXs = useMediaQuery(theme.breakpoints.down('sm'));
  const history = useNavigate()

  useEffect(() => {

    if (notifyData.length == 0) {
      dispatch(getAllCustomerNotification(0, 20))
    } else {
      setElement(notifyData.notification)
      setItem(notifyData.notification.slice(0, val))
      setFlag(true)
    }

    if (cted) {
      dispatch(getAllCustomerNotification(0, element.length > 0 ? element.length : 20))
    }

  }, [notifyData, cted])

  const handleMark = () => {
    dispatch(getAllViewNotification())
  }

  const fetchMoreData = () => {
    dispatch(getAllCustomerNotification(element.length, 10))
    const n = item.length;
    setVal(n + 10);
  };

  return (<Popper
    id={open ? 'virtual-element-popper' : undefined}
    open={open}
    anchorEl={anchorEl}
    transition
    role={undefined}
    placement={matchesXs ? 'bottom' : 'bottom-end'}
    style={{ zIndex: '100' }}
  >
    {({ TransitionProps }) => (
      <Fade {...TransitionProps} timeout={350}>
        <Paper>
          <ClickAwayListener onClickAway={handleClose}>
            <Card>
              <CardContent>
                <Grid container direction="column" spacing={2}>
                  <Grid item xs={12}>
                    <div style={{ padding: '10px 0px 0' }}>
                      <Grid container alignItems="center" justifyContent="space-between">
                        <Grid item>
                          {/* <Stack direction="row" spacing={2}>
                            <Typography variant="subtitle1">All Notification <Chip size="small" label={nData} /></Typography>
                            <Chip size="small" label={nData} />
                          </Stack> */}
                          <Typography variant="subtitle1">All Notification <Chip size="small" label={nData} /></Typography>
                        </Grid>
                        <Grid item>
                          {element.length > 0 && <div className='notify-btn' disabled={loading} onClick={() => { handleMark() }}>
                            Mark as all read
                          </div>}
                        </Grid>
                        <Grid item sx={{ display: { xs: 'block', sm: 'block', md: 'none' } }}>
                          <IconButton title="Close" disabled={loading} onClick={() => { handleClose(0) }}>
                            <Close />
                          </IconButton>
                        </Grid>
                      </Grid>
                    </div>
                  </Grid>
                  <Grid item xs={12} sx={{
                    height: '100%',
                    maxHeight: 'calc(100vh - 205px)',
                    overflowX: 'hidden'
                  }}>
                    <List>

                      {flag ? item.length > 0 ? item.map((row) => (
                        <React.Fragment key={row._id}>
                          {/* <ListItem alignItems="center">
                            <ListItemAvatar>
                              <Avatar alt={"Notify"} src={row?.user_id?.image_link}>
                                {row?.user_id?.first_name.slice(0, 1)}
                              </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={<Typography variant="subtitle1">{row.message}</Typography>} />
                          </ListItem>
                          <Divider /> */}
                          <ListItem alignItems="flex-start" className={`${row.view == true ? 'notify-fade' : ''}`} onClick={() => {
                            history('/customer')
                          }}>
                            <ListItemAvatar>
                              <Avatar alt={"Notify"} src={row.image}>
                                {row?.name.slice(0, 1)}
                              </Avatar>
                            </ListItemAvatar>
                            <ListItemText
                              primary={row?.name}
                              secondary={
                                <React.Fragment>
                                  <Typography
                                    sx={{ display: 'inline' }}
                                    component="span"
                                    variant="body2"
                                    color="text.primary"
                                  >
                                    {row?.service_name}
                                  </Typography>
                                  {" — "} {row?.message}
                                </React.Fragment>
                              }
                            />
                          </ListItem>
                          <Divider variant="inset" component="li" />
                        </React.Fragment>
                      )) : <ListItem alignItems="center">No Notifications</ListItem> : <ListItem alignItems="center"><TableLoader /></ListItem>}
                    </List>

                    {element.length > 0 && <Grid container spacing={2}>
                      {total != element.length && <Grid item xs={12} sm={12} md={12} textAlign="center">
                        <LoadingButton loading={loading}
                          loadingIndicator="Loading..."
                          variant="outlined" disabled={loading} onClick={fetchMoreData} >View More</LoadingButton>
                      </Grid>}
                    </Grid>}
                  </Grid>
                </Grid>

              </CardContent>
            </Card>
          </ClickAwayListener>
        </Paper>
      </Fade>
    )}
  </Popper>)
};

export default NotificationView;
