import React, { useEffect, useState, useCallback } from 'react'
import { TextField, Grid, Card, Chip, Typography, Dialog, DialogContent, Checkbox, FormControlLabel, CardContent, RadioGroup, Radio, FormLabel, FormControl, InputLabel, Select, MenuItem, DialogTitle, IconButton, Box, useTheme, FormGroup, useMediaQuery } from '@mui/material'
import '../../../pages/Registration/register.css'
import FormError from '../../Registration/FormError';
import { useSelector, useDispatch } from 'react-redux'
import Geocode from 'react-geocode';
import { geocodeByPlaceId, getLatLng } from 'react-google-places-autocomplete';
import Location from '../../Shop/UsableComponent/Location';
import { keyLengthValidation, Languages, Seat } from 'config/KeyData';
import NewImageCropper from 'pages/ImageCropper/NewImageCropper';
import { Close } from '@mui/icons-material';
import VerifyOtp from '../../Shop/Profile/VerifyOtp';
import { BookingButton } from "components/common";
import { customerMobileCreateOtp, customerProfileUpdate } from 'redux/action';
Geocode.setApiKey('AIzaSyCqiCKV0cm-EpHp2hev4XkD5wG9JGxFbVs');
Geocode.setLocationType("ROOFTOP");
Geocode.enableDebug();

const EditProfile = ({ allvalue }) => {

  const theme = useTheme()
  const matches = useMediaQuery(theme.breakpoints.up('md'));
  const mainCheckU = useSelector((state) => state.auth.updated)
  const loading = useSelector(state => state.auth.loader)
  const dispatch = useDispatch()

  const [data, setData] = useState({
    name: allvalue.name,
    image_link: '',
    email: allvalue.email,
    oldimage: allvalue.image_link,
    location: allvalue.location,
    lattitude: allvalue.lattitude,
    longitude: allvalue.longitude,
    mobile: allvalue.mobile,
  })

  const [err, setErr] = useState({})
  const [btn, setBtn] = useState(true)
  const [view, setView] = useState(false)
  const [show, setShow] = useState(false)
  const [mb, setMb] = useState(true)
  const [mobile, setMobile] = useState(allvalue.mobile)
  const [image, setImage] = useState(allvalue.image_link)
  const [open, setOpen] = useState({
    open: false,
    name: '',
    image: ''
  })

  useEffect(() => {
    console.log('Hellp Updated', mainCheckU)
    if (mainCheckU == true) {
      console.log('Hellp Updated', mainCheckU)
      setView(true)
    }

  }, [mainCheckU])

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
    setBtn(false)
  }

  const onHandleImage = (e) => {

    if (e.target.files == null || e.target.files == undefined) {
      return
    }

    const file = Math.round((e.target.files[0].size / 1024 * 1024));
    console.log('Image Size', e.target.files[0].size, 'Image Size Mb', file)

    if (file > 5242880) {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      setErr({ ...err, [e.target.name]: 'Image Size Should be less than 5 Mb' })
      return
    }

    const VIDEO_FORMATS = ["video/mp4", "video/mkv"];
    console.log('Image Console', e.target.name, e.target.files)
    console.log('Main', document.getElementsByName(e.target.name)[0].value)
    if (!VIDEO_FORMATS.includes(e.target.files[0].type)) {
      setData({ ...data, [e.target.name]: e.target.files[0] })
      setErr({ ...err, [e.target.name]: '' })
      setOpen({ ...open, open: true, name: e.target.name, image: URL.createObjectURL(e.target.files[0]) })
      setBtn(false)
    } else {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      return
    }
  }


  const CrooppedImageN = (imgss) => {
    console.log('Cropped Image Called', imgss)
    var ext = (imgss.type == "image/jpeg") ? '.jpg' : (imgss.type == "image/png") ? '.png' : '.jpg'
    const values = blobToFile(imgss, open.name + ext)
    console.log('Cropped Image Called', values)
    setData({ ...data, [open.name]: values, oldimage: URL.createObjectURL(values) })
    setOpen({ ...open, open: false })
    setImage(URL.createObjectURL(values))
  }

  const blobToFile = (theBlob, fileName) => {
    return new File([theBlob], fileName, { lastModified: new Date().getTime(), type: theBlob.type })
  }

  const handleClose = () => {
    document.getElementsByName(open.name)[0].value = ''
    setData({ ...data, [open.name]: '' })
    setOpen({ ...open, open: false })
  }

  const handleSubmit = () => {
    var check = customerValidation({ ...data })
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)

    var formData = new FormData()
    formData.append('name', data.name)
    formData.append('email', data.email)
    formData.append('image', data.image_link)
    formData.append('oldimage', data.oldimage)
    formData.append('location', data.location)
    formData.append('lattitude', data.lattitude)
    formData.append('longitude', data.longitude)
    formData.append('mobile', data.mobile)
    dispatch(customerProfileUpdate(formData))
  }


  const handleBoxLocation = useCallback((val) => {
    geocodeByPlaceId(val.value.place_id).then((ss) => {
      getLatLng(ss[0]).then((dd) => {
        console.log('Location seleted', { name: val.label, latitude: dd.lat, longitude: dd.lng })
        setData({ ...data, location: val.label, longitude: dd.lng, lattitude: dd.lat })
        setErr({ ...err, location: '' })
        setBtn(false)
      })
    }).catch((err) => {
      console.log('Location SS Data Errorr', err)
    })
  }, [data])

  const handleLiveLocation = useCallback(() => {
    if ("geolocation" in navigator) {
      console.log("Available");
    } else {
      console.log("Not Available");
    }

    navigator.geolocation.getCurrentPosition(function (position) {
      Geocode.fromLatLng(position.coords.latitude, position.coords.longitude).then(
        (response) => {
          const address = response.results[0].formatted_address;
          console.log(address, 'latitude', position.coords.latitude, 'longitude', position.coords.longitude);
          setData({ ...data, location: address, longitude: position.coords.longitude, lattitude: position.coords.latitude })
          setErr({ ...err, location: '' })
          setBtn(false)
        },
        (error) => {
          console.error(error);
        }
      );
    },
      function (error) {
        console.error("Error Code = " + error.code + " - " + error.message);
      });
  }, [data])

  const handleMobile = (e) => {
    setMobile(e.target.value)
    setErr({ ...err, [e.target.name]: '' })
    setMb(false)
  }

  const AddMobile = () => {

    var check = checkMobile({ mobile: mobile })
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formmobile', mobile)

    if (allvalue.mobile == mobile) {
      console.log('mobile match', allvalue.mobile, mobile)
      setMb(true)
      return
    }
    dispatch(customerMobileCreateOtp(allvalue?._id, { mobile: mobile, resend: 'single' }))
  }

  const handleViewClose = () => {
    setView(false)
  }


  const handleShow = (val) => {
    setShow(val)
  }

  return (
    <Grid>
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12}>
                <Typography className='title_head' variant="h4" component="h2" align="center">
                  Edit Profile
                </Typography>
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <Typography variant="h6" component="h6" align="left">
                  General Information
                </Typography>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  type="text"
                  value={data.name}
                  label="Customer Name"
                  name="name"
                  onChange={handleChange}
                />
                {err.name && (<FormError data={err.name}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }}
                  type="email"
                  value={data.email}
                  label="Email"
                  name="email"
                  disabled={(data.email == null || data.email == undefined || data.email == "") ? false : true}
                  onChange={handleChange}
                />
                {err.email && (<FormError data={err.email}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <TextField name="location" type="text" value={data.location} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} disabled label="Location" />
                {err.location && (<FormError data={err.location}></FormError>)}
              </Grid>
              <Location handleLocation={handleLiveLocation} handleDropdown={handleBoxLocation} />
              <Grid item xs={12} sm={12} md={4}>
                <TextField name="mobile" type="tel" inputProps={{
                  maxLength: keyLengthValidation.mobile
                }} value={mobile} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleMobile} label="Mobile Number" />
                {err.mobile && (<FormError data={err.mobile}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={2}>
                <BookingButton type="button" variant="contained" disabled={(mobile == null || mobile == undefined || mobile == '' || mobile.length < 10 || mb)} onClick={() => { AddMobile() }} fullWidth >Add Mobile</BookingButton>
              </Grid>

              <Grid item xs={12} sm={12} md={12}>
                <Typography variant="h6" component="h6" align="left">
                  Profile Images
                </Typography>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField id="imageFile" type="file"
                  fullWidth
                  InputLabelProps={{
                    shrink: true
                  }} name="image_link" label="Profile Image" onChange={(e) => { onHandleImage(e) }} />

                {err.image_link && (<FormError data={err.image_link}></FormError>)}
              </Grid>
              <Grid item xs={12} sm={12} md={6} sx={(theme) => ({
                [theme.breakpoints.up('xs')]: {
                  textAlign: 'center'
                },
                [theme.breakpoints.up('sm')]: {
                  textAlign: 'center'
                }
              })} >
                <img src={image} className="image_tag" />
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }} >
              <Grid item xs={12} align={matches ? 'right' : 'center'}>
                <BookingButton type="button" disabled={(loading || btn == true) ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Update</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
      <Dialog open={open.open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description" maxWidth="md" fullWidth>
        <DialogContent>
          <NewImageCropper img={open.image} click={CrooppedImageN} ratio={16 / 9} />
        </DialogContent>
      </Dialog>
      <Dialog open={view}
        disableEscapeKeyDown={true}
        onClose={handleViewClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description" maxWidth="md" fullWidth>
        <VerifyOtp allvalue={{ _id: allvalue?._id, mobile: mobile }} setId={handleViewClose} status={false} handleView={handleShow} />
      </Dialog>
    </Grid>
  )
}

export default EditProfile

const customerValidation = (data) => {

  let errors = {}

  if (!data.oldimage) {
    errors['image_link'] = "Profile Picture is Required";
  }

  if (!data.name) {
    errors['name'] = "Customer Name is Required";
  }

  if (!data.email) {
    errors['email'] = "Email is Required";
  }

  if (data.email) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    if (!pattern.test(data["email"])) {
      errors["email"] = "Please enter valid email.";
    }
  }

  // if (!data.email) {
  //   if (!data.mobile) {
  //     errors['mobile'] = "Mobile Number is Required";
  //   }

  //   if (data.mobile) {
  //     if (!data["mobile"].match(/^[0-9]{10}$/)) {
  //       errors["mobile"] = "Please enter valid mobile no.";
  //     }
  //   }
  // }

  return errors
}

const checkMobile = (data) => {
  let errors = {}

  if (!data.mobile) {
    errors['mobile'] = "Mobile Number is Required";
  }

  if (data.mobile) {
    if (!data["mobile"].match(/^[0-9]{10}$/)) {
      errors["mobile"] = "Please enter valid mobile no.";
    }
  }
  return errors
}