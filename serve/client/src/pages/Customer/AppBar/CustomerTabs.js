import React from 'react';
import { Badge, Grid, Typography, useTheme } from '@mui/material';
import { ContactMail, History, Info, Mail, ManageAccounts, Policy } from '@mui/icons-material';
import { useLocation, useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { tabStyles } from 'pages/Shop/Dashboard/styles';

const CustomerTabs = () => {

  const theme = useTheme()
  const classes = tabStyles(theme)
  const history = useNavigate();
  const location = useLocation()
  const countData = useSelector((state) => state.auth.customerTotalCount);

  return (
    <Grid id="fixedToolbar" className="shop-mobile-tab" container sx={{ color: theme.palette.mode == 'dark' ? 'white' : 'black', justifyContent: 'center', backdropFilter: 'blur(10px)' }} >
      {/* <Grid item xs={2} sm={2} md={2} textAlign="center" className={(location.pathname == "/search") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/search')
      }}>
        <Policy fontSize="large" />
        <Typography className={classes.font + ' title_head'}>Search</Typography>
      </Grid>
      <Grid item xs={2} sm={2} md={2} textAlign="center" className={(location.pathname == "/customer") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/customer')
      }}>
        <Badge color="secondary" badgeContent={countData}>
          <Mail fontSize="large" />
        </Badge>
        <Typography className={classes.font + ' title_head'} >Current Appointment</Typography>
      </Grid>
      <Grid item xs={2} sm={2} md={2} textAlign="center" className={(location.pathname == "/customer/history") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/customer/history')
      }}>
        <History fontSize="large" />
        <Typography className={classes.font + ' title_head'} >History</Typography>
      </Grid>
      <Grid item xs={2} sm={2} md={2} textAlign="center" className={(location.pathname == "/about") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/about')
      }}>
        <Info fontSize="large" />
        <Typography className={classes.font + ' title_head'} >About</Typography>
      </Grid>
      <Grid item xs={2} sm={2} md={2} textAlign="center" className={(location.pathname == "/contact") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/contact')
      }}>
        <ContactMail fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Contact Us</Typography>
      </Grid> */}
      <div className={(location.pathname == "/search") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/search')
      }}>
        <Policy fontSize="large" />
        <Typography className={classes.font + ' title_head'}>Search</Typography>
      </div>
      <div className={(location.pathname == "/customer") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/customer')
      }}>
        <Badge color="secondary" badgeContent={countData}>
          <Mail fontSize="large" />
        </Badge>
        <Typography className={classes.font + ' title_head'} >Current Appointment</Typography>
      </div><div className={(location.pathname == "/customer/profile") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/customer/profile')
      }}>
        <ManageAccounts fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Profile</Typography>
      </div>
      <div className={(location.pathname == "/customer/history") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/customer/history')
      }}>
        <History fontSize="large" />
        <Typography className={classes.font + ' title_head'} >History</Typography>
      </div>
      <div className={(location.pathname == "/about") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/about')
      }}>
        <Info fontSize="large" />
        <Typography className={classes.font + ' title_head'} >About</Typography>
      </div>
      <div className={(location.pathname == "/contact") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/contact')
      }}>
        <ContactMail fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Contact Us</Typography>
      </div>
    </Grid>
  )
}

export default CustomerTabs