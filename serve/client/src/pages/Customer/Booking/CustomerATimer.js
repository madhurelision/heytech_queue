import NewTimer from 'pages/Shop/UsableComponent/NewTimer'
import ProgressTimer from 'pages/Shop/UsableComponent/ProgressTimer'
import React from 'react'

const CustomerATimer = ({ row, handleTimerClose }) => {
  return (
    <><ProgressTimer time={new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))} ma={row._id} func={handleTimerClose} show={true} />
      <NewTimer main={new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))} />
    </>
  )
}

export default CustomerATimer