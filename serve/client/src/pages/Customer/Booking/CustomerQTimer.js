import NewTimer from 'pages/Shop/UsableComponent/NewTimer'
import ProgressTimer from 'pages/Shop/UsableComponent/ProgressTimer'
import React from 'react'

const CustomerQTimer = ({ row, handleTimerClose }) => {
  return (
    <>
      {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) > new Date() && <>
        <ProgressTimer time={new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))} ma={row._id} func={handleTimerClose} show={true} />
        <NewTimer main={new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))} />
      </>}
      {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) < new Date() && new Date(row.approximate_date) > new Date() && <>
        <ProgressTimer time={new Date(row.approximate_date)} ma={row._id} func={handleTimerClose} show={true} />
        <NewTimer main={new Date(row.approximate_date)} />
      </>}
      {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) < new Date() && new Date(row.approximate_date) < new Date() && <>
        <ProgressTimer time={new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))} ma={row._id} func={handleTimerClose} show={true} />
        <NewTimer main={new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))} />
      </>}
    </>
  )
}

export default CustomerQTimer