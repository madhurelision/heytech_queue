import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Button, Typography, IconButton, Box } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError'
import '../../Registration/register.css'
import { customerVerifyCancelBooking } from 'redux/action'
import { keyLengthValidation } from 'config/KeyData'
import { BookingButton } from "components/common";
import { Close } from '@mui/icons-material'

const CancelBooking = ({ allvalue, setId }) => {

  const [otp, setOtp] = useState('')

  const [err, setErr] = useState({})
  const dispatch = useDispatch()
  const mainVal = useSelector(state => state.auth.open)
  const loading = useSelector(state => state.auth.loader)

  useEffect(() => {

    if (mainVal) {
      setId(0)
    }

  }, [mainVal])


  const handleChange = (e) => {
    setOtp(e.target.value)
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    if (otp == null || otp == undefined || otp == "") {
      setErr({ ...err, otp: 'Otp is Required' })
      return
    }
    console.log('formData', otp)
    dispatch(customerVerifyCancelBooking(allvalue?._id, { otp: otp }))
  }

  return (
    <Grid >
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={12} md={12} sm={12}>
                <Box display="flex" alignItems="center">
                  <Box flexGrow={1} >
                    <Typography className='title_head' variant="h4" component="h2" align="center">
                      Cancel Booking Otp Verify
                    </Typography>
                  </Box>
                  <Box>
                    <IconButton onClick={setId}>
                      <Close />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <TextField name="otp" type="text" value={otp} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} inputProps={{
                  maxLength: keyLengthValidation.otp
                }} onChange={handleChange} label="Otp" />
                {err.otp && (<FormError data={err.otp}></FormError>)}
              </Grid>
            </Grid>
            <Grid container spacing={2} sx={{ marginTop: '5px' }}>
              <Grid item xs={6} align='left'>
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { setId() }}>Close</BookingButton>
              </Grid>
              <Grid item xs={6} align="right">
                <BookingButton type="button" disabled={loading ? true : false} variant="contained" onClick={() => { handleSubmit() }}>Submit</BookingButton>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default CancelBooking
