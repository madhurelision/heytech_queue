import React from 'react'
import { Grid, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Rating, List, ListItemAvatar, ListItem, ListItemText, Typography, Avatar } from '@mui/material'
import moment from 'moment'
import { useLocation } from 'react-router'
import { DetailStyles } from 'pages/Shop/Dashboard/styles'
import Test from 'pages/Clock/Test'
import StatusBox from 'pages/Shop/UsableComponent/StatusBox'
import TooltipBox from 'pages/Shop/UsableComponent/TooltipBox'
import Testimonial from 'pages/Shop/UsableComponent/Testimonial'
import { getDateChange } from 'config/KeyData'

const CustomerAppointmentView = ({ item }) => {

  const classes = DetailStyles()
  const location = useLocation()

  return (
    <Grid container spacing={2} className={classes.grid}>
      {(location.pathname == '/customer') && <Grid item xs={12} md={10} sm={12}>
        {(item.waiting_verify == true && item.end_time !== null) ? <Test maxTime={parseInt(item.waiting_time) * 60} maxDate={new Date(item.end_time).setSeconds(parseInt((item.durationVal !== null && item.durationVal !== undefined) ? item.durationVal : 0))} /> : null}
      </Grid>}
      <Grid item xs={12} md={4} sm={12}>
        <TableContainer component={Paper}>
          <Table size="small" aria-label="simple table">
            <TableBody>
              <TableRow>
                <TableCell></TableCell>
                <TableCell></TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Size</TableCell>
                <TableCell>{(!item.size || item.size == null || item.size == undefined) ? '----' : item.size}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Number</TableCell>
                <TableCell>{(!item.mobile || item.mobile == null || item.mobile == undefined) ? '-----' : item.mobile}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Appointment Date</TableCell>
                <TableCell>{moment(item.appointment_date).format('LL')}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Appointment Time</TableCell>
                <TableCell>{moment(item.appointment_date).calendar()}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Waiting Time</TableCell>
                <TableCell>{(!item.waiting_number || item.waiting_number == null || item.waiting_number == undefined) ? '-------' : (item?.waiting_number < 60) ? item?.waiting_number + ' ' + item.waiting_key : getDateChange(item.waiting_number)}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Reach Time</TableCell>
                <TableCell>{(!item.end_time || item.end_time == null || item.end_time == undefined) ? '----' : moment(new Date(item.end_time).setSeconds(parseInt((item.durationVal !== null && item.durationVal !== undefined) ? item.durationVal : 0))).calendar()}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Shop Name</TableCell>
                <TableCell>{item?.shop_name}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Shop Email</TableCell>
                <TableCell>{item.shop_email}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Service Name</TableCell>
                <TableCell>{item?.service_id.name}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Seat</TableCell>
                <TableCell>{item?.seat}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Service Time</TableCell>
                <TableCell> {(item.queue == true) ? (item?.approximate_number < 60) ? item?.approximate_number + ' ' + item.approximate_key : getDateChange(item.approximate_number) : '------'}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Status</TableCell>
                <TableCell><StatusBox value={item.status} /></TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Description</TableCell>
                <TableCell sx={{ display: { xs: 'none', md: 'block', sm: 'none' } }}>
                  <TooltipBox title="hover" data={item.description} />
                </TableCell>
                <TableCell sx={{ display: { xs: 'block', md: 'none', sm: 'block' } }}>
                  {item.description}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      {(location.pathname == '/customer/history') && <Grid container width="100%" >
        <Grid item xs={12} md={4} sm={12} textAlign="center"></Grid>
        <Grid item xs={12} md={4} sm={12} textAlign="center">
          {item.review.length > 0 && item.user_review == true ? <List>
            <h2>Reviews</h2>
            {item.review.map((cc) => (
              (cc.shop_review == true) ? <ListItem alignItems="flex-start">
                <ListItemAvatar>
                  <Avatar alt={"Notify"} src={cc?.shop_image}>
                    {cc?.shop_name.slice(0, 1)}
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={cc?.shop_name}
                  secondary={
                    <React.Fragment>
                      <Typography
                        sx={{ display: 'inline' }}
                        component="span"
                        variant="body2"
                        color="text.primary"
                      >
                        <Rating name="read-only" value={cc.rating} readOnly />
                      </Typography>
                      {" — "} {cc?.description}
                    </React.Fragment>
                  }
                />
              </ListItem> : <ListItem alignItems="flex-start">
                <ListItemAvatar>
                  <Avatar alt={"Notify"} src={cc?.image}>
                    {cc?.user_name.slice(0, 1)}
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={cc?.user_name}
                  secondary={
                    <React.Fragment>
                      <Typography
                        sx={{ display: 'inline' }}
                        component="span"
                        variant="body2"
                        color="text.primary"
                      >
                        <Rating name="read-only" value={cc.rating} readOnly />
                      </Typography>
                      {" — "} {cc?.description}
                    </React.Fragment>
                  }
                />
              </ListItem>
            ))}
          </List> : <div style={{ textAlign: 'center' }}>No Review Found</div>}
        </Grid>
      </Grid>}
    </Grid>
  )
}

export default CustomerAppointmentView
