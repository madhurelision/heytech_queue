import React, { useCallback, useContext, useEffect, useState } from 'react'
import { Grid, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Typography, Card, CardContent, IconButton, Dialog, DialogTitle, DialogContent, Box, Collapse, Button, useTheme } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import TableLoader from 'pages/Shop/UsableComponent/TableLoader'
import NoData from 'pages/Shop/UsableComponent/NoData'
import StatusBox from 'pages/Shop/UsableComponent/StatusBox'
import DialogBox from 'pages/Shop/Dashboard/DialogBox'
import { tableStyles } from 'pages/Shop/Dashboard/styles'
import { customerCancelBooking, getCustomerCurrentBooking } from 'redux/action'
import moment from 'moment'
import CustomerAppointmentView from './CustomerAppointmentView'
import EmailBox from 'pages/Shop/UsableComponent/EmailBox'
import ProgressTimer from 'pages/Shop/UsableComponent/ProgressTimer'
import { MoreVert, Refresh, Close, Cancel, Password, Timelapse, KeyboardArrowDown, KeyboardArrowUp, Star, VisibilityOutlined, ViewList, GridView, Payment as PaymentIcon } from '@mui/icons-material'
import SocketContext, { socket, socketAuth } from 'config/socket'
import { CUSTOMER_COUNT, SOCKET_CUSTOMER_BOOKING_REMOVE, SOCKET_CUSTOMER_BOOKING_UPDATE, SOCKET_CUSTOMER_NOTIFICATION } from 'redux/action/types'
import TooltipBox from 'pages/Shop/UsableComponent/TooltipBox'
import CancelBooking from './CancelBooking'
import ListCard from 'pages/Shop/UsableComponent/ListCard'
import { ChangeNumber, getDateChange } from 'config/KeyData'
import MobileLoader from 'pages/Shop/UsableComponent/MobileLoader'
import NewTimer from 'pages/Shop/UsableComponent/NewTimer'
import '../../Shop/Queue/queue.css'
import InfiniteScroll from 'react-infinite-scroll-component'
import Geocode from 'react-geocode';
import ServicePaymentForm from '../PaymentPage/ServicePaymentForm'
import CustomerQTimer from './CustomerQTimer'
import CustomerATimer from './CustomerATimer'
Geocode.setApiKey('AIzaSyCqiCKV0cm-EpHp2hev4XkD5wG9JGxFbVs');
Geocode.enableDebug();


var DailogData
const CustomerBookingList = () => {

  const theme = useTheme()
  const [element, setElement] = useState([])
  const [box, setBox] = useState([])
  const [flag, setFlag] = useState(false)
  const [open, setOpen] = useState(false)
  const [view, setView] = useState(true)
  const [boxview, setBoxView] = useState('')
  const bookingData = useSelector((state) => state.auth.customerCurrentBooking)
  const moreData = useSelector(state => state.auth.customerMore)
  const customerData = useSelector((state) => state.auth.profile)
  const cted = useSelector((state) => state.auth.open)
  const loading = useSelector((state) => state.auth.loader)
  const dispatch = useDispatch()
  const classes = tableStyles()
  const [data, setData] = useState({
    open: false,
    _id: ''
  })
  const [book, setBook] = useState({
    open: false,
    data: {}
  })
  const [userD, setUserD] = useState({
    lattitude: '',
    longitude: '',
    address: ''
  })

  const [payment, setPayment] = useState({
    open: false,
    customer: '',
    service: [],
    _id: '',
    shop_id: '',
    service_id: ''
  })


  const context = useContext(SocketContext)


  useEffect(() => {

    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
    })

    context.socket.on('userDisconnect', (data) => {
      console.log('Socket Err', data)
    })

    context.socket.on('customerCount', (data) => {
      // console.log('Customer Count Data', data)
      dispatch({ type: CUSTOMER_COUNT, payload: data })
    })

    context.socket.on('customerBooking', (data) => {
      // console.log('Customer Booking', data)
      dispatch({ type: SOCKET_CUSTOMER_BOOKING_UPDATE, payload: [data] })
    })

    context.socket.on('customerBookingRemove', (data) => {
      //console.log('Customer Booking Remove', data)
      dispatch({ type: SOCKET_CUSTOMER_BOOKING_REMOVE, payload: data })
    })

    context.socket.on('notificationCount', (data) => {
      //console.log('notificationCount', data)
      dispatch({ type: SOCKET_CUSTOMER_NOTIFICATION, payload: data })
    })
    context.socket.on('disconnect', () => {
      console.log('socket is disconnect')
    })

    return () => {
      context.socket.removeListener(['customerCount', 'customerBooking', 'customerBookingRemove', 'notificationCount'])
    }

  }, [])


  useEffect(() => {

    if ("geolocation" in navigator) {
      console.log("Available");
    } else {
      console.log("Not Available");
    }

    navigator.geolocation.getCurrentPosition(function (position) {
      console.log('latitude', position.coords.latitude, 'longitude', position.coords.longitude)
      setUserD({ ...userD, lattitude: position.coords.latitude, longitude: position.coords.longitude })

      if (Object.keys(customerData).length > 0) {
        context.socket.emit('customerLiveLocation', { lattitude: position.coords.latitude, longitude: position.coords.longitude }, customerData._id)
      }

      Geocode.fromLatLng(position.coords.latitude, position.coords.longitude).then(
        (response) => {
          const address = response.results[0].formatted_address;
          console.log(address, 'latitude', position.coords.latitude, 'longitude', position.coords.longitude);
          setUserD({ ...userD, address: address, lattitude: position.coords.latitude, longitude: position.coords.longitude })
        },
        (error) => {
          console.error('Location Err', error);
        }
      );
    },
      function (error) {
        console.error("Error Code = " + error.code + " - " + error.message);
      });

  }, [])

  useEffect(() => {

    if (bookingData.length == 0) {
      dispatch(getCustomerCurrentBooking(0, 10))
    } else {
      setElement(bookingData.booking)
      setFlag(true)
    }

    if (cted) {

      setBook({ ...book, open: false })
    }

  }, [bookingData, cted])


  const handleDialogClose = () => {
    setOpen(false)
  }

  const handleDialogOpen = (item) => {
    DailogData = <CustomerAppointmentView item={item} />
    setOpen(true)
  }

  const handleRefresh = () => {
    dispatch(getCustomerCurrentBooking(0, 10))
  }

  const handleEdit = (val) => {
    setData({
      ...data,
      open: true,
      _id: val._id,
    })
  }

  const handleClose = () => {
    setData({ ...data, open: false })
  }

  const handleBookingClose = () => {
    setBook({ ...book, open: false })
  }

  const handleCancel = (item) => {
    dispatch(customerCancelBooking(item))
    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }
  }

  const handleTimerClose = useCallback((id) => {
    // console.log('Timer Close', id)
    var arr = [...box]
    arr.push(id)
    setBox(arr)
  }, [box.length]);

  const handleViewCard = (row) => {
    setBook({ ...book, data: row, open: true })
  }

  const handlePaymentM = (val) => {
    setPayment({
      ...payment,
      open: true,
      customer: val?.user_id?._id,
      shop_id: val?.shop_id,
      service: [{ ...val.service_id, stock: 1, mark: true }],
      service_id: val.service_id,
      _id: val._id,
    })
  }

  const handlePayClose = () => {
    setPayment({ ...payment, open: false })
  }

  return (
    <Grid id="mainCusBook" container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card className="innerCusBook">
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={2} md={2} >
                {/* <IconButton title="Refresh" disabled={loading} onClick={handleRefresh} >
                  <Refresh />
                </IconButton> */}
                <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh} >
                  Refresh
                </Button>
                {element.length > 0 && <Button className='customer-btn' startIcon={view ? <GridView color='primary' /> : <ViewList color='primary' />} onClick={() => { setView(!view) }} sx={{ display: { xs: "none", md: 'inline-flex', sm: 'inline-flex' } }}>
                  {view ? 'Grid' : 'List'}
                </Button>}
              </Grid>
              <Grid item xs={12} sm={8} md={8} >
                <Typography className='title_head loginHead' variant="h4" component="h2" align="center">
                  Booking List
                </Typography>
              </Grid>
              <Grid item xs={12} sm={2} md={2} ></Grid>
            </Grid>
            <InfiniteScroll className="searchServices"
              dataLength={element.length}
              next={() => { dispatch(getCustomerCurrentBooking(element.length, 10)) }}
              hasMore={moreData}
              loader={<h4 className="loading">Loading...</h4>}>
              <TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "block" } }}>
                {view == true && <Table sx={{ minWidth: 650 }} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell></TableCell>
                      <TableCell>View Time</TableCell>
                      <TableCell>Booking Id</TableCell>
                      <TableCell>Seat Number</TableCell>
                      <TableCell>Booking Type</TableCell>
                      <TableCell>Status</TableCell>
                      <TableCell>Timer</TableCell>
                      <TableCell>Reach Date</TableCell>
                      <TableCell>Reach Time</TableCell>
                      <TableCell>Appointment Date</TableCell>
                      <TableCell>Appointment Time</TableCell>
                      <TableCell>Waiting Time</TableCell>
                      <TableCell>Service Time</TableCell>
                      <TableCell>Total Waiting Time</TableCell>
                      <TableCell>Service Name</TableCell>
                      <TableCell>Size</TableCell>
                      <TableCell>Status</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {flag ? element.length > 0 ? element.map((row) => (
                      <React.Fragment key={row._id}>
                        <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                          <TableCell>
                            {
                              boxview == row._id ? <IconButton
                                title='Open'
                                aria-label="open"
                                size="small"
                                onClick={() => setBoxView('')}
                              >
                                <KeyboardArrowUp />
                              </IconButton> : <IconButton
                                title='Close'
                                aria-label="close"
                                size="small"
                                onClick={() => setBoxView(row._id)}
                              >
                                <KeyboardArrowDown />
                              </IconButton>
                            }
                          </TableCell>
                          <TableCell>
                            <IconButton title="Details" onClick={() => { handleDialogOpen(row) }} >
                              <VisibilityOutlined />
                            </IconButton>
                            {row.cancel_verify == false && <IconButton title="Cancel booking" onClick={() => { handleCancel(row._id) }} >
                              <Cancel />
                            </IconButton>}
                            {row.cancel_verify == true && <IconButton title="Cancel booking" onClick={() => { handleEdit(row) }} >
                              <Password />
                            </IconButton>}
                            {row.payment.length == 0 && <IconButton title="Payment" onClick={() => { handlePaymentM(row) }}>
                              <PaymentIcon />
                            </IconButton>}
                          </TableCell>
                          <TableCell>
                            {'#' + row?.bookingid}
                          </TableCell>
                          <TableCell>
                            {row?.seat}
                          </TableCell>
                          <TableCell>
                            {row.queue == true ? 'Queue' : 'Appointment'}
                          </TableCell>
                          <TableCell>
                            {row.waiting_verify == true ? <>
                              {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) > new Date() && <>
                                {'Waiting'}
                              </>}
                              {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) < new Date() && new Date(row.approximate_date) > new Date() && <>
                                {'Serving'}
                              </>}
                              {new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0)) < new Date() && new Date(row.approximate_date) < new Date() && <>
                                {'Finished'}
                              </>}
                            </> : '--------'}
                          </TableCell>
                          <TableCell>
                            {row.waiting_verify == true && row.queue == true ? <CustomerQTimer row={row} handleTimerClose={handleTimerClose} /> : ''}
                            {row.waiting_verify == true && row.queue == false ? <CustomerATimer row={row} handleTimerClose={handleTimerClose} /> : ''}
                          </TableCell>
                          <TableCell>
                            {(!row.end_time || row.end_time == null || row.end_time == undefined) ? '----' : moment(new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))).format('LL')}
                          </TableCell>
                          <TableCell>
                            {(!row.end_time || row.end_time == null || row.end_time == undefined) ? '----' : moment(new Date(row.end_time).setSeconds(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? row.durationVal : 0))).calendar()}
                          </TableCell>
                          <TableCell>{moment(row.appointment_date).format('LL')}</TableCell>
                          <TableCell>{moment(row.appointment_date).calendar()}</TableCell>
                          <TableCell>
                            {(!row.waiting_number || row.waiting_number == null || row.waiting_number == undefined) ? '-------' : (row?.waiting_number < 60) ? row?.waiting_number + ' ' + row.waiting_key : getDateChange(row.waiting_number)}
                          </TableCell>
                          <TableCell>
                            {(row.queue == true) ? (row?.approximate_number < 60) ? row?.approximate_number + ' ' + row.approximate_key : getDateChange(row.approximate_number) : '------'}
                          </TableCell>
                          <TableCell>
                            {row.queue == true ? <IconButton title={(row.count == 0) ? 'No Users in Queue' : `Total ${row.count} member in queue total waiting time is ${(row?.waiting_number < 60) ? parseInt((row.durationVal !== null && row.durationVal !== undefined) ? (row.durationVal + (row.waiting_number * 60)) / 60 : row.waiting_number) + ' ' + row.waiting_key : getDateChange(parseInt((row.durationVal !== null && row.durationVal !== undefined) ? (row.durationVal * (row.waiting_number * 60)) / 60 : row.waiting_number))}.`}>
                              <Timelapse />
                            </IconButton> : '---------'}
                          </TableCell>
                          <TableCell>{row?.service_id.name}</TableCell>
                          <TableCell >
                            {(!row.size || row.size == null || row.size == undefined) ? '----' : row.size}
                          </TableCell>
                          <TableCell>
                            <StatusBox value={row.status} />
                          </TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell className='customer-tablecell' colSpan={14}>
                            <Collapse in={boxview == row._id} timeout="auto" unmountOnExit>
                              <Box sx={{ margin: '3px' }}>
                                <Typography variant="h6" gutterBottom component="div" sx={{ fontSize: '1.1rem' }} >
                                  Shop Details
                                </Typography>
                                <Table size="small" aria-label="shopDetail">
                                  <TableHead>
                                    <TableRow>
                                      <TableCell></TableCell>
                                      <TableCell>Description</TableCell>
                                      <TableCell>Contact Number</TableCell>
                                      <TableCell>Shop Rating</TableCell>
                                      <TableCell>Shop Name</TableCell>
                                      <TableCell>Shop Email</TableCell>
                                    </TableRow>
                                  </TableHead>
                                  <TableBody>
                                    <TableRow>
                                      <TableCell></TableCell>

                                      <TableCell>
                                        <TooltipBox title="hover" data={row.description} />
                                      </TableCell>
                                      <TableCell >{ChangeNumber(row.mobile)}</TableCell>
                                      <TableCell>
                                        <Star color="warning" />{row.totalShop}
                                      </TableCell>
                                      <TableCell component="th" scope="row">
                                        {row?.shop_name}
                                      </TableCell>
                                      <TableCell>
                                        <EmailBox title={row.shop_email} />
                                      </TableCell>
                                    </TableRow>
                                  </TableBody>
                                </Table>
                              </Box>
                            </Collapse>
                          </TableCell>
                        </TableRow>
                      </React.Fragment>
                    )) : <TableRow>
                      <TableCell colSpan="14">
                        <NoData />
                      </TableCell>
                    </TableRow> : <TableRow>
                      <TableCell colSpan="14">
                        <TableLoader />
                      </TableCell>
                    </TableRow>}
                  </TableBody>
                </Table>}
                {view == false && <Table size="small" aria-label="simple table">
                  <Grid container spacing={2} id="bookingViewInside">
                    {flag ? element.length > 0 ? element.map((row) => (
                      <Grid
                        item xs={12} sm={6} md={4}
                        key={row._id}
                      >
                        <ListCard {...row} handleClick={handleViewCard} />
                      </Grid>
                    )) : <Grid item xs={12} sm={12} md={12}>
                      <NoData />
                    </Grid> : <Grid item xs={12} sm={12} md={12}>
                      <MobileLoader />
                    </Grid>}
                  </Grid>
                </Table>}
              </TableContainer>
              {/* Mobile View */}
              <TableContainer className='customBookingView' component={Paper} sx={{ display: { xs: "block", md: "none", sm: "none" } }} >
                <Table size="small" aria-label="simple table">
                  <TableBody id="bookingViewInside">
                    {flag ? element.length > 0 ? element.map((row) => (
                      <TableRow
                        key={row._id}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell className='customer-table'>
                          <ListCard {...row} handleClick={handleViewCard} />
                        </TableCell>
                      </TableRow>
                    )) : <TableRow>
                      <TableCell colSpan="12">
                        <NoData />
                      </TableCell>
                    </TableRow> : <TableRow>
                      <TableCell colSpan="12">
                        <MobileLoader />
                      </TableCell>
                    </TableRow>}
                  </TableBody>
                </Table>
              </TableContainer>
            </InfiniteScroll>
          </CardContent>
        </Card>
      </Grid>
      <Dialog
        disableEscapeKeyDown={true}
        open={data.open}
        onClose={handleClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <CancelBooking allvalue={data} setId={handleClose} />
      </Dialog>
      <DialogBox open={open} handleClose={handleDialogClose} title={'Appointment Detail'} data={DailogData} />
      <Dialog
        open={book.open}
        onClose={handleBookingClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <Grid>
          <Card>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Box flexGrow={1} >
                      <Typography className='title_head loginHead' variant="h4" component="h2" align="center">
                        Booking Details
                      </Typography>
                    </Box>
                    <Box>
                      <IconButton onClick={handleBookingClose}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                <Grid item xs={6} sm={6} md={6}>
                  <div>{book.data.description}</div>
                </Grid>
                <Grid item xs={6} sm={6} md={6} >
                  <div>Waiting Time</div>
                  <div className='customer-font'>
                    {(!book.data.waiting_number || book.data.waiting_number == null || book.data.waiting_number == undefined) ? '-------' : (book.data?.waiting_number < 60) ? book.data?.waiting_number + ' ' + book.data.waiting_key : getDateChange(book.data.waiting_number)}
                  </div>
                </Grid>
                <Grid item xs={6} sm={6} md={6} >
                  <div>Service Time</div>
                  <div className='customer-font'>
                    {(book.data.queue == true) ? (book.data?.approximate_number < 60) ? book.data?.approximate_number + ' ' + book.data.approximate_key : getDateChange(book.data.approximate_number) : '------'}
                  </div>
                </Grid>
                <Grid item xs={6} sm={6} md={6}>
                  {book.data.queue == true ? <div>{(book.data.count == 0) ? 'No Users in Queue' : `Total ${book.data.count} member in queue total waiting time is ${(book.data?.waiting_number < 60) ? parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? (book.data.durationVal + (book.data.waiting_number * 60)) / 60 : book.data.waiting_number) + ' ' + book.data.waiting_key : getDateChange(parseInt((book.data.durationVal !== null && book.data.durationVal !== undefined) ? (book.data.durationVal * (book.data.waiting_number * 60)) / 60 : book.data.waiting_number))}.`}</div> : <div>{'---------'}</div>}
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <span className='customer-fonts'>
                    {book.data.waiting_verify == true && book.data.queue == true ? <CustomerQTimer row={book.data} handleTimerClose={handleTimerClose} /> : ''}
                    {book.data.waiting_verify == true && book.data.queue == false ? <CustomerATimer row={book.data} handleTimerClose={handleTimerClose} /> : ''}
                  </span>
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <div>{book.data?.shop_name} <Star color="warning" />{book.data.totalShop}</div>
                  <div>{book.data.user_email}</div>
                  <div>{ChangeNumber(book.data.mobile)}</div>
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <IconButton title="Details" onClick={() => { handleDialogOpen(book.data) }} >
                    <VisibilityOutlined color="primary" />
                  </IconButton>
                  {book.data.cancel_verify == false && <IconButton title="Cancel booking" onClick={() => { handleCancel(book.data._id) }} >
                    <Cancel color="disabled" />
                  </IconButton>}
                  {book.data.cancel_verify == true && <IconButton title="Cancel booking" onClick={() => { handleEdit(book.data) }} >
                    <Password color="warning" />
                  </IconButton>}
                  {book.data?.payment?.length == 0 && <IconButton title="Payment" onClick={() => { handlePaymentM(book.data) }}><PaymentIcon /></IconButton>}
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Dialog>
      <Dialog
        open={payment.open}
        onClose={handlePayClose}
        fullWidth
        maxWidth="lg"
        aria-labelledby="responsive-dialog-title"
      >
        <ServicePaymentForm allvalue={payment} setId={handlePayClose} />
      </Dialog>
    </Grid>
  )
}

export default CustomerBookingList
