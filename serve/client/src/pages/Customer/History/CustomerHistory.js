import React, { useContext, useEffect, useState } from 'react'
import { Grid, TableCell, TableContainer, TableRow, Paper, TableBody, Table, TableHead, Typography, Card, CardContent, DialogTitle, DialogContent, Box, Dialog, IconButton, TextField, Collapse, Button } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import TableLoader from '../../Shop/UsableComponent/TableLoader'
import NoData from '../../Shop/UsableComponent/NoData'
import { getCustomerBooking, getCustomerQuertBooking, getCustomerQuertBookingUpdate } from 'redux/action'
import { tableStyles } from 'pages/Shop/Dashboard/styles'
import moment from 'moment'
import StatusBox from 'pages/Shop/UsableComponent/StatusBox'
import CustomerAppointmentView from '../Booking/CustomerAppointmentView'
import DialogBox from 'pages/Shop/Dashboard/DialogBox'
import EmailBox from 'pages/Shop/UsableComponent/EmailBox'
import SocketContext from 'config/socket'
import { Close, MoreVert, PersonSearch, Refresh, Reviews, Search, KeyboardArrowUp, KeyboardArrowDown } from '@mui/icons-material'
import { CUSTOMER_COUNT, SOCKET_CUSTOMER_BOOKING_REMOVE, SOCKET_CUSTOMER_BOOKING_UPDATE, SOCKET_CUSTOMER_NOTIFICATION } from 'redux/action/types'
import ReviewForm from 'pages/Shop/ReviewForm/ReviewForm'
import TooltipBox from 'pages/Shop/UsableComponent/TooltipBox'
import { DesktopDatePicker, LocalizationProvider, MobileDatePicker } from '@mui/lab'
import AdapterDateFns from '@mui/lab/AdapterDateFns'
import ListCard from 'pages/Shop/UsableComponent/ListCard'
import { ChangeNumber, getDateChange } from 'config/KeyData'
import HistoryCard from 'pages/Shop/UsableComponent/HistoryCard'
import { LoadingButton } from '@mui/lab'
import MobileLoader from 'pages/Shop/UsableComponent/MobileLoader'
import '../../Shop/Queue/queue.css'


var DailogData

const CustomerHistory = () => {

  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const [open, setOpen] = useState(false)
  const [view, setView] = useState('')
  const bookingData = useSelector((state) => state.auth.customerBooking)
  const allCount = useSelector(state => state.auth.customerHistoryCount)
  const loading = useSelector((state) => state.auth.loader)
  const cted = useSelector((state) => state.auth.updated)
  const dispatch = useDispatch()
  const [data, setData] = useState({
    open: false,
    shop_id: '',
    user_id: '',
    shop_email: '',
    shop_name: '',
    user_name: '',
    user_email: '',
    booking_id: '',
    service_id: '',
    image: '',
    shop_image: '',
    mobile: ''
  })
  const [value, setValue] = useState({
    start_date: new Date(),
    end_date: new Date(),
    search: ''
  });

  const [book, setBook] = useState({
    open: false,
    data: {}
  })

  const classes = tableStyles()

  const context = useContext(SocketContext)


  useEffect(() => {

    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
    })

    context.socket.on('userDisconnect', (data) => {
      console.log('Socket Err', data)
    })

    context.socket.on('customerCount', (data) => {
      //console.log('Customer Count Data', data)
      dispatch({ type: CUSTOMER_COUNT, payload: data })
    })

    context.socket.on('customerBooking', (data) => {
      // console.log('Customer Booking', data)
      dispatch({ type: SOCKET_CUSTOMER_BOOKING_UPDATE, payload: [data] })
    })

    context.socket.on('customerBookingRemove', (data) => {
      // console.log('Customer Booking Remove', data)
      dispatch({ type: SOCKET_CUSTOMER_BOOKING_REMOVE, payload: data })
    })

    context.socket.on('notificationCount', (data) => {
      console.log('notificationCount', data)
    })

    context.socket.on('notificationCount', (data) => {
      //console.log('notificationCount', data)
      dispatch({ type: SOCKET_CUSTOMER_NOTIFICATION, payload: data })
    })

    context.socket.on('disconnect', () => {
      console.log('socket is disconnect')
    })

    return () => {
      context.socket.removeListener(['customerCount', 'customerBooking', 'customerBookingRemove', 'notificationCount'])
    }
  }, [])

  useEffect(() => {

    if (bookingData.length == 0) {
      dispatch(getCustomerQuertBooking({ startDate: value.start_date, endDate: value.end_date }, 0, 10))
    } else {
      setElement(bookingData.booking)
      setFlag(true)
    }

    if (cted) {
      dispatch(getCustomerQuertBookingUpdate(data.booking_id))
    }

  }, [bookingData, cted])

  const handleDialogClose = () => {
    setOpen(false)
  }

  const handleDialogOpen = (item) => {
    DailogData = <CustomerAppointmentView item={item} />
    setOpen(true)
  }

  const handleClose = () => {
    setData({ ...data, open: false })
  }

  const handleOpen = (item) => {
    setData({
      ...data,
      open: true,
      shop_id: item.shop_id.user_information,
      user_id: item.user_id._id,
      shop_email: item.shop_email,
      shop_name: item.shop_name,
      user_name: item.user_id.name,
      user_email: item.user_email,
      booking_id: item._id,
      service_id: item.service_id._id,
      image: item.user_id.image_link,
      shop_image: item.shop_id.image_link,
      mobile: item.mobile
    })
  }

  const handleRefresh = () => {
    dispatch(getCustomerQuertBooking({ startDate: value.start_date, endDate: value.end_date }, 0, 10))
  }

  const handleStartDate = (newValue) => {
    console.log('new date', newValue)
    setValue({ ...value, start_date: newValue });
    dispatch(getCustomerQuertBooking({ startDate: newValue, endDate: value.end_date }, 0, 10))
  };

  const handleEndDate = (newValue) => {
    console.log('new date', newValue)
    setValue({ ...value, end_date: newValue });
    dispatch(getCustomerQuertBooking({ startDate: value.start_date, endDate: newValue }, 0, 10))
  };

  const handleSearch = () => {
    console.log('Search Data', value.start_date, value.end_date)
    dispatch(getCustomerQuertBooking({ startDate: value.start_date, endDate: value.end_date }, 0, 10))
  }

  const handleChange = (e) => {
    setValue({ ...value, [e.target.name]: e.target.value })
  }

  const handleBtn = () => {

    if (value.search == null || value.search == undefined || value.search == "") {
      setElement(bookingData.booking)
      return
    }
    setElement(bookingData.booking.reduce(function (newVal, data) {
      if (data.shop_name.includes(value.search)) {
        return newVal.concat(data);
      } else if (data.description.includes(value.search)) {
        return newVal.concat(data)
      } else if (data.service_id.name.includes(value.search)) {
        return newVal.concat(data)
      } else if (data.mobile == parseInt(value.search)) {
        return newVal.concat(data)
      } else if (data.shop_email.includes(value.search)) {
        return newVal.concat(data)
      } else if (data.status.includes(value.search)) {
        return newVal.concat(data)
      } else {
        return newVal
      }
    }, []))
  }

  const fetchMoreData = () => {
    dispatch(getCustomerQuertBooking({ startDate: value.start_date, endDate: value.end_date }, element.length, 10))
  };


  const handleViewCard = (row) => {
    setBook({
      ...book,
      open: true,
      data: row,
    })
  }

  const handleBookingClose = () => {
    setBook({ ...book, open: false })
  }


  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12} className="page-margin">
        <Card>
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={2} md={2} >
                {/* <IconButton title="Refresh" disabled={loading} onClick={handleRefresh} >
                  <Refresh />
                </IconButton> */}
                <Button className='customer-btn' disabled={loading} startIcon={<Refresh color='primary' />} onClick={handleRefresh} >
                  Refresh
                </Button>
              </Grid>
              <Grid item xs={12} sm={8} md={8} >
                <Typography className='title_head' variant="h4" component="h2" align="center">
                  History List
                </Typography>
              </Grid>
              <Grid item xs={12} sm={2} md={2} ></Grid>

              <Grid item xs={5} sm={5} md={5}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <DesktopDatePicker
                    label="Start Date"
                    inputFormat="MM/dd/yyyy"
                    value={value.start_date}
                    onChange={handleStartDate}
                    maxDate={new Date()}
                    renderInput={(params) => <TextField {...params} fullWidth sx={{ display: { xs: 'none', sm: 'block', md: 'block' } }} />}
                  />
                  <MobileDatePicker
                    label="Start Date"
                    inputFormat="MM/dd/yyyy"
                    value={value.start_date}
                    onChange={handleStartDate}
                    maxDate={new Date()}
                    renderInput={(params) => <TextField {...params} fullWidth sx={{ display: { xs: 'block', sm: 'none', md: 'none' } }} />}
                  />
                </LocalizationProvider>
              </Grid>
              <Grid item xs={5} sm={5} md={5}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <DesktopDatePicker
                    label="End Date"
                    inputFormat="MM/dd/yyyy"
                    value={value.end_date}
                    onChange={handleEndDate}
                    minDate={value.start_date}
                    maxDate={new Date()}
                    renderInput={(params) => <TextField {...params} fullWidth sx={{ display: { xs: 'none', sm: 'block', md: 'block' } }} />}
                  />
                  <MobileDatePicker
                    label="End Date"
                    inputFormat="MM/dd/yyyy"
                    value={value.end_date}
                    onChange={handleEndDate}
                    minDate={value.start_date}
                    maxDate={new Date()}
                    renderInput={(params) => <TextField {...params} fullWidth sx={{ display: { xs: 'block', sm: 'none', md: 'none' } }} />}
                  />
                </LocalizationProvider>
              </Grid>
              <Grid item xs={2} sm={2} md={2} justifyContent="center" alignItems="center" textAlign="center" >
                <IconButton title="Find" disabled={loading} onClick={handleSearch} sx={{ height: '100%' }} >
                  <Search />
                </IconButton>
              </Grid>
              <Grid item xs={6} sm={6} md={6} >
                <TextField label="Search By Name Service" placeholder="Search By Name" name="search" value={value.search} onChange={handleChange} fullWidth />
              </Grid>
              <Grid item xs={6} sm={6} md={6} >
                <IconButton title="Search" disabled={loading} onClick={handleBtn} sx={{ height: '100%' }} >
                  <PersonSearch />
                </IconButton>
              </Grid>
            </Grid>
            <TableContainer component={Paper} sx={{ display: { xs: "none", md: "block", sm: "block" } }}>
              <Table sx={{ minWidth: 750 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Review / Details</TableCell>
                    <TableCell>Appointment Date</TableCell>
                    <TableCell>Appointment Time</TableCell>
                    <TableCell>Service Name</TableCell>
                    <TableCell>Size</TableCell>
                    <TableCell>Description</TableCell>
                    <TableCell>Contact Number</TableCell>
                    <TableCell>Shop Name</TableCell>
                    <TableCell>Shop Email</TableCell>
                    <TableCell>Status</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <TableRow
                      key={row._id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}

                    >
                      <TableCell>
                        {(row.status == 'completed' && row.user_review == false) && <IconButton title="Review" onClick={() => { handleOpen(row) }}>
                          <Reviews />
                        </IconButton>}
                        <IconButton title="Details" onClick={() => { handleDialogOpen(row) }} >
                          <MoreVert />
                        </IconButton>
                      </TableCell>
                      <TableCell >{moment(row.appointment_date).format('LL')}</TableCell>
                      <TableCell >{moment(row.appointment_date).calendar()}</TableCell>
                      <TableCell >{row?.service_id.name}</TableCell>
                      <TableCell component="th" scope="row">
                        {(!row.size || row.size == null || row.size == undefined) ? '----' : row.size}
                      </TableCell>
                      <TableCell >
                        <TooltipBox title="hover" data={row.description} />
                      </TableCell>
                      <TableCell  >{ChangeNumber(row.mobile)}</TableCell>
                      <TableCell component="th" scope="row">
                        {row?.shop_name}
                      </TableCell>
                      <TableCell component="th" scope="row">
                        <EmailBox title={row.shop_email} />
                      </TableCell>
                      <TableCell >
                        <StatusBox value={row.status} />
                      </TableCell>
                    </TableRow>
                  )) : <TableRow>
                    <TableCell colSpan="10">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="10">
                      <TableLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
            {/* Mobile View */}
            <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "none" } }} >
              <Table size="small" aria-label="simple table">
                <TableBody>
                  {flag ? element.length > 0 ? element.map((row) => (
                    <React.Fragment key={row._id}>
                      <TableRow
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell className='customer-table'>
                          <HistoryCard {...row} handleOpen={handleOpen} handleDialogOpen={handleDialogOpen} handleClick={handleViewCard} />
                          {/* {
                            view == row._id ? <IconButton
                              aria-label="expand row"
                              size="small"
                              onClick={() => setView('')}
                            >
                              <KeyboardArrowUp />
                            </IconButton> : <IconButton
                              aria-label="expand row"
                              size="small"
                              onClick={() => setView(row._id)}
                            >
                              <KeyboardArrowDown />
                            </IconButton>
                          } */}
                        </TableCell>
                      </TableRow>
                      {/* <TableRow>
                        <TableCell className='customer-tablecell' colSpan={6}>
                          <Collapse in={view == row._id} timeout="auto" unmountOnExit>
                            <Box sx={{ margin: 1 }}>
                              <Grid container spacing={2}>
                                <Grid item xs={6} sm={6}>
                                  <div>{row.description}</div>
                                </Grid>
                                <Grid item xs={6} sm={6} >
                                  <div>Waiting Time</div>
                                  <div className='customer-font'>
                                    {(!row.waiting_number || row.waiting_number == null || row.waiting_number == undefined) ? '-------' : (row?.waiting_number < 60) ? row?.waiting_number + ' ' + row.waiting_key : getDateChange(row.waiting_number)}
                                  </div>
                                </Grid>
                                <Grid item xs={6} sm={6} >
                                  <div>Service Time</div>
                                  <div className='customer-font'>
                                    {(row.queue == true) ? (row?.approximate_number < 60) ? row?.approximate_number + ' ' + row.approximate_key : getDateChange(row.approximate_number) : '------'}
                                  </div>
                                </Grid>
                                <Grid item xs={12} sm={12}>
                                  <div>{row?.shop_name}</div>
                                  <div>{row.user_email}</div>
                                  <div>{ChangeNumber(row.mobile)}</div>
                                </Grid>
                              </Grid>
                            </Box>
                          </Collapse>
                        </TableCell>
                      </TableRow> */}
                    </React.Fragment>
                  )) : <TableRow>
                    <TableCell colSpan="12">
                      <NoData />
                    </TableCell>
                  </TableRow> : <TableRow>
                    <TableCell colSpan="12">
                      <MobileLoader />
                    </TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            </TableContainer>
          </CardContent>
          {element.length > 0 && (value.search == null || value.search == undefined || value.search == "") && allCount !== null && <Grid container spacing={2}>
            {allCount != element.length && <Grid item xs={12} sm={12} md={12} textAlign="center">
              <LoadingButton loading={loading}
                loadingIndicator="Loading..."
                variant="outlined" disabled={loading} onClick={fetchMoreData} >View More</LoadingButton>
            </Grid>}
          </Grid>}
        </Card>
      </Grid>
      <DialogBox open={open} handleClose={handleDialogClose} title={'History Detail'} data={DailogData} />
      <Dialog
        open={data.open}
        onClose={handleClose}
        sx={(theme) => ({
          [theme.breakpoints.up('sm')]: {
            minHeight: '750px !important'
          },
          [theme.breakpoints.up('xs')]: {
            minHeight: '450px !important'
          },
          [theme.breakpoints.up('md')]: {
            minHeight: '800px !important'
          },
        })}
        aria-labelledby="responsive-dialog-title"
      >
        <ReviewForm allvalue={data} setId={handleClose} type={true} />
      </Dialog>
      <Dialog
        open={book.open}
        onClose={handleBookingClose}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <Grid>
          <Card>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Box flexGrow={1} >
                      <Typography className='title_head loginHead' variant="h4" component="h2" align="center">
                        Booking Details
                      </Typography>
                    </Box>
                    <Box>
                      <IconButton onClick={handleBookingClose}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                <Grid item xs={6} sm={6}>
                  <div>{book.data.description}</div>
                </Grid>
                <Grid item xs={6} sm={6} >
                  <div>Waiting Time</div>
                  <div className='customer-font'>
                    {(!book.data.waiting_number || book.data.waiting_number == null || book.data.waiting_number == undefined) ? '-------' : (book.data?.waiting_number < 60) ? book.data?.waiting_number + ' ' + book.data.waiting_key : getDateChange(book.data.waiting_number)}
                  </div>
                </Grid>
                <Grid item xs={6} sm={6} >
                  <div>Service Time</div>
                  <div className='customer-font'>
                    {(book.data.queue == true) ? (book.data?.approximate_number < 60) ? book.data?.approximate_number + ' ' + book.data.approximate_key : getDateChange(book.data.approximate_number) : '------'}
                  </div>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <div>{book.data?.shop_name}</div>
                  <div>{book.data.user_email}</div>
                  <div>{ChangeNumber(book.data.mobile)}</div>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Dialog>
    </Grid>
  )
}

export default CustomerHistory
