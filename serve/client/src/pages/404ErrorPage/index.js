import React from 'react'
import { Grid } from '@mui/material'
import { styled } from '@mui/material/styles';
import Appbar from 'components/Appbar';
import ErrImage from '../../assets/404Error1.svg'
import ErrorImage from '../../assets/404Error2.svg'
import '../Shop/UsableComponent/NoData.css'

const PageOneWrapper = styled('div')({
  backgroundColor: 'transparent',
  height: '100%',
  width: '100%',
  overflow: 'hidden',
  position: 'relative',
});

const ErrorPage = () => {

  return (
    <PageOneWrapper>
      <Grid container direction="column" wrap="nowrap" sx={{ height: '100%', overflowX: 'auto' }}>
        <Grid item sx={{ zIndex: 1 }}>
          <Appbar />
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={12} md={12} textAlign="center">
            <img src={ErrImage} alt="404Error" className="noimageContent" />
          </Grid>
        </Grid>
      </Grid>
    </PageOneWrapper>
  )
};

export default ErrorPage