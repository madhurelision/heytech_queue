import React, { useState, useRef } from 'react'
import { TextField, Grid, Card, Typography, CardContent, FormControl, InputLabel, Select, MenuItem, Dialog, Box, IconButton } from '@mui/material'
import FormError from 'pages/Registration/FormError';
import { BookingButton } from "components/common";
import { Close } from '@mui/icons-material';
import { exportComponentAsJPEG, exportComponentAsPDF, exportComponentAsPNG } from 'react-component-export-image'

const Banner = () => {

  const componentRef = useRef();

  const [data, setData] = useState({
    title: 'Curtin haircare services',
    logo: '',
    logolink: 'COMPANY LOGO HERE',
    qrcode: '',
    qrcodelink: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAYAAAA9zQYyAAAAAklEQVR4AewaftIAAAdASURBVO3BQY4cSRLAQDLQ//8yt49+SiBRJWk24Gb2i7UucVjrIoe1LnJY6yKHtS5yWOsih7UucljrIoe1LnJY6yKHtS5yWOsih7UucljrIoe1LnJY6yI/fEjlb6qYVN6omFTeqHii8kbFpPIvVUwqf1PFJw5rXeSw1kUOa13khy+r+CaVJxWTyhOVJxXfVPFEZar4hMpU8UTljYpvUvmmw1oXOax1kcNaF/nhD1N5o+KbKiaVqeKJylQxqbyh8kTlExWTypOKT6i8UfEnHda6yGGtixzWusgP/+dUnqj8TRWTypOKN1SmiknlDZWp4v/ZYa2LHNa6yGGti/xwmYonKpPKVDFVfFPFpDJVfFPFpHKzw1oXOax1kcNaF/nhD6v4L6mYVJ6oPKl4Q+WJypOKSeWNim+q+C85rHWRw1oXOax1kR++TOW/RGWq+ETFpDJVPKmYVKaKSeWNikllqphUpoonKv9lh7UucljrIoe1LvLDhyr+yyomlaniScWk8kTljYpJ5ZsqPlHx/+Sw1kUOa13ksNZF7BcfUJkq3lCZKiaVv6liUpkq3lB5UjGpTBWTylTxRGWqeKLyTRVPVKaKTxzWushhrYsc1rqI/eKLVJ5UfEJlqphUnlRMKlPFpDJVTCpTxROVJxWTyicqJpU3KiaVNyomlanimw5rXeSw1kUOa13khz+s4onKVDGpvFExqUwqT1Smim+qmFQmlaliUvmmikllUnmjYlKZKv6kw1oXOax1kcNaF/nhQypTxROVqWJSmSqeqDypeKIyVbxRMak8qZgqJpVPVLyh8qTim1Smim86rHWRw1oXOax1EfvFF6n8SxWTylTxCZWpYlKZKiaVqWJSeVIxqUwVn1B5o+KJyhsVnzisdZHDWhc5rHUR+8UHVKaKJypTxaQyVUwqU8UTlScVk8obFZPKk4o3VD5RMak8qXhD5UnF33RY6yKHtS5yWOsiP/xjKk9U3lCZKj5RMak8qZhU/qSKSWVSeUNlqphU3lCZKv6kw1oXOax1kcNaF7FffEDlScUTlaniicpU8TepPKn4hMpU8QmVqWJSmSqeqDypmFSeVHzTYa2LHNa6yGGti/zwoYpJ5Y2KSeVJxaTypOKJyt+k8obKVDGpTBWfUHmjYlJ5UjGpTBWfOKx1kcNaFzmsdRH7xT+k8qRiUnlSMal8ouINlTcq3lCZKiaVqWJSmSo+ofKJim86rHWRw1oXOax1kR8+pDJVTCpTxVTxRGWqmFQmlaliUnlS8URlqpgqJpVPqPw/q/ibDmtd5LDWRQ5rXcR+8QGVqWJSeVIxqUwVf5LKVDGp/D+pmFTeqPiEyhsV33RY6yKHtS5yWOsi9ou/SOVJxaQyVbyh8qRiUpkq3lCZKt5QeVLxCZVvqniiMlX8SYe1LnJY6yKHtS5iv/gilScV36TypGJSeVLxROVJxaQyVXxCZaqYVL6pYlJ5UvFEZar4psNaFzmsdZHDWhexX/xBKm9UTCpPKr5JZaqYVN6oeKIyVTxReVLxRGWqeEPlExV/0mGtixzWushhrYv88CGVqWKqeKLypGJSmVSeVPxLKlPFVDGpfJPKE5UnFW9UTCpPVKaKTxzWushhrYsc1rqI/eIDKk8qnqhMFZPKk4onKlPF36TypGJSeVLxTSqfqJhU3qj4psNaFzmsdZHDWhexX3xA5U+qmFTeqHiiMlW8ofKkYlJ5o2JS+aaKT6i8UfEnHda6yGGtixzWusgPH6p4ojJVvKHypGJSeaLyhspU8YbKVDGpTBVvVDxReUPlScWTin/psNZFDmtd5LDWRX74x1SeVEwqTyqeVEwqn6iYVN6omFSmiqnim1SmijcqJpUnFZPKVPGJw1oXOax1kcNaF7FffEDlExVPVJ5UPFG5ScWkMlU8Ufkvq/jEYa2LHNa6yGGti9gv/o+pTBWfUJkqJpVPVHxCZaqYVKaKJypTxRsqU8W/dFjrIoe1LnJY6yI/fEjlb6p4ovKk4knFGxVPVJ6oPKl4o2JSmSreUJkq/ssOa13ksNZFDmtd5Icvq/gmlScVT1QmlaniEypTxVTxCZWpYlKZKr6p4g2VJxWTylTxicNaFzmsdZHDWhf54Q9TeaPiDZU3Kt5QmSomlTdUnlT8Syp/kspU8U2HtS5yWOsih7Uu8sP/uYpJZaqYVKaKJxVPKiaVNyomlaliUpkqJpWp4k+qmFSmiicqU8UnDmtd5LDWRQ5rXeSHy1R8QmWqmFSmiqliUnmjYlJ5ovKGyicqvqnimw5rXeSw1kUOa13EfvEBlanim1SmijdU3qiYVP6liicqU8UnVKaKJyqfqPimw1oXOax1kcNaF7FffEDlb6qYVJ5UPFGZKj6hMlW8ofKk4l9SmSomlU9UfOKw1kUOa13ksNZF7BdrXeKw1kUOa13ksNZFDmtd5LDWRQ5rXeSw1kUOa13ksNZFDmtd5LDWRQ5rXeSw1kUOa13ksNZF/ge0s8FRIA1CxAAAAABJRU5ErkJggg==",
    text1: 'Scan and get in queue save your time & reach at perfect time',
    text2: 'open to all online users just the app',
    linktitle: 'click on Hey Barber to visit website',
    type: 'text'
  })
  const [err, setErr] = useState({})
  const [view, setView] = useState({
    open: false,
    width: 738 * 0.667,
    height: 534 / 0.667,
    type: 'jpg'
  })

  const handleViewChange = (e) => {
    if (e.target.name == 'height') {
      setView({ ...view, [e.target.name]: e.target.value, width: parseInt(e.target.value) * 0.667 })
      setErr({ ...err, [e.target.name]: '' })
    } else if (e.target.name == 'width') {
      setView({ ...view, [e.target.name]: e.target.value, height: parseInt(e.target.value) / 0.667 })
      setErr({ ...err, [e.target.name]: '' })
    } else {
      setView({ ...view, [e.target.name]: e.target.value })
      setErr({ ...err, [e.target.name]: '' })
    }
  }

  const handleViewClose = () => {
    setView({ ...view, open: false, width: 738 * 0.667, height: 534 / 0.667 })
  }

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }


  const onHandleImage = (e) => {

    if (e.target.files == null || e.target.files == undefined) {
      return
    }

    const VIDEO_FORMATS = ["video/mp4", "video/mkv"];
    console.log('Image Console', e.target.name, e.target.files)
    console.log('Main', document.getElementsByName(e.target.name)[0].value)
    if (!VIDEO_FORMATS.includes(e.target.files[0].type)) {
      if (e.target.name == 'logo') {
        setData({ ...data, [e.target.name]: e.target.files[0], logolink: URL.createObjectURL(e.target.files[0]) })
        setErr({ ...err, [e.target.name]: '' })
      } else {
        setData({ ...data, [e.target.name]: e.target.files[0], qrcodelink: URL.createObjectURL(e.target.files[0]) })
        setErr({ ...err, [e.target.name]: '' })
      }
    } else {
      document.getElementsByName(e.target.name)[0].value = ''
      setData({ ...data, [e.target.name]: '' })
      return
    }
  }

  const handleSubmit = () => {
    var check = checkBanner(data)
    setErr(check)
    console.log('check', check)

    if (Object.keys(check).length > 0) return
    setView({ ...view, open: true })
  }

  const handleBtn = () => {

    if (view.type == 'jpg') {
      exportComponentAsJPEG(componentRef, { fileName: 'hey', html2CanvasOptions: { width: parseInt(view.height) * 0.667, height: parseInt(view.width) / 0.667 } });
      return
    }
    if (view.type == 'png') {
      exportComponentAsPNG(componentRef, { fileName: 'hey', html2CanvasOptions: { width: parseInt(view.height) * 0.667, height: parseInt(view.width) / 0.667 } });
      return
    }
    if (view.type == 'pdf') {
      exportComponentAsPDF(componentRef, { fileName: 'hey', html2CanvasOptions: { width: parseInt(view.height) * 0.667, height: parseInt(view.width) / 0.667 } })
      return
    }

  }

  return (
    <Grid className="page-margin">
      <Card>
        <CardContent>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12}>
                <Typography variant="h5" component="div" align="center">
                  Banner
                </Typography>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <div ref={componentRef} style={{ width: parseInt(view.height) * 0.667, height: parseInt(view.width) / 0.667 }} id="bannerQr" className='bannerDown'>
                  <div className="container">
                    <div className='innerPart'>
                      <div className='tilte'>
                        {data.title}
                      </div>
                      <div className='Clogo'>
                        <span className={(data.type == 'text') ? '' : 'shop-banner-type'} >{data.logolink}</span>
                        <img className={(data.type == 'image') ? '' : 'shop-banner-type'} src={data.logolink} width="100px" />
                      </div>
                      <div className='qrcodescanner'>
                        <img src={data.qrcodelink} width="100px" />
                      </div>
                      <div className="barbar_logo">
                        <div className="logoMobile MuiBox-root css-oe0vnf"><a className="mainLogo css-144sflg" href="/"><svg enableBackground="new 0 0 512 512" id="Layer_1" version="1.1" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><g><path d="   M373.9,24l-1.2-8L162.9,391.7c-6.3,11.4-20.6,15.5-32.1,9.5c-0.2-0.1-0.3-0.2-0.5-0.3c-22.2-11.4-49.8-3.3-62.4,18.2   c-13.5,23-5.4,52.5,17.7,65.4c22.9,12.8,51.7,4.6,64.5-18.3l55.8-100c3.2-5.7,10.3-7.7,16-4.5h0c6.5,3.6,14.6,1.3,18.2-5.2   l108.4-194.2C372.1,120.3,381,71.6,373.9,24z M127.1,453.3c-5.7,10.1-18.5,13.8-28.6,8.1c-10.1-5.7-13.8-18.5-8.1-28.6   c5.7-10.1,18.5-13.8,28.6-8.1C129.2,430.4,132.8,443.2,127.1,453.3z M259.4,284.2c-6.6,0-11.9-5.3-11.9-11.9s5.3-11.9,11.9-11.9   c6.6,0,11.9,5.3,11.9,11.9S265.9,284.2,259.4,284.2z" fill="none" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" strokeWidth="10"></path><path d="   M444.6,425.4c-12.2-21.8-39.6-30.4-62-19.5c-0.2,0.1-0.3,0.2-0.5,0.3c-11.6,5.8-25.8,1.3-31.9-10.1l-53.8-102.8l-29.2,52.3   l6.4,13.6c3.5,6.5,11.6,9,18.1,5.6h0c5.7-3,12.9-0.9,15.9,4.9l53.7,101.2c12.3,23.1,41,31.9,64.1,19.6   C448.9,478,457.6,448.7,444.6,425.4z M413.1,467.2c-10.3,5.4-23,1.5-28.4-8.7c-5.4-10.3-1.5-23,8.7-28.4c10.3-5.4,23-1.5,28.4,8.7   C427.2,449,423.3,461.7,413.1,467.2z" fill="none" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" strokeWidth="10"></path><path d="   M216.8,255.2l29.2-52.4L148.5,16l-1.4,8c-8.1,47.5-0.3,96.3,22.3,138.8L216.8,255.2z" fill="none" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" strokeWidth="10"></path></g></svg><button className="MuiButtonBase-root css-1mjc1cl-MuiButtonBase-root" type="button">HeyBarber<span className="MuiTouchRipple-root css-8je8zh-MuiTouchRipple-root"></span></button></a></div>
                      </div>
                      <div className='insidetext'>
                        <h3>{data.text1}</h3>
                        <div className="appTitle">
                          {data.text2}
                        </div>
                        <a href='#0'>{data.linktitle}</a>
                      </div>
                    </div>
                  </div>
                </div>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextField
                      fullWidth
                      InputLabelProps={{
                        shrink: true
                      }}
                      type="text"
                      value={data.title}
                      label="Title"
                      name="title"
                      onChange={handleChange}
                    />
                    {err.title && (<FormError data={err.title}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <FormControl fullWidth>
                      <InputLabel id="demo-simple-select-label">Type</InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={data.type}
                        label="Type"
                        name="type"
                        onChange={handleChange}
                      >
                        <MenuItem value="text">Text</MenuItem>
                        <MenuItem value="image">Image</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  {data.type == 'text' && <Grid item xs={12} sm={12} md={12}>
                    <TextField
                      fullWidth
                      InputLabelProps={{
                        shrink: true
                      }}
                      type="text"
                      value={data.logolink}
                      label="Logo"
                      name="logolink"
                      onChange={handleChange}
                    />
                    {err.logolink && (<FormError data={err.logolink}></FormError>)}
                  </Grid>}
                  {data.type == 'image' && <Grid item xs={12} sm={12} md={12}>
                    <TextField
                      name="logo" type="file" fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={onHandleImage} label="Logo"
                    />
                    {err.logo && (<FormError data={err.logo}></FormError>)}
                  </Grid>}
                  <Grid item xs={12} sm={12} md={12}>
                    <TextField name="qrcode" type="file" fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={onHandleImage} label="Code" />
                    {err.qrcode && (<FormError data={err.qrcode}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextField
                      fullWidth
                      InputLabelProps={{ shrink: true }}
                      type="text"
                      value={data.text1}
                      label="Text One"
                      name="text1"
                      onChange={handleChange}
                    />
                    {err.text1 && (<FormError data={err.text1}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextField
                      fullWidth
                      InputLabelProps={{ shrink: true }}
                      type="text"
                      value={data.text2}
                      label="Text Two"
                      name="text2"
                      onChange={handleChange}
                    />
                    {err.text2 && (<FormError data={err.text2}></FormError>)}
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextField
                      fullWidth
                      InputLabelProps={{ shrink: true }}
                      type="text"
                      value={data.linktitle}
                      label="Link Title"
                      name="linktitle"
                      onChange={handleChange}
                    />
                    {err.linktitle && (<FormError data={err.linktitle}></FormError>)}
                  </Grid>
                </Grid>
                <Grid container spacing={2} sx={{ marginTop: '5px' }} >
                  <Grid item xs={12} align="right">
                    <BookingButton type="button" variant="contained" onClick={() => { handleSubmit() }} >Export</BookingButton>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
      <Dialog
        open={view.open}
        onClose={handleViewClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        fullWidth
      >

        <Grid>
          <Card>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} md={12} sm={12}>
                  <Box display="flex" alignItems="center">
                    <Box flexGrow={1} >Set Details</Box>
                    <Box>
                      <IconButton onClick={handleViewClose}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">Type</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={view.type}
                      label="Type"
                      name="type"
                      onChange={handleViewChange}
                    >
                      <MenuItem value="jpg">Jpeg</MenuItem>
                      <MenuItem value="png">Png</MenuItem>
                      <MenuItem value="pdf">Pdf</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <TextField name="width" type="number" inputProps={{ min: 0, step: 1 }} value={view.width} fullWidth
                    variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleViewChange} label="Width" />
                  {err.width && (<FormError data={err.width}></FormError>)}
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <TextField name="height" type="number" inputProps={{ min: 0, step: 1 }} value={view.height} fullWidth
                    variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleViewChange} label="Height" />
                  {err.height && (<FormError data={err.height}></FormError>)}
                </Grid>
              </Grid>
              <Grid container spacing={2} sx={{ marginTop: '5px' }} >
                <Grid item xs={12} align="right">
                  <BookingButton type="button" variant="contained" onClick={handleBtn}>Download</BookingButton>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>

      </Dialog>
    </Grid>
  )
}

export default Banner


const checkBanner = (data) => {
  let errors = {}

  if (!data.title) {
    errors['title'] = "Title is Required";
  }

  if (!data.logolink) {
    errors['logolink'] = "Logo is Required";
  }

  if (!data.qrcodelink) {
    errors['qrcodelink'] = "QrCode is Required";
  }

  if (!data.text1) {
    errors['text1'] = "Text One is Required";
  }

  if (!data.text2) {
    errors['text2'] = "Text Two is Required";
  }

  if (!data.linktitle) {
    errors['linktitle'] = "Link Title is Required";
  }

  return errors
}