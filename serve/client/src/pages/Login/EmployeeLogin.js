import React, { useEffect, useState, useCallback } from 'react'
import { Grid, Card, CardContent, Box, Tab, styled } from '@mui/material';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import { TabContext, TabList, TabPanel } from '@mui/lab';
import EmployeeIdLogin from 'pages/Employee/LoginSignup/EmployeeIdLogin';
import EmployeeMobileLogin from 'pages/Employee/LoginSignup/EmployeeMobileLogin';
import EmployeeSignup from 'pages/Employee/LoginSignup/EmployeeSignup';

const PageOneWrapper = styled('div')({
  backgroundColor: 'transparent',
  height: '100%',
  width: '100%',
  overflow: 'hidden',
  position: 'relative',
});

const StyledTab = styled((props) => <Tab disableRipple {...props} />)(
  ({ theme }) => ({
    textTransform: 'none',
    fontWeight: theme.typography.fontWeightRegular,
    fontSize: theme.typography.pxToRem(15),
    minHeight: '50px',
    marginRight: theme.spacing(1),
    color: (theme.palette.mode == 'dark') ? '#f5f5f5' : '#242424',
    '&.Mui-selected': {
      color: '#d6a354 !important',
    },
    '&.Mui-focusVisible': {
      backgroundColor: 'rgba(100, 95, 228, 0.32)',
    },
    '&.MuiTabs-indicator': {
      display: 'none',
      left: '271px !important'
    }
  }),
);

const EmployeeLogin = () => {

  const userAuth = useSelector(state => state.auth.isAuthenticated);
  const history = useNavigate();
  const [view, setView] = useState('1')

  useEffect(() => {

    console.log('useEffect called', localStorage.getItem('type') == 'employee')
    if (userAuth == true && localStorage.getItem('type') == 'employee') {
      history('/employee')
    }
  }, [userAuth])

  return (
    <PageOneWrapper>
      <Grid sx={{ padding: '200px 20px' }}>
        <Card>
          <CardContent>
            <form>
              <Box>
                <TabContext value={view}>
                  <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                    <TabList onChange={(e, v) => { setView(v) }} aria-label="Login Form">
                      <StyledTab className='title_head' label="Employee Id" value="1" />
                      <StyledTab className='title_head' label="Mobile" value="2" />
                      <StyledTab className='title_head' label="SignUp" value="3" />
                    </TabList>
                  </Box>
                  <TabPanel value="1">
                    <EmployeeIdLogin />
                  </TabPanel>
                  <TabPanel value="2">
                    <EmployeeMobileLogin />
                  </TabPanel>
                  <TabPanel value="3">
                    <EmployeeSignup />
                  </TabPanel>
                </TabContext>
              </Box>
            </form>
          </CardContent>
        </Card>
      </Grid>

    </PageOneWrapper>
  )
}

export default EmployeeLogin