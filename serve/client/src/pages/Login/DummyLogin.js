import React, { useEffect, useState } from 'react'
import { Grid, Typography, Button, Card, CardContent, TextField } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import { keyLengthValidation } from 'config/KeyData';
import FormError from 'pages/Registration/FormError';
import { BookingButton } from "components/common";

const DummyLogin = ({ handleClose }) => {

  //const created = useSelector(state => state.auth.created);
  const loading = useSelector(state => state.auth.loader);
  const dispatch = useDispatch()
  const history = useNavigate()
  const [element, setElement] = useState({
    //mobile: '',
    //otp: '',
    email: '',
    password: ''
  });

  // useEffect(() => {

  //   if (created) {
  //handleClose()
  //   }

  // }, [created])


  const [err, setErr] = useState({})

  const handleChange = (e) => {
    setElement({ ...element, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    var check = checkMobile(element)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', element)
  }

  return (
    <Grid container direction="column" wrap="nowrap" sx={{ height: '100%' }}>
      <Card>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md={12} sm={12}>
              <Typography className='title_head' variant="h4" component="h2" align="center">
                Login
              </Typography>
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <TextField name="email" type="text" value={element.email} disabled={loading} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} label="Email"
                onChange={handleChange}
              />
              {err.email && (<FormError data={err.email}></FormError>)}
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <TextField name="password" type="password" value={element.password} disabled={loading} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} label="Password"
                onChange={handleChange}
              />
              {err.password && (<FormError data={err.password}></FormError>)}
            </Grid>
            {/* <Grid item xs={12} sm={12} md={12}>
              <TextField name="mobile" type="tel" inputProps={{
                maxLength: keyLengthValidation.mobile
              }} value={element.mobile} disabled={loading} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} label="Mobile Number"
                onChange={handleChange}
              />
              {err.mobile && (<FormError data={err.mobile}></FormError>)}
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <TextField name="otp" type="tel" inputProps={{
                maxLength: keyLengthValidation.otp
              }} value={element.otp} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} label="Otp"
                onChange={handleChange}
              />
              {err.otp && (<FormError data={err.otp}></FormError>)}
            </Grid> */}
            <Grid container spacing={2} sx={{ marginTop: '5px' }} >
              <Grid item xs={12} align="center">
                <BookingButton type="button" disabled={loading} variant="contained" onClick={() => { handleSubmit() }}>Login</BookingButton>
              </Grid>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default DummyLogin

const checkMobile = (data) => {
  let errors = {}

  // if (!data.mobile) {
  //   errors['mobile'] = "Mobile Number is Required";
  // }

  // if (data.mobile) {
  //   if (!data["mobile"].match(/^[0-9]{10}$/)) {
  //     errors["mobile"] = "Please enter valid mobile no.";
  //   }
  // }

  if (!data.email) {
    errors['email'] = "Email is Required";
  }

  // if (!data.otp) {
  //   errors['otp'] = "Otp is Required";
  // }

  if (!data.password) {
    errors['password'] = "Password is Required";
  }
  return errors
}
