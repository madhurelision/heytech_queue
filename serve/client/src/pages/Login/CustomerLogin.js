import React, { useEffect, useState, useCallback } from 'react'
import { Grid, Typography, Button, Card, CardContent, TextField, FormControl, RadioGroup, FormLabel, FormControlLabel, Radio, Box, Checkbox, IconButton, Dialog } from '@mui/material'
import { SERVER_URL, CLIENT_URL } from '../../config.keys';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import { keyLengthValidation } from 'config/KeyData';
import FormError from 'pages/Registration/FormError';
import GoogleIcon from 'pages/Shop/UsableComponent/GoogleIcon';
import GoogleButton from 'pages/Shop/UsableComponent/GoogleButton';
import { customerNumberLogin, customerNumberLoginVerify } from 'redux/action';
import { BookingButton } from 'components/common';
import ProgressTimer from 'pages/Shop/UsableComponent/ProgressTimer';
import { Password, Close } from '@mui/icons-material';

const CustomerLogin = ({ handleClose, handleView }) => {


  const userAuth = useSelector(state => state.auth.isAuthenticated);
  const loading = useSelector(state => state.auth.loader)
  const created = useSelector(state => state.auth.created);
  const updated = useSelector(state => state.auth.updated);
  const link = useSelector(state => state.auth.link);
  const shopData = useSelector(state => state.auth.shopLoginData);
  const history = useNavigate();
  const [element, setElement] = useState([]);
  const dispatch = useDispatch()
  const [view, setView] = useState(false)

  const [data, setData] = useState({
    id: '',
    mobile: '',
    otp: ''
  })
  const [count, setCount] = useState(0)
  const [show, setShow] = useState({
    open: false,
    date: new Date().setSeconds(120)
  })

  const [open, setOpen] = useState(false)

  const [value, setValue] = useState({
    name: '',
    email: '',
    mobile: ''
  })

  const [err, setErr] = useState({})

  useEffect(() => {

    console.log('useEffect called', localStorage.getItem('type') == 'customer')
    if (userAuth == true && localStorage.getItem('type') == 'customer') {
      history('/customer')
    }
  }, [userAuth])

  useEffect(() => {

    if (created) {

      setElement(shopData.data)
      setShow({ ...show, open: true, date: new Date().setSeconds(120) })
      handleView(true)

      if (data.id == '') {
        handleOpen(true)
      }
    }

  }, [created, shopData])

  useEffect(() => {

    if (updated) {
      history(link)
    }

  }, [updated, link])


  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleLoginWithMobile = () => {
    var check = checkMobileLogin({ mobile: data.mobile })
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(customerNumberLogin({ mobile: data.mobile, resend: 'single' }))
  }

  const handleSubmitLogin = () => {
    var check = checkMobileOtp(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(customerNumberLoginVerify(data))
  }

  const handleSelect = (e, cc) => {
    if (e.target.checked == true) {
      setData({ ...data, id: cc._id })
      setErr({ ...err, id: '' })
      setValue({ ...value, name: cc?.first_name, email: cc?.email, mobile: cc?.mobile })
      setOpen(false)
    } else {
      setData({ ...data, id: '' })
      setErr({ ...err, id: '' })
      setValue({ ...value, name: '', email: '', mobile: '' })
    }
  }


  const handleResendOtp = () => {
    setCount(count + 1)
    setShow({ ...show, open: true, date: new Date().setSeconds(120) })
    dispatch(customerNumberLogin({ mobile: data.mobile, resend: 'multiple' }))
  }
  const handleCloseTimer = useCallback((val) => {
    setShow({ ...show, open: false })
  }, [show])


  const handleCloseUser = () => {
    setOpen(false)
  }

  const handleOpen = () => {
    setOpen(true)
  }

  return (
    <Grid container direction="column" wrap="nowrap" sx={{ height: '100%' }}>

      <Card>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md={12} sm={12}>

              <Box display="flex" alignItems="center">
                <Box flexGrow={1}>
                  <Typography className='title_head loginHead' variant="h4" component="h2" align="center">
                    Customer Login
                  </Typography>
                </Box>
                <Box>
                  <IconButton onClick={handleClose}>
                    <Close />
                  </IconButton>
                </Box>
              </Box>
            </Grid>
            {view == false && <>
              <Grid item xs={12} md={6} sm={6}>
                <GoogleButton
                  onClick={() => {
                    var a = document.createElement('a')
                    a.href = `${SERVER_URL}/web/user/auth/google`
                    a.click()
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6} sm={6}>
                <BookingButton fullWidth variant="contained" onClick={() => { setView(!view) }}>Mobile Login</BookingButton>
              </Grid>
            </>}
            {view == true && <>
              {data.id !== null && data.id !== undefined && data.id !== '' && <>
                <Grid item xs={12} sm={12} md={12} onClick={handleOpen} textAlign="center" >
                  <div> <b>Name:</b> {value?.name}</div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} onClick={handleOpen} textAlign="center" >
                  <div> <b>Email</b>: {value?.email}</div>
                </Grid>
              </>}
              <Grid item xs={12} sm={12} md={12} textAlign="center">
                <TextField name="mobile" type="tel" inputProps={{
                  maxLength: keyLengthValidation.mobile
                }} value={data.mobile} disabled={data.length > 0 || loading} style={{ width: '450px' }} variant="outlined" InputLabelProps={{ shrink: true }} label="Mobile Number"
                  onChange={handleChange}
                />
                {err.mobile && (<FormError data={err.mobile}></FormError>)}
              </Grid>
              {element.length > 0 && <Grid item xs={12} sm={12} md={12} textAlign="center">{'Verifying on '}<span className="booking_number_high" >{'+91' + data.mobile}</span> </Grid>}
              {element.length > 0 &&
                <Grid item xs={12} sm={12} md={12} textAlign="center" >
                  <TextField name="otp" type="tel" inputProps={{
                    maxLength: keyLengthValidation.otp
                  }} value={data.otp} variant="outlined" style={{ width: '450px' }} InputLabelProps={{ shrink: true }} label="Otp"
                    onChange={handleChange}
                  />
                  {err.otp && (<FormError data={err.otp}></FormError>)}
                </Grid>}

              {element.length > 0 && <div className='shop-login-btn'><Button sx={{ textTransform: 'capitalize' }} disabled={(loading || count == 2 || show.open)} startIcon={<Password />} onClick={() => { handleResendOtp() }} >
                Resend Otp
              </Button>
                {show.open == true && <span style={{ marginTop: '5px' }} ><ProgressTimer time={show.date} ma={'hello'} func={handleCloseTimer} show={false} /></span>}</div>}
              {err.id && (<FormError data={err.id}></FormError>)}
              <Grid container spacing={2} sx={{ marginTop: '5px' }} >
                <Grid item xs={12} align="center">
                  {element.length == 0 && <BookingButton type="button" disabled={loading} variant="contained" onClick={() => { handleLoginWithMobile() }}>Login</BookingButton>}
                  {element.length > 0 && <BookingButton type="button" disabled={loading} variant="contained" onClick={() => { handleSubmitLogin() }}>Login</BookingButton>}
                </Grid>
              </Grid>
            </>}
          </Grid>
        </CardContent>
      </Card>
      <Dialog
        open={open}
        onClose={() => {
          if (data.id !== '') {
            handleCloseUser()
          }
        }}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <Grid>
          <Card>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Typography className='title_head' flexGrow={1} variant="h5" component="div" align="center">
                      Select User
                    </Typography>
                    <Box>
                      <IconButton onClick={() => {
                        if (data.id !== '') {
                          handleCloseUser()
                        }
                      }}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                {element.map((cc) => {
                  return (
                    <Grid key={cc._id} item xs={12} sm={4} md={4}>
                      <Card onClick={() => {
                        if (data.id == cc._id) {
                          handleSelect({ target: { checked: false } }, cc)
                        } else {
                          handleSelect({ target: { checked: true } }, cc)
                        }
                      }} >
                        <Box sx={data.id == cc._id ? { background: 'red', color: 'black' } : { background: 'white', color: 'black' }}>
                          <CardContent>
                            <Grid container>
                              <Grid item xs={12} sm={12} >
                                <div style={{ fontSize: '0.75rem' }}><b>Name:</b> {cc?.first_name}</div>
                              </Grid>
                              <Grid item xs={12} sm={12} >
                                <div style={{ fontSize: '0.75rem' }}> <b>Email:</b> {cc?.email}</div>
                              </Grid>
                              <Grid item xs={12} sm={12} >
                                <div style={{ fontSize: '0.75rem' }}><b>Mobile:</b> {cc?.mobile}</div>
                              </Grid>
                              <Grid item xs={12} sm={12} md={12}>
                                <Checkbox
                                  color="primary"
                                  sx={{ color: 'blue' }}
                                  inputProps={{ 'aria-label': 'secondary checkbox' }}
                                  onClick={(e) => { handleSelect(e, cc) }}
                                  id={cc._id}
                                  name={cc._id}
                                  value={cc._id}
                                  checked={data.id == cc._id ? true : false}
                                />
                              </Grid>
                            </Grid>
                          </CardContent>
                        </Box>
                      </Card>
                    </Grid>
                  )
                })}
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Dialog>
    </Grid>
  )
}

export default CustomerLogin

const checkLogin = (data) => {
  let error = {}

  if (!data.id) {
    error['id'] = "Employe Id Required"
  }
  return error
}

const checkMobileLogin = (data) => {
  let error = {}

  if (!data.mobile) {
    error['mobile'] = "Mobile Number is Required";
  }

  if (data.mobile) {
    if (!data["mobile"].match(/^[0-9]{10}$/)) {
      error["mobile"] = "Please enter valid mobile no.";
    }
  }
  return error
}


const checkMobileOtp = (data) => {
  let errors = {}

  if (!data.mobile) {
    errors['mobile'] = "Mobile Number is Required";
  }

  if (data.mobile) {
    if (!data["mobile"].match(/^[0-9]{10}$/)) {
      errors["mobile"] = "Please enter valid mobile no.";
    }
  }
  if (!data.otp) {
    errors['otp'] = "Otp is Required";
  }

  if (!data.id) {
    errors['id'] = "Select Customer is Required";
  }

  return errors
}