import React, { useEffect, useState, useCallback } from 'react'
import { Grid, Typography, Button, Card, CardContent, TextField, FormControl, RadioGroup, FormLabel, FormControlLabel, Radio, Box, Checkbox, IconButton, Dialog } from '@mui/material'
import { SERVER_URL, CLIENT_URL } from '../../config.keys';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import { keyLengthValidation } from 'config/KeyData';
import FormError from 'pages/Registration/FormError';
import { shopNumberLogin, shopNumberLoginVerify } from 'redux/action';
import GoogleIcon from 'pages/Shop/UsableComponent/GoogleIcon'
import GoogleButton from 'pages/Shop/UsableComponent/GoogleButton';
import { BookingButton } from "components/common";
import { Password, Close } from '@mui/icons-material';
import ProgressTimer from 'pages/Shop/UsableComponent/ProgressTimer';

const Login = ({ handleClose, handleView }) => {

  const link = useSelector(state => state.auth.link);
  const shopData = useSelector(state => state.auth.shopLoginData);
  const created = useSelector(state => state.auth.created);
  const updated = useSelector(state => state.auth.updated);
  const loading = useSelector(state => state.auth.loader);
  const [data, setData] = useState([]);
  const [view, setView] = useState(false)
  const [open, setOpen] = useState(false)
  const dispatch = useDispatch()
  const history = useNavigate()
  const [element, setElement] = useState({
    mobile: '',
    otp: '',
    email: ''
  });
  const [count, setCount] = useState(0)
  const [show, setShow] = useState({
    open: false,
    date: new Date().setSeconds(120)
  })

  const [value, setValue] = useState({
    name: '',
    email: '',
    mobile: ''
  })

  useEffect(() => {

    if (created) {

      if (shopData.redirect == true) {
        // var a = document.createElement('a')
        // a.href = `${CLIENT_URL}/register/${shopData?.data[0]?._id}`
        // a.click()
        history(`/register/${shopData?.data[0]?._id}`)
      } else {
        setData(shopData.data)
        setShow({ ...show, open: true, date: new Date().setSeconds(120) })
        handleView(true)
        if (element.email == '') {
          handleOpen(true)
        }
      }
    }

  }, [created, shopData])

  useEffect(() => {

    if (updated) {
      history(link)
    }

  }, [updated, link])


  const [err, setErr] = useState({})

  const handleChange = (e) => {
    setElement({ ...element, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    var check = checkMobile({ mobile: element.mobile })
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', element)
    dispatch(shopNumberLogin({ mobile: element.mobile, resend: 'single' }))
  }

  const handleSubmitLogin = () => {
    var check = checkMobileOtp(element)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', element)
    dispatch(shopNumberLoginVerify(element))

  }

  const handleSelect = (e, cc) => {
    if (e.target.checked == true) {
      setElement({ ...element, email: cc._id })
      setErr({ ...err, email: '' })
      setValue({ ...value, name: cc?.shop_name, email: cc?.email, mobile: cc?.mobile })
      setOpen(false)
    } else {
      setElement({ ...element, email: '' })
      setErr({ ...err, email: '' })
      setValue({ ...value, name: '', email: '', mobile: '' })
    }
  }

  const handleResendOtp = () => {
    setCount(count + 1)
    setShow({ ...show, open: true, date: new Date().setSeconds(120) })
    dispatch(shopNumberLogin({ mobile: element.mobile, resend: 'multiple' }))
  }
  const handleCloseTimer = useCallback((val) => {
    setShow({ ...show, open: false })
  }, [show])

  const handleCloseUser = () => {
    setOpen(false)
  }

  const handleOpen = () => {
    setOpen(true)
  }

  return (
    <Grid container direction="column" wrap="nowrap" sx={{ height: '100%' }}>
      <Card>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md={12} sm={12}>
              <Box display="flex" alignItems="center">
                <Box flexGrow={1}>
                  <Typography className='title_head loginHead' variant="h4" component="h2" align="center">
                    Shop Login
                  </Typography>
                </Box>
                <Box>
                  <IconButton onClick={handleClose}>
                    <Close />
                  </IconButton>
                </Box>
              </Box>
            </Grid>
            {view == false && <>
              {/* <Grid item xs={6} md={6} sm={6}>
                <Button href={`${SERVER_URL}/web/mentor/auth/google`} color="primary" variant="contained" component="a" startIcon={<GoogleIcon />} >Sign In With Google</Button>
              </Grid> */}
              <Grid item xs={12} md={6} sm={6}>
                <GoogleButton className="googleLoginBtn"
                  onClick={() => {
                    var a = document.createElement('a')
                    a.href = `${SERVER_URL}/web/mentor/auth/google`
                    a.click()
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6} sm={6}>
                <BookingButton fullWidth variant="contained" onClick={() => { setView(!view) }}>Mobile Login</BookingButton>
              </Grid>
            </>}
            {view == true && <>
              {element.email !== null && element.email !== undefined && element.email !== '' && <>
                <Grid item xs={12} sm={12} md={12} textAlign="center" onClick={handleOpen}>
                  <div> <b>Name:</b> {value?.name}</div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} textAlign="center" onClick={handleOpen}>
                  <div> <b>Email</b>: {value?.email}</div>
                </Grid>
              </>}
              <Grid item xs={12} sm={12} md={12} textAlign="center">
                <TextField
                  name="mobile"
                  type="tel"
                  inputProps={{
                    maxLength: keyLengthValidation.mobile
                  }}
                  value={element.mobile}
                  disabled={data.length > 0 || loading}
                  style={{ width: '450px' }}
                  variant="outlined"
                  InputLabelProps={{ shrink: true }}
                  label="Mobile Number"
                  onChange={handleChange}
                />
                {err.mobile && (<FormError data={err.mobile}></FormError>)}
              </Grid>
              {data.length > 0 && <Grid item xs={12} sm={12} md={12} textAlign="center">{'Verifying on '}<span className="booking_number_high" >{'+91' + element.mobile}</span> </Grid>}
              {data.length > 0 &&
                <Grid item xs={12} sm={12} md={12} textAlign="center">
                  <TextField
                    name="otp"
                    type="tel"
                    inputProps={{
                      maxLength: keyLengthValidation.otp
                    }}
                    value={element.otp}
                    variant="outlined"
                    InputLabelProps={{ shrink: true }}
                    label="Otp"
                    onChange={handleChange}
                    style={{ width: '450px' }}
                  />
                  {err.otp && (<FormError data={err.otp}></FormError>)}
                </Grid>}

              {data.length > 0 && <div className="shop-login-btn"><Button sx={{ textTransform: 'capitalize' }} disabled={(loading || count == 2 || show.open)} startIcon={<Password />} onClick={() => { handleResendOtp() }} >
                Resend Otp
              </Button>
                {show.open == true && <span style={{ marginTop: '5px' }} ><ProgressTimer time={show.date} ma={'hello'} func={handleCloseTimer} show={false} /></span>}</div>}
              {/* 
              {data.length > 0 && <Grid item xs={12} sm={12} md={12}>
                <Button variant="contained" onClick={handleOpen} >Select User</Button>
              </Grid>} */}
              {err.email && (<FormError data={err.email}></FormError>)}
              <Grid container spacing={2} sx={{ marginTop: '5px' }} >
                <Grid item xs={12} align="center">
                  {data.length == 0 && <BookingButton type="button"
                    style={{ width: '200px' }} disabled={loading} variant="contained" onClick={() => { handleSubmit() }}>Login</BookingButton>}
                  {data.length > 0 && <BookingButton type="button"
                    style={{ width: '200px' }} disabled={loading} variant="contained" onClick={() => { handleSubmitLogin() }}>Login</BookingButton>}
                </Grid>
              </Grid>
            </>}
          </Grid>
        </CardContent>
      </Card>
      <Dialog
        open={open}
        onClose={() => {
          if (element.email !== '') {
            handleCloseUser()
          }
        }}
        fullWidth
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <Grid>
          <Card>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Box display="flex" alignItems="center">
                    <Typography className='title_head' flexGrow={1} variant="h5" component="div" align="center">
                      Select User
                    </Typography>
                    <Box>
                      <IconButton onClick={() => {
                        if (element.email !== '') {
                          handleCloseUser()
                        }
                      }}>
                        <Close />
                      </IconButton>
                    </Box>
                  </Box>
                </Grid>
                {data.map((cc) => {
                  return (
                    <Grid key={cc._id} item xs={12} sm={4} md={4}>
                      <Card onClick={() => {
                        if (element.email == cc._id) {
                          handleSelect({ target: { checked: false } }, cc)
                        } else {
                          handleSelect({ target: { checked: true } }, cc)
                        }
                      }}>
                        <Box sx={element.email == cc._id ? { background: 'red', color: 'black' } : { background: 'white', color: 'black' }}>
                          <CardContent>
                            <Grid container>
                              <Grid item xs={12} sm={12} md={12} >
                                <div style={{ fontSize: '0.75rem' }}> <b>Name:</b> {cc?.shop_name}</div>
                                {/* <div style={{ fontSize: '1.0rem' }}>{cc?.shop_name}</div> */}
                              </Grid>
                              <Grid item xs={12} sm={12} md={12} >
                                <div style={{ fontSize: '0.75rem' }}> <b>Email</b>: {cc?.email}</div>
                                {/* <div style={{ fontSize: '0.8rem' }}>{cc?.email}</div> */}
                              </Grid>
                              <Grid item xs={12} sm={12} md={12} >
                                <div style={{ fontSize: '0.75rem' }}> <b>Mobile:</b> {cc?.mobile}</div>
                                {/* <div style={{ fontSize: '1.0rem' }}>{cc?.mobile}</div> */}
                              </Grid>
                              <Grid item xs={12} sm={12} md={12}>
                                <Checkbox
                                  color="primary"
                                  sx={{ color: 'blue' }}
                                  inputProps={{ 'aria-label': 'secondary checkbox' }}
                                  onClick={(e) => { handleSelect(e, cc) }}
                                  id={cc._id}
                                  name={cc._id}
                                  value={cc._id}
                                  checked={element.email == cc._id ? true : false}
                                />
                              </Grid>
                            </Grid>
                          </CardContent>
                        </Box>
                      </Card>
                    </Grid>
                  )
                })}
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Dialog>
    </Grid>
  )
}

export default Login

const checkMobile = (data) => {
  let errors = {}

  if (!data.mobile) {
    errors['mobile'] = "Mobile Number is Required";
  }

  if (data.mobile) {
    if (!data["mobile"].match(/^[0-9]{10}$/)) {
      errors["mobile"] = "Please enter valid mobile no.";
    }
  }
  return errors
}

const checkMobileOtp = (data) => {
  let errors = {}

  if (!data.mobile) {
    errors['mobile'] = "Mobile Number is Required";
  }

  if (data.mobile) {
    if (!data["mobile"].match(/^[0-9]{10}$/)) {
      errors["mobile"] = "Please enter valid mobile no.";
    }
  }
  if (!data.otp) {
    errors['otp'] = "Otp is Required";
  }

  if (!data.email) {
    errors['email'] = "Select User is Required";
  }

  return errors
}