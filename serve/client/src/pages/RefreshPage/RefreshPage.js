import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllActiveService, getAllExpertise, getAllService, getAllTagMetaData } from 'redux/action';
import { Grid, Typography, styled, Button } from '@mui/material';

const StyledGrid = styled(Grid)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignContent: 'center',
  color: '#f5f5f5',
  paddingLeft: '2rem',

  '& .MuiTypography-root': {
    diplay: 'inline-flex',
    [theme.breakpoints.up('xs')]: {
      fontSize: '15vw',
      flexWrap: 'nowrap',
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '11vw',
      flexWrap: 'nowrap',
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '9vw',
      flexWrap: 'nowrap',
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '8vw',
      flexWrap: 'nowrap',
    },
    fontFamily: 'inter',
    fontWeight: '800',
    opacity: '0.9',
  },
}));

const RefreshPage = () => {

  const dispatch = useDispatch()

  const loadVal = useSelector(state => state.auth.loader)

  const handleButton = () => {
    // dispatch(getAllService())
    // dispatch(getAllExpertise())
    // dispatch(getAllActiveService())
    dispatch(getAllTagMetaData())
  }

  return (
    <Grid container item direction="row">
      <StyledGrid item sm={12} lg={6} style={{ width: '100%' }}>
        <Typography>Something.</Typography>
        <Typography>Went.</Typography>
        <Typography>Wrong.</Typography>
      </StyledGrid>
      <Grid
        container
        item
        sm={12}
        lg={6}
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'column',
          flexGrow: 1,
        }}>
        <Button color="primary" variant="contained" disabled={loadVal} onClick={handleButton} >Try Again</Button>
      </Grid>
    </Grid>
  );
};

export default RefreshPage;
