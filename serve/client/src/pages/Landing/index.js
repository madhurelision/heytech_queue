import React from 'react';
import Appbar from '../../components/Appbar/index';
import Grid from '@mui/material/Grid';
import Particles from '../../components/Particles/index';
import Hero from '../../components/Hero/index';
import { styled } from '@mui/material/styles';
import { useSelector } from 'react-redux';
import RefreshPage from 'pages/RefreshPage/RefreshPage';
import MobileToolbar from 'components/MobileToolbar/MobileToolbar';

const PageOneWrapper = styled('div')({
  backgroundColor: 'transparent',
  height: '90vh',
  width: '100%',
  overflow: 'hidden',
  position: 'relative',
});

const Landing = () => {

  const checkService = useSelector((state) => state.auth);

  return (
    <>
      <PageOneWrapper className='mainWrapper'>
        <Grid className="innerContainer" container direction="column" wrap="nowrap" sx={{ height: '100%' }} >
          {/* <Grid item sx={{ zIndex: 1 }}>
            <Appbar />
          </Grid> */}
          <Grid item sx={{ display: 'flex', flexGrow: 1 }}>
            {(checkService.serviceRequestFailed == false || checkService.expertiseRequestFailed == false) ? <Hero /> : <RefreshPage />}
          </Grid>
          {/* <Particles /> */}

          {/* <Grid
            item
            sx={{ display: { xs: "block", md: "none", sm: "block" } }}
          >
            <MobileToolbar />
          </Grid> */}
        </Grid>
      </PageOneWrapper>
    </>
  )
};

export default Landing;
