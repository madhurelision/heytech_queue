//const BASE_URL = 'http://192.168.1.10:'
//https://heybarber.herokuapp.com/

const BASE_URL = 'http://localhost:';
//const BASE_URL = 'https://heybarber.herokuapp.com/';

const C_PORT = 3000
const S_PORT = 5000
const SOCKET_PORT = 5002
const ANALYTICS_PORT = 5007
const INVENTORY_PORT = 5008
const PAYMENT_PORT = 5009

// export const CLIENT_URL = 'https://heybarber.herokuapp.com';
// export const SERVER_URL = 'https://heybarberserver.herokuapp.com';
// export const SOCKET_URL = 'https://heybarberservices.herokuapp.com';
// export const SOCKET_URLNEW = 'ws://heybarberservices.herokuapp.com';

// export const CLIENT_URL = 'https://heybarber.herokuapp.com';
// export const SERVER_URL = 'https://queuickapi.herokuapp.com';
// export const SOCKET_URL = 'https://queuickservice.herokuapp.com';
// export const SOCKET_URLNEW = 'ws://queuickservice.herokuapp.com';
// export const PAYMENT_URL = 'https://queuick-payment.onrender.com';

export const CLIENT_URL = BASE_URL + C_PORT;
export const SERVER_URL = BASE_URL + S_PORT;
export const SOCKET_URL = BASE_URL + SOCKET_PORT;
export const SOCKET_URLNEW = BASE_URL + SOCKET_PORT;
export const ANALYTIC_URL = BASE_URL + ANALYTICS_PORT;
export const INVENTORY_URL = BASE_URL + INVENTORY_PORT;
export const PAYMENT_URL = BASE_URL + PAYMENT_PORT;



export const WebPushKey = "BAVqi7qbi4PHBnZBVd3BS5hKEh3ogJ_-C0Kd8M7aHKRMAy6R-efJjn70rkbesDGEpUdkf-3f4qrIICMN0A5A9A8"


// export const CLIENT_URL = 'https://heybarber.herokuapp.com';
// export const SERVER_URL = 'https://heybarber.herokuapp.com';
// export const SOCKET_URL = 'https://heybarber.herokuapp.com';
// export const SOCKET_URLNEW = 'ws://heybarber.herokuapp.com';
// export const CLIENT_URL = BASE_URL + C_PORT;
// export const SERVER_URL = BASE_URL + S_PORT;
// export const SOCKET_URL = BASE_URL + S_PORT;
// export const SOCKET_URLNEW = BASE_URL + S_PORT;

//const SOCKET_PORT = 5000

// export const CLIENT_URL = BASE_URL + C_PORT;
// export const SERVER_URL = BASE_URL + S_PORT;
// export const SOCKET_URL = BASE_URL + S_PORT;


export const CDRT_SERVER =
  process.env.REACT_APP_CRDT_SERVER || 'ws://192.168.1.10:1234';
export const PRODUCTION = process.env.REACT_APP_PROD === 'true';
export const GOOGLE_ANALYTICS_KEY =
  process.env.REACT_APP_GOOGLE_ANALYTICS_KEY || 'UA-000000000-0';

