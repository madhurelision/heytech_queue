import { atom, AtomEffect, DefaultValue } from 'recoil';
import { MentorSchemaType } from '../types';
interface Preferences {
  name?: string;
}

const localStorageEffect =
  (key: string): AtomEffect<Preferences> =>
    ({ setSelf, onSet }) => {
      const savedValue = localStorage.getItem(key);
      if (savedValue !== null) {
        setSelf(JSON.parse(savedValue));
      }

      onSet((newValue) => {
        if (newValue instanceof DefaultValue) {
          localStorage.removeItem(key);
        } else {
          localStorage.setItem(key, JSON.stringify(newValue));
        }
      });
    };

export const preferencesState = atom<Preferences>({
  key: 'preferencesState',
  default: {},
  effects_UNSTABLE: [localStorageEffect('PREFERENCES')],
});

export const userStreamState = atom<MediaStream | null>({
  key: 'userMediaState',
  default: null,
});
export const displayStreamState = atom<MediaStream | null>({
  key: 'displayMediaState',
  default: null,
});

// List of audio devices you have
export const audioDevicesState = atom<MediaDeviceInfo[]>({
  key: 'audioDevicesState',
  default: [],
});

export const currentMicIdState = atom<string | null>({
  key: 'currentAudioDevice',
  default: null,
});

export const videoDevicesState = atom<MediaDeviceInfo[]>({
  key: 'videoDevicesState',
  default: [],
});

// Cuurent camera you are using
export const currentCameraIdState = atom<string | null>({
  key: 'currentVideoDevice',
  default: null,
});

export const motivationState = atom<unknown>({
  key: 'motivationState',
  default: null,
});

export const expertiseState = atom<unknown>({
  key: 'expertiseState',
  default: null,
});

export const tabIndexState = atom<string>({
  key: 'tabIndex',
  default: '1',
});

export const topicState = atom<any[]>({
  key: 'topicState',
  default: [],
});

export const mentorState = atom<MentorSchemaType>({
  key: 'mentorState',
  default: {} as MentorSchemaType,
});

export const categoryState = atom<unknown>({
  key: 'categoryState',
  default: null,
});

export const serviceState = atom<unknown>({
  key: 'serviceState',
  default: null,
});

export const expertState = atom<unknown>({
  key: 'expertState',
  default: null,
});

export const topicsState = atom<any[]>({
  key: 'topicsState',
  default: [],
});

export const globalTabIndexState = atom<string>({
  key: 'globalTabIndex',
  default: '1',
});

export const globalPackageState = atom<any[]>({
  key: 'globalPackage',
  default: [],
});