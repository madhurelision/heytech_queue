import React from 'react';
import MaterialAppBar from '@mui/material/AppBar';
import ScrollTop from 'components/ScrollToTop';
import ToolBar from './Toolbar';
import ShopHeader from '../../pages/Shop/AppBar/ShopHeader'
import CustomerHeader from '../../pages/Customer/AppBar/CustomerHeader'
import { useSelector } from 'react-redux'
import EmployeeHeader from 'pages/Employee/AppBar/EmployeHeader';

const Appbar = () => {

  const auth = useSelector((state: any) => state.auth.isAuthenticated)

  return (
    <>
      <MaterialAppBar
        variant="outlined"
        position="relative"
        elevation={0}
        sx={{ padding: '8px', backgroundColor: '#242424' }}>
        {auth == false ? <ToolBar /> : (auth == true && localStorage.getItem('type') == 'shop') ? <ShopHeader /> : (auth == true && localStorage.getItem('type') == 'customer') ? <CustomerHeader /> : (auth == true && localStorage.getItem('type') == 'employee') ? <EmployeeHeader /> : <ToolBar />}
      </MaterialAppBar>
      <div id="back-to-top-anchor" />
      <ScrollTop />
    </>
  );
}

export default Appbar;
