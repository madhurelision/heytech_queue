import React from 'react';
import { Badge, Grid, Typography, useTheme } from '@mui/material';
import { Groups, Mail, Policy, Info, ContactMail } from '@mui/icons-material';
import { useLocation, useNavigate } from 'react-router-dom';
import { tabStyles } from 'pages/Shop/Dashboard/styles';

const ToolbarTabs = () => {

  const theme = useTheme()
  const history = useNavigate()
  const classes = tabStyles(theme)
  const location = useLocation()

  return (
    <Grid id="fixedToolbar" className="shop-mobile-tab" container sx={{ color: theme.palette.mode == 'dark' ? 'white' : 'black', justifyContent: 'center', backdropFilter: 'blur(10px)' }}>
      {/* <Grid item xs={3} sm={3} md={3} textAlign="center" className={(location.pathname == "/search") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/search')
      }}>
        <Policy fontSize="large" />
        <Typography className={classes.font + ' title_head'}>Find</Typography>
      </Grid>
      <Grid item xs={3} sm={3} md={3} textAlign="center" className={(location.pathname == "/global") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/global')
      }}>
        <Policy fontSize="large" />
        <Typography className={classes.font + ' title_head'}>Global</Typography>
      </Grid>
      <Grid item xs={3} sm={3} md={3} textAlign="center" className={(location.pathname == "/about") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/about')
      }}>
        <Info fontSize="large" />
        <Typography className={classes.font + ' title_head'} >About</Typography>
      </Grid>
      <Grid item xs={3} sm={3} md={3} textAlign="center" className={(location.pathname == "/contact") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/contact')
      }}>
        <ContactMail fontSize="large" />
        <Typography className={classes.font + ' title_head'} >Contact Us</Typography>
      </Grid> */}
      {/* <Grid item xs={4} sm={4} md={4} textAlign="center" className={classes.grid} onClick={() => {
        history('/room')
      }}>
        <Groups fontSize="large"/>
        <Typography className={classes.font + ' title_head'} >HeyMeet</Typography>
      </Grid> */}

      <div className={(location.pathname == "/search") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/search')
      }}>
        <Policy fontSize="large" />
        <Typography className={`title_head ${classes.font}`}>Find</Typography>
      </div>
      <div className={(location.pathname == "/global") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/global')
      }}>
        <Policy fontSize="large" />
        <Typography className={`title_head ${classes.font}`}>Global</Typography>
      </div>
      <div className={(location.pathname == "/about") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/about')
      }}>
        <Info fontSize="large" />
        <Typography className={`title_head ${classes.font}`} >About</Typography>
      </div>
      <div className={(location.pathname == "/contact") ? classes.selectGrid : classes.grid} onClick={() => {
        history('/contact')
      }}>
        <ContactMail fontSize="large" />
        <Typography className={`title_head ${classes.font}`} >Contact Us</Typography>
      </div>
    </Grid>
  )
}

export default ToolbarTabs