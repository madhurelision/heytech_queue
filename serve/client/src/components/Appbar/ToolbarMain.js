import React from 'react'
import { Grid, useTheme } from '@mui/material'
import MaterialAppBar from '@mui/material/AppBar';
import { styled } from '@mui/material/styles';
import ScrollTop from 'components/ScrollToTop';
import ToolBar from './Toolbar';
import ShopHeader from '../../pages/Shop/AppBar/ShopHeader'
import CustomerHeader from '../../pages/Customer/AppBar/CustomerHeader'
import { useSelector } from 'react-redux'
import EmployeeHeader from 'pages/Employee/AppBar/EmployeHeader';
import MobileToolbar from '../MobileToolbar/MobileToolbar';
import { useLocation, useNavigate } from 'react-router-dom';

const PageOneWrapper = styled('div')({
  backgroundColor: 'transparent',
  height: '100%',
  width: '100%',
  overflow: 'hidden',
  position: 'relative',
});


const ToolbarMain = ({ children }) => {

  const history = useNavigate()
  const auth = useSelector((state) => state.auth.isAuthenticated)
  const location = useLocation()
  const theme = useTheme()

  return (
    <Grid container direction="column" wrap="nowrap" sx={{ height: '100%' }}>
      {location.pathname.includes('/register/') !== true && <Grid item >

        <MaterialAppBar
          variant="outlined"
          position="relative"
          elevation={0}
          sx={{ padding: '8px', backgroundColor: theme.palette.mode == 'dark' ? '#242424' : '#ffff' }}>
          {auth == false && location.pathname.includes('/employeeLogin') !== true && <ToolBar />}
          {auth == true && location.pathname.includes('/employeeLogin') !== true && localStorage.getItem('type') == 'shop' && <ShopHeader />}
          {auth == true && location.pathname.includes('/employeeLogin') !== true && localStorage.getItem('type') == 'customer' && <CustomerHeader />}
          {auth == true && localStorage.getItem('type') == 'employee' && <EmployeeHeader />}

          {/* {auth == false ? <ToolBar /> : (auth == true && localStorage.getItem('type') == 'shop') ? <ShopHeader /> : (auth == true && localStorage.getItem('type') == 'customer') ? <CustomerHeader /> : (auth == true && localStorage.getItem('type') == 'employee') ? <EmployeeHeader /> : <ToolBar />} */}
        </MaterialAppBar>
        {/* <div id="back-to-top-anchor" /> */}
        {/* <ScrollTop /> */}
      </Grid>}
      {children}
      {location.pathname.includes('/employeeLogin') !== true && <Grid item
        sx={{
          display: { xs: "block", md: "none", sm: "block" },
          // position: 'fixed',
          bottom: 0,
          padding: '10px 10px 0px 10px',
          width: '100%',
        }}
      >
        <MobileToolbar />
      </Grid>}
    </Grid>
  )
}

export default ToolbarMain
