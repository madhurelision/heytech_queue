import { Grid } from '@mui/material';
import React, { useState, useEffect } from 'react';
import { styled, Button } from '@mui/material';
import { expertiseOptions, motivationOptions } from '../../data/index';
import { motivationState, expertiseState, topicState } from '../../store/local';
import { useRecoilState } from 'recoil';
import { Link, ReactSelect as Select } from '../common/index';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useDispatch, useSelector } from 'react-redux'
import { getAllExpertise, getAllService, getAllActiveService, getAllActiveBarberService, getAllActiveBarberExpertise } from '../../redux/action/index'

const StyledButton = React.memo(styled(Button)`
  background-size: 200%;
  font-weight: 500;
  color: #fff;
  font-size: 1rem;
  //background-image: linear-gradient(90deg, green, white);
  background: #d6a354;
  line-height: 32px;
  box-shadow: 0 2px 1px transparent, 0 4px 2px transparent,
    0 8px 4px transparent, 0 16px 8px transparent, 0 32px 16px transparent;
  transition: all 0.8s cubic-bezier(0.32, 1.32, 0.42, 0.68);

  /* transition: all 0.8s cubic-bezier(0.32, 1.32, 0.42, 0.68); */
  :hover {
    transform: none;
    background: #e3be85;
    color:#fff;
    /* transform: linear-gradient(230deg, #35249b, #8b13c3 50%, #ea577a); */
  }
`);

const StyledGrid = React.memo(styled(Grid)({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-around',
  width: '100%',
  p: 2,
  // BackgroundColor: 'greyx`',
}));


var mainValue = [{
  value: 'All',
  label: 'All',
}]

const Seachbox = () => {

  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('md'));
  const [data, setData] = useState([])
  const [element, setElement] = useState([])
  const [flag, setFlag] = useState(false)
  const [motivation, setMotivation] = useRecoilState(motivationState);
  const [expertise, setExpertise] = useRecoilState(expertiseState);
  const [topic, setTopic] = useRecoilState(topicState);
  const serviceData = useSelector((state) => state.auth.barberServiceList)
  const expertiseData = useSelector((state) => state.auth.barberExpertiseList)
  const checkService = useSelector(state => state.auth)
  const dispatch = useDispatch()

  useEffect(() => {
    if (serviceData.length == 0 && checkService.serviceRequestFailed == false) {
      dispatch(getAllActiveBarberService(0, 20))
    } else {
      setData(mainValue.concat([...new Set(serviceData.map((cc) => cc.motivation))].map((ss) => {
        return {
          label: ss,
          value: ss
        }
      })))
      setFlag(true)
    }
  }, [serviceData])

  useEffect(() => {
    if (expertiseData.length == 0 && checkService.expertiseRequestFailed == false) {
      dispatch(getAllActiveBarberExpertise())
    }
  }, [expertiseData])

  const handleMotivation = (val) => {
    setMotivation(val)
    setTopic(serviceData.filter((cc) => cc.motivation == val.label).map((dd) => dd._id))
  }

  const filterExpert = (val) => {
    if (val == 'All' || val == null || val == undefined) return expertiseData
    const mains = serviceData.filter((cc) => cc.motivation == val).map((dd) => dd.shop)
    return expertiseData.filter((cc) => mains.includes(cc.shop))
  };

  return (
    <>
      <div id="searchContainer">
        <Grid container item>
          <StyledGrid item xs={12} sm={6} md={6}>
            <Select
              menuPlacement="auto"
              name="Services"
              sx={{
                fontSize: '20px',
                padding: '8px 8px',
                color: 'white',
                '.select__menu': {
                  width: '90%',
                },
              }}
              // options={motivationOptions}
              options={data}
              value={motivation}
              onChange={(e) => { handleMotivation(e) }}
              isSearchable={matches}
              classNamePrefix="select"
              placeholder={<span>Filter by Services</span>}
            />
          </StyledGrid>
          <StyledGrid item xs={12} sm={6} md={6} sx={{ display: { xs: 'block', md: 'none', sm: 'none' }, color: theme.palette.mode == 'dark' ? 'white' : '#242424' }}>
            <div className='App'>OR</div>
          </StyledGrid>
          <StyledGrid item xs={12} sm={6} md={6}>
            <Select
              name="Expertise"
              menuPlacement="auto"
              sx={{
                fontSize: '20px',
                padding: '8px 8px',
                color: 'white',
                '.select__menu': {
                  width: '90%',
                },
              }}
              //options={expertiseOptions}
              //@ts-ignore
              options={mainValue.concat(filterExpert(motivation?.label))}
              value={expertise}
              onChange={setExpertise}
              isSearchable={matches}
              classNamePrefix="select"
              placeholder={<span>Filter by Expertise</span>}
            />
          </StyledGrid>
        </Grid>
        <Grid
          container
          item
          sx={{
            placeContent: 'center',
            padding: '1rem 0 0 ',
          }}>
          <Link to="/search">
            <StyledButton className='findBtn' variant="contained">Find a Barber</StyledButton>
          </Link>
        </Grid>
      </div>
    </>
  );
};

export default React.memo(Seachbox);
