import React, { useContext, useEffect } from 'react';
import { Grid, Typography, styled } from '@mui/material';
import Searchbox from './Searchbox';
import MobileToolbar from '../MobileToolbar/MobileToolbar'
import SocketContext from 'config/socket';

const StyledGrid = React.memo(styled(Grid)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignContent: 'center',
  color: theme.palette.mode == 'dark' ? '#f5f5f5' : 'black',
  paddingLeft: '2rem',

  '& .MuiTypography-root': {
    diplay: 'inline-flex',
    [theme.breakpoints.up('xs')]: {
      fontSize: '15vw',
      flexWrap: 'nowrap',
    },
    [theme.breakpoints.up('sm')]: {
      fontSize: '11vw',
      flexWrap: 'nowrap',
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '9vw',
      flexWrap: 'nowrap',
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: '8vw',
      flexWrap: 'nowrap',
    },
    fontFamily: 'inter',
    fontWeight: '800',
    opacity: '0.9',
  },
})));

const Hero = () => {

  const context = useContext(SocketContext)

  useEffect(() => {

    if (context.socket.connected == false) {
      context.socket = context.socketAuth(localStorage.getItem('auth-token'))
    }

    context.socket.on('connect', () => {
      console.log('Socket is connected')
      if (localStorage.getItem('auth-token') !== null) {
        context.socket.emit('userConnect', { token: localStorage.getItem('auth-token'), type: localStorage.getItem('type') })
      }
    })

    context.socket.on('userDisconnect', (data) => {
      console.log('Socket Err', data)
    })

    context.socket.on('disconnect', () => {
      context.socket.removeAllListeners()
    })

    return () => {
      context.socket.removeAllListeners()
    }
  }, [])

  return (
    <Grid id="searchBox" container item direction="row" justifyContent="space-between">
      <StyledGrid className='searchContent' item sm={12} lg={5} style={{ width: '100%', padding: '0 0 0 45px' }}>
        <Typography fontSize="25px" className="title_head_home" >Search.</Typography>
        <Typography className="title_head_home" >Your.</Typography>
        <Typography className="title_head_home" >Salon.</Typography>
      </StyledGrid>
      <Grid
        container
        item
        sm={12}
        lg={6}
        style={{
          display: 'flex',
          alignItems: 'center',
          flexDirection: 'column',
          justifyContent: 'center',
          margin: 'auto',
          flexGrow: 1,
        }}
        sx={(theme) => ({ 
          [theme.breakpoints.up('sm')]: {
            justifyContent: 'unset',
          },
          [theme.breakpoints.up('xs')]: {
            justifyContent: 'unset',
          },
          [theme.breakpoints.up('md')]: {
            justifyContent: 'center',
          },
        })}
      >
        <Searchbox />
      </Grid>
        
    </Grid>

   
  )
};

export default React.memo(Hero);
