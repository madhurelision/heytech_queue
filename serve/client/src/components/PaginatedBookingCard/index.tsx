import React from 'react';
import { Grid, List, Pagination, styled } from '@mui/material';
import BookingCard from './BookingCard';
import { useRecoilValue } from 'recoil';
import { mentorState } from 'store';
import { range } from 'utils/helper';
import { DayEnumType } from 'types';
import { dayOfWeek, getDateChange } from 'config/KeyData';
// import { Topic } from 'types';


interface PaginatedBookingCardProps {
  motivation: string;
  topics: [];
  page: number;
  setPage: React.Dispatch<React.SetStateAction<number>>;
  value: [];
  wtime: string;
  location: {},
  offer: [];
  locationBook: {},
  stime: []
}

const CssPagination = React.memo(styled(Pagination)({
  '& .Mui-selected': {
    backgroundColor: '#d6a354!important',
  },

  '& .MuiPagination-root': {
    color: 'white',
    '& svg': {
      fill: 'white',
    },
    '& button': {
      color: 'white',
    },
  },
}));

const filterTopics = (topicOptions: [], motivation: string, data: any[]) => {
  if (motivation == "All" && data.length == 0) {
    return topicOptions
  }

  if (motivation == "All" && data.length > 0) {
    return topicOptions.filter((cc: any) => cc).sort(function (a: any, b: any) {
      return data.indexOf(b._id) - data.indexOf(a._id)
    })
  }

  return topicOptions.filter((topic: any) => topic.motivation == motivation).sort(function (a: any, b: any) {
    return data.indexOf(b._id) - data.indexOf(a._id)
  });
};

const PaginatedBookingCard: React.FC<PaginatedBookingCardProps> = ({
  motivation,
  topics: topics_,
  page,
  setPage,
  value,
  wtime,
  location,
  offer,
  locationBook,
  stime
}) => {

  /*
  Along with query paranmeter of the mentor
  axios.get('/api/motivation', )
  */


  const { time_slot } = useRecoilValue(mentorState);
  const dayName: DayEnumType = dayOfWeek[new Date().getDay()];
  const { start_hour, end_hour, available } = time_slot[dayName];
  const limitRange = available == true ? range(start_hour, end_hour, 1) : []

  const rowsPerPage = 6;

  const topics = filterTopics(topics_, motivation, offer);
  const count = Math.floor(
    topics.length % rowsPerPage === 0
      ? topics.length / rowsPerPage
      : topics.length / rowsPerPage + 1,
  );

  return (
    <>
      <Grid container spacing={2}>
        {/* <List component="nav" aria-label="booking appointments">
          {(rowsPerPage > 0
            ? topics.slice(
              (page - 1) * rowsPerPage,
              (page - 1) * rowsPerPage + rowsPerPage,
            )
            : topics
          ).map((topic: any, index) => (
            <BookingCard topic={topic} key={index} value={value} wtime={wtime} />
          ))}
        </List> */}
        {(rowsPerPage > 0
          ? topics.slice(
            (page - 1) * rowsPerPage,
            (page - 1) * rowsPerPage + rowsPerPage,
          )
          : topics
        ).map((topic: any, index) => (
          <Grid item xs={12} sm={6} md={4} key={index}>
            <BookingCard topic={topic} key={index} value={value} wtime={wtime} location={location} locationBook={locationBook} stime={stime} limit={limitRange} />
          </Grid>
        ))}
      </Grid>
      <Grid
        item
        sx={{
          py: 2,
          justifyContent: 'center',
          display: 'flex',
          width: '100%',
        }}>
        <CssPagination
          shape="rounded"
          count={count}
          page={page}
          onChange={(event, val) => setPage(val)}
        />
      </Grid>
    </>
  );
};

export default React.memo(PaginatedBookingCard);
