import React from 'react';
import { Chip, Typography, Paper, Dialog, Card, CardMedia, Grid, Box, CardContent, useTheme } from '@mui/material';
import { BookingButton as Button } from 'components/common';
import DatePicker from 'components/DaterPicker';
import { LOADERCLOSE } from 'redux/action/types';
import { useDispatch, useSelector } from 'react-redux';
import { getAllOffers } from 'redux/action';
import { useEffect } from 'react';
import './offer.css'
import { dayOfWeek, getDateChange } from 'config/KeyData';
import TableLoader from 'pages/Shop/UsableComponent/TableLoader';
// import { Topic } from 'types';
// import { colorPalatte } from 'data';
interface BookingCardProps {
  topic: any;
  value: any;
  wtime: any;
  location: any;
  locationBook: any;
  stime: any
  limit: any
}
const BookingCard: React.FC<BookingCardProps> = ({ topic, value, wtime, location, locationBook, stime, limit }) => {
  const { motivation, name, description, image, _id, accept, waiting_time, waiting_number, waiting_key, tags, seat } = topic;
  // const topicColor = colorPalatte[motivation].overlay;
  const theme = useTheme()
  const [open, setOpen] = React.useState(false);
  const [key, setKey] = React.useState(false);
  const [show, setShow] = React.useState(false);
  const [element, setElement] = React.useState<any>([]);
  const offerData = useSelector((state: any) => state.auth.offerList)
  const dispatch = useDispatch();
  const topicText = '#f5f5f5';
  const main = seat.join()

  useEffect(() => {
    if (offerData.length == 0) {
      dispatch(getAllOffers())
    } else {
      setElement(offerData.offer)
    }
  }, [offerData])

  const handleClose = () => {
    setOpen(false)
    setKey(false)
    dispatch({ type: LOADERCLOSE })
  }

  const handleCurrent = () => {
    // if (Object.keys(location).length == 0) {
    //   if ("geolocation" in navigator) {
    //     console.log("Available");
    //   } else {
    //     console.log("Not Available");
    //   }
    //   return
    // }
    setOpen(true)
    setKey(true)
  }

  const handleBook = () => {
    // if (Object.keys(location).length == 0) {
    //   if ("geolocation" in navigator) {
    //     console.log("Available");
    //   } else {
    //     console.log("Not Available");
    //   }
    //   return
    // }
    setOpen(true)
    setKey(false)
  }

  const handleShow = (val: any) => {
    setShow(val)
  }

  return (
    <>
      <Paper id="product-card" elevation={4} className={(accept == false) ? 'colorMainC' : ''} >
        {/* <Grid
          sx={{
            width: '100%',
            backgroundColor: '#121212 !important',
            py: 1,
            my: 1,
            border: '2px ridge rgba(255, 255, 255, 0.12)',
            mx: 0
          }}
          container
          direction="row"
          // JustifyContent="center"
          alignItems="center" spacing={2} >
          <Grid item xs={12} sm={12} md={2} sx={{ px: 2 }}>
            <Chip
              style={{
                width: '100%',
                fontWeight: 700,
                color: topicText,
                //backgroundColor: topicColor,
                //backgroundImage: `url(${image})`
              }}
              // Size="medium"
              label={motivation}
            // variant="outlined"
            />
          </Grid>
          <Grid
            item
            container
            direction="column"
            xs={12}
            sm={12}
            md={2}
            sx={{ color: 'white', px: 2 }}>
            <Typography variant="h6" sx={{ fontWeight: 800 }}>
              {name}
            </Typography>
            <Typography
              variant="body2"
              sx={{ paddingBottom: '8px', opacity: 0.6 }}>
              {description}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={12} md={2} sx={{ px: 2 }}>
            <Chip
              style={{
                width: '100%',
                fontWeight: 700,
                color: topicText,
              }}
              size="small"
              label={waiting_time}
            />
          </Grid>
          {value.length > 0 && <Grid item xs={12} sm={12} md={2} sx={{ px: 2 }}>
            <Chip
              style={{
                width: '100%',
                fontWeight: 700,
                color: topicText,
              }}
              size="small"
              label={wtime}
            />
          </Grid>}
          <Grid item xs={12} sm={12} md={2} sx={{ px: 2 }}>
            <Button variant="contained" disabled={(!accept || accept == null || accept == undefined) ? false : (accept == true) ? false : true} onClick={() => { handleBook() }} fullWidth >
              Book
            </Button>
          </Grid>
          <Grid item xs={12} sm={12} md={2} sx={{ px: 2 }}>
            <Button variant="contained" disabled={(!accept || accept == null || accept == undefined) ? false : (accept == true) ? false : true} onClick={() => { handleCurrent() }} fullWidth>
              GetInQueue({value.length > 0 && value.filter((cc: any) => cc._id == _id)[0].count})
            </Button>
          </Grid>
        </Grid> */}
        <Card className="bookingcardIn" sx={{ borderRadius: '0' }} >
          <div className="cardImg">
            {element.length > 0 && element.filter((cc: any) => cc.service_id == _id).length > 0 && <div className='offer-Card' style={{ background: element.filter((cc: any) => cc.service_id == _id)[0]?.color }} >
              <span>{element.filter((cc: any) => cc.service_id == _id)[0]?.value}</span>
            </div>}
            <CardMedia
              component="img"
              sx={{ width: '100%', objectFit: 'unset', borderRadius: '0', border: 0 }}
              image={image}
              alt={name}
            />
          </div>
          <Box>
            <CardContent >
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Chip
                    style={{
                      width: '100%',
                      fontWeight: 700,
                      color: theme.palette.mode == 'dark' ? topicText : 'black'
                    }}
                    label={motivation}
                  />
                </Grid>
                <Grid
                  item
                  container
                  direction="column"
                  xs={12}
                  sm={12}
                  md={12}
                  sx={{ color: theme.palette.mode == 'dark' ? 'white' : 'black', px: 2 }}>
                  <Typography className="bookingTitle" variant="h6" sx={{ fontWeight: 800 }}>
                    {name} (<span className={`${(!accept || accept == null || accept == undefined) ? 'open-btn-red' : (accept == true && limit.includes(new Date().getHours()) == true) ? 'open-btn-green' : 'open-btn-red'}`} >{(!accept || accept == null || accept == undefined) ? 'close' : (accept == true && limit.includes(new Date().getHours()) == true) ? 'open' : 'close'}</span>)
                  </Typography>
                  <Typography className="reviews" variant="h6" sx={{ fontWeight: 400 }}>
                    {value.length > 0 && value.filter((cc: any) => cc._id == _id)[0]?.review} Reviews
                  </Typography>
                  <Typography
                    variant="body2"
                    sx={{ paddingBottom: '8px', opacity: 0.6 }}>
                    {description}
                  </Typography>
                  {/* 
                  <Chip
                    style={{
                      width: '100%',
                      fontWeight: 700,
                      color: topicText,
                    }}
                    size="small"
                    label={`Service Time is ${waiting_number + ' ' + waiting_key}`}
                  /> */}
                </Grid>
                <Grid className="cardContent" item xs={12} sm={12} md={12}>
                  <Typography
                    variant="body2" textAlign="left" sx={{ fontSize: '20px' }}>
                    {value.length > 0 && <>
                      {value.length > 0 && value.filter((cc: any) => cc._id == _id).reduce((acc: any, val: any) => val.count + acc, 0) == 0 ? <span className='waiting'>No waiting</span> : <><span style={{ color: 'red' }}>{value.length > 0 && value.filter((cc: any) => cc._id == _id).reduce((acc: any, val: any) => val.count + acc, 0)}</span> users waiting in queue</>}</>}
                  </Typography>
                </Grid>
                {/* <Grid item xs={12} sm={12} md={12}>
                  {Object.keys(location).length > 0 && <Chip
                    style={{
                      fontWeight: 500,
                      color: topicText,
                    }}
                    size="small"
                    label={`Shop dist. is ${location?.distance} & dur. is ${location?.duration}`}
                  />}
                </Grid> */}

                <Grid className="paddingTop" item xs={6} sm={6} md={6}>
                  <Typography className="respFont" style={{ fontWeight: 'bold' }}>Seat Number</Typography>
                </Grid>
                <Grid className="paddingTop" item xs={6} sm={6} md={6}>
                  <Typography variant="body2">{main}</Typography>
                </Grid>
                <Grid className="paddingTop" item xs={6} sm={6} md={6}>
                  <Typography className="respFont" style={{ fontWeight: 'bold' }}>Service Time</Typography>
                </Grid>
                <Grid className="paddingTop" item xs={6} sm={6} md={6}>
                  <Typography className='waiting' variant="body2">{(waiting_number < 60) ? waiting_number + ' ' + waiting_key : getDateChange(waiting_number)}</Typography>
                </Grid>
                {/* {parseInt(wtime) > 0 && <>
                  <Grid item xs={6} sm={6} md={6}>
                    <Typography style={{ fontWeight: 'bold' }}>Waiting Time</Typography>
                  </Grid>
                  <Grid item xs={6} sm={6} md={6}>
                    <Typography className='waiting' variant="body2">{wtime}</Typography>
                  </Grid>
                </>} */}
                {stime.length > 0 ? <>
                  <Grid className="paddingTop" item xs={6} sm={6} md={6}>
                    <Typography className="respFont" style={{ fontWeight: 'bold', fontSize: '0.95rem' }}>Min. Waiting Time</Typography>
                  </Grid>
                  <Grid className="paddingTop" item xs={6} sm={6} md={6}>
                    <Typography className='waiting' variant="body2">
                      {/* {stime.filter((cc: any) => seat.includes(cc._id)).sort((a: any, b: any) => parseInt(b.data) - parseInt(a.data))[0]?.data} */}
                      {stime.filter((cc: any) => seat.includes(cc._id)).reduce((result: any, val: any) => {
                        let minT = result.length ? result[0].time : val.time
                        //console.log('check', minT, val.time, val.time)
                        if (val.time < minT) {
                          minT = val.time
                          result.length = 0
                        }

                        if (val.time === minT) {
                          result.push(val)
                        }
                        return result
                      }, [])[0]?.data}
                    </Typography>
                  </Grid>
                  {/* <Grid item xs={12} sm={12} md={12}>
                    <div>{JSON.stringify(stime.filter((cc: any) => seat.includes(cc._id)).sort((a: any, b: any) => a.time - b.time))}</div>
                    <div>Reduce------------</div>
                    <div>
                      {JSON.stringify(stime.filter((cc: any) => seat.includes(cc._id)).reduce((result: any, val: any) => {
                        let minT = result.length ? result[0].time : val.time
                        console.log('check', minT, val.time, val.time)
                        if (val.time < minT) {
                          minT = val.time
                          result.length = 0
                        }

                        if (val.time === minT) {
                          result.push(val)
                        }
                        return result
                      }, []))}
                    </div>
                  </Grid> */}
                </> :
                  <Grid item xs={12} sm={6} md={12}><TableLoader /> </Grid>}
                {Object.keys(location).length > 0 ? <>
                  <Grid className="paddingTop" item xs={6} sm={6} md={6}>
                    <Typography className="respFont" style={{ fontWeight: 'bold' }}>Travelling Time</Typography>
                  </Grid>
                  <Grid className="paddingTop" item xs={6} sm={6} md={6}>
                    <Typography className='travelling' variant="body2">{location?.duration}</Typography>
                  </Grid>
                </> :
                  <Grid item xs={12} sm={6} md={12}><TableLoader /> </Grid>}
                {/* {value.length > 0 && <Grid item xs={12} sm={12} md={2} sx={{ px: 2 }}>
                  <Chip
                    style={{
                      width: '100%',
                      fontWeight: 700,
                      color: topicText,
                    }}
                    size="small"
                    label={wtime}
                  />
                </Grid>} */}
                {/* <Grid
                  item
                  container
                  direction="column"
                  xs={12}
                  sm={12}
                  md={12}
                  sx={{ color: 'white', px: 2 }}>
                  <Typography
                    variant="body2" sx={{ paddingBottom: '8px' }} >
                    Number of User in Queue {value.length > 0 && value.filter((cc: any) => cc._id == _id)[0].count}
                  </Typography>
                  {parseInt(wtime) > 0 && <Chip
                    style={{
                      width: '100%',
                      fontWeight: 700,
                      color: topicText,
                    }}
                    size="small"
                    label={`Waiting Time is ${wtime}`}
                  />}
                </Grid> */}

                <Grid className="bookBtn bookingBtn" item xs={6} sm={6} md={6} sx={{ px: 2 }}>
                  <Button variant="contained" disabled={(!accept || accept == null || accept == undefined) ? false : (accept == true && limit.includes(new Date().getHours()) == true) ? false : true} onClick={() => { handleCurrent() }} fullWidth >

                    Book
                  </Button>
                </Grid>
                <Grid className="appointBtn bookingBtn" item xs={6} sm={6} md={6} sx={{ px: 2 }}>
                  <Button variant="contained" disabled={(!accept || accept == null || accept == undefined) ? false : (accept == true) ? false : true} onClick={() => { handleBook() }} fullWidth>

                    Appointment
                  </Button>
                </Grid>
              </Grid>
            </CardContent>
          </Box>
        </Card>
      </Paper>
      <Dialog className="bookingPopup"
        disableEscapeKeyDown={true} open={open} onClose={() => {
          if (show == true) {

          } else { handleClose() }
        }}
        sx={(theme) => ({
          [theme.breakpoints.up('sm')]: {
            minHeight: '770px !important'
          },
          [theme.breakpoints.up('xs')]: {
            minHeight: '450px !important'
          },
          [theme.breakpoints.up('md')]: {
            minHeight: '770px !important'
          },
        })}
      >
        {open == true && key == true && <DatePicker service={{ label: motivation, value: _id, labelType: name, tagVal: tags, location: locationBook }} close={handleClose} some={true} handleView={handleShow} />}
        {open == true && key == false && <DatePicker service={{ label: motivation, value: _id, labelType: name, tagVal: tags, location: locationBook }} close={handleClose} some={false} handleView={handleShow} />}
      </Dialog>
    </>
  );
};

export default React.memo(BookingCard);
