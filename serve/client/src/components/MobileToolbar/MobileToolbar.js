import React from 'react'
import ToolbarTabs from 'components/Appbar/ToolbarTabs'
import CustomerTabs from 'pages/Customer/AppBar/CustomerTabs'
import ShopTabs from 'pages/Shop/AppBar/ShopTabs'
import { useSelector } from 'react-redux'

const MobileToolbar = () => {

  const auth = useSelector((state) => state.auth.isAuthenticated);

  return (
    <>
      {/* {auth == false ? <ToolbarTabs /> : (auth == true && localStorage.getItem('type') == 'shop') ? <ShopTabs /> : (auth == true && localStorage.getItem('type') == 'customer') ? <CustomerTabs /> : <ToolbarTabs />} */}
      {auth == false && <ToolbarTabs />}
      {auth == true && localStorage.getItem('type') == 'shop' && <ShopTabs />}
      {auth == true && localStorage.getItem('type') == 'customer' && <CustomerTabs />}
      {/* <div style={{ textAlign: 'center', color: 'white' }}>
        {"Heytech IT Service Pvt. Ltd."}
      </div> */}
    </>
  )
}

export default MobileToolbar