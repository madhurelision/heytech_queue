import React from 'react';
import { styled, Grid, Button, useTheme } from '@mui/material';
interface TimeSelectorProps {
  date: Date | null;
  hour: number;
  timeslot: number[];
  setHour: React.Dispatch<React.SetStateAction<number>>;
}
const getDate = (_date: Date | null) => {
  const date = _date ? _date : new Date();
  const text = _date ? 'Selected Date' : 'Todays Date';

  return text + ': ' + date.toLocaleDateString('en-gb');
};

const StyledButton = styled(Button)(({ theme }) => ({
  color: theme.palette.mode == 'dark' ? 'white' : 'black',
  margin: '8px',
  /* border: 1px solid rgb(122 99 240 / 50%); */

  // :hover {
  //   /* border: 1px solid rgb(122 99 240 / 50%); */
  //   /* background: rgb(122 99 240 / 50%); */
  // }
}));

const addZero = (time: number) => {
  if (time > 9) return '';
  return '0';
};

const TimeSelector: React.FC<TimeSelectorProps> = ({
  date,
  setHour,
  timeslot,
}) => {

  const theme = useTheme()

  if (timeslot.length === 0) return <div />;

  return (
    <Grid
      className="timeform"
      style={{
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: theme.palette.mode == 'dark' ? '#303030' : 'white',
        padding: '1rem 2rem',
        height: '360px',
        color: theme.palette.mode == 'dark' ? '#f5f5f5' : 'black',
        overflowY: 'scroll',
      }}>
      <div style={{ fontWeight: 600, marginBottom: '8px' }}>
        {getDate(date)} (24 Hour Format)
      </div>
      {timeslot.map((time, index) => (
        <StyledButton
          variant="outlined"
          key={index}
          disabled={new Date().getDate() == date?.getDate() && date?.getHours() >= time}
          onClick={() => setHour(time)}>
          {`${addZero(time)}${time}:00 `}
        </StyledButton>
      ))}
    </Grid>
  );
};

export default TimeSelector;
