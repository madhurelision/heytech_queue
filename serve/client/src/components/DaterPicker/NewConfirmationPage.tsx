import React, { useCallback, useEffect, useState } from 'react';
import {
  Typography,
  Divider,
  styled,
  TextareaAutosize,
  Button,
  InputBase,
  DialogContent,
  DialogActions,
  Grid,
  Chip,
  Box,
  IconButton,
} from '@mui/material';
import { ReactSelectN } from '../../components/common/index';
import { Link, useNavigate } from 'react-router-dom';
import EventAvailableTwoToneIcon from '@mui/icons-material/EventAvailableTwoTone';
import ScheduleRoundedIcon from '@mui/icons-material/ScheduleRounded';
import Email from '@mui/icons-material/Email';
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import { useRecoilValue } from 'recoil';
import { mentorState } from '../../store/local';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useDispatch, useSelector } from 'react-redux'
import FormError from 'pages/Registration/FormError';
import { bookingCreate, bookingOtpGenerate, bookingQueueCreate, bookingVerify, getAllTag, getAllTagService } from 'redux/action';
import { Close, MobileFriendly, Password, Person } from '@mui/icons-material';
import { keyLengthValidation, setDateTime } from 'config/KeyData'
import ProgressTimer from 'pages/Shop/UsableComponent/ProgressTimer';
import moment from 'moment';
import { uniqueServiceElements } from 'config/uniqueElements';

const TextAreaWrapper = styled(Box)(({ theme }) => ({
  padding: '2px 4px',
  display: 'flex',
  alignItems: 'center',
  border: theme.palette.mode == 'dark' ? '1px solid white' : '1px solid black',
  borderRadius: '4px',
  marginTop: '6px',

  '&:focus-within': {
    border: '1px solid #2684ff',
  },

  '.Search_Input': {
    padding: '0px 6px',
    width: '100%',
  },
  '.MuiInputBase-input': {
    opacity: theme.palette.mode == 'dark' ? 1 : 1,
  },
  margin: '6px 0px'
}));

const TextArea = styled(TextareaAutosize)(({ theme }) => ({
  width: '100%',
  outline: 'none',
  backgroundColor: theme.palette.mode == 'dark' ? '#303030' : 'white',
  color: theme.palette.mode == 'dark' ? '#f5f5f5' : 'black',
  fontSize: '16px',
  borderColor: theme.palette.mode == 'dark' ? 'hsl(0, 0%, 80%)' : 'black',
  resize: 'none',
  borderRadius: '4px',
  '&:focus': {
    border: '1px solid #2684ff'
  }
}));

const StyledButton = styled(Button)(({ theme }) => ({
  backgroundSize: '200%',
  width: '100%',
  fontWeight: 700,
  color: '#fff',
  fontSize: '1rem',
  background: '#d6a354',
  //background-image: linear-gradient(90deg, #C31818, #d18873),
  boxShadow: '0 2px 1px transparent, 0 4px 2px transparent,0 8px 4px transparent, 0 16px 8px transparent, 0 32px 16px transparent',
  transition: ' all 0.8s cubic-bezier(0.32, 1.32, 0.42, 0.68)',
  '&:hover': {
    opacity: 1,
    background: '#139f7d',
    color: '#f2f2f2'
  },
}));
const TagButton = styled(Button)`
  color: #f5f5f5;
  width: 100%;
  font-size: 12px;
  background-image: linear-gradient(90deg, #C31818, #d18873);
  box-shadow: 0 2px 1px transparent, 0 4px 2px transparent,
  0 8px 4px transparent, 0 16px 8px transparent, 0 32px 16px transparent;
  transition: all 0.8s cubic-bezier(0.32, 1.32, 0.42, 0.68);
`;
interface NewConfirmationProps {
  date: Date | null;
  hour: number;
  setHour: React.Dispatch<React.SetStateAction<number>>;
  service: any | null,
  close: Function,
  main: Boolean,
  handleView: Function
}

const NewConfirmation: React.FC<NewConfirmationProps> = ({
  date: date_,
  setHour,
  hour,
  service,
  close,
  main,
  handleView
}) => {
  const { first_name, last_name, email, _id, topics, user_information, waiting_time, shop_name } = useRecoilValue(mentorState);
  const date = date_ ? date_ : new Date();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('md'));
  const userData = useSelector((state: any) => state.auth.profile)
  const [selectedOption, setSelectedOption] = useState<any>({ label: service?.label, value: service?.label });
  const [viewOption, setViewOption] = useState<any>({ label: service?.labelType, value: service?.value });
  const [tagK, setTagK] = useState(service?.tagVal)
  const mentorName = (shop_name == null || shop_name == undefined) ? `${first_name} ${last_name}` : shop_name;
  const dayString = date.toLocaleString('default', {
    weekday: 'short',
    month: 'short',
    day: 'numeric',
  });
  const timeString = (main == false) ? `${hour}:00 - ${hour + 1}:00` : hour

  const [err, setErr] = useState<any>({})
  const dispatch = useDispatch()
  const [otp, setOtp] = useState('')
  const bookingData = useSelector((state: any) => state.auth.bookingData)
  const tagS = useSelector((state: any) => state.auth.tagServiceList)
  const loader = useSelector((state: any) => state.auth.loader)
  const cted = useSelector((state: any) => state.auth.created)
  const doneProcess = useSelector((state: any) => state.auth.complete)
  const [key, setKey] = useState(0)
  const [count, setCount] = useState(0)
  const [show, setShow] = useState({
    open: false,
    date: new Date().setSeconds(120)
  })
  const [tag, setTag] = useState([])

  const [element, setElement] = useState({
    user_id: '',
    user_email: userData?.email,
    service_id: service.value,
    appointment_date: date,
    appointment_time: timeString,
    shop_email: email,
    shop_name: mentorName,
    shop_id: user_information,
    description: '',
    size: '',
    mobile: '',
    time: waiting_time,
    location: service?.location,
    form_type: 'Special'
  })

  useEffect(() => {

    if (tagS.length == 0) {
      dispatch(getAllTagService())
    } else {
      setTag(tagS.tag)
    }

  }, [tagS])

  useEffect(() => {

    if (cted) {
      setKey(1)
      setShow({ ...show, open: true, date: new Date().setSeconds(120) })
      handleView(true)
    }

    if (doneProcess) {
      setKey(2)
      handleView(false)
    }

  }, [cted, doneProcess])


  const filterTopics = (motivation: string) => {
    return uniqueServiceElements(topics).filter((main: any) => main.motivation === motivation);
  };

  const filterTagTopics = (motivation: string) => {
    return tag.filter((main: any) => main._id === motivation);
  };

  const handleChange = (e: any) => {
    setElement({ ...element, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleOption = (data: any) => {
    setSelectedOption(data)
    var ssMai: any = filterTopics(data.label)
    setViewOption({
      label: ssMai[0]?.name,
      value: ssMai[0]?._id
    })
    setElement({ ...element, service_id: ssMai[0]?._id })
    setTagK(ssMai[0]?.tags)
  }

  const handleViewOption = (data: any) => {
    console.log('sg', data)
    setViewOption(data)
    setElement({ ...element, service_id: data.value })
    setTagK(data?.tags)
    setErr({ ...err, service_id: '' })
  }

  const handleSubmit = () => {

    const check = validateForm(element)
    setErr(check)
    console.log('Err', check)
    if (Object.keys(check).length > 0) return

    var file = {
      newtime: 0,
      approximate_time: ''
    }

    var main_key = {
      created_by: (Object.keys(userData).length == 0) ? null : localStorage.getItem('type') == 'shop' ? userData?.user_information : userData?._id,
      create_type: localStorage.getItem('type') == null ? 'global' : localStorage.getItem('type')
    }

    // console.log('mainCheck', selectedOption)

    const valCheck: any = topics.filter((cc: any) => cc._id == viewOption.value)

    //console.log('mainCheck', valCheck, parseInt(valCheck[0].waiting_number), selectedOption)
    file.approximate_time = parseInt(valCheck[0].waiting_number) * parseInt(element.size) + ' ' + valCheck[0].waiting_key
    file.newtime = (valCheck[0].waiting_key == 'min') ? parseInt(valCheck[0].waiting_number) * parseInt(element.size) : parseInt(valCheck[0].waiting_number) * parseInt(element.size) * 60
    // console.log('mai', file)

    if (main == false) {
      dispatch(bookingCreate({ ...element, ...main_key }))
    } else {
      dispatch(bookingQueueCreate({ ...element, ...file, ...main_key }))
    }

  }

  const handleVerify = () => {
    if (otp == null || otp == undefined || otp == "") {
      setErr({ ...err, otp: 'Otp is Required' })
      return
    }

    if (Object.keys(bookingData).length == 0) return

    dispatch(bookingVerify(bookingData?._id, { otp: otp }))
  }

  const handleOtp = (e: any) => {
    setOtp(e.target.value);
    setErr({ ...err, otp: '' });
  }

  // const allowOnlyNumericsOrDigits = (e: any) => {
  //   const charCode = e.which ? e.which : e.keyCode;

  //   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
  //     setErr({ ...err, size: 'OOPs! Only numeric values or digits allowed' });
  //   }
  // }

  const handleMainCloseD = () => {
    close()
    //dispatch({ type: LOADERCLOSE })
  }

  const history = useNavigate()

  const handleTagBtn = (item: any) => {
    setElement({ ...element, description: element.description.length == 0 ? element.description.concat(item) : element.description.concat(' ', item) })
    setErr({ ...err, description: '' })
  }

  const handleResendOtp = () => {
    if (Object.keys(bookingData).length == 0) return
    setCount(count + 1)
    setShow({ ...show, open: true, date: new Date().setSeconds(120) })
    dispatch(bookingOtpGenerate(bookingData?._id))
  }

  const handleCloseTimer = useCallback((val: any) => {
    setShow({ ...show, open: false })
  }, [show])

  const handleChangeFormS = () => {
    setKey(0);
    setCount(0);
  }

  return (
    <>
      {
        key == 0 && <div className='bookingform'>
          <Box display="flex" alignItems="center">
            <Box flexGrow={1}>
              <Typography variant="h5" sx={{ fontWeight: 800, p: '8px 0px' }}>
                {main == false ? 'Confirm your Appointment' : 'Confirm your Queue '}
              </Typography></Box>
            <Box>
              <IconButton onClick={() => { handleMainCloseD() }}>
                <Close />
              </IconButton>
            </Box>
          </Box>
          <Typography variant="body1" style={{ opacity: 0.8, fontWeight: 700 }}>
            {viewOption?.label} session with{' '}
            <Link to="/" style={{ color: 'grey' }}>
              {mentorName}
            </Link>
          </Typography>
          <div style={{ display: 'flex', fontWeight: 600, padding: '0.5rem 0rem' }}>
            <EventAvailableTwoToneIcon color="action" />
            <span style={{ padding: '0px 1rem' }}>{dayString}</span>
            <ScheduleRoundedIcon color="action" />
            {/* <span style={{ padding: '0px 1rem' }}>{timeString} {main == false ? hour < 12 ? 'AM' : 'PM' : (date.getHours() < 12) ? 'AM' : 'PM'}</span> */}
            <span style={{ padding: '0px 1rem' }}>{main == false ? moment(setDateTime(date, hour)).calendar() : moment(date).calendar()}</span>
          </div>
          <Divider style={{ margin: '0.5rem 0rem' }} />

          <Grid
            container
            // direction="row"
            //justifyContent="center"
            // maxHeight="400px"
            spacing={2}
          >
            <Grid item xs={12} md={6}>
              <label style={{ fontWeight: 500 }}>Select Service Type</label>
              <ReactSelectN
                menuPlacement="auto"
                sx={{
                  margin: '6px 0px',
                  '.select__control': {
                    border: theme.palette.mode == 'dark' ? '1px solid white' : '1px solid black',
                    background: 'transparent !important',
                  },
                  '.select__control:hover': {
                    border: theme.palette.mode == 'dark' ? '1px solid white !important' : '1px solid black !important',
                  },
                }}
                name="Service"
                value={selectedOption}
                options={[...new Set(topics.map((cc: any) => cc.motivation))].map((ss: any) => {
                  return {
                    label: ss,
                    value: ss
                  }
                })}
                onChange={(e) => { handleOption(e) }}
                isSearchable={matches}
                classNamePrefix="select"
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <label style={{ fontWeight: 500 }}>Select Service</label>
              <ReactSelectN
                menuPlacement="auto"
                sx={{
                  margin: '6px 0px',
                  '.select__control': {
                    border: theme.palette.mode == 'dark' ? '1px solid white' : '1px solid black',
                    background: 'transparent !important',
                  },
                  '.select__control:hover': {
                    border: theme.palette.mode == 'dark' ? '1px solid white !important' : '1px solid black !important',
                  },
                }}
                name="Service"
                value={viewOption}
                options={filterTopics(selectedOption.label).map((cc: any) => {
                  return {
                    label: cc.name,
                    value: cc._id,
                    tags: cc.tags
                  }
                })}
                onChange={(e) => { handleViewOption(e) }}
                isSearchable={matches}
                classNamePrefix="select"
              />
              {err.service_id && (<FormError data={err.service_id}></FormError>)}
            </Grid>
            <Grid item xs={12} md={6}>
              <label style={{ fontWeight: 500 }}>Add your email id</label>
              <TextAreaWrapper>
                <Email sx={{ color: theme.palette.mode == 'dark' ? 'white' : 'black' }} />
                <InputBase
                  value={element.user_email}
                  onChange={handleChange}
                  name="user_email"
                  type="email"
                  className="Search_Input"
                  placeholder="Get invite link in you mail"
                  inputProps={{
                    'aria-label': 'Enter Email address to recieve invite link',
                  }}
                />
              </TextAreaWrapper>
              {err.user_email && (<FormError data={err.user_email}></FormError>)}
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <label style={{ fontWeight: 500 }}>Mobile Number</label>
              <TextAreaWrapper>
                <MobileFriendly sx={{ color: theme.palette.mode == 'dark' ? 'white' : 'black' }} />
                <InputBase
                  value={element.mobile}
                  onChange={handleChange}
                  name="mobile"
                  type="tel"
                  className="Search_Input"
                  placeholder="Enter Mobile Number"
                  inputProps={{
                    'aria-label': 'Enter Mobile Number',
                    maxLength: keyLengthValidation.mobile
                  }}
                />
              </TextAreaWrapper>
              {err.mobile && (<FormError data={err.mobile}></FormError>)}
            </Grid>
            <Grid item xs={12} sm={6} md={6}>

              <label style={{ fontWeight: 500 }}>User Size</label>
              <TextAreaWrapper>
                <Person sx={{ color: theme.palette.mode == 'dark' ? 'white' : 'black' }} />
                <InputBase
                  value={element.size}
                  onChange={handleChange}
                  //onKeyUp={allowOnlyNumericsOrDigits}
                  name="size"
                  type="number"
                  className="Search_Input"
                  placeholder="User Size"
                  inputProps={{
                    'aria-label': 'Enter User Size',
                    min: "0", step: "1", max: keyLengthValidation.size
                  }}
                />
              </TextAreaWrapper>
              {err.size && (<FormError data={err.size}></FormError>)}
            </Grid>
          </Grid>
          <label style={{ fontWeight: 500 }}>Description</label>
          <div style={{ margin: '6px 0px' }}>
            <TextArea
              aria-label="minimum height"
              value={element.description}
              onChange={handleChange}
              name="description"
              minRows={3}
              maxRows={5}
              maxLength={keyLengthValidation.description}
              placeholder="Tips on getting booking accepted &#13;&#10; · Keep your questions specific &#13;&#10;
         · Share your goal for session "
            />

            {err.description && (<FormError data={err.description}></FormError>)}
          </div>
          {/* {tag.length > 0 && <>
            {tag.map((cc: any) => {
              return (
                <Chip key={cc._id} size="small" onClick={() => { handleTagBtn(cc.type) }} label={cc.name} variant="outlined"></Chip>
              )
            })}
          </>} */}
          {
            (tagK !== "" || tagK !== null || tagK !== undefined) && <>
              {filterTagTopics(tagK).map((cc: any, i: any) => {
                return <div key={i}>
                  {cc.tag.map((dd: any, i: any) => {
                    return (
                      <Chip key={dd} size="small" onClick={() => { handleTagBtn(dd) }} label={dd} variant="outlined"></Chip>
                    )
                  })}
                </div>
              })}
            </>
          }
          <StyledButton className='confirmBtn' sx={{ marginTop: '10px' }} disabled={loader ? true : false} onClick={() => { handleSubmit() }}>
            {/*"Confirm your Booking"*/}
            {main == false ? 'Confirm your Appointment' : 'Confirm your Queue'}
          </StyledButton>
          {main == false && <div style={{ marginTop: '0.2rem' }}>
            <Button
              onClick={() => setHour(-1)}
              startIcon={<KeyboardBackspaceIcon />}
              sx={{
                margin: 'auto',
                width: '100%',
                color: '#f5f5f5',
                fontWeight: 700,
              }}>
              Change Date or Time
            </Button>
          </div>}
        </div>
      }
      {
        key == 1 && <Grid container spacing={2}>
          <Grid item xs={12} sm={12} md={12}>
            <div className='otpform' >
              <Box display="flex" alignItems="center">
                <Box flexGrow={1}>
                  <Typography variant="h5" sx={{ fontWeight: 800, p: '8px 0px' }}>
                    Verify your Otp
                  </Typography>
                </Box>
                <Box>
                  <IconButton onClick={() => { handleMainCloseD() }}>
                    <Close />
                  </IconButton>
                </Box>
              </Box>
              <label style={{ fontWeight: 500 }}>Otp</label>
              <Button sx={{ textTransform: 'capitalize' }} disabled={(loader || count == 2 || show.open)} startIcon={<Password />} onClick={() => { handleResendOtp() }} >
                Resend Otp
              </Button>
              {show.open == true && <ProgressTimer time={show.date} ma={'hello'} func={handleCloseTimer} show={false} />}
              <div>{'Verifying on '}<span className="booking_number_high" >{'+91' + element.mobile}</span> <Button sx={{ textTransform: 'capitalize' }} onClick={() => { handleChangeFormS() }} >
                Change
              </Button> </div>
              <TextAreaWrapper>
                <Password sx={{ color: theme.palette.mode == 'dark' ? 'white' : 'black' }} />
                <InputBase
                  value={otp}
                  onChange={(e) => { handleOtp(e) }}
                  name="otp"
                  className="Search_Input"
                  placeholder="Enter Otp"
                  inputProps={{
                    'aria-label': 'Enter Otp',
                    maxLength: keyLengthValidation.otp
                  }}
                />
              </TextAreaWrapper>
              {err.otp && (<FormError data={err.otp}></FormError>)}
              <StyledButton disabled={loader ? true : false} onClick={() => { handleVerify() }}>
                Complete Booking
              </StyledButton>
            </div>
          </Grid>
        </Grid>
      }

      {
        key == 2 && <Grid container spacing={2}>
          <Grid item xs={12} sm={12} md={12}>
            <div className='confirmform'>
              <Box display="flex" alignItems="center">
                <Box flexGrow={1}>
                  <Typography className={`bookingSucss ${matches ? 'success-title' : 'success-titles'} `} variant="h5" sx={{ fontWeight: 800, p: '8px 0px' }}>
                    Booking Completed Successfully
                  </Typography>
                </Box>
                <Box>
                  <IconButton onClick={() => { handleMainCloseD() }}>
                    <Close />
                  </IconButton>
                </Box>
              </Box>
              <DialogActions>
                <Button fullWidth variant="contained" color="primary" onClick={() => {
                  if (localStorage.getItem('auth-token') == null) {
                    history('/')
                  } else if (localStorage.getItem('type') == "customer") {
                    history('/customer')
                  } else {
                    handleMainCloseD()
                  }
                }} >View</Button>
                <Button fullWidth variant="contained" color="primary" onClick={handleMainCloseD}>No</Button>
              </DialogActions>
            </div>
          </Grid>
        </Grid>
      }
    </>
  );
};

export default NewConfirmation;


const validateForm = (val: any) => {
  let errors: any = {}

  if (!val.service_id) {
    errors['service_id'] = "Service is Required";
  }

  if (!val.description) {
    errors['description'] = "Description is Required";
  }

  if (!val.user_email && !val.mobile) {
    errors['user_email'] = "User Email is Required";
    errors['mobile'] = "Mobile Number is Required";
  }

  if (val.user_email) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    if (!pattern.test(val["user_email"])) {
      errors["user_email"] = "Please enter valid email.";
    }
  }

  if (!val.size) {
    errors['size'] = "User Size is Required";
  }

  if (val.size) {
    if (parseInt(val.size) > keyLengthValidation.size) {
      errors['size'] = `User Size must be less then or equal to ${keyLengthValidation.size}`;
    }
  }

  if (!val.user_email) {
    if (!val.mobile) {
      errors['mobile'] = "Mobile Number is Required";
    }

    if (val.mobile) {
      if (!val["mobile"].match(/^[0-9]{10}$/)) {
        errors["mobile"] = "Please enter valid mobile no.";
      }
    }
  }

  return errors
}
