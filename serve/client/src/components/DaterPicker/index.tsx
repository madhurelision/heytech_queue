// @ts-nocheck
import React, { useState } from 'react';
import Grid from '@mui/material/Grid';
import Calendar from './Calendar';
import TimeSelector from './TimeSelector';
import Confirmation from './ConfirmationPage';
import NewConfirmation from './NewConfirmationPage';
import { useTheme } from '@mui/material'
import { useSelector } from 'react-redux';
const Scheduler = ({ service, close, some, handleView }) => {

  const theme = useTheme()
  const [timeslot, setTimeslot] = useState<number[]>([]);
  const [date, setDate] = useState<Date | null>(some == true ? new Date() : null);
  const [hour, setHour] = useState<number>(some == true ? new Date().getHours() + ':' + new Date().getMinutes() : -1);
  const hostD = useSelector((state: any) => state.auth.hostName);

  return (
    <>
      {hour === -1 && (
        <>
          <Grid
            container
            // direction="row"
            //justifyContent="center"
            // maxHeight="400px"
            spacing={2}
          >
            <Grid item xs={12} sm={12} md={timeslot.length == 0 ? 12 : 6}>
              <Calendar date={date} setDate={setDate} setTimeslot={setTimeslot} />
            </Grid>
            {/* <Divider orientation="vertical" flexItem /> */}
            {timeslot.length > 0 && <Grid item xs={12} sm={12} md={6}>
              <TimeSelector
                date={date}
                hour={hour}
                setHour={setHour}
                timeslot={timeslot}
              />
            </Grid>}
          </Grid>
        </>
      )}
      {hour !== -1 && (
        <>
          <Grid
            container
            alignItems="center"
            direction="column"
            style={{
              padding: '0.7rem',
              backgroundColor: theme.palette.mode == 'dark' ? 'rgb(48 48 48)' : 'white',
              color: theme.palette.mode == 'dark' ? '#f5f5f5' : 'black',
            }}>
            {/* {Object.keys(hostD).length == 0 ? <Confirmation date={date} hour={hour} setHour={setHour} service={service} close={close} main={some} handleView={handleView} /> : (hostD.type == 2) ? <Confirmation date={date} hour={hour} setHour={setHour} service={service} close={close} main={some} handleView={handleView} /> : <NewConfirmation date={date} hour={hour} setHour={setHour} service={service} close={close} main={some} handleView={handleView} />} */}
            <Confirmation date={date} hour={hour} setHour={setHour} service={service} close={close} main={some} handleView={handleView} />
          </Grid>
        </>
      )}
    </>
  );
};

export default Scheduler;
