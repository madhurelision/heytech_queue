import React from 'react';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import { styled, useTheme } from '@mui/material/styles';

const LoaderWrapper = styled('div')(({ theme }) => ({
  height: '100vh',
  width: '100vw',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: theme.palette.mode == 'dark' ? '#1e1e1e' : 'white',
}));

const LoadingComponent = () => {

  const theme = useTheme()

  return (
    <LoaderWrapper>
      <Loader type="Grid" color={theme.palette.mode == 'dark' ? "#f5f5f5" : '#1e1e1e'} height={80} width={80} />
    </LoaderWrapper>
  )
};

export default LoadingComponent;
