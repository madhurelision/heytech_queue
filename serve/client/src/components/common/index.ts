import { Link as Link_ } from 'react-router-dom';
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import Reactselect from 'react-select';
import { ButtonBase, Autocomplete, Switch } from '@mui/material';

export const Link = styled(Link_)({ color: '#C31818', textDecoration: 'none' });

export const ReactSelect = styled(Reactselect)(({ theme }) => ({
  width: '100%',
  '.select__single-value': {
    color: '#444',
  },
  '.select__placeholder': {
    color: '#444',
  },
  '.select__control': {
    borderColor: 'transparent',
    cursor: 'pointer',
    backgroundColor: '#fff',
    border: '1px solid #a6a2a2',
  },
  '.select__input-container': {
    color: '#444',
    fontSize: '15px',
    padding: '7px 0',
  },
  '.select__control:hover': {
    borderColor: '#a6a2a2',
  },
  '.select__menu': {
    backgroundColor: '#fff',
    textTransform: 'capitalize',
    color: '#444',
    fontSize: '15px',
    margin: '0',
  },
  '.select__option--is-focused': {
    backgroundColor: '#424040',
    color: '#fff',
  },
  '.select__option--is-focused:hover': {
    backgroundColor: '#424040',
    color: '#fff',
  },
}));

export const ReactSelectN = styled(Reactselect)(({ theme }) => ({
  width: '100%',
  '.select__single-value': {
    color: theme.palette.mode == 'dark' ? '#fff' : 'black',
  },
  '.select__placeholder': {
    color: theme.palette.mode == 'dark' ? '#fff' : 'black',
  },
  '.select__control': {
    borderColor: 'transparent',
    cursor: 'pointer',
    backgroundColor: '#d6a354',
  },
  '.select__input-container': {
    color: theme.palette.mode == 'dark' ? '#fff' : 'black',
  },
  '.select__control:hover': {
    borderColor: 'transparent',
  },
  '.select__menu': {
    backgroundColor: theme.palette.mode == 'dark' ? '#272626' : 'white',
    textTransform: 'capitalize'
  },
  '.select__option--is-focused': {
    backgroundColor: '#424040',
  },
  '.select__option--is-focused:hover': {
    backgroundColor: '#424040',
  },
}));

export const StyledButton = styled(Button)(({ theme }) => ({
  //background: 'rgb(59,57,57,0.4)',
  background: '#d6a354',
  textTransform: 'none',
  color: '#fff',
  fontWeight: 500,
  fontSize: '15px',
  // Margin: '1rem',

  [theme.breakpoints.up('xs')]: {
    padding: '12px 6px',
  },
  [theme.breakpoints.up('sm')]: {
    padding: '12px 16px',
  },

  '&:hover': {
    opacity: 1,
    color: '#d6a354',
  },
}));

export const CenteredDiv = styled('div')`
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;


export const ImageButton = styled(ButtonBase)(({ theme }) => ({
  position: 'relative',
  color: theme.palette.mode == 'dark' ? '#ffff' : 'black',
  lineHeight: 1.75,
  padding: '10px 6px',
  // verticalAlign: 'baseline',
  // [theme.breakpoints.down('sm')]: {
  //   width: '100% !important', 
  // },
  '&:hover, &.Mui-focusVisible': {
    zIndex: 1,
    color: '#d6a354',
    '& .MuiImageBackdrop-root': {
      opacity: 0.15,
    },
    '& .MuiImageMarked-root': {
      opacity: 0,
    },
    '& .MuiTypography-root': {
      border: '4px solid currentColor',
    },
  },
}));

export const BookingButton = styled(Button)(({ theme }) => ({
  background: '#d6a354',
  textTransform: 'none',
  color: '#fff',
  fontWeight: 500,
  fontSize: '16px',
  // Margin: '1rem',

  [theme.breakpoints.up('xs')]: {
    padding: '12px 6px',
  },
  [theme.breakpoints.up('sm')]: {
    padding: '12px 16px',
  },

  '&:hover': {
    opacity: 1,
    background: '#e3be85',
    color: '#fff'
  },
}));


export const SearchBarber = styled(Autocomplete)(({ theme }) => ({
  background: '#d6a354',
  color: 'black',
  '.Mui-focused': {
    color: 'black',
  },
  '.MuiInputLabel-root': {
    fontSize: '1.2rem',
    color: 'white',
    left: '-6px !important',
    top: '-7px !important'
  },
  '.MuiOutlinedInput-root': {
    padding: '4px !important',
    color: 'white',
  }
}))

export const NewSearchBarber = styled(Autocomplete)(({ theme }) => ({
  background: '#ffff',
  color: 'black',
  borderRadius: '4px',
  border: '1px solid #252525',
  height: '44px',
  '.Mui-focused': {
    color: '#ffff',
    borderRadius: '4px',
  },
  '.MuiInputLabel-root': {
    fontSize: '1.2rem',
    color: 'black',
    left: '-6px !important',
    top: '-7px !important'
  },
  '.MuiOutlinedInput-root': {
    padding: '0px !important',
    color: 'black',
    fontSize: 'larger'
  },
  '.MuiOutlinedInput-notchedOutline': {
    borderRadius: '10px',
  }
}))

export const NewReactSelect = styled(Reactselect)(({ theme }) => ({
  width: '100%',
  '.select__single-value': {
    color: '#212529',
  },
  '.select__placeholder': {
    color: '#212529',
  },
  '.select__control': {
    borderColor: '#252525',
    cursor: 'pointer',
    backgroundColor: '#fff',
    borderRadius: '40px'
  },
  '.select__input-container': {
    color: 'black',
  },
  '.select__control:hover': {
    borderColor: '#252525',
  },
  '.select__menu': {
    backgroundColor: theme.palette.mode == 'dark' ? '#272626' : 'white',
    textTransform: 'capitalize'
  },
  '.select__option--is-focused': {
    backgroundColor: '#424040',
  },
  '.select__option--is-focused:hover': {
    backgroundColor: '#424040',
  },
}));

export const ShopSelect = styled(Reactselect)(({ theme }) => ({
  width: '100%',
  '.select__single-value': {
    color: 'black',
  },
  '.select__placeholder': {
    color: 'black',
  },
  '.select__control': {
    borderColor: '#252525',
    cursor: 'pointer',
    backgroundColor: '#fff',
    minHeight: '44px',
  },
  '.select__input-container': {
    color: 'black',
  },
  '.select__control:hover': {
    borderColor: '#252525',
  },
  '.select__menu': {
    backgroundColor: theme.palette.mode == 'dark' ? 'black' : 'white',
    fontSize: '15px',
    textTransform: 'capitalize',
    color: theme.palette.mode == 'dark' ? '#fff' : '#444',
  },
  '.select__option--is-focused': {
    backgroundColor: '#424040',
    color: '#fff'
  },
  '.select__option--is-focused:hover': {
    backgroundColor: '#424040',
    color: '#fff'
  },
  '.listbox': {
    fontSize: '15px',
    color: '#fff',
    backgroundColor: 'black'
  }
}));

export const ShopSelectN = styled(Reactselect)(({ theme }) => ({
  width: '100%',
  '.select__single-value': {
    color: 'black',
  },
  '.select__placeholder': {
    color: 'black',
  },
  '.select__control': {
    borderColor: '#252525',
    cursor: 'pointer',
    backgroundColor: '#fff',
    borderRadius: '4px',
    minHeight: '44px',
  },
  '.select__input-container': {
    color: 'black',
  },
  '.select__control:hover': {
    borderColor: '#252525',
  },
  '.select__menu': {
    color: theme.palette.mode == 'dark' ? '#fff' : '#444',
    backgroundColor: theme.palette.mode == 'dark' ? 'black' : 'white',
    fontSize: '15px',
    textTransform: 'capitalize'
  },
  '.select__option--is-focused': {
    backgroundColor: '#424040',
    color: '#fff'
  },
  '.select__option--is-focused:hover': {
    backgroundColor: '#424040',
    color: '#fff'
  },
}));

export const ServiceButton = styled(Button)(({ theme }) => ({
  background: '#d6a354',
  textTransform: 'none',
  borderRadius: '30px',
  color: '#fff',
  fontWeight: 500,
  padding: '10px 35px',
  [theme.breakpoints.up('xs')]: {
    marginBottom: '12px',
  },
  [theme.breakpoints.up('sm')]: {
    marginBottom: '12px',
  },
  [theme.breakpoints.up('md')]: {
    marginBottom: '5px',
  },
  '&:hover': {
    opacity: 1,
    color: '#d6a354',
  },
}));

export const AntSwitch = styled(Switch)(({ theme }) => ({
  width: 28,
  height: 16,
  padding: 0,
  display: 'flex',
  '&:active': {
    '& .MuiSwitch-thumb': {
      width: 15,
    },
    '& .MuiSwitch-switchBase.Mui-checked': {
      transform: 'translateX(9px)',
    },
  },
  '& .MuiSwitch-switchBase': {
    padding: 2,
    '&.Mui-checked': {
      transform: 'translateX(12px)',
      color: '#fff',
      '& + .MuiSwitch-track': {
        opacity: 1,
        backgroundColor: theme.palette.mode === 'dark' ? '#177ddc' : '#1890ff',
      },
    },
  },
  '& .MuiSwitch-thumb': {
    boxShadow: '0 2px 4px 0 rgb(0 35 11 / 20%)',
    width: 12,
    height: 12,
    borderRadius: 6,
    transition: theme.transitions.create(['width'], {
      duration: 200,
    }),
  },
  '& .MuiSwitch-track': {
    borderRadius: 16 / 2,
    opacity: 1,
    backgroundColor:
      theme.palette.mode === 'dark' ? 'rgba(255,255,255,.35)' : 'rgba(0,0,0,.25)',
    boxSizing: 'border-box',
  },
}));

export const MaterialUISwitch = styled(Switch)(({ theme }) => ({
  width: 62,
  height: 34,
  padding: 7,
  '& .MuiSwitch-switchBase': {
    margin: 1,
    padding: 0,
    transform: 'translateX(6px)',
    '&.Mui-checked': {
      color: '#fff',
      transform: 'translateX(22px)',
      '& .MuiSwitch-thumb:before': {
        backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 20 20"><path fill="${encodeURIComponent(
          '#fff',
        )}" d="M4.2 2.5l-.7 1.8-1.8.7 1.8.7.7 1.8.6-1.8L6.7 5l-1.9-.7-.6-1.8zm15 8.3a6.7 6.7 0 11-6.6-6.6 5.8 5.8 0 006.6 6.6z"/></svg>')`,
      },
      '& + .MuiSwitch-track': {
        opacity: 1,
        backgroundColor: theme.palette.mode === 'dark' ? '#8796A5' : '#aab4be',
      },
    },
  },
  '& .MuiSwitch-thumb': {
    backgroundColor: theme.palette.mode === 'dark' ? '#003892' : '#001e3c',
    width: 32,
    height: 32,
    '&:before': {
      content: "''",
      position: 'absolute',
      width: '100%',
      height: '100%',
      left: 0,
      top: 0,
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 20 20"><path fill="${encodeURIComponent(
        '#fff',
      )}" d="M9.305 1.667V3.75h1.389V1.667h-1.39zm-4.707 1.95l-.982.982L5.09 6.072l.982-.982-1.473-1.473zm10.802 0L13.927 5.09l.982.982 1.473-1.473-.982-.982zM10 5.139a4.872 4.872 0 00-4.862 4.86A4.872 4.872 0 0010 14.862 4.872 4.872 0 0014.86 10 4.872 4.872 0 0010 5.139zm0 1.389A3.462 3.462 0 0113.471 10a3.462 3.462 0 01-3.473 3.472A3.462 3.462 0 016.527 10 3.462 3.462 0 0110 6.528zM1.665 9.305v1.39h2.083v-1.39H1.666zm14.583 0v1.39h2.084v-1.39h-2.084zM5.09 13.928L3.616 15.4l.982.982 1.473-1.473-.982-.982zm9.82 0l-.982.982 1.473 1.473.982-.982-1.473-1.473zM9.305 16.25v2.083h1.389V16.25h-1.39z"/></svg>')`,
    },
  },
  '& .MuiSwitch-track': {
    opacity: 1,
    backgroundColor: theme.palette.mode === 'dark' ? '#8796A5' : '#aab4be',
    borderRadius: 20 / 2,
  },
}));