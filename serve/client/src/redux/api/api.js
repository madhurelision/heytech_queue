import axios from 'axios'
import { INVENTORY_URL, PAYMENT_URL, SERVER_URL } from '../../config.keys'

// Shop
export const ShopProfile = () => axios.get(`${SERVER_URL}/api/shop/profile`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllShopService = (skip, limit) => axios.get(`${SERVER_URL}/api/shop/getService/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const createShopService = (data) => axios.post(`${SERVER_URL}/api/shop/createService`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token'),
    "Content-Type": 'multipart/form'
  }
})

export const updateShopService = (id, data) => axios.post(`${SERVER_URL}/api/shop/updateService/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token'),
    "Content-Type": 'multipart/form'
  }
})

export const updateShopProfile = (data) => axios.post(`${SERVER_URL}/api/shop/updateShop`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token'),
    "Content-Type": 'multipart/form'
  }
})

// Customer
export const CustomerProfile = () => axios.get(`${SERVER_URL}/api/user/profile`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const updateCustomerProfile = (data) => axios.post(`${SERVER_URL}/api/user/updateCustomer`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token'),
    "Content-Type": 'multipart/form'
  }
})

export const createCustomerMobile = (id, data) => axios.post(`${SERVER_URL}/api/user/createCustomerOtp/` + id, data)
export const verifyCustomerMobile = (id, data) => axios.post(`${SERVER_URL}/api/user/verifyCustomerOtp/` + id, data)

/// 
export const AllService = (skip, limit) => axios.get(`${SERVER_URL}/api/getAllService/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllExpertise = () => axios.get(`${SERVER_URL}/api/getAllExpertise`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllShopGallery = () => axios.get(`${SERVER_URL}/api/shop/getGallery`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const createShopBooking = (data) => axios.post(`${SERVER_URL}/api/booking/createBooking`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const verifyShopBooking = (id, data) => axios.post(`${SERVER_URL}/api/booking/verifyBooking/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllShopBooking = () => axios.get(`${SERVER_URL}/api/shop/getAllBooking`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllCustomerBooking = () => axios.get(`${SERVER_URL}/api/user/getAllBooking`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const waitingShopBooking = (id, data) => axios.post(`${SERVER_URL}/api/shop/addWaitingTime/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const remarkShopBooking = (id, data) => axios.post(`${SERVER_URL}/api/shop/addRemark/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const seatShopBooking = (id, data) => axios.post(`${SERVER_URL}/api/shop/addbookingSeat/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const QueueWaitingShopBooking = (id, data) => axios.post(`${SERVER_URL}/api/shop/addQueueWaitingTime/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const singleBookingDetail = (id) => axios.get(`${SERVER_URL}/api/booking/getSingleBooking/` + id)

export const AllShopCurrentBooking = (skip, limit) => axios.get(`${SERVER_URL}/api/shop/getCurrentBooking/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllCustomerCurrentBooking = (skip, limit) => axios.get(`${SERVER_URL}/api/user/getCurrentBooking/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const createShopQueueBooking = (data) => axios.post(`${SERVER_URL}/api/booking/createBookingQueue`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllShopQueue = (skip, limit) => axios.get(`${SERVER_URL}/api/shop/getQueueBooking/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllCustomerBookingCount = () => axios.get(`${SERVER_URL}/api/user/bookingCount`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const ShopLike = (id, data) => axios.post(`${SERVER_URL}/api/likeShopUser/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const ShopCompleteBook = (data) => axios.post(`${SERVER_URL}/api/shop/completeBooking`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const ShopUnlike = (id, data) => axios.post(`${SERVER_URL}/api/unlikeShopUser/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const ShopReview = (data) => axios.post(`${SERVER_URL}/api/shop/makeReview`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const CustomerReview = (data) => axios.post(`${SERVER_URL}/api/user/makeReview`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const createShopGallery = (data) => axios.post(`${SERVER_URL}/api/shop/createGallery`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token'),
    "Content-Type": 'multipart/form'
  }
})

export const updateShopGallery = (id, data) => axios.post(`${SERVER_URL}/api/shop/editGallery/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token'),
    "Content-Type": 'multipart/form'
  }
})

export const deleteShopGallery = (id) => axios.get(`${SERVER_URL}/api/shop/deleteGallery/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const cancelShopBooking = (id) => axios.get(`${SERVER_URL}/api/shop/cancelBooking/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const cancelCustomerBooking = (id) => axios.get(`${SERVER_URL}/api/user/cancelBooking/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const verifyCustomerCancelBooking = (id, data) => axios.post(`${SERVER_URL}/api/user/verifyCancelBooking/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const shopQueryBooking = (data, skip, limit) => axios.post(`${SERVER_URL}/api/shop/getQueryBooking/` + skip + '/' + limit, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const customerQueryBooking = (data, skip, limit) => axios.post(`${SERVER_URL}/api/user/getQueryBooking/` + skip + '/' + limit, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const createShopCode = () => axios.get(`${SERVER_URL}/api/shop/Qrcode`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const updateShopServiceStatus = (id, data) => axios.post(`${SERVER_URL}/api/shop/updateServiceStatus/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const updateShopQueueStatus = (id, data) => axios.post(`${SERVER_URL}/api/shop/updateQueueStatus/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const updateShopOpenStatus = (id, data) => axios.post(`${SERVER_URL}/api/shop/updateOpenStatus/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const updateShopAppointmentStatus = (id, data) => axios.post(`${SERVER_URL}/api/shop/updateAppointmentStatus/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllTag = () => axios.get(`${SERVER_URL}/api/getAllTag`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllCustomerNotification = (skip, limit) => axios.get(`${SERVER_URL}/api/user/notification/${skip}/${limit}`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllCustomerViewNotification = () => axios.get(`${SERVER_URL}/api/user/view`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllReviewTag = () => axios.get(`${SERVER_URL}/api/getAllReviewTag`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const createShopMobile = (id, data) => axios.post(`${SERVER_URL}/api/shop/createShopOtp/` + id, data)
export const verifyShopMobile = (id, data) => axios.post(`${SERVER_URL}/api/shop/verifyShopOtp/` + id, data)

export const AllShopTextQ = (data) => axios.get(`${SERVER_URL}/api/getShopQuery/` + data,)

export const AllShopRegexQ = (data) => axios.get(`${SERVER_URL}/api/getShopRegexQuery/` + data)

export const AllShopListView = () => axios.get(`${SERVER_URL}/api/getAllShopsList`)

export const AllShopNotification = (skip, limit) => axios.get(`${SERVER_URL}/api/shop/notification/${skip}/${limit}`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllShopViewNotification = () => axios.get(`${SERVER_URL}/api/shop/view`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})


export const createBookingOtpMobile = (id) => axios.post(`${SERVER_URL}/api/booking/generateOtpBooking/` + id)

export const createShopRegistrationService = (data) => axios.post(`${SERVER_URL}/api/shop/createService`, data, {
  headers: {
    'auth-token': localStorage.getItem('registration-token'),
    "Content-Type": 'multipart/form'
  }
})


export const updateShopProfileService = (id, data) => axios.post(`${SERVER_URL}/api/shop/updateServiceShop/` + id, data)

export const updateShopProfileTimeslot = (id, data) => axios.post(`${SERVER_URL}/api/shop/updateTimeslotShop/` + id, data)

export const updateShopKyc = (id, data) => axios.post(`${SERVER_URL}/api/shop/shopKyc/` + id, data, {
  headers: {
    "Content-Type": 'multipart/form'
  }
})

export const updateShopProfileKyc = (data) => axios.post(`${SERVER_URL}/api/shop/shopProfileKyc`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token'),
    "Content-Type": 'multipart/form'
  }
})

export const createShopEmployee = (data) => axios.post(`${SERVER_URL}/api/shop/createEmployee`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const updateShopEmployee = (id, data) => axios.post(`${SERVER_URL}/api/shop/editEmployee/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const updateShopEmployeeStatus = (id, data) => axios.post(`${SERVER_URL}/api/shop/updateEmployeeStatus/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const deleteShopEmployee = (id) => axios.get(`${SERVER_URL}/api/shop/deleteEmployee/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllShopEmployee = () => axios.get(`${SERVER_URL}/api/shop/getEmployee`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const shopHistoryQuery = (id) => axios.get(`${SERVER_URL}/api/shop/getBookQuery/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const customerHistoryQuery = (id) => axios.get(`${SERVER_URL}/api/user/getBookQuery/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

/// employee
export const employeeLoginWithId = (data) => axios.post(`${SERVER_URL}/api/employeeLoginId`, data)

export const EmployeeProfile = () => axios.get(`${SERVER_URL}/api/employee/profile`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const employeeAssignBooking = () => axios.get(`${SERVER_URL}/api/employee/getCurrentBooking`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const employeeBookings = () => axios.get(`${SERVER_URL}/api/employee/getBookings`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const employeeIdBooking = (id, data) => axios.post(`${SERVER_URL}/api/shop/assignEmployee/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const shopMobileLgn = (data) => axios.post(`${SERVER_URL}/api/mobileShopLogin`, data)

export const shopMobileLgnVerify = (data) => axios.post(`${SERVER_URL}/api/mobileShopLoginVerify`, data)

export const createShopRegistrationCloneService = (id) => axios.get(`${SERVER_URL}/api/shop/cloneService/` + id, {
  headers: {
    'auth-token': localStorage.getItem('registration-token'),
  }
})

export const createShopCloneService = (id) => axios.get(`${SERVER_URL}/api/shop/cloneService/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token'),
  }
})

export const cloneAllServiceShop = (data) => axios.post(`${SERVER_URL}/api/shop/cloneAllServiceWithUpdate`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token'),
  }
})

export const cloneAllServiceRegistration = (data) => axios.post(`${SERVER_URL}/api/shop/cloneAllService`, data, {
  headers: {
    'auth-token': localStorage.getItem('registration-token'),
  }
})

export const editShopService = (id, data) => axios.post(`${SERVER_URL}/api/shop/updateService/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('registration-token'),
    "Content-Type": 'multipart/form'
  }
})

export const deleteService = (id) => axios.get(`${SERVER_URL}/api/shop/deleteService/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})


export const employeeMobileLgn = (data) => axios.post(`${SERVER_URL}/api/mobileEmployeeLogin`, data)

export const employeeMobileLgnVerify = (data) => axios.post(`${SERVER_URL}/api/mobileEmployeeLoginVerify`, data)

export const AllServiceTag = () => axios.get(`${SERVER_URL}/api/getAllServiceTag`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllDescriptionTag = () => axios.get(`${SERVER_URL}/api/getAllDescriptionTag`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})


export const AllServiceTextQ = (data) => axios.get(`${SERVER_URL}/api/getServiceQuery/` + data,)

export const AllServiceRegexQ = (data) => axios.get(`${SERVER_URL}/api/getServiceRegexQuery/` + data,)

export const shopServiceQuery = (data) => axios.get(`${SERVER_URL}/api/shop/getServiceShopRegexQuery/` + data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

/// 
export const AllActiveService = (data, skip, limit) => axios.post(`${SERVER_URL}/api/activeService/` + skip + '/' + limit, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})


export const AllActiveServiceRegexQ = (data) => axios.get(`${SERVER_URL}/api/activeServiceRegexQuery/` + data,)

export const AllTagService = () => axios.get(`${SERVER_URL}/api/getAllTagService`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllMetaData = () => axios.get(`${SERVER_URL}/api/getAllMetadata`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllOffer = () => axios.get(`${SERVER_URL}/api/getAllOffer`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllBanner = () => axios.get(`${SERVER_URL}/api/getAllBanner`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllShopCategory = () => axios.get(`${SERVER_URL}/api/getAllShopCategory`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

/// 
export const AllGetActiveService = (data, skip, limit) => axios.post(`${SERVER_URL}/api/getactiveService/` + skip + '/' + limit, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllServiceOffer = () => axios.get(`${SERVER_URL}/api/getAllServiceOffer`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllAdminPage = () => axios.get(`${SERVER_URL}/api/getAllAdminPage`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const createContact = (data) => axios.post(`${SERVER_URL}/api/createContact`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllContactPage = () => axios.get(`${SERVER_URL}/api/getAllContactPage`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const customerMobileLgn = (data) => axios.post(`${SERVER_URL}/api/mobileCustomerLogin`, data)

export const customerMobileLgnVerify = (data) => axios.post(`${SERVER_URL}/api/mobileCustomerLoginVerify`, data)

export const employeeSignup = (data) => axios.post(`${SERVER_URL}/api/employeeSignup`, data)
export const employeeSignupVerify = (data) => axios.post(`${SERVER_URL}/api/employeeVerify`, data)

export const AllAboutPage = () => axios.get(`${SERVER_URL}/api/getAllAboutPage`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllActiveBarberService = () => axios.get(`${SERVER_URL}/api/getBarberServices`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllActiveBarberExpertise = () => axios.get(`${SERVER_URL}/api/getBarberExpertise`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllActBarberService = (skip, limit) => axios.get(`${SERVER_URL}/api/getTestService/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const employeeLogout = () => axios.get(`${SERVER_URL}/api/employee/logout`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const shopLogout = () => axios.get(`${SERVER_URL}/api/shop/logout`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllServiceName = () => axios.get(`${SERVER_URL}/api/getAllServiceName`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllExpertiseName = () => axios.get(`${SERVER_URL}/api/getAllExpertiseName`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

//// Shop Package

export const AllShopPackage = (skip, limit) => axios.get(`${SERVER_URL}/api/shop/getShopPackage/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const createShopPackage = (data) => axios.post(`${SERVER_URL}/api/shop/createShopPackage`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token'),
    "Content-Type": 'multipart/form'
  }
})

export const updateShopPackage = (id, data) => axios.post(`${SERVER_URL}/api/shop/updateShopPackage/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token'),
    "Content-Type": 'multipart/form'
  }
})

export const deletePackage = (id) => axios.get(`${SERVER_URL}/api/shop/deleteShopPackage/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const payShopPackage = (data) => axios.post(`${SERVER_URL}/api/shopPay`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllShopPayment = (skip, limit) => axios.get(`${SERVER_URL}/api/shop/getShopPayment/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

//// Shop Feature

export const AllShopFeature = (skip, limit) => axios.get(`${SERVER_URL}/api/shop/getShopFeature/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const createShopFeature = (data) => axios.post(`${SERVER_URL}/api/shop/createShopFeature`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const updateShopFeature = (id, data) => axios.post(`${SERVER_URL}/api/shop/updateShopFeature/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const deleteFeature = (id) => axios.get(`${SERVER_URL}/api/shop/deleteShopFeature/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllShopMainFeature = () => axios.get(`${SERVER_URL}/api/getAllShopFeatureList`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const SingleShopMainFeature = (id) => axios.get(`${SERVER_URL}/api/getSingleShopFeature/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

//// Inventory

export const AllShopCategorys = (skip, limit) => axios.get(`${INVENTORY_URL}/invent/shop/getShopCategory/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const createShopCategory = (data) => axios.post(`${INVENTORY_URL}/invent/shop/createShopCategory`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const updateShopCategory = (id, data) => axios.post(`${INVENTORY_URL}/invent/shop/updateShopCategory/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const deleteShopCategory = (id) => axios.get(`${INVENTORY_URL}/invent/shop/deleteShopCategory/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllShopBrand = (skip, limit) => axios.get(`${INVENTORY_URL}/invent/shop/getShopBrand/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const createShopBrand = (data) => axios.post(`${INVENTORY_URL}/invent/shop/createShopBrand`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const updateShopBrand = (id, data) => axios.post(`${INVENTORY_URL}/invent/shop/updateShopBrand/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const deleteShopBrand = (id) => axios.get(`${INVENTORY_URL}/invent/shop/deleteShopBrand/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})


export const AllShopProduct = (skip, limit) => axios.get(`${INVENTORY_URL}/invent/shop/getShopProduct/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const createShopProduct = (data) => axios.post(`${INVENTORY_URL}/invent/shop/createShopProduct`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token'),
    "Content-Type": 'multipart/form'
  }
})

export const updateShopProduct = (id, data) => axios.post(`${INVENTORY_URL}/invent/shop/updateShopProduct/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token'),
    "Content-Type": 'multipart/form'
  }
})

export const deleteShopProduct = (id) => axios.get(`${INVENTORY_URL}/invent/shop/deleteShopProduct/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const updateShopStock = (id, data) => axios.post(`${INVENTORY_URL}/invent/shop/stockUpdate/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const productQuerySearch = (data) => axios.post(`${INVENTORY_URL}/invent/shop/productSearchQuery/` + data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})


export const AllShopVendor = (skip, limit) => axios.get(`${INVENTORY_URL}/invent/shop/getShopVendor/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const createShopVendor = (data) => axios.post(`${INVENTORY_URL}/invent/shop/createShopVendor`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const updateShopVendor = (id, data) => axios.post(`${INVENTORY_URL}/invent/shop/updateShopVendor/` + id, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const deleteShopVendor = (id) => axios.get(`${INVENTORY_URL}/invent/shop/deleteShopVendor/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllShopStockhistory = (skip, limit) => axios.get(`${INVENTORY_URL}/invent/shop/getShopStock/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllShopInventTag = () => axios.get(`${INVENTORY_URL}/invent/shop/getShopTag`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllShopInventUnit = () => axios.get(`${INVENTORY_URL}/invent/shop/getShopUnit`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllShopInventVendorType = () => axios.get(`${INVENTORY_URL}/invent/shop/getShopVendorType`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const createShopVendorType = (data) => axios.post(`${INVENTORY_URL}/invent/shop/createShopVendorType`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

///customer
export const customerQuerySearch = (data) => axios.post(`${SERVER_URL}/api/customerSearchQuery/` + data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})


export const AllCustomer = (skip, limit) => axios.get(`${SERVER_URL}/api/getAllCustomer/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const getSingleCustomer = (id) => axios.get(`${SERVER_URL}/api/getSingleCustomerPackage/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllHostName = (id) => axios.post(`${SERVER_URL}/api/getHostName`, id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const shopCashPayment = (data) => axios.post(`${PAYMENT_URL}/payment/shop/payCashBooking`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const customerCashPayment = (data) => axios.post(`${PAYMENT_URL}/payment/customer/payCashBooking`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const shopUpiPayment = (data) => axios.post(`${PAYMENT_URL}/payment/shop/payUpiBooking`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const getSinglePayment = (id) => axios.get(`${PAYMENT_URL}/payment/singlePayment/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const AllCustomerPackagePayment = (skip, limit) => axios.get(`${SERVER_URL}/api/user/getCustomerPayment/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const getSingleService = (id) => axios.get(`${SERVER_URL}/api/shop/getSingleService/` + id, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})


export const shopPackageBookPayment = (data) => axios.post(`${PAYMENT_URL}/payment/user/packagePayBooking`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})


export const AllShopPackagePayment = (skip, limit) => axios.get(`${SERVER_URL}/api/shop/getPackageShop/` + skip + '/' + limit, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})

export const shopDailyCount = (data) => axios.post(`${SERVER_URL}/api/shop/getDailyCount`, data, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})


export const shopTotalCount = () => axios.get(`${SERVER_URL}/api/shop/getTotalCount`, {
  headers: {
    'auth-token': localStorage.getItem('auth-token')
  }
})