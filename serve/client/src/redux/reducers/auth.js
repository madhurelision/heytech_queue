import { removeElements, uniqueElements, uniqueServiceElements } from "config/uniqueElements";
import { FAILED, LOADING, SUCCESS, LOGIN, LOGOUT, LOADERCLOSE, SUCCESS_REDIRECT, SHOP_BOOKING_LIST, SHOP_SERVICE_LIST, SERVICE_LIST, EXPERTISE_LIST, SHOP_GALLERY_LIST, BOOKING_DONE, BOOKING_SUCCESS, CUSTOMER_BOOKING_LIST, SINGLE_BOOKING_DATA, CUSTOMER_CURRENT_BOOKING_LIST, SHOP_CURRENT_BOOKING_LIST, SHOP_QUEUE_LIST, CUSTOMER_COUNT, SHOP_COUNT, SOCKET_CUSTOMER_BOOKING_UPDATE, SOCKET_SHOP_BOOKING_UPDATE, SOCKET_SHOP_QUEUE_UPDATE, SOCKET_CUSTOMER_BOOKING_REMOVE, SOCKET_SHOP_BOOKING_REMOVE, SOCKET_SHOP_QUEUE_REMOVE, SERVICE_REQ_FAILED, EXPERTISE_REQ_FAILED, TAG_LIST, LIKE_SUCCESS, SOCKET_CUSTOMER_NOTIFICATION, CUSTOMER_NOTIFY, REVIEW_TAG_LIST, SHOP_LIST, SOCKET_SHOP_NOTIFICATION, SHOP_NOTIFY, NOTIFY_SUCCESS, SHOP_EMPLOYEE_LIST, CUSTOMER_BOOKING_LIST_UPDATE, SHOP_BOOKING_LIST_UPDATE, EMPLOYEE_CURRENT_BOOKING_LIST, EMPLOYEE_BOOKING_LIST, EMPLOYEE_COUNT, SOCKET_EMPLOYEE_BOOKING, SOCKET_EMPLOYEE_BOOKING_REMOVE, SOCKET_EMPLOYEE_BOOKING_ASSIGN_REMOVE, SOCKET_EMPLOYEE_BOOKING_ASSIGN, MOBILE_LOGIN, LOGIN_VERIFY, CLONE_SERVICE_SUCCESS, REGISTRATION, REGISTRATION_SERVICE, CLONE_ALL_SERVICE, SERVICE_TAG_LIST, DESCRIPTION_TAG_LIST, SHOP_DATE_BOOKING_LIST, CUSTOMER_DATE_BOOKING_LIST, SHOP_QUERY_SERVICE_LIST, ACTIVE_SERVICE_LIST, TAG_SERVICE_LIST, All_TAGS_META, OFFER_LIST, BANNER_LIST, SHOP_CATEGORY_LIST, NEW_ACTIVE_SERVICE_LIST, SERVICE_OFFER_LIST, ADMIN_PAGE_LIST, ACTIVE_EMPLOYEE_LIST, CONTACT_PAGE_LIST, SOCKET_CONNECT, ABOUT_PAGE_LIST, ACTIVE_BARBER_SERVICE_LIST, ACTIVE_BARBER_EXPERTISE_LIST, SERVICE_MORE, NEW_SERVICE_MORE, REMOVE_SERVICE_LIST, THEME, FORM_OPEN, REGISTRATION_SUCCESS, EMPLOYEE_LOGOUT, SHOP_LOGOUT, SERVICE_NAME_LIST, EXPERTISE_NAME_LIST, SHOP_LIST_ALL, EMPLOYEE_SIGNUP, SERVICE_LIST_ALL, SHOP_PACKAGE_LIST, REMOVE_PACKAGE_LIST, PAYMENT_DONE, STRIPE_DETAIL, SHOP_PACKAGE_PAYMENT, REMOVE_FEATURE_LIST, SHOP_FEATURE_LIST, PACKAGE_FEATURE_LIST, INVENTORY_BRAND, REMOVE_INVENTORY_BRAND, REMOVE_INVENTORY_CATEGORY, INVENTORY_CATEGORY, REMOVE_INVENTORY_PRODUCT, INVENTORY_PRODUCT, SEARCH_INVENTORY_PRODUCT, INVENTORY_VENDOR, REMOVE_INVENTORY_VENDOR, INVENTORY_STOCK_HISTORY, INVENTORY_TAG, INVENTORY_UNIT, INVENTORY_VENDOR_TYPE, CUSTOMER_SEARCH_QUERY, CUSTOMER_LIST, HOST_NAME, CUSTOMER_SINGLE, PAYMENT_SINGLE, CUSTOMER_PACKAGE_PAYMENT, SINGLE_SHOP_SERVICE, SHOP_PAYMENT_PACKAGE, SHOP_DAILY, SHOP_TOTAL } from "../action/types";
import { setItemStore } from 'config/localClient'

const initialState = {
  isAuthenticated: false,
  profile: {},
  loader: false,
  open: false,
  close: false,
  created: false,
  updated: false,
  complete: false,
  notify: false,
  socket: false,
  message: '',
  shopService: [],
  shopBooking: [],
  shopCurrentBooking: [],
  serviceList: [],
  serviceListTotal: null,
  activeServiceList: [],
  expertiseList: [],
  shopGallery: [],
  bookingData: {},
  customerBooking: [],
  customerCurrentBooking: [],
  singleBooking: {},
  shopQueueBooking: [],
  customerBookingCount: null,
  customerQueueCount: null,
  customerTotalCount: null,
  shopBookingCount: null,
  shopQueueCount: null,
  serviceRequestFailed: false,
  expertiseRequestFailed: false,
  tagList: [],
  customerNotificationCount: null,
  customerNotificationTotal: null,
  customerNotification: [],
  reviewTagList: [],
  shopList: [],
  shopNotificationCount: null,
  shopNotificationTotal: null,
  shopNotification: [],
  shopEmployee: [],
  employeeCurrentBooking: [],
  employeeBooking: [],
  employeeBookingCount: null,
  employeeQueueCount: null,
  employeeTotalCount: null,
  employeeBookingTotalCount: null,
  shopLoginData: {},
  link: '',
  serviceSelect: [],
  registrationData: {},
  clone: false,
  serviceTagList: [],
  descriptionTagList: [],
  tagServiceList: [],
  serviceCount: null,
  shopHistoryCount: null,
  customerHistoryCount: null,
  listBookcustomerCount: null,
  listABookshopCount: null,
  listQBookshopCount: null,
  offerList: [],
  serviceOfferList: [],
  bannerList: [],
  shopCategoryList: [],
  newActiveServiceList: [],
  adminPageList: {},
  activeEmployeeList: [],
  contactPageList: {},
  aboutPageList: {},
  barberServiceList: [],
  barberExpertiseList: [],
  serviceMore: false,
  shopQueueMore: false,
  shopAppointmentMore: false,
  customerMore: false,
  newServiceMore: false,
  theme: localStorage.getItem('theme') ? localStorage.getItem('theme') : 'dark',
  login: false,
  logout: false,
  serviceNameList: [],
  expertiseNameList: [],
  shop_list: [],
  empLoginData: {},
  packageList: [],
  packageTotal: null,
  paymentDone: false,
  stripe: {},
  paymentList: [],
  paymentTotal: null,
  packageFeatureList: [],
  packageFeatureTotal: null,
  featurePackageList: [],
  inventoryCategoryList: [],
  inventoryCategoryTotal: null,
  inventoryBrandList: [],
  inventoryBrandTotal: null,
  inventoryProductList: [],
  inventoryProductTotal: null,
  inventoryVendorList: [],
  inventoryVendorTotal: null,
  inventoryStockList: [],
  inventoryStockTotal: null,
  inventoryTagList: [],
  inventoryUnitList: [],
  inventoryVendorTypeList: [],
  customerList: [],
  hostName: {},
  domain: false,
  payList: {},
  customerPackageList: [],
  customerPackageTotal: null,
  shopPackageList: [],
  shopPackageTotal: null,
  shopDaily: [],
  shopTotal: [],
}

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case LOGIN:
      localStorage.setItem('auth-token', payload.token)
      localStorage.setItem('type', payload.type)
      localStorage.setItem('isAuthenticated', true)
      return {
        ...state,
        profile: payload.data,
        isAuthenticated: true,
        loader: false,
        open: false,
        close: false,
        created: false,
        updated: false,
        link: '',
        serviceSelect: [],
        registrationData: {}
      }
    case SUCCESS:
      return {
        ...state,
        loader: false,
        open: true,
        close: false,
        message: payload
      }
    case LOADING:
      return {
        ...state,
        loader: true,
        serviceSelect: [],
        shopLoginData: {},
        //open: false,
        //close: false
      }
    case FAILED:
      return {
        ...state,
        loader: false,
        open: false,
        close: true,
        updated: false,
        message: payload
      }
    case LOADERCLOSE:
      return {
        ...state,
        loader: false,
        open: false,
        close: false,
        created: false,
        complete: false,
        updated: false,
        notify: false,
        clone: false,
        logout: false,
        paymentDone: false,
        message: ''
      }
    case SUCCESS_REDIRECT:
      return {
        ...state,
        loader: false,
        open: true,
        close: false,
        created: true,
        complete: false,
        updated: false,
        message: payload,
        serviceSelect: [],
      }
    case PAYMENT_DONE:
      return {
        ...state,
        loader: false,
        open: true,
        close: false,
        paymentDone: true,
        complete: false,
        updated: false,
        message: payload,
        serviceSelect: [],
      }
    case LIKE_SUCCESS:
      return {
        ...state,
        loader: false,
        open: true,
        close: false,
        complete: false,
        updated: true,
        message: payload
      }
    case LOGOUT:
      localStorage.removeItem('auth-token')
      localStorage.removeItem('type')
      localStorage.removeItem('isAuthenticated')
      return initialState
    case SHOP_SERVICE_LIST:
      let serviceSB = (state.shopService.service == null || state.shopService.service == undefined) ? {} : uniqueServiceElements(state.shopService.service.concat(payload.data.service))
      setItemStore('SHOP_SERVICE_LIST', state.shopService.length == 0 ? payload.data : { service: serviceSB })
      return {
        ...state,
        shopService: state.shopService.length == 0 ? payload.data : { service: serviceSB },
        serviceCount: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        created: false,
      }
    case SHOP_BOOKING_LIST:
      let bookingSB = (state.shopBooking.booking == null || state.shopBooking.booking == undefined) ? {} : uniqueElements(state.shopBooking.booking.concat(payload.data.booking))
      setItemStore('SHOP_BOOKING_LIST', state.shopBooking.length == 0 ? payload.data : { booking: bookingSB })
      return {
        ...state,
        shopBooking: state.shopBooking.length == 0 ? payload.data : { booking: bookingSB },
        shopHistoryCount: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        open: false,
        close: false,
        created: false,
        updated: false,
      }
    case SHOP_BOOKING_LIST_UPDATE:
      let shopB = (state.shopBooking.booking == null || state.shopBooking.booking == undefined) ? {} : uniqueElements(state.shopBooking.booking.concat(payload.data.booking))
      setItemStore('SHOP_BOOKING_LIST', state.shopBooking.length == 0 ? payload.data : { booking: shopB })
      return {
        ...state,
        shopBooking: state.shopBooking.length == 0 ? payload.data : { booking: shopB },
        loader: false,
        open: false,
        close: false,
        created: false,
        updated: false,
      }
    case SERVICE_LIST:
      setItemStore('SERVICE_LIST', state.serviceList.length == 0 ? payload.data.service : uniqueElements(state.serviceList.concat(payload.data.service)))
      return {
        ...state,
        serviceList: state.serviceList.length == 0 ? payload.data.service : uniqueElements(state.serviceList.concat(payload.data.service)),
        serviceListTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        serviceRequestFailed: false
      }
    case SERVICE_LIST_ALL:
      setItemStore('SERVICE_LIST', state.serviceList.length == 0 ? payload.data : uniqueElements(state.serviceList.concat(payload.data)))
      return {
        ...state,
        serviceList: state.serviceList.length == 0 ? payload.data : uniqueElements(state.serviceList.concat(payload.data)),
        loader: false,
        serviceRequestFailed: false
      }
    case EXPERTISE_LIST:
      setItemStore('EXPERTISE_LIST', payload.data)
      return {
        ...state,
        expertiseList: payload.data,
        loader: false,
        open: false,
        close: false,
        created: false,
        expertiseRequestFailed: false
      }
    case SHOP_GALLERY_LIST:
      setItemStore('SHOP_GALLERY_LIST', payload.data)
      return {
        ...state,
        shopGallery: payload.data,
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case BOOKING_DONE:
      return {
        ...state,
        loader: false,
        open: true,
        created: true,
        close: false,
        message: payload.message,
        bookingData: payload.data
      }
    case BOOKING_SUCCESS:
      return {
        ...state,
        loader: false,
        open: true,
        created: false,
        close: false,
        complete: true,
        message: payload,
        bookingData: {}
      }
    case CUSTOMER_BOOKING_LIST:
      let bookingCB = (state.customerBooking.booking == null || state.customerBooking.booking == undefined) ? {} : uniqueElements(state.customerBooking.booking.concat(payload.data.booking))
      setItemStore('CUSTOMER_BOOKING_LIST', state.customerBooking.length == 0 ? payload.data : { booking: bookingCB })
      return {
        ...state,
        customerBooking: state.customerBooking.length == 0 ? payload.data : { booking: bookingCB },
        customerHistoryCount: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        open: false,
        close: false,
        created: false,
        updated: false,
      }
    case CUSTOMER_BOOKING_LIST_UPDATE:
      let cutomerB = (state.customerBooking.booking == null || state.customerBooking.booking == undefined) ? {} : uniqueElements(state.customerBooking.booking.concat(payload.data.booking))
      setItemStore('CUSTOMER_BOOKING_LIST', state.customerBooking.length == 0 ? payload.data : { booking: cutomerB })
      return {
        ...state,
        customerBooking: state.customerBooking.length == 0 ? payload.data : { booking: cutomerB },
        loader: false,
        open: false,
        close: false,
        created: false,
        updated: false,
      }
    case SINGLE_BOOKING_DATA:
      return {
        ...state,
        singleBooking: payload.data,
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case CUSTOMER_CURRENT_BOOKING_LIST:
      let customerCB = (state.customerCurrentBooking.booking == null || state.customerCurrentBooking.booking == undefined) ? {} : uniqueElements(state.customerCurrentBooking.booking.concat(payload.data.booking))
      setItemStore('CUSTOMER_CURRENT_BOOKING_LIST', state.customerCurrentBooking.length == 0 ? payload.data : { booking: customerCB })
      return {
        ...state,
        customerCurrentBooking: state.customerCurrentBooking.length == 0 ? payload.data : { booking: customerCB },
        listBookcustomerCount: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        customerMore: payload.data.booking.length > 0 ? true : false,
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SHOP_CURRENT_BOOKING_LIST:
      let bookingCBA = (state.shopCurrentBooking.booking == null || state.shopCurrentBooking.booking == undefined) ? {} : uniqueElements(state.shopCurrentBooking.booking.concat(payload.data.booking))
      setItemStore('SHOP_CURRENT_BOOKING_LIST', state.shopCurrentBooking.length == 0 ? payload.data : { booking: bookingCBA })
      return {
        ...state,
        shopCurrentBooking: state.shopCurrentBooking.length == 0 ? payload.data : { booking: bookingCBA },
        listABookshopCount: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        shopAppointmentMore: payload.data.booking.length > 0 ? true : false,
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SHOP_QUEUE_LIST:
      let shopQ = (state.shopQueueBooking.booking == null || state.shopQueueBooking.booking == undefined) ? {} : uniqueElements(state.shopQueueBooking.booking.concat(payload.data.booking))
      setItemStore('SHOP_QUEUE_LIST', state.shopQueueBooking.length == 0 ? payload.data : { booking: shopQ })
      return {
        ...state,
        shopQueueBooking: state.shopQueueBooking.length == 0 ? payload.data : { booking: shopQ },
        listQBookshopCount: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        shopQueueMore: payload.data.booking.length > 0 ? true : false,
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case CUSTOMER_COUNT:
      return {
        ...state,
        ...payload,
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SHOP_COUNT:
      return {
        ...state,
        ...payload,
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SOCKET_CUSTOMER_BOOKING_UPDATE:
      let arr2 = (state.customerCurrentBooking.booking == null || state.customerCurrentBooking.booking == undefined) ? {} : uniqueElements(state.customerCurrentBooking.booking.concat(payload))
      setItemStore('CUSTOMER_CURRENT_BOOKING_LIST', state.customerCurrentBooking.length == 0 ? { booking: payload } : { booking: arr2 })
      return {
        ...state,
        customerCurrentBooking: state.customerCurrentBooking.length == 0 ? { booking: payload } : { booking: arr2 },
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SOCKET_SHOP_BOOKING_UPDATE:
      let arr3 = (state.shopCurrentBooking.booking == null || state.shopCurrentBooking.booking == undefined) ? {} : uniqueElements(state.shopCurrentBooking.booking.concat(payload))
      setItemStore('SHOP_CURRENT_BOOKING_LIST', state.shopCurrentBooking.length == 0 ? { booking: payload } : { booking: arr3 })
      return {
        ...state,
        shopCurrentBooking: state.shopCurrentBooking.length == 0 ? { booking: payload } : { booking: arr3 },
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SOCKET_SHOP_QUEUE_UPDATE:
      let arr = (state.shopQueueBooking.booking == null || state.shopQueueBooking.booking == undefined) ? {} : uniqueElements(state.shopQueueBooking.booking.concat(payload))
      setItemStore('SHOP_QUEUE_LIST', state.shopQueueBooking.length == 0 ? { booking: payload } : { booking: arr })
      return {
        ...state,
        shopQueueBooking: state.shopQueueBooking.length == 0 ? { booking: payload } : { booking: arr },
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SOCKET_CUSTOMER_BOOKING_REMOVE:
      let arr4 = (state.customerCurrentBooking.booking == null || state.customerCurrentBooking.booking == undefined) ? {} : removeElements(state.customerCurrentBooking.booking, payload._id)
      setItemStore('CUSTOMER_CURRENT_BOOKING_LIST', state.customerCurrentBooking.length == 0 ? { booking: [] } : { booking: arr4 })
      return {
        ...state,
        customerCurrentBooking: state.customerCurrentBooking.length == 0 ? { booking: [] } : { booking: arr4 },
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SOCKET_SHOP_BOOKING_REMOVE:
      let arr5 = (state.shopCurrentBooking.booking == null || state.shopCurrentBooking.booking == undefined) ? {} : removeElements(state.shopCurrentBooking.booking, payload._id)
      setItemStore('SHOP_CURRENT_BOOKING_LIST', state.shopCurrentBooking.length == 0 ? { booking: [] } : { booking: arr5 })
      return {
        ...state,
        shopCurrentBooking: state.shopCurrentBooking.length == 0 ? { booking: [] } : { booking: arr5 },
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SOCKET_SHOP_QUEUE_REMOVE:
      let arr6 = (state.shopQueueBooking.booking == null || state.shopQueueBooking.booking == undefined) ? {} : removeElements(state.shopQueueBooking.booking, payload._id)
      setItemStore('SHOP_QUEUE_LIST', state.shopQueueBooking.length == 0 ? { booking: [] } : { booking: arr6 })
      return {
        ...state,
        shopQueueBooking: state.shopQueueBooking.length == 0 ? { booking: [] } : { booking: arr6 },
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SERVICE_REQ_FAILED:
      return {
        ...state,
        loader: false,
        open: false,
        close: true,
        message: payload,
        serviceRequestFailed: true
      }
    case EXPERTISE_REQ_FAILED:
      return {
        ...state,
        loader: false,
        open: false,
        close: true,
        message: payload,
        expertiseRequestFailed: true
      }
    case TAG_LIST:
      setItemStore('TAG_LIST', payload.data)
      return {
        ...state,
        tagList: payload.data,
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SOCKET_CUSTOMER_NOTIFICATION:
      let custeNoti = (state.customerNotification.notification == null || state.customerNotification.notification == undefined) ? {} : uniqueElements(state.customerNotification.notification.concat(payload.customerNotification))
      return {
        ...state,
        customerNotification: state.customerNotification.length == 0 ? { notification: payload.customerNotification } : { notification: custeNoti },
        customerNotificationTotal: (payload.customerNotificationTotal == null || payload.customerNotificationTotal == undefined) ? null : payload.customerNotificationTotal,
        customerNotificationCount: (payload.customerNotificationCount == null || payload.customerNotificationCount == undefined) ? null : payload.customerNotificationCount,
      }
    case CUSTOMER_NOTIFY:
      let custNoti = (state.customerNotification.notification == null || state.customerNotification.notification == undefined) ? {} : uniqueElements(state.customerNotification.notification.concat(payload.data.customerNotification))
      return {
        ...state,
        customerNotification: state.customerNotification.length == 0 ? { notification: payload.data.customerNotification } : { notification: custNoti },
        customerNotificationTotal: (payload.data.customerNotificationTotal == null || payload.data.customerNotificationTotal == undefined) ? null : payload.data.customerNotificationTotal,
        customerNotificationCount: (payload.data.customerNotificationCount == null || payload.data.customerNotificationCount == undefined) ? null : payload.data.customerNotificationCount,
        loader: false,
        open: false,
        close: false,
        notify: false,
      }
    case REVIEW_TAG_LIST:
      setItemStore('REVIEW_TAG_LIST', payload.data)
      return {
        ...state,
        reviewTagList: payload.data,
        loader: false,
      }
    case SHOP_LIST:
      setItemStore('SHOP_LIST', state.shopList.length == 0 ? payload.data : uniqueElements(state.shopList.concat(payload.data)))
      return {
        ...state,
        shopList: state.shopList.length == 0 ? payload.data : uniqueElements(state.shopList.concat(payload.data)),
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SOCKET_SHOP_NOTIFICATION:
      let shopNotis = (state.shopNotification.notification == null || state.shopNotification.notification == undefined) ? {} : uniqueElements(state.shopNotification.notification.concat(payload.shopNotification))
      return {
        ...state,
        shopNotification: state.shopNotification.length == 0 ? { notification: payload.shopNotification } : { notification: shopNotis },
        shopNotificationTotal: (payload.shopNotificationTotal == null || payload.shopNotificationTotal == undefined) ? null : payload.shopNotificationTotal,
        shopNotificationCount: (payload.shopNotificationCount == null || payload.shopNotificationCount == undefined) ? null : payload.shopNotificationCount,
      }
    case SHOP_NOTIFY:
      let shopNoti = (state.shopNotification.notification == null || state.shopNotification.notification == undefined) ? {} : uniqueElements(state.shopNotification.notification.concat(payload.data.shopNotification))
      return {
        ...state,
        shopNotification: state.shopNotification.length == 0 ? { notification: payload.data.shopNotification } : { notification: shopNoti },
        shopNotificationTotal: (payload.data.shopNotificationTotal == null || payload.data.shopNotificationTotal == undefined) ? null : payload.data.shopNotificationTotal,
        shopNotificationCount: (payload.data.shopNotificationCount == null || payload.data.shopNotificationCount == undefined) ? null : payload.data.shopNotificationCount,
        loader: false,
        open: false,
        close: false,
        notify: false,
      }
    case NOTIFY_SUCCESS:
      return {
        ...state,
        loader: false,
        open: true,
        notify: true,
        message: payload
      }
    case SHOP_EMPLOYEE_LIST:
      setItemStore('SHOP_EMPLOYEE_LIST', payload.data)
      return {
        ...state,
        shopEmployee: payload.data,
        loader: false,
        created: false,
      }
    case EMPLOYEE_CURRENT_BOOKING_LIST:
      setItemStore('EMPLOYEE_CURRENT_BOOKING_LIST', payload.data)
      return {
        ...state,
        employeeCurrentBooking: payload.data,
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case EMPLOYEE_BOOKING_LIST:
      setItemStore('EMPLOYEE_BOOKING_LIST', payload.data)
      return {
        ...state,
        employeeBooking: payload.data,
        loader: false,
        open: false,
        close: false,
        created: false,
        updated: false,
      }
    case EMPLOYEE_COUNT:
      return {
        ...state,
        ...payload,
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SOCKET_EMPLOYEE_BOOKING_REMOVE:
      let employee1 = (state.employeeBooking.booking == null || state.employeeBooking.booking == undefined) ? {} : removeElements(state.employeeBooking.booking, payload._id)
      setItemStore('EMPLOYEE_BOOKING_LIST', state.employeeBooking.length == 0 ? { booking: [] } : { booking: employee1 })
      return {
        ...state,
        employeeBooking: state.employeeBooking.length == 0 ? { booking: [] } : { booking: employee1 },
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SOCKET_EMPLOYEE_BOOKING_ASSIGN_REMOVE:
      let employee2 = (state.employeeCurrentBooking.booking == null || state.employeeCurrentBooking.booking == undefined) ? {} : removeElements(state.employeeCurrentBooking.booking, payload._id)
      setItemStore('EMPLOYEE_CURRENT_BOOKING_LIST', state.employeeCurrentBooking.length == 0 ? { booking: [] } : { booking: employee2 })
      return {
        ...state,
        employeeCurrentBooking: state.employeeCurrentBooking.length == 0 ? { booking: [] } : { booking: employee2 },
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SOCKET_EMPLOYEE_BOOKING:
      let employee3 = (state.employeeBooking.booking == null || state.employeeBooking.booking == undefined) ? {} : uniqueElements(state.employeeBooking.booking.concat(payload))
      setItemStore('EMPLOYEE_BOOKING_LIST', state.employeeBooking.length == 0 ? { booking: payload } : { booking: employee3 })
      return {
        ...state,
        employeeBooking: state.employeeBooking.length == 0 ? { booking: payload } : { booking: employee3 },
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SOCKET_EMPLOYEE_BOOKING_ASSIGN:
      let employee4 = (state.employeeCurrentBooking.booking == null || state.employeeCurrentBooking.booking == undefined) ? {} : uniqueElements(state.employeeCurrentBooking.booking.concat(payload))
      setItemStore('EMPLOYEE_CURRENT_BOOKING_LIST', state.employeeCurrentBooking.length == 0 ? { booking: payload } : { booking: employee4 })
      return {
        ...state,
        employeeCurrentBooking: state.employeeCurrentBooking.length == 0 ? { booking: payload } : { booking: employee4 },
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case MOBILE_LOGIN:
      return {
        ...state,
        shopLoginData: payload.data,
        loader: false,
        created: true,
        open: true,
        message: payload.message
      }
    case EMPLOYEE_SIGNUP:
      return {
        ...state,
        empLoginData: payload.data,
        loader: false,
        created: true,
        open: true,
        message: payload.message
      }
    case LOGIN_VERIFY:
      return {
        ...state,
        loader: false,
        open: true,
        updated: true,
        link: payload.data,
        message: payload.message,
        shopLoginData: {}
      }
    case REGISTRATION_SUCCESS:
      return {
        ...state,
        loader: false,
        open: true,
        created: true,
        link: payload.data,
        message: payload.message,
        shopLoginData: {}
      }
    case CLONE_SERVICE_SUCCESS:
      return {
        ...state,
        loader: false,
        open: true,
        complete: true,
        message: payload.message,
        serviceSelect: payload.data
      }
    case REGISTRATION:
      return {
        ...state,
        registrationData: payload
      }
    case REGISTRATION_SERVICE:
      return {
        ...state,
        serviceSelect: payload.data
      }
    case CLONE_ALL_SERVICE:
      return {
        ...state,
        loader: false,
        open: true,
        clone: true,
        message: payload.message,
        serviceSelect: payload.data
      }
    case SERVICE_TAG_LIST:
      setItemStore('SERVICE_TAG_LIST', payload.data)
      return {
        ...state,
        serviceTagList: payload.data,
        loader: false,
      }
    case DESCRIPTION_TAG_LIST:
      setItemStore('DESCRIPTION_TAG_LIST', payload.data)
      return {
        ...state,
        descriptionTagList: payload.data,
        loader: false,
      }
    case TAG_SERVICE_LIST:
      setItemStore('TAG_SERVICE_LIST', payload.data)
      return {
        ...state,
        tagServiceList: payload.data,
        loader: false,
      }
    case SHOP_DATE_BOOKING_LIST:
      setItemStore('SHOP_DATE_BOOKING_LIST', payload.data)
      return {
        ...state,
        shopBooking: payload.data,
        shopHistoryCount: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        open: false,
        close: false,
        created: false,
        updated: false,
      }
    case CUSTOMER_DATE_BOOKING_LIST:
      setItemStore('CUSTOMER_DATE_BOOKING_LIST', payload.data)
      return {
        ...state,
        customerBooking: payload.data,
        customerHistoryCount: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        open: false,
        close: false,
        created: false,
        updated: false,
      }
    case SHOP_QUERY_SERVICE_LIST:
      let serviceCB = (state.shopService.service == null || state.shopService.service == undefined) ? {} : uniqueServiceElements(state.shopService.service.concat(payload.data))
      setItemStore('SHOP_SERVICE_LIST', state.shopService.length == 0 ? { service: payload.data } : { service: serviceCB })
      return {
        ...state,
        shopService: state.shopService.length == 0 ? { service: payload.data } : { service: serviceCB },
        loader: false,
        created: false,
      }
    case ACTIVE_SERVICE_LIST:
      setItemStore('ACTIVE_SERVICE_LIST', state.activeServiceList.length == 0 ? payload.data : uniqueElements(state.activeServiceList.concat(payload.data)))
      return {
        ...state,
        activeServiceList: state.activeServiceList.length == 0 ? payload.data : uniqueElements(state.activeServiceList.concat(payload.data)),
        loader: false,
        serviceRequestFailed: false
      }
    case All_TAGS_META:
      return {
        ...state,
        ...payload.data,
        loader: false,
        serviceRequestFailed: false,
        expertiseRequestFailed: false,
      }
    case OFFER_LIST:
      setItemStore('OFFER_LIST', payload.data)
      return {
        ...state,
        offerList: payload.data,
        loader: false,
      }
    case BANNER_LIST:
      setItemStore('BANNER_LIST', payload.data)
      return {
        ...state,
        bannerList: payload.data,
        loader: false,
      }
    case SHOP_CATEGORY_LIST:
      setItemStore('SHOP_CATEGORY_LIST', payload.data)
      return {
        ...state,
        shopCategoryList: payload.data,
        loader: false,
      }
    case NEW_ACTIVE_SERVICE_LIST:
      setItemStore('NEW_ACTIVE_SERVICE_LIST', state.activeServiceList.length == 0 ? payload.data : uniqueElements(state.newActiveServiceList.concat(payload.data)))
      return {
        ...state,
        newActiveServiceList: state.activeServiceList.length == 0 ? payload.data : uniqueElements(state.newActiveServiceList.concat(payload.data)),
        newServiceMore: payload.data.length > 0 ? true : false,
        loader: false,
        serviceRequestFailed: false
      }
    case SERVICE_OFFER_LIST:
      setItemStore('SERVICE_OFFER_LIST', payload.data)
      return {
        ...state,
        serviceOfferList: payload.data,
        loader: false,
      }
    case ADMIN_PAGE_LIST:
      setItemStore('ADMIN_PAGE_LIST', payload.data)
      return {
        ...state,
        adminPageList: payload.data,
        loader: false,
      }
    case ACTIVE_EMPLOYEE_LIST:
      setItemStore('ACTIVE_EMPLOYEE_LIST', payload.data)
      return {
        ...state,
        activeEmployeeList: payload.data,
        loader: false
      }
    case CONTACT_PAGE_LIST:
      setItemStore('CONTACT_PAGE_LIST', payload.data)
      return {
        ...state,
        contactPageList: payload.data,
        loader: false,
      }
    case SOCKET_CONNECT:
      return {
        ...state,
        socket: payload
      }
    case ABOUT_PAGE_LIST:
      setItemStore('ABOUT_PAGE_LIST', payload.data)
      return {
        ...state,
        aboutPageList: payload.data,
        loader: false,
      }

    case ACTIVE_BARBER_SERVICE_LIST:
      setItemStore('ACTIVE_BARBER_SERVICE_LIST', state.barberServiceList.length == 0 ? payload.data : uniqueElements(state.barberServiceList.concat(payload.data)))
      return {
        ...state,
        barberServiceList: state.barberServiceList.length == 0 ? payload.data : uniqueElements(state.barberServiceList.concat(payload.data)),
        serviceMore: payload.data.length > 0 ? true : false,
        loader: false,
        serviceRequestFailed: false
      }
    case ACTIVE_BARBER_EXPERTISE_LIST:
      setItemStore('ACTIVE_BARBER_EXPERTISE_LIST', payload.data)
      return {
        ...state,
        barberExpertiseList: payload.data,
        loader: false,
        open: false,
        close: false,
        created: false,
        expertiseRequestFailed: false
      }
    case SERVICE_MORE:
      return {
        ...state,
        serviceMore: payload
      }
    case NEW_SERVICE_MORE:
      return {
        ...state,
        newServiceMore: payload
      }
    case REMOVE_SERVICE_LIST:
      let serviceR = (state.shopService.service == null || state.shopService.service == undefined) ? {} : removeElements(state.shopService.service, payload.data._id)
      setItemStore('SHOP_SERVICE_LIST', state.shopService.length == 0 ? { service: [] } : { service: serviceR })
      return {
        ...state,
        shopService: state.shopService.length == 0 ? { service: [] } : { service: serviceR },
        loader: false,
        open: true,
        created: true,
        message: payload.message
      }
    case THEME:
      return {
        ...state,
        theme: payload
      }
    case FORM_OPEN:
      return {
        ...state,
        login: payload
      }
    case SHOP_LOGOUT:
      return {
        ...state,
        loader: false,
        open: true,
        logout: true,
        close: false,
        message: payload
      }
    case EMPLOYEE_LOGOUT:
      return {
        ...state,
        loader: false,
        open: true,
        logout: true,
        close: false,
        message: payload
      }
    case SERVICE_NAME_LIST:
      setItemStore('SERVICE_NAME_LIST', payload.data)
      return {
        ...state,
        serviceNameList: payload.data,
        loader: false,
      }
    case EXPERTISE_NAME_LIST:
      setItemStore('EXPERTISE_NAME_LIST', payload.data)
      return {
        ...state,
        expertiseNameList: payload.data,
        loader: false,
      }
    case SHOP_LIST_ALL:
      setItemStore('SHOP_LIST_ALL', state.shop_list.length == 0 ? payload.data : uniqueElements(state.shop_list.concat(payload.data.shop)))
      return {
        ...state,
        shop_list: state.shop_list.length == 0 ? payload.data : uniqueElements(state.shop_list.concat(payload.data.shop)),
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SHOP_PACKAGE_LIST:
      let packageSB = (state.packageList.package == null || state.packageList.package == undefined) ? {} : uniqueServiceElements(state.packageList.package.concat(payload.data.package))
      setItemStore('SHOP_PACKAGE_LIST', state.packageList.length == 0 ? payload.data : { package: packageSB })
      return {
        ...state,
        packageList: state.packageList.length == 0 ? payload.data : { package: packageSB },
        packageTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        created: false,
      }
    case REMOVE_PACKAGE_LIST:
      let packageR = (state.packageList.package == null || state.packageList.package == undefined) ? {} : removeElements(state.packageList.package, payload.data._id)
      setItemStore('SHOP_PACKAGE_LIST', state.packageList.length == 0 ? { package: [] } : { package: packageR })
      return {
        ...state,
        packageList: state.packageList.length == 0 ? { package: [] } : { package: packageR },
        loader: false,
        open: true,
        created: true,
        message: payload.message
      }
    case STRIPE_DETAIL:
      return {
        ...state,
        stripe: payload.data,
        loader: false,
        open: false,
        close: false,
        created: false,
      }
    case SHOP_PACKAGE_PAYMENT:
      let paymentSB = (state.paymentList.payment == null || state.paymentList.payment == undefined) ? {} : uniqueServiceElements(state.paymentList.payment.concat(payload.data.payment))
      setItemStore('SHOP_PACKAGE_PAYMENT', state.paymentList.length == 0 ? payload.data : { payment: paymentSB })
      return {
        ...state,
        paymentList: state.paymentList.length == 0 ? payload.data : { payment: paymentSB },
        paymentTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        created: false,
      }
    case SHOP_FEATURE_LIST:
      let featureSB = (state.packageFeatureList.feature == null || state.packageFeatureList.feature == undefined) ? {} : uniqueServiceElements(state.packageFeatureList.feature.concat(payload.data.feature))
      setItemStore('SHOP_FEATURE_LIST', state.packageFeatureList.length == 0 ? payload.data : { feature: featureSB })
      return {
        ...state,
        packageFeatureList: state.packageFeatureList.length == 0 ? payload.data : { feature: featureSB },
        packageFeatureTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        created: false,
      }
    case REMOVE_FEATURE_LIST:
      let featureR = (state.packageFeatureList.feature == null || state.packageFeatureList.feature == undefined) ? {} : removeElements(state.packageFeatureList.feature, payload.data._id)
      setItemStore('SHOP_FEATURE_LIST', state.packageFeatureList.length == 0 ? { feature: [] } : { feature: featureR })
      return {
        ...state,
        packageFeatureList: state.packageFeatureList.length == 0 ? { feature: [] } : { feature: featureR },
        loader: false,
        open: true,
        created: true,
        message: payload.message
      }
    case PACKAGE_FEATURE_LIST:
      let packageFB = (state.featurePackageList == null || state.featurePackageList == undefined) ? {} : uniqueServiceElements(state.featurePackageList.concat(payload.data))
      setItemStore('PACKAGE_FEATURE_LIST', state.featurePackageList.length == 0 ? payload.data : packageFB)
      return {
        ...state,
        featurePackageList: state.featurePackageList.length == 0 ? payload.data : packageFB,
        loader: false,
        created: false,
      }
    case INVENTORY_BRAND:
      let categt = (state.inventoryBrandList.brand == null || state.inventoryBrandList.brand == undefined) ? {} : uniqueServiceElements(state.inventoryBrandList.brand.concat(payload.data.brand))
      setItemStore('INVENTORY_BRAND', state.inventoryBrandList.length == 0 ? payload.data : { brand: categt })
      return {
        ...state,
        inventoryBrandList: state.inventoryBrandList.length == 0 ? payload.data : { brand: categt },
        inventoryBrandTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        created: false,
      }
    case REMOVE_INVENTORY_BRAND:
      let cateR = (state.inventoryBrandList.brand == null || state.inventoryBrandList.brand == undefined) ? {} : removeElements(state.inventoryBrandList.brand, payload.data._id)
      setItemStore('INVENTORY_BRAND', state.inventoryBrandList.length == 0 ? { brand: [] } : { brand: cateR })
      return {
        ...state,
        inventoryBrandList: state.inventoryBrandList.length == 0 ? { brand: [] } : { brand: cateR },
        loader: false,
        open: true,
        created: true,
        message: payload.message
      }
    case INVENTORY_CATEGORY:
      let brandD = (state.inventoryCategoryList.category == null || state.inventoryCategoryList.category == undefined) ? {} : uniqueServiceElements(state.inventoryCategoryList.category.concat(payload.data.category))
      setItemStore('INVENTORY_CATEGORY', state.inventoryCategoryList.length == 0 ? payload.data : { category: brandD })
      return {
        ...state,
        inventoryCategoryList: state.inventoryCategoryList.length == 0 ? payload.data : { category: brandD },
        inventoryCategoryTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        created: false,
      }
    case REMOVE_INVENTORY_CATEGORY:
      let brandRR = (state.inventoryCategoryList.category == null || state.inventoryCategoryList.category == undefined) ? {} : removeElements(state.inventoryCategoryList.category, payload.data._id)
      setItemStore('INVENTORY_CATEGORY', state.inventoryCategoryList.length == 0 ? { category: [] } : { category: brandRR })
      return {
        ...state,
        inventoryCategoryList: state.inventoryCategoryList.length == 0 ? { category: [] } : { category: brandRR },
        loader: false,
        open: true,
        created: true,
        message: payload.message
      }
    case INVENTORY_PRODUCT:
      let ProductD = (state.inventoryProductList.product == null || state.inventoryProductList.product == undefined) ? {} : uniqueServiceElements(state.inventoryProductList.product.concat(payload.data.product))
      setItemStore('INVENTORY_PRODUCT', state.inventoryProductList.length == 0 ? payload.data : { product: ProductD })
      return {
        ...state,
        inventoryProductList: state.inventoryProductList.length == 0 ? payload.data : { product: ProductD },
        inventoryProductTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        created: false,
      }
    case REMOVE_INVENTORY_PRODUCT:
      let productRR = (state.inventoryProductList.product == null || state.inventoryProductList.product == undefined) ? {} : removeElements(state.inventoryProductList.product, payload.data._id)
      setItemStore('INVENTORY_PRODUCT', state.inventoryProductList.length == 0 ? { product: [] } : { product: productRR })
      return {
        ...state,
        inventoryProductList: state.inventoryProductList.length == 0 ? { product: [] } : { product: productRR },
        loader: false,
        open: true,
        created: true,
        message: payload.message
      }
    case SEARCH_INVENTORY_PRODUCT:
      let productWCB = (state.inventoryProductList.product == null || state.inventoryProductList.product == undefined) ? {} : uniqueServiceElements(state.inventoryProductList.product.concat(payload.data))
      setItemStore('INVENTORY_PRODUCT', state.inventoryProductList.length == 0 ? { product: payload.data } : { product: productWCB })
      return {
        ...state,
        inventoryProductList: state.inventoryProductList.length == 0 ? { product: payload.data } : { product: productWCB },
        loader: false,
        created: false,
      }
    case INVENTORY_VENDOR:
      let vendorD = (state.inventoryVendorList.vendor == null || state.inventoryVendorList.vendor == undefined) ? {} : uniqueServiceElements(state.inventoryVendorList.vendor.concat(payload.data.vendor))
      setItemStore('INVENTORY_VENDOR', state.inventoryVendorList.length == 0 ? payload.data : { vendor: vendorD })
      return {
        ...state,
        inventoryVendorList: state.inventoryVendorList.length == 0 ? payload.data : { vendor: vendorD },
        inventoryVendorTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        created: false,
      }
    case REMOVE_INVENTORY_VENDOR:
      let vendorRR = (state.inventoryVendorList.vendor == null || state.inventoryVendorList.vendor == undefined) ? {} : removeElements(state.inventoryVendorList.vendor, payload.data._id)
      setItemStore('INVENTORY_VENDOR', state.inventoryVendorList.length == 0 ? { vendor: [] } : { vendor: vendorRR })
      return {
        ...state,
        inventoryVendorList: state.inventoryVendorList.length == 0 ? { vendor: [] } : { vendor: vendorRR },
        loader: false,
        open: true,
        created: true,
        message: payload.message
      }
    case INVENTORY_STOCK_HISTORY:
      let stockD = (state.inventoryStockList.stock == null || state.inventoryStockList.stock == undefined) ? {} : uniqueServiceElements(state.inventoryStockList.stock.concat(payload.data.stock))
      setItemStore('INVENTORY_STOCK_HISTORY', state.inventoryStockList.length == 0 ? payload.data : { stock: stockD })
      return {
        ...state,
        inventoryStockList: state.inventoryStockList.length == 0 ? payload.data : { stock: stockD },
        inventoryStockTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        created: false,
      }
    case INVENTORY_TAG:
      let tagID = (state.inventoryTagList.tag == null || state.inventoryTagList.tag == undefined) ? {} : uniqueServiceElements(state.inventoryTagList.tag.concat(payload.data.tag))
      setItemStore('INVENTORY_TAG', state.inventoryTagList.length == 0 ? payload.data : { tag: tagID })
      return {
        ...state,
        inventoryTagList: state.inventoryTagList.length == 0 ? payload.data : { tag: tagID },
        loader: false,
        created: false,
      }
    case INVENTORY_UNIT:
      let unitID = (state.inventoryUnitList.unit == null || state.inventoryUnitList.unit == undefined) ? {} : uniqueServiceElements(state.inventoryUnitList.unit.concat(payload.data.unit))
      setItemStore('INVENTORY_UNIT', state.inventoryUnitList.length == 0 ? payload.data : { unit: unitID })
      return {
        ...state,
        inventoryUnitList: state.inventoryUnitList.length == 0 ? payload.data : { unit: unitID },
        loader: false,
        created: false,
      }
    case INVENTORY_VENDOR_TYPE:
      let vendorID = (state.inventoryVendorTypeList.type == null || state.inventoryVendorTypeList.type == undefined) ? {} : uniqueServiceElements(state.inventoryVendorTypeList.type.concat(payload.data.type))
      setItemStore('INVENTORY_VENDOR_TYPE', state.inventoryVendorTypeList.length == 0 ? payload.data : { type: vendorID })
      return {
        ...state,
        inventoryVendorTypeList: state.inventoryVendorTypeList.length == 0 ? payload.data : { type: vendorID },
        loader: false,
        created: false,
      }
    case CUSTOMER_LIST:
      let customD = (state.customerList.customer == null || state.customerList.customer == undefined) ? {} : uniqueServiceElements(state.customerList.customer.concat(payload.data.customer))
      setItemStore('CUSTOMER_LIST', state.customerList.length == 0 ? payload.data : { customer: customD })
      return {
        ...state,
        customerList: state.customerList.length == 0 ? payload.data : { customer: customD },
        loader: false,
        created: false,
      }
    case CUSTOMER_SEARCH_QUERY:
      let customerWCB = (state.customerList.customer == null || state.customerList.customer == undefined) ? {} : uniqueServiceElements(state.customerList.customer.concat(payload.data))
      setItemStore('CUSTOMER_LIST', state.customerList.length == 0 ? { customer: payload.data } : { customer: customerWCB })
      return {
        ...state,
        customerList: state.customerList.length == 0 ? { customer: payload.data } : { customer: customerWCB },
        loader: false,
        created: false,
      }
    case CUSTOMER_SINGLE:
      let customerMX = (state.customerList.customer == null || state.customerList.customer == undefined) ? {} : uniqueServiceElements(state.customerList.customer.concat(payload.data))
      setItemStore('CUSTOMER_LIST', state.customerList.length == 0 ? { customer: payload.data } : { customer: customerMX })
      return {
        ...state,
        customerList: state.customerList.length == 0 ? { customer: payload.data } : { customer: customerMX },
        loader: false,
        created: false,
      }
    case HOST_NAME:
      return {
        ...state,
        hostName: Object.keys(payload.data).length == 0 ? { name: '' } : payload.data,
        domain: Object.keys(payload.data).length == 0 ? true : false,
        loader: false,
        created: false,
      }
    case PAYMENT_SINGLE:
      return {
        ...state,
        payList: payload.data,
        loader: false,
        created: false,
      }
    case CUSTOMER_PACKAGE_PAYMENT:
      let customPayD = (state.customerPackageList.payment == null || state.customerPackageList.payment == undefined) ? {} : uniqueServiceElements(state.customerPackageList.payment.concat(payload.data.payment))
      setItemStore('CUSTOMER_PACKAGE_PAYMENT', state.customerPackageList.length == 0 ? payload.data : { payment: customPayD })
      return {
        ...state,
        customerPackageList: state.customerPackageList.length == 0 ? payload.data : { payment: customPayD },
        customerPackageTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        created: false,
      }
    case SINGLE_SHOP_SERVICE:
      let serviceSBS = (state.shopService.service == null || state.shopService.service == undefined) ? {} : uniqueServiceElements(state.shopService.service.concat(payload.data))
      setItemStore('SINGLE_SHOP_SERVICE', state.shopService.length == 0 ? { service: payload.data } : { service: serviceSBS })
      return {
        ...state,
        shopService: state.shopService.length == 0 ? { service: payload.data } : { service: serviceSBS },
        loader: false,
        created: false,
      }
    case SHOP_PAYMENT_PACKAGE:
      let shopPayD = (state.shopPackageList.payment == null || state.shopPackageList.payment == undefined) ? {} : uniqueServiceElements(state.shopPackageList.payment.concat(payload.data.payment))
      setItemStore('SHOP_PAYMENT_PACKAGE', state.shopPackageList.length == 0 ? payload.data : { payment: shopPayD })
      return {
        ...state,
        shopPackageList: state.shopPackageList.length == 0 ? payload.data : { payment: shopPayD },
        shopPackageTotal: (payload.data.total == null || payload.data.total == undefined) ? null : payload.data.total,
        loader: false,
        created: false,
      }
      case SHOP_DAILY:
        return {
          ...state,
          shopDaily:  payload.data ,
          loader: false,
          created: false,
        }
        case SHOP_TOTAL:
          return {
            ...state,
            shopTotal:  payload.data ,
            loader: false,
            created: false,
          }
    default:
      return state;
  }
}