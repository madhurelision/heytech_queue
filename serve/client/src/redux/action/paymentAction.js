import { customerCashPayment, shopCashPayment, shopPackageBookPayment, shopUpiPayment } from "redux/api/api"
import { FAILED, LOADING, SUCCESS_REDIRECT } from "./types"

export const shopMakeCashPay = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await shopCashPayment(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const customerMakeCashPay = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await customerCashPayment(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const shopMakeUpiPay = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await shopUpiPayment(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const customerMakePackagePay = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await shopPackageBookPayment(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

