import { CustomerProfile, ShopProfile, AllService, AllShopService, createShopService, updateShopProfile, AllExpertise, updateShopService, AllShopGallery, createShopBooking, verifyShopBooking, AllShopBooking, AllCustomerBooking, waitingShopBooking, singleBookingDetail, AllCustomerCurrentBooking, AllShopCurrentBooking, createShopQueueBooking, AllShopQueue, AllCustomerBookingCount, ShopLike, ShopUnlike, ShopReview, CustomerReview, createShopGallery, updateShopGallery, cancelShopBooking, cancelCustomerBooking, verifyCustomerCancelBooking, shopQueryBooking, customerQueryBooking, createShopCode, QueueWaitingShopBooking, updateShopServiceStatus, updateShopQueueStatus, AllTag, deleteShopGallery, AllCustomerNotification, AllCustomerViewNotification, updateShopAppointmentStatus, AllReviewTag, createShopMobile, verifyShopMobile, AllShopTextQ, AllShopRegexQ, AllShopViewNotification, AllShopNotification, createBookingOtpMobile, updateShopProfileService, createShopRegistrationService, updateShopEmployeeStatus, createShopEmployee, updateShopEmployee, AllShopEmployee, customerHistoryQuery, shopHistoryQuery, employeeLoginWithId, EmployeeProfile, employeeAssignBooking, employeeBookings, deleteShopEmployee, employeeIdBooking, shopMobileLgn, shopMobileLgnVerify, createShopRegistrationCloneService, createShopCloneService, cloneAllServiceRegistration, editShopService, deleteService, employeeMobileLgn, employeeMobileLgnVerify, AllServiceTag, AllDescriptionTag, cloneAllServiceShop, AllServiceRegexQ, AllServiceTextQ, shopServiceQuery, AllActiveService, AllActiveServiceRegexQ, AllTagService, AllMetaData, AllOffer, AllBanner, AllShopCategory, AllGetActiveService, AllServiceOffer, AllAdminPage, createContact, AllContactPage, customerMobileLgn, customerMobileLgnVerify, AllAboutPage, AllActiveBarberService, AllActiveBarberExpertise, AllActBarberService, remarkShopBooking, seatShopBooking, updateShopOpenStatus, updateShopProfileTimeslot, updateCustomerProfile, createCustomerMobile, verifyCustomerMobile, ShopCompleteBook, shopLogout, employeeLogout, updateShopKyc, updateShopProfileKyc, AllServiceName, AllExpertiseName, AllShopListView, employeeSignup, employeeSignupVerify, deletePackage, updateShopPackage, createShopPackage, AllShopPackage, payShopPackage, AllShopPayment, deleteFeature, updateShopFeature, createShopFeature, AllShopFeature, AllShopMainFeature, SingleShopMainFeature, customerQuerySearch, AllCustomer, AllHostName, getSingleCustomer, getSinglePayment, AllCustomerPackagePayment, getSingleService, AllShopPackagePayment, shopDailyCount, shopTotalCount } from 'redux/api/api'
import { LOGIN, SUCCESS, LOADING, SUCCESS_REDIRECT, FAILED, LOGOUT, SHOP_SERVICE_LIST, SERVICE_LIST, EXPERTISE_LIST, SHOP_GALLERY_LIST, BOOKING_DONE, BOOKING_SUCCESS, SHOP_BOOKING_LIST, CUSTOMER_BOOKING_LIST, SINGLE_BOOKING_DATA, CUSTOMER_CURRENT_BOOKING_LIST, SHOP_CURRENT_BOOKING_LIST, SHOP_QUEUE_LIST, CUSTOMER_COUNT, SERVICE_REQ_FAILED, EXPERTISE_REQ_FAILED, TAG_LIST, LIKE_SUCCESS, CUSTOMER_NOTIFY, REVIEW_TAG_LIST, SHOP_LIST, SHOP_NOTIFY, NOTIFY_SUCCESS, SHOP_EMPLOYEE_LIST, SHOP_BOOKING_LIST_UPDATE, CUSTOMER_BOOKING_LIST_UPDATE, EMPLOYEE_CURRENT_BOOKING_LIST, EMPLOYEE_BOOKING_LIST, MOBILE_LOGIN, LOGIN_VERIFY, CLONE_SERVICE_SUCCESS, REGISTRATION_SERVICE, CLONE_ALL_SERVICE, SERVICE_TAG_LIST, DESCRIPTION_TAG_LIST, SHOP_QUERY_SERVICE_LIST, ACTIVE_SERVICE_LIST, TAG_SERVICE_LIST, All_TAGS_META, OFFER_LIST, BANNER_LIST, SHOP_CATEGORY_LIST, NEW_ACTIVE_SERVICE_LIST, SERVICE_OFFER_LIST, ADMIN_PAGE_LIST, CONTACT_PAGE_LIST, ABOUT_PAGE_LIST, ACTIVE_BARBER_SERVICE_LIST, ACTIVE_BARBER_EXPERTISE_LIST, REMOVE_SERVICE_LIST, REGISTRATION_SUCCESS, SHOP_LOGOUT, EMPLOYEE_LOGOUT, SERVICE_NAME_LIST, EXPERTISE_NAME_LIST, SHOP_LIST_ALL, EMPLOYEE_SIGNUP, SERVICE_LIST_ALL, REMOVE_PACKAGE_LIST, SHOP_PACKAGE_LIST, PAYMENT_DONE, SHOP_PACKAGE_PAYMENT, REMOVE_FEATURE_LIST, SHOP_FEATURE_LIST, PACKAGE_FEATURE_LIST, CUSTOMER_LIST, CUSTOMER_SEARCH_QUERY, HOST_NAME, CUSTOMER_SINGLE, PAYMENT_SINGLE, CUSTOMER_PACKAGE_PAYMENT, SINGLE_SHOP_SERVICE, SHOP_PAYMENT_PACKAGE, SHOP_DAILY, SHOP_TOTAL } from './types'

//// shop 
export const shopProfileData = (val) => async dispatch => {
  dispatch({ type: LOADING })
  try {
    const { data } = await ShopProfile(val)
    dispatch({ type: LOGIN, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    // if (!err.response || err.response === null || err.response === undefined) {
    //   dispatch({ type: FAILED, payload: err.message })
    // } else {
    //   dispatch({ type: FAILED, payload: err.response.data.message })
    // }
    dispatch({ type: LOGOUT })
  }
}

//// customer 
export const customerProfileData = (val) => async dispatch => {
  dispatch({ type: LOADING })
  try {
    const { data } = await CustomerProfile(val)
    dispatch({ type: LOGIN, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    dispatch({ type: LOGOUT })
  }
}

export const customerProfileUpdate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateCustomerProfile(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const customerMobileCreateOtp = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createCustomerMobile(id, val)
    dispatch({ type: LIKE_SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const customerMobileVerifyOtp = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await verifyCustomerMobile(id, val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getShopService = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopService(skip, limit)
    dispatch({ type: SHOP_SERVICE_LIST, payload: data })
    dispatch({ type: SERVICE_LIST_ALL, payload: { data: data.data.service } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllService = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllService(skip, limit)
    dispatch({ type: SERVICE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: SERVICE_REQ_FAILED, payload: err.message })
    } else {
      dispatch({ type: SERVICE_REQ_FAILED, payload: err.response.data.message })
    }
  }

}

export const serviceCreate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopService(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const shopProfileUpdate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopProfile(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllExpertise = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllExpertise()
    dispatch({ type: EXPERTISE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: EXPERTISE_REQ_FAILED, payload: err.message })
    } else {
      dispatch({ type: EXPERTISE_REQ_FAILED, payload: err.response.data.message })
    }
  }

}

export const serviceupdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopService(id, val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getShopGallery = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopGallery()
    dispatch({ type: SHOP_GALLERY_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const bookingCreate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopBooking(val)
    dispatch({ type: BOOKING_DONE, payload: { message: data.message, data: data.data } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const bookingVerify = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await verifyShopBooking(id, val)
    dispatch({ type: BOOKING_SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getShopBooking = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopBooking()
    dispatch({ type: SHOP_BOOKING_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getCustomerBooking = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllCustomerBooking()
    dispatch({ type: CUSTOMER_BOOKING_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const bookingTimeUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await waitingShopBooking(id, val)
    dispatch({ type: SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const bookingRemarkUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await remarkShopBooking(id, val)
    dispatch({ type: SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const bookingSeatChangeUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await seatShopBooking(id, val)
    dispatch({ type: SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const QueuebookingTimeUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await QueueWaitingShopBooking(id, val)
    dispatch({ type: SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getSingleBooking = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await singleBookingDetail(id)
    dispatch({ type: SINGLE_BOOKING_DATA, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getShopCurrentBooking = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopCurrentBooking(skip, limit)
    dispatch({ type: SHOP_CURRENT_BOOKING_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getCustomerCurrentBooking = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllCustomerCurrentBooking(skip, limit)
    dispatch({ type: CUSTOMER_CURRENT_BOOKING_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const bookingQueueCreate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopQueueBooking(val)
    dispatch({ type: BOOKING_DONE, payload: { message: data.message, data: data.data } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getShopQueue = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopQueue(skip, limit)
    dispatch({ type: SHOP_QUEUE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getCustomerCount = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllCustomerBookingCount()
    dispatch({ type: CUSTOMER_COUNT, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const shopUserLike = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await ShopLike(id, val)
    dispatch({ type: LIKE_SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const shopUserDislike = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await ShopUnlike(id, val)
    dispatch({ type: LIKE_SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const shopUserReview = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await ShopReview(val)
    dispatch({ type: LIKE_SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const customerUserReview = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await CustomerReview(val)
    dispatch({ type: LIKE_SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const galleryCreate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopGallery(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const galleryUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopGallery(id, val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const shopCancelBooking = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await cancelShopBooking(id)
    dispatch({ type: SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const shopCompleteBooking = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await ShopCompleteBook(val)
    dispatch({ type: SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const customerCancelBooking = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await cancelCustomerBooking(id)
    dispatch({ type: SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const customerVerifyCancelBooking = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await verifyCustomerCancelBooking(id, val)
    dispatch({ type: SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getShopQueryBooking = (val, skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await shopQueryBooking(val, skip, limit)
    dispatch({ type: SHOP_BOOKING_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getCustomerQuertBooking = (val, skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await customerQueryBooking(val, skip, limit)
    dispatch({ type: CUSTOMER_BOOKING_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const shopQrcodeGenerate = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopCode()
    dispatch({ type: SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const serviceStatusUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopServiceStatus(id, val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const queueStatusUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopQueueStatus(id, val)
    dispatch({ type: LIKE_SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const openStatusUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopOpenStatus(id, val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const appointmentStatusUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopAppointmentStatus(id, val)
    dispatch({ type: LIKE_SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllTag = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllTag()
    dispatch({ type: TAG_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const galleryDelete = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await deleteShopGallery(id)
    dispatch({ type: SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllCustomerNotification = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllCustomerNotification(skip, limit)
    dispatch({ type: CUSTOMER_NOTIFY, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllViewNotification = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllCustomerViewNotification()
    dispatch({ type: NOTIFY_SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllReviewTag = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllReviewTag()
    dispatch({ type: REVIEW_TAG_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const shopMobileCreateOtp = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopMobile(id, val)
    dispatch({ type: LIKE_SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const shopMobileVerifyOtp = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await verifyShopMobile(id, val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllShopTextMatch = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopTextQ(val)
    dispatch({ type: SHOP_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllShopRegexMatch = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopRegexQ(val)
    dispatch({ type: SHOP_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllShopListMatch = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopListView()
    dispatch({ type: SHOP_LIST_ALL, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllShopNotification = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopNotification(skip, limit)
    dispatch({ type: SHOP_NOTIFY, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllShopViewNotification = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopViewNotification()
    dispatch({ type: NOTIFY_SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const bookingOtpGenerate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createBookingOtpMobile(val)
    dispatch({ type: BOOKING_DONE, payload: { message: data.message, data: data.data } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const shopProfileService = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopProfileService(id, val)
    dispatch({ type: LIKE_SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const shopProfileTimeSlot = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopProfileTimeslot(id, val)
    dispatch({ type: REGISTRATION_SUCCESS, payload: { data: data.data, message: data.message } })

    localStorage.removeItem('registration-token')

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const shopProfileKYC = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopKyc(id, val)
    dispatch({ type: LOGIN_VERIFY, payload: { data: data.data, message: data.message } })

    localStorage.removeItem('registration-token')

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const shopProfileUpdateKYC = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopProfileKyc(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const shopProfileUpdateService = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopProfileService(id, val)
    dispatch({ type: LIKE_SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const serviceRegistrationCreate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopRegistrationService(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })
    dispatch({ type: SERVICE_LIST_ALL, payload: { data: [data.data] } })
    dispatch({ type: REGISTRATION_SERVICE, payload: { data: [data.data] } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getShopEmployee = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopEmployee()
    dispatch({ type: SHOP_EMPLOYEE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const employeeShopCreate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopEmployee(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const employeeShopUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopEmployee(id, val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const employeeStatusUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopEmployeeStatus(id, val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const employeeDelete = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await deleteShopEmployee(id)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getShopQueryBookingUpdate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await shopHistoryQuery(val)
    dispatch({ type: SHOP_BOOKING_LIST_UPDATE, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getCustomerQuertBookingUpdate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await customerHistoryQuery(val)
    dispatch({ type: CUSTOMER_BOOKING_LIST_UPDATE, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const loginEmployeeId = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await employeeLoginWithId(val)
    dispatch({ type: LOGIN, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }
}

export const employeeProfileData = (val) => async dispatch => {
  dispatch({ type: LOADING })
  try {
    const { data } = await EmployeeProfile(val)
    dispatch({ type: LOGIN, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    dispatch({ type: LOGOUT })
  }
}

export const getEmployeeAssignBooking = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await employeeAssignBooking()
    dispatch({ type: EMPLOYEE_CURRENT_BOOKING_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getEmployeeBookingList = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await employeeBookings()
    dispatch({ type: EMPLOYEE_BOOKING_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const bookingEmployeeAdd = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await employeeIdBooking(id, val)
    dispatch({ type: SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const shopNumberLogin = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await shopMobileLgn(val)
    dispatch({ type: MOBILE_LOGIN, payload: { data: data.data, message: data.message } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const shopNumberLoginVerify = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await shopMobileLgnVerify(val)
    dispatch({ type: LOGIN_VERIFY, payload: { data: data.data, message: data.message } })
  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const serviceRegistrationClone = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopRegistrationCloneService(val)
    dispatch({ type: CLONE_SERVICE_SUCCESS, payload: { data: data.data, message: data.message } })
    dispatch({ type: SERVICE_LIST_ALL, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const serviceClone = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopCloneService(val)
    dispatch({ type: CLONE_SERVICE_SUCCESS, payload: { data: data.data, message: data.message } })
    dispatch({ type: SERVICE_LIST_ALL, payload: data })
    // dispatch({ type: SHOP_SERVICE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const serviceAllClone = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await cloneAllServiceRegistration(val)
    dispatch({ type: CLONE_ALL_SERVICE, payload: { data: data.data, message: data.message } })
    dispatch({ type: SERVICE_LIST_ALL, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const serviceAllShopClone = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await cloneAllServiceShop(val)
    // dispatch({ type: CLONE_ALL_SERVICE, payload: { data: data.data, message: data.message } })    
    dispatch({ type: LIKE_SUCCESS, payload: data.message })
    dispatch({ type: SERVICE_LIST_ALL, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const serviceRegistrationUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await editShopService(id, val)
    dispatch({ type: CLONE_SERVICE_SUCCESS, payload: { data: [data.data], message: data.message } })
    dispatch({ type: SERVICE_LIST_ALL, payload: { data: [data.data] } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const serviceDelete = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await deleteService(id)
    dispatch({ type: REMOVE_SERVICE_LIST, payload: { data: data.data, message: data.message } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const employeeNumberLogin = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await employeeMobileLgn(val)
    dispatch({ type: MOBILE_LOGIN, payload: { data: data.data, message: data.message } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const employeeNewSignup = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await employeeSignup(val)
    dispatch({ type: EMPLOYEE_SIGNUP, payload: { data: data.data, message: data.message } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const employeeNumberLoginVerify = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await employeeMobileLgnVerify(val)
    dispatch({ type: LOGIN_VERIFY, payload: { data: data.data, message: data.message } })
  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const employeeSignupVerifys = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await employeeSignupVerify(val)
    dispatch({ type: LOGIN_VERIFY, payload: { data: data.data, message: data.message } })
  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllServiceTag = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllServiceTag()
    dispatch({ type: SERVICE_TAG_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllDescriptionTag = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllDescriptionTag()
    dispatch({ type: DESCRIPTION_TAG_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getAllServiceTextMatch = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllServiceTextQ(val)
    dispatch({ type: SERVICE_LIST_ALL, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllServiceRegexMatch = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllServiceRegexQ(val)
    dispatch({ type: SERVICE_LIST_ALL, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllServiceShopRegexMatch = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await shopServiceQuery(val)
    dispatch({ type: SHOP_QUERY_SERVICE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllActiveService = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllActiveService()
    dispatch({ type: ACTIVE_SERVICE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: SERVICE_REQ_FAILED, payload: err.message })
    } else {
      dispatch({ type: SERVICE_REQ_FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllActiveServiceRegexMatch = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllActiveServiceRegexQ(val)
    dispatch({ type: ACTIVE_SERVICE_LIST, payload: data })
    dispatch({ type: NEW_ACTIVE_SERVICE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllTagService = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllTagService()
    dispatch({ type: TAG_SERVICE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllTagMetaData = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllMetaData()
    dispatch({ type: All_TAGS_META, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: SERVICE_REQ_FAILED, payload: err.message })
    } else {
      dispatch({ type: SERVICE_REQ_FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllOffers = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllOffer()
    dispatch({ type: OFFER_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllBanner = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllBanner()
    dispatch({ type: BANNER_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getAllShopCategory = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopCategory()
    dispatch({ type: SHOP_CATEGORY_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllTypeActiveService = (val, skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllGetActiveService(val, skip, limit)
    dispatch({ type: NEW_ACTIVE_SERVICE_LIST, payload: data })
    dispatch({ type: ACTIVE_SERVICE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: SERVICE_REQ_FAILED, payload: err.message })
    } else {
      dispatch({ type: SERVICE_REQ_FAILED, payload: err.response.data.message })
    }
  }

}

export const getAlServiceOffers = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllServiceOffer()
    dispatch({ type: SERVICE_OFFER_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getAllAdminPage = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllAdminPage()
    dispatch({ type: ADMIN_PAGE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const contactCreate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createContact(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllContactPage = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllContactPage()
    dispatch({ type: CONTACT_PAGE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const customerNumberLogin = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await customerMobileLgn(val)
    dispatch({ type: MOBILE_LOGIN, payload: { data: data.data, message: data.message } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const customerNumberLoginVerify = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await customerMobileLgnVerify(val)
    dispatch({ type: LOGIN_VERIFY, payload: { data: data.data, message: data.message } })
  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllAboutPage = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllAboutPage()
    dispatch({ type: ABOUT_PAGE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllActiveBarberService = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllActBarberService(skip, limit)
    dispatch({ type: ACTIVE_BARBER_SERVICE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: SERVICE_REQ_FAILED, payload: err.message })
    } else {
      dispatch({ type: SERVICE_REQ_FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllActiveBarberExpertise = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllActiveBarberExpertise()
    dispatch({ type: ACTIVE_BARBER_EXPERTISE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: SERVICE_REQ_FAILED, payload: err.message })
    } else {
      dispatch({ type: SERVICE_REQ_FAILED, payload: err.response.data.message })
    }
  }

}

export const shopLogoutBtn = () => async dispatch => {
  dispatch({ type: LOADING })
  try {
    const { data } = await shopLogout()
    dispatch({ type: SHOP_LOGOUT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    dispatch({ type: LOGOUT })
  }
}

export const employeeLogoutBtn = () => async dispatch => {
  dispatch({ type: LOADING })
  try {
    const { data } = await employeeLogout()
    dispatch({ type: EMPLOYEE_LOGOUT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    dispatch({ type: LOGOUT })
  }
}


export const getAllServiceName = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllServiceName()
    dispatch({ type: SERVICE_NAME_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}



export const getAllExpertiseName = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllExpertiseName()
    dispatch({ type: EXPERTISE_NAME_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getShopPackage = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopPackage(skip, limit)
    dispatch({ type: SHOP_PACKAGE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const packageCreate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopPackage(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const packageupdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopPackage(id, val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}
export const packageDelete = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await deletePackage(id)
    dispatch({ type: REMOVE_PACKAGE_LIST, payload: { data: data.data, message: data.message } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const shopPackagePayment = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await payShopPackage(val)
    dispatch({ type: PAYMENT_DONE, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getShopPaymentList = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopPayment(skip, limit)
    dispatch({ type: SHOP_PACKAGE_PAYMENT, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getShopFeature = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopFeature(skip, limit)
    dispatch({ type: SHOP_FEATURE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const featureCreate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopFeature(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const featureUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopFeature(id, val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}
export const featureDelete = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await deleteFeature(id)
    dispatch({ type: REMOVE_FEATURE_LIST, payload: { data: data.data, message: data.message } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getShopPackageFeatureL = () => async dispatch => {
  try {
    const { data } = await AllShopMainFeature()
    dispatch({ type: PACKAGE_FEATURE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getSingleShopPackage = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await SingleShopMainFeature(id)
    dispatch({ type: PACKAGE_FEATURE_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }
}


export const getAllCustomerRegexMatch = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await customerQuerySearch(val)
    dispatch({ type: CUSTOMER_SEARCH_QUERY, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getAllCustomer = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllCustomer(skip, limit)
    dispatch({ type: CUSTOMER_LIST, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getIpHost = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllHostName(id)
    dispatch({ type: HOST_NAME, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllSingleCustomer = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await getSingleCustomer(id)
    dispatch({ type: CUSTOMER_SINGLE, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getAllSinglePayment = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await getSinglePayment(id)
    dispatch({ type: PAYMENT_SINGLE, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getCustomerPackagePaymentList = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllCustomerPackagePayment(skip, limit)
    dispatch({ type: CUSTOMER_PACKAGE_PAYMENT, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getAllSingleService = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await getSingleService(id)
    dispatch({ type: SINGLE_SHOP_SERVICE, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getShopPackagePaymentList = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopPackagePayment(skip, limit)
    dispatch({ type: SHOP_PAYMENT_PACKAGE, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getShopDaily = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await shopDailyCount(val)
    dispatch({ type: SHOP_DAILY, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getShopTotal = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await shopTotalCount()
    dispatch({ type: SHOP_TOTAL, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}