import { AllShopBrand, AllShopCategorys, AllShopInventTag, AllShopInventUnit, AllShopProduct, AllShopStockhistory, AllShopVendor, createShopBrand, createShopCategory, createShopProduct, createShopVendor, deleteShopBrand, deleteShopCategory, deleteShopProduct, deleteShopVendor, productQuerySearch, updateShopBrand, updateShopCategory, updateShopProduct, updateShopStock, updateShopVendor, AllShopInventVendorType, createShopVendorType } from 'redux/api/api'
import { LOADING, SUCCESS_REDIRECT, FAILED, INVENTORY_BRAND, INVENTORY_CATEGORY, INVENTORY_PRODUCT, REMOVE_INVENTORY_BRAND, REMOVE_INVENTORY_CATEGORY, REMOVE_INVENTORY_PRODUCT, SEARCH_INVENTORY_PRODUCT, INVENTORY_VENDOR, REMOVE_INVENTORY_VENDOR, INVENTORY_STOCK_HISTORY, INVENTORY_TAG, INVENTORY_UNIT, INVENTORY_VENDOR_TYPE, LIKE_SUCCESS } from './types'

export const getShopCategory = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopCategorys(skip, limit)
    dispatch({ type: INVENTORY_CATEGORY, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const categoryShopCreate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopCategory(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const categoryShopUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopCategory(id, val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}
export const categoryShopDelete = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await deleteShopCategory(id)
    dispatch({ type: REMOVE_INVENTORY_CATEGORY, payload: { data: data.data, message: data.message } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getShopBrand = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopBrand(skip, limit)
    dispatch({ type: INVENTORY_BRAND, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const brandShopCreate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopBrand(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const brandShopUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopBrand(id, val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}
export const brandShopDelete = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await deleteShopBrand(id)
    dispatch({ type: REMOVE_INVENTORY_BRAND, payload: { data: data.data, message: data.message } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getShopProduct = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopProduct(skip, limit)
    dispatch({ type: INVENTORY_PRODUCT, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const productShopCreate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopProduct(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const productShopUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopProduct(id, val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}
export const productShopDelete = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await deleteShopProduct(id)
    dispatch({ type: REMOVE_INVENTORY_PRODUCT, payload: { data: data.data, message: data.message } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const productShopStock = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopStock(id, val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getAllProductRegexMatch = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await productQuerySearch(val)
    dispatch({ type: SEARCH_INVENTORY_PRODUCT, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getShopVendor = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopVendor(skip, limit)
    dispatch({ type: INVENTORY_VENDOR, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const vendorShopCreate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopVendor(val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const vendorShopUpdate = (id, val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await updateShopVendor(id, val)
    dispatch({ type: SUCCESS_REDIRECT, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}
export const vendorShopDelete = (id) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await deleteShopVendor(id)
    dispatch({ type: REMOVE_INVENTORY_VENDOR, payload: { data: data.data, message: data.message } })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getShopStock = (skip, limit) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopStockhistory(skip, limit)
    dispatch({ type: INVENTORY_STOCK_HISTORY, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const getShopTag = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopInventTag()
    dispatch({ type: INVENTORY_TAG, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getShopUnit = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopInventUnit()
    dispatch({ type: INVENTORY_UNIT, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}

export const getShopVendorType = () => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await AllShopInventVendorType()
    dispatch({ type: INVENTORY_VENDOR_TYPE, payload: data })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}


export const vendorTypeShopCreate = (val) => async dispatch => {
  dispatch({ type: LOADING })

  try {
    const { data } = await createShopVendorType(val)
    dispatch({ type: LIKE_SUCCESS, payload: data.message })

  } catch (err) {
    console.log('Api Err', err)
    if (!err.response || err.response === null || err.response === undefined) {
      dispatch({ type: FAILED, payload: err.message })
    } else {
      dispatch({ type: FAILED, payload: err.response.data.message })
    }
  }

}
