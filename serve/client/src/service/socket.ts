import io from 'socket.io-client';

import { SERVER_URL, SOCKET_URL } from 'config.keys';


export const socket = io(SOCKET_URL);
