const { createLogger, transports, format } = require('winston');
const path = require('path');
const DailyRotateFile = require('winston-daily-rotate-file');
const axios = require('axios')
const config = require('config');
const { LOG_SERVICE } = require('./key');

const transport = new DailyRotateFile({
  filename: path.join(__dirname, '../log/application-%DATE%.log'),
  datePattern: 'YYYY-MM-DD',
  //zippedArchive: true,
  maxSize: '20m'
});

transport.on('rotate', function (oldFilename, newFilename) {
  // do something fun
});

const wLog = createLogger({
  transports: [
    transport
  ]
});

const logger = createLogger({
  transports: [
    new transports.File({
      filename: path.join(__dirname, '../log/info.log'),
      level: 'info',
      format: format.combine(format.timestamp(), format.json())
    }),
    new transports.File({
      filename: path.join(__dirname, '../log/error.log'),
      level: 'error',
      format: format.combine(format.timestamp(), format.json())
    })
  ]
})

class LoggerStream {
  write(message) {
    console.log('check', message,)
    logger.info(message.substring(0, message.lastIndexOf('\n')));
  }
}

class MainLogFn {
  constructor() {
  }

  static async init() {
    if (logger == null || logger == undefined) {
      return false
    } else {
      return true
    }
  }

  static async write(req, res, next) {
    var db = req.app.get('client').db("heytechQueue");

    console.log('check', {
      method: req.method,
      url: req.originalUrl || req.url,
    })

    axios.post(config.has('LOG_SERVICE') ? config.get('LOG_SERVICE') + 'hey/shortLog' : LOG_SERVICE + 'hey/shortLog', {
      method: req.method,
      content: req.headers['content-length'],
      type: req.headers['content-type'],
      url: req.originalUrl || req.url,
      agent: req.headers['user-agent'],
      ip: req.ip,
      http: req.httpVersionMajor + '.' + req.httpVersionMinor,
      referer: req.headers.referer || req.headers.referrer,
      header: req.headers,
      params: req.params,
      body: req.body
    }).then((cc) => {
      next()

    }).catch((err) => {
      next()

    })

    // const check = await MainLogFn.init();
    // const ccD = await db.collection('shortlogs').insertOne({
    //   method: req.method,
    //   content: req.headers['content-length'],
    //   type: req.headers['content-type'],
    //   url: req.originalUrl || req.url,
    //   agent: req.headers['user-agent'],
    //   ip: req.ip,
    //   http: req.httpVersionMajor + '.' + req.httpVersionMinor,
    //   referer: req.headers.referer || req.headers.referrer,
    //   header: req.headers,
    //   params: req.params,
    //   body: req.body,
    //   deleted: false,
    //   status: false,
    //   createdAt: new Date(),
    //   updatedAt: new Date()
    // })

    // logger.info({
    //   method: req.method,
    //   content: req.headers['content-length'],
    //   type: req.headers['content-type'],
    //   url: req.originalUrl || req.url,
    //   agent: req.headers['user-agent'],
    //   ip: req.ip,
    //   http: req.httpVersionMajor + '.' + req.httpVersionMinor,
    //   referer: req.headers.referer || req.headers.referrer,
    //   date: new Date(),
    //   params: req.params,
    //   body: req.body
    // });
    // wLog.info({
    //   method: req.method,
    //   content: req.headers['content-length'],
    //   type: req.headers['content-type'],
    //   url: req.originalUrl || req.url,
    //   agent: req.headers['user-agent'],
    //   ip: req.ip,
    //   http: req.httpVersionMajor + '.' + req.httpVersionMinor,
    //   referer: req.headers.referer || req.headers.referrer,
    //   date: new Date(),
    //   params: req.params,
    //   body: req.body
    // })
  }
}


module.exports = {
  LoggerStream,
  MainLogFn
}