const jwt = require('jsonwebtoken');
const { SECRET_KEY } = require('./key')
const { ObjectId } = require('mongodb')

const AdminVerify = (req, res, next) => {
  var db = req.app.get('client').db("heytechQueue");
  const token = req.header('auth-token');

  if (!token || token == null || token == undefined || token == 'null' || token == 'undefined') return res.status(400).json({
    status: "Failed",
    status_code: res.statusCode,
    message: "Access Denied",
    data: 'Token Not Found'
  })

  try {
    const verified = jwt.verify(token, SECRET_KEY);
    console.log(verified)

    db.collection('admins').findOne({ _id: ObjectId(verified._id) }).then((item) => {

      if (item == null || item == undefined) {
        return res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Invalid Token",
          data: 'Token Not Found'
        })
      } else {

        req.user = item;
        next()
      }

    }).catch((err) => {
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Invalid Token",
        data: err
      })
    })


  } catch (err) {
    res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: err
    })
  }


}

module.exports = {
  AdminVerify
}