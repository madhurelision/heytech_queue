const dotenv = require('dotenv')
const crypto = require('crypto');
dotenv.config()

const BASE_URL = 'http://localhost:';
const C_PORT = 3000;
const S_PORT = 5000;
const SERVER_URL = BASE_URL + S_PORT;
//const SERVER_URL = 'https://heybarberserver.herokuapp.com'

const Short_Port = parseInt(process.env.PORT) || 500;
const DATABASE_URL = 'mongodb+srv://heycalls:madhur%404994%40heycalls@cluster0.fuagf.mongodb.net/heytechQueue?retryWrites=true';
const ServiceSmsMail = "https://queuickses.herokuapp.com/heytech/api/";
//const ServiceSmsMail = "http://localhost:5003/heytech/api/";
const ServiceTemplateId = {
  book: '1701164603567987143',
  otp: '1701164603567987143',
  confirm: '1701164603567987143',
  cancel: '1701164603567987143'
}

const SECRET_KEY = "heytech";
const MAIL_SECRET_KEY = "Mailheytech";
//const CLIENT_URL = BASE_URL + C_PORT;
const CLIENT_URL = 'https://heybarber.herokuapp.com';
const ERROR_URL = CLIENT_URL + '/error'

const generateHash = (few) => {

  const val = crypto.createHash('md5').update(few.toString()).digest("hex")

  return val
}

const LOG_SERVICE = "https://heytechlogs.herokuapp.com/"
const SOCKET_URL = 'https://qdiscovery.herokuapp.com'
const KEY = 'heytechdiscovery'


module.exports = {
  SERVER_URL,
  Short_Port,
  DATABASE_URL,
  SECRET_KEY,
  CLIENT_URL,
  ServiceSmsMail,
  MAIL_SECRET_KEY,
  ServiceTemplateId,
  generateHash,
  LOG_SERVICE,
  ERROR_URL,
  SOCKET_URL,
  KEY
}