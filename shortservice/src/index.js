const express = require('express');
const { DATABASE_URL, Short_Port } = require('./config/key');
const { MongoClient } = require('mongodb');
const http = require('http');
const router = require('./routes/routes')
const { MainLogFn } = require('./config/logModule')
const cors = require('cors')
const path = require('path')
const discover = require('../../discovery/disovery')
const defaultConfig = require('./config/default-config')

const app = express()
const httpServer = new http.Server(app);

app.use(
  cors({
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    allowedHeaders: 'Content-Type,auth-token',
    credentials: true, // allow session cookies from browser to pass throught
  }),
);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const connectDB = async (app) => {
  const client = new MongoClient(DATABASE_URL, {
    useNewUrlParser: true, useUnifiedTopology: true
  })

  try {
    await client.connect();
    app.set('client', client);
    console.log('MongoDB Connected :D');
    app.use(MainLogFn.write)
    app.use(discover(defaultConfig))
    app.use('/', router);

  } catch (err) {
    if (err instanceof Error) console.error(err.message);
    process.exit(1);
  }


  app.use(express.static(path.join(__dirname, 'client/build')));
  app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, 'client', 'build', 'index.html'));
  });
};

connectDB(app)

httpServer.listen(Short_Port, () => {
  console.log(`Server is running ${Short_Port}`)
})