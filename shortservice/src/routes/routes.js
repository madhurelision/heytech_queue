const Router = require('express');
const { AdminVerify } = require('../config/verifyToken');
const { adminLogin, getAdminProfile, getAllLogList } = require('../controller/adminController');
const router = Router();
const { createShortUrl, openShortUrl, updateShortUrl, checkServerOnline } = require('../controller/controller')

// ui 
router.post('/login', adminLogin)

router.get('/profile', AdminVerify, getAdminProfile)

router.get('/check', checkServerOnline)

//// log data
router.get('/getlogList/:skip/:limit', AdminVerify, getAllLogList)


router.get('/create/:id', createShortUrl)

router.get('/update/:id', updateShortUrl)

router.get('/:id', openShortUrl)

module.exports = router;