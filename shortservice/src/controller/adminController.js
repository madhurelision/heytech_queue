const jwt = require('jsonwebtoken');
const { SECRET_KEY, generateHash } = require('../config/key')
const { ObjectId } = require('mongodb')


const adminLogin = (req, res, next) => {
  var db = req.app.get('client').db("heytechQueue");

  console.log('body', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  if (req.body.email == null || req.body.email == undefined || req.body.email == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Email is Required",
      data: 'err'
    })
  }

  if (req.body.password == null || req.body.password == undefined || req.body.password == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Password is Required",
      data: 'err'
    })
  }

  console.log('pass', req.body.password, 'hash', generateHash(req.body.password))

  db.collection('admins').findOne({ email: req.body.email, password: generateHash(req.body.password) }).then((item) => {
    console.log('Admin', item)

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: item
      })
    }

    var adminData = item

    const token = jwt.sign({ _id: adminData._id, email: adminData.email }, SECRET_KEY)
    res.header("auth-token", token)

    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: item,
      token: token,
    })

  }).catch((err) => {
    console.log('Admin err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

const getAdminProfile = (req, res) => {
  var db = req.app.get('client').db("heytechQueue");

  console.log('Admin', req.user)
  const val = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  db.collection('admins').findOne({ _id: ObjectId(val._id) }).then((item) => {

    console.log('Admin', item)

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: item
      })
    }

    var adminData = item

    const token = jwt.sign({ _id: adminData._id, email: adminData.email }, SECRET_KEY)
    res.header("auth-token", token)

    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: item,
      token: token
    })

  }).catch((err) => {
    console.log('Admin Profile err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

// Log View
const getAllLogList = (req, res, next) => {
  var db = req.app.get('client').db("heytechQueue");

  db.collection('shortlogs').aggregate([
    {
      '$match': {
        'deleted': false
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'log': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'log': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).toArray((err, item) => {

    if (err) {

      console.log('Log Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        log: [],
        total: 0
      }
    })
  })
}

module.exports = {
  adminLogin,
  getAdminProfile,
  getAllLogList
}