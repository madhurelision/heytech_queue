const { nanoid } = require('nanoid');
const { SERVER_URL, CLIENT_URL, ERROR_URL } = require('../config/key');
const { Main } = require('../config/location');
const { ObjectId } = require('mongodb');
const config = require('config')

const createShortUrl = function (req, res, next) {
  var db = req.app.get('client').db("heytechQueue");


  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', ObjectId.isValid(req.params.id))

  if (ObjectId.isValid(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  db.collection('shorts').insertOne({
    product_id: ObjectId(req.params.id),
    short_id: nanoid(6),
    count: 0,
    status: true,
    deleted: false,
    createdAt: new Date(),
    updatedAt: new Date(),
    type: 'Booking',
    link: ''
  }).then((cc) => {
    console.log('Created', cc)
    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: cc
    })
  }).catch((err) => {
    console.log('Short Url Create Err', err)
    res.status(400).json({
      message: 'Failed',
      status: res.statusCode,
      data: err
    })
  })
}

const updateShortUrl = function (req, res, next) {
  var db = req.app.get('client').db("heytechQueue");
  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', ObjectId.isValid(req.params.id))

  if (ObjectId.isValid(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  db.collection('shorts').findOneAndUpdate({ product_id: ObjectId(req.params.id) }, {
    $set: {
      short_id: nanoid(6),
      updatedAt: new Date()
    }
  }, { upsert: true, new: true, returnNewDocument: true, returnOriginal: false, setDefaultsOnInsert: true }).then((cc) => {
    console.log('Created Or Updated', cc)
    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: cc
    })
  }).catch((err) => {
    console.log('Short Url Update Err', err)
    res.status(400).json({
      message: 'Failed',
      status: res.statusCode,
      data: err
    })
  })
}

const openShortUrl = async function (req, res, next) {
  var db = req.app.get('client').db("heytechQueue");

  if (req.params.id == null || req.params.id == undefined || req.params.id == 'null' || req.params.id == 'undefined' || req.params.id == "") return res.redirect(config.has('ERROR_URL') ? config.get('ERROR_URL') : ERROR_URL)

  var value = {
    ip: (req.headers['x-forwarded-for'] == undefined && req.hostname == 'localhost') ? '127.0.0.1' : req.ip,
    header: req.headers,
    method: req.method,
    ipDetail: {},
    createdAt: new Date(),
    updatedAt: new Date()
  }

  const data = await db.collection('shorts').aggregate([
    {
      $match: {
        short_id: req.params.id,
        deleted: false
      }
    }
  ]).toArray()

  if (data == null || data == undefined || data.length == 0) return res.redirect(config.has('ERROR_URL') ? config.get('ERROR_URL') : ERROR_URL)

  await Main(Test, req.ip)

  async function Test(status, err, cc) {
    if (err) {
      return res.redirect(config.has('ERROR_URL') ? config.get('ERROR_URL') : ERROR_URL)
    }
    console.log('UpD', cc)
    value.ipDetail = (cc == null || cc == undefined) ? {} : cc

    value.product_id = ObjectId(data[0].product_id)
    value.short_id = ObjectId(data[0]._id)
    value.key = req.params.id

    console.log('Short value', value)

    const statC = await db.collection('shortstats').insertOne(value)

    console.log('Created Stat', statC)
  }

  const UpdateCount = await db.collection('shorts').findOneAndUpdate({ _id: data[0]._id }, {
    $inc: { count: 1 }
  }, { new: true, returnNewDocument: true, returnOriginal: false })

  console.log('Updated', UpdateCount)

  const redirectUrl = (data[0].type == 'Booking') ? config.has('CLIENT_URL') ? config.get('CLIENT_URL') + '/booking/' + data[0].product_id : CLIENT_URL + '/booking/' + data[0].product_id : (data[0].type == "Shop") ? config.has('CLIENT_URL') ? config.get('CLIENT_URL') + '/user/' + data[0].product_id : CLIENT_URL + '/user/' + data[0].product_id : (data[0].type == 'Custom') ? data[0].link : config.has('ERROR_URL') ? config.get('ERROR_URL') : ERROR_URL

  res.redirect(redirectUrl)
}

const getAllShortUrl = function (req, res, next) {
  var db = req.app.get('client').db("heytechQueue");

  db.collection('shorts').find({ deleted: false }).then((item) => {
    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: { short: item }
    })
  }).catch((err) => {
    console.log('get All Short Url Err', err)
    res.status(400).json({
      message: 'Failed',
      status: res.statusCode,
      data: err
    })
  })

}

const checkServerOnline = (req, res, next) => {
  res.json(true)
}

module.exports = {
  createShortUrl, updateShortUrl, openShortUrl, getAllShortUrl, checkServerOnline
}