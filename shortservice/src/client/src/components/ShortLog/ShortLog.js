import React, { useEffect, useState } from 'react'
import { TableBody, TableCell, TableRow, IconButton, Dialog, Box, DialogTitle, Button as ButtonM, Grid, Paper, Table, CardContent, TableContainer, Typography, Card } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux';
import { tableStyles, mobileviewStyles } from '../usableComponent/styles'
import { getAllLogList } from '../../redux/action';
import { Close, MoreVert, Refresh } from '@mui/icons-material';
import useTable from '../usableComponent/useTable';
import TablePaginations from '../usableComponent/TablePaginations';
import TableLoader from '../usableComponent/TableLoader';
import moment from 'moment';

const headCells = [
  { id: '', label: '', disableSorting: true },
  { id: 'method', label: 'Method' },
  { id: 'url', label: 'Url' },
  { id: 'ip', label: 'Ip' },
  { id: 'http', label: 'Http' },
  { id: 'agent', label: 'Agent' },
  { id: 'createdAt', label: 'CreatedAt' },
]

const ShortLog = () => {

  const classes = tableStyles()
  const mainClasses = mobileviewStyles()

  const [data, setData] = useState([])
  const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })
  const [flag, setFlag] = useState(false)
  const Loading = useSelector(state => state.auth.loader)
  const allValues = useSelector(state => state.auth.logList);
  const allCount = useSelector(state => state.auth.logTotal)
  const dispatch = useDispatch()

  const [view, setView] = useState({
    open: false,
    data: {},
  })

  const pages = [10, 15, 20]
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(pages[page])

  useEffect(() => {

    if (allValues.length == 0) {
      dispatch(getAllLogList(0, rowsPerPage))
    } else {
      setData(allValues.log)
      setFlag(true)
    }

  }, [allValues])


  const {
    TblContainer,
    TblHead,
    recordsAfterPagingAndSorting
  } = useTable(data, headCells, filterFn)

  const handleChangePage = (event, newPage) => {
    console.log('check', allCount, 'pages', newPage, 'current val', (newPage + 1) * 10, (newPage + 1) * 10 > allCount, 'check Condition', (newPage + 1) * 10 > data.length, allCount > data.length)
    if ((newPage + 1) * rowsPerPage > data.length && allCount > data.length) {
      dispatch(getAllLogList(data.length, rowsPerPage))
    }
    console.log('page', newPage, 'event', event)
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(event.target.value)
    //setPage(0);
    if (event.target.value > data.length && allCount > data.length) {
      dispatch(getAllLogList(data.length, rowsPerPage))
    }
  }


  const handleButton = () => {
    dispatch(getAllLogList(0, data.length == 0 ? rowsPerPage : data.length))
  }

  const handleDialogOpen = (val) => {
    setView({
      ...view,
      open: true,
      data: val,
    })
  }

  const handleClose = () => {
    setView({ ...view, open: false })
  }

  return (
    <Grid container spacing={2} className={classes.grid}>
      <Grid item xs={12} md={10} sm={12}>
        <Card>
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={2} sm={2} md={2} >
                <IconButton title="Refresh" disabled={Loading} onClick={handleButton} >
                  <Refresh />
                </IconButton>
              </Grid>
              <Grid item xs={8} sm={8} md={8}>
                <Typography variant="h4" component="h2" align="center">
                  Short Log({allCount})
                </Typography>
              </Grid>
              <Grid item xs={2} sm={2} md={2}>
              </Grid>
            </Grid>
          </CardContent>

          <CardContent sx={{ display: { xs: "none", md: "block", sm: "none" } }}>
            <TblContainer>
              <TblHead />
              <TableBody>
                {flag ? data.length > 0 ? (
                  recordsAfterPagingAndSorting().slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((row) => {
                    return (<TableRow key={row._id}>
                      <TableCell>
                        <IconButton title={"Detail"} onClick={() => { handleDialogOpen(row) }}>
                          <MoreVert />
                        </IconButton>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {row.method}
                      </TableCell>
                      <TableCell>{row?.url}</TableCell>
                      <TableCell>{row?.ip}</TableCell>
                      <TableCell>{row?.http}</TableCell>
                      <TableCell>{row?.agent}</TableCell>
                      <TableCell>{moment(row.createdAt).format('LL')}</TableCell>
                    </TableRow>)
                  })
                ) : <TableRow>
                  <TableCell colSpan="12">{"No Data Found"}</TableCell>
                </TableRow> : <TableRow>
                  <TableCell colSpan="12">
                    <TableLoader />
                  </TableCell>
                </TableRow>}
              </TableBody>
            </TblContainer>
            {/* <TblPagination /> */}
            {data.length > 0 && <TablePaginations
              page={page}
              pages={pages}
              rowsPerPage={rowsPerPage}
              count={allCount}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage} />}
          </CardContent>
          {/* Mobile View */}
          <TableContainer component={Paper} sx={{ display: { xs: "block", md: "none", sm: "block" } }}>
            <Table size="small" aria-label="simple table">
              <TableBody>
                {flag ? data.length > 0 ? data.map((row) => (
                  <TableRow className={mainClasses.table} key={row._id}>
                    <TableCell>
                      <Card>
                        <CardContent className={mainClasses.cardContentM}>
                          <Grid container spacing={1}>
                            <Grid className={mainClasses.cardTitleM} item xs={6} sm={6}>
                              <div>Method</div>
                              <div className={mainClasses.cardBodyM}>{row.method}</div>
                            </Grid>
                            <Grid className={mainClasses.cardTitleM} item xs={6} sm={6}>
                              <div>Url</div>
                              <div className={mainClasses.cardBodyM}>{row?.url}</div>
                            </Grid>
                            <Grid className={mainClasses.cardTitleM} item xs={6} sm={6}>
                              <div>Ip</div>
                              <div className={mainClasses.cardBodyM}>{row?.ip}</div>
                            </Grid>
                            <Grid className={mainClasses.cardTitleM} item xs={6} sm={6}>
                              <div>Http</div>
                              <div className={mainClasses.cardBodyM}>{row?.http}</div>
                            </Grid>
                            <Grid className={mainClasses.cardTitleM} item xs={12} sm={12}>
                              <div>Agent</div>
                              <div className={mainClasses.cardBodyM}>{row?.agent}</div>
                            </Grid>
                            <Grid className={mainClasses.cardTitleM} item xs={12} sm={12}>
                              <ButtonM className={mainClasses.mobileCardBtn} startIcon={<MoreVert />} onClick={() => { handleDialogOpen(row) }} >
                                Detail
                              </ButtonM>
                            </Grid>
                          </Grid>
                        </CardContent>
                      </Card>
                    </TableCell>
                  </TableRow>
                )) : <TableRow>
                  <TableCell style={{ textAlign: 'center' }} colSpan="12">
                    {"No Data Found"}
                  </TableCell>
                </TableRow> : <TableRow>
                  <TableCell colSpan="12">
                    <TableLoader />
                  </TableCell>
                </TableRow>}
              </TableBody>
            </Table>
          </TableContainer>
        </Card>
      </Grid>
      <Dialog
        open={view.open}
        onClose={handleClose}
        fullScreen
        maxWidth="md"
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="form-dialog-title"><Box display="flex" alignItems="center">
          <Box flexGrow={1} >Log Details</Box>
          <Box>
            <IconButton onClick={handleClose}>
              <Close />
            </IconButton>
          </Box>
        </Box></DialogTitle>
        {Object.keys(view.data).length > 0 && <Grid container spacing={2}>
          <Grid item xs={12} sm={12} md={12}>
            <Card>
              <CardContent>
                <Grid container spacing={2}>
                  <Grid item md={6} xs={12} sm={12}>
                    <div className={mainClasses.mobileCardTitle}>Method</div>
                    <div className={mainClasses.mobileCardData}>{view.data.method}</div>
                  </Grid>
                  <Grid item md={6} xs={12} sm={12}>
                    <div className={mainClasses.mobileCardTitle}>Url</div>
                    <div className={mainClasses.mobileCardData}>{view.data?.url}</div>
                  </Grid>
                  <Grid item md={6} xs={12} sm={12}>
                    <div className={mainClasses.mobileCardTitle}>Ip</div>
                    <div className={mainClasses.mobileCardData}>{view.data?.ip}</div>
                  </Grid>
                  <Grid item md={6} xs={12} sm={12}>
                    <div className={mainClasses.mobileCardTitle}>Http</div>
                    <div className={mainClasses.mobileCardData}>{view.data?.http}</div>
                  </Grid>
                  <Grid item md={6} xs={12} sm={12}>
                    <div className={mainClasses.mobileCardTitle}>Agent</div>
                    <div className={mainClasses.mobileCardData}>{view.data?.agent}</div>
                  </Grid>
                  <Grid item md={6} xs={12} sm={12}>
                    <div className={mainClasses.mobileCardTitle}>Referer</div>
                    <div className={mainClasses.mobileCardData}>{view.data?.referer}</div>
                  </Grid>
                  <Grid item md={6} xs={12} sm={12}>
                    <div className={mainClasses.mobileCardTitle}>Content</div>
                    <div className={mainClasses.mobileCardData}>{view.data?.content}</div>
                  </Grid>
                  <Grid item md={6} xs={12} sm={12}>
                    <div className={mainClasses.mobileCardTitle}>Type</div>
                    <div className={mainClasses.mobileCardData}>{view.data?.type}</div>
                  </Grid>
                  {
                    Object.entries(view.data.header).map(([key, value]) => {
                      return (
                        <Grid item xs={12} sm={12} md={6} key={key + value}>
                          <div className={mainClasses.mobileCardTitle}>{key}</div>
                          <div className={mainClasses.mobileCardData}>{value}</div>
                        </Grid>
                      )
                    })
                  }
                </Grid>
              </CardContent>
            </Card>
          </Grid>
        </Grid>}
      </Dialog>
    </Grid>
  )
}

export default ShortLog