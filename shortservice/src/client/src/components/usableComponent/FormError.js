import React from 'react'
import { FormHelperText } from '@mui/material'
import { makeStyles } from '@mui/styles'

const styles = {
  main: {
    color: 'red !important'
  }
};

const useStyles = makeStyles(styles);

const FormError = ({ data }) => {

  const classes = useStyles()

  return (
    <FormHelperText className={classes.main}>{data}</FormHelperText>
  )
}

export default FormError
