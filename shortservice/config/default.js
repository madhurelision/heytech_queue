const dotenv = require('dotenv')
dotenv.config()

const BASE_URL = 'http://localhost:';
const C_PORT = 3000;
const S_PORT = 5000;
const SERVER_URL = BASE_URL + S_PORT;

const Short_Port = parseInt(process.env.PORT) || 500;
const DATABASE_URL = 'mongodb+srv://heycalls:madhur%404994%40heycalls@cluster0.fuagf.mongodb.net/heytechQueue?retryWrites=true';

const ServiceSmsMail = "http://localhost:5003/heytech/api/";


const SECRET_KEY = "heytech";
const MAIL_SECRET_KEY = "Mailheytech";
const CLIENT_URL = BASE_URL + C_PORT;
const ERROR_URL = CLIENT_URL + '/error';

const LOG_SERVICE = "http://localhost:5004/"
const SOCKET_URL = 'http://localhost:5006'
const KEY = 'heytechdiscovery'

module.exports = {
  SERVER_URL,
  Short_Port,
  DATABASE_URL,
  SECRET_KEY,
  CLIENT_URL,
  ServiceSmsMail,
  MAIL_SECRET_KEY,
  LOG_SERVICE,
  ERROR_URL,
  SOCKET_URL,
  KEY
}