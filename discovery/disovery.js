const onHeaders = require('on-headers');
const validate = require('./validate');
const onHeadersListener = require('./header-listener');
const socketIoInit = require('./socket-io-Init');
const { SOCKET_URL } = require('./config');

const middlewareWrapper = config => {
  const validatedConfig = validate(config);
  console.log('file supports')
  socketIoInit(SOCKET_URL, validatedConfig);
  const middleware = (req, res, next) => {
    const startTime = process.hrtime();

    onHeaders(res, () => {
      onHeadersListener(res.statusCode, startTime, validatedConfig.spans);
    });

    next();
  };
  middleware.middleware = middleware;
  return middleware;
};

module.exports = middlewareWrapper;