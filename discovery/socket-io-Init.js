
const socket = require('socket.io-client');
const os = require('os')
const { KEY } = require('./config');
const gatherOsMetrics = require('./gatherOsMetrics');

module.exports = (SERVER_URL, config) => {
  const jwt = require('jsonwebtoken')

  const token = jwt.sign({ name: config.name }, KEY)

  const io = socket(SERVER_URL, {
    reconnection: true,
    transports: ["websocket"],
    upgrade: false,
    query: `token=${token}`
  });

  io.on('connect', () => {
    console.log('socket id connected')
    io.emit('userConnect')

    io.emit('ram', {
      ram: os.networkInterfaces(),
      name: config.name
    })
  })

  io.on('status', (data) => {
    console.log('status', data)
  })

  config.spans.forEach(span => {
    span.os = [];
    span.responses = [];
    const interval = setInterval(() => gatherOsMetrics(io, span, config.name), span.interval * 1000);

    // Don't keep Node.js process up
    interval.unref();
  });

  io.on('disconnect', () => {
    console.log('socket is disconnected')
  })

}