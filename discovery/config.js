const SOCKET_URL = 'http://localhost:5006'
//const SOCKET_URL = 'https://qdiscovery.herokuapp.com'
const KEY = 'heytechdiscovery'

module.exports = {
  SOCKET_URL,
  KEY
}