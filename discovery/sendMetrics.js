module.exports = (io, span, name) => {
  io.emit('esm_stats', {
    name: name,
    stat: {
      os: span.os[span.os.length - 2],
      responses: span.responses[span.responses.length - 2],
      interval: span.interval,
      retention: span.retention,
    }
  });
};