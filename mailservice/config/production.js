const LOG_SERVICE = "https://heytechlogs.herokuapp.com/"
const SOCKET_URL = 'https://qdiscovery.herokuapp.com'
const KEY = 'heytechdiscovery'

module.exports = {
  LOG_SERVICE,
  SOCKET_URL,
  KEY
}