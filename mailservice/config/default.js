const LOG_SERVICE = "http://localhost:5004/"
const SOCKET_URL = 'http://localhost:5006'
const KEY = 'heytechdiscovery'

module.exports = {
  LOG_SERVICE,
  SOCKET_URL,
  KEY
}