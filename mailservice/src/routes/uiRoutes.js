const Router = require('express');
const router = Router();
const { AdminVerify } = require('../config/verifyToken');
const { adminLogin, getAllLogList, getAdminProfile } = require('../controllers/adminController');

// ui 
router.post('/login', adminLogin)
router.get('/profile', AdminVerify, getAdminProfile)

//// log data
router.get('/getlogList/:skip/:limit', AdminVerify, getAllLogList)

module.exports = router;