const Router = require('express');
const router = Router();

const { emailSend, smsSend, bookSmsSend, emailOtpSend, newSMS } = require('../controllers/controller')
const { ApiVerify } = require('../config/verifyToken');


router.post("/sendSms", ApiVerify, smsSend);

router.post("/sendEmail", ApiVerify, emailSend);

router.post("/sendOtpMail", ApiVerify, emailOtpSend);

router.post('/sendBooking', ApiVerify, bookSmsSend)

router.post('/smsSend', ApiVerify, newSMS)

module.exports = router;