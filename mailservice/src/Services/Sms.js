
var unirest = require("unirest");
const { ObjectId } = require("mongodb");

const { SMS_body } = require('../config/Template')
const sms = () => {
  const accountSid = process.env.TWILIO_ACCOUNT_SID;
  const authToken = process.env.TWILIO_AUTH_TOKEN;
  const client = require('twilio')(accountSid, authToken);
  client.messages
    .create({
      body: 'This is the ship that made the Kessel Run in fourteen parsecs?',
      from: '+15017122661',
      to: '+15558675310'
    })
    .then(message => console.log(message.sid));

}

const sms1 = async (wait, body, id) => {
  console.log("SMS1")
  var db = wait.app.get('client').db("heytechQueue");


  var req = unirest("GET", "https://www.fast2sms.com/dev/bulkV2");
  var text = await SMS_body('Hair', body.otp)
  await req.query({
    "authorization": "LbtlVpQQB6nuPPXRjmOJ7NDHdP4GZBFBWroBjrkDECHfCb76VoHLnq9VitK2",
    "variables_values": text,
    "route": "otp",
    "numbers": body.number
  });

  req.headers({
    "cache-control": "no-cache"
  });


  req.end(async function (res) {
    if (res.error) {
      const upService = await db.collection('servicerequests').findOneAndUpdate({ _id: ObjectId(id) }, {
        $set: {
          message: { message: res.error },
          status: false,
          updatedAt: new Date()
        }
      })
      console.log('save Reques', upService)

      console.log(res.error);
      return
    }

    const upsService = await db.collection('servicerequests').findOneAndUpdate({ _id: ObjectId(id) }, {
      $set: {
        message: { ...res.body },
        status: true,
        updatedAt: new Date()
      }
    })

    console.log('save Reques', upsService)

    console.log(res.body);
  });
}

const sms2 = async (wait, body, id) => {
  console.log("SMS2")
  var db = wait.app.get('client').db("heytechQueue");

  var req = unirest("GET", "https://www.fast2sms.com/dev/bulkV2");
  var text = body.message
  await req.query({
    "authorization": "LbtlVpQQB6nuPPXRjmOJ7NDHdP4GZBFBWroBjrkDECHfCb76VoHLnq9VitK2",
    "variables_values": text,
    "route": "otp",
    "numbers": body.number
  });

  req.headers({
    "cache-control": "no-cache"
  });


  req.end(async function (res) {
    if (res.error) {
      const upService = await db.collection('servicerequests').findOneAndUpdate({ _id: ObjectId(id) }, {
        $set: {
          message: { message: res.error },
          status: false,
          updatedAt: new Date()
        }
      })
      console.log('save Reques', upService)
      console.log(res.error);
      return
    }

    const upsService = await db.collection('servicerequests').findOneAndUpdate({ _id: ObjectId(id) }, {
      $set: {
        message: { ...res.body },
        status: true,
        updatedAt: new Date()
      }
    })
    console.log('save Reques', upsService)

    console.log(res.body);
  });

}


module.exports = { sms, sms1, sms2 };