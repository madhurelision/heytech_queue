const axios = require('axios');
const { ObjectId } = require("mongodb");
const { SMS_CREDENTIAL } = require('../config/keys')


const smsSendN = async (req, id) => {
  var db = req.app.get('client').db("heytechQueue");

  console.log('req.body', req.body, 'req.user', req.user, 'id', id)
  const urlD = encodeURIComponent(req.user.message)

  console.log('check', urlD)

  console.log('req', `http://sms.innuvissolutions.com/api/mt/SendSMS?APIKey=${SMS_CREDENTIAL.key}&senderid=${SMS_CREDENTIAL.sender}&channel=${SMS_CREDENTIAL.channel}&DCS=${SMS_CREDENTIAL.dcs}&flashsms=${SMS_CREDENTIAL.flash}&number=91${req.body.number}&text=${urlD}&route=${SMS_CREDENTIAL.route}&peid=${req.user.id}`)

  axios.get(`http://sms.innuvissolutions.com/api/mt/SendSMS?APIKey=${SMS_CREDENTIAL.key}&senderid=${SMS_CREDENTIAL.sender}&channel=${SMS_CREDENTIAL.channel}&DCS=${SMS_CREDENTIAL.dcs}&flashsms=${SMS_CREDENTIAL.flash}&number=91${req.body.number}&text=${urlD}&route=${SMS_CREDENTIAL.route}&peid=${req.user.id}`).then(async (cc) => {
    console.log('reponse msg', cc.data)
    const upsService = await db.collection('servicerequests').findOneAndUpdate({ _id: ObjectId(id) }, {
      $set: {
        message: { ...cc.data },
        status: true,
        updatedAt: new Date()
      }
    })
    console.log('save Reques', upsService)

  }).catch(async (err) => {
    console.log('sms Send', err)
    const upService = await db.collection('servicerequests').findOneAndUpdate({ _id: ObjectId(id) }, {
      $set: {
        message: { message: err },
        status: false,
        updatedAt: new Date()
      }
    })
    console.log('save Reques', upService)
  })

}

module.exports = {
  smsSendN
}