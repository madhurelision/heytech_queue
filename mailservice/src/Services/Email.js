const nodemailer = require("nodemailer");
const { Email_body_Email, Email_body_OTP, Email_Subject } = require('../config/Template')
const { ObjectId } = require("mongodb");

const email = async (req, body, id) => {
  var db = req.app.get('client').db("heytechQueue");
  try {
    let transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: 'heybarber.heytech@gmail.com', // generated ethereal user
        pass: 'madhur@4994', // generated ethereal password
      },
    });

    var text = await Email_body_Email("Hair", body.link)

    let info = await transporter.sendMail({
      from: 'heybarber.heytech@gmail.com', // sender address
      to: body.email, // list of receivers
      subject: Email_Subject, // Subject line
      text: text, // plain text body
      html: `<b>${text}</b>`, // html body
    });

    console.log("Message sent: %s", info.messageId);
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    const upService = await db.collection('servicerequests').findOneAndUpdate({ _id: ObjectId(id) }, {
      $set: {
        message: info,
        status: true,
        updatedAt: new Date()
      }
    })
    console.log('save Reques', upService)
  } catch (e) {
    console.log(e)
    const upsService = await db.collection('servicerequests').findOneAndUpdate({ _id: ObjectId(id) }, {
      $set: {
        message: { message: e },
        status: false,
        updatedAt: new Date()
      }
    })
    console.log('save Reques', upsService)
  }


}



const email1 = () => {
  try { } catch (e) { console.log(e) }
}

const emailOtp = async (req, body, id) => {
  var db = req.app.get('client').db("heytechQueue");
  try {
    let transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: 'heybarber.heytech@gmail.com', // generated ethereal user
        pass: 'madhur@4994', // generated ethereal password
      },
    });

    var text = await Email_body_OTP("Hair", body.otp)

    let info = await transporter.sendMail({
      from: 'heybarber.heytech@gmail.com', // sender address
      to: body.email, // list of receivers
      subject: Email_Subject, // Subject line
      text: text, // plain text body
      html: `<b>${text}</b>`, // html body
    });

    console.log("Message sent: %s", info.messageId);
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    const upService = await db.collection('servicerequests').findOneAndUpdate({ _id: ObjectId(id) }, {
      $set: {
        message: info,
        status: true,
        updatedAt: new Date()
      }
    })
    console.log('save Reques', upService)
  } catch (e) {
    console.log(e)
    const upsService = await db.collection('servicerequests').findOneAndUpdate({ _id: ObjectId(id) }, {
      $set: {
        message: { message: e },
        status: false,
        updatedAt: new Date()
      }
    })
    console.log('save Reques', upsService)
  }


}

module.exports = { email, email1, emailOtp };