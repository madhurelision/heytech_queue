const express = require('express')
const { Port, DATABASE_URL } = require('./config/keys')
const router = require('./routes/route')
const app = express()
const { MongoClient } = require('mongodb');
const cors = require('cors')
const { MainLogFn } = require('./config/logModule')
const uiRoute = require('./routes/uiRoutes')
const path = require('path')
const discover = require('../../discovery/disovery')
const defaultConfig = require('./config/default-config')

app.use(express.json());
app.use(cors({
  origin: '*',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  allowedHeaders: 'Content-Type,auth-token',
  credentials: true, // allow session cookies from browser to pass throught
}));

const connectDB = async (app) => {
  const client = new MongoClient(DATABASE_URL, {
    useNewUrlParser: true, useUnifiedTopology: true
  })

  try {
    await client.connect();
    app.set('client', client);
    console.log('MongoDB Connected :D');

    app.use(discover(defaultConfig))

    app.use(MainLogFn.write)
    app.use('/heytech/api/', router);
    app.use('/ui/', uiRoute)


    app.get('/check', async (req, res, next) => {
      res.json(true)
    })
    app.use(express.static(path.join(__dirname, 'client/build')));
    app.get('*', function (req, res) {

      res.sendFile(path.join(__dirname, 'client', 'build', 'index.html'));

    });


  } catch (err) {
    if (err instanceof Error) console.error(err.message);
    process.exit(1);
  }
};

connectDB(app)



app.listen(Port, () => {
  console.log(`Server is Running ${Port}`)
})