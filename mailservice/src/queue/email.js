const {Queue,Worker} = require('bullmq');

const sendMailQueue = new Queue('sendMail', {
  connection: {
    host: 'redis-17642.c266.us-east-1-3.ec2.cloud.redislabs.com',
    port: 17642
  }
});

const worker = new Worker('sendMail', {
  connection: {
    host: 'redis-17642.c266.us-east-1-3.ec2.cloud.redislabs.com',
    port: 17642
  }
});

module.exports=sendMailQueue
