const Email_Subject = "HeyBarber Queue"

const SMS_body = async (service, otp) => {
    return `for ${service} code ${otp}`;
}
const Email_body_OTP = async (service, otp) => {
    var text = `Your OTP for service ${service} is ${otp}`
    return text;
}
const Email_body_Email = async (service, link) => {
    var text = `Your service ${service} link is ${link}`
    return text;
}

module.exports = { SMS_body, Email_body_Email, Email_body_OTP, Email_Subject }