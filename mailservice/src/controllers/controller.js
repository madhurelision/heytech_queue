const { email, emailOtp } = require('../Services/Email')
const { sms, sms1, sms2 } = require('../Services/Sms')
const { smsSendN } = require('../Services/SmsInnovation')
const { ObjectId } = require('mongodb')

const emailSend = async (req, res, next) => {
    var db = req.app.get('client').db("heytechQueue");
    const saveService = await db.collection('servicerequests').insertOne({
        email: req.body.email,
        data: { ...req.body, ...req.user, message_length: req.user.message.length },
        val: req.user.type,
        resend: (req.user.resend == null || req.user.resend == undefined || req.user.resend == '') ? 'single' : req.user.resend,
        user_id: req.user.user_id.map((cc) => ObjectId(cc)),
        type: 'email',
        deleted: false,
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
    })
    console.log('save Reques', saveService)

    console.log(req.body)
    await email(req, req.body, saveService.insertedId);
    res.json("email")
}
const emailOtpSend = async (req, res, next) => {
    var db = req.app.get('client').db("heytechQueue");
    const saveService = await db.collection('servicerequests').insertOne({
        email: req.body.email,
        data: { ...req.body, ...req.user, message_length: req.user.message.length },
        val: req.user.type,
        resend: (req.user.resend == null || req.user.resend == undefined || req.user.resend == '') ? 'single' : req.user.resend,
        user_id: req.user.user_id.map((cc) => ObjectId(cc)),
        type: 'email',
        deleted: false,
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
    })
    console.log('save Reques', saveService)

    console.log(req.body)
    await emailOtp(req, req.body, saveService.insertedId);
    res.json("email")
}
const smsSend = async (req, res, next) => {
    var db = req.app.get('client').db("heytechQueue");
    const saveService = await db.collection('servicerequests').insertOne({
        mobile: JSON.stringify(req.body.number),
        data: { ...req.body, ...req.user, message_length: req.user.message.length },
        val: req.user.type,
        resend: (req.user.resend == null || req.user.resend == undefined || req.user.resend == '') ? 'single' : req.user.resend,
        user_id: req.user.user_id.map((cc) => ObjectId(cc)),
        type: 'sms',
        deleted: false,
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
    })
    console.log('save Reques', saveService)
    console.log(req.body)
    await sms1(req, req.body, saveService.insertedId);
    res.json("sms")
}
const bookSmsSend = async (req, res, next) => {
    var db = req.app.get('client').db("heytechQueue");
    const saveService = await db.collection('servicerequests').insertOne({
        mobile: JSON.stringify(req.body.number),
        data: { ...req.body, ...req.user, message_length: req.user.message.length },
        val: req.user.type,
        resend: (req.user.resend == null || req.user.resend == undefined || req.user.resend == '') ? 'single' : req.user.resend,
        user_id: req.user.user_id.map((cc) => ObjectId(cc)),
        type: 'sms',
        deleted: false,
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
    })
    console.log('save Reques', saveService)
    console.log(req.body)
    await sms2(req, req.body, saveService.insertedId);
    res.json("sms")
}

const newSMS = async (req, res, next) => {
    var db = req.app.get('client').db("heytechQueue");
    const saveService = await db.collection('servicerequests').insertOne({
        mobile: JSON.stringify(req.body.number),
        data: { ...req.body, ...req.user, message_length: req.user.message.length },
        val: req.user.type,
        resend: (req.user.resend == null || req.user.resend == undefined || req.user.resend == '') ? 'single' : req.user.resend,
        user_id: req.user.user_id.map((cc) => ObjectId(cc)),
        type: 'sms',
        deleted: false,
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
    })
    console.log('save Reques', saveService)
    console.log(req.body)

    await smsSendN(req, saveService.insertedId)
    res.json("sms")

}

module.exports = { emailSend, smsSend, bookSmsSend, emailOtpSend, newSMS };