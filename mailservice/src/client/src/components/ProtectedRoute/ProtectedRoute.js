import React, { useEffect } from 'react'
import { Navigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux'
import { getProfileAdmin } from '../../redux/action/index'

const ProtectedRoute = ({ children }) => {

  const dispatch = useDispatch()
  const user = useSelector(state => state.auth.isAuthenticated)

  useEffect(() => {

    if (localStorage.getItem('short-token') && user == false) {
      dispatch(getProfileAdmin())
    }

  }, [user])

  return (localStorage.getItem('short-token')) ? children : <Navigate to="/" />
}

export default ProtectedRoute
