const { MongoClient } = require("mongodb");
const queue = require("../queue");
const { initQueues } = require("../queue/multiQueue");
const { DATABASE_URL } = require("./key");

const client = new MongoClient(DATABASE_URL, {
  useNewUrlParser: true, useUnifiedTopology: true
});
var defaultValues;

async function init(app) {
  try {
    await client.connect();
    defaultValues = await findData(app, client)
    console.log('MongoDB Connected :D');

  } catch (err) {
    if (err instanceof Error) console.error(err.message);
    process.exit(1);
  }
}


async function findData(app, client) {
  const result = await client.db("heytechQueue").collection("shops").find({ deleted: false }).toArray();
  if (result) {
    console.log(`Found a listing in the collection with the name `);
    initQueues(result)
    queue(app)
  } else {
    console.log(`No listings found with the name `);
  }
  return { result }
}

module.exports = { client, init };
