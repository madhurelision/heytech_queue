const Router = require('express');
const { checkBookingQueueAdd } = require('../controller/queueController');
const router = Router();

router.post('/api/add/:id', checkBookingQueueAdd)

module.exports = router;