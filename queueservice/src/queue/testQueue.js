const Bull = require('bull');
const { redisConfig } = require('../config/key');

const testQueue = new Bull('testQueue', { redis: redisConfig });

testQueue.process((job, done) => {
  done(null, job.data)
  return "vishwaUrl";
})

const testAdd = (data) => {
  console.log('queue', data)
  testQueue.add(data)
}

module.exports = { testQueue, testAdd }