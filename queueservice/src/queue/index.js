const { createClient } = require('redis');
const { createBullBoard } = require('@bull-board/api');
const { BullAdapter } = require('@bull-board/api/bullAdapter');
const { ExpressAdapter } = require('@bull-board/express');
const { testAdd, testQueue } = require('./testQueue');
const { redisConfig, redifconf } = require('../config/key');
const { getAllQueue, getQueue } = require('./multiQueue');
const _ = {}

const queue = (app) => {

  try {
    var redisClient = createClient(redifconf);
    redisClient.connect()
    redisClient.on('connect', () => {
      console.log('Redis Connection Established');
      _.connect = true
    });
    redisClient.on('error', () => {
      console.error('Redis Failed To Establish Connection');
    });

    const serverAdapter = new ExpressAdapter();

    const board = createBullBoard({
      queues: [
        new BullAdapter(testQueue),
        ...getAllQueue().map((cc) => {
          return new BullAdapter(cc)
        })
      ],
      serverAdapter
    })

    serverAdapter.setBasePath('/admin/queues')

    app.use('/admin/queues', serverAdapter.getRouter());

    app.get('/api/test/send', async (req, res) => {

      testAdd({ data: 'Vishwa', wait: 'hello' })
      res.send({ status: 'ok' });
    });

    app.get('/api/getConnection', async (req, res) => {

      try {
        const val = await redisClient.info('clients')
        console.log('test', val)
        var str = val
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          main: val,
          data: str.replace('#', '').replace('\\r\\n', ' ').replace('\r\n', '').replace('\r\n', ' ').replace('\r\n', ' ').replace('\r\n', ' ').replace('\r\n', ' ').replace('\r\n', ' ').replace('\r\n', ' ').replace('\r\n', ' ')
        })
      } catch (err) {
        console.log('finde Comemane', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      }
    })
  } catch (err) {
    console.log('Redis Connection Err', err)
  } finally {
    if (_.connect) {
      redisClient.disconnect()
    }
  }

};

module.exports = queue;
