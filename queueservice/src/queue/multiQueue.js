const Bull = require('bull');
const moment = require('moment');
const { redisConfig } = require('../config/key');
const queues = {};

// 2
const QUEUE_PROCESSORS = {
  test: (job, done) => {
    console.log(`${moment()}::Job with id: ${job.id} is being executed.\n`, {
      message: job.data.message
    });
    const { checkBookingQueue } = require('../controller/queueController');

    checkBookingQueue(job.data.id, job.data.body, (status, err, data) => {

      if (status == 200) return done(null, data)
      if (status == 400) return done(err)

    })

    return "HeytechUrl";
  }
};
// 3
const initQueues = (arr) => {
  console.log('init queues');
  arr.forEach((queueName) => {
    // 4
    queues[queueName['shop_name']] = getQueue(queueName['shop_name']);
    // 5
    queues[queueName['shop_name']].process(QUEUE_PROCESSORS.test);
  });
  return Object.values(queues).length
};

const getQueue = (queueName) => {
  if (!queues[queueName]) {
    queues[queueName] = new Bull(queueName, { redis: redisConfig });
    console.log('created queue: ', queueName);
  }
  return queues[queueName];
};

const getAllQueue = () => {
  if (Object.keys(queues).length == 0) {
    console.log('made', 0)
    return []
  } else {
    const test = Object.values(queues)
    console.log('check', test.length)
    return test
  }
}

module.exports = { QUEUE_PROCESSORS, getAllQueue, getQueue, initQueues }