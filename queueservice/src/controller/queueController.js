const { createShortUrl } = require('../config/function')
const { ObjectId } = require('mongodb')
const { performance } = require('perf_hooks')
const { getQueue } = require('../queue/multiQueue')

const checkBookingQueueAdd = async (req, res, next) => {
  const { client } = require('../config/connectDB')
  var db = await client.db("heytechQueue")

  if (req.params.id == null || req.params.id == undefined || req.params.id == '') {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }
  console.log('body', req.body, 'Id', req.params.id)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  console.log('check', ObjectId.isValid(req.params.id))

  if (ObjectId.isValid(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  db.collection('bookings').aggregate([
    {
      $match: {
        '_id': ObjectId(req.params.id),
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    },
    {
      '$unwind': {
        'path': '$shop_id'
      }
    },
  ]).toArray(async (err, item) => {
    if (err) {
      console.log('bookErr', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "All Fields are Required",
        data: err
      })
    }

    if (item.length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "All Fields are Required",
        data: 'err'
      })
    }
    const add = getQueue(item[0].shop_id.shop_name)
    add.add({ id: req.params.id, body: req.body })
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: 'Success'
    })

  })

}

const checkBookingQueue = async (id, body, cb) => {
  const { client } = require('../config/connectDB')
  var db = await client.db("heytechQueue")

  var aTime = performance.now()
  const q1 = [
    {
      $match: {
        '_id': ObjectId(id),
      }
    },
    {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    },
    {
      '$unwind': {
        'path': '$service_id'
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    },
    {
      '$unwind': {
        'path': '$shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'travels',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'travel'
      }
    },
    {
      '$addFields': {
        'distance': {
          '$arrayElemAt': [
            '$travel.distance', 0
          ]
        },
        'distanceVal': {
          '$arrayElemAt': [
            '$travel.distanceVal', 0
          ]
        },
        'duration': {
          '$arrayElemAt': [
            '$travel.duration', 0
          ]
        },
        'durationVal': {
          '$arrayElemAt': [
            '$travel.durationVal', 0
          ]
        },
        'lattitude': {
          '$arrayElemAt': [
            '$travel.lattitude', 0
          ]
        },
        'longitude': {
          '$arrayElemAt': [
            '$travel.longitude', 0
          ]
        },
      }
    },
  ]
  db.collection('bookings').aggregate(q1).toArray(async (err, item) => {
    if (err) {
      console.log('Booking, find err', err)

      return cb(400, {
        status: 'Failed',
        status_code: 400,
        message: "Otp Verification Failed",
        data: err
      }, null)
    }

    if (item.length == 0) {
      return cb(400, {
        status: 'Failed',
        status_code: 400,
        message: "Otp Verification Failed",
        data: item
      }, null)
    }

    var bTime = (performance.now() - aTime).toFixed();

    const clogw = await db.collection('logqueries').insertOne({
      queryName: q1,
      collectionName: 'bookings',
      startTime: aTime,
      endTime: parseInt(bTime),
      status: true,
      deleted: false,
      createdAt: new Date(),
      updatedAt: new Date()
    })

    var startTime = performance.now()
    const q2 = [
      {
        '$match': {
          '_id': ObjectId(item[0].service_id._id),
          'user_id': ObjectId(item[0].shop_id.user_information)
        }
      }, {
        '$unwind': {
          'path': '$seat'
        }
      }, {
        '$lookup': {
          'from': 'bookings',
          'let': {
            'main': '$user_id',
            'service': '$_id',
            'seat': '$seat',
          },
          'pipeline': [
            {
              '$sort': {
                '_id': -1
              }
            }, {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$seat', '$$seat'
                      ]
                    }, {
                      '$eq': [
                        '$queue', true
                      ]
                    }, {
                      '$eq': [
                        '$test', false
                      ]
                    }, {
                      '$gte': [
                        '$approximate_date', new Date()
                      ]
                    }, {
                      '$in': [
                        '$status', [
                          'waiting', 'accepted'
                        ]
                      ]
                    }, {
                      '$eq': [
                        '$otp_verify', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'booking'
        }
      }, {
        '$addFields': {
          'user_booking': {
            '$arrayElemAt': [
              '$booking.approximate_date', 0
            ]
          }
        }
      }, {
        '$group': {
          '_id': '$seat',
          'data': {
            '$first': '$user_booking'
          }
        }
      }, {
        '$sort': {
          'data': 1
        }
      }
    ]
    db.collection('services').aggregate(q2).toArray(async (error, value) => {

      if (error) {
        console.log('Booking, find err', error)

        return cb(400, {
          status: 'Failed',
          status_code: 400,
          message: "Otp Verification Failed",
          data: error
        }, null)
      }

      if (value.length == 0) {
        return cb(400, {
          status: 'Failed',
          status_code: 400,
          message: "Otp Verification Failed",
          data: item
        }, null)
      }


      var endTime = (performance.now() - startTime).toFixed();

      const clogsw = await db.collection('logqueries').insertOne({
        queryName: q2,
        collectionName: 'services',
        startTime: startTime,
        endTime: parseInt(endTime),
        status: true,
        deleted: false,
        createdAt: new Date(),
        updatedAt: new Date()
      })

      console.log('Booking Data', item[0])
      console.log('Server Data', value)

      const sData = value
      const mData = value.filter((cc) => cc.data == null)

      const file = {
        approximate_time: parseInt(item[0].service_id.waiting_number) * parseInt(item[0].size) + ' ' + item[0].service_id.waiting_key,
        newtime: (item[0].service_id.waiting_key == 'min') ? parseInt(item[0].service_id.waiting_number) * parseInt(item[0].size) : parseInt(item[0].service_id.waiting_number) * parseInt(item[0].size) * 60
      }

      const location = (item[0].travel.length == 0) ? 0 : (item[0].travel[0].durationVal == null || item[0].travel[0].durationVal == undefined) ? 0 : item[0].travel[0].durationVal

      const text = (mData.length > 0) ? mData[0]._id : sData[0]._id

      console.log('text', text)

      var wTime = performance.now()

      const query = (location == 0) ? [
        {
          '$match': {
            'shop_id': ObjectId(item[0].shop_id.user_information),
            'queue': true,
            'test': false,
            'appointment_date': { '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0)), $lt: new Date(new Date().setHours(23, 59, 59)) },
            'status': { '$in': ['waiting', 'accepted'] },
            'approximate_date': { '$gte': new Date(item[0].appointment_date) },
            'otp_verify': true,
            'seat': text
          }
        },
        {
          '$sort': {
            'approximate_date': -1
          }
        },
        {
          '$limit': 1
        }
      ] : [
        {
          '$match': {
            'shop_id': ObjectId(item[0].shop_id.user_information),
            'queue': true,
            'test': false,
            'appointment_date': { '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0)), $lt: new Date(new Date().setHours(23, 59, 59)) },
            'status': { '$in': ['waiting', 'accepted'] },
            'approximate_date': { '$gte': new Date(item[0].appointment_date) },
            'otp_verify': true,
            'seat': text
          }
        },
        {
          '$sort': {
            'approximate_date': -1
          }
        },
        {
          '$lookup': {
            'from': 'travels',
            'localField': '_id',
            'foreignField': 'booking_id',
            'as': 'travel'
          }
        },
        {
          '$addFields': {
            'distance': {
              '$arrayElemAt': [
                '$travel.distance', 0
              ]
            },
            'distanceVal': {
              '$arrayElemAt': [
                '$travel.distanceVal', 0
              ]
            },
            'duration': {
              '$arrayElemAt': [
                '$travel.duration', 0
              ]
            },
            'durationVal': {
              '$arrayElemAt': [
                '$travel.durationVal', 0
              ]
            }
          }
        },
        {
          '$match': {
            'durationVal': {
              '$lte': location
            }
          }
        },
        {
          '$sort': {
            'durationVal': 1
          }
        },
        {
          '$limit': 1
        }
      ]

      const MainCheck = await db.collection('bookings').aggregate(query).toArray()

      var rTime = (performance.now() - wTime).toFixed();

      const clogswa = await db.collection('logqueries').insertOne({
        queryName: query,
        collectionName: 'bookings',
        startTime: wTime,
        endTime: parseInt(rTime),
        status: true,
        deleted: false,
        createdAt: new Date(),
        updatedAt: new Date()
      })

      console.log('mainD Data', MainCheck)

      var mainD = (MainCheck.length > 0) ? MainCheck[0].approximate_date : createQueueDate(item[0].appointment_date, parseInt((!item[0].shop_id.waiting_number || item[0].shop_id.waiting_number == null || item[0].shop_id.waiting_number == undefined) ? 0 : item[0].shop_id.waiting_number))

      console.log('mainD', mainD, createQueueDate(item[0].appointment_date, parseInt((!item[0].shop_id.waiting_number || item[0].shop_id.waiting_number == null || item[0].shop_id.waiting_number == undefined) ? 0 : item[0].shop_id.waiting_number)))

      var data = {}
      data.waiting_time = (MainCheck.length) ? parseInt(minusDate(mainD, item[0].appointment_date)) + ' ' + 'min' : item[0].shop_id.waiting_number
      data.waiting_number = (MainCheck.length) ? parseInt(minusDate(mainD, item[0].appointment_date)) : parseInt(item[0].shop_id.waiting_number)
      data.waiting_key = 'min'
      data.waiting_date = mainD
      data.approximate_date = createQueueDate(mainD, parseInt((!file.newtime || file.newtime == null || file.newtime == undefined) ? 0 : file.newtime))
      data.approximate_time = file.approximate_time
      data.approximate_number = parseInt(file.approximate_time)
      data.approximate_key = file.approximate_time.replace(/[0-9]/g, '').replace(' ', '')
      data.end_time = mainD
      data.waiting_verify = true
      data.seat = text

      console.log('data set', data)

      db.collection('bookings').findOneAndUpdate({ _id: ObjectId(id), otp: parseInt(body.otp) }, {
        $set: {
          ...data,
          otp_verify: true
        }
      }, { new: true, returnNewDocument: true, returnOriginal: false }).then(async (item) => {

        console.log('test Verify Booking', item.value, item.value == null, item.value == undefined)

        if (item.value == null || item.value == undefined) {

          console.log('check', otpPass, parseInt(body.otp), otpPass == parseInt(body.otp))

          if (otpPass == parseInt(body.otp)) {

            db.collection('bookings').findOneAndUpdate({ _id: ObjectId(id) }, {
              $set: {
                ...data,
                otp_verify: true
              }
            }, { new: true, returnNewDocument: true, returnOriginal: false }).then(async (val) => {
              const ShortUrl = await createShortUrl(val.value._id)

              return cb(200, null, {
                status: 'Success',
                status_code: 200,
                message: "Request Success",
                data: val.value
              })
            }).catch((err) => {
              console.log('Booking Otp Verification Err', err)
              cb(400, {
                status: 'Failed',
                status_code: 400,
                message: "Request Failed",
                data: err
              }, null)

            })



          } else {

            return cb(400, {
              status: 'Failed',
              status_code: 400,
              message: "Otp Verification Failed",
              data: item
            }, null)

          }
        } else {

          const ShortUrl = await createShortUrl(item.value._id)

          cb(200, null, {
            status: 'Success',
            status_code: 200,
            message: "Request Success",
            data: item.value
          })
        }

      }).catch((err) => {
        console.log('Booking Otp Verification Err', err)
        cb(400, {
          status: 'Failed',
          status_code: 400,
          message: "Request Failed",
          data: err
        }, null)

      })


    })

  })


}

module.exports = { checkBookingQueue, checkBookingQueueAdd }

const createQueueDate = (date, time) => {
  var now = new Date(date);
  console.log('now', now, date, time)
  now.setMinutes(now.getMinutes() + time);
  now.setMilliseconds(0);
  return now
}

const minusDate = (start, end) => {
  var now = new Date(start).getTime();
  var later = new Date(end).getTime();
  var result = (now - later) / 60000;
  return (result > 0) ? result : 0
}