const express = require('express');
const { PORT } = require('./config/key');
const http = require('http');
const cors = require('cors');
const { init, client } = require('./config/connectDB');
const router = require('./routes/routes');
const discover = require('../../discovery/disovery')
const defaultConfig = require('./config/default-config')

const app = express()
const httpServer = new http.Server(app);

app.use(
  cors({
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    allowedHeaders: 'Content-Type,auth-token',
    credentials: true, // allow session cookies from browser to pass throught
  }),
);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

init(app).then(() => {

  app.set('client', client);
  app.use(discover(defaultConfig))
  app.use('/', router);

}).catch((err) => {
  if (err instanceof Error) console.error(err.message);
  process.exit(1);
})

httpServer.listen(PORT, () => {
  console.log(`Server is running ${PORT}`)
})