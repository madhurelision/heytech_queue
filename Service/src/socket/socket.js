const { Server, Socket } = require('socket.io');
const jwt = require('jsonwebtoken');
const { CLIENT_URL, SECRET_KEY } = require('../config/key');
const { getShopCurrenQueue, shopQueueAndBooking, userQueueAndBooking, BookingWaitingTime, countNotification, countShopNotification, employeeQueueAndBooking, adminCount, getAllExpertise, getAllActiveService, getShopLocation, getShopOfferQueue, getAllShopLocation, getBookingLocation, shopBookingQueueData, getShopEmployee, getShopServiceBookingTime, createSocketLog, adminShopBookingQueueData, getShopQueueCount, getAllCustomerLocationAdd, getShopTotalCount } = require('../config/function');
const { ObjectId } = require('mongodb');
const watcher = require('../watcher/watcher');

const socketService = (http, app) => {
  const IO_OPTIONS = {
    cors: {
      origin: ['*', 'http://heybarber.herokuapp.com', 'https://heybarber.herokuapp.com', 'http://localhost:3000', 'https://heybarber.in', 'http://heybarber.in'],
      methods: ['GET', 'POST', 'DELETE', 'PATCH', 'UPDATE', 'OPTIONS']
    },
  };

  const io = new Server(http, IO_OPTIONS);
  watcher(app, io)

  io.on('connection', (socket) => {
    console.log(`A user ${socket.id} connection`);
    console.log('loction', socket.handshake.headers)
    var db = app.get('client').db("heytechQueue");
    // console.log('socket decoded data0', socket.decoded_token._id)
    socket.join('global')

    // db.collection('sockets').insertOne({
    //   socket_id: socket.id,
    //   key: socket.id,
    //   status: true,
    //   type: 'global',
    //   data: {},
    //   updatedAt: new Date(),
    //   deleted: false,
    //   createdAt: new Date()
    // }, (err, doc) => {
    //   if (err) {
    //     console.log(err)
    //     return
    //   }
    //   console.log('socket', doc)
    // })

    socket.on('userConnect', async (data) => {
      console.log('main Data Socket', data)
      const token = data.token

      try {

        const verified = jwt.verify(token, SECRET_KEY)
        console.log('User Data verify', verified)
        if (Object.keys(verified).length == 0) {
          return socket.emit('userDisconnect', 'Token is Err')
        }

        db.collection('sockets').findOneAndUpdate({ user_id: ObjectId(verified._id), }, {
          $set: {
            key: socket.id,
            socket_id: socket.id,
            status: true,
            type: data.type,
            data: { ...data },
            updatedAt: new Date()
          },
          $setOnInsert: {
            deleted: false,
            createdAt: new Date()
          }
        }, { upsert: true, new: true, returnNewDocument: true, returnOriginal: false, setDefaultsOnInsert: true }, (err, doc) => {
          if (err) {
            console.log(err)
            return
          }
          console.log('socket connect', doc)
        })

        if (data.type == 'customer') {
          console.log('customer socket connected')

          socket.join('User' + verified._id)

          userQueueAndBooking(app, verified._id, (status, err, data) => {

            if (err) {
              return
            }

            socket.emit('customerCount', data)
            // io.sockets.in('User' + verified._id).emit('customerCount', data)

          })

          countNotification(app, verified._id, (status, err, data) => {
            if (err) {
              return
            }

            socket.emit('notificationCount', data)
          })

        } else if (data.type == 'shop') {
          console.log('shop socket connected')

          socket.join('Shop' + verified._id)

          shopQueueAndBooking(app, verified._id, (status, err, data) => {

            if (err) {
              return
            }
            console.log('sent')
            socket.emit('shopCount', data)
            // io.sockets.in('Shop' + verified._id).emit('shopCount', data)

          })

          countShopNotification(app, verified._id, (status, err, data) => {
            if (err) {
              return
            }

            socket.emit('notificationShopCount', data)
          })

          getShopQueueCount(app, verified._id, (status, err, val) => {
            if (err) {
              return
            }

            socket.emit('shopQueueCount', { data: verified._id, val })
          })

          getShopTotalCount(app, verified._id, (status, err, val) => {
            if (err) {
              return
            }

            socket.emit('shopTotalCount', { data: verified._id, val })
          })

        } else if (data.type == 'employee') {
          console.log('employee socket connected')


          socket.join('Employee' + verified._id)

          employeeQueueAndBooking(app, verified._id, (status, err, data) => {

            if (err) {
              return
            }
            socket.emit('employeeCount', data)
          })

        } else if (data.type == 'admin') {
          console.log('admin socket connected')
          socket.join('Admin' + verified._id)

          adminCount(app, verified._id, (status, err, data) => {

            if (err) {
              return
            }
            socket.emit('adminCount', data)
          })

        }

        // db.collection('customers').findOne({ _id: ObjectId(verified._id) }).then((item) => {

        //   console.log('check', item)

        //   if (item == null || item == undefined) {


        //   }


        // }).catch((err) => {
        //   console.log('User Data Find Socket Error', err)
        // })

      } catch (err) {
        console.log('Socket Emit Function Err', err)
        socket.emit('userDisconnect', 'Token is Err')
      }

    })

    socket.on('shopQueue', (data) => {
      createSocketLog(app, {
        key: 'shopQueue',
        val: 'Shop',
        user_id: ObjectId(data),
        body: { data },
        type: false,
        deleted: false,
        status: true,
        createdAt: new Date(),
        updatedAt: new Date()
      }, (status, err, val) => {
        if (err) {
          return
        }
      })

      getShopCurrenQueue(app, data, (status, err, val) => {
        if (err) {
          return
        }
        socket.emit('shopValue', { data, val })
        io.sockets.in('global').emit('shopValue', { data, val })
        createSocketLog(app, {
          key: 'shopValue',
          val: 'Shop',
          user_id: ObjectId(data),
          body: { val },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })

      })
    })

    socket.on('shopQueueTime', (data) => {

      createSocketLog(app, {
        key: 'shopQueueTime',
        val: 'Shop',
        user_id: ObjectId(data),
        body: { data },
        type: false,
        deleted: false,
        status: true,
        createdAt: new Date(),
        updatedAt: new Date()
      }, (status, err, val) => {
        if (err) {
          return
        }
      })

      BookingWaitingTime(app, data, (status, err, val) => {
        if (err) {
          return
        }
        socket.emit('bookingValue', { data, val })
        io.sockets.in('global').emit('bookingValue', { data, val })
        createSocketLog(app, {
          key: 'bookingValue',
          val: 'Shop',
          user_id: ObjectId(data),
          body: { val },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })
      })
    })

    socket.on('shopServiceTime', (data) => {

      createSocketLog(app, {
        key: 'shopServiceTime',
        val: 'Shop',
        user_id: ObjectId(data),
        body: { data },
        type: false,
        deleted: false,
        status: true,
        createdAt: new Date(),
        updatedAt: new Date()
      }, (status, err, val) => {
        if (err) {
          return
        }
      })

      getShopServiceBookingTime(app, data, (status, err, val) => {
        if (err) {
          return
        }
        socket.emit('timeValue', { data, val })
        io.sockets.in('global').emit('timeValue', { data, val })
        createSocketLog(app, {
          key: 'timeValue',
          val: 'Shop',
          user_id: ObjectId(data),
          body: { val },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })
      })
    })

    socket.on('getExpertise', (data) => {
      getAllExpertise(app, (status, err, val) => {
        if (err) {
          return
        }
        socket.emit('expertise', val)
      })
    })

    socket.on('getActiveService', (data) => {
      getAllActiveService(app, (status, err, val) => {
        if (err) {
          return
        }
        socket.emit('service', val)
      })
    })

    socket.on('location', (loc, id) => {
      console.log('data', loc, 'id', id)

      createSocketLog(app, {
        key: 'location',
        val: 'Shop',
        user_id: ObjectId(id),
        body: { loc, id },
        type: false,
        deleted: false,
        status: true,
        createdAt: new Date(),
        updatedAt: new Date()
      }, (status, err, val) => {
        if (err) {
          return
        }
      })

      getShopLocation(app, loc, id, (status, err, val) => {
        if (err) {
          return
        }
        console.log('location', val)
        socket.emit('userLocation', val)
        createSocketLog(app, {
          key: 'userLocation',
          val: 'Shop',
          user_id: ObjectId(id),
          body: { val },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })
      })
    })

    socket.on('offer', (data) => {
      getShopOfferQueue(app, (status, err, val) => {
        if (err) {
          console.log('Offer Socket Err', err)
          return
        }
        socket.emit('offerValue', val)
      })
    })

    socket.on('shopQueueGet', (data) => {
      getShopQueueCount(app, data, (status, err, val) => {
        if (err) {
          return
        }

        socket.emit('shopQueueCount', { data: data, val })
      })
    })

    socket.on('locationGlobal', (loc) => {
      console.log('data', loc)

      createSocketLog(app, {
        key: 'locationGlobal',
        val: 'Shop',
        user_id: null,
        body: { loc },
        type: false,
        deleted: false,
        status: true,
        createdAt: new Date(),
        updatedAt: new Date()
      }, (status, err, val) => {
        if (err) {
          return
        }
      })

      getAllShopLocation(app, loc, (status, err, val) => {
        if (err) {
          return
        }
        console.log('location', val)
        socket.emit('locationValue', val)
        createSocketLog(app, {
          key: 'locationValue',
          val: 'Shop',
          user_id: null,
          body: { val },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })
      })
    })

    socket.on('bookingLocation', (loc, id) => {
      console.log('data', loc, 'id', id)
      getBookingLocation(app, loc, id, (status, err, val) => {
        if (err) {
          return
        }
        console.log('location', val)
        db.collection('travels').findOneAndUpdate({
          booking_id: ObjectId(id)
        }, {
          $set: {
            ...loc,
            ...val,
            updatedAt: new Date()
          }
        }, { new: true, returnNewDocument: true, returnOriginal: false, setDefaultsOnInsert: true }, (err, doc) => {
          if (err) {
            console.log(err)
            return
          }
          console.log('travel location update', doc)
        })
      })
    })

    socket.on('customerLocation', (loc, id) => {
      console.log('data', loc, 'id', id)
      if (ObjectId.isValid(id)) {
        db.collection('customers').findOneAndUpdate({
          _id: ObjectId(id)
        }, {
          $set: {
            ...loc,
            updatedAt: new Date()
          }
        }, { new: true, returnNewDocument: true, returnOriginal: false }, (err, doc) => {
          if (err) {
            console.log(err)
            return
          }
          console.log('customerlocation update', doc)
        })
      }
    })

    //customer Location
    socket.on('customerLiveLocation', (loc, id) => {
      console.log('data', loc, 'id', id)

      getAllCustomerLocationAdd(app, loc, id, (status, err, val) => {
        if (err) {
          return
        }
        console.log('location', val)
      })
    })

    socket.on('shopBookingQueue', (data) => {
      createSocketLog(app, {
        key: 'shopBookingQueue',
        val: 'Shop',
        user_id: ObjectId(data),
        body: { data },
        type: false,
        deleted: false,
        status: true,
        createdAt: new Date(),
        updatedAt: new Date()
      }, (status, err, val) => {
        if (err) {
          return
        }
      })

      shopBookingQueueData(app, data, (status, err, val) => {
        if (err) {
          return
        }
        socket.emit('shopQueueData', { data, val })
        io.sockets.in('global').emit('shopQueueData', { data, val })
        createSocketLog(app, {
          key: 'shopQueueData',
          val: 'Shop',
          user_id: ObjectId(data),
          body: { val },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })
      })
    })

    socket.on('shopEmployee', (data) => {
      getShopEmployee(app, (status, err, val) => {
        if (err) {
          return
        }
        socket.emit('shopEmployeeData', val)
        io.sockets.in('global').emit('shopEmployeeData', val)
      })
    })

    socket.on('route', (data) => {
      console.log('check', data)

      var ss = data
      ss.user_id = ObjectId.isValid(data.user_id) ? ObjectId(data.user_id) : null

      db.collection('socketstats').insertOne({
        ...ss,
        deleted: false,
        createdAt: new Date(),
        updatedAt: new Date(),
        socket_id: socket.id,
        status: true
      }, (err, doc) => {
        if (err) {
          console.log(err)
          return
        }
        console.log('socket stat create', doc)
      })
    })

    socket.on('logout', (data) => {
      console.log('logout', data)
      db.collection('sockets').findOneAndUpdate({ socket_id: socket.id }, {
        $set: {
          socket_id: '',
          status: false,
          updatedAt: new Date(),
        }
      }, { new: true, returnNewDocument: true, returnOriginal: false }, (err, doc) => {
        if (err) {
          return console.log(err)
        }
      })
    })

    socket.on('adminShopBookingQueue', (data) => {
      createSocketLog(app, {
        key: 'adminShopBookingQueue',
        val: 'Shop',
        user_id: ObjectId(data),
        body: { data },
        type: false,
        deleted: false,
        status: true,
        createdAt: new Date(),
        updatedAt: new Date()
      }, (status, err, val) => {
        if (err) {
          return
        }
      })

      adminShopBookingQueueData(app, data, (status, err, val) => {
        if (err) {
          return
        }
        socket.emit('adminQueueData', { data, val })
        createSocketLog(app, {
          key: 'adminQueueData',
          val: 'Shop',
          user_id: ObjectId(data),
          body: { val },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })
      })
    })

    socket.on('disconnect', function () {
      console.log('Socket Disconnect', socket.id)
      socket.removeAllListeners()
      db.collection('sockets').findOneAndUpdate({ socket_id: socket.id }, {
        $set: {
          socket_id: '',
          status: false,
          updatedAt: new Date(),
        }
      }, { new: true, returnNewDocument: true, returnOriginal: false }, (err, doc) => {
        if (err) {
          return console.log(err)
        }
        console.log('socket disconnect', doc)
        if (!doc || doc == null || doc == undefined) {

        } else {
          // socket.leave('Shop' + doc.value.user_id)
          // socket.leave('User' + doc.value.user_id)
          // socket.leave('Employee' + doc.value.user_id)
          // socket.leave('Admin' + doc.value.user_id)
        }
      })
    })

  });
};

module.exports = socketService;
