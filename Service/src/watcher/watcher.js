const { ObjectId } = require("mongodb");
const { getShopCurrenQueue, shopQueueAndBooking, userQueueAndBooking, BookingWaitingTime, createNotification, countNotification, BookingMessage, countShopNotification, employeeQueueAndBooking, adminCount, bookingMsg, bookingOTPMsg, confirmationMsg, getShopOfferQueue, shopBookingQueueData, getShopEmployee, getShopServiceBookingTime, createSocketLog, getShopQueueCount, getShopTotalCount } = require("../config/function");
const { SERVER_URL, ServiceSmsMail, MAIL_SECRET_KEY, ServiceTemplateId, SHORT_URL_KEY, PAYMENT_URL_KEY } = require('../config/key')
const QRCode = require('qrcode')
const axios = require('axios')
const jwt = require('jsonwebtoken')
const config = require('config')

const watcher = (app, io) => {
  console.log('Watcher Start')
  var db = app.get('client').db("heytechQueue");

  db.collection('bookings').watch([{
    $match: {
      $and: [
        { "updateDescription.updatedFields.otp_verify": true },
        { operationType: "update" }
      ]
    }
  }]).on('change', async (data) => {



    db.collection('bookings').aggregate([
      {
        '$match': {
          '_id': ObjectId(data.documentKey._id),
        }
      }, {
        '$lookup': {
          'from': 'services',
          'localField': 'service_id',
          'foreignField': '_id',
          'as': 'service_id'
        }
      }, {
        '$unwind': {
          'path': '$service_id'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'user_id',
          'foreignField': '_id',
          'as': 'user_id'
        }
      }, {
        '$unwind': {
          'path': '$user_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
      {
        '$lookup': {
          'from': 'bookingpayments',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'payment'
        }
      },
      {
        '$lookup': {
          'from': 'travels',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'travel'
        }
      },
      {
        '$addFields': {
          'distance': {
            '$arrayElemAt': [
              '$travel.distance', 0
            ]
          },
          'distanceVal': {
            '$arrayElemAt': [
              '$travel.distanceVal', 0
            ]
          },
          'duration': {
            '$arrayElemAt': [
              '$travel.duration', 0
            ]
          },
          'durationVal': {
            '$arrayElemAt': [
              '$travel.durationVal', 0
            ]
          },
          'lattitude': {
            '$arrayElemAt': [
              '$travel.lattitude', 0
            ]
          },
          'longitude': {
            '$arrayElemAt': [
              '$travel.longitude', 0
            ]
          },
        }
      },
      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_id._id'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$user_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$shop_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'userreview'
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'user_count': {
            '$reduce': {
              'input': '$userreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalUser': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$userreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$user_count', {
                      '$size': '$userreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      },

      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      },
      {
        '$lookup': {
          'from': 'bookings',
          'let': {
            'id': '$_id',
            'main': '$shop_id.user_information',
            'approximate_date': '$approximate_date',
            'seat': '$seat'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$seat', '$$seat'
                      ]
                    }, {
                      '$not': [
                        {
                          '$eq': [
                            '$_id', '$$id'
                          ]
                        }
                      ]
                    }, {
                      '$eq': [
                        '$queue', true
                      ]
                    }, {
                      '$eq': [
                        '$test', false
                      ]
                    }, {
                      '$lte': [
                        '$approximate_date', '$$approximate_date'
                      ]
                    }, {
                      '$in': [
                        '$status', [
                          'waiting', 'accepted'
                        ]
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'sbook'
        }
      }, {
        '$addFields': {
          'count': {
            '$reduce': {
              'input': '$sbook',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.size'
                ]
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'created_by',
          'foreignField': 'user_information',
          'as': 'shop'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'customer'
        }
      }, {
        '$lookup': {
          'from': 'employees',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'employee'
        }
      },
      {
        '$addFields': {
          'created_name': {
            '$arrayElemAt': [
              {
                '$cond': {
                  'if': {
                    '$eq': [
                      {
                        '$size': '$shop'
                      }, 1
                    ]
                  },
                  'then': '$shop.shop_name',
                  'else': {
                    '$cond': {
                      'if': {
                        '$eq': [
                          {
                            '$size': '$customer'
                          }, 1
                        ]
                      },
                      'then': '$customer.first_name',
                      'else': {
                        '$cond': {
                          'if': {
                            '$eq': [
                              {
                                '$size': '$employee'
                              }, 1
                            ]
                          },
                          'then': '$employee.name',
                          'else': [
                            ''
                          ]
                        }
                      }
                    }
                  }
                }
              }, 0
            ]
          }
        }
      }
    ]).toArray(async (err, item) => {

      if (err) {
        console.log('Watcher Otp Verify Err', err)
        return
      }



      if (item.length > 0) {


        io.sockets.in('User' + item[0].user_id._id).emit('customerBooking', item[0])
        io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopBooking', item[0])

        createSocketLog(app, {
          key: 'customerBooking shopBooking offerValue timeValue shopQueueData shopEmployeeData customerCount shopCount shopValue bookingValue',
          val: 'Booking',
          user_id: ObjectId(item[0]._id),
          data: { ...item[0] },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })

        getShopOfferQueue(app, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('offerValue', val)
          io.sockets.in('User' + item[0].user_id._id).emit('offerValue', val)
          io.sockets.in('global').emit('offerValue', val)
        })

        getShopQueueCount(app, item[0].shop_id.user_information, (status, err, val) => {
          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopQueueCount', { data: item[0].shop_id.user_information, val })
          io.sockets.in('global').emit('shopQueueCount', { data: item[0].shop_id.user_information, val })
        })

        getShopServiceBookingTime(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('timeValue', { data: item[0].shop_id._id, val })
        })

        shopBookingQueueData(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('shopQueueData', { data: item[0].shop_id._id, val })
        })


        getShopEmployee(app, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('shopEmployeeData', val)
        })

        userQueueAndBooking(app, item[0].user_id._id, (status, err, val) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item[0].user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(app, item[0].shop_id.user_information, (status, err, val) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopCount', val)
        })

        getShopTotalCount(app, item[0].shop_id.user_information, (status, err, val) => {
          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopTotalCount', { data: item[0].shop_id.user_information, val })
        })

        getShopCurrenQueue(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopValue', { data: item[0].shop_id._id, val })
          io.sockets.in('User' + item[0].user_id._id).emit('shopValue', { data: item[0].shop_id._id, val })
          io.sockets.in('global').emit('shopValue', { data: item[0].shop_id._id, val })
        })

        BookingWaitingTime(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('bookingValue', { data: item[0].shop_id._id, val })
          io.sockets.in('User' + item[0].user_id._id).emit('bookingValue', { data: item[0].shop_id._id, val })
          io.sockets.in('global').emit('bookingValue', { data: item[0].shop_id._id, val })
        })

        createNotification(app, item[0], (status, err, val) => {
          if (err) {
            console.log('Notification create err', err)
            return
          }

          console.log('notification created', val)
        })

        if (item[0].assign != null && item[0].assign != undefined) {
          io.sockets.in('Employee' + item[0].assign).emit('employeeAssignBooking', item[0])

          employeeQueueAndBooking(app, item[0].assign, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].assign).emit('employeeCount', data)
          })

        }

        if (item[0].create_type == "employee") {
          io.sockets.in('Employee' + item[0].created_by).emit('employeeBooking', item[0])

          employeeQueueAndBooking(app, item[0].created_by, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].created_by).emit('employeeCount', data)
          })
        }

        if (item[0].test == false && item[0].sms == false) {
          try {
            const msgT = await bookingMsg(item[0])
            const smsToken = jwt.sign({ date: new Date(), message: msgT, id: ServiceTemplateId.book, type: 'booking', bookingid: item[0].bookingid, user_id: [item[0]._id] }, MAIL_SECRET_KEY)
            const msg = await BookingMessage(item[0])
            const mainData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
              number: item[0].mobile + ',' + item[0].shop_id.mobile,
              message: msg
            }, {
              headers: {
                'auth-token': smsToken
              }
            })

            console.log('Booking SMs Sent', mainData)

          } catch (err) {
            console.log('Watcher Booking Sms Send Err', err)
          }
        }


      }

    })


  })

  db.collection('bookings').watch([{
    $match: {
      $and: [
        { "updateDescription.updatedFields.status": 'accepted' },
        { operationType: "update" }
      ]
    }
  }]).on('change', async (data) => {



    db.collection('bookings').aggregate([
      {
        '$match': {
          '_id': ObjectId(data.documentKey._id),
        }
      }, {
        '$lookup': {
          'from': 'services',
          'localField': 'service_id',
          'foreignField': '_id',
          'as': 'service_id'
        }
      }, {
        '$unwind': {
          'path': '$service_id'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'user_id',
          'foreignField': '_id',
          'as': 'user_id'
        }
      }, {
        '$unwind': {
          'path': '$user_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
      {
        '$lookup': {
          'from': 'bookingpayments',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'payment'
        }
      },
      {
        '$lookup': {
          'from': 'travels',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'travel'
        }
      },
      {
        '$addFields': {
          'distance': {
            '$arrayElemAt': [
              '$travel.distance', 0
            ]
          },
          'distanceVal': {
            '$arrayElemAt': [
              '$travel.distanceVal', 0
            ]
          },
          'duration': {
            '$arrayElemAt': [
              '$travel.duration', 0
            ]
          },
          'durationVal': {
            '$arrayElemAt': [
              '$travel.durationVal', 0
            ]
          },
          'lattitude': {
            '$arrayElemAt': [
              '$travel.lattitude', 0
            ]
          },
          'longitude': {
            '$arrayElemAt': [
              '$travel.longitude', 0
            ]
          },
        }
      },
      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_id._id'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$user_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$shop_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'userreview'
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'user_count': {
            '$reduce': {
              'input': '$userreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalUser': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$userreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$user_count', {
                      '$size': '$userreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      },

      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      },
      {
        '$lookup': {
          'from': 'bookings',
          'let': {
            'id': '$_id',
            'main': '$shop_id.user_information',
            'approximate_date': '$approximate_date',
            'seat': '$seat'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$seat', '$$seat'
                      ]
                    }, {
                      '$not': [
                        {
                          '$eq': [
                            '$_id', '$$id'
                          ]
                        }
                      ]
                    }, {
                      '$eq': [
                        '$queue', true
                      ]
                    }, {
                      '$eq': [
                        '$test', false
                      ]
                    }, {
                      '$lte': [
                        '$approximate_date', '$$approximate_date'
                      ]
                    }, {
                      '$in': [
                        '$status', [
                          'waiting', 'accepted'
                        ]
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'sbook'
        }
      }, {
        '$addFields': {
          'count': {
            '$reduce': {
              'input': '$sbook',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.size'
                ]
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'created_by',
          'foreignField': 'user_information',
          'as': 'shop'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'customer'
        }
      }, {
        '$lookup': {
          'from': 'employees',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'employee'
        }
      },
      {
        '$lookup': {
          'from': 'shorts',
          'localField': '_id',
          'foreignField': 'product_id',
          'as': 'short'
        }
      },
      {
        '$addFields': {
          'created_name': {
            '$arrayElemAt': [
              {
                '$cond': {
                  'if': {
                    '$eq': [
                      {
                        '$size': '$shop'
                      }, 1
                    ]
                  },
                  'then': '$shop.shop_name',
                  'else': {
                    '$cond': {
                      'if': {
                        '$eq': [
                          {
                            '$size': '$customer'
                          }, 1
                        ]
                      },
                      'then': '$customer.first_name',
                      'else': {
                        '$cond': {
                          'if': {
                            '$eq': [
                              {
                                '$size': '$employee'
                              }, 1
                            ]
                          },
                          'then': '$employee.name',
                          'else': [
                            ''
                          ]
                        }
                      }
                    }
                  }
                }
              }, 0
            ]
          }
        }
      }
    ]).toArray(async (err, item) => {

      if (err) {
        console.log('Watcher Booking Status Accepted Err', err)
        return
      }



      if (item.length > 0) {


        io.sockets.in('User' + item[0].user_id._id).emit('customerBooking', item[0])
        io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopBooking', item[0])

        createSocketLog(app, {
          key: 'customerBooking shopBooking offerValue timeValue shopQueueData shopEmployeeData customerCount shopCount shopValue bookingValue',
          val: 'Booking',
          user_id: ObjectId(item[0]._id),
          data: { ...item[0] },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })

        getShopOfferQueue(app, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('offerValue', val)
          io.sockets.in('User' + item[0].user_id._id).emit('offerValue', val)
          io.sockets.in('global').emit('offerValue', val)
        })

        getShopQueueCount(app, item[0].shop_id.user_information, (status, err, val) => {
          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopQueueCount', { data: item[0].shop_id.user_information, val })
          io.sockets.in('global').emit('shopQueueCount', { data: item[0].shop_id.user_information, val })
        })

        getShopTotalCount(app, item[0].shop_id.user_information, (status, err, val) => {
          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopTotalCount', { data: item[0].shop_id.user_information, val })
        })

        shopBookingQueueData(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('shopQueueData', { data: item[0].shop_id._id, val })
        })

        getShopServiceBookingTime(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('timeValue', { data: item[0].shop_id._id, val })
        })

        getShopEmployee(app, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('shopEmployeeData', val)
        })

        userQueueAndBooking(app, item[0].user_id._id, (status, err, val) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item[0].user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(app, item[0].shop_id.user_information, (status, err, val) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopCount', val)
        })

        createNotification(app, item[0], (status, err, val) => {
          if (err) {
            console.log('Notification create err', err)
            return
          }

          console.log('notification created', val)
        })

        if (item[0].assign != null && item[0].assign != undefined) {
          io.sockets.in('Employee' + item[0].assign).emit('employeeAssignBooking', item[0])

          employeeQueueAndBooking(app, item[0].assign, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].assign).emit('employeeCount', data)
          })

        }

        if (item[0].create_type == "employee") {
          io.sockets.in('Employee' + item[0].created_by).emit('employeeBooking', item[0])

          employeeQueueAndBooking(app, item[0].created_by, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].created_by).emit('employeeCount', data)
          })
        }

      }


      if (item.length > 0 && (item[0].test == false && item[0].user_email !== null && item[0].user_email !== undefined && item[0].user_email == "" && item[0].status == 'accepted' && item[0].sms == false)) {

        try {
          const msgT = config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short[0].short_id : SHORT_URL_KEY + item[0].short[0].short_id
          const mainMsg = confirmationMsg({ ...item[0], link: msgT, countIcon: item[0].count == 0 ? 'no' : item[0].count })
          const smsToken = jwt.sign({ date: new Date(), message: mainMsg, id: ServiceTemplateId.confirm, type: 'booking', bookingid: item[0].bookingid, user_id: [item[0]._id] }, MAIL_SECRET_KEY)
          const emailData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'sendEmail' : ServiceSmsMail + 'sendEmail', {
            email: item[0].user_email,
            link: config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short_id : SHORT_URL_KEY + item[0].short_id
          }, {
            headers: {
              'auth-token': smsToken
            }
          })

          console.log('Email Sent', emailData)

        } catch (err) {
          console.log('Watcher Email Send Err', err)
        }
      }

      if (item.length > 0 && (item[0].test == false && item[0].mobile !== null && item[0].mobile !== undefined && item[0].mobile == "" && item[0].status == 'accepted' && item[0].sms == false)) {

        try {
          const msgTT = config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short[0].short_id : SHORT_URL_KEY + item[0].short[0].short_id
          const mainMsg = confirmationMsg({ ...item[0], link: msgTT, countIcon: item[0].count == 0 ? 'no' : item[0].count })
          const smssToken = jwt.sign({ date: new Date(), message: mainMsg, id: ServiceTemplateId.confirm, type: 'booking', bookingid: item[0].bookingid, user_id: [item[0]._id] }, MAIL_SECRET_KEY)
          const smsData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
            number: item[0].mobile,
            message: config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short_id : SHORT_URL_KEY + item[0].short_id
          }, {
            headers: {
              'auth-token': smssToken
            }
          })

          console.log('Sms Sent', smsData)

        } catch (err) {
          console.log('Watcher Sms Send Err', err)
        }

      }

    })


  })

  db.collection('bookings').watch([{
    $match: {
      $and: [
        { "updateDescription.updatedFields.status": 'completed' },
        { operationType: "update" }
      ]
    }
  }]).on('change', async (data) => {



    db.collection('bookings').aggregate([
      {
        '$match': {
          '_id': ObjectId(data.documentKey._id),
        }
      }, {
        '$lookup': {
          'from': 'services',
          'localField': 'service_id',
          'foreignField': '_id',
          'as': 'service_id'
        }
      }, {
        '$unwind': {
          'path': '$service_id'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'user_id',
          'foreignField': '_id',
          'as': 'user_id'
        }
      }, {
        '$unwind': {
          'path': '$user_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_id._id'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$user_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$shop_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'userreview'
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'user_count': {
            '$reduce': {
              'input': '$userreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalUser': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$userreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$user_count', {
                      '$size': '$userreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }
    ]).toArray((err, item) => {

      if (err) {
        console.log('Watcher Booking Status Completed Err', err)
        return
      }



      if (item.length > 0) {

        io.sockets.in('User' + item[0].user_id._id).emit('customerBookingRemove', item[0])
        io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopBookingRemove', item[0])

        createSocketLog(app, {
          key: 'customerBookingRemove shopBookingRemove offerValue timeValue shopQueueData shopEmployeeData customerCount shopCount shopValue bookingValue',
          val: 'Booking',
          user_id: ObjectId(item[0]._id),
          data: { ...item[0] },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })

        getShopOfferQueue(app, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('offerValue', val)
          io.sockets.in('User' + item[0].user_id._id).emit('offerValue', val)
          io.sockets.in('global').emit('offerValue', val)
        })

        getShopQueueCount(app, item[0].shop_id.user_information, (status, err, val) => {
          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopQueueCount', { data: item[0].shop_id.user_information, val })
          io.sockets.in('global').emit('shopQueueCount', { data: item[0].shop_id.user_information, val })
        })

        getShopServiceBookingTime(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('timeValue', { data: item[0].shop_id._id, val })
        })

        shopBookingQueueData(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('shopQueueData', { data: item[0].shop_id._id, val })
        })


        getShopEmployee(app, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('shopEmployeeData', val)
        })

        userQueueAndBooking(app, item[0].user_id._id, (status, err, val) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item[0].user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(app, item[0].shop_id.user_information, (status, err, val) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopCount', val)
        })

        getShopTotalCount(app, item[0].shop_id.user_information, (status, err, val) => {
          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopTotalCount', { data: item[0].shop_id.user_information, val })
        })

        getShopCurrenQueue(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopValue', { data: item[0].shop_id._id, val })
          io.sockets.in('User' + item[0].user_id._id).emit('shopValue', { data: item[0].shop_id._id, val })
          io.sockets.in('global').emit('shopValue', { data: item[0].shop_id._id, val })
        })

        BookingWaitingTime(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('bookingValue', { data: item[0].shop_id._id, val })
          io.sockets.in('User' + item[0].user_id._id).emit('bookingValue', { data: item[0].shop_id._id, val })
          io.sockets.in('global').emit('bookingValue', { data: item[0].shop_id._id, val })
        })

        createNotification(app, item[0], (status, err, val) => {
          if (err) {
            console.log('Notification create err', err)
            return
          }

          console.log('notification created', val)
        })

        if (item[0].assign != null && item[0].assign != undefined) {
          io.sockets.in('Employee' + item[0].assign).emit('employeeAssignBookingRemove', item[0])

          employeeQueueAndBooking(app, item[0].assign, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].assign).emit('employeeCount', data)
          })

        }

        if (item[0].create_type == "employee") {
          io.sockets.in('Employee' + item[0].created_by).emit('employeeBookingRemove', item[0])

          employeeQueueAndBooking(app, item[0].created_by, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].created_by).emit('employeeCount', data)
          })
        }

      }

    })


  })


  db.collection('bookings').watch([{
    $match: {
      $and: [
        { "updateDescription.updatedFields.status": 'expired' },
        { operationType: "update" }
      ]
    }
  }]).on('change', async (data) => {



    db.collection('bookings').aggregate([
      {
        '$match': {
          '_id': ObjectId(data.documentKey._id),
        }
      }, {
        '$lookup': {
          'from': 'services',
          'localField': 'service_id',
          'foreignField': '_id',
          'as': 'service_id'
        }
      }, {
        '$unwind': {
          'path': '$service_id'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'user_id',
          'foreignField': '_id',
          'as': 'user_id'
        }
      }, {
        '$unwind': {
          'path': '$user_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
    ]).toArray((err, item) => {

      if (err) {
        console.log('Watcher Booking Expired Err', err)
        return
      }



      if (item.length > 0) {

        io.sockets.in('User' + item[0].user_id._id).emit('customerBookingRemove', item[0])
        io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopBookingRemove', item[0])

        createSocketLog(app, {
          key: 'customerBookingRemove shopBookingRemove offerValue timeValue shopQueueData shopEmployeeData customerCount shopCount shopValue bookingValue',
          val: 'Booking',
          user_id: ObjectId(item[0]._id),
          data: { ...item[0] },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })

        getShopOfferQueue(app, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('offerValue', val)
          io.sockets.in('User' + item[0].user_id._id).emit('offerValue', val)
          io.sockets.in('global').emit('offerValue', val)
        })

        getShopQueueCount(app, item[0].shop_id.user_information, (status, err, val) => {
          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopQueueCount', { data: item[0].shop_id.user_information, val })
          io.sockets.in('global').emit('shopQueueCount', { data: item[0].shop_id.user_information, val })
        })

        getShopTotalCount(app, item[0].shop_id.user_information, (status, err, val) => {
          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopTotalCount', { data: item[0].shop_id.user_information, val })
        })

        getShopServiceBookingTime(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('timeValue', { data: item[0].shop_id._id, val })
        })

        shopBookingQueueData(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('shopQueueData', { data: item[0].shop_id._id, val })
        })


        getShopEmployee(app, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('shopEmployeeData', val)
        })

        userQueueAndBooking(app, item[0].user_id._id, (status, err, val) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item[0].user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(app, item[0].shop_id.user_information, (status, err, val) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopCount', val)
        })

        getShopCurrenQueue(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopValue', { data: item[0].shop_id._id, val })
          io.sockets.in('User' + item[0].user_id._id).emit('shopValue', { data: item[0].shop_id._id, val })
          io.sockets.in('global').emit('shopValue', { data: item[0].shop_id._id, val })
        })

        BookingWaitingTime(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('bookingValue', { data: item[0].shop_id._id, val })
          io.sockets.in('User' + item[0].user_id._id).emit('bookingValue', { data: item[0].shop_id._id, val })
          io.sockets.in('global').emit('bookingValue', { data: item[0].shop_id._id, val })
        })

        createNotification(app, item[0], (status, err, val) => {
          if (err) {
            console.log('Notification create err', err)
            return
          }

          console.log('notification created', val)
        })

        if (item[0].assign != null && item[0].assign != undefined) {
          io.sockets.in('Employee' + item[0].assign).emit('employeeAssignBookingRemove', item[0])

          employeeQueueAndBooking(app, item[0].assign, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].assign).emit('employeeCount', data)
          })

        }

        if (item[0].create_type == "employee") {
          io.sockets.in('Employee' + item[0].created_by).emit('employeeBookingRemove', item[0])

          employeeQueueAndBooking(app, item[0].created_by, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].created_by).emit('employeeCount', data)
          })
        }

      }

    })


  })

  db.collection('bookings').watch([{
    $match: {
      $and: [
        { "updateDescription.updatedFields.waiting_number": { $exists: true } },
        { operationType: "update" }
      ]
    }
  }]).on('change', async (data) => {



    db.collection('bookings').aggregate([
      {
        '$match': {
          '_id': ObjectId(data.documentKey._id),
        }
      }, {
        '$lookup': {
          'from': 'services',
          'localField': 'service_id',
          'foreignField': '_id',
          'as': 'service_id'
        }
      }, {
        '$unwind': {
          'path': '$service_id'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'user_id',
          'foreignField': '_id',
          'as': 'user_id'
        }
      }, {
        '$unwind': {
          'path': '$user_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
      {
        '$lookup': {
          'from': 'bookingpayments',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'payment'
        }
      },
      {
        '$lookup': {
          'from': 'travels',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'travel'
        }
      },
      {
        '$addFields': {
          'distance': {
            '$arrayElemAt': [
              '$travel.distance', 0
            ]
          },
          'distanceVal': {
            '$arrayElemAt': [
              '$travel.distanceVal', 0
            ]
          },
          'duration': {
            '$arrayElemAt': [
              '$travel.duration', 0
            ]
          },
          'durationVal': {
            '$arrayElemAt': [
              '$travel.durationVal', 0
            ]
          },
          'lattitude': {
            '$arrayElemAt': [
              '$travel.lattitude', 0
            ]
          },
          'longitude': {
            '$arrayElemAt': [
              '$travel.longitude', 0
            ]
          },
        }
      },
      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_id._id'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$user_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$shop_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'userreview'
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'user_count': {
            '$reduce': {
              'input': '$userreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalUser': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$userreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$user_count', {
                      '$size': '$userreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      },

      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      },
      {
        '$lookup': {
          'from': 'bookings',
          'let': {
            'id': '$_id',
            'main': '$shop_id.user_information',
            'approximate_date': '$approximate_date',
            'seat': '$seat'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$seat', '$$seat'
                      ]
                    }, {
                      '$not': [
                        {
                          '$eq': [
                            '$_id', '$$id'
                          ]
                        }
                      ]
                    }, {
                      '$eq': [
                        '$queue', true
                      ]
                    }, {
                      '$eq': [
                        '$test', false
                      ]
                    }, {
                      '$lte': [
                        '$approximate_date', '$$approximate_date'
                      ]
                    }, {
                      '$in': [
                        '$status', [
                          'waiting', 'accepted'
                        ]
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'sbook'
        }
      }, {
        '$addFields': {
          'count': {
            '$reduce': {
              'input': '$sbook',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.size'
                ]
              }
            }
          }
        }
      },
      {
        '$lookup': {
          'from': 'shops',
          'localField': 'created_by',
          'foreignField': 'user_information',
          'as': 'shop'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'customer'
        }
      }, {
        '$lookup': {
          'from': 'employees',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'employee'
        }
      },
      {
        '$lookup': {
          'from': 'shorts',
          'localField': '_id',
          'foreignField': 'product_id',
          'as': 'short'
        }
      },
      {
        '$addFields': {
          'created_name': {
            '$arrayElemAt': [
              {
                '$cond': {
                  'if': {
                    '$eq': [
                      {
                        '$size': '$shop'
                      }, 1
                    ]
                  },
                  'then': '$shop.shop_name',
                  'else': {
                    '$cond': {
                      'if': {
                        '$eq': [
                          {
                            '$size': '$customer'
                          }, 1
                        ]
                      },
                      'then': '$customer.first_name',
                      'else': {
                        '$cond': {
                          'if': {
                            '$eq': [
                              {
                                '$size': '$employee'
                              }, 1
                            ]
                          },
                          'then': '$employee.name',
                          'else': [
                            ''
                          ]
                        }
                      }
                    }
                  }
                }
              }, 0
            ]
          }
        }
      }
    ]).toArray(async (err, item) => {

      if (err) {
        console.log('Watcher Booking Waiting Time Updated Err', err)
        return
      }



      if (item.length > 0) {


        io.sockets.in('User' + item[0].user_id._id).emit('customerBooking', item[0])
        io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopBooking', item[0])

        createSocketLog(app, {
          key: 'customerBooking shopBooking offerValue timeValue shopQueueData shopEmployeeData customerCount shopCount shopValue bookingValue',
          val: 'Booking',
          user_id: ObjectId(item[0]._id),
          data: { ...item[0] },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })

        userQueueAndBooking(app, item[0].user_id._id, (status, err, val) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item[0].user_id._id).emit('customerCount', val)
        })

        getShopServiceBookingTime(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('timeValue', { data: item[0].shop_id._id, val })
        })

        shopQueueAndBooking(app, item[0].shop_id.user_information, (status, err, val) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopCount', val)
        })

        if (item[0].assign != null && item[0].assign != undefined) {
          io.sockets.in('Employee' + item[0].assign).emit('employeeAssignBooking', item[0])

          employeeQueueAndBooking(app, item[0].assign, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].assign).emit('employeeCount', data)
          })

        }

        if (item[0].create_type == "employee") {
          io.sockets.in('Employee' + item[0].created_by).emit('employeeBooking', item[0])

          employeeQueueAndBooking(app, item[0].created_by, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].created_by).emit('employeeCount', data)
          })
        }



      }

      if (item.length > 0 && (item[0].test == false && item[0].user_email !== null && item[0].user_email !== undefined && item[0].user_email == "" && item[0].status == 'accepted' && item[0].sms == false)) {

        try {
          const msgT = config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short[0].short_id : SHORT_URL_KEY + item[0].short[0].short_id
          const mainMsg = confirmationMsg({ ...item[0], link: msgT, countIcon: item[0].count == 0 ? 'no' : item[0].count })
          const smsToken = jwt.sign({ date: new Date(), message: mainMsg, id: ServiceTemplateId.confirm, type: 'booking', bookingid: item[0].bookingid, user_id: [item[0]._id] }, MAIL_SECRET_KEY)
          const emailData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'sendEmail' : ServiceSmsMail + 'sendEmail', {
            email: item[0].user_email,
            link: config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short_id : SHORT_URL_KEY + item[0].short_id
          }, {
            headers: {
              'auth-token': smsToken
            }
          })

          console.log('Email Sent', emailData)

        } catch (err) {
          console.log('Watcher Email Send Err', err)
        }
      }

      if (item.length > 0 && (item[0].test == false && item[0].mobile !== null && item[0].mobile !== undefined && item[0].mobile == "" && item[0].status == 'accepted' && item[0].sms == false)) {

        try {
          const msgTT = config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short[0].short_id : SHORT_URL_KEY + item[0].short[0].short_id
          const mainMsg = confirmationMsg({ ...item[0], link: msgTT, countIcon: item[0].count == 0 ? 'no' : item[0].count })
          const smssToken = jwt.sign({ date: new Date(), message: mainMsg, id: ServiceTemplateId.confirm, type: 'booking', bookingid: item[0].bookingid, user_id: [item[0]._id] }, MAIL_SECRET_KEY)
          const smsData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
            number: item[0].mobile,
            message: config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short_id : SHORT_URL_KEY + item[0].short_id
          }, {
            headers: {
              'auth-token': smssToken
            }
          })

          console.log('Sms Sent', smsData)

        } catch (err) {
          console.log('Watcher Sms Send Err', err)
        }

      }

    })


  })


  db.collection('bookings').watch([{
    $match: {
      $and: [
        { "updateDescription.updatedFields.status": 'cancelled' },
        { operationType: "update" }
      ]
    }
  }]).on('change', async (data) => {



    db.collection('bookings').aggregate([
      {
        '$match': {
          '_id': ObjectId(data.documentKey._id),
        }
      }, {
        '$lookup': {
          'from': 'services',
          'localField': 'service_id',
          'foreignField': '_id',
          'as': 'service_id'
        }
      }, {
        '$unwind': {
          'path': '$service_id'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'user_id',
          'foreignField': '_id',
          'as': 'user_id'
        }
      }, {
        '$unwind': {
          'path': '$user_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
    ]).toArray((err, item) => {

      if (err) {
        console.log('Watcher Booking Cancelled Err', err)
        return
      }



      if (item.length > 0) {

        io.sockets.in('User' + item[0].user_id._id).emit('customerBookingRemove', item[0])
        io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopBookingRemove', item[0])

        createSocketLog(app, {
          key: 'customerBookingRemove shopBookingRemove offerValue timeValue shopQueueData shopEmployeeData customerCount shopCount shopValue bookingValue',
          val: 'Booking',
          user_id: ObjectId(item[0]._id),
          data: { ...item[0] },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })

        getShopOfferQueue(app, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('offerValue', val)
          io.sockets.in('User' + item[0].user_id._id).emit('offerValue', val)
          io.sockets.in('global').emit('offerValue', val)
        })

        getShopQueueCount(app, item[0].shop_id.user_information, (status, err, val) => {
          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopQueueCount', { data: item[0].shop_id.user_information, val })
          io.sockets.in('global').emit('shopQueueCount', { data: item[0].shop_id.user_information, val })
        })

        getShopServiceBookingTime(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('timeValue', { data: item[0].shop_id._id, val })
        })

        getShopTotalCount(app, item[0].shop_id.user_information, (status, err, val) => {
          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopTotalCount', { data: item[0].shop_id.user_information, val })
        })

        shopBookingQueueData(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('shopQueueData', { data: item[0].shop_id._id, val })
        })


        getShopEmployee(app, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('shopEmployeeData', val)
        })

        userQueueAndBooking(app, item[0].user_id._id, (status, err, val) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item[0].user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(app, item[0].shop_id.user_information, (status, err, val) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopCount', val)
        })

        getShopCurrenQueue(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopValue', { data: item[0].shop_id._id, val })
          io.sockets.in('User' + item[0].user_id._id).emit('shopValue', { data: item[0].shop_id._id, val })
          io.sockets.in('global').emit('shopValue', { data: item[0].shop_id._id, val })
        })

        BookingWaitingTime(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('bookingValue', { data: item[0].shop_id._id, val })
          io.sockets.in('User' + item[0].user_id._id).emit('bookingValue', { data: item[0].shop_id._id, val })
          io.sockets.in('global').emit('bookingValue', { data: item[0].shop_id._id, val })
        })

        createNotification(app, item[0], (status, err, val) => {
          if (err) {
            console.log('Notification create err', err)
            return
          }

          console.log('notification created', val)
        })

        if (item[0].assign != null && item[0].assign != undefined) {
          io.sockets.in('Employee' + item[0].assign).emit('employeeAssignBookingRemove', item[0])

          employeeQueueAndBooking(app, item[0].assign, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].assign).emit('employeeCount', data)
          })

        }

        if (item[0].create_type == "employee") {
          io.sockets.in('Employee' + item[0].created_by).emit('employeeBookingRemove', item[0])

          employeeQueueAndBooking(app, item[0].created_by, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].created_by).emit('employeeCount', data)
          })
        }

      }

    })


  })

  db.collection('bookings').watch([{
    $match: {
      $and: [
        { "updateDescription.updatedFields.status": 'waiting' },
        { operationType: "update" }
      ]
    }
  }]).on('change', async (data) => {



    db.collection('bookings').aggregate([
      {
        '$match': {
          '_id': ObjectId(data.documentKey._id),
        }
      }, {
        '$lookup': {
          'from': 'services',
          'localField': 'service_id',
          'foreignField': '_id',
          'as': 'service_id'
        }
      }, {
        '$unwind': {
          'path': '$service_id'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'user_id',
          'foreignField': '_id',
          'as': 'user_id'
        }
      }, {
        '$unwind': {
          'path': '$user_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
      {
        '$lookup': {
          'from': 'bookingpayments',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'payment'
        }
      },
      {
        '$lookup': {
          'from': 'travels',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'travel'
        }
      },
      {
        '$addFields': {
          'distance': {
            '$arrayElemAt': [
              '$travel.distance', 0
            ]
          },
          'distanceVal': {
            '$arrayElemAt': [
              '$travel.distanceVal', 0
            ]
          },
          'duration': {
            '$arrayElemAt': [
              '$travel.duration', 0
            ]
          },
          'durationVal': {
            '$arrayElemAt': [
              '$travel.durationVal', 0
            ]
          },
          'lattitude': {
            '$arrayElemAt': [
              '$travel.lattitude', 0
            ]
          },
          'longitude': {
            '$arrayElemAt': [
              '$travel.longitude', 0
            ]
          },
        }
      },
      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_id._id'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$user_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$shop_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'userreview'
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'user_count': {
            '$reduce': {
              'input': '$userreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalUser': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$userreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$user_count', {
                      '$size': '$userreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      },

      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      },
      {
        '$lookup': {
          'from': 'bookings',
          'let': {
            'id': '$_id',
            'main': '$shop_id.user_information',
            'approximate_date': '$approximate_date',
            'seat': '$seat'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$seat', '$$seat'
                      ]
                    }, {
                      '$not': [
                        {
                          '$eq': [
                            '$_id', '$$id'
                          ]
                        }
                      ]
                    }, {
                      '$eq': [
                        '$queue', true
                      ]
                    }, {
                      '$eq': [
                        '$test', false
                      ]
                    }, {
                      '$lte': [
                        '$approximate_date', '$$approximate_date'
                      ]
                    }, {
                      '$in': [
                        '$status', [
                          'waiting', 'accepted'
                        ]
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'sbook'
        }
      }, {
        '$addFields': {
          'count': {
            '$reduce': {
              'input': '$sbook',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.size'
                ]
              }
            }
          }
        }
      },
      {
        '$lookup': {
          'from': 'shops',
          'localField': 'created_by',
          'foreignField': 'user_information',
          'as': 'shop'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'customer'
        }
      }, {
        '$lookup': {
          'from': 'employees',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'employee'
        }
      },
      {
        '$addFields': {
          'created_name': {
            '$arrayElemAt': [
              {
                '$cond': {
                  'if': {
                    '$eq': [
                      {
                        '$size': '$shop'
                      }, 1
                    ]
                  },
                  'then': '$shop.shop_name',
                  'else': {
                    '$cond': {
                      'if': {
                        '$eq': [
                          {
                            '$size': '$customer'
                          }, 1
                        ]
                      },
                      'then': '$customer.first_name',
                      'else': {
                        '$cond': {
                          'if': {
                            '$eq': [
                              {
                                '$size': '$employee'
                              }, 1
                            ]
                          },
                          'then': '$employee.name',
                          'else': [
                            ''
                          ]
                        }
                      }
                    }
                  }
                }
              }, 0
            ]
          }
        }
      }
    ]).toArray((err, item) => {

      if (err) {
        console.log('Watcher Booking waiting Err', err)
        return
      }



      if (item.length > 0) {

        io.sockets.in('User' + item[0].user_id._id).emit('customerBooking', item[0])
        io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopBooking', item[0])

        createSocketLog(app, {
          key: 'customerBooking shopBooking offerValue timeValue shopQueueData shopEmployeeData customerCount shopCount shopValue bookingValue',
          val: 'Booking',
          user_id: ObjectId(item[0]._id),
          data: { ...item[0] },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })

        userQueueAndBooking(app, item[0].user_id._id, (status, err, val) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item[0].user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(app, item[0].shop_id.user_information, (status, err, val) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopCount', val)
        })

        getShopCurrenQueue(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopValue', { data: item[0].shop_id._id, val })
          io.sockets.in('User' + item[0].user_id._id).emit('shopValue', { data: item[0].shop_id._id, val })
          io.sockets.in('global').emit('shopValue', { data: item[0].shop_id._id, val })
        })

        BookingWaitingTime(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('bookingValue', { data: item[0].shop_id._id, val })
          io.sockets.in('User' + item[0].user_id._id).emit('bookingValue', { data: item[0].shop_id._id, val })
          io.sockets.in('global').emit('bookingValue', { data: item[0].shop_id._id, val })
        })

        getShopTotalCount(app, item[0].shop_id.user_information, (status, err, val) => {
          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopTotalCount', { data: item[0].shop_id.user_information, val })
        })

        createNotification(app, item[0], (status, err, val) => {
          if (err) {
            console.log('Notification create err', err)
            return
          }

          console.log('notification created', val)
        })

        if (item[0].assign != null && item[0].assign != undefined) {
          io.sockets.in('Employee' + item[0].assign).emit('employeeAssignBooking', item[0])

          employeeQueueAndBooking(app, item[0].assign, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].assign).emit('employeeCount', data)
          })

        }

        if (item[0].create_type == "employee") {
          io.sockets.in('Employee' + item[0].created_by).emit('employeeBooking', item[0])

          employeeQueueAndBooking(app, item[0].created_by, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].created_by).emit('employeeCount', data)
          })
        }

      }

    })


  })

  db.collection('bookings').watch([{
    $match: {
      $and: [
        { "updateDescription.updatedFields.cancel_verify": { $exists: true } },
        { operationType: "update" }
      ]
    }
  }]).on('change', async (data) => {



    db.collection('bookings').aggregate([
      {
        '$match': {
          '_id': ObjectId(data.documentKey._id),
        }
      }, {
        '$lookup': {
          'from': 'services',
          'localField': 'service_id',
          'foreignField': '_id',
          'as': 'service_id'
        }
      }, {
        '$unwind': {
          'path': '$service_id'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'user_id',
          'foreignField': '_id',
          'as': 'user_id'
        }
      }, {
        '$unwind': {
          'path': '$user_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
      {
        '$lookup': {
          'from': 'bookingpayments',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'payment'
        }
      },
      {
        '$lookup': {
          'from': 'travels',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'travel'
        }
      },
      {
        '$addFields': {
          'distance': {
            '$arrayElemAt': [
              '$travel.distance', 0
            ]
          },
          'distanceVal': {
            '$arrayElemAt': [
              '$travel.distanceVal', 0
            ]
          },
          'duration': {
            '$arrayElemAt': [
              '$travel.duration', 0
            ]
          },
          'durationVal': {
            '$arrayElemAt': [
              '$travel.durationVal', 0
            ]
          },
          'lattitude': {
            '$arrayElemAt': [
              '$travel.lattitude', 0
            ]
          },
          'longitude': {
            '$arrayElemAt': [
              '$travel.longitude', 0
            ]
          },
        }
      },
      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_id._id'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$user_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$shop_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'userreview'
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'user_count': {
            '$reduce': {
              'input': '$userreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalUser': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$userreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$user_count', {
                      '$size': '$userreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      },

      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      },
      {
        '$lookup': {
          'from': 'bookings',
          'let': {
            'id': '$_id',
            'main': '$shop_id.user_information',
            'approximate_date': '$approximate_date',
            'seat': '$seat'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$seat', '$$seat'
                      ]
                    }, {
                      '$not': [
                        {
                          '$eq': [
                            '$_id', '$$id'
                          ]
                        }
                      ]
                    }, {
                      '$eq': [
                        '$queue', true
                      ]
                    }, {
                      '$eq': [
                        '$test', false
                      ]
                    }, {
                      '$lte': [
                        '$approximate_date', '$$approximate_date'
                      ]
                    }, {
                      '$in': [
                        '$status', [
                          'waiting', 'accepted'
                        ]
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'sbook'
        }
      }, {
        '$addFields': {
          'count': {
            '$reduce': {
              'input': '$sbook',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.size'
                ]
              }
            }
          }
        }
      },
      {
        '$lookup': {
          'from': 'shops',
          'localField': 'created_by',
          'foreignField': 'user_information',
          'as': 'shop'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'customer'
        }
      }, {
        '$lookup': {
          'from': 'employees',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'employee'
        }
      },
      {
        '$addFields': {
          'created_name': {
            '$arrayElemAt': [
              {
                '$cond': {
                  'if': {
                    '$eq': [
                      {
                        '$size': '$shop'
                      }, 1
                    ]
                  },
                  'then': '$shop.shop_name',
                  'else': {
                    '$cond': {
                      'if': {
                        '$eq': [
                          {
                            '$size': '$customer'
                          }, 1
                        ]
                      },
                      'then': '$customer.first_name',
                      'else': {
                        '$cond': {
                          'if': {
                            '$eq': [
                              {
                                '$size': '$employee'
                              }, 1
                            ]
                          },
                          'then': '$employee.name',
                          'else': [
                            ''
                          ]
                        }
                      }
                    }
                  }
                }
              }, 0
            ]
          }
        }
      }
    ]).toArray((err, item) => {

      if (err) {
        console.log('Watcher Booking Vancel Verify Err', err)
        return
      }



      if (item.length > 0) {


        io.sockets.in('User' + item[0].user_id._id).emit('customerBooking', item[0])
        io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopBooking', item[0])

        createSocketLog(app, {
          key: 'customerBooking shopBooking offerValue timeValue shopQueueData shopEmployeeData customerCount shopCount shopValue bookingValue',
          val: 'Booking',
          user_id: ObjectId(item[0]._id),
          data: { ...item[0] },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })

        userQueueAndBooking(app, item[0].user_id._id, (status, err, val) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item[0].user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(app, item[0].shop_id.user_information, (status, err, val) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopCount', val)
        })

        if (item[0].assign != null && item[0].assign != undefined) {
          io.sockets.in('Employee' + item[0].assign).emit('employeeAssignBooking', item[0])

          employeeQueueAndBooking(app, item[0].assign, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].assign).emit('employeeCount', data)
          })

        }

        if (item[0].create_type == "employee") {
          io.sockets.in('Employee' + item[0].created_by).emit('employeeBooking', item[0])

          employeeQueueAndBooking(app, item[0].created_by, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].created_by).emit('employeeCount', data)
          })
        }

      }

    })


  })

  db.collection('shops').watch([
    {
      '$match': {
        'operationType': 'insert',
      }
    }
  ]).on('change', async (data) => {

    const urlString = config.has('SERVER_URL') ? config.get('SERVER_URL') + '/api/open/' + data.documentKey._id : SERVER_URL + '/api/open/' + data.documentKey._id
    console.log('qrcode urle', urlString)

    QRCode.toDataURL(urlString, function (err, val) {

      if (err) {
        return console.log('Qrcode Generate Watcher Err', err)
      }

      console.log('Url Created After Qe', val)

      db.collection('shops').findOneAndUpdate({ _id: ObjectId(data.documentKey._id) }, {
        $set: {
          code: val,
          updatedAt: new Date()
        }
      }).then((cc) => {
        console.log('Qrcode Generate Shop User')
      }).catch((err) => {
        console.log('Shop Qrcode Watcher Update Err', err)
      })

    })

    const adminData = await db.collection('admins').findOne({ deleted: false })

    if (adminData !== null || adminData !== undefined) {
      adminCount(app, adminData._id, (status, err, data) => {

        if (err) {
          return
        }
        io.sockets.in('Admin' + adminData._id)('adminCount', data)
      })
    }

  })

  db.collection('customers').watch([
    {
      '$match': {
        'operationType': 'insert',
      }
    }
  ]).on('change', async (data) => {

    console.log('Customer Insert')

    const adminData = await db.collection('admins').findOne({ deleted: false })

    if (adminData !== null || adminData !== undefined) {
      adminCount(app, adminData._id, (status, err, data) => {

        if (err) {
          return
        }
        io.sockets.in('Admin' + adminData._id)('adminCount', data)
      })
    }

  })

  db.collection('locationlogs').watch([
    {
      '$match': {
        'operationType': 'insert',
      }
    }
  ]).on('change', async (data) => {

    console.log('Location Log Insert')

    const adminData = await db.collection('admins').findOne({ deleted: false })

    if (adminData !== null || adminData !== undefined) {
      adminCount(app, adminData._id, (status, err, data) => {

        if (err) {
          return
        }
        io.sockets.in('Admin' + adminData._id)('adminCount', data)
      })
    }

  })

  db.collection('servicerequests').watch([
    {
      '$match': {
        'operationType': 'insert',
      }
    }
  ]).on('change', async (data) => {

    console.log('Service Request Insert')

    const adminData = await db.collection('admins').findOne({ deleted: false })

    if (adminData !== null || adminData !== undefined) {
      adminCount(app, adminData._id, (status, err, data) => {

        if (err) {
          return
        }
        io.sockets.in('Admin' + adminData._id)('adminCount', data)
      })
    }

  })

  db.collection('bookings').watch([{
    $match: {
      $and: [
        { "updateDescription.updatedFields.approximate_date": { $exists: true } },
        { operationType: "update" }
      ]
    }
  }]).on('change', async (data) => {



    db.collection('bookings').aggregate([
      {
        '$match': {
          '_id': ObjectId(data.documentKey._id),
        }
      }, {
        '$lookup': {
          'from': 'services',
          'localField': 'service_id',
          'foreignField': '_id',
          'as': 'service_id'
        }
      }, {
        '$unwind': {
          'path': '$service_id'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'user_id',
          'foreignField': '_id',
          'as': 'user_id'
        }
      }, {
        '$unwind': {
          'path': '$user_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
      {
        '$lookup': {
          'from': 'bookingpayments',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'payment'
        }
      },
      {
        '$lookup': {
          'from': 'travels',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'travel'
        }
      },
      {
        '$addFields': {
          'distance': {
            '$arrayElemAt': [
              '$travel.distance', 0
            ]
          },
          'distanceVal': {
            '$arrayElemAt': [
              '$travel.distanceVal', 0
            ]
          },
          'duration': {
            '$arrayElemAt': [
              '$travel.duration', 0
            ]
          },
          'durationVal': {
            '$arrayElemAt': [
              '$travel.durationVal', 0
            ]
          },
          'lattitude': {
            '$arrayElemAt': [
              '$travel.lattitude', 0
            ]
          },
          'longitude': {
            '$arrayElemAt': [
              '$travel.longitude', 0
            ]
          },
        }
      },
      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_id._id'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$user_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$shop_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'userreview'
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'user_count': {
            '$reduce': {
              'input': '$userreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalUser': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$userreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$user_count', {
                      '$size': '$userreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      },

      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      },
      {
        '$lookup': {
          'from': 'bookings',
          'let': {
            'id': '$_id',
            'main': '$shop_id.user_information',
            'approximate_date': '$approximate_date',
            'seat': '$seat'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$seat', '$$seat'
                      ]
                    }, {
                      '$not': [
                        {
                          '$eq': [
                            '$_id', '$$id'
                          ]
                        }
                      ]
                    }, {
                      '$eq': [
                        '$queue', true
                      ]
                    }, {
                      '$eq': [
                        '$test', false
                      ]
                    }, {
                      '$lte': [
                        '$approximate_date', '$$approximate_date'
                      ]
                    }, {
                      '$in': [
                        '$status', [
                          'waiting', 'accepted'
                        ]
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'sbook'
        }
      }, {
        '$addFields': {
          'count': {
            '$reduce': {
              'input': '$sbook',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.size'
                ]
              }
            }
          }
        }
      },
      {
        '$lookup': {
          'from': 'shops',
          'localField': 'created_by',
          'foreignField': 'user_information',
          'as': 'shop'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'customer'
        }
      }, {
        '$lookup': {
          'from': 'employees',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'employee'
        }
      },
      {
        '$lookup': {
          'from': 'shorts',
          'localField': '_id',
          'foreignField': 'product_id',
          'as': 'short'
        }
      },
      {
        '$addFields': {
          'created_name': {
            '$arrayElemAt': [
              {
                '$cond': {
                  'if': {
                    '$eq': [
                      {
                        '$size': '$shop'
                      }, 1
                    ]
                  },
                  'then': '$shop.shop_name',
                  'else': {
                    '$cond': {
                      'if': {
                        '$eq': [
                          {
                            '$size': '$customer'
                          }, 1
                        ]
                      },
                      'then': '$customer.first_name',
                      'else': {
                        '$cond': {
                          'if': {
                            '$eq': [
                              {
                                '$size': '$employee'
                              }, 1
                            ]
                          },
                          'then': '$employee.name',
                          'else': [
                            ''
                          ]
                        }
                      }
                    }
                  }
                }
              }, 0
            ]
          }
        }
      }
    ]).toArray(async (err, item) => {

      if (err) {
        console.log('Watcher Booking Approximate Date Updated Err', err)
        return
      }



      if (item.length > 0) {


        io.sockets.in('User' + item[0].user_id._id).emit('customerBooking', item[0])
        io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopBooking', item[0])

        createSocketLog(app, {
          key: 'customerBooking shopBooking offerValue timeValue shopQueueData shopEmployeeData customerCount shopCount shopValue bookingValue',
          val: 'Booking',
          user_id: ObjectId(item[0]._id),
          data: { ...item[0] },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })

        userQueueAndBooking(app, item[0].user_id._id, (status, err, val) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item[0].user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(app, item[0].shop_id.user_information, (status, err, val) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopCount', val)
        })

        if (item[0].assign != null && item[0].assign != undefined) {
          io.sockets.in('Employee' + item[0].assign).emit('employeeAssignBooking', item[0])

          employeeQueueAndBooking(app, item[0].assign, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].assign).emit('employeeCount', data)
          })

        }

        if (item[0].create_type == "employee") {
          io.sockets.in('Employee' + item[0].created_by).emit('employeeBooking', item[0])

          employeeQueueAndBooking(app, item[0].created_by, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].created_by).emit('employeeCount', data)
          })
        }

      }

      if (item.length > 0 && (item[0].test == false && item[0].user_email !== null && item[0].user_email !== undefined && item[0].user_email == "" && item[0].status == 'accepted' && item[0].sms == false)) {

        try {
          const msgT = config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short[0].short_id : SHORT_URL_KEY + item[0].short[0].short_id
          const mainMsg = confirmationMsg({ ...item[0], link: msgT, countIcon: item[0].count == 0 ? 'no' : item[0].count })
          const smsToken = jwt.sign({ date: new Date(), message: mainMsg, id: ServiceTemplateId.confirm, type: 'booking', bookingid: item[0].bookingid, user_id: [item[0]._id] }, MAIL_SECRET_KEY)
          const emailData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'sendEmail' : ServiceSmsMail + 'sendEmail', {
            email: item[0].user_email,
            link: config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short_id : SHORT_URL_KEY + item[0].short_id
          }, {
            headers: {
              'auth-token': smsToken
            }
          })

          console.log('Email Sent', emailData)

        } catch (err) {
          console.log('Watcher Email Send Err', err)
        }
      }

      if (item.length > 0 && (item[0].test == false && item[0].mobile !== null && item[0].mobile !== undefined && item[0].mobile == "" && item[0].status == 'accepted' && item[0].sms == false)) {

        try {
          const msgTT = config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short[0].short_id : SHORT_URL_KEY + item[0].short[0].short_id
          const mainMsg = confirmationMsg({ ...item[0], link: msgTT, countIcon: item[0].count == 0 ? 'no' : item[0].count })
          const smssToken = jwt.sign({ date: new Date(), message: mainMsg, id: ServiceTemplateId.confirm, type: 'booking', bookingid: item[0].bookingid, user_id: [item[0]._id] }, MAIL_SECRET_KEY)
          const smsData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
            number: item[0].mobile,
            message: config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short_id : SHORT_URL_KEY + item[0].short_id
          }, {
            headers: {
              'auth-token': smssToken
            }
          })

          console.log('Sms Sent', smsData)

        } catch (err) {
          console.log('Watcher Sms Send Err', err)
        }

      }

    })

  })

  db.collection('bookings').watch([
    {
      '$match': {
        'operationType': 'insert',
      }
    }
  ]).on('change', async (data) => {

    console.log('Booking Insert Main Check', data)

    const adminData = await db.collection('admins').findOne({ deleted: false })

    if (adminData !== null || adminData !== undefined) {
      adminCount(app, adminData._id, (status, err, data) => {

        if (err) {
          return
        }
        io.sockets.in('Admin' + adminData._id)('adminCount', data)
      })
    }



    db.collection('bookings').aggregate([
      {
        '$match': {
          '_id': ObjectId(data.documentKey._id),
          test: false
        }
      }, {
        '$lookup': {
          'from': 'services',
          'localField': 'service_id',
          'foreignField': '_id',
          'as': 'service_id'
        }
      }, {
        '$unwind': {
          'path': '$service_id'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'user_id',
          'foreignField': '_id',
          'as': 'user_id'
        }
      }, {
        '$unwind': {
          'path': '$user_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
    ]).toArray(async (err, item) => {

      if (err) {
        console.log('Watcher Otp Verify Err', err)
        return
      }

      if (item.length > 0 && item[0].sms == false) {

        try {

          const msgT = await bookingOTPMsg(item[0])
          const smsToken = jwt.sign({ date: new Date(), message: msgT, id: ServiceTemplateId.otp, type: 'booking', bookingid: item[0].bookingid, user_id: [item[0]._id] }, MAIL_SECRET_KEY)

          if (data.fullDocument.mobile == null || data.fullDocument.mobile == undefined) {

            const emailOtp = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'sendOtpMail' : ServiceSmsMail + 'sendOtpMail', {
              email: data.fullDocument.user_email,
              otp: data.fullDocument.otp
            }, {
              headers: {
                'auth-token': smsToken
              }
            })



            console.log('Otp Sent', emailOtp)

          } else {
            const mobileOtp = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
              number: data.fullDocument.mobile,
              otp: data.fullDocument.otp
            }, {
              headers: {
                'auth-token': smsToken
              }
            })


            console.log('Otp Sent', mobileOtp)

          }

        } catch (err) {
          console.log('Watcher Otp Send Err', err)
        }
      }

    })

  })

  db.collection('shorts').watch([
    {
      '$match': {
        'operationType': 'insert',
      }
    }
  ]).on('change', async (data) => {

    console.log('Short Id Insert Main Check', data)

    db.collection('shorts').aggregate([
      {
        $match: {
          _id: ObjectId(data.documentKey._id),
          type: 'Booking'
        }
      }, {
        '$lookup': {
          'from': 'bookings',
          'localField': 'product_id',
          'foreignField': '_id',
          'as': 'product_id'
        }
      }, {
        '$unwind': {
          'path': '$product_id'
        }
      },
      {
        '$lookup': {
          'from': 'services',
          'localField': 'product_id.service_id',
          'foreignField': '_id',
          'as': 'product_id.service_id'
        }
      }, {
        '$unwind': {
          'path': '$product_id.service_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'product_id.shop_id',
          'foreignField': 'user_information',
          'as': 'product_id.shop_id'
        }
      }, {
        '$unwind': {
          'path': '$product_id.shop_id'
        }
      },

      {
        '$lookup': {
          'from': 'bookings',
          'let': {
            'id': '$product_id._id',
            'main': '$product_id.shop_id.user_information',
            'approximate_date': '$product_id.approximate_date',
            'seat': '$product_id.seat'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$seat', '$$seat'
                      ]
                    }, {
                      '$not': [
                        {
                          '$eq': [
                            '$_id', '$$id'
                          ]
                        }
                      ]
                    }, {
                      '$eq': [
                        '$queue', true
                      ]
                    }, {
                      '$eq': [
                        '$test', false
                      ]
                    }, {
                      '$lte': [
                        '$approximate_date', '$$approximate_date'
                      ]
                    }, {
                      '$in': [
                        '$status', [
                          'waiting', 'accepted'
                        ]
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'sbook'
        }
      }, {
        '$addFields': {
          'count': {
            '$reduce': {
              'input': '$sbook',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.size'
                ]
              }
            }
          }
        }
      },
    ]).toArray(async (err, item) => {
      if (err) {
        console.log('Shorts Find Err', err)
        return
      }

      if (item.length > 0 && (item[0].product_id.test == false && item[0].product_id.user_email !== null && item[0].product_id.user_email !== undefined && item[0].product_id.user_email !== "" && item[0].product_id.status == 'accepted' && item[0].product_id.sms == false)) {

        try {
          const msgT = config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short_id : SHORT_URL_KEY + item[0].short_id
          const mainMsg = confirmationMsg({ ...item[0].product_id, link: msgT, countIcon: item[0].count == 0 ? 'no' : item[0].count })
          const smsToken = jwt.sign({ date: new Date(), message: mainMsg, id: ServiceTemplateId.confirm, type: 'booking', bookingid: item[0].product_id.bookingid, user_id: [item[0].product_id._id] }, MAIL_SECRET_KEY)
          const emailData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'sendEmail' : ServiceSmsMail + 'sendEmail', {
            email: item[0].product_id.user_email,
            link: config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short_id : SHORT_URL_KEY + item[0].short_id
          }, {
            headers: {
              'auth-token': smsToken
            }
          })

          console.log('Email Sent', emailData)

        } catch (err) {
          console.log('Watcher Email Send Err', err)
        }
      }

      if (item.length > 0 && (item[0].product_id.test == false && item[0].product_id.mobile !== null && item[0].product_id.mobile !== undefined && item[0].product_id.mobile !== "" && item[0].product_id.status == 'accepted' && item[0].product_id.sms == false)) {

        try {
          const msgTT = config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short_id : SHORT_URL_KEY + item[0].short_id
          const mainMsg = confirmationMsg({ ...item[0].product_id, link: msgTT, countIcon: item[0].count == 0 ? 'no' : item[0].count })
          const smssToken = jwt.sign({ date: new Date(), message: mainMsg, id: ServiceTemplateId.confirm, type: 'booking', bookingid: item[0].product_id.bookingid, user_id: [item[0].product_id._id] }, MAIL_SECRET_KEY)
          const smsData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
            number: item[0].product_id.mobile,
            message: config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') + item[0].short_id : SHORT_URL_KEY + item[0].short_id
          }, {
            headers: {
              'auth-token': smssToken
            }
          })

          console.log('Sms Sent', smsData)

        } catch (err) {
          console.log('Watcher Sms Send Err', err)
        }

      }

    })

  })

  db.collection('notifications').watch([
    {
      '$match': {
        'operationType': 'insert',
      }
    }
  ]).on('change', async (data) => {

    console.log('Notification Insert Main Check', data)


    db.collection('notifications').findOne({ _id: ObjectId(data.documentKey._id) }).then((cc) => {

      if (cc == null || cc == undefined) {
        console.log('Notigication find Err', cc)
        return
      }

      countNotification(app, cc.user_id, (status, err, data) => {
        if (err) {
          return
        }

        io.sockets.in('User' + cc.user_id).emit('notificationCount', data)
      })

      countShopNotification(app, cc.shop_id, (status, err, data) => {
        if (err) {
          return
        }

        io.sockets.in('Shop' + cc.shop_id).emit('notificationShopCount', data)
      })

    }).catch((err) => {
      console.log('Notigication find Err', err)
    })


  })

  db.collection('employees').watch([
    {
      '$match': {
        'operationType': 'insert',
      }
    }
  ]).on('change', async (data) => {

    console.log('Empoyee Insert Main Check', data)


    db.collection('employees').findOne({ _id: ObjectId(data.documentKey._id) }).then(async (cc) => {

      if (cc == null || cc == undefined) {
        console.log('Employees find Err', cc)
        return
      }

      if (cc.create_type == 'Shop') {
        try {
          const msgT = bookingOTPMsg({ service_id: { name: 'Employee Created' }, shop_name: 'Employee', otp: cc.employee_id, bookingid: '1', queue: true })
          const smsToken = jwt.sign({ date: new Date(), message: msgT, id: ServiceTemplateId.book, type: 'employeeCreate', user_id: [cc._id] }, MAIL_SECRET_KEY)
          const mainData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
            number: cc.mobile,
            otp: cc.employee_id
          }, {
            headers: {
              'auth-token': smsToken
            }
          })

          console.log('Employee Id Sent', mainData)

        } catch (err) {
          console.log('Watcher Employee Id Send Err', err)
        }
      }

      if (cc.create_type == 'Direct') {

        try {
          const msgT = bookingOTPMsg({ service_id: { name: 'Signup' }, shop_name: 'Employee', otp: cc.otp, bookingid: '1', queue: true })
          const smsToken = jwt.sign({ date: new Date(), id: ServiceTemplateId.book, message: msgT, type: 'employeeSignup', user_id: [cc._id] }, MAIL_SECRET_KEY)
          const mainData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
            number: cc.mobile,
            otp: cc.otp
          }, {
            headers: {
              'auth-token': smsToken
            }
          })

          console.log('Employee Id Sent', mainData)

        } catch (err) {
          console.log('Watcher Employee Id Send Err', err)
        }
      }

    }).catch((err) => {
      console.log('Employees find Err', err)
    })


  })

  db.collection('bookings').watch([{
    $match: {
      $and: [
        { "updateDescription.updatedFields.assign": { $exists: true } },
        { operationType: "update" }
      ]
    }
  }]).on('change', async (data) => {

    console.log('watcher Booking Employee Assign',)


    db.collection('bookings').aggregate([
      {
        '$match': {
          '_id': ObjectId(data.documentKey._id),
        }
      }, {
        '$lookup': {
          'from': 'services',
          'localField': 'service_id',
          'foreignField': '_id',
          'as': 'service_id'
        }
      }, {
        '$unwind': {
          'path': '$service_id'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'user_id',
          'foreignField': '_id',
          'as': 'user_id'
        }
      }, {
        '$unwind': {
          'path': '$user_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
      {
        '$lookup': {
          'from': 'bookingpayments',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'payment'
        }
      },
      {
        '$lookup': {
          'from': 'travels',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'travel'
        }
      },
      {
        '$addFields': {
          'distance': {
            '$arrayElemAt': [
              '$travel.distance', 0
            ]
          },
          'distanceVal': {
            '$arrayElemAt': [
              '$travel.distanceVal', 0
            ]
          },
          'duration': {
            '$arrayElemAt': [
              '$travel.duration', 0
            ]
          },
          'durationVal': {
            '$arrayElemAt': [
              '$travel.durationVal', 0
            ]
          },
          'lattitude': {
            '$arrayElemAt': [
              '$travel.lattitude', 0
            ]
          },
          'longitude': {
            '$arrayElemAt': [
              '$travel.longitude', 0
            ]
          },
        }
      },
      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_id._id'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$user_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$shop_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'userreview'
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'user_count': {
            '$reduce': {
              'input': '$userreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalUser': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$userreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$user_count', {
                      '$size': '$userreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      },

      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      },
      {
        '$lookup': {
          'from': 'bookings',
          'let': {
            'id': '$_id',
            'main': '$shop_id.user_information',
            'approximate_date': '$approximate_date',
            'seat': '$seat'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$seat', '$$seat'
                      ]
                    }, {
                      '$not': [
                        {
                          '$eq': [
                            '$_id', '$$id'
                          ]
                        }
                      ]
                    }, {
                      '$eq': [
                        '$queue', true
                      ]
                    }, {
                      '$eq': [
                        '$test', false
                      ]
                    }, {
                      '$lte': [
                        '$approximate_date', '$$approximate_date'
                      ]
                    }, {
                      '$in': [
                        '$status', [
                          'waiting', 'accepted'
                        ]
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'sbook'
        }
      }, {
        '$addFields': {
          'count': {
            '$reduce': {
              'input': '$sbook',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.size'
                ]
              }
            }
          }
        }
      },
      {
        '$lookup': {
          'from': 'shops',
          'localField': 'created_by',
          'foreignField': 'user_information',
          'as': 'shop'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'customer'
        }
      }, {
        '$lookup': {
          'from': 'employees',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'employee'
        }
      },
      {
        '$addFields': {
          'created_name': {
            '$arrayElemAt': [
              {
                '$cond': {
                  'if': {
                    '$eq': [
                      {
                        '$size': '$shop'
                      }, 1
                    ]
                  },
                  'then': '$shop.shop_name',
                  'else': {
                    '$cond': {
                      'if': {
                        '$eq': [
                          {
                            '$size': '$customer'
                          }, 1
                        ]
                      },
                      'then': '$customer.first_name',
                      'else': {
                        '$cond': {
                          'if': {
                            '$eq': [
                              {
                                '$size': '$employee'
                              }, 1
                            ]
                          },
                          'then': '$employee.name',
                          'else': [
                            ''
                          ]
                        }
                      }
                    }
                  }
                }
              }, 0
            ]
          }
        }
      }
    ]).toArray(async (err, item) => {

      if (err) {
        console.log('Watcher Assign Employee Err', err)
        return
      }



      if (item.length > 0) {

        io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopBooking', item[0])

        createSocketLog(app, {
          key: 'shopBooking',
          val: 'Booking',
          user_id: ObjectId(item[0]._id),
          data: { ...item[0] },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })
        console.log('watcher Assign Updated')
        console.log('check Employee', item[0].assign, 'condition', item[0].assign != null && item[0].assign != undefined)

        if (item[0].assign != null && item[0].assign != undefined) {
          io.sockets.in('Employee' + item[0].assign).emit('employeeAssignBooking', item[0])

          employeeQueueAndBooking(app, item[0].assign, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].assign).emit('employeeCount', data)
          })

        }


        getShopEmployee(app, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('shopEmployeeData', val)
        })

        if (item[0].create_type == "employee") {
          io.sockets.in('Employee' + item[0].created_by).emit('employeeBooking', item[0])

          employeeQueueAndBooking(app, item[0].created_by, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].created_by).emit('employeeCount', data)
          })
        }


      }

    })


  })

  db.collection('bookings').watch([{
    $match: {
      $and: [
        { "updateDescription.updatedFields.remark": { $exists: true } },
        { operationType: "update" }
      ]
    }
  }]).on('change', async (data) => {



    db.collection('bookings').aggregate([
      {
        '$match': {
          '_id': ObjectId(data.documentKey._id),
        }
      }, {
        '$lookup': {
          'from': 'services',
          'localField': 'service_id',
          'foreignField': '_id',
          'as': 'service_id'
        }
      }, {
        '$unwind': {
          'path': '$service_id'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'user_id',
          'foreignField': '_id',
          'as': 'user_id'
        }
      }, {
        '$unwind': {
          'path': '$user_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
      {
        '$lookup': {
          'from': 'bookingpayments',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'payment'
        }
      },
      {
        '$lookup': {
          'from': 'travels',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'travel'
        }
      },
      {
        '$addFields': {
          'distance': {
            '$arrayElemAt': [
              '$travel.distance', 0
            ]
          },
          'distanceVal': {
            '$arrayElemAt': [
              '$travel.distanceVal', 0
            ]
          },
          'duration': {
            '$arrayElemAt': [
              '$travel.duration', 0
            ]
          },
          'durationVal': {
            '$arrayElemAt': [
              '$travel.durationVal', 0
            ]
          },
          'lattitude': {
            '$arrayElemAt': [
              '$travel.lattitude', 0
            ]
          },
          'longitude': {
            '$arrayElemAt': [
              '$travel.longitude', 0
            ]
          },
        }
      },
      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_id._id'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$user_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$shop_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'userreview'
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'user_count': {
            '$reduce': {
              'input': '$userreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalUser': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$userreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$user_count', {
                      '$size': '$userreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      },

      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      },
      {
        '$lookup': {
          'from': 'bookings',
          'let': {
            'id': '$_id',
            'main': '$shop_id.user_information',
            'approximate_date': '$approximate_date',
            'seat': '$seat'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$seat', '$$seat'
                      ]
                    }, {
                      '$not': [
                        {
                          '$eq': [
                            '$_id', '$$id'
                          ]
                        }
                      ]
                    }, {
                      '$eq': [
                        '$queue', true
                      ]
                    }, {
                      '$eq': [
                        '$test', false
                      ]
                    }, {
                      '$lte': [
                        '$approximate_date', '$$approximate_date'
                      ]
                    }, {
                      '$in': [
                        '$status', [
                          'waiting', 'accepted'
                        ]
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'sbook'
        }
      }, {
        '$addFields': {
          'count': {
            '$reduce': {
              'input': '$sbook',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.size'
                ]
              }
            }
          }
        }
      },
      {
        '$lookup': {
          'from': 'shops',
          'localField': 'created_by',
          'foreignField': 'user_information',
          'as': 'shop'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'customer'
        }
      }, {
        '$lookup': {
          'from': 'employees',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'employee'
        }
      },
      {
        '$addFields': {
          'created_name': {
            '$arrayElemAt': [
              {
                '$cond': {
                  'if': {
                    '$eq': [
                      {
                        '$size': '$shop'
                      }, 1
                    ]
                  },
                  'then': '$shop.shop_name',
                  'else': {
                    '$cond': {
                      'if': {
                        '$eq': [
                          {
                            '$size': '$customer'
                          }, 1
                        ]
                      },
                      'then': '$customer.first_name',
                      'else': {
                        '$cond': {
                          'if': {
                            '$eq': [
                              {
                                '$size': '$employee'
                              }, 1
                            ]
                          },
                          'then': '$employee.name',
                          'else': [
                            ''
                          ]
                        }
                      }
                    }
                  }
                }
              }, 0
            ]
          }
        }
      }
    ]).toArray(async (err, item) => {

      if (err) {
        console.log('Watcher Assign Employee Err', err)
        return
      }



      if (item.length > 0) {

        io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopBooking', item[0])

        createSocketLog(app, {
          key: 'shopBooking',
          val: 'Booking',
          user_id: ObjectId(item[0]._id),
          data: { ...item[0] },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })

        if (item[0].assign != null && item[0].assign != undefined) {
          io.sockets.in('Employee' + item[0].assign).emit('employeeAssignBooking', item[0])

          employeeQueueAndBooking(app, item[0].assign, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].assign).emit('employeeCount', data)
          })

        }

        if (item[0].create_type == "employee") {
          io.sockets.in('Employee' + item[0].created_by).emit('employeeBooking', item[0])

          employeeQueueAndBooking(app, item[0].created_by, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].created_by).emit('employeeCount', data)
          })
        }


      }

    })


  })

  db.collection('bookings').watch([{
    $match: {
      $and: [
        { "updateDescription.updatedFields.seat": { $exists: true } },
        { operationType: "update" }
      ]
    }
  }]).on('change', async (data) => {

    console.log('seat Updated watcher')

    db.collection('bookings').aggregate([
      {
        '$match': {
          '_id': ObjectId(data.documentKey._id),
        }
      }, {
        '$lookup': {
          'from': 'services',
          'localField': 'service_id',
          'foreignField': '_id',
          'as': 'service_id'
        }
      }, {
        '$unwind': {
          'path': '$service_id'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'user_id',
          'foreignField': '_id',
          'as': 'user_id'
        }
      }, {
        '$unwind': {
          'path': '$user_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
      {
        '$lookup': {
          'from': 'bookingpayments',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'payment'
        }
      },
      {
        '$lookup': {
          'from': 'travels',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'travel'
        }
      },
      {
        '$addFields': {
          'distance': {
            '$arrayElemAt': [
              '$travel.distance', 0
            ]
          },
          'distanceVal': {
            '$arrayElemAt': [
              '$travel.distanceVal', 0
            ]
          },
          'duration': {
            '$arrayElemAt': [
              '$travel.duration', 0
            ]
          },
          'durationVal': {
            '$arrayElemAt': [
              '$travel.durationVal', 0
            ]
          },
          'lattitude': {
            '$arrayElemAt': [
              '$travel.lattitude', 0
            ]
          },
          'longitude': {
            '$arrayElemAt': [
              '$travel.longitude', 0
            ]
          },
        }
      },
      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_id._id'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$user_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$shop_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'userreview'
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'user_count': {
            '$reduce': {
              'input': '$userreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalUser': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$userreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$user_count', {
                      '$size': '$userreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      },

      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$shop_id.user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      },
      {
        '$lookup': {
          'from': 'bookings',
          'let': {
            'id': '$_id',
            'main': '$shop_id.user_information',
            'approximate_date': '$approximate_date',
            'seat': '$seat'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$seat', '$$seat'
                      ]
                    }, {
                      '$not': [
                        {
                          '$eq': [
                            '$_id', '$$id'
                          ]
                        }
                      ]
                    }, {
                      '$eq': [
                        '$queue', true
                      ]
                    }, {
                      '$eq': [
                        '$test', false
                      ]
                    }, {
                      '$lte': [
                        '$approximate_date', '$$approximate_date'
                      ]
                    }, {
                      '$in': [
                        '$status', [
                          'waiting', 'accepted'
                        ]
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'sbook'
        }
      }, {
        '$addFields': {
          'count': {
            '$reduce': {
              'input': '$sbook',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.size'
                ]
              }
            }
          }
        }
      },
      {
        '$lookup': {
          'from': 'shops',
          'localField': 'created_by',
          'foreignField': 'user_information',
          'as': 'shop'
        }
      }, {
        '$lookup': {
          'from': 'customers',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'customer'
        }
      }, {
        '$lookup': {
          'from': 'employees',
          'localField': 'created_by',
          'foreignField': '_id',
          'as': 'employee'
        }
      },
      {
        '$addFields': {
          'created_name': {
            '$arrayElemAt': [
              {
                '$cond': {
                  'if': {
                    '$eq': [
                      {
                        '$size': '$shop'
                      }, 1
                    ]
                  },
                  'then': '$shop.shop_name',
                  'else': {
                    '$cond': {
                      'if': {
                        '$eq': [
                          {
                            '$size': '$customer'
                          }, 1
                        ]
                      },
                      'then': '$customer.first_name',
                      'else': {
                        '$cond': {
                          'if': {
                            '$eq': [
                              {
                                '$size': '$employee'
                              }, 1
                            ]
                          },
                          'then': '$employee.name',
                          'else': [
                            ''
                          ]
                        }
                      }
                    }
                  }
                }
              }, 0
            ]
          }
        }
      }
    ]).toArray(async (err, item) => {

      if (err) {
        console.log('Watcher seat Err', err)
        return
      }

      const adminData = await db.collection('admins').findOne({ deleted: false })

      if (item.length > 0) {

        io.sockets.in('User' + item[0].user_id._id).emit('customerBooking', item[0])
        io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopBooking', item[0])


        if (adminData !== null || adminData !== undefined) {
          io.sockets.in('Admin' + adminData._id).emit('adminBooking', item[0])
        }

        createSocketLog(app, {
          key: 'customerBooking shopBooking offerValue timeValue shopQueueData shopEmployeeData customerCount shopCount shopValue bookingValue',
          val: 'Booking',
          user_id: ObjectId(item[0]._id),
          data: { ...item[0] },
          type: true,
          deleted: false,
          status: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }, (status, err, val) => {
          if (err) {
            return
          }
        })

        getShopOfferQueue(app, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('offerValue', val)
          io.sockets.in('User' + item[0].user_id._id).emit('offerValue', val)
          io.sockets.in('global').emit('offerValue', val)
        })

        getShopQueueCount(app, item[0].shop_id.user_information, (status, err, val) => {
          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopQueueCount', { data: item[0].shop_id.user_information, val })
          io.sockets.in('global').emit('shopQueueCount', { data: item[0].shop_id.user_information, val })
        })

        getShopServiceBookingTime(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('timeValue', { data: item[0].shop_id._id, val })
        })

        shopBookingQueueData(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('global').emit('shopQueueData', { data: item[0].shop_id._id, val })
        })

        userQueueAndBooking(app, item[0].user_id._id, (status, err, val) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item[0].user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(app, item[0].shop_id.user_information, (status, err, val) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopCount', val)
        })

        getShopCurrenQueue(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopValue', { data: item[0].shop_id._id, val })
          io.sockets.in('User' + item[0].user_id._id).emit('shopValue', { data: item[0].shop_id._id, val })
          io.sockets.in('global').emit('shopValue', { data: item[0].shop_id._id, val })
        })

        BookingWaitingTime(app, item[0].shop_id._id, (status, err, val) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item[0].shop_id.user_information).emit('bookingValue', { data: item[0].shop_id._id, val })
          io.sockets.in('User' + item[0].user_id._id).emit('bookingValue', { data: item[0].shop_id._id, val })
          io.sockets.in('global').emit('bookingValue', { data: item[0].shop_id._id, val })
        })

        if (item[0].assign != null && item[0].assign != undefined) {
          io.sockets.in('Employee' + item[0].assign).emit('employeeAssignBooking', item[0])

          employeeQueueAndBooking(app, item[0].assign, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].assign).emit('employeeCount', data)
          })

        }

        if (item[0].create_type == "employee") {
          io.sockets.in('Employee' + item[0].created_by).emit('employeeBooking', item[0])

          employeeQueueAndBooking(app, item[0].created_by, (status, err, data) => {

            if (err) {
              return
            }
            io.sockets.in('Employee' + item[0].created_by).emit('employeeCount', data)
          })
        }


      }

    })


  })

  db.collection('sockets').watch([
    {
      '$match': {
        'operationType': 'insert',
      }
    }
  ]).on('change', async (data) => {

    console.log('Socket Request Insert')

    const adminData = await db.collection('admins').findOne({ deleted: false })

    if (adminData !== null || adminData !== undefined) {
      adminCount(app, adminData._id, (status, err, data) => {

        if (err) {
          return
        }
        io.sockets.in('Admin' + adminData._id).emit('adminCount', data)
      })
    }

  })

  db.collection('sockets').watch([{
    $match: {
      $and: [
        { "updateDescription.updatedFields.status": { $exists: true } },
        { operationType: "update" }
      ]
    }
  }]).on('change', async (data) => {

    console.log('Socket Request Update')

    const adminData = await db.collection('admins').findOne({ deleted: false })

    if (adminData !== null || adminData !== undefined) {
      adminCount(app, adminData._id, (status, err, data) => {

        if (err) {
          return
        }
        io.sockets.in('Admin' + adminData._id).emit('adminCount', data)
      })
    }

  })

  db.collection('employees').watch([{
    $match: {
      $and: [
        { "updateDescription.updatedFields.otp_verify": { $exists: true } },
        { operationType: "update" }
      ]
    }
  }]).on('change', async (data) => {

    console.log('Employee')


    db.collection('employees').aggregate([
      {
        $match: { _id: ObjectId(data.documentKey._id) }
      },
      {
        '$lookup': {
          'from': 'shops',
          'localField': 'user_id',
          'foreignField': 'user_information',
          'as': 'user_id'
        }
      },
      {
        '$unwind': {
          'path': '$user_id'
        }
      }
    ]).toArray(async (err, cc) => {

      if (err) {
        console.log('Employees find Err', err)
        return
      }

      if (cc.length == 0) {
        console.log('Employees find Err', cc)
        return
      }

      if (cc.length > 0) {
        try {
          const msgT = bookingOTPMsg({ service_id: { name: 'Id' }, shop_name: 'Employee', otp: cc[0].employee_id, bookingid: '1', queue: true })
          const smsToken = jwt.sign({ date: new Date(), id: ServiceTemplateId.book, message: msgT, type: 'employeeCreate', user_id: [cc[0]._id] }, MAIL_SECRET_KEY)
          const mainData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
            number: cc[0].mobile,
            otp: cc[0].employee_id
          }, {
            headers: {
              'auth-token': smsToken
            }
          })

          console.log('Employee Id Sent', mainData)

        } catch (err) {
          console.log('Watcher Employee Id Send Err', err)
        }

        db.collection('notifications').insertOne({
          user_id: cc[0]._id,
          shop_id: cc[0].user_id.user_information,
          name: cc[0].name,
          service_name: '',
          shop_name: cc[0].user_id.shop_name,
          shop_image: cc[0].user_id.image_link,
          message: `Dear ${cc[0].user_id.shop_name} a new employee ${cc[0].name} is added`,
          view: false,
          type: 'employee',
          shop_view: false,
          like: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }).then((cc) => {

        }).catch((err) => {
          console.log('Notify Err', err)
        })
      }

    })

  })

  db.collection('travels').watch([
    {
      '$match': {
        'operationType': 'update',
      }
    }
  ]).on('change', async (data) => {

    console.log('travel Request Insert')

    db.collection('travels').aggregate([
      {
        $match: {
          _id: ObjectId(data.documentKey._id)
        }
      }
    ]).toArray(async (err, cc) => {

      if (err) {
        console.log('Watcher seat Err', err)
        return
      }

      if (cc.length > 0) {

        db.collection('bookings').aggregate([
          {
            '$match': {
              '_id': ObjectId(cc[0].booking_id),
            }
          }, {
            '$lookup': {
              'from': 'services',
              'localField': 'service_id',
              'foreignField': '_id',
              'as': 'service_id'
            }
          }, {
            '$unwind': {
              'path': '$service_id'
            }
          }, {
            '$lookup': {
              'from': 'customers',
              'localField': 'user_id',
              'foreignField': '_id',
              'as': 'user_id'
            }
          }, {
            '$unwind': {
              'path': '$user_id'
            }
          }, {
            '$lookup': {
              'from': 'shops',
              'localField': 'shop_id',
              'foreignField': 'user_information',
              'as': 'shop_id'
            }
          }, {
            '$unwind': {
              'path': '$shop_id'
            }
          },
          {
            '$lookup': {
              'from': 'bookingpayments',
              'localField': '_id',
              'foreignField': 'booking_id',
              'as': 'payment'
            }
          },
          {
            '$lookup': {
              'from': 'travels',
              'localField': '_id',
              'foreignField': 'booking_id',
              'as': 'travel'
            }
          },
          {
            '$addFields': {
              'distance': {
                '$arrayElemAt': [
                  '$travel.distance', 0
                ]
              },
              'distanceVal': {
                '$arrayElemAt': [
                  '$travel.distanceVal', 0
                ]
              },
              'duration': {
                '$arrayElemAt': [
                  '$travel.duration', 0
                ]
              },
              'durationVal': {
                '$arrayElemAt': [
                  '$travel.durationVal', 0
                ]
              },
              'lattitude': {
                '$arrayElemAt': [
                  '$travel.lattitude', 0
                ]
              },
              'longitude': {
                '$arrayElemAt': [
                  '$travel.longitude', 0
                ]
              },
            }
          },
          {
            '$lookup': {
              'from': 'reviews',
              'let': {
                'main': '$user_id._id'
              },
              'pipeline': [
                {
                  '$match': {
                    '$expr': {
                      '$and': [
                        {
                          '$eq': [
                            '$user_id', '$$main'
                          ]
                        }, {
                          '$eq': [
                            '$shop_review', true
                          ]
                        }
                      ]
                    }
                  }
                }
              ],
              'as': 'userreview'
            }
          }, {
            '$lookup': {
              'from': 'reviews',
              'let': {
                'main': '$shop_id.user_information'
              },
              'pipeline': [
                {
                  '$match': {
                    '$expr': {
                      '$and': [
                        {
                          '$eq': [
                            '$shop_id', '$$main'
                          ]
                        }, {
                          '$eq': [
                            '$user_review', true
                          ]
                        }
                      ]
                    }
                  }
                }
              ],
              'as': 'shopreview'
            }
          }, {
            '$addFields': {
              'user_count': {
                '$reduce': {
                  'input': '$userreview',
                  'initialValue': 0,
                  'in': {
                    '$sum': [
                      '$$value', '$$this.rating'
                    ]
                  }
                }
              }
            }
          }, {
            '$addFields': {
              'totalUser': {
                '$cond': {
                  'if': {
                    '$eq': [
                      {
                        '$size': '$userreview'
                      }, 0
                    ]
                  },
                  'then': 0,
                  'else': {
                    '$ceil': {
                      '$divide': [
                        '$user_count', {
                          '$size': '$userreview'
                        }
                      ]
                    }
                  }
                }
              }
            }
          }, {
            '$addFields': {
              'shop_count': {
                '$reduce': {
                  'input': '$shopreview',
                  'initialValue': 0,
                  'in': {
                    '$sum': [
                      '$$value', '$$this.rating'
                    ]
                  }
                }
              }
            }
          }, {
            '$addFields': {
              'totalShop': {
                '$cond': {
                  'if': {
                    '$eq': [
                      {
                        '$size': '$shopreview'
                      }, 0
                    ]
                  },
                  'then': 0,
                  'else': {
                    '$ceil': {
                      '$divide': [
                        '$shop_count', {
                          '$size': '$shopreview'
                        }
                      ]
                    }
                  }
                }
              }
            }
          },

          {
            '$lookup': {
              'from': 'reviews',
              'let': {
                'main': '$shop_id.user_information'
              },
              'pipeline': [
                {
                  '$match': {
                    '$expr': {
                      '$and': [
                        {
                          '$eq': [
                            '$shop_id', '$$main'
                          ]
                        }, {
                          '$eq': [
                            '$user_review', true
                          ]
                        }
                      ]
                    }
                  }
                }
              ],
              'as': 'shopreview'
            }
          },
          {
            '$lookup': {
              'from': 'bookings',
              'let': {
                'id': '$_id',
                'main': '$shop_id.user_information',
                'approximate_date': '$approximate_date',
                'seat': '$seat'
              },
              'pipeline': [
                {
                  '$match': {
                    '$expr': {
                      '$and': [
                        {
                          '$eq': [
                            '$shop_id', '$$main'
                          ]
                        }, {
                          '$eq': [
                            '$seat', '$$seat'
                          ]
                        }, {
                          '$not': [
                            {
                              '$eq': [
                                '$_id', '$$id'
                              ]
                            }
                          ]
                        }, {
                          '$eq': [
                            '$queue', true
                          ]
                        }, {
                          '$eq': [
                            '$test', false
                          ]
                        }, {
                          '$lte': [
                            '$approximate_date', '$$approximate_date'
                          ]
                        }, {
                          '$in': [
                            '$status', [
                              'waiting', 'accepted'
                            ]
                          ]
                        }
                      ]
                    }
                  }
                }
              ],
              'as': 'sbook'
            }
          }, {
            '$addFields': {
              'count': {
                '$reduce': {
                  'input': '$sbook',
                  'initialValue': 0,
                  'in': {
                    '$sum': [
                      '$$value', '$$this.size'
                    ]
                  }
                }
              }
            }
          },
          {
            '$lookup': {
              'from': 'shops',
              'localField': 'created_by',
              'foreignField': 'user_information',
              'as': 'shop'
            }
          }, {
            '$lookup': {
              'from': 'customers',
              'localField': 'created_by',
              'foreignField': '_id',
              'as': 'customer'
            }
          }, {
            '$lookup': {
              'from': 'employees',
              'localField': 'created_by',
              'foreignField': '_id',
              'as': 'employee'
            }
          },
          {
            '$addFields': {
              'created_name': {
                '$arrayElemAt': [
                  {
                    '$cond': {
                      'if': {
                        '$eq': [
                          {
                            '$size': '$shop'
                          }, 1
                        ]
                      },
                      'then': '$shop.shop_name',
                      'else': {
                        '$cond': {
                          'if': {
                            '$eq': [
                              {
                                '$size': '$customer'
                              }, 1
                            ]
                          },
                          'then': '$customer.first_name',
                          'else': {
                            '$cond': {
                              'if': {
                                '$eq': [
                                  {
                                    '$size': '$employee'
                                  }, 1
                                ]
                              },
                              'then': '$employee.name',
                              'else': [
                                ''
                              ]
                            }
                          }
                        }
                      }
                    }
                  }, 0
                ]
              }
            }
          }
        ]).toArray(async (err, item) => {

          if (err) {
            console.log('Watcher travels Err', err)
            return
          }

          if (item.length > 0) {

            io.sockets.in('User' + item[0].user_id._id).emit('customerBooking', item[0])
            io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopBooking', item[0])

            createSocketLog(app, {
              key: 'customerBooking shopBooking  shopQueueData',
              val: 'Booking',
              user_id: ObjectId(item[0]._id),
              data: { ...item[0] },
              type: true,
              deleted: false,
              status: true,
              createdAt: new Date(),
              updatedAt: new Date()
            }, (status, err, val) => {
              if (err) {
                return
              }
            })

            shopBookingQueueData(app, item[0].shop_id._id, (status, err, val) => {
              if (err) {
                return
              }
              io.sockets.in('global').emit('shopQueueData', { data: item[0].shop_id._id, val })
            })

          }

        })

      }

    })

  })


  db.collection('bookingpayments').watch([
    {
      '$match': {
        'operationType': 'insert',
      }
    }
  ]).on('change', async (data) => {

    db.collection('bookingpayments').aggregate([
      {
        $match: {
          _id: ObjectId(data.documentKey._id)
        }
      }
    ]).toArray(async (err, cc) => {

      if (err) {
        console.log('Watcher seat Err', err)
        return
      }

      if (cc.length > 0) {

        db.collection('bookings').aggregate([
          {
            '$match': {
              '_id': ObjectId(cc[0].booking_id),
            }
          }, {
            '$lookup': {
              'from': 'services',
              'localField': 'service_id',
              'foreignField': '_id',
              'as': 'service_id'
            }
          }, {
            '$unwind': {
              'path': '$service_id'
            }
          }, {
            '$lookup': {
              'from': 'customers',
              'localField': 'user_id',
              'foreignField': '_id',
              'as': 'user_id'
            }
          }, {
            '$unwind': {
              'path': '$user_id'
            }
          }, {
            '$lookup': {
              'from': 'shops',
              'localField': 'shop_id',
              'foreignField': 'user_information',
              'as': 'shop_id'
            }
          }, {
            '$unwind': {
              'path': '$shop_id'
            }
          },
          {
            '$lookup': {
              'from': 'bookingpayments',
              'localField': '_id',
              'foreignField': 'booking_id',
              'as': 'payment'
            }
          },
          {
            '$lookup': {
              'from': 'travels',
              'localField': '_id',
              'foreignField': 'booking_id',
              'as': 'travel'
            }
          },
          {
            '$addFields': {
              'distance': {
                '$arrayElemAt': [
                  '$travel.distance', 0
                ]
              },
              'distanceVal': {
                '$arrayElemAt': [
                  '$travel.distanceVal', 0
                ]
              },
              'duration': {
                '$arrayElemAt': [
                  '$travel.duration', 0
                ]
              },
              'durationVal': {
                '$arrayElemAt': [
                  '$travel.durationVal', 0
                ]
              },
              'lattitude': {
                '$arrayElemAt': [
                  '$travel.lattitude', 0
                ]
              },
              'longitude': {
                '$arrayElemAt': [
                  '$travel.longitude', 0
                ]
              },
            }
          },
          {
            '$lookup': {
              'from': 'reviews',
              'let': {
                'main': '$user_id._id'
              },
              'pipeline': [
                {
                  '$match': {
                    '$expr': {
                      '$and': [
                        {
                          '$eq': [
                            '$user_id', '$$main'
                          ]
                        }, {
                          '$eq': [
                            '$shop_review', true
                          ]
                        }
                      ]
                    }
                  }
                }
              ],
              'as': 'userreview'
            }
          }, {
            '$lookup': {
              'from': 'reviews',
              'let': {
                'main': '$shop_id.user_information'
              },
              'pipeline': [
                {
                  '$match': {
                    '$expr': {
                      '$and': [
                        {
                          '$eq': [
                            '$shop_id', '$$main'
                          ]
                        }, {
                          '$eq': [
                            '$user_review', true
                          ]
                        }
                      ]
                    }
                  }
                }
              ],
              'as': 'shopreview'
            }
          }, {
            '$addFields': {
              'user_count': {
                '$reduce': {
                  'input': '$userreview',
                  'initialValue': 0,
                  'in': {
                    '$sum': [
                      '$$value', '$$this.rating'
                    ]
                  }
                }
              }
            }
          }, {
            '$addFields': {
              'totalUser': {
                '$cond': {
                  'if': {
                    '$eq': [
                      {
                        '$size': '$userreview'
                      }, 0
                    ]
                  },
                  'then': 0,
                  'else': {
                    '$ceil': {
                      '$divide': [
                        '$user_count', {
                          '$size': '$userreview'
                        }
                      ]
                    }
                  }
                }
              }
            }
          }, {
            '$addFields': {
              'shop_count': {
                '$reduce': {
                  'input': '$shopreview',
                  'initialValue': 0,
                  'in': {
                    '$sum': [
                      '$$value', '$$this.rating'
                    ]
                  }
                }
              }
            }
          }, {
            '$addFields': {
              'totalShop': {
                '$cond': {
                  'if': {
                    '$eq': [
                      {
                        '$size': '$shopreview'
                      }, 0
                    ]
                  },
                  'then': 0,
                  'else': {
                    '$ceil': {
                      '$divide': [
                        '$shop_count', {
                          '$size': '$shopreview'
                        }
                      ]
                    }
                  }
                }
              }
            }
          },

          {
            '$lookup': {
              'from': 'reviews',
              'let': {
                'main': '$shop_id.user_information'
              },
              'pipeline': [
                {
                  '$match': {
                    '$expr': {
                      '$and': [
                        {
                          '$eq': [
                            '$shop_id', '$$main'
                          ]
                        }, {
                          '$eq': [
                            '$user_review', true
                          ]
                        }
                      ]
                    }
                  }
                }
              ],
              'as': 'shopreview'
            }
          },
          {
            '$lookup': {
              'from': 'bookings',
              'let': {
                'id': '$_id',
                'main': '$shop_id.user_information',
                'approximate_date': '$approximate_date',
                'seat': '$seat'
              },
              'pipeline': [
                {
                  '$match': {
                    '$expr': {
                      '$and': [
                        {
                          '$eq': [
                            '$shop_id', '$$main'
                          ]
                        }, {
                          '$eq': [
                            '$seat', '$$seat'
                          ]
                        }, {
                          '$not': [
                            {
                              '$eq': [
                                '$_id', '$$id'
                              ]
                            }
                          ]
                        }, {
                          '$eq': [
                            '$queue', true
                          ]
                        }, {
                          '$eq': [
                            '$test', false
                          ]
                        }, {
                          '$lte': [
                            '$approximate_date', '$$approximate_date'
                          ]
                        }, {
                          '$in': [
                            '$status', [
                              'waiting', 'accepted'
                            ]
                          ]
                        }
                      ]
                    }
                  }
                }
              ],
              'as': 'sbook'
            }
          }, {
            '$addFields': {
              'count': {
                '$reduce': {
                  'input': '$sbook',
                  'initialValue': 0,
                  'in': {
                    '$sum': [
                      '$$value', '$$this.size'
                    ]
                  }
                }
              }
            }
          },
          {
            '$lookup': {
              'from': 'shops',
              'localField': 'created_by',
              'foreignField': 'user_information',
              'as': 'shop'
            }
          }, {
            '$lookup': {
              'from': 'customers',
              'localField': 'created_by',
              'foreignField': '_id',
              'as': 'customer'
            }
          }, {
            '$lookup': {
              'from': 'employees',
              'localField': 'created_by',
              'foreignField': '_id',
              'as': 'employee'
            }
          },
          {
            '$addFields': {
              'created_name': {
                '$arrayElemAt': [
                  {
                    '$cond': {
                      'if': {
                        '$eq': [
                          {
                            '$size': '$shop'
                          }, 1
                        ]
                      },
                      'then': '$shop.shop_name',
                      'else': {
                        '$cond': {
                          'if': {
                            '$eq': [
                              {
                                '$size': '$customer'
                              }, 1
                            ]
                          },
                          'then': '$customer.first_name',
                          'else': {
                            '$cond': {
                              'if': {
                                '$eq': [
                                  {
                                    '$size': '$employee'
                                  }, 1
                                ]
                              },
                              'then': '$employee.name',
                              'else': [
                                ''
                              ]
                            }
                          }
                        }
                      }
                    }
                  }, 0
                ]
              }
            }
          }
        ]).toArray(async (err, item) => {

          if (err) {
            console.log('Watcher payment Err', err)
            return
          }

          if (item.length > 0) {

            io.sockets.in('User' + item[0].user_id._id).emit('customerBooking', item[0])
            io.sockets.in('Shop' + item[0].shop_id.user_information).emit('shopBooking', item[0])

            createSocketLog(app, {
              key: 'customerBooking shopBooking  shopQueueData',
              val: 'Booking',
              user_id: ObjectId(item[0]._id),
              data: { ...item[0] },
              type: true,
              deleted: false,
              status: true,
              createdAt: new Date(),
              updatedAt: new Date()
            }, (status, err, val) => {
              if (err) {
                return
              }
            })

            shopBookingQueueData(app, item[0].shop_id._id, (status, err, val) => {
              if (err) {
                return
              }
              io.sockets.in('global').emit('shopQueueData', { data: item[0].shop_id._id, val })
            })

          }

        })


      }

    })


  })

  db.collection('bookingshorts').watch([
    {
      '$match': {
        'operationType': 'insert',
      }
    }
  ]).on('change', async (data) => {

    db.collection('bookingshorts').aggregate([
      {
        $match: {
          _id: ObjectId(data.documentKey._id)
        }
      },
      {
        '$lookup': {
          'from': 'customers',
          'localField': 'user_id',
          'foreignField': '_id',
          'as': 'user_id'
        }
      },
      {
        '$unwind': {
          'path': '$user_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
    ]).toArray(async (err, item) => {

      if (err) {
        console.log('Watcher seat Err', err)
        return
      }


      if (item.length > 0 && item[0].sms == false) {

        try {

          const msgT = await bookingOTPMsg({ service_id: { name: 'Payment Link' }, shop_name: item[0]?.shop_id?.shop_name, otp: config.has('PAYMENT_URL_KEY') ? config.get('PAYMENT_URL_KEY') + 'payment/short/' + item[0]?.short_id : PAYMENT_URL_KEY + 'payment/short/' + item[0]?.short_id, bookingid: 1, queue: true })
          const smsToken = jwt.sign({ date: new Date(), message: msgT, id: ServiceTemplateId.otp, type: 'Payment', bookingid: item[0].booking_id, user_id: [item[0].user_id._id] }, MAIL_SECRET_KEY)

          if (item[0]?.user_id.mobile == null || item[0]?.user_id.mobile == undefined) {

            const emailOtp = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'sendOtpMail' : ServiceSmsMail + 'sendOtpMail', {
              email: item[0]?.user_id.email,
              otp: config.has('PAYMENT_URL_KEY') ? config.get('PAYMENT_URL_KEY') + 'payment/short/' + item[0]?.short_id : PAYMENT_URL_KEY + 'payment/short/' + item[0]?.short_id
            }, {
              headers: {
                'auth-token': smsToken
              }
            })



            console.log('Otp Sent', emailOtp)

          } else {
            const mobileOtp = await axios.post(config.has('PAYMENT_URL_KEY') ? config.get('PAYMENT_URL_KEY') + 'smsSend' : PAYMENT_URL_KEY + 'smsSend', {
              number: item[0]?.user_id.mobile,
              otp: config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'payment/short/' + item[0]?.short_id : ServiceSmsMail + 'payment/short/' + item[0]?.short_id
            }, {
              headers: {
                'auth-token': smsToken
              }
            })


            console.log('Otp Sent', mobileOtp)

          }

        } catch (err) {
          console.log('Watcher Otp Send Err', err)
        }
      }


    })


  })

}

module.exports = watcher