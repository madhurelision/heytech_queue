const express = require('express');
const { DATABASE_URL, Socket_Port } = require('./config/key');
const { MongoClient } = require('mongodb');
const socketService = require('./socket/socket');
const http = require('http');
const { shopQueueAndBooking } = require('./config/function');
process.env.TZ = "Asia/Calcutta";

const app = express()
const httpServer = new http.Server(app);

const connectDB = async (app) => {
  const client = new MongoClient(DATABASE_URL, {
    useNewUrlParser: true, useUnifiedTopology: true
  })

  try {
    await client.connect();
    app.set('client', client);
    console.log('MongoDB Connected :D');
    socketService(httpServer, app)

    app.get('/check', async (req, res, next) => {
      var db = req.app.get('client').db("heytechQueue");

      db.collection('shops').createIndex({ first_name: 'text', last_name: 'text', expertise: 'text' }).then((cc) => {
        res.json(cc)
      }).catch((err) => {
        console.log('Create Index Err', err)
        res.json(err)
      })
      // console.log('index', index)
      // shopQueueAndBooking(req.app, '61ceeb8e3cdb44c16d61fc52', (status, err, data) => {

      //   if (err) {
      //     return res.json(err)
      //   }

      //   res.json(data)

      // })


    })

  } catch (err) {
    if (err instanceof Error) console.error(err.message);
    process.exit(1);
  }
};

connectDB(app)

httpServer.listen(Socket_Port, () => {
  console.log(`Server is running ${Socket_Port}`)
})