const { ObjectId } = require('mongodb');
const axios = require('axios')
const moment = require('moment');
const { PROD } = require('./key');
const config = require('config')


// count user Queue and Booking
const userQueueAndBooking = (app, id, cb) => {
  var db = app.get('client').db("heytechQueue");

  const promise1 = new Promise((resolve, reject) => {
    db.collection('bookings').countDocuments({
      user_id: ObjectId(id),
      queue: true,
      test: false,
      otp_verify: true,
      appointment_date: {
        $gte: new Date(new Date().setHours(00, 00, 00)),
        $lt: new Date(new Date().setHours(23, 59, 59))
      },
      status: { $in: ['waiting', 'accepted'] },
      approximate_date: { $gte: new Date() }
    }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise2 = new Promise((resolve, reject) => {
    db.collection('bookings').countDocuments({
      user_id: ObjectId(id),
      queue: false,
      test: false,
      otp_verify: true,
      appointment_date: { $gte: new Date(new Date().setHours(00, 00, 00)) },
      status: { $in: ['waiting', 'accepted'] },
      end_time: { $gte: new Date() }
    }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  Promise.allSettled([promise1, promise2]).then((cc) => {

    cb(200, null, {
      customerBookingCount: cc[0].value,
      customerQueueCount: cc[1].value,
      customerTotalCount: cc[0].value + cc[1].value
    })

  }).catch((err) => {
    cb(400, err, null)
  })
}

// count shop Queue and Booking
const shopQueueAndBooking = (app, id, cb) => {
  var db = app.get('client').db("heytechQueue");

  const promise1 = new Promise((resolve, reject) => {
    db.collection('bookings').countDocuments({
      shop_id: ObjectId(id),
      queue: true,
      test: false,
      otp_verify: true,
      appointment_date: {
        $gte: new Date(new Date().setHours(00, 00, 00)),
        $lt: new Date(new Date().setHours(23, 59, 59))
      },
      status: { $in: ['waiting', 'accepted'] },
      approximate_date: { $gte: new Date() }
    }).then((item) => {
      resolve(item)
    }).catch((err) => {
      console.log('With queuess', err)
      reject(err)
    })
  })

  const promise2 = new Promise((resolve, reject) => {
    db.collection('bookings').countDocuments({
      shop_id: ObjectId(id),
      queue: false,
      test: false,
      otp_verify: true,
      appointment_date: { $gte: new Date(new Date().setHours(00, 00, 00)) },
      status: { $in: ['waiting', 'accepted'] },
      end_time: { $gte: new Date() }
    }).then((item) => {
      resolve(item)
    }).catch((err) => {
      console.log('without queue ss', err)
      reject(err)
    })
  })

  Promise.allSettled([promise1, promise2]).then((cc) => {
    console.log('ss', cc)
    cb(200, null, {
      shopQueueCount: cc[0].value,
      shopBookingCount: cc[1].value,
    })

  }).catch((err) => {
    cb(400, err, null)
  })

}

// shop Queue Counts
const getShopCurrenQueue = (app, id, cb) => {

  var db = app.get('client').db("heytechQueue");

  db.collection('shops').aggregate([
    {
      '$match': {
        '_id': ObjectId(id)
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    },
    {
      '$unwind': {
        'path': '$topics'
      }
    },
    {
      '$unwind': {
        'path': '$topics.seat'
      }
    },
    {
      '$lookup': {
        'from': 'bookings',
        'let': {
          'main': '$user_information',
          'service': '$topics._id',
          'seat': '$topics.seat',
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$seat', '$$seat'
                    ]
                  },
                  {
                    '$eq': [
                      '$queue', true
                    ]
                  },
                  {
                    '$eq': [
                      '$test', false
                    ]
                  },
                  {
                    '$gte': [
                      '$approximate_date', new Date()
                    ]
                  },
                  {
                    $in: ['$status', ['waiting', 'accepted']]
                  },
                  {
                    '$eq': ['$otp_verify', true]
                  }
                ]
              }
            }
          }
        ],
        'as': 'booking'
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_information',
          'topics': '$topics._id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }, {
                    '$eq': [
                      '$service_id', '$$topics'
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },
    {
      '$project': {
        '_id': '$topics._id',
        'seat': '$topics.seat',
        'count': {
          '$reduce': {
            'input': '$booking',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.size'
              ]
            }
          }
        },
        'review': {
          '$size': '$review'
        }
      }
    }
    // {
    //   '$group': {
    //     '_id': '$topics._id',
    //     'count': {
    //       '$sum': {
    //         '$add': [
    //           {
    //             '$reduce': {
    //               'input': '$booking',
    //               'initialValue': 0,
    //               'in': {
    //                 '$sum': [
    //                   '$$value', '$$this.size'
    //                 ]
    //               }
    //             }
    //           }, 0
    //         ]
    //       }
    //     },
    //     'review': {
    //       '$first': {
    //         '$size': '$review'
    //       }
    //     }
    //   }
    // }
  ]).toArray((err, item) => {

    if (err) {
      return cb(400, err, null)
    }

    cb(200, null, item)
  })
}

const BookingWaitingTime = (app, id, cb) => {
  console.log('itemS', id)
  var db = app.get('client').db("heytechQueue");

  db.collection('shops').aggregate([
    {
      '$match': {
        '_id': ObjectId(id)
      }
    }, {
      '$lookup': {
        'from': 'bookings',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  },
                  {
                    '$eq': [
                      '$queue', true
                    ]
                  },
                  {
                    '$eq': [
                      '$test', false
                    ]
                  },
                  {
                    '$gte': [
                      '$approximate_date', new Date()
                    ]
                  }, {
                    '$in': [
                      '$status', [
                        'waiting', 'accepted'
                      ]
                    ]
                  },
                  {
                    '$eq': ['$otp_verify', true]
                  }
                ]
              }
            }
          }
        ],
        'as': 'booking'
      }
    }, {
      '$unwind': {
        'path': '$booking'
      }
    }, {
      '$sort': {
        'booking._id': -1
      }
    }, {
      '$limit': 1
    }, {
      '$project': {
        '_id': 1,
        'approximate_date': '$booking.approximate_date'
      }
    }
  ]).toArray((err, item) => {

    if (err) {
      return cb(400, err, null)
    }

    console.log('Item', item)

    cb(200, null, item.length > 0 ? minusDate(item[0].approximate_date, new Date()) : "0 min")
  })

}

const createNotification = (app, item, cb) => {
  var db = app.get('client').db("heytechQueue");

  db.collection('notifications').insertOne({
    user_id: item.user_id._id,
    shop_id: item.shop_id.user_information,
    booking_id: item._id,
    name: item.user_id.name,
    service_name: item.service_id.name,
    status: item.status,
    image: item.user_id.image_link,
    shop_name: item.shop_id.shop_name,
    shop_image: item.shop_id.image_link,
    message: `Dear ${item.user_id.name} your booking for the service ${item.service_id.name} has been ${item.status}`,
    view: false,
    type: 'booking',
    shop_view: false,
    like: false,
    createdAt: new Date(),
    updatedAt: new Date()
  }).then((cc) => {
    cb(200, null, cc)
  }).catch((err) => {
    cb(400, err, null)
  })

}

const countNotification = (app, id, cb) => {
  var db = app.get('client').db("heytechQueue");

  const promise1 = new Promise((resolve, reject) => {
    db.collection('notifications').countDocuments({
      user_id: ObjectId(id),
      view: false,
      like: false
    }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise2 = new Promise((resolve, reject) => {
    db.collection('notifications').aggregate([
      {
        $match: {
          user_id: ObjectId(id),
          like: false
        }
      }, {
        '$facet': {
          'metadata': [
            {
              '$group': {
                '_id': null,
                'total': {
                  '$sum': 1
                }
              }
            }
          ],
          'notification': [
            {
              '$sort': {
                '_id': -1
              }
            }, {
              '$skip': 0
            }, {
              '$limit': 20
            }
          ]
        }
      }, {
        '$project': {
          'notification': 1,
          'total': {
            '$arrayElemAt': [
              '$metadata.total', 0
            ]
          }
        }
      }
    ]).toArray((err, item) => {
      if (err) {
        reject(err)
        return
      }
      resolve((item.length > 0) ? item[0] : {
        notification: [],
        total: 0
      })
    })
  })

  Promise.allSettled([promise1, promise2]).then((item) => {


    cb(200, null, {
      customerNotificationCount: item[0].value,
      customerNotificationTotal: item[1].value.total,
      customerNotification: item[1].value.notification
    })

  }).catch((err) => {

    cb(400, err, null)
  })


}

const countShopNotification = (app, id, cb) => {
  var db = app.get('client').db("heytechQueue");

  const promise1 = new Promise((resolve, reject) => {
    db.collection('notifications').countDocuments({
      shop_id: ObjectId(id),
      shop_view: false
    }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise2 = new Promise((resolve, reject) => {
    db.collection('notifications').aggregate([
      {
        $match: {
          shop_id: ObjectId(id)
        }
      },
      {
        '$facet': {
          'metadata': [
            {
              '$group': {
                '_id': null,
                'total': {
                  '$sum': 1
                }
              }
            }
          ],
          'notification': [
            {
              '$sort': {
                '_id': -1
              }
            }, {
              '$skip': 0
            }, {
              '$limit': 20
            }
          ]
        }
      }, {
        '$project': {
          'notification': 1,
          'total': {
            '$arrayElemAt': [
              '$metadata.total', 0
            ]
          }
        }
      }
    ]).toArray((err, item) => {
      if (err) {
        reject(err)
        return
      }
      resolve((item.length > 0) ? item[0] : {
        notification: [],
        total: 0
      })
    })
  })

  Promise.allSettled([promise1, promise2]).then((item) => {


    cb(200, null, {
      shopNotificationCount: item[0].value,
      shopNotificationTotal: item[1].value.total,
      shopNotification: item[1].value.notification
    })

  }).catch((err) => {

    cb(400, err, null)
  })



}

// count user Queue and Booking
const employeeQueueAndBooking = (app, id, cb) => {
  var db = app.get('client').db("heytechQueue");

  const promise1 = new Promise((resolve, reject) => {
    db.collection('bookings').countDocuments({
      assign: ObjectId(id),
      queue: true,
      test: false,
      otp_verify: true,
      appointment_date: {
        $gte: new Date(new Date().setHours(00, 00, 00)),
        $lt: new Date(new Date().setHours(23, 59, 59))
      },
      status: { $in: ['waiting', 'accepted'] },
      approximate_date: { $gte: new Date() }
    }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise2 = new Promise((resolve, reject) => {
    db.collection('bookings').countDocuments({
      assign: ObjectId(id),
      queue: false,
      test: false,
      otp_verify: true,
      appointment_date: { $gte: new Date(new Date().setHours(00, 00, 00)) },
      status: { $in: ['waiting', 'accepted'] },
      end_time: { $gte: new Date() }
    }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise3 = new Promise((resolve, reject) => {
    db.collection('bookings').countDocuments({
      created_by: ObjectId(id),
      queue: true,
      test: false,
      otp_verify: true,
      appointment_date: {
        $gte: new Date(new Date().setHours(00, 00, 00)),
        $lt: new Date(new Date().setHours(23, 59, 59))
      },
      status: { $in: ['waiting', 'accepted'] },
      approximate_date: { $gte: new Date() }
    }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise4 = new Promise((resolve, reject) => {
    db.collection('bookings').countDocuments({
      created_by: ObjectId(id),
      queue: false,
      test: false,
      otp_verify: true,
      appointment_date: { $gte: new Date(new Date().setHours(00, 00, 00)) },
      status: { $in: ['waiting', 'accepted'] },
      end_time: { $gte: new Date() }
    }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  Promise.allSettled([promise1, promise2, promise3, promise4]).then((cc) => {
    //console.log('employee Socket Count', cc)

    cb(200, null, {
      employeeBookingCount: cc[0].value,
      employeeQueueCount: cc[1].value,
      employeeTotalCount: cc[0].value + cc[1].value,
      employeeBookingTotalCount: cc[2].value + cc[3].value
    })

  }).catch((err) => {
    cb(400, err, null)
  })
}

const BookingMessage = async (item) => {
  // return `Dear Mr. ${item.user_id.first_name} your Booking for the ${item.service_id.name} service for ${item.description} has been Successfully Booked`
  return "Successfully"
}

const adminCount = (app, id, cb) => {
  var db = app.get('client').db("heytechQueue");

  const promise1 = new Promise((resolve, reject) => {
    db.collection('bookings').countDocuments({
      queue: true,
      test: false,
      otp_verify: true,
      appointment_date: {
        $gte: new Date(new Date().setHours(00, 00, 00)),
        $lt: new Date(new Date().setHours(23, 59, 59))
      },
      status: { $in: ['waiting', 'accepted'] },
      approximate_date: { $gte: new Date() }
    }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise2 = new Promise((resolve, reject) => {
    db.collection('bookings').countDocuments({
      queue: false,
      test: false,
      otp_verify: true,
      appointment_date: { $gte: new Date(new Date().setHours(00, 00, 00)) },
      status: { $in: ['waiting', 'accepted'] },
      end_time: { $gte: new Date() }
    }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise3 = new Promise((resolve, reject) => {
    db.collection('bookings').countDocuments({}).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise4 = new Promise((resolve, reject) => {
    db.collection('shops').countDocuments({}).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise5 = new Promise((resolve, reject) => {
    db.collection('customers').countDocuments({}).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise6 = new Promise((resolve, reject) => {
    db.collection('servicerequests').countDocuments({ type: 'sms' }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise7 = new Promise((resolve, reject) => {
    db.collection('servicerequests').countDocuments({ type: 'email' }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise8 = new Promise((resolve, reject) => {
    db.collection('locationlogs').countDocuments({}).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise9 = new Promise((resolve, reject) => {
    db.collection('sockets').countDocuments({ status: true }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  Promise.allSettled([promise1, promise2, promise3, promise4, promise5, promise6, promise7, promise8, promise9]).then((cc) => {

    cb(200, null, {
      adminQueueCount: cc[0].value,
      adminAppointmentCount: cc[1].value,
      adminBookingCount: cc[2].value,
      adminShopCount: cc[3].value,
      adminCustomerCount: cc[4].value,
      adminSmsCount: cc[5].value,
      adminEmailCount: cc[6].value,
      adminLocationCount: cc[7].value,
      adminSocketCount: cc[8].value,
    })

  }).catch((err) => {
    cb(400, err, null)
  })

}

const getAllExpertise = (app, cb) => {
  var db = app.get('client').db("heytechQueue");

  db.collection('shops').aggregate([
    {
      '$match': {
        'status': true
      }
    },
    {
      '$unwind': {
        'path': '$expertise'
      }
    }, {
      '$group': {
        '_id': 'null',
        'data': {
          '$push': '$expertise'
        }
      }
    }, {
      '$addFields': {
        'data': {
          '$map': {
            'input': '$data',
            'as': 'num',
            'in': {
              'label': '$$num',
              'value': '$$num'
            }
          }
        }
      }
    }
  ]).toArray((err, item) => {

    if (err) {
      return cb(400, err, null)
    }
    cb(200, err, (item.length > 0) ? item[0].data : [])
  })

}

const getAllActiveService = (app, cb) => {
  var db = app.get('client').db("heytechQueue");

  db.collection('shops').aggregate([

    {
      '$match': {
        'status': true
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    }, {
      '$unwind': {
        'path': '$topics'
      }
    }, {
      '$addFields': {
        'topics.user_name': '$shop_name'
      }
    }, {
      '$group': {
        '_id': null,
        'data': {
          '$push': '$topics'
        }
      }
    }
  ]).toArray((err, item) => {

    if (err) {
      return cb(400, err, null)
    }
    cb(200, err, (item.length > 0) ? item[0].data : [])
  })

}

const getShopLocation = async (app, location, id, cb) => {
  var db = app.get('client').db("heytechQueue");
  const shopData = await db.collection('shops').findOne({ _id: ObjectId(id) })
  if (shopData == null || shopData == undefined) {
    return cb(400, shopData, null)
  }

  if (shopData.lattitude == null || shopData.lattitude == undefined || shopData.longitude == null || shopData.longitude == undefined) {
    return cb(400, shopData, null)
  }

  var config = {
    method: 'get',
    url: `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${location.lattitude}%2C${location.longitude}&destinations=${shopData.lattitude}%2C${shopData.longitude}&key=AIzaSyCqiCKV0cm-EpHp2hev4XkD5wG9JGxFbVs`,
    headers: {}
  };

  axios(config)
    .then(async (cc) => {
      console.log('fetch Data', cc.data.rows[0].elements[0])
      const LocationLog = await db.collection('locationlogs').insertOne({
        ...config,
        data: {
          ...location
        },
        message: { ...cc.data },
        shop_id: ObjectId(id),
        type: 'single',
        deleted: false,
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
      })
      console.log('save Reques', LocationLog)
      cb(200, null, {
        distance: cc?.data?.rows[0]?.elements[0]?.distance?.text,
        distanceVal: cc?.data?.rows[0]?.elements[0]?.distance?.value,
        duration: cc?.data?.rows[0]?.elements[0]?.duration?.text,
        durationVal: cc?.data?.rows[0]?.elements[0]?.duration?.value,
      })
    }).catch(async (err) => {
      console.log('Location Find Err', err)
      const LLT = await db.collection('locationlogs').insertOne({
        ...config,
        data: {
          ...location
        },
        message: { ...err },
        shop_id: ObjectId(id),
        type: 'single',
        deleted: false,
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
      })
      console.log('save Reques', LLT)
      cb(400, err, null)
    })

}

const getShopOfferQueue = (app, cb) => {

  var db = app.get('client').db("heytechQueue");

  db.collection('shops').aggregate([
    {
      '$match': {
        'status': true
      }
    }, {
      '$lookup': {
        'from': 'offers',
        'let': {
          'main': '$topics'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$in': [
                      '$service_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$status', true
                    ]
                  }, {
                    '$eq': [
                      '$deleted', false
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'string'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    },
    {
      '$unwind': {
        'path': '$string'
      }
    },
    {
      '$lookup': {
        'from': 'services',
        'localField': 'string.service_id',
        'foreignField': '_id',
        'as': 'string.service_id'
      }
    },
    {
      '$unwind': {
        'path': '$string.service_id'
      }
    },
    {
      '$unwind': {
        'path': '$string.service_id.seat'
      }
    }, {
      '$lookup': {
        'from': 'bookings',
        'let': {
          'main': '$user_information',
          'service': '$string.service_id._id',
          'seat': '$string.service_id.seat',
        },
        'pipeline': [
          {
            '$sort': {
              '_id': -1
            }
          },
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  },
                  {
                    '$eq': [
                      '$seat', '$$seat'
                    ]
                  },
                  {
                    '$eq': [
                      '$queue', true
                    ]
                  },
                  {
                    '$eq': [
                      '$test', false
                    ]
                  },
                  {
                    '$gte': [
                      '$approximate_date', new Date()
                    ]
                  }, {
                    '$in': [
                      '$status', [
                        'waiting', 'accepted'
                      ]
                    ]
                  }, {
                    '$eq': [
                      '$otp_verify', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'booking'
      }
    }, {
      '$addFields': {
        'time': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$booking'
                    }, 1
                  ]
                },
                'then': '$booking.approximate_date',
                'else': null
              }
            }, 0
          ]
        }
      }
    }, {
      '$project': {
        '_id': '$string.service_id._id',
        'key': '$string._id',
        'shop_id': '$_id',
        'time': '$time',
        'seat': '$string.service_id.seat',
        'count': {
          '$reduce': {
            'input': '$booking',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.size'
              ]
            }
          }
        }
      }
    }
  ]).toArray((err, item) => {

    if (err) {
      return cb(400, err, null)
    }

    const data = item.map((cc) => {
      return { ...cc, time: (cc.time == null || cc.time == undefined) ? "0 min" : minusDate(cc.time, new Date()) }
    })

    cb(200, null, data)
  })
}

const getAllShopLocation = async (app, location, cb) => {

  var db = app.get('client').db("heytechQueue");

  const shopD = await db.collection('shops').aggregate([
    {
      '$match': {
        'status': true,
        'longitude': {
          '$nin': [
            null
          ]
        }
      }
    }, {
      '$group': {
        '_id': '$_id',
        'location': {
          '$first': '$location'
        },
        'lattitude': {
          '$first': '$lattitude'
        },
        'longitude': {
          '$first': '$longitude'
        }
      }
    }
  ]).toArray()

  if (shopD == null || shopD == undefined || shopD.length == 0) {
    return cb(400, shopD, null)
  }

  const locationData = shopD.reduce(function (author, val, index) {
    var comma = author.length ? "%7C" : "";
    return author + comma + val.lattitude + "%2C" + val.longitude;
  }, '');

  //console.log(locationData);

  var config = {
    method: 'get',
    url: `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${location.lattitude}%2C${location.longitude}&destinations=${locationData}&key=AIzaSyCqiCKV0cm-EpHp2hev4XkD5wG9JGxFbVs`,
    headers: {}
  };

  axios(config)
    .then(async (cc) => {
      //console.log('fetch Data', cc.data.rows[0].elements[0])
      const LocationLog = await db.collection('locationlogs').insertOne({
        ...config,
        data: {
          ...location
        },
        message: { ...cc.data },
        shop_id: shopD.map((cc) => ObjectId(cc._id)),
        type: 'multiple',
        deleted: false,
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
      })
      //console.log('save Reques', LocationLog)
      cb(200, null, cc.data.rows[0].elements.map((cc, i) => {
        return {
          _id: shopD[i]._id,
          distance: cc?.distance?.text,
          distanceVal: cc?.distance?.value,
          duration: cc?.duration?.text,
          durationVal: cc?.duration?.value,
        }
      }))
    }).catch(async (err) => {
      console.log('Location Find Err', err)
      const LLT = await db.collection('locationlogs').insertOne({
        ...config,
        data: {
          ...location
        },
        message: { ...err },
        shop_id: shopD.map((cc) => ObjectId(cc._id)),
        type: 'multiple',
        deleted: false,
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
      })
      console.log('save Reques', LLT)
      cb(400, err, null)
    })

}

const getBookingLocation = async (app, location, id, cb) => {
  var db = app.get('client').db("heytechQueue");
  const bookingData = await db.collection('bookings').aggregate([
    {
      $match: {
        _id: ObjectId(id)
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    },
    {
      '$unwind': {
        'path': '$shop_id'
      }
    },
  ]).toArray()

  if (bookingData == null || bookingData == undefined || bookingData.length == 0) {
    return cb(400, bookingData, null)
  }

  if (bookingData[0].status == 'completed' || bookingData[0].status == 'cancelled' || bookingData[0].queue == false) {
    return cb(400, bookingData, null)
  }

  if (bookingData[0].shop_id.lattitude == null || bookingData[0].shop_id.lattitude == undefined || bookingData[0].shop_id.longitude == null || bookingData[0].shop_id.longitude == undefined) {
    return cb(400, bookingData, null)
  }

  var config = {
    method: 'get',
    url: `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${location.lattitude}%2C${location.longitude}&destinations=${bookingData[0].shop_id.lattitude}%2C${bookingData[0].shop_id.longitude}&key=AIzaSyCqiCKV0cm-EpHp2hev4XkD5wG9JGxFbVs`,
    headers: {}
  };

  axios(config)
    .then(async (cc) => {
      console.log('fetch Data', cc.data.rows[0].elements[0])
      const LocationLog = await db.collection('locationlogs').insertOne({
        ...config,
        data: {
          ...location
        },
        message: { ...cc.data },
        shop_id: ObjectId(bookingData[0].shop_id._id),
        type: 'single',
        deleted: false,
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
      })
      console.log('save Reques', LocationLog)
      cb(200, null, {
        distance: cc?.data?.rows[0]?.elements[0]?.distance?.text,
        distanceVal: cc?.data?.rows[0]?.elements[0]?.distance?.value,
        duration: cc?.data?.rows[0]?.elements[0]?.duration?.text,
        durationVal: cc?.data?.rows[0]?.elements[0]?.duration?.value,
        address: cc?.data?.origin_addresses[0]
      })
    }).catch(async (err) => {
      console.log('Location Find Err', err)
      const LLT = await db.collection('locationlogs').insertOne({
        ...config,
        data: {
          ...location
        },
        message: { ...err },
        shop_id: ObjectId(bookingData[0].shop_id._id),
        type: 'single',
        deleted: false,
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
      })
      console.log('save Reques', LLT)
      cb(400, err, null)
    })

}

const shopBookingQueueData = async (app, id, cb) => {

  var db = app.get('client').db("heytechQueue");

  const shopD = await db.collection('shops').findOne({ _id: ObjectId(id) })

  //console.log('shopD queue', shopD)

  if (shopD == null || shopD == undefined) {
    return cb(400, shopD, null)
  }

  db.collection('bookings').aggregate([
    {
      '$match': {
        'shop_id': ObjectId(shopD.user_information),
        'queue': true,
        'test': false,
        approximate_date: {
          $gte: new Date(new Date().setHours(00, 00, 00)),
        },
        'status': {
          $in: ['waiting', 'accepted']
        },
        'otp_verify': true
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    },
    {
      '$unwind': {
        'path': '$service_id'
      }
    },
    {
      $sort: {
        'approximate_date': 1
      }
    }
  ]).toArray((err, item) => {

    if (err) {
      return cb(400, err, null)
    }

    cb(200, null, { booking: item })
  })

}

const adminShopBookingQueueData = async (app, id, cb) => {

  var db = app.get('client').db("heytechQueue");

  const shopD = await db.collection('shops').aggregate([
    {
      $match: {
        $or: [
          {
            '_id': ObjectId(id),
          },
          {
            'user_information': ObjectId(id)
          }
        ]
      }
    }
  ]).toArray()

  //console.log('shopD queue', shopD)

  if (shopD == null || shopD == undefined || shopD.length == 0) {
    return cb(400, shopD, null)
  }

  db.collection('bookings').aggregate([
    {
      '$match': {
        'shop_id': ObjectId(shopD[0].user_information),
        'queue': true,
        'test': false,
        appointment_date: {
          $gte: new Date(new Date().setHours(00, 00, 00)),
          $lt: new Date(new Date().setHours(23, 59, 59))
        },
        'status': {
          $in: ['waiting', 'accepted']
        },
        'otp_verify': true
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    },
  ]).toArray((err, item) => {

    if (err) {
      return cb(400, err, null)
    }

    cb(200, null, { booking: item })
  })

}

const getShopQueueCount = async (app, id, cb) => {

  var db = app.get('client').db("heytechQueue");

  db.collection('bookings').aggregate([
    {
      '$match': {
        'shop_id': ObjectId(id),
        'queue': true,
        'test': false,
        appointment_date: {
          $gte: new Date(new Date().setHours(00, 00, 00)),
          $lt: new Date(new Date().setHours(23, 59, 59))
        },
        'status': {
          $in: ['waiting', 'accepted']
        },
        'otp_verify': true
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    },
  ]).toArray((err, item) => {

    if (err) {
      return cb(400, err, null)
    }

    cb(200, null, { booking: item })
  })

}

const getShopTotalCount = (app, id, cb) => {

  var db = app.get('client').db("heytechQueue");

  db.collection('shops').aggregate([
    {
      '$match': {
        user_information: ObjectId(id)
      }
    },
    {
      '$lookup': {
        'from': 'bookings',
        'let': { 'user_information': '$user_information' },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$user_information'
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'booking'
      }
    },
    {
      '$addFields': {
        'appointment': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.queue', false
                ]
              }
            }
          }
        },
        'queue': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.queue', true
                ]
              }
            }
          }
        },
        'total': {
          '$size': '$booking'
        },
        'verify': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.otp_verify', true
                ]
              }
            }
          }
        },
        'notVerify': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.otp_verify', false
                ]
              }
            }
          }
        },
        'completed': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.status', 'completed'
                ]
              }
            }
          }
        },
        'expired': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.status', 'expired'
                ]
              }
            }
          }
        }
      }
    }
  ]).toArray((err, item) => {

    if (err) {
      return cb(400, err, null)
    }

    cb(200, null, { booking: item.length > 0 ? item[0] : {} })
  })
}

const getShopEmployee = (app, cb) => {

  var db = app.get('client').db("heytechQueue");

  db.collection('employees').aggregate([
    {
      '$match': {
        'deleted': false
      }
    },
    // {
    //   '$lookup': {
    //     'from': 'bookings',
    //     'let': {
    //       'main': '$user_id',
    //       'service': '$_id'
    //     },
    //     'pipeline': [
    //       {
    //         '$match': {
    //           '$expr': {
    //             '$and': [
    //               {
    //                 '$eq': [
    //                   '$shop_id', '$$main'
    //                 ]
    //               }, {
    //                 '$eq': [
    //                   '$assign', '$$service'
    //                 ]
    //               }, {
    //                 '$eq': [
    //                   '$queue', true
    //                 ]
    //               }, {
    //                 '$gte': [
    //                   '$approximate_date', new Date('Wed, 13 Apr 2022 00:00:00 GMT')
    //                 ]
    //               }, {
    //                 '$in': [
    //                   '$status', [
    //                     'waiting', 'accepted'
    //                   ]
    //                 ]
    //               }, {
    //                 '$eq': [
    //                   '$otp_verify', true
    //                 ]
    //               }
    //             ]
    //           }
    //         }
    //       }
    //     ],
    //     'as': 'string'
    //   }
    // }, {
    //   '$addFields': {
    //     'count': {
    //       '$size': '$string'
    //     }
    //   }
    // }, {
    //   '$match': {
    //     'count': 0
    //   }
    // }
  ]).toArray((err, item) => {

    if (err) {
      return cb(400, err, null)
    }

    cb(200, null, item.length > 0 ? { employee: item } : { employee: [] })
  })

}

const getShopServiceBookingTime = (app, id, cb) => {

  var db = app.get('client').db("heytechQueue");

  db.collection('shops').aggregate([
    {
      '$match': {
        '_id': ObjectId(id)
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    },
    {
      '$unwind': {
        'path': '$topics'
      }
    },
    {
      '$unwind': {
        'path': '$topics.seat'
      }
    },
    {
      '$lookup': {
        'from': 'bookings',
        'let': {
          'main': '$user_information',
          'service': '$topics._id',
          'seat': '$topics.seat'
        },
        'pipeline': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$seat', '$$seat'
                    ]
                  },
                  {
                    '$eq': [
                      '$queue', true
                    ]
                  },
                  {
                    '$eq': [
                      '$test', false
                    ]
                  },
                  {
                    '$gte': [
                      '$approximate_date', new Date()
                    ]
                  }, {
                    '$in': [
                      '$status', [
                        'waiting', 'accepted'
                      ]
                    ]
                  }, {
                    '$eq': [
                      '$otp_verify', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'booking'
      }
    }, {
      '$addFields': {
        'user_booking': {
          '$arrayElemAt': [
            '$booking.approximate_date', 0
          ]
        }
      }
    }, {
      '$group': {
        '_id': '$topics.seat',
        'data': {
          '$first': '$user_booking'
        }
      }
    }
  ]).toArray((err, item) => {

    if (err) {
      return cb(400, err, null)
    }

    const data = item.map((cc) => {
      return { ...cc, data: (cc.data == null || cc.data == undefined) ? "0 min" : minusDate(cc.data, new Date()), time: (cc.data == null || cc.data == undefined) ? new Date() : new Date(cc.data) }
    })

    cb(200, null, data)
  })

}

const createSocketLog = (app, data, cb) => {

  var db = app.get('client').db("heytechQueue");

  db.collection('socketlogs').insertOne(data).then((cc) => {
    cb(200, null, cc)
  }).catch((err) => {
    cb(400, err, null)
  })

}

const minusDate = (start, end) => {
  var now = new Date(start).getTime();
  var later = new Date(end).getTime();
  var result = (now - later) / 60000;
  //console.log('Res', result, Math.floor(result / 60) + ":" + result % 60 + ':', parseInt(result % 60))
  return (result > 0) ? (result > 60) ? Math.floor(result / 60) + ' ' + 'hr ' + parseInt(result % 60) + ' ' + 'min' : parseInt(result) + ' ' + 'min' : 0 + ' ' + 'min'
}

const bookingOTPMsg = (item) => {
  return `Your OTP for ${item.service_id.name} in ${item.shop_name} is ${item.otp}. Your ${item.queue == false ? 'Appointment' : 'Queue'} Id is #${item.bookingid}.- Team HEYTEC`
}

const confirmationMsg = (item) => {
  return `Thank you for booking ${item.queue == false ? 'Appointment' : 'Queue'} for ${item.service_id.name} in ${item.shop_name} your ${item.queue == false ? 'Appointment' : 'Queue'} Id is #${item.bookingid}, there are ${item.countIcon} users in the ${item.queue == false ? 'Appointment' : 'Queue'} and the total waiting time is ${(item.waiting_number == null || item.waiting_number == undefined || item.waiting_number == "") ? 0 + ' ' + item.waiting_key : (item?.waiting_number < 60) ? item?.waiting_number + ' ' + item.waiting_key : getDateChange(item.waiting_number)} , you have to reach 10 min before the reach time. Your reach time is ${(config.get('PROD') == true && item.queue == true) ? moment(item.end_time).calendar() : moment(item.end_time).calendar()}. Check your status ${item.link} .- Team HEYTEC`
}

const bookingMsg = (item) => {
  return `You have a booking for ${item.service_id.name} in ${item.shop_name} the total number of users is ${item.size}.- Team HEYTEC`
}

const cancelMsg = (item) => {
  return `Your booking for ${item.service_id.name} in ${item.shop_name} with ${item.queue == false ? 'Appointment' : 'Queue'} Id is #${item.bookingid} is canceled. - Team HEYTEC`
}

const getDateChange = (num) => {
  var hours = Math.floor(num / 60);
  var minutes = num % 60;
  return (hours > 0) ? hours + ' ' + 'hr' + ' ' + parseInt(minutes) + ' ' + 'min' : parseInt(minutes) + ' ' + 'min'
}


const getAllCustomerLocationAdd = async (app, location, id, cb) => {

  var db = app.get('client').db("heytechQueue");

  const shopD = await db.collection('bookings').aggregate([
    {
      '$match': {
        'user_id': ObjectId(id),
        'end_time': {
          '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0))
        },
        'status': {
          '$in': [
            'waiting', 'accepted'
          ]
        },
        'otp_verify': true,
        'test': false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    }, {
      '$group': {
        '_id': '$_id',
        'shop_id': {
          '$first': '$shop_id.user_information'
        },
        'location': {
          '$first': '$shop_id.location'
        },
        'lattitude': {
          '$first': '$shop_id.lattitude'
        },
        'longitude': {
          '$first': '$shop_id.longitude'
        }
      }
    }
  ]).toArray()

  if (shopD == null || shopD == undefined || shopD.length == 0) {
    return cb(400, shopD, null)
  }


  const locationData = shopD.reduce(function (author, val, index) {
    var comma = author.length ? "%7C" : "";
    return author + comma + val.lattitude + "%2C" + val.longitude;
  }, '');

  //console.log(locationData);

  var config = {
    method: 'get',
    url: `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${location.lattitude}%2C${location.longitude}&destinations=${locationData}&key=AIzaSyCqiCKV0cm-EpHp2hev4XkD5wG9JGxFbVs`,
    headers: {}
  };

  axios(config)
    .then(async (cc) => {
      //console.log('fetch Data', cc.data.rows[0].elements[0])
      const LocationLog = await db.collection('locationlogs').insertOne({
        ...config,
        data: {
          ...location
        },
        message: { ...cc.data },
        shop_id: shopD.map((cc) => ObjectId(cc.shop_id)),
        type: 'multiple',
        deleted: false,
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
      })

      Promise.all(cc.data.rows[0].elements.map(async (cc, i) => {

        const hell = await db.collection('travels').findOneAndUpdate({ booking_id: ObjectId(shopD[i]._id) }, {
          $set: {
            distance: cc?.distance?.text,
            distanceVal: cc?.distance?.value,
            duration: cc?.duration?.text,
            durationVal: cc?.duration?.value,
            ...location,
            updatedAt: new Date()
          }
        })
        return { data: hell, booking_id: shopD[i]._id }
      })).then((cc) => {
        cb(200, null, cc)
      }).catch((err) => {
        console.log('Primisee Err', err)
        cb(400, err, null)
      })

    }).catch(async (err) => {
      console.log('Location Find Err', err)
      const LLT = await db.collection('locationlogs').insertOne({
        ...config,
        data: {
          ...location
        },
        message: { ...err },
        shop_id: shopD.map((cc) => ObjectId(cc.shop_id)),
        type: 'multiple',
        deleted: false,
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
      })
      console.log('save Reques', LLT)
      cb(400, err, null)
    })

}

module.exports = {
  userQueueAndBooking,
  shopQueueAndBooking,
  getShopCurrenQueue,
  BookingWaitingTime,
  createNotification,
  countNotification,
  BookingMessage,
  countShopNotification,
  employeeQueueAndBooking,
  adminCount,
  bookingMsg,
  bookingOTPMsg,
  confirmationMsg,
  cancelMsg,
  getAllExpertise,
  getAllActiveService,
  getShopLocation,
  getShopOfferQueue,
  getAllShopLocation,
  getBookingLocation,
  shopBookingQueueData,
  getShopEmployee,
  getShopServiceBookingTime,
  createSocketLog,
  adminShopBookingQueueData,
  getShopQueueCount,
  getAllCustomerLocationAdd,
  getShopTotalCount
}