const dotenv = require('dotenv')
const moment = require('moment-timezone')
dotenv.config()

const BASE_URL = 'http://localhost:';
const C_PORT = 3000;
const S_PORT = 5000;
const SERVER_URL = BASE_URL + S_PORT;
//const SERVER_URL = 'https://heybarberserver.herokuapp.com'

const Socket_Port = parseInt(process.env.PORT) || 5002;
const DATABASE_URL = 'mongodb+srv://heycalls:madhur%404994%40heycalls@cluster0.fuagf.mongodb.net/heytechQueue?retryWrites=true';
const ServiceSmsMail = "https://queuickses.herokuapp.com/heytech/api/";
//const ServiceSmsMail = "http://localhost:5003/heytech/api/";
const ServiceTemplateId = {
  book: '1701164603567987143',
  otp: '1701164603567987143',
  confirm: '1701164603567987143',
  cancel: '1701164603567987143'
}
const PROD = JSON.parse(process.env.PROD || 'false');

const SECRET_KEY = "heytech";
const MAIL_SECRET_KEY = "Mailheytech";
const CLIENT_URL = BASE_URL + C_PORT;
//const CLIENT_URL = 'https://heybarber.herokuapp.com';

const SHORT_URL_KEY = 'https://vbn.link/';
//const SHORT_URL_KEY = 'http://localhost:500/';

const PAYMENT_URL_KEY = 'https://queuick-payment.onrender.com/';
//const PAYMENT_URL_KEY = 'http://localhost:5009/';

const DATE_MOMENT = (d) => {
  var tst = moment(d)
  return tst.tz('Asia/Kolkata').calendar()
}

module.exports = {
  SERVER_URL,
  Socket_Port,
  DATABASE_URL,
  SECRET_KEY,
  CLIENT_URL,
  ServiceSmsMail,
  MAIL_SECRET_KEY,
  ServiceTemplateId,
  SHORT_URL_KEY,
  PROD,
  DATE_MOMENT,
  PAYMENT_URL_KEY
}