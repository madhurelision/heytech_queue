const dotenv = require('dotenv')
dotenv.config()

const BASE_URL = 'http://localhost:';
const C_PORT = 3000;
const S_PORT = 5000;
const SERVER_URL = BASE_URL + S_PORT;

const Socket_Port = parseInt(process.env.PORT) || 5002;
const DATABASE_URL = 'mongodb+srv://heycalls:madhur%404994%40heycalls@cluster0.fuagf.mongodb.net/heytechQueue?retryWrites=true';

const ServiceSmsMail = "http://localhost:5003/heytech/api/";
const ServiceTemplateId = {
  book: '1701164603567987143',
  otp: '1701164603567987143',
  confirm: '1701164603567987143',
  cancel: '1701164603567987143'
}

const SECRET_KEY = "heytech";
const MAIL_SECRET_KEY = "Mailheytech";
const CLIENT_URL = BASE_URL + C_PORT;
const SHORT_URL_KEY = 'http://localhost:500/';
const PAYMENT_URL_KEY = 'http://localhost:5009/';
const PROD = false;

module.exports = {
  SERVER_URL,
  Socket_Port,
  DATABASE_URL,
  SECRET_KEY,
  CLIENT_URL,
  ServiceSmsMail,
  MAIL_SECRET_KEY,
  ServiceTemplateId,
  SHORT_URL_KEY,
  PROD,
  PAYMENT_URL_KEY
}