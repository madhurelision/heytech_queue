import { ShopModel } from '../../Models/User';
import { BookingModel } from '../../Models/Booking';
import moment from 'moment';
import mongoose from 'mongoose';
import { TravelModel } from '../../Models/Travel';
import { saveIpDetail } from '../../config/IpDetail';
import { createShortUrl } from '../../modules/ShortUrl';

const Shop = class {
  shop_data: any;
  shop_id: any;

  constructor(id: any) {
    this.shop_data = {}
    this.shop_id = id
  }

  getShop() {
    this.shop_data = ShopModel.findOne({ _id: this.shop_id }).populate('topics').exec()
    return this.shop_data
  }

  async getShopBoth() {
    const shopD = await ShopModel.aggregate([
      {
        $match: {
          $or: [
            {
              '_id': new mongoose.Types.ObjectId(this.shop_id),
            },
            {
              'user_information': new mongoose.Types.ObjectId(this.shop_id)
            }
          ]
        }
      },
      {
        '$lookup': {
          'from': 'services',
          'localField': 'topics',
          'foreignField': '_id',
          'as': 'topics'
        }
      },
    ]).exec()
    this.shop_data = shopD.length > 0 ? shopD[0] : {}
    return this.shop_data
  }

  async getAvailibility() {

    console.log('test Shop Id', this.shop_data)

    const bookData = await BookingModel.find({
      'shop_id': this.shop_data.user_information,
      'queue': true,
      'appointment_date': { $gte: moment().startOf('day'), $lte: moment().endOf('day') },
      'status': { $in: ['waiting', 'accepted'] },
      'approximate_date': { $gte: new Date() },
      'otp_verify': true
    })
    return bookData.length > 0 ? false : true
  }

  async createBooking(service: any) {

    var serviceValue: any = service
    var data: any

    data.shop_id = this.shop_data.user_information
    data.shop_email = this.shop_data.email
    data.shop_name = this.shop_data.shop_name
    data.service_id = ''
    data.user_email = ''
    data.appointment_date = ''
    data.appointment_time = ''
    data.description = ''
    data.size = ''
    data.mobile = ''
    data.time = this.shop_data.waiting_time
    data.location = ''
    data.assign = null
    // data.created_by = ''
    // data.create_type = ''
    data.approximate_time = parseInt(serviceValue.waiting_number) * parseInt(data.size) + ' ' + serviceValue.waiting_key
    data.newtime = (serviceValue.waiting_key == 'min') ? parseInt(serviceValue.waiting_number) * parseInt(data.size) : parseInt(serviceValue.waiting_number) * parseInt(data.size) * 60

    const location: any = (data.location == null || data.location == undefined) ? 0 : (data.location.durationVal == null || data.location.durationVal == undefined) ? 0 : data.location.durationVal
    const query = (location == 0) ? [
      {
        '$match': {
          'shop_id': new mongoose.Types.ObjectId(data.shop_id),
          'queue': true,
          'appointment_date': { '$gte': moment().startOf('day'), $lte: moment().endOf('day') },
          'status': { '$in': ['waiting', 'accepted'] },
          'approximate_date': { '$gte': new Date(data.appointment_date) },
          'otp_verify': true
        }
      },
      {
        '$sort': {
          '_id': -1
        }
      },
      {
        '$limit': 1
      }
    ] : [
      {
        '$match': {
          'shop_id': new mongoose.Types.ObjectId(data.shop_id),
          'queue': true,
          'appointment_date': { '$gte': moment().startOf('day'), $lte: moment().endOf('day') },
          'status': { '$in': ['waiting', 'accepted'] },
          'approximate_date': { '$gte': new Date(data.appointment_date) },
          'otp_verify': true
        }
      },
      {
        '$sort': {
          '_id': -1
        }
      },
      {
        '$lookup': {
          'from': 'travels',
          'localField': '_id',
          'foreignField': 'booking_id',
          'as': 'travel'
        }
      },
      {
        '$addFields': {
          'distance': {
            '$arrayElemAt': [
              '$travel.distance', 0
            ]
          },
          'distanceVal': {
            '$arrayElemAt': [
              '$travel.distanceVal', 0
            ]
          },
          'duration': {
            '$arrayElemAt': [
              '$travel.duration', 0
            ]
          },
          'durationVal': {
            '$arrayElemAt': [
              '$travel.durationVal', 0
            ]
          }
        }
      },
      {
        '$match': {
          'durationVal': {
            '$lte': location
          }
        }
      },
      {
        '$sort': {
          'durationVal': 1
        }
      },
      {
        '$limit': 1
      }
    ]

    const MainCheck: any = await BookingModel.aggregate(query).exec()

    var mainD: any = (MainCheck.length > 0) ? MainCheck[0].approximate_date : createQueueDate(data.appointment_date, parseInt((!data.time || data.time == null || data.time == undefined) ? 0 : data.time))

    console.log('mainD', mainD, createQueueDate(data.appointment_date, parseInt((!data.time || data.time == null || data.time == undefined) ? 0 : data.time)))

    // backend work    
    // data.user_id = val._id
    // data.queue = true
    // data.waiting_time = (MainCheck.length) ? parseInt(minusDate(mainD, data.appointment_date)) + ' ' + 'min' : data.time
    // data.waiting_number = (MainCheck.length) ? parseInt(minusDate(mainD, data.appointment_date)) : parseInt(data.time)
    // data.waiting_key = 'min'
    // data.waiting_date = mainD
    // data.approximate_date = createQueueDate(mainD, parseInt((!data.newtime || data.newtime == null || data.newtime == undefined) ? 0 : data.newtime))
    // data.approximate_time = data.approximate_time
    // data.approximate_number = parseInt(data.approximate_time)
    // data.approximate_key = data.approximate_time.replace(/[0-9]/g, '').replace(' ', '')
    // data.waiting_verify = true
    // data.end_time = mainD
    // data.otp = otpGenerate()
    // data.otp_verify = true
    // data.test = true
    // data.status = (!this.shop_data || this.shop_data == null || this.shop_data == undefined) ? 'waiting' : (this.shop_data.queue == true) ? 'accepted' : 'waiting'
    // data.hash = generateHash(data)

    const BookingData = new BookingModel({ ...data })
    BookingData.save().then(async (item: any) => {
      // const locationD = await saveIpDetail(req, item._id, true)
      // const travelD = await TravelModel.create({ ...data.location, booking_id: item._id })
      //const ShortUrl = await createShortUrl(item._id)
    }).catch((err: Error) => {
    })

  }

}

const createQueueDate = (date: any, time: any) => {
  var now = new Date(date);
  console.log('now', now, date, time)
  now.setMinutes(now.getMinutes() + time);
  now.setMilliseconds(0);
  return now
}

const minusDate = (start: Date, end: Date) => {
  var now = new Date(start).getTime();
  var later = new Date(end).getTime();
  var result: any = (now - later) / 60000;
  return (result > 0) ? result : 0
}

export default Shop