import { Schema, model } from 'mongoose';

const TravelSchema = new Schema({
  duration: { type: String, default: '' },
  durationVal: { type: Number, default: null },
  data: { type: Object },
  booking_id: {
    type: Schema.Types.ObjectId,
    ref: 'Booking',
    default: null,
  },
  address: { type: String, default: '' },
  lattitude: { type: Number, default: null },
  longitude: { type: Number, default: null },
  distance: { type: String, default: '' },
  distanceVal: { type: Number, default: null },
},
  {
    timestamps: true
  });

export const TravelModel = model('Travel', TravelSchema);
