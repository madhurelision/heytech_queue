import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const LoginLogSchema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  user_type: { type: String },
  type: { type: String },
  token: { type: String },
  message: { type: String },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
}, {
  timestamps: true
});

export const LoginLogModel = model('LoginLog', LoginLogSchema);
