import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const GallerySchema = new Schema({
  image: { type: String, index: 'text' },
  data: { type: Object },
  user_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  status: { type: Boolean, default: true }
}, {
  timestamps: true
});

export const GalleryModel = model('Gallery', GallerySchema);
