import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const BookingPaymentSchema = new Schema({
  amount: { type: Number },
  total: { type: Number },
  order_tax: { type: Number },
  discount: { type: Number },
  service_data: { type: Array },
  service_id: {
    type: [Schema.Types.ObjectId],
    ref: 'Service',
    default: [],
  },
  user_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  shop_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  booking_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  type: { type: String },
  pay_id: { type: String },
  payment_id: { type: String },
  payment_method_id: { type: String },
  charge_id: { type: String },
  payment_url: { type: String },
  refunds_url: { type: String },
  payment_method: { type: String, default: 'Stripe' },
  payment_status: { type: String },
  payment_data: { type: Object },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false }
}, {
  timestamps: true
})

export const BookingPaymentModel = model('BookingPayment', BookingPaymentSchema)