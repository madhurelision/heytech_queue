import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const OfferSchema = new Schema({
  name: { type: String },
  value: { type: String },
  color: { type: String },
  expire: { type: Date },
  service_id: {
    type: Schema.Types.ObjectId,
    ref: 'Service',
    default: null,
  },
  admin_id: {
    type: Schema.Types.ObjectId,
    ref: 'Admin',
    default: null,
  },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});


export const OfferModel = model('Offer', OfferSchema);
