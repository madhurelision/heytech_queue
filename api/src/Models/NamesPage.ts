import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const ServiceNameSchema = new Schema({
  name: { type: String, index: 'text' },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
}, {
  timestamps: true
});

const ExpertiseNameSchema = new Schema({
  name: { type: String, index: 'text' },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
}, {
  timestamps: true
});

export const ServiceNameModel = model('ServiceName', ServiceNameSchema);
export const ExpertiseNameModel = model('ExpertiseName', ExpertiseNameSchema);
