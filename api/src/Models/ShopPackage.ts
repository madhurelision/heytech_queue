import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const ShopPackageSchema = new Schema({
  name: { type: String },
  description: { type: String },
  image: { type: String },
  price: { type: Number },
  view: { type: String },
  btn: { type: String },
  user_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
}, {
  timestamps: true
})

const ShopOrderSchema = new Schema({
  name: { type: String },
  email: { type: String },
  mobile: { type: Number },
  product_id: {
    type: Schema.Types.ObjectId,
    ref: 'ShopPackage',
    default: null,
  },
  user_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  shop_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  pay_id: { type: String },
  pay_type: { type: String },
  address_1: { type: String },
  address_2: { type: String },
  city: { type: String },
  pincode: { type: String },
  country: { type: String },
  state: { type: String },
  Amount: { type: Number },
  status: { type: Boolean, default: false },
  deleted: { type: Boolean, default: false },
}, {
  timestamps: true
})

const ShopPaymentSchema = new Schema({
  name: { type: String },
  email: { type: String },
  mobile: { type: Number },
  Amount: { type: Number },
  order_id: {
    type: Schema.Types.ObjectId,
    ref: 'ShopOrder',
    default: null,
  },
  product_id: {
    type: Schema.Types.ObjectId,
    ref: 'ShopPackage',
    default: null,
  },
  user_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  shop_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  pay_id: { type: String },
  payment_id: { type: String },
  payment_method_id: { type: String },
  charge_id: { type: String },
  payment_url: { type: String },
  refunds_url: { type: String },
  payment_method: { type: String, default: 'Stripe' },
  payment_status: { type: String },
  payment_data: { type: Object },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  startDate: { type: Date },
  endDate: { type: Date }
}, {
  timestamps: true
})

const ShopFeatureSchema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  package_id: {
    type: Schema.Types.ObjectId,
    ref: 'ShopPackage',
    default: null,
  },
  service: {
    type: [Schema.Types.ObjectId],
    ref: 'Service',
    default: [],
  },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
}, {
  timestamps: true
})

export const ShopPackageModel = model('ShopPackage', ShopPackageSchema)
export const ShopOrderModel = model('ShopOrder', ShopOrderSchema)
export const ShopPaymentModel = model('ShopPayment', ShopPaymentSchema)
export const ShopFeatureModel = model('ShopFeature', ShopFeatureSchema)