import { Schema, model } from 'mongoose';

const PushNotificationSchema = new Schema({
  endpoint: { type: String },
  expirationTime: { type: String },
  keys: { type: Object },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
},
  {
    timestamps: true
  });

export const PushNotificationModel = model('pushNotification', PushNotificationSchema);
