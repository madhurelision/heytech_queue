import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const HostSchema = new Schema({
  name: { type: String },
  link: { type: String },
  type: { type: Number },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
}, {
  timestamps: true
})

export const AdminHostModel = model('Host', HostSchema)