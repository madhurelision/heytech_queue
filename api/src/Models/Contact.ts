import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const ContactSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  mobile: { type: Number, required: true },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});

const ContactPageSchema = new Schema({
  title: { type: String, default: '' },
  title_small: { type: String, default: '' },
  description: { type: String, default: '' },
  mobile: { type: Number, default: null },
  email: { type: String, default: '' },
  admin_id: {
    type: Schema.Types.ObjectId,
    ref: 'Admin',
    default: null,
  },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});

export const ContactModel = model('Contact', ContactSchema);
export const ContactPageModel = model('ContactPage', ContactPageSchema)
