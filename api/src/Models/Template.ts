import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const TemplateSchema = new Schema({
  title: { type: String, index: 'text' },
  image: { type: String, index: 'text' },
  description: { type: String, index: 'text' },
  url: { type: String, index: 'text' },
  view: { type: String, index: 'text' },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
}, {
  timestamps: true
});

export const TemplateModel = model('Template', TemplateSchema);
