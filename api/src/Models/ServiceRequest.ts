import { Schema, model } from 'mongoose';

const ServiceRequestSchema = new Schema({
  email: { type: String },
  mobile: { type: String },
  data: { type: Object },
  type: { type: String, default: 'sms' },
  resend: { type: String, default: 'single' },
  val: { type: String, default: '' },
  user_id: {
    type: [Schema.Types.ObjectId],
    default: [],
  },
  message: { type: Object },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
},
  {
    timestamps: true
  });

export const ServiceRequestModel = model('ServiceRequest', ServiceRequestSchema);
