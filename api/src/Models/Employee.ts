import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const EmployeeSchema = new Schema({
  name: { type: String, index: 'text' },
  type: { type: String, index: 'text' },
  employee_id: { type: Number },
  mobile: { type: Number },
  create_type: { type: String, default: 'Shop' },
  status: { type: Boolean, default: false },
  deleted: { type: Boolean, default: false },
  user_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  otp: { type: Number },
  otp_verify: { type: Boolean, default: false },
  lastLogin: { type: Date },
  lastLogout: { type: Date },
}, {
  timestamps: true
});

export const EmployeeModel = model('Employee', EmployeeSchema);
