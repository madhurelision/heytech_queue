import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const TagServiceSchema = new Schema({
  name: { type: String },
  tag: { type: Array },
  user_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});

export const TagServiceModel = model('TagService', TagServiceSchema);
