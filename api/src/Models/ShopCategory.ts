import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const ShopCategorySchema = new Schema({
  name: { type: String },
  type: { type: String },
  image: { type: String, index: 'text' },
  user_id: {
    type: Schema.Types.ObjectId,
  },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});

export const ShopCategoryModel = model('ShopCategory', ShopCategorySchema);
