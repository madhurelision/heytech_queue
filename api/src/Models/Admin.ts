import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const AdminSchema = new Schema({
  email: { type: String },
  password: { type: String },
  name: { type: String, default: 'admin' },
  type: { type: String, default: 'main' },
  feature: {
    type: [Schema.Types.ObjectId],
    ref: 'AdminFeature',
    default: [],
  },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
  access: { type: Boolean, default: true },
}, {
  timestamps: true
});

const AdminFeatureSchema = new Schema({
  name: { type: String },
  type: { type: String },
  admin_id: {
    type: Schema.Types.ObjectId,
    ref: 'Admin',
    default: null,
  },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});

export const AdminModel = model('Admin', AdminSchema);
export const FeatureModel = model('AdminFeature', AdminFeatureSchema);
