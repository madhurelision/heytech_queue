import { Schema, model } from 'mongoose';
const mongoose = require('mongoose')
const AutoIncrement = require('mongoose-sequence')(mongoose);

const BookingSchema = new Schema({
  user_email: { type: String, default: '' },
  shop_email: String,
  appointment_time: { type: String, required: true },
  appointment_date: { type: Date, required: true },
  size: { type: Number },
  seat: { type: Number },
  status: {
    type: String,
    enum: ['accepted', 'cancelled', 'waiting', 'completed', 'expired'],
    default: 'waiting',
  },
  description: { type: String, required: true },
  service_id: {
    type: Schema.Types.ObjectId,
    ref: 'Service',
    default: null,
    required: true
  },
  user_id: {
    type: Schema.Types.ObjectId,
    ref: 'customer',
    default: null,
    required: true
  },
  shop_name: String,
  shop_id: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null,
  },
  mobile: { type: Number, default: null },
  start_time: { type: Date },
  end_time: { type: Date },
  deleted: { type: Boolean, default: false },
  otp: { type: Number, default: 123456 },
  otp_verify: { type: Boolean, default: false },
  waiting_time: { type: String, default: '' },
  waiting_number: { type: Number, default: '' },
  waiting_key: { type: String, default: 'min' },
  approximate_time: { type: String, default: '' },
  approximate_number: { type: Number, default: '' },
  approximate_key: { type: String, default: 'min' },
  waiting_verify: { type: Boolean, default: false },
  expired: { type: Boolean, default: false },
  queue: { type: Boolean, default: false },
  hash: { type: String },
  waiting_date: { type: Date },
  approximate_date: { type: Date },
  user_review: { type: Boolean, default: false },
  shop_review: { type: Boolean, default: false },
  cancel_otp: { type: Number },
  cancel_verify: { type: Boolean, default: false },
  created_by: { type: Schema.Types.ObjectId, default: null },
  assign: { type: Schema.Types.ObjectId, default: null },
  remark: { type: String, default: '' },
  create_type: { type: String, default: 'global' },
  shop_status: { type: Boolean },
  shop_message: { type: String, default: '' },
  form_type: { type: String, default: 'Normal' },
  test: { type: Boolean, default: false },
  sms: { type: Boolean, default: false },
},
  {
    timestamps: true
  });

const TestingSchema = new Schema({
  user_email: { type: String, required: true },
  shop_email: String,
  appointment_time: { type: String, required: true },
  appointment_date: { type: Date, required: true },
  size: { type: Number },
  status: {
    type: String,
    enum: ['accepted', 'cancelled', 'waiting', 'completed', 'expired'],
    default: 'waiting',
  },
  description: { type: String, required: true },
  service_id: {
    type: Schema.Types.ObjectId,
    ref: 'Service',
    default: null,
    required: true
  },
  user_id: {
    type: Schema.Types.ObjectId,
    ref: 'customer',
    default: null,
    required: true
  },
  shop_name: String,
  shop_id: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null,
  },
  mobile: { type: Number },
  start_time: { type: Date },
  end_time: { type: Date },
  deleted: { type: Boolean, default: false },
  otp: { type: Number, default: 123456 },
  otp_verify: { type: Boolean, default: false },
  waiting_time: { type: String, default: '' },
  waiting_number: { type: Number, default: '' },
  waiting_key: { type: String, default: '' },
  approximate_time: { type: String, default: '' },
  approximate_number: { type: Number, default: '' },
  approximate_key: { type: String, default: '' },
  waiting_verify: { type: Boolean, default: false },
  expired: { type: Boolean, default: false },
  queue: { type: Boolean, default: false },
  hash: { type: String },
  waiting_date: { type: Date },
  approximate_date: { type: Date },
  user_review: { type: Boolean, default: false },
  shop_review: { type: Boolean, default: false },
  cancel_otp: { type: Number },
  cancel_verify: { type: Boolean, default: false },
  shop_status: { type: Boolean },
  shop_message: { type: String },
},
  {
    timestamps: true
  });


const NewBookingSchema = new Schema({
  user_email: { type: String, default: '' },
  shop_email: String,
  appointment_time: { type: String, required: true },
  appointment_date: { type: Date, required: true },
  size: { type: Number },
  seat: { type: Number },
  status: {
    type: String,
    enum: ['accepted', 'cancelled', 'waiting', 'completed', 'expired'],
    default: 'waiting',
  },
  description: { type: String, required: true },
  service_id: {
    type: Schema.Types.ObjectId,
    ref: 'Service',
    default: null,
    required: true
  },
  user_id: {
    type: Schema.Types.ObjectId,
    ref: 'customer',
    default: null,
    required: true
  },
  shop_name: String,
  shop_id: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null,
  },
  mobile: { type: Number, default: null },
  start_time: { type: Date },
  end_time: { type: Date },
  deleted: { type: Boolean, default: false },
  otp: { type: Number, default: 123456 },
  otp_verify: { type: Boolean, default: false },
  waiting_time: { type: String, default: '' },
  waiting_number: { type: Number, default: '' },
  waiting_key: { type: String, default: 'min' },
  approximate_time: { type: String, default: '' },
  approximate_number: { type: Number, default: '' },
  approximate_key: { type: String, default: 'min' },
  waiting_verify: { type: Boolean, default: false },
  expired: { type: Boolean, default: false },
  queue: { type: Boolean, default: false },
  hash: { type: String },
  waiting_date: { type: Date },
  approximate_date: { type: Date },
  user_review: { type: Boolean, default: false },
  shop_review: { type: Boolean, default: false },
  cancel_otp: { type: Number },
  cancel_verify: { type: Boolean, default: false },
  created_by: { type: Schema.Types.ObjectId, default: null },
  assign: { type: Schema.Types.ObjectId, default: null },
  remark: { type: String, default: '' },
  create_type: { type: String, default: 'global' },
  shop_status: { type: Boolean },
  shop_message: { type: String, default: '' },
  test: { type: Boolean, default: false },
  sms: { type: Boolean, default: false },
},
  {
    timestamps: true
  });

BookingSchema.plugin(AutoIncrement, { inc_field: 'bookingid' })

export const BookingModel = model('Booking', BookingSchema);
export const TestingModel = model('Testing', TestingSchema);
export const NewBookingModel = model('BookingForm', NewBookingSchema);
