import { Schema, model } from 'mongoose';

const OtpSchema = new Schema({
  email: { type: String },
  mobile: { type: String },
  data: { type: Object },
  type: { type: String },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: false },
},
  {
    timestamps: true
  });

export const OtpModel = model('Otp', OtpSchema);
