import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const ServiceSchema = new Schema({
  emojiIcon: { type: String, index: 'text' },
  motivation: { type: String, index: 'text' },
  name: { type: String, index: 'text' },
  image: { type: String, index: 'text' },
  description: { type: String, index: 'text' },
  user_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  tags: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  waiting_number: { type: Number, default: 10 },
  price: { type: Number, default: 100 },
  waiting_key: { type: String, default: 'min' },
  waiting_time: { type: String },
  accept: { type: Boolean, default: true },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  seat: { type: [Number], default: [1] },
}, {
  timestamps: true
});

const ServicesSchema = new Schema({
  emojiIcon: { type: String, index: 'text' },
  motivation: { type: String, index: 'text' },
  name: { type: String, index: 'text' },
  image: { type: String, index: 'text' },
  description: { type: String, index: 'text' },
  user_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  tags: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  waiting_number: { type: Number, default: 10 },
  price: { type: Number, default: 100 },
  waiting_key: { type: String, default: 'min' },
  waiting_time: { type: String },
  accept: { type: Boolean, default: true },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  seat: { type: Array, default: [1] },
}, {
  timestamps: true
});

export const ServiceModel = model('Service', ServiceSchema);
export const TestingServiceModel = model('TestingService', ServicesSchema);
