import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const AdminPageSchema = new Schema({
  name: { type: String, default: '' },
  image: { type: String, default: '' },
  admin_id: {
    type: Schema.Types.ObjectId,
    ref: 'Admin',
    default: null,
  },
  type: { type: Boolean, default: false },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});


export const AdminPageModel = model('AdminPage', AdminPageSchema);
