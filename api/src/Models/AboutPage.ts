import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const AboutPageSchema = new Schema({
  title: { type: String, default: '' },
  description: { type: String, default: '' },
  admin_id: {
    type: Schema.Types.ObjectId,
    ref: 'Admin',
    default: null,
  },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});

export const AboutPageModel = model('AboutPage', AboutPageSchema)
