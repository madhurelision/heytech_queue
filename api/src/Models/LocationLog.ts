import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const LocationLogSchema = new Schema({
  method: { type: String },
  url: { type: String },
  headers: { type: Object },
  data: { type: Object },
  message: { type: Object },
  shop_id: {
    type: [Schema.Types.ObjectId],
    ref: 'shop',
    default: [],
  },
  type: { type: String, default: 'single' },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});


export const LocationLogModel = model('LocationLog', LocationLogSchema);
