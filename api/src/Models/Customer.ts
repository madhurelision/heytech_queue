import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const CustomerSchema = new Schema({
  first_name: { type: String, default: 'c' },
  last_name: { type: String, default: 's' },
  name: { type: String, default: '' },
  mobile: { type: String },
  email: String,
  google: String,
  image_link: String,
  oauth_provider: String,
  location: { type: String, default: '' },
  lattitude: { type: Number, default: null },
  longitude: { type: Number, default: null },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  mobile_verify: { type: Boolean, default: false },
  lastLogin: { type: Date }
}, {
  timestamps: true
});


export const CustomerModel = model('customer', CustomerSchema);
