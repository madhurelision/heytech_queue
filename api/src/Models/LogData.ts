import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const LogSchema = new Schema({
  method: { type: String },
  content: { type: String },
  type: { type: String },
  url: { type: String },
  agent: { type: String },
  ip: { type: String },
  http: { type: String },
  referer: { type: String },
  header: { type: Object },
  body: { type: Object },
  params: { type: Object },
  ipDetail: { type: Object },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});

const MailLogSchema = new Schema({
  method: { type: String },
  content: { type: String },
  type: { type: String },
  url: { type: String },
  agent: { type: String },
  ip: { type: String },
  http: { type: String },
  referer: { type: String },
  header: { type: Object },
  body: { type: Object },
  params: { type: Object },
  ipDetail: { type: Object },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});

const ShortLogSchema = new Schema({
  method: { type: String },
  content: { type: String },
  type: { type: String },
  url: { type: String },
  agent: { type: String },
  ip: { type: String },
  http: { type: String },
  referer: { type: String },
  header: { type: Object },
  body: { type: Object },
  params: { type: Object },
  ipDetail: { type: Object },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});

const SocketLogSchema = new Schema({
  key: { type: String, default: '' },
  val: { type: String, default: '' },
  user_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  body: { type: Object },
  type: { type: Boolean, default: false },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});

const QueryLogSchema = new Schema({
  method: { type: String },
  content: { type: String },
  type: { type: String },
  url: { type: String },
  agent: { type: String },
  ip: { type: String },
  http: { type: String },
  referer: { type: String },
  header: { type: Object },
  startTime: { type: Array },
  endTime: { type: Array },
  endTimeMS: { type: String },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});

const QuerySchema = new Schema({
  queryName: { type: Array },
  collectionName: { type: String },
  startTime: { type: Number },
  endTime: { type: Number },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});


const InventLogSchema = new Schema({
  method: { type: String },
  content: { type: String },
  type: { type: String },
  url: { type: String },
  agent: { type: String },
  ip: { type: String },
  http: { type: String },
  referer: { type: String },
  header: { type: Object },
  body: { type: Object },
  params: { type: Object },
  ipDetail: { type: Object },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});

const PaymentLogSchema = new Schema({
  method: { type: String },
  content: { type: String },
  type: { type: String },
  url: { type: String },
  agent: { type: String },
  ip: { type: String },
  http: { type: String },
  referer: { type: String },
  header: { type: Object },
  body: { type: Object },
  params: { type: Object },
  ipDetail: { type: Object },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});

const AnalyticLogSchema = new Schema({
  method: { type: String },
  content: { type: String },
  type: { type: String },
  url: { type: String },
  agent: { type: String },
  ip: { type: String },
  http: { type: String },
  referer: { type: String },
  header: { type: Object },
  body: { type: Object },
  params: { type: Object },
  ipDetail: { type: Object },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});

export const LogModel = model('Log', LogSchema);
export const ShortLogModel = model('ShortLog', ShortLogSchema);
export const MailLogModel = model('MailLog', MailLogSchema);
export const SocketLogModel = model('SocketLog', SocketLogSchema);
export const QueryLogModel = model('QueryLog', QueryLogSchema);
export const LogQueryModel = model('LogQuery', QuerySchema)
export const InventoryLogModel = model('InventoryLog', InventLogSchema)
export const PaymentLogModel = model('PaymentLog', PaymentLogSchema)
export const AnalyticLogModel = model('LogAnalytic', AnalyticLogSchema)