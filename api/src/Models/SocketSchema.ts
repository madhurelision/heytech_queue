import { Schema, model } from 'mongoose';

const SocketSchema = new Schema({
  socket_id: { type: String },
  status: { type: Boolean, default: true },
  key: { type: String },
  type: { type: String },
  data: { type: Object },
  user_id: {
    type: Schema.Types.ObjectId,
    default: null
  },
  deleted: { type: Boolean, default: false },
},
  {
    timestamps: true
  });

const SocketStat = new Schema({
  socket_id: { type: String },
  status: { type: Boolean, default: true },
  route: { type: String },
  type: { type: String },
  key: { type: String },
  user_id: {
    type: Schema.Types.ObjectId,
    default: null
  },
  deleted: { type: Boolean, default: false },
},
  {
    timestamps: true
  })

export const SocketModel = model('Socket', SocketSchema);
export const SocketStatModel = model('SocketStat', SocketStat);
