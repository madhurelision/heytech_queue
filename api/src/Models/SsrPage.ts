import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const MenuSchema = new Schema({
  name: { type: String },
  route: { type: String },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
}, {
  timestamps: true
})

const HomeSchema = new Schema({
  title: { type: String },
  description: { type: String },
  btntitle: { type: String },
  btnlink: { type: String },
  image: { type: String },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
}, {
  timestamps: true
})

const FeatureSchema = new Schema({
  title: { type: String },
  description: { type: String },
  position: { type: Number, default: 1 },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
}, {
  timestamps: true
})

const AboutSchema = new Schema({
  title: { type: String },
  element: { type: Array, default: [] },
  status: { type: Boolean, default: false },
  deleted: { type: Boolean, default: false },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
}, {
  timestamps: true
})

const ProductSchema = new Schema({
  title: { type: String },
  description: { type: String },
  link: { type: String },
  image: { type: String },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
}, {
  timestamps: true
})

const ReviewSchema = new Schema({
  title: { type: String },
  description: { type: String },
  designation: { type: String },
  rating: { type: Number },
  image: { type: String },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
}, {
  timestamps: true
})

const MainSchema = new Schema({
  producttitle: { type: String },
  productdescription: { type: String },
  abouttitle: { type: String },
  aboutdescription: { type: String },
  aboutformtitle: { type: String },
  title: { type: String },
  blogtitle: { type: String },
  blogdescription: { type: String },
  catalogtitle: { type: String },
  catalogdescription: { type: String },
  reviewtitle: { type: String },
  reviewdescription: { type: String },
  affiliatetitle: { type: String },
  affiliatedescription: { type: String },
  contacttitle: { type: String },
  contactdescription: { type: String },
  footertitle: { type: String },
  linktitle: { type: String },
  infotitle: { type: String },
  newstitle: { type: String },
  location: { type: String },
  mobile: { type: Number },
  email: { type: String },
  facebooklink: { type: String },
  twitterlink: { type: String },
  instagramlink: { type: String },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  element: { type: Array, default: [] },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
}, {
  timestamps: true
})

const CatalogSchema = new Schema({
  title: { type: String },
  description: { type: String },
  image: { type: String },
  date: { type: Date },
  price: { type: Number },
  price_year: { type: Number },
  currency: { type: String },
  btn: { type: String },
  hour: { type: Number },
  duration: { type: String },
  banner: { type: String },
  color: { type: String },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  product_id: {
    type: Schema.Types.ObjectId,
    ref: 'SsrProduct',
    default: null,
  },
}, {
  timestamps: true
})

const ContactSchema = new Schema({
  name: { type: String },
  email: { type: String },
  message: { type: String },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
}, {
  timestamps: true
})

const BlogSchema = new Schema({
  title: { type: String },
  image: { type: String },
  description: { type: String },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
}, {
  timestamps: true
})

const BlogCommentSchema = new Schema({
  name: { type: String },
  email: { type: String },
  website: { type: String },
  message: { type: String },
  blog_id: {
    type: Schema.Types.ObjectId,
    ref: 'SsrBlog',
    default: null,
  },
  reply: { type: Array, default: [] },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
}, {
  timestamps: true
})

const AffiliateSchema = new Schema({
  image: { type: String },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
}, {
  timestamps: true
})

const LogoSchema = new Schema({
  image: { type: String },
  title: { type: String },
  status: { type: Boolean, default: false },
  deleted: { type: Boolean, default: false },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
}, {
  timestamps: true
})

const NewsletterSchema = new Schema({
  email: { type: String },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
}, {
  timestamps: true
})

const UserSchema = new Schema({
  name: { type: String },
  email: { type: String },
  type: {
    type: Schema.Types.ObjectId,
    ref: 'ShopCategory',
    default: null,
  },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
}, {
  timestamps: true
})

const StripeSchema = new Schema({
  key: { type: String },
  mainKey: { type: String },
  status: { type: Boolean, default: false },
  deleted: { type: Boolean, default: false },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
}, {
  timestamps: true
})


const OrderSchema = new Schema({
  name: { type: String },
  email: { type: String },
  mobile: { type: Number },
  product_id: {
    type: Schema.Types.ObjectId,
    ref: 'SsrCatalog',
    default: null,
  },
  user_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  pay_id: { type: String },
  pay_type: { type: String },
  address_1: { type: String },
  address_2: { type: String },
  order: { type: Object },
  city: { type: String },
  pincode: { type: String },
  country: { type: String },
  state: { type: String },
  Amount: { type: Number },
  status: { type: Boolean, default: false },
  deleted: { type: Boolean, default: false },
}, {
  timestamps: true
})

const PaymentSchema = new Schema({
  name: { type: String },
  email: { type: String },
  mobile: { type: Number },
  Amount: { type: Number },
  order_id: {
    type: Schema.Types.ObjectId,
    ref: 'SsrOrder',
    default: null,
  },
  product_id: {
    type: Schema.Types.ObjectId,
    ref: 'SsrCatalog',
    default: null,
  },
  user_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  pay_id: { type: String },
  payment_id: { type: String },
  payment_method_id: { type: String },
  charge_id: { type: String },
  payment_url: { type: String },
  refunds_url: { type: String },
  payment_method: { type: String, default: 'Stripe' },
  payment_status: { type: String },
  payment_data: { type: Object },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  startDate: { type: Date },
  endDate: { type: Date }
}, {
  timestamps: true
})

const UserFeatureSchema = new Schema({
  title: { type: String, default: 'User Feature' },
  element: { type: Array, default: [] },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
}, {
  timestamps: true
})

const PackageFeatureSchema = new Schema({
  package_id: {
    type: Schema.Types.ObjectId,
    ref: 'SsrCatalog',
    default: null,
  },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  data: { type: Object },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
},
  {
    timestamps: true
  })

const RazorpaySchema = new Schema({
  key: { type: String },
  mainKey: { type: String },
  status: { type: Boolean, default: false },
  deleted: { type: Boolean, default: false },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
}, {
  timestamps: true
})

export const SsrMenuModel = model('SsrMenu', MenuSchema)
export const SsrHomeModel = model('SsrHome', HomeSchema)
export const SsrFeatureModel = model('SsrFeature', FeatureSchema)
export const SsrAboutModel = model('SsrAbout', AboutSchema)
export const SsrProductModel = model('SsrProduct', ProductSchema)
export const SsrReviewModel = model('SsrReview', ReviewSchema)
export const SsrMainModel = model('SsrMain', MainSchema)
export const SsrCatalogModel = model('SsrCatalog', CatalogSchema)
export const SsrContactModel = model('SsrContact', ContactSchema)
export const SsrBlogModel = model('SsrBlog', BlogSchema)
export const SsrCommentModel = model('SsrBlogComment', BlogCommentSchema)
export const SsrAffiliateModel = model('SsrAffiliate', AffiliateSchema)
export const SsrLogoModel = model('SsrLogo', LogoSchema)
export const SsrNewsletterModel = model('SsrNewsletter', NewsletterSchema)
export const SsrUserModel = model('SsrUser', UserSchema)
export const SsrStripeModel = model('SsrStripe', StripeSchema)
export const SsrOrderModel = model('SsrOrder', OrderSchema)
export const SsrPaymentModel = model('SsrPayment', PaymentSchema)
export const SsrUserFeatureModel = model('SsrUserFeature', UserFeatureSchema)
export const SsrPackageFeatureModel = model('SsrPackageFeature', PackageFeatureSchema)
export const SsrRazorpayModel = model('SsrRazorpay', RazorpaySchema)