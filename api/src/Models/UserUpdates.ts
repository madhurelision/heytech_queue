import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const UserUpdateSchema = new Schema({
  type: { type: String },
  user_name: { type: String, default: '' },
  service_name: { type: String, default: '' },
  user_id: {
    type: Schema.Types.ObjectId,
    default: null
  },
  old: { type: Object },
  new: { type: Object },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
  user: { type: Boolean, default: true },
}, {
  timestamps: true
});

export const UserUpdateModel = model('UserUpdate', UserUpdateSchema);
