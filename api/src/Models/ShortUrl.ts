import { Schema, model } from 'mongoose';
import { nanoid } from 'nanoid';

const ShortUrlSchema = new Schema({
  product_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  type: { type: String, default: 'Booking' },
  link: { type: String, default: '' },
  short_id: { type: String, default: nanoid() },
  count: { type: Number, default: 0 },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
}, {
  timestamps: true
})

const ShortUrlStatSchema = new Schema({
  ip: String,
  header: Object,
  method: String,
  ipDetail: { type: Object },
  product_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  short_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  key: String
}, {
  timestamps: true
})

const IpDetailsSchema = new Schema({
  ip: String,
  header: Object,
  method: String,
  ipDetail: { type: Object },
  request_id: {
    type: Schema.Types.ObjectId,
    ref: 'Booking',
    default: null,
  },
  key: String,
  type: { type: Boolean, default: true },
  status: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false },
  content: { type: String },
  ctype: { type: String },
  url: { type: String },
  agent: { type: String },
  http: { type: String },
  referer: { type: String },
  body: { type: Object },
  params: { type: Object },
}, {
  timestamps: true
})

export const ShortUrlModel = model('Short', ShortUrlSchema);

export const ShortUrlStatModal = model('ShortStat', ShortUrlStatSchema);

export const IpDetailModel = model('IpDetail', IpDetailsSchema);