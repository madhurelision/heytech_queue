import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const BannerSchema = new Schema({
  name: { type: String },
  color: { type: String },
  shop_id: {
    type: Schema.Types.ObjectId,
    ref: 'shop',
    default: null,
  },
  admin_id: {
    type: Schema.Types.ObjectId,
    ref: 'Admin',
    default: null,
  },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});


export const BannerModel = model('Banner', BannerSchema);
