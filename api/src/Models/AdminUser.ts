import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const AdminUserSchema = new Schema({
  email: { type: String },
  mobile: { type: String },
  admin_id: {
    type: Schema.Types.ObjectId,
    ref: 'Admin',
    default: null,
  },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: false },
}, {
  timestamps: true
});

export const AdminUserModel = model('AdminUser', AdminUserSchema);
