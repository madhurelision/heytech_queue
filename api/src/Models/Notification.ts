import { Schema, model } from 'mongoose';

const NotificationSchema = new Schema({
  message: { type: String },
  booking_id: {
    type: Schema.Types.ObjectId,
    ref: 'Booking',
    default: null,
  },
  name: { type: String },
  service_name: { type: String },
  status: { type: String },
  image: { type: String },
  shop_name: { type: String },
  shop_image: { type: String },
  user_id: {
    type: Schema.Types.ObjectId,
    ref: 'customer',
    default: null,
  },
  shop_id: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null,
  },
  view: { type: Boolean, default: false },
  shop_view: { type: Boolean, default: false },
  like: { type: Boolean, default: false },
  type: { type: String, default: 'booking' }
},
  {
    timestamps: true
  });

export const NotificationModel = model('Notification', NotificationSchema);
