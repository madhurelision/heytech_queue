import { Schema, model, SchemaDefinitionProperty } from 'mongoose';

const ReviewTagSchema = new Schema({
  name: { type: String },
  type: { type: String },
  user_id: {
    type: Schema.Types.ObjectId,
  },
  admin_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  deleted: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
}, {
  timestamps: true
});

export const ReviewTagModel = model('ReviewTag', ReviewTagSchema);
