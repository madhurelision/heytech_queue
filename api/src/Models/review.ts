import { Schema, model } from 'mongoose';

const ReviewSchema = new Schema({
  user_email: { type: String },
  mobile: { type: String },
  shop_email: String,
  description: { type: String, required: true },
  rating: { type: Number, required: true },
  image: { type: String },
  shop_image: { type: String },
  booking_id: {
    type: Schema.Types.ObjectId,
    ref: 'Booking',
  },
  user_id: {
    type: Schema.Types.ObjectId,
    ref: 'customer',
    required: true
  },
  shop_name: String,
  user_name: String,
  shop_id: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  service_id: {
    type: Schema.Types.ObjectId,
    ref: 'Service',
  },
  deleted: { type: Boolean, default: false },
  user_review: { type: Boolean, default: false },
  shop_review: { type: Boolean, default: false },
},
  {
    timestamps: true
  });

export const ReviewModel = model('Review', ReviewSchema);
