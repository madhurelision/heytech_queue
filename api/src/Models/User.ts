import { Schema, model, SchemaDefinitionProperty } from 'mongoose';
import {
  UserSchemaType,
  MentorSchemaType,
  DayEnumType,
  DurationType,
} from '../types';

const Duration = new Schema<DurationType>({
  start_hour: Number,
  end_hour: Number,
  available: Boolean,
  locale: String,
});
const TopicSchema = new Schema({
  emojiIcon: { type: String, index: 'text' },
  emojiBadge: { type: String, index: 'text' },
  motivation: { type: String, index: 'text' },
  topicName: { type: String, index: 'text' },
  topicDescription: { type: String, index: 'text' },
});

const MentorSchema = new Schema<MentorSchemaType>({
  user_id: { type: String },
  first_name: { type: String },
  last_name: { type: String },
  image_link: { type: String },
  //job_title: { type: String },
  company: { type: String },
  description: { type: [String] },
  expertise: { type: [String] },
  language: { type: [String] },
  // linkedIn: String,
  instagram: String,
  is_mentoring: { type: Boolean, default: true },
  topics: [Number],
  time_slot: {
    monday: { type: Duration },
    tuesday: { type: Duration },
    wednesday: { type: Duration },
    thursday: { type: Duration },
    friday: { type: Duration },
    saturday: { type: Duration },
    sunday: { type: Duration },
  },
  user_information: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null,
  },
});

const UserSchema = new Schema({
  user_id: String,
  first_name: { type: String, default: '' },
  last_name: { type: String, default: '' },
  email: { type: String, default: '' },
  google: String,
  image_link: { type: String, default: '' },
  oauth_provider: String,
  is_mentor: Boolean,
  signup_completed: { type: Boolean, default: false },
  mentor_information: {
    type: Schema.Types.ObjectId,
    ref: 'shop',
    default: null,
  },
  otp: { type: Number },
  mobile: { type: Number, default: null },
  mobile_verify: { type: Boolean, default: false },
  mobile_login: { type: Boolean, default: false },
  lastLogin: { type: Date },
  lastLogout: { type: Date },
  status: { type: Boolean, default: false },
  deleted: { type: Boolean, default: false },
}, {
  timestamps: true
});

const ShopSchema = new Schema({
  user_id: { type: String },
  first_name: { type: String },
  last_name: { type: String },
  shop_name: { type: String },
  image_link: { type: String },
  banner_link: { type: String, default: '' },
  email: { type: String },
  upi_id: { type: String },
  company: { type: String },
  description: { type: String },
  expertise: { type: [String] },
  language: { type: [String] },
  like: {
    type: [Schema.Types.ObjectId],
    ref: 'customer',
    default: [],
  },
  instagram: String,
  is_mentoring: { type: Boolean, default: true },
  topics: {
    type: [Schema.Types.ObjectId],
    ref: 'Service',
    default: [],
  },
  time_slot: {
    monday: { type: Duration },
    tuesday: { type: Duration },
    wednesday: { type: Duration },
    thursday: { type: Duration },
    friday: { type: Duration },
    saturday: { type: Duration },
    sunday: { type: Duration },
  },
  user_information: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null,
  },
  waiting_number: { type: Number },
  waiting_key: { type: String },
  waiting_time: { type: String },
  code: { type: String },
  location: { type: String },
  lattitude: { type: Number },
  longitude: { type: Number },
  mobile: { type: Number },
  seat: { type: Number, default: 1 },
  queue: { type: Boolean, default: true },
  open: { type: Boolean, default: true },
  appointment: { type: Boolean, default: true },
  mobile_verify: { type: Boolean, default: false },
  status: { type: Boolean, default: false },
  deleted: { type: Boolean, default: false },
  banner: { type: String },
  type: {
    type: Schema.Types.ObjectId,
    ref: 'ShopCategory',
    default: null,
  },
  locate: { type: Object },
  lastLogin: { type: Date },
  lastLogout: { type: Date },
  aadhar_link: { type: String, default: '' },
  gst_link: { type: String, default: '' },
  order: { type: Number, default: 1 },
  time_zone: { type: Number, default: -330 },
  time_val: { type: String, default: 'Asia/Kolkata' }
}, {
  timestamps: true
});

export const UserModel = model('User', UserSchema);
export const MentorModel = model('Mentor', MentorSchema);
export const ShopModel = model('shop', ShopSchema);
