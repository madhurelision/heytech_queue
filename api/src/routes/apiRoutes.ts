import { Router,Request, Response ,NextFunction } from 'express';
import {
  authController,
  logoutController,
} from '../controllers/auth-controller';
import {
  getTopicsController,
  getMentorController,
  getMentorsController,
} from '../controllers/api-controller';

import {
  fakeDataController,
  topicDataController,
} from '../data/fakeData-controller';

import { upload } from '../config/uploadImage';
import { createShopUser, getAllExpertise, getShopUserProfile, SingleUser, updateShopUser, likeShopUser, unlikeShopUser, createToken, createOrChangeQrCode, changeShopQueueStatus, changeShopAppointmentStatus, SearchShopQuery, CreateOtp, verifyOtp, SearchShopRegexQuery, UpdateServiceInShop, shopMobileLoginSignup, shopMobileLoginOtpVerify, getAllActiveServices, getAllQueryActiveServices } from '../controllers/mentorController';
import { BookingQueueVerify, BookingVerify, CustomerVerify, EmployeeVerify, ShopVerify } from '../config/verifyToken';
import { getCustomerProfile } from '../controllers/customerController';
import { changeServiceAcceptStatus, cloneAllService, cloneService, CreateService, deleteService, getAllService, getShopService, SearchServiceQuery, SearchServiceRegexQuery, SearchServiceShopIdRegexQuery, updateService } from '../controllers/serviceController';
import { getShopUserGallery, createGallery, EditGallery, deleteGallery } from '../controllers/galleryController';
import { addWaitingTime, CountBooking, CreateBookingCustomer, CreateQueueBookingCustomer, getCustomerBookings, getCustomerCurrentAppointment, getShopBookings, getShopUserCurrentAppointment, getSingleBooking, shopBookingCancel, shopBookingQuery, shopUserQueueList, userCancelBooking, userVerifyCancelBooking, verifyBookingOtp, customerBookingQuery, addQueueWaitingTime, CreateBookingOtp, getSingleQuery, assignEmployeeIdBooking } from '../controllers/bookingController';
import { openShortUrl } from '../modules/ShortUrl';
import { makeReview } from '../controllers/reviewController';
import { CreateAdmin, getUserLocation, OpenUrl, OpenUrlWithoutToken, SequeneceId, UpdateAllQueuedata } from '../controllers/testController';
import { verify } from '../config/googleVerify';
import { CreateManyTag, CreateTag, getAllTags } from '../controllers/tagController';
import { changeShopViewStatus, changeViewStatus, getCustomerNotification, getShopNotification } from '../controllers/notificationController';
import { CreateManyReviewTag, CreateReviewTag, getAllReviewTags } from '../controllers/reviewtagController';
import { changeEmployeeStatus, CreateEmployee, deleteEmployee, EditEmployee, getShopEmployee } from '../controllers/shopEmployeeController';
import { employeeLoginId, getEmployeeAppointment, getEmployeeBookings, getEmployeeProfile, employeeMobileLoginOtpVerify, employeeMobileLogin } from '../controllers/employeeController';
import { getAllServiceTags } from '../controllers/serviceTagController';
import { getAllDescriptionTags } from '../controllers/descriptionTagController';
import { subscriberPost } from '../modules/pushNotification';
import { getAllCombinedTagList } from '../controllers/metadaController';
import { getAllTagService } from '../controllers/tagServiceController';
import { getAllOffers } from '../controllers/offerController';
import { getAllBanners } from '../controllers/bannerController';

const router = Router();

// we will do our re-routing from the client side just send information from here
// GET to /api/auth will return current logged in user info
router.get('/auth', authController);
router.get('/logout', logoutController); // auth logout

router.get('/get-mentor', getMentorController);
router.get('/get-mentors', getMentorsController);
router.get('/get-topics', getTopicsController);
router.get('/data', fakeDataController);
router.get('/topicData', topicDataController);

// shop
router.post('/createShop/:id', createShopUser)
router.get('/getUser/:id', SingleUser)
router.get('/shop/profile', ShopVerify, getShopUserProfile)
router.post('/shop/updateShop', ShopVerify, updateShopUser)
router.post('/shop/updateQueueStatus/:id', ShopVerify, changeShopQueueStatus)
router.post('/shop/updateAppointmentStatus/:id', ShopVerify, changeShopAppointmentStatus)

router.post('/updateServiceShop/:id', UpdateServiceInShop)

// customer
router.get('/user/profile', CustomerVerify, getCustomerProfile)
//
router.get('/getAllExpertise', getAllExpertise)

// service
router.post('/shop/createService', ShopVerify, CreateService)
router.post('/shop/updateService/:id', ShopVerify, updateService)
router.get('/shop/getService/:skip/:limit', ShopVerify, getShopService)
router.get('/getAllService', getAllService)
router.post('/shop/updateServiceStatus/:id', ShopVerify, changeServiceAcceptStatus)
router.get('/shop/cloneService/:id', ShopVerify, cloneService)
router.post('/shop/cloneAllService', ShopVerify, cloneAllService)
router.get('/shop/deleteService/:id', ShopVerify, deleteService)
router.get('/activeService', getAllActiveServices)

// gallery
router.get('/shop/getGallery', ShopVerify, getShopUserGallery)
router.post('/shop/createGallery', ShopVerify, createGallery)
router.post('/shop/editGallery/:id', ShopVerify, EditGallery)
router.get('/shop/deleteGallery/:id', ShopVerify, deleteGallery)

/// Booking
router.post('/createBooking', BookingVerify, CreateBookingCustomer)
router.post('/createBookingQueue', BookingQueueVerify, CreateQueueBookingCustomer)
router.post('/verifyBooking/:id', verifyBookingOtp)
router.post('/shop/addWaitingTime/:id', ShopVerify, addWaitingTime)
router.post('/shop/addQueueWaitingTime/:id', ShopVerify, addQueueWaitingTime)
router.get('/shop/getAllBooking', ShopVerify, getShopBookings)
router.get('/shop/getCurrentBooking/:skip/:limit', ShopVerify, getShopUserCurrentAppointment)
router.get('/shop/getQueueBooking/:skip/:limit', ShopVerify, shopUserQueueList)
router.get('/user/getAllBooking', CustomerVerify, getCustomerBookings)
router.get('/user/getCurrentBooking', CustomerVerify, getCustomerCurrentAppointment)
router.get('/getSingleBooking/:id', getSingleBooking)
router.get('/user/bookingCount', CustomerVerify, CountBooking)
router.post('/shop/getQueryBooking/:skip/:limit', ShopVerify, shopBookingQuery)
router.post('/user/getQueryBooking/:skip/:limit', CustomerVerify, customerBookingQuery)
router.post('/generateOtpBooking/:id', CreateBookingOtp)
router.get('/user/getBookQuery/:id', CustomerVerify, getSingleQuery)
router.get('/shop/getBookQuery/:id', ShopVerify, getSingleQuery)
router.post('/shop/assignEmployee/:id', ShopVerify, assignEmployeeIdBooking)

// Shop Employee
router.post('/shop/createEmployee', ShopVerify, CreateEmployee);
router.post('/shop/editEmployee/:id', ShopVerify, EditEmployee);
router.get('/shop/getEmployee', ShopVerify, getShopEmployee);
router.post('/shop/updateEmployeeStatus/:id', ShopVerify, changeEmployeeStatus);
router.get('/shop/deleteEmployee/:id', ShopVerify, deleteEmployee);


/// Employee
router.post('/employeeLoginId', employeeLoginId)
router.get('/employee/profile', EmployeeVerify, getEmployeeProfile)
router.get('/employee/getCurrentBooking', EmployeeVerify, getEmployeeAppointment)
router.get('/employee/getBookings', EmployeeVerify, getEmployeeBookings);

// employee Mobile Login
router.post('/mobileEmployeeLogin', employeeMobileLogin)
router.post('/mobileEmployeeLoginVerify', employeeMobileLoginOtpVerify)

// create or update QRCode
router.get('/shop/Qrcode', ShopVerify, createOrChangeQrCode)

// cancelBooking
router.get('/user/cancelBooking/:id', CustomerVerify, userCancelBooking)
router.post('/user/verifyCancelBooking/:id', CustomerVerify, userVerifyCancelBooking)
router.get('/shop/cancelBooking/:id', ShopVerify, shopBookingCancel)

// short
router.get('/openShortUrl/:id', openShortUrl)

//like unlike Shop
router.post('/likeShopUser/:id', likeShopUser)
router.post('/unlikeShopUser/:id', unlikeShopUser)

// review
router.post('/shop/makeReview', ShopVerify, makeReview)
router.post('/user/makeReview', CustomerVerify, makeReview)

// createToken
router.get('/makeToken/:id', createToken)

// dummy Url
router.get('/open/:id', OpenUrlWithoutToken)
router.get('/open/:id/:token', verify, OpenUrl)

///chekc
router.post('/testing/:id', ShopVerify, UpdateAllQueuedata)

///admin
router.get('/createAdmin', CreateAdmin)

// booking tag
router.get('/getAllTag', getAllTags);

// review tag
router.get('/getAllReviewTag', getAllReviewTags);

// service tag
router.get('/getAllServiceTag', getAllServiceTags)

// description tag
router.get('/getAllDescriptionTag', getAllDescriptionTags)

//tag service
router.get('/getAllTagService', getAllTagService)

// notification Customer
router.get('/user/notification', CustomerVerify, getCustomerNotification)
router.get('/user/view', CustomerVerify, changeViewStatus)
router.get('/shop/notification', ShopVerify, getShopNotification)
router.get('/shop/view', ShopVerify, changeShopViewStatus)

// shop Query
router.get('/getShopQuery/:id', SearchShopQuery)
router.get('/getShopRegexQuery/:id', SearchShopRegexQuery)

// createOtp
router.post('/createShopOtp/:id', CreateOtp)
router.post('/verifyShopOtp/:id', verifyOtp)

// shop Mobile Login
router.post('/mobileShopLogin', shopMobileLoginSignup)
router.post('/mobileShopLoginVerify', shopMobileLoginOtpVerify)

// service Query
router.get('/getServiceQuery/:id', SearchServiceQuery)
router.get('/getServiceRegexQuery/:id', SearchServiceRegexQuery)
router.get('/activeServiceRegexQuery/:id', getAllQueryActiveServices)
router.get('/getServiceShopRegexQuery/:id', ShopVerify, SearchServiceShopIdRegexQuery)

//metadata
router.get('/getAllMetadata', getAllCombinedTagList)

// push
router.post('/subscribe', (req: Request, res: Response, next: NextFunction) => { subscriberPost(req,res,next,true) })

//offer User
router.get('/getAllOffer', getAllOffers)

//banner User
router.get('/getAllBanner', getAllBanners)

//location
router.get('/location', getUserLocation)

//testing purpose
router.get('/test', SequeneceId)

export default router;
