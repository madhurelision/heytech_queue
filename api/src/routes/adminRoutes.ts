import { Router } from 'express';
import { AdminVerify } from '../config/verifyToken';
import { AdminLogin, getAdminProfile, CustomerListAdmin, getAdminCurrentBookings, getAllAdminService, getAllGalleryList, getAllShopList, GetDayWiseCount, dateWiseShopList, getSingleShop, getSingleCustomer, getDateWiseBookingData, getAllAdminBookingList, getAllAdminEmployee, getAllBookingIpDetail, getAllBookingShortIdDetail, readLogFile, deleteLogFile, SearchAdminBookingQuery, getAllLogList, getSingleBookingIpDetail, getAllLocationLogList, getSingleBooking } from '../controllers/adminController';
import { customerCsvImport, SerachAdminCustomerQuery, updateCustomer } from '../controllers/customerController';
import { CreateDescriptionTag, deleteDescriptionTag, EditDescriptionTag, getAllDescriptionTagList, CreateManyDescriptionTag } from '../controllers/descriptionTagController';
import { createGallery, deleteGallery, EditGallery } from '../controllers/galleryController';
import { changeShopStatusByAdmin, createOrChangeQrCodeByAdmin, SearchShopAdminRegexQuery, SearchShopRegexQuery, shopCsvImport, updateShopByAdmin } from '../controllers/mentorController';
import { CreateReviewTag, deleteReviewTag, EditReviewTag, getAllReviewTagList } from '../controllers/reviewtagController';
import { changeServiceAcceptStatus, CreateService, deleteService, SearchAdminServiceRegexQuery, SearchServiceRegexQuery, serviceCsvImport, updateAdminService, updateService } from '../controllers/serviceController';
import { CreateServiceTag, deleteServiceTag, EditServiceTag, getAllServiceTagList, CreateManyServiceTag } from '../controllers/serviceTagController';
import { changeEmployeeStatus, deleteEmployee, EditEmployee } from '../controllers/shopEmployeeController';
import { CreateTag, deleteTag, EditTag, getAllTagList } from '../controllers/tagController';
import { deletePushNotification, getAllPushNotificationList, sendNotication } from '../modules/pushNotification';
import { getAllTemplateList, CreateTemplate, updateTemplate, deleteTemplate } from '../controllers/templateController'
import { CreateTagService, deleteTagService, EditTagService, getAllTagServiceList } from '../controllers/tagServiceController';
import { deleteUserUpdate, getAllUserUpdateList } from '../controllers/userUpdateController';
import { getAllEmployeeSearchQuery } from '../controllers/employeeController';
import { CreateOffer, deleteOffer, EditOffer, getAllOfferList, updateStatusOffer } from '../controllers/offerController';
import { CreateBanner, deleteBanner, EditBanner, getAllBannerList } from '../controllers/bannerController';
import { getAllMailService } from '../controllers/MailServiceController';

const router = Router();

router.post('/login', AdminLogin)
router.get('/profile', AdminVerify, getAdminProfile)

//customer
router.get('/customerList/:skip/:limit', AdminVerify, CustomerListAdmin)
router.get('/singleCustomer/:id', AdminVerify, getSingleCustomer)
router.post('/editCustomer/:id', AdminVerify, updateCustomer)

//booking current
router.get('/currentBookingList/:skip/:limit', AdminVerify, getAdminCurrentBookings)
router.get('/bookingList/:skip/:limit', AdminVerify, getAllAdminBookingList)
router.get('/singleBooking/:id', AdminVerify, getSingleBooking)

//service
router.get('/serviceList/:skip/:limit', AdminVerify, getAllAdminService)
router.post('/createService', AdminVerify, CreateService)
router.post('/updateService/:id', AdminVerify, updateAdminService)
router.post('/updateServiceStatus/:id', AdminVerify, changeServiceAcceptStatus)
router.get('/deleteService/:id', AdminVerify, deleteService);

//gallery
router.get('/galleryList/:skip/:limit', AdminVerify, getAllGalleryList)
router.post('/createGallery', AdminVerify, createGallery)
router.post('/editGallery/:id', AdminVerify, EditGallery)
router.get('/deleteGallery/:id', AdminVerify, deleteGallery)

//shop
router.get('/shopList/:skip/:limit', AdminVerify, getAllShopList)
router.get('/singleShop/:id', AdminVerify, getSingleShop)
router.post('/editShop/:id', AdminVerify, updateShopByAdmin)
router.get('/Qrcode/:id', AdminVerify, createOrChangeQrCodeByAdmin)
router.post('/changeShopStatus/:id', AdminVerify, changeShopStatusByAdmin)

//count Boooking
//router.post('/queryBookingList', AdminVerify, GetDayWiseCount)
router.post('/queryBookingList', AdminVerify, getDateWiseBookingData)

// booking Tag
router.get('/tagList/:skip/:limit', AdminVerify, getAllTagList)
router.post('/createTag', AdminVerify, CreateTag)
router.post('/editTag/:id', AdminVerify, EditTag)
router.get('/deleteTag/:id', AdminVerify, deleteTag)

// review Tag
router.get('/reviewTagList/:skip/:limit', AdminVerify, getAllReviewTagList)
router.post('/createReviewTag', AdminVerify, CreateReviewTag)
router.post('/editReviewTag/:id', AdminVerify, EditReviewTag)
router.get('/deleteReviewTag/:id', AdminVerify, deleteReviewTag)

// service tag
router.get('/serviceTagList/:skip/:limit', AdminVerify, getAllServiceTagList)
router.post('/createServiceTag', AdminVerify, CreateServiceTag)
router.post('/editServiceTag/:id', AdminVerify, EditServiceTag)
router.get('/deleteServiceTag/:id', AdminVerify, deleteServiceTag)

// description tag
router.get('/descriptionTagList/:skip/:limit', AdminVerify, getAllDescriptionTagList)
router.post('/createDescriptionTag', AdminVerify, CreateDescriptionTag)
router.post('/editDescriptionTag/:id', AdminVerify, EditDescriptionTag)
router.get('/deleteDescriptionTag/:id', AdminVerify, deleteDescriptionTag)

////csv Import
router.post('/importServiceCsv', AdminVerify, serviceCsvImport)
router.post('/importCustomerCsv', AdminVerify, customerCsvImport)
router.post('/importShopCsv', AdminVerify, shopCsvImport)

//employee
router.get('/employeeList/:skip/:limit', AdminVerify, getAllAdminEmployee)
router.post('/editEmployee/:id', AdminVerify, EditEmployee);
router.post('/updateEmployeeStatus/:id', AdminVerify, changeEmployeeStatus);
router.get('/deleteEmployee/:id', AdminVerify, deleteEmployee);

// ip Details
router.get('/bookingIpList/:skip/:limit', AdminVerify, getAllBookingIpDetail)
// short Id Click
router.get('/bookingShortIdList/:skip/:limit', AdminVerify, getAllBookingShortIdDetail)

//log
router.get('/logList', AdminVerify, readLogFile)
router.get('/clearlogList', AdminVerify, deleteLogFile)

//push
router.get('/pushNotificationList/:skip/:limit', AdminVerify, getAllPushNotificationList)
router.post('/sendPushNotification', AdminVerify, sendNotication)
router.get('/deletePushNotification/:id', AdminVerify, deletePushNotification);

//template
router.get('/templateList/:skip/:limit', AdminVerify, getAllTemplateList)
router.post('/createTemplate', AdminVerify, CreateTemplate)
router.post('/updateTemplate/:id', AdminVerify, updateTemplate)
router.get('/deleteTemplate/:id', AdminVerify, deleteTemplate);

// tag Service
router.get('/tagServiceList/:skip/:limit', AdminVerify, getAllTagServiceList)
router.post('/createTagService', AdminVerify, CreateTagService)
router.post('/editTagService/:id', AdminVerify, EditTagService)
router.get('/deleteTagService/:id', AdminVerify, deleteTagService)

// user Updates
router.get('/userUpdateList/:skip/:limit', AdminVerify, getAllUserUpdateList)
router.get('/deleteUserUpdate/:id', AdminVerify, deleteUserUpdate)

//querySearch
router.get('/getServiceQuery/:id', AdminVerify, SearchAdminServiceRegexQuery)
router.get('/getShopQuery/:id', AdminVerify, SearchShopAdminRegexQuery)
router.get('/getEmployeeQuery/:id', AdminVerify, getAllEmployeeSearchQuery)
router.get('/getCustomerQuery/:id', AdminVerify, SerachAdminCustomerQuery)
router.get('/getBookingQuery/:id', AdminVerify, SearchAdminBookingQuery)

// offer
router.get('/offerList/:skip/:limit', AdminVerify, getAllOfferList)
router.post('/createOffer', AdminVerify, CreateOffer)
router.post('/editOffer/:id', AdminVerify, EditOffer)
router.get('/deleteOffer/:id', AdminVerify, deleteOffer)
router.post('/updateOfferStatus/:id', AdminVerify, updateStatusOffer)

// banner
router.get('/bannerList/:skip/:limit', AdminVerify, getAllBannerList)
router.post('/createBanner', AdminVerify, CreateBanner)
router.post('/editBanner/:id', AdminVerify, EditBanner)
router.get('/deleteBanner/:id', AdminVerify, deleteBanner)

/// service request
router.get('/serviceRequestList/:skip/:limit', AdminVerify, getAllMailService)

//// log data
router.get('/getlogList/:skip/:limit', AdminVerify, getAllLogList)
router.get('/getBookDetail/:id', AdminVerify, getSingleBookingIpDetail)

// location Log
router.get('/locationLogList/:skip/:limit', AdminVerify, getAllLocationLogList)


router.get('/check', dateWiseShopList)

export default router;
