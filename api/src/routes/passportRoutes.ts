import { Request, Response } from "express";
import { SECRET_KEY, CLIENT_URL } from "../config/keys";
import jwt from 'jsonwebtoken'
const router = require('express').Router();
const passport = require('passport');
const { mentorMiddleware, userMiddleware } = require('../config/passportSetup');
import config from 'config'
import { LoginLogModel } from "../Models/LoginLog";


router.get('/mentor/auth/google', mentorMiddleware, passport.authenticate('google', {
  scope: ['profile', 'email', 'https://www.googleapis.com/auth/userinfo.profile',
    'https://www.googleapis.com/auth/userinfo.email'], accessType: 'offline', prompt: 'consent'
}));

router.get('/user/auth/google', userMiddleware,
  passport.authenticate("google", {
    scope: ['profile', 'email', 'https://www.googleapis.com/auth/userinfo.profile',
      'https://www.googleapis.com/auth/userinfo.email'], accessType: 'offline', prompt: 'consent'
  }));

router.get('/mentor/auth/google/callback',
  passport.authenticate('google', { failureRedirect: config.has('CLIENT_URL') ? config.get('CLIENT_URL') : CLIENT_URL }),
  async function (req: Request, res: Response) {
    var shopData: any = req.user
    console.log('completed', req.user)
    const token: any = await jwt.sign({ _id: shopData._id, email: shopData.email }, SECRET_KEY)
    res.header("auth-token", token)
    const shopSLogin = await LoginLogModel.create({
      user_id: shopData._id,
      user_type: 'Shop',
      type: 'Login',
      token: token,
      message: 'Login',
    })
    const link: any = (shopData.signup_completed == true) ? config.has('CLIENT_URL') ? config.get('CLIENT_URL') + '/shoplogin/' + token : CLIENT_URL + '/shoplogin/' + token : config.has('CLIENT_URL') ? config.get('CLIENT_URL') + '/register/' + shopData._id : CLIENT_URL + '/register/' + shopData._id
    res.status(301).redirect(link)
    // if (shopData.signup_completed == true) {
    //   res.redirect(config.has('CLIENT_URL') ? config.get('CLIENT_URL') + '/shoplogin/' + token : CLIENT_URL + '/shoplogin/' + token);
    // } else {
    //   res.redirect(config.has('CLIENT_URL') ? config.get('CLIENT_URL') + '/register/' + shopData._id : CLIENT_URL + '/register/' + shopData._id);
    // }
  });

router.get('/user/auth/google/callback',
  passport.authenticate('google', { failureRedirect: config.has('CLIENT_URL') ? config.get('CLIENT_URL') : CLIENT_URL }),
  async function (req: Request, res: Response) {
    const Main: any = req.user
    console.log('completed', req.user)
    const token: any = await jwt.sign({ _id: Main._id, email: Main.email }, SECRET_KEY)
    res.header("auth-token", token)
    const link = config.has('CLIENT_URL') ? config.get('CLIENT_URL') + '/userlogin/' + token : CLIENT_URL + '/userlogin/' + token
    res.status(301).redirect(link);
  });

export default router