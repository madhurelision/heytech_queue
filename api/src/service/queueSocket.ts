import { Server, Socket } from 'socket.io';
import chalk from 'chalk';
import { nanoid } from 'nanoid';
import jwt from 'jsonwebtoken'
import socketJWT from 'socketio-jwt'
import { CLIENT_URL, SECRET_KEY } from '../config/keys';
import http from 'http';
import { SocketModel } from '../Models/SocketSchema';
import { CustomerModel } from '../Models/Customer';
import { getShopCurrenQueue, shopQueueAndBooking, userQueueAndBooking } from '../config/function';
import watcher from '../config/watcher';

const queueService = (httpServer: http.Server): void => {
  const IO_OPTIONS = {
    cors: {
      origin: CLIENT_URL,
      methods: ['GET', 'POST', 'DELETE', 'PATCH', 'UPDATE'],
      credentials: true,
    },
  };

  const io = new Server(httpServer, IO_OPTIONS);
  watcher(io)

  // io.use(function (socket, next) {
  //   console.log('Socket token', socket.handshake.query.token);
  //   const token: any = socket.handshake.query.token
  //   if (!token) return socket.emit('disconnect', "Access Denied")
  //   try {
  //     const verified = jwt.verify(token, SECRET_KEY);
  //     //console.log('Socket Verify', verified)
  //     next()
  //   } catch (err) {
  //     socket.emit('disconnect', "Invalid Token")
  //   }
  // });
  // io.use(socketJWT.authorize({
  //   secret: SECRET_KEY,
  //   handshake: true
  // }));

  io.on('connection', (socket: any) => {
    console.log(`A user ${chalk.green(socket.id)} conmnection`);
    // console.log('socket decoded data0', socket.decoded_token._id)

    socket.on('userConnect', async (data: any) => {

      const token = data

      try {

        const verified: any = jwt.verify(token, SECRET_KEY)
        console.log('User Data verify', verified)
        if (Object.keys(verified).length == 0) {
          return socket.emit('userDisconnect', 'Token is Err')
        }

        SocketModel.findOneAndUpdate({ user_id: verified._id }, {
          $set: {
            socket_id: socket.id,
            status: true,
          },
          $setOnInsert: {
            delete: false
          }
        }, { upsert: true, new: true, returnNewDocument: true, returnOriginal: false, setDefaultsOnInsert: true }, (err, doc) => {
          if (err) {
            console.log(err)
          }
          console.log('socket connect', doc)
        })

        CustomerModel.findOne({ _id: verified._id }).then((item) => {

          console.log('check', item)

          if (item == null || item == undefined) {

            socket.join('Shop' + verified._id)

            shopQueueAndBooking(verified._id, (status: any, err: Error, data: any) => {

              if (err) {
                return
              }
              console.log('sent')
              socket.emit('shopCount', data)
              // io.sockets.in('Shop' + verified._id).emit('shopCount', data)

            })

          }

          socket.join('User' + verified._id)

          userQueueAndBooking(verified._id, (status: any, err: Error, data: any) => {

            if (err) {
              return
            }

            socket.emit('customerCount', data)
            // io.sockets.in('User' + verified._id).emit('customerCount', data)

          })

        }).catch((err) => {
          console.log('User Data Find Socket Error', err)
        })

      } catch (err) {
        console.log('Socket Emit Function Err', err)
        socket.emit('userDisconnect', 'Token is Err')
      }

    })


    socket.on('shopQueue', (data: any) => {
      getShopCurrenQueue(data, (status: any, err: Error, val: any) => {
        if (err) {
          return
        }
        socket.emit('shopValue', val)
      })
    })

    // SocketModel.findOneAndUpdate({ user_id: socket.decoded_token._id }, {
    //   $set: {
    //     socket_id: socket.id,
    //     status: true,
    //   },
    //   $setOnInsert: {
    //     delete: false
    //   }
    // }, { upsert: true, new: true, returnNewDocument: true, returnOriginal: false, setDefaultsOnInsert: true }, (err, doc) => {
    //   if (err) {
    //     console.log(err)
    //   }
    //   console.log('socket connect', doc)
    // })

    // CustomerModel.findOne({ _id: socket.decoded_token._id }).then((item) => {

    //   console.log('check', item)

    //   if (item == null || item == undefined) {

    //     socket.join('Shop' + socket.decoded_token._id)

    //     shopQueueAndBooking(socket.decoded_token._id, (status: any, err: Error, data: any) => {

    //       if (err) {
    //         return
    //       }
    //       console.log('sent')
    //       socket.emit('shopCount', data)
    //       // io.sockets.in('Shop' + socket.decoded_token._id).emit('shopCount', data)

    //     })

    //   }

    //   socket.join('User' + socket.decoded_token._id)

    //   userQueueAndBooking(socket.decoded_token._id, (status: any, err: Error, data: any) => {

    //     if (err) {
    //       return
    //     }

    //     socket.emit('customerCount', data)
    //     // io.sockets.in('User' + socket.decoded_token._id).emit('customerCount', data)

    //   })

    // }).catch((err) => {
    //   console.log('User Data Find Socket Error', err)
    // })

    // socket.on('shopQueue', (data: any) => {
    //   getShopCurrenQueue(data, (status: any, err: Error, val: any) => {
    //     if (err) {
    //       return
    //     }
    //     socket.emit('shopValue', val)
    //   })
    // })

    socket.on('disconnect', function () {
      console.log('Socket Disconnect')
      socket.removeAllListeners()
      // SocketModel.findOneAndUpdate({ user_id: socket.decoded_token._id }, {
      //   $set: {
      //     socket_id: '',
      //     status: false,
      //     updatedAt: new Date(),
      //   }
      // }, { new: true }, (err, doc) => {
      //   if (err) {
      //     return console.log(err)
      //   }
      //   console.log('socket disconnect', doc)
      //   if (!doc || doc == null || doc == undefined) {

      //   } else {
      //     if (doc.user_id === socket.decoded_token._id) {
      //       socket.leave('Shop' + doc.user_id)
      //       socket.leave('User' + doc.user_id)
      //     }
      //   }
      // })
    })

  });
};

export default queueService;
