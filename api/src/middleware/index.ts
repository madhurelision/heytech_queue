import express, { Express } from 'express';
import cors from 'cors';
import passport from 'passport';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import { COOKIE_KEYS, CLIENT_URL } from '../config/keys';

const addMiddleWare = (app: Express) => {

  app.use(
    cors({
      origin: ['*','https://loadio.herokuapp.com','https://queuickadmin.herokuapp.com', 'https://heybarberadmin.herokuapp.com', 'http://heybarberadmin.herokuapp.com', 'http://heybarber.herokuapp.com', 'https://heybarber.herokuapp.com', 'http://localhost:3000', 'http://localhost:3001', 'http://localhost:3002', 'http://localhost:3003', 'https://localhost:5000', 'http://localhost:5000', 'https://heybarber.in', 'http://heybarber.in', 'http://43.204.28.85:3000', 'https://43.204.28.85:3000', 'http://queuick.com', 'https://queuick.com'],
      methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
      allowedHeaders: 'Content-Type,auth-token,Access-Control-Allow-Headers,Origin',
      credentials: true,
      "optionsSuccessStatus": 204
    }),
  );



  // app.use((req, res, next) => {
  //   res.header('Access-Control-Allow-Origin',  'https://heybarber.herokuapp.com');
  //   res.header("Access-Control-Allow-Credentials",'true');
  //   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, auth-token");
  //   res.header("Access-Control-Allow-Methods","POST, PUT, OPTIONS, GET, DELETE");

  //   next()
  // })


  app.use(express.json({ limit: '50mb' }));

  app.set('trust proxy', 1);

  app.use(cookieParser()); // parse cookies


  app.use(express.urlencoded({ limit: '50mb', extended: true }));

  app.use(
    session({
      secret: COOKIE_KEYS,
      name: 'caucus-session',
      resave: false,
      saveUninitialized: false,
      cookie: {
        maxAge: 24 * 60 * 60 * 1000,
        sameSite: true,
        httpOnly: true,
        secure: false,
      },
    }),
  );

  // app.use(passport.initialize());
  // app.use(passport.session());
};

export default addMiddleWare;
