// import express from 'express';
import chalk from 'chalk';
import http from 'http';
import { port } from './config/keys';
// import useMiddleWare from './middleware/index';
// import apiRoutes from './routes/apiRoutes';
// import connectDB from './config/connectDatabase';
// import './Models/User';
// import socketioService from './service/socket-io-service';
// import passport from 'passport';
// import passportService from './service/passport';
// passportService(passport);
// import passportRoute from './routes/passportRoutes'
//import queueService from './service/queueSocket';
//import app from './queue/queue'
//import app from './queue/queueBull'

import app from './app'

// import app from './queue/queueTest'

//const app = express();
const httpServer = new http.Server(app);

// connectDB();
// useMiddleWare(app);

// require('./controllers/cronController').init(app)

// app.use(passport.initialize());
// app.use(passport.session());

// // require('./config/passportSetup')
// app.use('/image/', express.static('src/upload'))
// app.use('/api', apiRoutes);
// app.use('/web', passportRoute)
//socketioService(httpServer);
//queueService(httpServer);

httpServer.listen(port, () =>
  console.log(chalk.blueBright(`Express Server listening to port ${port}`)),
);
