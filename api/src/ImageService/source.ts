export const Con: any = {
  SOURCE_1: "SOURCE_1",
  SOURCE_2: "SOURCE_2",
  SOURCE_3: "SOURCE_3",
};

export const Source = [
  {
    id: Con.SOURCE_1,
    publicKey: "public_gL9h5Yg1NjXbpsp2bfFepdM3eOU=",
    privateKey: "private_XILa4wdjEIG1So9ZKiE5CGlCXg0=",
    urlEndpoint: "https://ik.imagekit.io/nmg2p396hcb",
    mail: "madhurelsion"
  },
  {
    id: Con.SOURCE_2,
    publicKey: "public_xy4mCmqB1lRSivtpjvhuSs7nyjc=",
    privateKey: "private_o7YmBkvnOB0EMuFGiXM4oKBOLOU=",
    urlEndpoint: "https://ik.imagekit.io/yfeoreioobv",
    mail: "heytech"
  },
  {
    id: Con.SOURCE_3,
    publicKey: "public_VlhiEAduputNQXjba45Lty92Wds=",
    privateKey: "private_mxj9T6T8dO1LC0NRvv+OPyEkF0U=",
    urlEndpoint: "https://ik.imagekit.io/5w4xz6iz0p0",
    mail: "anaya"
  },
];
