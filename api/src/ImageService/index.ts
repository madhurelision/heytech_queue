const { Con, Source } = require('./source');
var ImageKit = require("imagekit");

export const app = async (source_id: any) => {
    var imagekit
    console.log(source_id)
    switch (source_id) {
        case Con.SOURCE_1: {
            imagekit = await new ImageKit({

                publicKey: Source[0].publicKey,

                privateKey: Source[0].privateKey,

                urlEndpoint: Source[0].urlEndpoint

            });
            break;
        }
        case Con.SOURCE_2: {
            imagekit = await new ImageKit({

                publicKey: Source[1].publicKey,

                privateKey: Source[1].privateKey,

                urlEndpoint: Source[1].urlEndpoint

            });
            break;
        }
        case Con.SOURCE_3: {
            imagekit = await new ImageKit({

                publicKey: Source[2].publicKey,

                privateKey: Source[2].privateKey,

                urlEndpoint: Source[2].urlEndpoint

            });
            break;
        }
    }
    return imagekit
}