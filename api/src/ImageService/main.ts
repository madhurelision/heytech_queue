import { app } from './index'
import fs from 'fs'
var ImageKit = require("imagekit");



export const ImageUpload = async (source: any, filePath: any, fileName: any) => {
    try {
        ImageKit = await app(source)
        const response = await uploadLocalFile(ImageKit, filePath, fileName);
        return { response, source: source }
    } catch (e) {
        console.log('Image Upload ImagekIt', e)
    }

}


const uploadLocalFile = async (imagekitInstance: any, filePath: any, fileName: any) => {
    const file = fs.createReadStream(filePath);
    const response = await imagekitInstance.upload({ file, fileName });
    return response;
};

export const removeImage = (ss: any) => {
    fs.unlink(ss, (err) => {
        if (err) {
            console.error('Image Removed Err', err)
            return
        }
        console.log('File Removed')
    })
}