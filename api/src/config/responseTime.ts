import { Request, Response, NextFunction } from 'express'
import { QueryLogModel } from '../Models/LogData';
import chalk from 'chalk';

export const responseTime = ((req: Request, res: Response, next: NextFunction) => {
  const startTime = process.hrtime();

  res.on('finish', async () => {
    const totalTime = process.hrtime(startTime);
    const totalTimeInMs = totalTime[0] * 1000 + totalTime[1] / 1e6;
    console.log(`${chalk.green(req.path) + ' : '}${' ' + chalk.blueBright(totalTimeInMs)}`);
    const qlog = await QueryLogModel.create({
      method: req.method,
      content: req.headers['content-length'],
      type: req.headers['content-type'],
      url: req.originalUrl || req.url,
      agent: req.headers['user-agent'],
      ip: req.ip,
      http: req.httpVersionMajor + '.' + req.httpVersionMinor,
      referer: req.headers.referer || req.headers.referrer,
      header: req.headers,
      startTime: startTime,
      endTime: totalTime,
      endTimeMS: totalTimeInMs
    })
  });

  next();
})