import jwt from 'jsonwebtoken'
import { Request, Response, NextFunction } from 'express'
import { CustomerModel } from '../Models/Customer';
import { ShopModel, UserModel } from '../Models/User';
import { adminStatus, DATE_LOCAL, EmployeeIdGenerate, makeSingleArray, SECRET_KEY } from './keys';
import mongoose from 'mongoose'
import { AdminModel } from '../Models/Admin';
import { EmployeeModel } from '../Models/Employee';
import { ServiceModel } from '../Models/Service';
import { LoginLogModel } from '../Models/LoginLog';
import config from 'config';

export const ShopVerify = (req: Request, res: Response, next: NextFunction) => {
  const token = req.header('auth-token');

  if (!token || token == null || token == undefined || token == 'null' || token == 'undefined') return res.status(400).json({
    status: "Failed",
    status_code: res.statusCode,
    message: "Access Denied",
    data: 'Token Not Found'
  })

  try {
    const verified: any = jwt.verify(token, SECRET_KEY);
    console.log(verified)

    UserModel.findOne({ _id: verified._id }).then((item: any) => {

      if (item == null || item == undefined || item.signup_completed == false || item.signup_completed == 'false') {
        return res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Invalid Token",
          data: 'Token Not Found'
        })
      } else {

        req.user = item;
        next()
      }

    }).catch((err) => {
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Invalid Token",
        data: err
      })
    })


  } catch (err) {
    //console.log('Token Err', err)
    res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: err
    })
  }


}

export const CustomerVerify = (req: Request, res: Response, next: NextFunction) => {
  const token = req.header('auth-token');

  if (!token || token == null || token == undefined || token == 'null' || token == 'undefined') return res.status(400).json({
    status: "Failed",
    status_code: res.statusCode,
    message: "Access Denied",
    data: 'Token Not Found'
  })

  try {
    const verified: any = jwt.verify(token, SECRET_KEY);
    console.log(verified)

    CustomerModel.findOne({ _id: verified._id }).then((item: any) => {

      if (item == null || item == undefined) {
        return res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Invalid Token",
          data: 'Token Not Found'
        })
      } else {

        req.user = verified;
        next()
      }

    }).catch((err) => {
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Invalid Token",
        data: err
      })
    })


  } catch (err) {
    res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: err
    })
  }


}

export const BookingVerify = (req: Request, res: Response, next: NextFunction) => {

  console.log('customer booking data', req.body.appointment_date, new Date(), req.body.appointment_time, new Date(req.body.appointment_date).getDate(), parseInt(req.body.appointment_time.replace(/\-.*/, '')))

  if (new Date().getDate() == new Date(req.body.appointment_date).getDate() && new Date(req.body.appointment_date).getHours() >= parseInt(req.body.appointment_time.replace(/\-.*/, ''))) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Booking Appointment Date for this Time is not Available",
      data: 'Booking Appointment Date for this Time is not Available'
    })
  }

  var email = req.body.user_email
  var mobile = req.body.mobile

  if (!email || email == null || email == undefined || email == 'null' || email == 'undefined') {

    try {

      CustomerModel.findOne({ mobile: mobile }).then((item: any) => {

        if (item == null || item == undefined) {

          const userData = new CustomerModel({
            mobile: mobile,
            name: 'guest' + EmployeeIdGenerate(),
            first_name: 'guest' + EmployeeIdGenerate(),
          })

          userData.save().then((cc: any) => {

            req.user = cc;
            next()

          }).catch((err: Error) => {
            console.log('catch booking verify customer create err', err)
            res.status(400).json({
              status: "Failed",
              status_code: res.statusCode,
              message: "Something Went Wrong Please try after some time",
              data: err
            })
          })


        } else {

          req.user = item;
          next()
        }

      }).catch((err) => {
        console.log('catch booking verify customer find err', err)
        res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Something Went Wrong Please try after some time",
          data: err
        })
      })


    } catch (err) {
      console.log('catch booking verify err', err)
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Booking Failed",
        data: err
      })

    }

  } else {


    try {

      CustomerModel.findOne({ email: email }).then((item: any) => {

        if (item == null || item == undefined) {

          const userData = new CustomerModel({
            email: email,
            name: 'guest' + EmployeeIdGenerate(),
            first_name: 'guest' + EmployeeIdGenerate(),
          })

          userData.save().then((cc: any) => {

            req.user = cc;
            next()

          }).catch((err: Error) => {
            console.log('catch booking verify customer create err', err)
            res.status(400).json({
              status: "Failed",
              status_code: res.statusCode,
              message: "Something Went Wrong Please try after some time",
              data: err
            })
          })


        } else {

          req.user = item;
          next()
        }

      }).catch((err) => {
        console.log('catch booking verify customer find err', err)
        res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Something Went Wrong Please try after some time",
          data: err
        })
      })


    } catch (err) {
      console.log('catch booking verify err', err)
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Booking Failed",
        data: err
      })

    }

  }

}


export const BookingQueueVerify = async (req: Request, res: Response, next: NextFunction) => {

  var main: any = req.body

  const check = bookingFormValidate(main)
  if (Object.keys(check).length > 0) return res.status(400).json({
    status: "Failed",
    status_code: res.statusCode,
    message: "All Fields are Required",
    data: check
  })
  // const check = bookingFormValidate(main)
  // console.log('Err', check)
  // if (Object.keys(check).length > 0) return res.status(400).json({
  //   status: "Failed",
  //   status_code: res.statusCode,
  //   message: "All fields are Required",
  //   data: check
  // })
  console.log('time', parseInt(main.appointment_time.replace(/\:.*/, '')))
  var name: any = 'time_slot.' + dayOfWeek[new Date(main.appointment_date).getDay()] + '.'
  console.log('timeslot', name)
  console.log('timeslot', req.body)

  var email = req.body.user_email
  var mobile = req.body.mobile

  if (!email || email == null || email == undefined || email == 'null' || email == 'undefined') {

    try {

      const matchData = await ShopModel.aggregate([
        {
          '$match': {
            'user_information': new mongoose.Types.ObjectId(main.shop_id),
            //`${name}available`: true
          }
        }, {
          '$addFields': {
            'range': {
              '$range': [
                `$${name}start_hour`, `$${name}end_hour`, 1
              ]
            }
          }
        }, {
          '$match': {
            'range': {
              '$in': [
                parseInt(main.appointment_time.replace(/\:.*/, ''))
              ]
            }
          }
        }
      ]).exec()

      console.log('matchData', matchData)

      if (matchData.length == 0) {
        return res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Booking Appointment Date for this Time is not Available",
          data: 'Booking Appointment Date for this Time is not Available'
        })
      }

      const serviceCheck = await ServiceModel.findOne({ _id: main.service_id, user_id: main.shop_id })

      if (serviceCheck == null || serviceCheck == undefined) {
        return res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Booking Appointment Service for this Time is not Available",
          data: 'Booking Appointment Service for this Time is not Available'
        })
      }

      if (serviceCheck.accept == false) {
        return res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: `Booking Appointment for ${serviceCheck.name} service for this Time is not Available`,
          data: `Booking Appointment for ${serviceCheck.name} service for this Time is not Available`
        })
      }


      CustomerModel.findOne({ mobile: mobile }).then((item: any) => {

        if (item == null || item == undefined) {

          const userData = new CustomerModel({
            mobile: mobile,
            name: 'guest' + EmployeeIdGenerate(),
            first_name: 'guest' + EmployeeIdGenerate(),
          })

          userData.save().then((cc: any) => {
            var q = {
              _id: cc._id
            }
            req.user = { ...q, service: serviceCheck };
            next()

          }).catch((err: Error) => {
            console.log('catch booking verify customer create err', err)
            res.status(400).json({
              status: "Failed",
              status_code: res.statusCode,
              message: "Something Went Wrong Please try after some time",
              data: err
            })
          })


        } else {
          var s = {
            _id: item._id
          }
          req.user = { ...s, service: serviceCheck };
          next()
        }

      }).catch((err) => {
        console.log('catch booking verify customer find err', err)
        res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Something Went Wrong Please try after some time",
          data: err
        })
      })


    } catch (err) {
      console.log('catch booking verify err', err)
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Booking Appointment Date for this Time is not Available",
        data: err
      })

    }

  } else {
    try {

      const matchData = await ShopModel.aggregate([
        {
          '$match': {
            'user_information': new mongoose.Types.ObjectId(main.shop_id),
            //`${name}available`: true
          }
        }, {
          '$addFields': {
            'range': {
              '$range': [
                `$${name}start_hour`, `$${name}end_hour`, 1
              ]
            }
          }
        }, {
          '$match': {
            'range': {
              '$in': [
                parseInt(main.appointment_time.replace(/\:.*/, ''))
              ]
            }
          }
        }
      ]).exec()

      console.log('matchData', matchData)

      if (matchData.length == 0) {
        return res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Booking Appointment Date for this Time is not Available",
          data: 'Booking Appointment Date for this Time is not Available'
        })
      }

      const serviceCheck = await ServiceModel.findOne({ _id: main.service_id, user_id: main.shop_id })

      if (serviceCheck == null || serviceCheck == undefined) {
        return res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Booking Appointment Service for this Time is not Available",
          data: 'Booking Appointment Service for this Time is not Available'
        })
      }

      if (serviceCheck.accept == false) {
        return res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: `Booking Appointment for ${serviceCheck.name} service for this Time is not Available`,
          data: `Booking Appointment for ${serviceCheck.name} service for this Time is not Available`
        })
      }


      CustomerModel.findOne({ email: email }).then((item: any) => {

        if (item == null || item == undefined) {

          const userData = new CustomerModel({
            email: email,
            name: 'guest' + EmployeeIdGenerate(),
            first_name: 'guest' + EmployeeIdGenerate(),
          })

          userData.save().then((cc: any) => {
            var c = {
              _id: cc._id
            }
            req.user = { ...c, service: serviceCheck };
            next()

          }).catch((err: Error) => {
            console.log('catch booking verify customer create err', err)
            res.status(400).json({
              status: "Failed",
              status_code: res.statusCode,
              message: "Something Went Wrong Please try after some time",
              data: err
            })
          })


        } else {
          var k = {
            _id: item._id
          }
          req.user = { ...k, service: serviceCheck };
          next()
        }

      }).catch((err) => {
        console.log('catch booking verify customer find err', err)
        res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Something Went Wrong Please try after some time",
          data: err
        })
      })


    } catch (err) {
      console.log('catch booking verify err', err)
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Booking Appointment Date for this Time is not Available",
        data: err
      })

    }
  }


}

export const newBookingQueueVerify = (req: Request, res: Response, next: NextFunction) => {

  var main: any = req.body
  // var mnc = config.get('PROD') == true ? new Date().setMinutes(new Date().getMinutes() + 330) : new Date()
  // console.log('timing', mnc)
  main.appointment_date = new Date()
  main.appointment_time = new Date(DATE_LOCAL()).getHours() + ':' + new Date(DATE_LOCAL()).getMinutes()
  const check = bookingFormValidate(main)
  if (Object.keys(check).length > 0) return res.status(400).json({
    status: "Failed",
    status_code: res.statusCode,
    message: "All Fields are Required",
    data: check
  })

  console.log('time', parseInt(main.appointment_time.replace(/\:.*/, '')))
  var name: any = 'time_slot.' + dayOfWeek[new Date(main.appointment_date).getDay()] + '.'
  console.log('timeslot', name)
  //console.log('timeslot', req.body)

  var email = req.body.user_email
  var mobile = req.body.mobile

  const promise1 = new Promise((resolve, reject) => {
    ShopModel.aggregate([
      {
        '$match': {
          'user_information': new mongoose.Types.ObjectId(main.shop_id)
        }
      }, {
        '$addFields': {
          'range': {
            '$range': [
              `$${name}start_hour`, `$${name}end_hour`, 1
            ]
          }
        }
      }, {
        '$match': {
          'range': {
            '$in': [
              parseInt(main.appointment_time.replace(/\:.*/, ''))
            ]
          }
        }
      }
    ]).exec((err, item) => {

      if (err) {
        console.log('shopD Err', err)
        reject(err)
        return
      }

      console.log('shopD', item)
      resolve(item)

    })

  })

  const promise2 = new Promise((resolve, reject) => {
    ServiceModel.findOne({ _id: main.service_id, user_id: main.shop_id }).then((item) => {

      console.log('serviceD', item)
      resolve(item)
    }).catch((err) => {
      console.log('serviceD Err', err)
      reject(err)
    })
  })

  Promise.allSettled([promise1, promise2]).then((cc: any) => {

    const matchData = cc[0].value
    const serviceCheck = cc[1].value

    console.log('matchData', matchData)
    // console.log('timing', mnc)
    console.log('main Date Check TS', new Date())
    console.log('timing', main.appointment_time)

    if (matchData.length == 0) {
      return res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Booking Appointment Date for this Time is not Available",
        data: 'Booking Appointment Date for this Time is not Available'
      })
    }

    if (matchData[0].open == false) {
      return res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Booking Appointment Date for this Time is not Available",
        data: 'Booking Appointment Date for this Time is not Available'
      })
    }

    if (serviceCheck == null || serviceCheck == undefined) {
      return res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Booking Appointment Service for this Time is not Available",
        data: 'Booking Appointment Service for this Time is not Available'
      })
    }

    if (serviceCheck.accept == false) {
      return res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: `Booking Appointment for ${serviceCheck.name} service for this Time is not Available`,
        data: `Booking Appointment for ${serviceCheck.name} service for this Time is not Available`
      })
    }

    if (!email || email == null || email == undefined || email == 'null' || email == 'undefined') {

      try {

        CustomerModel.findOne({ mobile: mobile }).then((item: any) => {

          if (item == null || item == undefined) {

            const userData = new CustomerModel({
              mobile: mobile,
              name: 'guest' + EmployeeIdGenerate(),
              first_name: 'guest' + EmployeeIdGenerate(),
            })

            userData.save().then((cc: any) => {
              var d = {
                _id: cc._id
              }
              req.user = { ...d, service: serviceCheck, date: { appointment_date: main.appointment_date, appointment_time: main.appointment_time }, shopData: matchData[0] };
              next()

            }).catch((err: Error) => {
              console.log('catch booking verify customer create err', err)
              res.status(400).json({
                status: "Failed",
                status_code: res.statusCode,
                message: "Something Went Wrong Please try after some time",
                data: err
              })
            })


          } else {
            var c = {
              _id: item._id
            }

            req.user = { ...c, service: serviceCheck, date: { appointment_date: main.appointment_date, appointment_time: main.appointment_time }, shopData: matchData[0] };
            next()
          }

        }).catch((err) => {
          console.log('catch booking verify customer find err', err)
          res.status(400).json({
            status: "Failed",
            status_code: res.statusCode,
            message: "Something Went Wrong Please try after some time",
            data: err
          })
        })


      } catch (err) {
        console.log('catch booking verify err', err)
        res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Booking Appointment Date for this Time is not Available",
          data: err
        })

      }

    } else {
      try {


        CustomerModel.findOne({ email: email }).then((item: any) => {

          if (item == null || item == undefined) {

            const userData = new CustomerModel({
              email: email,
              name: 'guest' + EmployeeIdGenerate(),
              first_name: 'guest' + EmployeeIdGenerate(),
            })

            userData.save().then((cc: any) => {
              var m = {
                _id: cc._id
              }
              req.user = { ...m, service: serviceCheck, date: { appointment_date: main.appointment_date, appointment_time: main.appointment_time }, shopData: matchData[0] };
              next()

            }).catch((err: Error) => {
              console.log('catch booking verify customer create err', err)
              res.status(400).json({
                status: "Failed",
                status_code: res.statusCode,
                message: "Something Went Wrong Please try after some time",
                data: err
              })
            })


          } else {
            var k = {
              _id: item._id
            }
            req.user = { ...k, service: serviceCheck, date: { appointment_date: main.appointment_date, appointment_time: main.appointment_time }, shopData: matchData[0] };
            next()
          }

        }).catch((err) => {
          console.log('catch booking verify customer find err', err)
          res.status(400).json({
            status: "Failed",
            status_code: res.statusCode,
            message: "Something Went Wrong Please try after some time",
            data: err
          })
        })


      } catch (err) {
        console.log('catch booking verify err', err)
        res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Booking Appointment Date for this Time is not Available",
          data: err
        })

      }
    }

  }).catch((err) => {
    console.log('catch booking verify err', err)
    res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Booking Appointment Date for this Time is not Available",
      data: err
    })
  })

}

export const testBookingQueueVerify = (req: Request, res: Response, next: NextFunction) => {

  var main: any = req.body
  main.appointment_date = new Date()
  main.appointment_time = new Date().getHours() + ':' + new Date().getMinutes()
  const check = bookingFormValidate(main)
  if (Object.keys(check).length > 0) return res.status(400).json({
    status: "Failed",
    status_code: res.statusCode,
    message: "All Fields are Required",
    data: check
  })
  console.log('time', parseInt(main.appointment_time.replace(/\:.*/, '')))
  var name: any = 'time_slot.' + dayOfWeek[new Date(main.appointment_date).getDay()] + '.'
  console.log('timeslot', name)
  console.log('timeslot', req.body)

  var email = req.body.user_email
  var mobile = req.body.mobile

  const promise1 = new Promise((resolve, reject) => {
    ShopModel.aggregate([
      {
        '$match': {
          'user_information': new mongoose.Types.ObjectId(main.shop_id)
        }
      }, {
        '$addFields': {
          'range': {
            '$range': [
              `$${name}start_hour`, `$${name}end_hour`, 1
            ]
          }
        }
      }, {
        '$match': {
          'range': {
            '$in': [
              parseInt(main.appointment_time.replace(/\:.*/, ''))
            ]
          }
        }
      }
    ]).exec((err, item) => {

      if (err) {
        console.log('shopD Err', err)
        reject(err)
        return
      }

      console.log('shopD', item)
      resolve(item)

    })

  })

  const promise2 = new Promise((resolve, reject) => {
    ServiceModel.findOne({ _id: main.service_id, user_id: main.shop_id }).then((item) => {

      console.log('serviceD', item)
      resolve(item)
    }).catch((err) => {
      console.log('serviceD Err', err)
      reject(err)
    })
  })

  Promise.allSettled([promise1, promise2]).then((cc: any) => {

    const matchData = cc[0].value
    const serviceCheck = cc[1].value

    console.log('matchData', matchData)

    if (matchData.length == 0) {
      return res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Booking Appointment Date for this Time is not Available",
        data: 'Booking Appointment Date for this Time is not Available'
      })
    }

    if (serviceCheck == null || serviceCheck == undefined) {
      return res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Booking Appointment Service for this Time is not Available",
        data: 'Booking Appointment Service for this Time is not Available'
      })
    }

    if (serviceCheck.accept == false) {
      return res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: `Booking Appointment for ${serviceCheck.name} service for this Time is not Available`,
        data: `Booking Appointment for ${serviceCheck.name} service for this Time is not Available`
      })
    }

    if (!email || email == null || email == undefined || email == 'null' || email == 'undefined') {

      try {

        CustomerModel.findOne({ mobile: mobile }).then((item: any) => {

          if (item == null || item == undefined) {

            const userData = new CustomerModel({
              mobile: mobile,
              name: 'guest' + EmployeeIdGenerate(),
              first_name: 'guest' + EmployeeIdGenerate(),
            })

            userData.save().then((cc: any) => {
              var d = {
                _id: cc._id
              }
              req.user = { ...d, service: serviceCheck, date: { appointment_date: main.appointment_date, appointment_time: main.appointment_time }, shopData: matchData[0] };
              next()

            }).catch((err: Error) => {
              console.log('catch booking verify customer create err', err)
              res.status(400).json({
                status: "Failed",
                status_code: res.statusCode,
                message: "Something Went Wrong Please try after some time",
                data: err
              })
            })


          } else {
            var c = {
              _id: item._id
            }

            req.user = { ...c, service: serviceCheck, date: { appointment_date: main.appointment_date, appointment_time: main.appointment_time }, shopData: matchData[0] };
            next()
          }

        }).catch((err) => {
          console.log('catch booking verify customer find err', err)
          res.status(400).json({
            status: "Failed",
            status_code: res.statusCode,
            message: "Something Went Wrong Please try after some time",
            data: err
          })
        })


      } catch (err) {
        console.log('catch booking verify err', err)
        res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Booking Appointment Date for this Time is not Available",
          data: err
        })

      }

    } else {
      try {


        CustomerModel.findOne({ email: email }).then((item: any) => {

          if (item == null || item == undefined) {

            const userData = new CustomerModel({
              email: email,
              name: 'guest' + EmployeeIdGenerate(),
              first_name: 'guest' + EmployeeIdGenerate(),
            })

            userData.save().then((cc: any) => {
              var m = {
                _id: cc._id
              }
              req.user = { ...m, service: serviceCheck, date: { appointment_date: main.appointment_date, appointment_time: main.appointment_time }, shopData: matchData[0] };
              next()

            }).catch((err: Error) => {
              console.log('catch booking verify customer create err', err)
              res.status(400).json({
                status: "Failed",
                status_code: res.statusCode,
                message: "Something Went Wrong Please try after some time",
                data: err
              })
            })


          } else {
            var k = {
              _id: item._id
            }
            req.user = { ...k, service: serviceCheck, date: { appointment_date: main.appointment_date, appointment_time: main.appointment_time }, shopData: matchData[0] };
            next()
          }

        }).catch((err) => {
          console.log('catch booking verify customer find err', err)
          res.status(400).json({
            status: "Failed",
            status_code: res.statusCode,
            message: "Something Went Wrong Please try after some time",
            data: err
          })
        })


      } catch (err) {
        console.log('catch booking verify err', err)
        res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Booking Appointment Date for this Time is not Available",
          data: err
        })

      }
    }

  }).catch((err) => {
    console.log('catch booking verify err', err)
    res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Booking Appointment Date for this Time is not Available",
      data: err
    })
  })

}

export const AdminVerify = (req: Request, res: Response, next: NextFunction) => {
  const token = req.header('auth-token');

  if (!token || token == null || token == undefined || token == 'null' || token == 'undefined') return res.status(400).json({
    status: "Failed",
    status_code: res.statusCode,
    message: "Access Denied",
    data: 'Token Not Found'
  })

  try {
    const verified: any = jwt.verify(token, SECRET_KEY);
    console.log(verified)

    AdminModel.findOne({ _id: verified._id }).populate('feature').then((item: any) => {

      if (item == null || item == undefined) {
        return res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Invalid Token",
          data: 'Token Not Found'
        })
      } else {

        if (item.access == true || item.deleted == false) {

          const featureD = makeSingleArray(item?.feature)
          const ss = req.url.split('/')[1]
          if (featureD.includes('/' + ss)) {
            req.user = item;
            next()
          } else {
            return res.status(400).json({
              status: "Failed",
              status_code: res.statusCode,
              message: "You Dont Have Acces For This Api.....",
              data: 'You Dont Have Acces For This Api.....'
            })
          }


        } else {
          return res.status(400).json({
            status: "Failed",
            status_code: res.statusCode,
            message: "You Have Not Access",
            data: 'You Have Not Access'
          })
        }
      }

    }).catch((err) => {
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Invalid Token",
        data: err
      })
    })


  } catch (err) {
    res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: err
    })
  }


}

export const EmployeeVerify = async (req: Request, res: Response, next: NextFunction) => {
  const token = req.header('auth-token');

  if (!token || token == null || token == undefined || token == 'null' || token == 'undefined') return res.status(400).json({
    status: "Failed",
    status_code: res.statusCode,
    message: "Access Denied",
    data: 'Token Not Found'
  })

  try {
    const verified: any = jwt.verify(token, SECRET_KEY);
    console.log(verified, 'date Check', new Date(verified.date).getDate(), new Date().getDate())

    if (new Date(verified.date).getDate() == new Date().getDate()) {

      EmployeeModel.findOne({ _id: verified._id }).then((item: any) => {

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: "Failed",
            status_code: res.statusCode,
            message: "Invalid Token",
            data: 'Token Not Found'
          })
        } else {

          if (item.status == false) {
            return res.status(400).json({
              status: "Failed",
              status_code: res.statusCode,
              message: "Your Account is Deactivated",
              data: 'Token Not Found'
            })
          }

          req.user = item;
          next()
        }

      }).catch((err) => {
        res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Invalid Token",
          data: err
        })
      })

    } else {
      const emploLogin = await LoginLogModel.create({
        user_id: verified._id,
        user_type: 'Employee',
        type: 'Logout',
        token: token,
        message: 'Token Invalid',
      })
      return res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Invalid Token",
        data: 'Token Invalid'
      })
    }



  } catch (err) {
    res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: err
    })
  }


}

export const customerCreateVerify = (req: Request, res: Response, next: NextFunction) => {

  var email = req.body.email
  var mobile = req.body.mobile

  if (!email || email == null || email == undefined || email == 'null' || email == 'undefined') {

    try {

      CustomerModel.findOne({ mobile: mobile }).then((item: any) => {

        if (item == null || item == undefined) {

          const userData = new CustomerModel({
            mobile: mobile,
            email: email,
            name: req.body.name,
            first_name: req.body.name,
          })

          userData.save().then((cc: any) => {
            req.user = cc;
            next()

          }).catch((err: Error) => {
            console.log('catch payment customer create err', err)
            res.status(400).json({
              status: "Failed",
              status_code: res.statusCode,
              message: "Something Went Wrong Please try after some time",
              data: err
            })
          })


        } else {

          req.user = item;
          next()
        }

      }).catch((err) => {
        console.log('catch payment verify customer find err', err)
        res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Something Went Wrong Please try after some time",
          data: err
        })
      })


    } catch (err) {
      console.log('catch payment verify err', err)
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Something Went Wrong Please try after some time",
        data: err
      })

    }

  } else {
    try {


      CustomerModel.findOne({ email: email }).then((item: any) => {

        if (item == null || item == undefined) {

          const userData = new CustomerModel({
            email: email,
            mobile: mobile,
            name: req.body.name,
            first_name: req.body.name,
          })

          userData.save().then((cc: any) => {
            req.user = cc;
            next()

          }).catch((err: Error) => {
            console.log('catch payment verify customer create err', err)
            res.status(400).json({
              status: "Failed",
              status_code: res.statusCode,
              message: "Something Went Wrong Please try after some time",
              data: err
            })
          })


        } else {
          req.user = item;
          next()
        }

      }).catch((err) => {
        console.log('catch payment verify customer find err', err)
        res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Something Went Wrong Please try after some time",
          data: err
        })
      })


    } catch (err) {
      console.log('catch payment verify err', err)
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Something Went Wrong Please try after some time",
        data: err
      })

    }
  }

}

export const shopCreateVerify = (req: Request, res: Response, next: NextFunction) => {


  var email = req.body.email
  var mobile = req.body.mobile

  if (!email || email == null || email == undefined || email == 'null' || email == 'undefined') {

    try {

      UserModel.findOne({ mobile: mobile }).then((item: any) => {

        if (item == null || item == undefined) {

          const userData = new UserModel({
            mobile: mobile,
            email: email,
            first_name: req.body.name,
            last_name: ''
          })

          userData.save().then((cc: any) => {
            req.user = cc;
            next()

          }).catch((err: Error) => {
            console.log('catch payment customer create err', err)
            res.status(400).json({
              status: "Failed",
              status_code: res.statusCode,
              message: "Something Went Wrong Please try after some time",
              data: err
            })
          })


        } else {

          req.user = item;
          next()
        }

      }).catch((err) => {
        console.log('catch payment verify customer find err', err)
        res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Something Went Wrong Please try after some time",
          data: err
        })
      })


    } catch (err) {
      console.log('catch payment verify err', err)
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Something Went Wrong Please try after some time",
        data: err
      })

    }

  } else {
    try {


      UserModel.findOne({ email: email }).then((item: any) => {

        if (item == null || item == undefined) {

          const userData = new UserModel({
            email: email,
            mobile: mobile,
            first_name: req.body.name,
            last_name: ''
          })

          userData.save().then((cc: any) => {
            req.user = cc;
            next()

          }).catch((err: Error) => {
            console.log('catch payment verify customer create err', err)
            res.status(400).json({
              status: "Failed",
              status_code: res.statusCode,
              message: "Something Went Wrong Please try after some time",
              data: err
            })
          })


        } else {
          req.user = item;
          next()
        }

      }).catch((err) => {
        console.log('catch payment verify customer find err', err)
        res.status(400).json({
          status: "Failed",
          status_code: res.statusCode,
          message: "Something Went Wrong Please try after some time",
          data: err
        })
      })


    } catch (err) {
      console.log('catch payment verify err', err)
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Something Went Wrong Please try after some time",
        data: err
      })

    }
  }

}

const dayOfWeek = [
  'sunday',
  'monday',
  'tuesday',
  'wednesday',
  'thursday',
  'friday',
  'saturday',
];

const bookingFormValidate = (val: any) => {
  let errors: any = {}

  if (!val.service_id) {
    errors['service_id'] = "Service is Required";
  }

  if (!val.appointment_date) {
    errors['appointment_date'] = "Appointment Date is Required";
  }

  if (!val.appointment_time) {
    errors['appointment_time'] = "Appointment Time is Required";
  }

  if (!val.shop_id) {
    errors['shop_id'] = "Shop Id is Required";
  }

  if (!val.description) {
    errors['description'] = "Description is Required";
  }

  if (!val.user_email && !val.mobile) {
    errors['user_email'] = "User Email is Required";
    errors['mobile'] = "Mobile Number is Required";
  }

  if (val.user_email) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    if (!pattern.test(val["user_email"])) {
      errors["user_email"] = "Please enter valid email.";
    }
  }

  if (!val.size) {
    errors['size'] = "User Size is Required";
  }

  if (val.size) {
    if (parseInt(val.size) > 50) {
      errors['size'] = `User Size must be less then or equal to 50`;
    }
  }

  if (!val.user_email) {
    if (!val.mobile) {
      errors['mobile'] = "Mobile Number is Required";
    }

    if (val.mobile) {
      if (!val["mobile"].match(/^[0-9]{10}$/)) {
        errors["mobile"] = "Please enter valid mobile no.";
      }
    }
  }

  return errors
}
