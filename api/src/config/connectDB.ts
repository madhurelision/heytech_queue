import express, { Express } from 'express'
import { MongoClient } from "mongodb";
import { DATABASE_URL as mongoURI } from './keys';
import { grey, redBright } from 'chalk';

const client = new MongoClient(mongoURI);

const init = async (app: Express) => {
  try {
    await client.connect();
    console.log(grey('MongoDB Connected S :D'));
    app.set('client', client);
  } catch (err) {
    if (err instanceof Error) console.error(redBright(err.message));
    process.exit(1);
  }
};


export default init;
