import { NextFunction, Request, Response } from 'express';
import { IpDetailModel } from '../Models/ShortUrl';
import { Main } from './location';


export const saveIpDetail = async (req: Request, id: any, val: any) => {

  var value: any = {
    ip: (req.headers['x-forwarded-for'] == undefined && req.hostname == 'localhost') ? '127.0.0.1' : req.ip,
    header: req.headers,
    method: req.method,
    ipDetail: {},
    type: val,
    content: req.headers['content-length'],
    ctype: req.headers['content-type'],
    url: req.originalUrl || req.url,
    agent: req.headers['user-agent'],
    http: req.httpVersionMajor + '.' + req.httpVersionMinor,
    referer: req.headers.referer || req.headers.referrer,
    params: req.params,
    body: req.body
  }

  await Main(Test, req.ip)

  async function Test(status: any, err: Error, cc: any) {
    console.log('Ip Details', cc)
    value.ipDetail = (cc == null || cc == undefined) ? {} : cc

    value.request_id = id

    console.log('Check value', value)

    const saveData = await IpDetailModel.create(value)

    console.log('Created Detail of Ip Req', saveData)
  }

}