const multer = require('multer')
const fs = require('fs')
import { nanoid } from 'nanoid';


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    console.log('Multer Destination consolse', file)
    fs.mkdirSync('./src/upload', { recursive: true })
    cb(null, './src/upload');
  },
  filename: (req, file, cb) => {
    console.log('Multer Filename consolse', file)
    const fileName = file.originalname.toLowerCase().split(' ').join('-');
    cb(null, nanoid() + '-' + fileName)
  }
});

export const upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 5 }
})