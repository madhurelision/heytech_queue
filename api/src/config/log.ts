import { createLogger, transports, format } from 'winston';
import path from 'path';
import { NextFunction, Request, Response } from 'express';
import DailyRotateFile from 'winston-daily-rotate-file';
import { LogModel } from '../Models/LogData';
import axios from 'axios';
import config from 'config'
import { LOG_SERVICE } from './keys';

const transport: DailyRotateFile = new DailyRotateFile({
  filename: path.join(__dirname, '../log/application-%DATE%.log'),
  datePattern: 'YYYY-MM-DD',
  //zippedArchive: true,
  maxSize: '20m',
});

transport.on('rotate', function (oldFilename, newFilename) {
  // do something fun
});

const wLog = createLogger({
  transports: [
    transport
  ]
});

const logger = createLogger({
  transports: [
    new transports.File({
      filename: path.join(__dirname, '../log/info.log'),
      level: 'info',
      format: format.combine(format.timestamp(), format.json())
    }),
    new transports.File({
      filename: path.join(__dirname, '../log/error.log'),
      level: 'error',
      format: format.combine(format.timestamp(), format.json())
    })
  ]
})

export class LoggerStream {
  write(message: string) {
    console.log('check', message,)
    logger.info(message.substring(0, message.lastIndexOf('\n')));
  }
}

export class MainLogFn {
  constructor() {
  }

  static async init() {
    if (logger == null || logger == undefined) {
      return false
    } else {
      return true
    }
  }

  static async write(req: Request, res: Response, next: NextFunction) {
    console.log('log', {
      method: req.method,
      url: req.originalUrl || req.url,
    })

    axios.post(config.has('LOG_SERVICE') ? config.get('LOG_SERVICE') + 'hey/serverLog' : LOG_SERVICE + 'hey/serverLog', {
      method: req.method,
      content: req.headers['content-length'],
      type: req.headers['content-type'],
      url: req.originalUrl || req.url,
      agent: req.headers['user-agent'],
      ip: req.ip,
      http: req.httpVersionMajor + '.' + req.httpVersionMinor,
      referer: req.headers.referer || req.headers.referrer,
      header: req.headers,
      params: req.params,
      body: req.body
    }).then((cc) => {
      next()

    }).catch((err) => {
      next()

    })

    // const check = await MainLogFn.init();
    // const ccD = await LogModel.create({
    //   method: req.method,
    //   content: req.headers['content-length'],
    //   type: req.headers['content-type'],
    //   url: req.originalUrl || req.url,
    //   agent: req.headers['user-agent'],
    //   ip: req.ip,
    //   http: req.httpVersionMajor + '.' + req.httpVersionMinor,
    //   referer: req.headers.referer || req.headers.referrer,
    //   header: req.headers,
    //   params: req.params,
    //   body: req.body
    // })
    // logger.info({
    //   method: req.method,
    //   content: req.headers['content-length'],
    //   type: req.headers['content-type'],
    //   url: req.originalUrl || req.url,
    //   agent: req.headers['user-agent'],
    //   ip: req.ip,
    //   http: req.httpVersionMajor + '.' + req.httpVersionMinor,
    //   referer: req.headers.referer || req.headers.referrer,
    //   date: new Date(),
    //   params: req.params,
    //   body: req.body
    // });
    // wLog.info({
    //   method: req.method,
    //   content: req.headers['content-length'],
    //   type: req.headers['content-type'],
    //   url: req.originalUrl || req.url,
    //   agent: req.headers['user-agent'],
    //   ip: req.ip,
    //   http: req.httpVersionMajor + '.' + req.httpVersionMinor,
    //   referer: req.headers.referer || req.headers.referrer,
    //   date: new Date(),
    //   params: req.params,
    //   body: req.body
    // })
  }
}