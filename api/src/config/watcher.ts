import { BookingModel } from "../Models/Booking";
import { ShopModel } from "../Models/User";
import { getShopCurrenQueue, shopQueueAndBooking, userQueueAndBooking } from "./function";
import { SERVER_URL } from "./keys";
import QRCode from 'qrcode'


const BookingUpdateOtp = BookingModel.watch([{
  $match: {
    $and: [
      { "updateDescription.updatedFields.otp_verify": true },
      { operationType: "update" }
    ]
  }
}])

const BookingStatusAccepted = BookingModel.watch([{
  $match: {
    $and: [
      { "updateDescription.updatedFields.status": 'accepted' },
      { operationType: "update" }
    ]
  }
}])

const BookingStatusCompleted = BookingModel.watch([{
  $match: {
    $and: [
      { "updateDescription.updatedFields.status": 'completed' },
      { operationType: "update" }
    ]
  }
}])

const BookingStatusExpired = BookingModel.watch([{
  $match: {
    $and: [
      { "updateDescription.updatedFields.status": 'expired' },
      { operationType: "update" }
    ]
  }
}])

const BookingTimeUpdated = BookingModel.watch([{
  $match: {
    $and: [
      { "updateDescription.updatedFields.waiting_number": { $exists: true } },
      { operationType: "update" }
    ]
  }
}])

const BookingStatusCancelled = BookingModel.watch([{
  $match: {
    $and: [
      { "updateDescription.updatedFields.status": 'cancelled' },
      { operationType: "update" }
    ]
  }
}])

const BookingCancelVerify = BookingModel.watch([{
  $match: {
    $and: [
      { "updateDescription.updatedFields.cancel_verify": { $exists: true } },
      { operationType: "update" }
    ]
  }
}])

const ShopUserCreate = ShopModel.watch([
  {
    '$match': {
      'operationType': 'insert',
    }
  }
])

const QueueTimeUpdated = BookingModel.watch([{
  $match: {
    $and: [
      { "updateDescription.updatedFields.approximate_date": { $exists: true } },
      { operationType: "update" }
    ]
  }
}])

const watcher = (io: any) => {
  console.log('Watcher Start')

  BookingUpdateOtp.on('change', async (data: any) => {

    BookingModel.findOne({ _id: data.documentKey._id }).populate('shop_id user_id service_id').then((item: any) => {

      if (item) {

        io.sockets.in('User' + item.user_id._id).emit('customerBooking', item)
        io.sockets.in('Shop' + item.shop_id._id).emit('shopBooking', item)

        userQueueAndBooking(item.user_id._id, (status: any, err: Error, val: any) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item.user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(item.shop_id._id, (status: any, err: Error, val: any) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item.shop_id._id).emit('shopCount', val)
        })

        getShopCurrenQueue(item.shop_id.mentor_information, (status: any, err: Error, val: any) => {
          if (err) {
            return
          }
          io.sockets.in('Shop' + item.shop_id._id).emit('shopValue', val)
          io.sockets.in('User' + item.user_id._id).emit('shopValue', val)
        })
      }

    }).catch((err) => {
      console.log('Watcher Otp Verify Err', err)
    })


  })

  BookingStatusAccepted.on('change', async (data: any) => {

    BookingModel.findOne({ _id: data.documentKey._id }).populate('shop_id user_id service_id').then((item: any) => {

      if (item) {

        io.sockets.in('User' + item.user_id._id).emit('customerBooking', item)
        io.sockets.in('Shop' + item.shop_id._id).emit('shopBooking', item)


        userQueueAndBooking(item.user_id._id, (status: any, err: Error, val: any) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item.user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(item.shop_id._id, (status: any, err: Error, val: any) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item.shop_id._id).emit('shopCount', val)
        })
      }

    }).catch((err) => {
      console.log('Watcher Booking Status Accepted Err', err)
    })

  })

  BookingStatusCompleted.on('change', async (data: any) => {

    BookingModel.findOne({ _id: data.documentKey._id }).populate('shop_id user_id service_id').then((item: any) => {

      if (item) {


        io.sockets.in('User' + item.user_id._id).emit('customerBookingRemove', item)
        io.sockets.in('Shop' + item.shop_id._id).emit('shopBookingRemove', item)


        userQueueAndBooking(item.user_id._id, (status: any, err: Error, val: any) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item.user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(item.shop_id._id, (status: any, err: Error, val: any) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item.shop_id._id).emit('shopCount', val)
        })
      }

    }).catch((err) => {
      console.log('Watcher Booking Status Completed Err', err)
    })

  })

  BookingStatusExpired.on('change', async (data: any) => {

    BookingModel.findOne({ _id: data.documentKey._id }).populate('shop_id user_id service_id').then((item: any) => {

      if (item) {


        io.sockets.in('User' + item.user_id._id).emit('customerBookingRemove', item)
        io.sockets.in('Shop' + item.shop_id._id).emit('shopBookingRemove', item)


        userQueueAndBooking(item.user_id._id, (status: any, err: Error, val: any) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item.user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(item.shop_id._id, (status: any, err: Error, val: any) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item.shop_id._id).emit('shopCount', val)
        })
      }

    }).catch((err) => {
      console.log('Watcher Booking Expired Err', err)
    })

  })

  BookingTimeUpdated.on('change', async (data: any) => {

    BookingModel.findOne({ _id: data.documentKey._id }).populate('shop_id user_id service_id').then((item: any) => {

      if (item) {

        io.sockets.in('User' + item.user_id._id).emit('customerBooking', item)
        io.sockets.in('Shop' + item.shop_id._id).emit('shopBooking', item)


        userQueueAndBooking(item.user_id._id, (status: any, err: Error, val: any) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item.user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(item.shop_id._id, (status: any, err: Error, val: any) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item.shop_id._id).emit('shopCount', val)
        })
      }

    }).catch((err) => {
      console.log('Watcher Booking Status Accepted Err', err)
    })

  })

  BookingStatusCancelled.on('change', async (data: any) => {

    BookingModel.findOne({ _id: data.documentKey._id }).populate('shop_id user_id service_id').then((item: any) => {

      if (item) {


        io.sockets.in('User' + item.user_id._id).emit('customerBookingRemove', item)
        io.sockets.in('Shop' + item.shop_id._id).emit('shopBookingRemove', item)


        userQueueAndBooking(item.user_id._id, (status: any, err: Error, val: any) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item.user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(item.shop_id._id, (status: any, err: Error, val: any) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item.shop_id._id).emit('shopCount', val)
        })
      }

    }).catch((err) => {
      console.log('Watcher Booking Cancelled Err', err)
    })

  })

  BookingCancelVerify.on('change', async (data: any) => {

    BookingModel.findOne({ _id: data.documentKey._id }).populate('shop_id user_id service_id').then((item: any) => {

      if (item) {


        io.sockets.in('User' + item.user_id._id).emit('customerBooking', item)
        //io.sockets.in('Shop' + item.shop_id._id).emit('shopBooking', item)


        // userQueueAndBooking(item.user_id._id, (status: any, err: Error, val: any) => {

        //   if (err) {
        //     return
        //   }
        //   io.sockets.in('User' + item.user_id._id).emit('customerCount', val)
        // })

        // shopQueueAndBooking(item.shop_id._id, (status: any, err: Error, val: any) => {

        //   if (err) {
        //     return
        //   }

        //   io.sockets.in('Shop' + item.shop_id._id).emit('shopCount', val)
        // })
      }

    }).catch((err) => {
      console.log('Watcher Booking Cancelled Err', err)
    })

  })

  ShopUserCreate.on('change', async (data: any) => {

    const urlString: any = SERVER_URL + '/api/open/' + data.documentKey._id
    console.log('qrcode urle', urlString)

    QRCode.toDataURL(urlString, function (err: Error, val: any) {

      if (err) {
        return console.log('Qrcode Generate Watcher Err', err)
      }

      console.log('Url Created After Qe', val)

      ShopModel.findOneAndUpdate({ _id: data.documentKey._id }, {
        $set: {
          code: val
        }
      }).then((cc) => {
        console.log('Qrcode Generate Shop User')
      }).catch((err) => {
        console.log('Shop Qrcode Watcher Update Err', err)
      })

    })


  })

  QueueTimeUpdated.on('change', async (data: any) => {

    BookingModel.findOne({ _id: data.documentKey._id }).populate('shop_id user_id service_id').then((item: any) => {

      if (item) {

        io.sockets.in('User' + item.user_id._id).emit('customerBooking', item)
        io.sockets.in('Shop' + item.shop_id._id).emit('shopBooking', item)


        userQueueAndBooking(item.user_id._id, (status: any, err: Error, val: any) => {

          if (err) {
            return
          }
          io.sockets.in('User' + item.user_id._id).emit('customerCount', val)
        })

        shopQueueAndBooking(item.shop_id._id, (status: any, err: Error, val: any) => {

          if (err) {
            return
          }

          io.sockets.in('Shop' + item.shop_id._id).emit('shopCount', val)
        })
      }

    }).catch((err) => {
      console.log('Watcher Booking Status Accepted Err', err)
    })

  })

}

export default watcher