/* eslint-disable @typescript-eslint/no-non-null-assertion */
// .dotenv why you do me dirty like this D:
import dotenv from 'dotenv';
import crypto from 'crypto';
import axios from 'axios';
dotenv.config();
const BASE_URL = 'http://localhost:';
//const BASE_URL = 'http://192.168.1.10:';
const C_PORT = 3000;
const S_PORT = 5000;

export const Socket_Port = 5002;
//const SERVER_URL_NEW = 'https://heybarberserver.herokuapp.com'
const SERVER_URL_NEW = BASE_URL + S_PORT;

export const DATABASE_URL =
  process.env.DATABASE_URL || 'mongodb+srv://heycalls:madhur%404994%40heycalls@cluster0.fuagf.mongodb.net/heytechQueue?retryWrites=true';
// || 'mongodb://localhost:27017/queue'
export const GOOGLE_KEY = {
  clientID: process.env.GOOGLE_KEY_CLIENTID! || '260572964952-k5oeai09gnj3vjs34v4899rat76689u4.apps.googleusercontent.com',
  clientSecret: process.env.GOOGLE_KEY_CLIENTSECRET! || 'RoDc2kykomsO3rp-GP015TF-',
  callbackURL: SERVER_URL_NEW + '/web/mentor/auth/google/callback',
};

export const getDateArray = function (start: any, end: any) {
  var arr = new Array();
  var dt = new Date(start);
  while (dt <= end) {
    arr.push(new Date(dt));
    dt.setDate(dt.getDate() + 1);
  }
  return arr;
}

export const GOOGLE_KEY_USER = {
  clientID: process.env.GOOGLE_KEY_CLIENTID! || '260572964952-k5oeai09gnj3vjs34v4899rat76689u4.apps.googleusercontent.com',
  clientSecret: process.env.GOOGLE_KEY_CLIENTSECRET! || 'RoDc2kykomsO3rp-GP015TF-',
  callbackURL: SERVER_URL_NEW + '/web/user/auth/google/callback',
};

export const PROD: boolean = JSON.parse(process.env.PROD || 'false');

export const port = parseInt(<string>process.env.PORT) || S_PORT;

export const SERVER_URL = SERVER_URL_NEW;


//export const CLIENT_URL = 'https://heybarber.herokuapp.com';
export const CLIENT_URL = BASE_URL + C_PORT;

export const ServiceSmsMail = "https://queuickses.herokuapp.com/heytech/api/";
//export const ServiceSmsMail = "http://localhost:5003/heytech/api/";
export const SERVICE_KEY = "https://queuickses.herokuapp.com/"

export const SHORT_URL_KEY = 'https://vbn.link/';
//export const SHORT_URL_KEY = 'http://localhost:500/'

export const ServiceTemplateId = {
  book: '1701164603567987143',
  otp: '1701164603567987143',
  confirm: '1701164603567987143',
  cancel: '1701164603567987143'
}




export const COOKIE_KEYS = process.env.COOKIE_KEYS || "heybarber";

export const LOCAL_BASE_IMAGE_URL = "/image/";

export const SECRET_KEY = "heytech";

export const MAIL_SECRET_KEY = "Mailheytech";

export const generateHash = (few: any) => {

  const val = crypto.createHash('md5').update(few.toString()).digest("hex")

  return val
}

export const otpGenerate = () => {
  const val = Math.floor(100000 + Math.random() * 900000);
  return val
}

export const EmployeeIdGenerate = () => {
  const val = Math.floor(1000 + Math.random() * 9000);
  return val
}

export const dayOfWeek = [
  'sunday',
  'monday',
  'tuesday',
  'wednesday',
  'thursday',
  'friday',
  'saturday',
];

export const bookingOTPMsg = (item: any) => {
  return `Your OTP for ${item.service_id.name} in ${item.shop_name} is ${item.otp}. Your ${item.queue == false ? 'Appointment' : 'Queue'} Id is #${item.bookingid}.- Team HEYTEC`
}

export const confirmationMsg = (type: any, service: any, shop: any, id: any, size: any, total: any, wait: any, time: any, link: any) => {
  return `Thank you for booking ${type == false ? 'Appointment' : 'Queue'} for ${service} in ${shop} your ${type == false ? 'Appointment' : 'Queue'} Id is #${id}, there are ${size} users in the ${type == false ? 'Appointment' : 'Queue'} and the total waiting time is ${total}, you have to reach ${wait} min before the reach time. Your reach time is ${time}. Check your status ${link}.- Team HEYTEC`
}

export const bookingMsg = (service: any, shop: any, size: any) => {
  return `You have a booking for ${service} in ${shop} the total number of users is ${size}.- Team HEYTEC`
}

export const cancelMsg = (type: any, service: any, shop: any, id: any) => {
  return `Your booking for ${service} in ${shop} with ${type ? 'Appointment' : 'Queue'} Id is #${id} is canceled. - Team HEYTEC`
}

export const loginMsg = (item: any) => {
  return `Your OTP for ${item.service_id.name} in ${item.shop_name} is ${item.otp}. Your ${item.queue == false ? 'Appointment' : 'Queue'} Id is #${item.bookingid}.- Team HEYTEC`
}

export const webPushKey = {
  Public_Key: 'BAVqi7qbi4PHBnZBVd3BS5hKEh3ogJ_-C0Kd8M7aHKRMAy6R-efJjn70rkbesDGEpUdkf-3f4qrIICMN0A5A9A8',
  Private_Key: 'VT0I00P_Q5COfD6e7jZeRnGZJhSh9NjA0gA6ITZGYRs',
  WEB_PUSH_CONTACT: "mailto: madhurelision@gmail.com"
}

export const randomImageName = () => {
  var key: string = "SOURCE_" + Math.floor(Math.random() * (3 - 1 + 1) + 1)
  return key
}

export const createDate = (date: Date, time: any) => {
  var now = new Date(date);
  now.setDate(now.getDate())
  now.setHours(time);
  now.setMinutes(0);
  now.setMilliseconds(0);
  return now
}

export const createQueueDate = (date: any, time: any) => {
  var now = new Date(date);
  console.log('now', now, date, time)
  now.setMinutes(now.getMinutes() + time);
  // now.setMilliseconds(0);
  return now
}

export const minusDate = (start: Date, end: Date) => {
  var now = new Date(start).getTime();
  var later = new Date(end).getTime();
  var result: any = (now - later) / 60000;
  return (result > 0) ? result : 0
}

export const LOG_SERVICE = "https://heytechlogs.herokuapp.com/"

export const radomThreeeDigit = (key: any, digit: any) => {
  var arr = [];
  while (arr.length < key) {
    var r = Math.floor(Math.random() * digit) + 1;
    if (arr.indexOf(r) === -1) arr.push(r);
  }
  return arr
}

export const SOCKET_URL = 'https://qdiscovery.herokuapp.com'
export const KEY = 'heytechdiscovery'

export const redisConfig = { port: 16868, host: 'redis-16868.c10.us-east-1-3.ec2.cloud.redislabs.com', password: 'VL1XpKEY3mSJW6eLLGMk75p9YRFj8pAu' }
// export const redisConfig = { port: 6379, host: '127.0.0.1', password: '' }
export const redifconf = { url: "redis://default:VL1XpKEY3mSJW6eLLGMk75p9YRFj8pAu@redis-16868.c10.us-east-1-3.ec2.cloud.redislabs.com:16868" }

export const otpPass = 555555;


export const QUEUE_URL = "http://localhost:5005/";

export const DATE_LOCAL = () => {
  return new Date().toLocaleString('en-US', { timeZone: 'Asia/Calcutta' });
}

export const adminStatus: any = {
  'Shops': [
    '/serviceList',
    '/createService',
    '/updateService',
    '/updateServiceStatus',
    '/deleteService',
    '/changeMultiServiceStatus',
    '/shopList',
    '/singleShop',
    '/editShop',
    '/Qrcode',
    '/changeShopStatus',
    '/changeMultiShopStatus',
    '/employeeList',
    '/editEmployee',
    '/updateEmployeeStatus',
    '/deleteEmployee',
    '/changeMultiEmployeeStatus',
    '/offerList',
    '/createOffer',
    '/editOffer',
    '/deleteOffer',
    '/updateOfferStatus',
    '/changeMultiOfferStatus',
    '/bannerList',
    '/createBanner',
    '/editBanner',
    '/deleteBanner',
    '/updateBannerStatus',
    '/changeMultiBannerStatus',
    '/shopCategoryList',
    '/createShopCategory',
    '/editShopCategory',
    '/deleteShopCategory',
    '/updateShopCategoryStatus',
    '/changeMultiShopCategoryStatus',
    '/getServiceQuery',
    '/getShopQuery',
    '/getEmployeeQuery',
    '/importServiceCsv',
    '/importShopCsv',
    '/userUpdateList',
    '/deleteUserUpdate',
    '/shopPackageList',
    '/createShopPackage',
    '/editShopPackage',
    '/deleteShopPackage',
    '/shopOrderList',
    '/shopPaymentList',
  ],
  'Users': [
    '/getCustomerQuery',
    '/customerList',
    '/singleCustomer',
    '/editCustomer',
    '/importCustomerCsv',
    '/userShopList',
  ],
  'Bookings': [
    '/currentBookingList',
    '/bookingList',
    '/singleBooking',
    '/changeBookingSeat',
    '/queryBookingList',
    '/bookingIpList',
    '/deleteBookingIpDetail',
    '/getBookingQuery',
    '/getQueueBooking',
    '/getAllTestBooking',
    '/bookingShortIdList',
    '/getBookDetail',
  ],
  'Settings': [
    '/logList',
    '/clearlogList',
    '/templateList',
    '/createTemplate',
    '/updateTemplate',
    '/deleteTemplate',
    '/tagServiceList',
    '/createTagService',
    '/editTagService',
    '/deleteTagService',
    '/updateTagServiceStatus',
    '/pushNotificationList',
    '/sendPushNotification',
    '/deletePushNotification',
    '/tagList',
    '/createTag',
    '/editTag',
    '/deleteTag',
    '/updateTagStatus',
    '/changeMultiTagStatus',
    '/reviewTagList',
    '/createReviewTag',
    '/editReviewTag',
    '/deleteReviewTag',
    '/updateReviewTagStatus',
    '/changeMultiReviewTagStatus',
    '/serviceTagList',
    '/createServiceTag',
    '/editServiceTag',
    '/deleteServiceTag',
    '/updateServiceTagStatus',
    '/descriptionTagList',
    '/createDescriptionTag',
    '/editDescriptionTag',
    '/deleteDescriptionTag',
    '/updateDescriptionTagStatus',
    '/galleryList',
    '/createGallery',
    '/editGallery',
    '/deleteGallery',
    '/serviceRequestList',
    '/deleteMailService',
    '/getlogList',
    '/getBookDetail',
    '/locationLogList',
    '/socketList',
    '/socketStatList',
    '/createAdminPage',
    '/editAdminPage',
    '/createContactPage',
    '/editContactPage',
    '/getBookingLogs',
    '/createShopShortId',
    '/getAllShopShortId',
    '/createCustomShortId',
    '/editCustomShortId',
    '/getAllCustomShortId',
    '/updateShortId',
    '/deleteShortId',
    '/singleReview',
    '/getExternal',
    '/getAllContactList',
    '/deleteContact',
    '/getAllReviewList',
    '/getAllShopReviewList',
    '/createAboutPage',
    '/editAboutPage',
    '/adminFeatureList',
    '/createAdminFeature',
    '/editAdminFeature',
    '/deleteAdminFeature',
    '/adminSubList',
    '/createAdminSub',
    '/editAdminSub',
    '/deleteAdminSub',
    '/updateStatusAdminSub',
    '/loginLogList',
    '/adminManageList',
    '/createAdminManage',
    '/editAdminManage',
    '/deleteAdminManage',
    '/updateAdminManageStatus',
    '/serviceNameList',
    '/createServiceName',
    '/editServiceName',
    '/deleteServiceName',
    '/expertiseNameList',
    '/createExpertiseName',
    '/editExpertiseName',
    '/deleteExpertiseName',
    '/serviceUserList',
    '/hostList',
    '/createHost',
    '/editHost',
    '/deleteHost',
    '/changeMultiServiceTagStatus',
    '/changeMultiDescriptionTagStatus',
    '/changeMultiTagServiceStatus',
    '/changeMultiServiceNameStatus',
    '/changeMultiExpertiseNameStatus',
  ],
  'Web Ssr': [
    '/menuList',
    '/createMenu',
    '/editMenu',
    '/deleteMenu',
    '/changeStatusMenu',
    '/getAllMenu',
    '/changeMultiSsrMenuStatus',
    '/featureList',
    '/createFeature',
    '/editFeature',
    '/deleteFeature',
    '/changeStatusFeature',
    '/getAllFeature',
    '/changeMultiSsrFeatureStatus',
    '/homeList',
    '/createHome',
    '/editHome',
    '/deleteHome',
    '/changeStatusHome',
    '/getAllHome',
    '/changeMultiSsrHomeStatus',
    '/productList',
    '/createProduct',
    '/editProduct',
    '/deleteProduct',
    '/changeStatusProduct',
    '/getAllProduct',
    '/changeMultiSsrProductStatus',
    '/reviewList',
    '/createReview',
    '/editReview',
    '/deleteReview',
    '/changeStatusReview',
    '/getAllReview',
    '/changeMultiSsrReviewStatus',
    '/getAllPage',
    '/createMain',
    '/editMain',
    '/aboutList',
    '/createAbout',
    '/editAbout',
    '/deleteAbout',
    '/updateAboutStatus',
    '/changeMultiSsrAboutStatus',
    '/catalogList',
    '/createCatalog',
    '/editCatalog',
    '/deleteCatalog',
    '/changeStatusCatalog',
    '/getAllCatalog',
    '/changeMultiSsrCatalogStatus',
    '/createContact',
    '/contactList',
    '/deleteContact',
    '/blogList',
    '/createBlog',
    '/editBlog',
    '/deleteBlog',
    '/changeStatusBlog',
    '/getAllBlog',
    '/getBlog',
    '/changeMultiSsrBlogStatus',
    '/blogCommentList',
    '/changeStatusComment',
    '/createBlogComment',
    '/replyBlogComment',
    '/getBlogData',
    '/affiliateList',
    '/createAffiliate',
    '/editAffiliate',
    '/deleteAffiliate',
    '/changeStatusAffiliate',
    '/getAllAffiliate',
    '/changeMultiSsrAffiliateStatus',
    '/logoList',
    '/createLogo',
    '/editLogo',
    '/deleteLogo',
    '/changeStatusLogo',
    '/changeMultiSsrLogoStatus',
    '/newsletterList',
    '/deleteNewsletter',
    '/userList',
    '/deleteUser',
    '/stripeList',
    '/createStripe',
    '/editStripe',
    '/deleteStripe',
    '/updateStripeStatus',
    '/changeMultiSsrStripeStatus',
    '/orderList',
    '/paymentList',
    '/createUserFeature',
    '/editUserFeature',
    '/packageFeatureList',
    '/createPackageFeature',
    '/editPackageFeature',
    '/deletePackageFeature',
    '/razorpayList',
    '/createRazorpay',
    '/editRazorpay',
    '/deleteRazorpay',
    '/updateRazorpayStatus',
    '/changeMultiSsrRazorpayStatus',
  ]
}

export const makeSingleArray = (feature: []) => {
  let arr: any = ['/profile', '/shopList', '/queryBookingList', '/getExternal']
  feature?.map((cc: any) => arr.push(...adminStatus[cc.name]))
  return arr
}

export const apiReq = (searchRec: any) => {
  return new Promise((resolve, reject) => {
    return axios({
      url: searchRec, // Endpoint
      method: 'GET'
    }).then((response) => {
      resolve(response.data);  // return results
    }).catch((error) => {
      resolve(false);
    });
  });
}