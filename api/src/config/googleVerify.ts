import { OAuth2Client } from 'google-auth-library';
import { Request, Response, NextFunction } from 'express'
import { CustomerModel } from '../Models/Customer';
import { CLIENT_URL } from './keys';
import config from 'config'

const client = new OAuth2Client('592038878601-nqjs625a5onbc33pmvdm0a45els3sk1b.apps.googleusercontent.com');

export const verify = async (req: Request, res: Response, next: NextFunction) => {

  const token = req.params.token

  if (token == null || token == undefined || token == "") {
    return res.redirect(config.has('CLIENT_URL') ? config.get('CLIENT_URL') : CLIENT_URL)
  }

  try {

    const ticket = await client.verifyIdToken({
      idToken: token,
      audience: '592038878601-nqjs625a5onbc33pmvdm0a45els3sk1b.apps.googleusercontent.com',
    });
    const payload: any = ticket.getPayload();
    console.log('Token Payload', payload)
    const userid = payload['sub'];
    console.log('Token Verify', userid)

    CustomerModel.findOne({ email: payload['email'] }).then((item: any) => {

      if (item == null || item == undefined) {

        const userData = new CustomerModel({
          email: payload['email'],
          first_name: payload['given_name'],
          last_name: payload['family_name'],
          image_link: payload['picture'],
          google: payload['sub'],
          oauth_provider: 'google',
          signup_completed: true,
        })

        userData.save().then((cc: any) => {

          req.user = cc;
          next()

        }).catch((err: Error) => {
          console.log('catch qrcode url customer create err', err)
          res.redirect(config.has('CLIENT_URL') ? config.get('CLIENT_URL') : CLIENT_URL)
        })


      } else {

        req.user = item;
        next()
      }

    }).catch((err) => {
      console.log('catch qrcode customer find err', err)
      res.redirect(config.has('CLIENT_URL') ? config.get('CLIENT_URL') : CLIENT_URL)
    })

  } catch (err) {
    console.log('google Verify Err', err)
    res.redirect(config.has('CLIENT_URL') ? config.get('CLIENT_URL') : CLIENT_URL)
  }
}
