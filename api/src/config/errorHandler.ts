
interface AppError extends Error {
  status: string
  statusCode: number
  isOperational: Boolean
}

class AppError extends Error {
  constructor(message: any, statusCode: any) {
    super(message);

    this.statusCode = statusCode;
    this.status = `${statusCode}`.startsWith('4') ? 'fail' : 'error';
    this.isOperational = true;

    Error.captureStackTrace(this, this.constructor);
  }
}