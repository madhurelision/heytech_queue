import { NextFunction, Request, Response } from "express";
import { GOOGLE_KEY, GOOGLE_KEY_USER } from "./keys";
const passport = require('passport')
const GoogleStrategy = require('passport-google-oauth20').Strategy
const { google } = require('../controllers/mentorController')
import { googleNew } from "../controllers/customerController";
import config from 'config'

passport.serializeUser(function (user: any, done: any) {
  done(null, user);
});

passport.deserializeUser(function (id: any, done: any) {
  console.log('id', id)
  done(null, id);
});

export const mentorMiddleware = async (req: Request, res: Response, next: NextFunction) => {
  await passport.use(new GoogleStrategy({
    clientID: config.has('GOOGLE_KEY.clientID') ? config.get('GOOGLE_KEY.clientID') : GOOGLE_KEY.clientID,
    clientSecret: config.has('GOOGLE_KEY.clientSecret') ? config.get('GOOGLE_KEY.clientSecret') : GOOGLE_KEY.clientSecret,
    callbackURL: config.has('GOOGLE_KEY.callbackURL') ? config.get('GOOGLE_KEY.callbackURL') : GOOGLE_KEY.callbackURL,
    passReqToCallback: true
  }, google));
  next()
}

export const userMiddleware = async (req: Request, res: Response, next: NextFunction) => {
  await passport.use(new GoogleStrategy({
    clientID: config.has('GOOGLE_KEY_USER.clientID') ? config.get('GOOGLE_KEY_USER.clientID') : GOOGLE_KEY_USER.clientID,
    clientSecret: config.has('GOOGLE_KEY_USER.clientSecret') ? config.get('GOOGLE_KEY_USER.clientSecret') : GOOGLE_KEY_USER.clientSecret,
    callbackURL: config.has('GOOGLE_KEY_USER.callbackURL') ? config.get('GOOGLE_KEY_USER.callbackURL') : GOOGLE_KEY_USER.callbackURL,
    passReqToCallback: true
  }, googleNew));
  next()
}
