import moment from 'moment';
import { BookingModel } from "../Models/Booking";
import { ShopModel } from '../Models/User';
import mongoose from 'mongoose'

// count user Queue and Booking
export const userQueueAndBooking = (id: any, cb: Function) => {

  const promise1 = new Promise((resolve, reject) => {
    BookingModel.countDocuments({ user_id: id, queue: true, appointment_date: { $gte: moment().startOf('day'), $lte: moment().endOf('day') }, status: { $in: ['waiting', 'accepted'] }, approximate_date: { $gte: new Date() } }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise2 = new Promise((resolve, reject) => {
    BookingModel.countDocuments({ user_id: id, queue: false, appointment_date: { $gte: moment().startOf('day') }, status: { $in: ['waiting', 'accepted'] }, end_time: { $gte: new Date() } }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  Promise.allSettled([promise1, promise2]).then((cc: any) => {

    cb(200, null, {
      customerBookingCount: cc[0].value,
      customerQueueCount: cc[1].value,
      customerTotalCount: cc[0].value + cc[1].value
    })

  }).catch((err) => {
    cb(400, err, null)
  })
}

// count shop Queue and Booking
export const shopQueueAndBooking = (id: any, cb: Function) => {

  const promise1 = new Promise((resolve, reject) => {
    BookingModel.countDocuments({ shop_id: id, queue: true, appointment_date: { $gte: moment().startOf('day'), $lte: moment().endOf('day') }, status: { $in: ['waiting', 'accepted'] }, approximate_date: { $gte: new Date() } }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise2 = new Promise((resolve, reject) => {
    BookingModel.countDocuments({ shop_id: id, queue: false, appointment_date: { $gte: moment().startOf('day') }, status: { $in: ['waiting', 'accepted'] }, end_time: { $gte: new Date() } }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  Promise.allSettled([promise1, promise2]).then((cc: any) => {
    cb(200, null, {
      shopQueueCount: cc[0].value,
      shopBookingCount: cc[1].value,
    })

  }).catch((err) => {
    cb(400, err, null)
  })

}

// shop Queue Counts
export const getShopCurrenQueue = (id: any, cb: Function) => {
  ShopModel.aggregate([
    {
      '$match': {
        '_id': new mongoose.Types.ObjectId(id)
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    }, {
      '$unwind': {
        'path': '$topics'
      }
    }, {
      '$lookup': {
        'from': 'bookings',
        'let': {
          'main': '$user_information',
          'service': '$topics._id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$service_id', '$$service'
                    ]
                  }, {
                    '$eq': [
                      '$queue', true
                    ]
                  }, {
                    '$gte': [
                      '$approximate_date', new Date()
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'booking'
      }
    }, {
      '$project': {
        '_id': '$topics._id',
        'count': {
          '$size': '$booking'
        }
      }
    }
  ]).then((item: any) => {
    cb(200, null, item)
  }).catch((err) => {
    cb(400, err, null)
  })
}