import { Queue, QueueEvents, Worker } from "bullmq";

const redisOptions = {
  port: 6379,
  host: 'localhost',
  password: '',
 // tls: false,
};

const appointmentQueue = new Queue('appointmentQueue');

const worker = new Worker('appointmentQueue',
 async job => {
  console.log('Worked', job)
  if (job.name === 'add') {
    console.log(job.data);
    return 'some value';
  }
});

worker.on('completed', (job, returnvalue) => {
  console.log('done appointmen', job, returnvalue);
});

worker.on('failed', (job, failedReason) => {
  console.error('error appointmen', failedReason);
});

// const queueEvents = new QueueEvents('appointmentQueue', { connection: redisOptions });

// queueEvents.on('completed', ({ jobId }) => {
//   console.log('done appointmen', jobId);
// });

// queueEvents.on('failed', ({ jobId, failedReason }) => {
//   console.error('error appointmen', failedReason);
// });

const addAppointment = async (data: any) => {
  console.log('add Appounment Called', data)
  await appointmentQueue.add('add', data)
}


export { appointmentQueue, addAppointment }