import Bull from 'bull';
import { redisConfig } from '../config/keys';

const bookingQueue = new Bull('bookingQueue', { redis: redisConfig });

bookingQueue.process((job, done) => {
  const { checkBookingQueue } = require('../apiService/bookingService/controllers/bookingController');

  checkBookingQueue(job.data.id, job.data.body, (status: any, err: any, data: any) => {

    if (status == 200) return done(null, data)
    if (status == 400) return done(err)

  })

  return "HeytechUrl";
})

const bookingAdd = (data: any) => {
  console.log('queue', data)
  bookingQueue.add(data)
}

const bookJob = (job: any, done: any) => {
  const { checkBookingQueue } = require('../apiService/bookingService/controllers/bookingController');

  checkBookingQueue(job.data.id, job.data.body, (status: any, err: any, data: any) => {

    if (status == 200) return done(null, data)
    if (status == 400) return done(err)

  })

  return "HeytechUrl";
}

export { bookingQueue, bookingAdd, bookJob }