import Bull from 'bull';
import { redisConfig } from '../config/keys';

const testQueue = new Bull('testQueue', { redis: redisConfig });

testQueue.process((job, done) => {
  done(null, job.data)
  return "vishwaUrl";
})

const testAdd = (data: any) => {
  console.log('queue', data)
  testQueue.add(data)
}

export { testQueue, testAdd }