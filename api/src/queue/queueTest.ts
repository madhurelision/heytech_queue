import app from '../app'
import { Request, Response, NextFunction } from 'express'
import chalk from 'chalk';
import { createClient } from 'redis';
import { createBullBoard } from '@bull-board/api';
import { BullAdapter } from '@bull-board/api/bullAdapter'
import { ExpressAdapter } from '@bull-board/express'
import { bookingAdd, bookingQueue } from './bookingQueue'
import { testAdd, testQueue } from './testQueue'
import { redisConfig, redifconf } from '../config/keys';
import { getAllQueue, getQueue } from './multiQueue';


const redisClient = createClient(redifconf);
redisClient.connect()
redisClient.on('connect', () => {
  console.log(chalk.green('Redis Connection Established'));
});
redisClient.on('error', () => {
  console.error(chalk.red('Redis Failed To Establish Connection'));
});

const serverAdapter = new ExpressAdapter();

const board = createBullBoard({
  queues: [
    new BullAdapter(testQueue),
    new BullAdapter(bookingQueue),
    // ...getAllQueue().map((cc: any) => {
    //   return new BullAdapter(cc)
    // })
  ],
  serverAdapter
})

serverAdapter.setBasePath('/admin/queues')

app.use('/admin/queues', serverAdapter.getRouter());

app.get('/api/test/send', async (req: Request, res: Response) => {

  // const add = getQueue('Beardo')
  // add.add({ data: 'Vishwa', wait: 'hello' })
  testAdd({ data: 'Vishwa', wait: 'hello' })
  res.send({ status: 'ok' });
});

export default app