import Bull from 'bull';
import moment from 'moment';
import { redisConfig } from '../config/keys';
import { bookJob } from './bookingQueue';
const queues: any = {};

// 1
// export const QUEUE_NAMES = {
//   SCHEDULE_JOB_1: 's1',
//   SCHEDULE_JOB_2: 's2',
//   SCHEDULE_JOB_3: 's3',
// };

// 2
export const QUEUE_PROCESSORS = {
  test: (job: any, done: any) => {
    console.log(`${moment()}::Job with id: ${job.id} is being executed.\n`, {
      message: job.data.message
    });
    done();
  }
};
// 3
export const initQueues = (arr: any) => {
  console.log('init queues');
  arr.forEach((queueName: any) => {
    // 4
    queues[queueName['shop_name']] = getQueue(queueName['shop_name']);
    // 5
    queues[queueName['shop_name']].process(bookJob);
  });
};

export const getQueue = (queueName: any) => {
  if (!queues[queueName]) {
    queues[queueName] = new Bull(queueName, { redis: redisConfig });
    console.log('created queue: ', queueName);
  }
  return queues[queueName];
};

export const getAllQueue = () => {
  if (Object.keys(queues).length == 0) {
    return []
  } else {
    const test = Object.values(queues)
    console.log('check', test)
    return test
  }
}