import { createBullBoard } from '@bull-board/api'
import { BullMQAdapter } from '@bull-board/api/bullMQAdapter'
import { ExpressAdapter } from '@bull-board/express'
import { Queue, Worker, QueueScheduler } from 'bullmq'
import { Request, Response, Express } from 'express'
import app from '../app'
import { addAppointment, appointmentQueue } from './appointmentQueue'

console.log('Queue is Running')

const sleep = (t: any) => new Promise((resolve) => setTimeout(resolve, t * 1000));

const redisOptions = {
  port: 6379,
  host: 'localhost',
  password: '',
  // tls: false,
};

const createQueueMQ = (name: any) => new Queue(name, { connection: redisOptions });

async function setupBullMQProcessor(queueName: any) {
  const queueScheduler = new QueueScheduler(queueName, {
    connection: redisOptions,
  });
  await queueScheduler.waitUntilReady();

  new Worker(queueName, async (job) => {
    for (let i = 0; i <= 100; i++) {
      await sleep(Math.random());
      await job.updateProgress(i);
      await job.log(`Processing job at interval ${i}`);

      if (Math.random() * 200 < 1) throw new Error(`Random error ${i}`);
    }

    return { jobId: `This is the return value of job (${job.id})` };
  });
}

const exampleBullMq = createQueueMQ('BullMQ');

setupBullMQProcessor(exampleBullMq.name);

const serverAdapter = new ExpressAdapter();
serverAdapter.setBasePath('/admin/ui');

createBullBoard({
  queues: [
    //new BullMQAdapter(exampleBullMq),
    new BullMQAdapter(appointmentQueue)
  ],
  serverAdapter,
});

app.use('/admin/ui', serverAdapter.getRouter());

app.get('/add', (req: Request, res: Response) => {
  console.log('Add Request')
  const opts: any = req.query.opts || {};

  if (opts.delay) {
    opts.delay = +opts.delay * 1000; // delay must be a number
  }

  exampleBullMq.add('Add', { title: req.query.title }, opts);

  res.json({
    ok: true,
  });
});

app.post('/check', (req: Request, res: Response) => {

  exampleBullMq.add('Sum', { data: req.body })
  res.json({
    ok: true,
  });
})

app.post('/wait', async (req: Request, res: Response) => {
  console.log('req.body', req.body)
  await addAppointment(req.body)
  res.json({
    ok: true,
  });
})

app.use('/', (req, res) =>
  res.send(`
<h1>Server is Running :)))</h1>
`),
);

export default app