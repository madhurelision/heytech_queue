import { createBullBoard } from '@bull-board/api'
import { BullAdapter } from '@bull-board/api/bullAdapter'
import { ExpressAdapter } from '@bull-board/express'
import { Request, Response, Express } from 'express'
import Bull from 'bull'
import app from '../app'

const redisOptions = {
  port: 6379,
  host: '127.0.0.1',
  password: '',
};

const TestQueue = new Bull('test', {
  redis: redisOptions
});

TestQueue.process(async (Data: any) => {

  console.log("Data", Data);

  return "cheked";
})

const serverAdapter = new ExpressAdapter();

const board = createBullBoard({
  queues: [
    new BullAdapter(TestQueue),
  ],
  serverAdapter
})

serverAdapter.setBasePath('/admin/ui')

app.use('/admin/ui', serverAdapter.getRouter());

app.post('/add', async (req: Request, res: Response) => {
  console.log('req.body', req.body)
  await TestQueue.add(req.body)
  res.json({
    ok: true,
  });
})

export default app