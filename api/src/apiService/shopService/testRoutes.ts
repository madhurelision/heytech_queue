import { Router } from 'express';

import { createShopUser, getShopUserProfile, SingleUser, updateShopUser, createOrChangeQrCode, changeShopQueueStatus, changeShopAppointmentStatus, CreateOtp, verifyOtp, UpdateServiceInShop } from './controllers/shopController';

import { ShopVerify } from '../../config/verifyToken';

import { changeServiceAcceptStatus, cloneAllService, cloneService, CreateService, deleteService, getShopService, SearchServiceShopIdRegexQuery, updateService } from './controllers/serviceController';

import { getShopUserGallery, createGallery, EditGallery, deleteGallery } from '../adminService/controllers/galleryController';

import { addWaitingTime, getShopBookings, getShopUserCurrentAppointment, shopBookingCancel, shopBookingQuery, shopUserQueueList, addQueueWaitingTime, getSingleQuery, assignEmployeeIdBooking } from '../bookingService/controllers/bookingController';


import { makeReview } from '../adminService/controllers/reviewController';

import { changeShopViewStatus, getShopNotification } from '../adminService/controllers/notificationController';
import { changeEmployeeStatus, CreateEmployee, deleteEmployee, EditEmployee, getShopEmployee } from './controllers/shopEmployeeController';

const router = Router();

// shop
router.post('/shop/createShop/:id', createShopUser)
router.get('/shop/getUser/:id', SingleUser)
router.get('/shop/profile', ShopVerify, getShopUserProfile)
router.post('/shop/updateShop', ShopVerify, updateShopUser)
router.post('/shop/updateQueueStatus/:id', ShopVerify, changeShopQueueStatus)
router.post('/shop/updateAppointmentStatus/:id', ShopVerify, changeShopAppointmentStatus)
router.post('/shop/updateServiceShop/:id', UpdateServiceInShop)

// service
router.post('/shop/createService', ShopVerify, CreateService)
router.post('/shop/updateService/:id', ShopVerify, updateService)
router.get('/shop/getService/:skip/:limit', ShopVerify, getShopService)
router.post('/shop/updateServiceStatus/:id', ShopVerify, changeServiceAcceptStatus)
router.get('/shop/cloneService/:id', ShopVerify, cloneService)
router.post('/shop/cloneAllService', ShopVerify, cloneAllService)
router.get('/shop/deleteService/:id', ShopVerify, deleteService)

// gallery
router.get('/shop/getGallery', ShopVerify, (req, res, next) => { getShopUserGallery(req, res, next) })
router.post('/shop/createGallery', ShopVerify, (req, res, next) => { createGallery(req, res, next) })
router.post('/shop/editGallery/:id', ShopVerify, (req, res, next) => { EditGallery(req, res, next) })
router.get('/shop/deleteGallery/:id', ShopVerify, (req, res, next) => { deleteGallery(req, res, next) })

/// Booking
router.post('/shop/addWaitingTime/:id', ShopVerify, (req, res, next) => { addWaitingTime(req, res, next, false) })
router.post('/shop/addQueueWaitingTime/:id', ShopVerify, (req, res, next) => { addQueueWaitingTime(req, res, next, false) })
router.get('/shop/getAllBooking', ShopVerify, (req, res, next) => { getShopBookings(req, res, next, false) })
router.get('/shop/getCurrentBooking/:skip/:limit', ShopVerify, (req, res, next) => { getShopUserCurrentAppointment(req, res, next, false) })
router.get('/shop/getQueueBooking/:skip/:limit', ShopVerify, (req, res, next) => { shopUserQueueList(req, res, next, false) })
router.post('/shop/getQueryBooking/:skip/:limit', ShopVerify, (req, res, next) => { shopBookingQuery(req, res, next, false) })
router.get('/shop/getBookQuery/:id', ShopVerify, (req, res, next) => { getSingleQuery(req, res, next, true) })
router.post('/shop/assignEmployee/:id', ShopVerify, (req, res, next) => { assignEmployeeIdBooking(req, res, next, false) })

// Shop Employee
router.post('/shop/createEmployee', ShopVerify, (req, res, next) => { CreateEmployee(req, res, next, true) });
router.post('/shop/editEmployee/:id', ShopVerify, (req, res, next) => { EditEmployee(req, res, next, true) });
router.get('/shop/getEmployee', ShopVerify, (req, res, next) => { getShopEmployee(req, res, next, true) });
router.post('/shop/updateEmployeeStatus/:id', ShopVerify, (req, res, next) => { changeEmployeeStatus(req, res, next, true) });
router.get('/shop/deleteEmployee/:id', ShopVerify, (req, res, next) => { deleteEmployee(req, res, next, true) });


// create or update QRCode
router.get('/shop/Qrcode', ShopVerify, createOrChangeQrCode)

router.get('/shop/cancelBooking/:id', ShopVerify, shopBookingCancel)

// review
router.post('/shop/makeReview', ShopVerify, (req, res, next) => { makeReview(req, res, next, true) })

router.get('/shop/notification', ShopVerify, getShopNotification)
router.get('/shop/view', ShopVerify, changeShopViewStatus)

// createOtp
router.post('/shop/createShopOtp/:id', CreateOtp)
router.post('/shop/verifyShopOtp/:id', verifyOtp)

router.get('/shop/getServiceShopRegexQuery/:id', ShopVerify, SearchServiceShopIdRegexQuery)

export default router;
