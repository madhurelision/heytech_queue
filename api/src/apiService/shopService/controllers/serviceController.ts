import { ServiceModel } from "../../../Models/Service";
import { NextFunction, Request, Response } from 'express';
import { SERVER_URL, randomImageName, radomThreeeDigit } from '../../../config/keys';
import { GalleryModel } from "../../../Models/Gallery";
import { upload } from '../../../config/uploadImage';
import multer from 'multer';
import { ImageUpload, removeImage } from '../../../ImageService/main';
import { Con } from '../../../ImageService/source';
import { isValidObjectId } from 'mongoose'
import { ShopModel } from "../../../Models/User";
import mongoose from 'mongoose'
import { UserUpdateModel } from "../../../Models/UserUpdates";
import { performance } from 'perf_hooks'
import { LogQueryModel } from '../../../Models/LogData';


export const CreateService = async (req: Request, res: Response, next: NextFunction) => {
  console.log('image', req.file)

  const MainCheck = upload.single('image')

  MainCheck(req, res, async function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 500KB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('data', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data
    var key: any = req.user
    var imgD = ''
    var check: any = {}
    var url: any = req.url

    if (isNaN(url.match('/shop/createService'))) {
      check.user_id = key._id
    } else {
      check.admin_id = key._id
    }

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        image: req.body.oldimage.replace(SERVER_URL, ""),
        ...check,
        name: req.body.name,
        motivation: req.body.motivation,
        emojiIcon: req.body.emojiIcon,
        description: req.body.description,
        waiting_number: req.body.waiting_number,
        waiting_key: req.body.waiting_key,
        waiting_time: req.body.waiting_time,
        seat: (typeof req.body.seat == "string") ? [req.body.seat] : req.body.seat,
        tags: req.body.tags,
        price: req.body.price,
      }


      const ServiceMde = new ServiceModel(data)

      ServiceMde.save().then((item: any) => {
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: {
            name: item.name,
            motivation: item.motivation,
            image: item.image,
            emojiIcon: item.emojiIcon,
            description: item.description,
            waiting_number: item.waiting_number,
            waiting_key: item.waiting_key,
            waiting_time: item.waiting_time,
            seat: item.seat,
            tags: item.tags,
            createdAt: item.createdAt,
            updatedAt: item.updatedAt,
            _id: item._id,
            status: item.status,
            accept: item.accept,
            user_id: item.user_id,
            price: item.price,
            type: ''
          }
        })
      }).catch((err: Error) => {
        console.log('Service Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })

      })

    } else {


      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }
        imgD = item?.response.url

        const ImageCreate = await GalleryModel.create({
          image: imgD,
          user_id: key._id,
          data: { ...req.file }
        })
        console.log('Image Made', ImageCreate)
        data = {
          name: req.body.name,
          motivation: req.body.motivation,
          image: imgD,
          ...check,
          emojiIcon: req.body.emojiIcon,
          description: req.body.description,
          waiting_number: req.body.waiting_number,
          waiting_key: req.body.waiting_key,
          waiting_time: req.body.waiting_time,
          seat: (typeof req.body.seat == "string") ? [req.body.seat] : req.body.seat,
          tags: req.body.tags,
          price: req.body.price,
        }


        const ServiceMde = new ServiceModel(data)

        ServiceMde.save().then((item: any) => {
          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: {
              name: item.name,
              motivation: item.motivation,
              image: item.image,
              emojiIcon: item.emojiIcon,
              description: item.description,
              waiting_number: item.waiting_number,
              waiting_key: item.waiting_key,
              waiting_time: item.waiting_time,
              seat: item.seat,
              tags: item.tags,
              createdAt: item.createdAt,
              updatedAt: item.updatedAt,
              _id: item._id,
              status: item.status,
              accept: item.accept,
              user_id: item.user_id,
              price: item.price,
              type: ''
            }
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err: Error) => {
          console.log('Service Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })

        })

      }).catch((err) => {
        console.log('Service Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    }


  })

}

export const getAllService = (req: Request, res: Response, next: NextFunction, verify: any) => {

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        },
        'type': {
          '$arrayElemAt': [
            '$shop_id.type', 0
          ]
        }
      }
    },
    {
      '$project': {
        'admin': 0,
        'shop_id': 0
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'service': [
          {
            '$sort': {
              '_id': 1
            }
          },
          {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'service': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]
  ServiceModel.aggregate(query).then(async (item) => {

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'services',
      startTime: startTime,
      endTime: parseInt(endTime)
    })
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item[0] : { service: [], total: 0 }
    })
  }).catch((err) => {
    console.log('Service Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

export const getShopService = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        'user_id': new mongoose.Types.ObjectId(key._id),
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        },
        'type': {
          '$arrayElemAt': [
            '$shop_id.type', 0
          ]
        }
      }
    },
    {
      '$project': {
        'admin': 0,
        'shop_id': 0
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'service': [
          {
            '$sort': {
              '_id': 1
            }
          },
          {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'service': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]
  ServiceModel.aggregate(query).then(async (item) => {


    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'services',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item[0] : { service: [], total: 0 }
    })
  }).catch((err) => {
    console.log('Service Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const updateService = async (req: Request, res: Response, next: NextFunction) => {
  console.log('image', req.file)

  const MainCheck = upload.single('image')

  MainCheck(req, res, async function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 500KB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    const serviD: any = await ServiceModel.findOne({ _id: req.params.id })
    console.log('body', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data: any
    var key: any = req.user
    var imgD = ''

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        image: req.body.oldimage,
        name: req.body.name,
        motivation: req.body.motivation,
        emojiIcon: req.body.emojiIcon,
        description: req.body.description,
        waiting_number: req.body.waiting_number,
        waiting_key: req.body.waiting_key,
        waiting_time: req.body.waiting_time,
        seat: (typeof req.body.seat == "string") ? [req.body.seat] : req.body.seat,
        tags: req.body.tags,
        price: req.body.price,
      }


      ServiceModel.findOneAndUpdate({ _id: req.params.id }, {
        $set: {
          ...data
        }
      }, { new: true }).then(async (cc) => {

        if (cc == null || cc == undefined) {
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: cc
          })
        }

        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: {
            name: cc.name,
            motivation: cc.motivation,
            image: cc.image,
            emojiIcon: cc.emojiIcon,
            description: cc.description,
            waiting_number: cc.waiting_number,
            waiting_key: cc.waiting_key,
            waiting_time: cc.waiting_time,
            seat: cc.seat,
            tags: cc.tags,
            createdAt: cc.createdAt,
            updatedAt: cc.updatedAt,
            _id: cc._id,
            status: cc.status,
            accept: cc.accept,
            user_id: cc.user_id,
            price: cc.price,
            type: ''
          }
        })

        const userUpdateNesw = await UserUpdateModel.create({
          type: 'Service Update',
          old: serviD,
          new: data,
          service_name: serviD.name,
          user: false,
          user_id: key._id
        })

      }).catch((err) => {
        console.log('Service Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }
        imgD = item?.response.url

        const ImageCreate = await GalleryModel.create({
          image: imgD,
          user_id: key._id,
          data: { ...req.file }
        })
        console.log('Image Made', ImageCreate)
        data = {
          name: req.body.name,
          motivation: req.body.motivation,
          image: imgD,
          user_id: key._id,
          emojiIcon: req.body.emojiIcon,
          description: req.body.description,
          waiting_number: req.body.waiting_number,
          waiting_key: req.body.waiting_key,
          waiting_time: req.body.waiting_time,
          seat: (typeof req.body.seat == "string") ? [req.body.seat] : req.body.seat,
          tags: req.body.tags,
          price: req.body.price,
        }


        ServiceModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            ...data
          },
        }, { new: true }).then(async (cc) => {

          if (cc == null || cc == undefined) {
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: cc
            })
          }

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: {
              name: cc.name,
              motivation: cc.motivation,
              image: cc.image,
              emojiIcon: cc.emojiIcon,
              description: cc.description,
              waiting_number: cc.waiting_number,
              waiting_key: cc.waiting_key,
              waiting_time: cc.waiting_time,
              seat: cc.seat,
              tags: cc.tags,
              createdAt: cc.createdAt,
              updatedAt: cc.updatedAt,
              _id: cc._id,
              status: cc.status,
              accept: cc.accept,
              user_id: cc.user_id,
              price: cc.price,
              type: ''
            }
          })
          const userUpdateNew = await UserUpdateModel.create({
            type: 'Service Update',
            old: serviD,
            new: data,
            service_name: serviD.name,
            user: false,
            user_id: key._id
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err) => {
          console.log('Service Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Service Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    }

  })


}

export const updateAdminService = async (req: Request, res: Response, next: NextFunction) => {
  console.log('image', req.file)

  const MainCheck = upload.single('image')

  MainCheck(req, res, async function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 500KB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }


    console.log('body', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data: any
    var key: any = req.user
    var imgD = ''

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        image: req.body.oldimage,
        name: req.body.name,
        motivation: req.body.motivation,
        emojiIcon: req.body.emojiIcon,
        description: req.body.description,
        waiting_number: req.body.waiting_number,
        waiting_key: req.body.waiting_key,
        waiting_time: req.body.waiting_time,
        seat: (typeof req.body.seat == "string") ? [req.body.seat] : req.body.seat,
        tags: req.body.tags,
        price: req.body.price,
      }


      ServiceModel.findOneAndUpdate({ _id: req.params.id }, {
        $set: {
          ...data
        }
      }, { new: true }).then(async (cc) => {

        if (cc == null || cc == undefined) {
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: cc
          })
        }

        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: cc
        })

      }).catch((err) => {
        console.log('Service Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }
        imgD = item?.response.url

        const ImageCreate = await GalleryModel.create({
          image: imgD,
          user_id: key._id,
          data: { ...req.file }
        })
        console.log('Image Made', ImageCreate)
        data = {
          name: req.body.name,
          motivation: req.body.motivation,
          image: imgD,
          user_id: key._id,
          emojiIcon: req.body.emojiIcon,
          description: req.body.description,
          waiting_number: req.body.waiting_number,
          waiting_key: req.body.waiting_key,
          waiting_time: req.body.waiting_time,
          seat: (typeof req.body.seat == "string") ? [req.body.seat] : req.body.seat,
          tags: req.body.tags,
          price: req.body.price,
        }


        ServiceModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            ...data
          },
        }, { new: true }).then(async (cc) => {

          if (cc == null || cc == undefined) {
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: cc
            })
          }

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: cc
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err) => {
          console.log('Service Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Service Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    }

  })


}

export const changeServiceAcceptStatus = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  var key: any = req.user
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  ServiceModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      accept: req.body.accept
    }
  }).then(async (cc) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })

    const userUpdateNew = await UserUpdateModel.create({
      type: 'Service Update',
      old: { accpet: cc.accept },
      new: { accept: req.body.accept },
      service_name: cc.name,
      user: false,
      user_id: key._id
    })
  }).catch((err) => {
    console.log('Service Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const cloneService = async (req: Request, res: Response, next: NextFunction) => {

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }


  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  console.log('apit, hit')

  var key: any = req.user

  const promise1 = new Promise((resolve, reject) => {
    ShopModel.findOne({ user_information: key._id }).then((cc) => {
      resolve(cc)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise2 = new Promise((resolve, reject) => {
    ServiceModel.findOne({ _id: req.params.id }).then((cc) => {
      resolve(cc)
    }).catch((err) => {
      reject(err)
    })
  })

  Promise.allSettled([promise1, promise2]).then((item: any) => {

    try {

      const shopData = item[0].value

      const serviceData = item[1].value

      console.log('shopData', shopData, 'serve', serviceData)

      if (serviceData == null || serviceData == undefined) {
        return res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: 'err'
        })
      }

      const newService = new ServiceModel({
        emojiIcon: serviceData.emojiIcon,
        motivation: serviceData.motivation,
        name: serviceData.name,
        image: serviceData.image,
        description: serviceData.description,
        user_id: key._id,
        waiting_number: serviceData.waiting_number,
        waiting_key: serviceData.waiting_key,
        waiting_time: serviceData.waiting_time,
        seat: [1],
        tags: serviceData.tags,
        price: serviceData.price,
      })

      newService.save().then((cc: any) => {

        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: [{
            name: cc.name,
            motivation: cc.motivation,
            image: cc.image,
            emojiIcon: cc.emojiIcon,
            description: cc.description,
            waiting_number: cc.waiting_number,
            waiting_key: cc.waiting_key,
            waiting_time: cc.waiting_time,
            seat: cc.seat,
            tags: cc.tags,
            createdAt: cc.createdAt,
            updatedAt: cc.updatedAt,
            _id: cc._id,
            status: cc.status,
            accept: cc.accept,
            user_id: cc.user_id,
            price: cc.price,
            type: shopData.type,
            user_name: shopData.shop_name
          }]
        })

      }).catch((err: Error) => {
        console.log('Service Clone Err Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } catch (err) {
      console.log('Service Clone Err Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

  }).catch((err) => {
    console.log('Service Clone Err Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const cloneAllService = async (req: Request, res: Response, next: NextFunction) => {

  console.log('req.body', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.id == "" || req.body.id.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  var key: any = req.user

  try {

    const shopData = await ShopModel.findOne({ user_information: key._id })

    const allService = await ServiceModel.find({ _id: { $in: req.body.id } })

    if (allService.length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    const createAllServiceD = await ServiceModel.insertMany(allService.map((cc) => {
      return {
        emojiIcon: cc.emojiIcon,
        motivation: cc.motivation,
        name: cc.name,
        image: cc.image,
        description: cc.description,
        user_id: key._id,
        waiting_number: cc.waiting_number,
        waiting_key: cc.waiting_key,
        waiting_time: cc.waiting_time,
        seat: [1],
        tags: cc.tags,
        price: cc.price,
      }
    }))

    console.log('all Service Created', createAllServiceD)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: createAllServiceD.map((cc) => { return { ...cc._doc, type: shopData.type, user_name: shopData.shop_name } })
    })

  } catch (err) {
    console.log('All Services Clone Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  }



}

export const deleteService = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  console.log('body', req.params.id)
  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }


  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  ServiceModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      deleted: true
    }
  }, { new: true }).then((cc) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Service Deelete Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const serviceCsvImport = async (req: Request, res: Response, next: NextFunction) => {

  if (!req.body || req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.data == null || req.body.data == undefined || req.body.data.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: 'Data Not Found',
      data: 'Data Not Found'
    })
  }

  var key: any = req.user
  var serviceArr: any = []

  req.body.data.map((cc: any) => {
    if (cc.id == "" || cc.id == null || cc.id == undefined) {
      serviceArr.push({
        id: cc.id,
        data: {
          name: cc["name"],
          motivation: cc["motivation"],
          emojiIcon: cc["emojiIcon"],
          seat: cc["seat"],
          waiting_key: cc["waiting_key"],
          waiting_number: cc["waiting_number"],
          description: cc["description"].trim(),
          image: cc["image"],
          user_id: isValidObjectId(cc["user_id"]) ? cc["user_id"] : null,
          admin_id: key._id,
          tags: cc["tags"],
          price: cc["price"],
        }
      })
    } else {
      serviceArr.push({
        id: cc.id,
        data: {
          name: cc["name"],
          motivation: cc["motivation"],
          emojiIcon: cc["emojiIcon"],
          seat: cc["seat"],
          waiting_key: cc["waiting_key"],
          waiting_number: cc["waiting_number"],
          description: cc["description"].trim(),
          image: cc["image"],
          tags: cc["tags"],
          price: cc["price"],
        },
      })
    }
  })

  try {

    Promise.all(serviceArr.map(async (cc: any) => {
      const createS = await ServiceModel.updateMany({ _id: isValidObjectId(cc.id) ? cc.id : new mongoose.Types.ObjectId() }, { $set: cc.data }, { upsert: true, setDefaultsOnInsert: true, new: true })
      console.log('Updated', createS)
      return createS
    })).then((cc) => {

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: cc,
        check: serviceArr
      })

    }).catch((err) => {
      console.log('Service Csv Import Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })

    })

  } catch (err) {

    console.log('Service Csv Import Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  }

}

/// quuery api

export const SearchServiceQuery = (req: Request, res: Response, next: NextFunction, verify: any) => {

  var startTime = performance.now()
  const query = [
    {
      $match: {
        $text: {
          $search: req.params.id
        }
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        },
        'type': {
          '$arrayElemAt': [
            '$shop_id.type', 0
          ]
        }
      }
    },
    {
      '$project': {
        'admin': 0,
        'shop_id': 0
      }
    }
  ]
  ServiceModel.aggregate(query).exec(async (err, item) => {

    if (err) {
      console.log('Service Query Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'services',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  })


}

export const SearchServiceRegexQuery = (req: Request, res: Response, next: NextFunction, verify: any) => {

  var startTime = performance.now()
  const query = [
    {
      $match: {
        $or: [
          { "name": { $regex: req.params.id, $options: "i" } },
          { "motivation": { $regex: req.params.id, $options: "i" } },
          { "description": { $regex: req.params.id, $options: "i" } },
        ],
        deleted: false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        },
        'type': {
          '$arrayElemAt': [
            '$shop_id.type', 0
          ]
        }
      }
    },
    {
      '$project': {
        'admin': 0,
        'shop_id': 0
      }
    }
  ]
  ServiceModel.aggregate(query).exec(async (err, item) => {

    if (err) {
      console.log('Service Query Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'services',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  })


}

export const SearchServiceShopIdRegexQuery = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  var startTime = performance.now()
  const query = [
    {
      $match: {
        $or: [
          { "name": { $regex: req.params.id, $options: "i" } },
          { "motivation": { $regex: req.params.id, $options: "i" } },
          { "description": { $regex: req.params.id, $options: "i" } },
        ],
        'deleted': false,
        'user_id': new mongoose.Types.ObjectId(key._id),
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        },
        'type': {
          '$arrayElemAt': [
            '$shop_id.type', 0
          ]
        }
      }
    },
    {
      '$project': {
        'admin': 0,
        'shop_id': 0
      }
    }
  ]
  ServiceModel.aggregate(query).exec(async (err, item) => {

    if (err) {
      console.log('Service Query Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'services',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  })


}

export const SearchAdminServiceRegexQuery = (req: Request, res: Response, next: NextFunction) => {

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        }
      }
    },
    {
      '$project': {
        'admin': 0,
        'shop_id': 0
      }
    },
    {
      $match: {
        $or: [
          { "name": { $regex: req.params.id, $options: "i" } },
          { "motivation": { $regex: req.params.id, $options: "i" } },
          { "description": { $regex: req.params.id, $options: "i" } },
          { "user_name": { $regex: req.params.id, $options: "i" } },
        ]
      }
    }
  ]
  ServiceModel.aggregate(query).exec(async (err, item) => {

    if (err) {
      console.log('Service Query Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'services',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  })


}

export const getAllBarberServices = (req: Request, res: Response, next: NextFunction, verify: any) => {

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        'status': true,
        deleted: false
      }
    }, {
      '$lookup': {
        'from': 'shopcategories',
        'localField': 'type',
        'foreignField': '_id',
        'as': 'type'
      }
    }, {
      '$unwind': {
        'path': '$type'
      }
    }, {
      '$match': {
        'type.name': 'Barber'
      }
    },
    {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    },
    {
      '$unwind': {
        'path': '$topics'
      }
    },
    {
      '$addFields': {
        'topics.user_name': '$shop_name',
        'topics.type': '$type._id'
      }
    },
    {
      '$group': {
        '_id': null,
        'data': {
          '$push': '$topics'
        }
      }
    }
  ]
  ShopModel.aggregate(query).then(async (cc: any) => {
    //console.log('Data fetched', cc)

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'shops',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (cc.length > 0) ? cc[0].data : []
    })
  }).catch((err) => {
    console.log('Shop Active Barber Service Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const getAllBarberTestServices = (req: Request, res: Response, next: NextFunction, verify: any) => {

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        'status': true,
        deleted: false
      }
    }, {
      '$lookup': {
        'from': 'shopcategories',
        'localField': 'type',
        'foreignField': '_id',
        'as': 'type'
      }
    }, {
      '$unwind': {
        'path': '$type'
      }
    }, {
      '$match': {
        'type.name': 'Barber'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    }, {
      '$unwind': {
        'path': '$topics'
      }
    }, {
      '$addFields': {
        'topics.user_name': '$shop_name',
        'topics.type': '$type._id',
        'topics.shop': '$_id'
      }
    }, {
      '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
    }, {
      '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
    }, {
      '$group': {
        '_id': null,
        'data': {
          '$push': '$topics'
        }
      }
    }
  ]
  ShopModel.aggregate(query).then(async (cc: any) => {
    //console.log('Data fetched', cc)
    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'shops',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (cc.length > 0) ? cc[0].data : []
    })
  }).catch((err) => {
    console.log('Shop Active Barber Service Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const changeMultipleServiceStatusByAdmin = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.id == null || req.body.id == undefined || req.body.id.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  ServiceModel.updateMany({ _id: req.body.id.map((cc: any) => cc) }, {
    $set: {
      ...req.body.status
    }
  }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Service Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}


export const getSingleService = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user

  const query = [
    {
      '$match': {
        '_id': new mongoose.Types.ObjectId(req.params.id),
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        },
        'type': {
          '$arrayElemAt': [
            '$shop_id.type', 0
          ]
        }
      }
    },
    {
      '$project': {
        'admin': 0,
        'shop_id': 0
      }
    }
  ]
  ServiceModel.aggregate(query).then(async (item) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item : []
    })
  }).catch((err) => {
    console.log('Service Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}