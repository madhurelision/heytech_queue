import { NextFunction, Request, Response } from 'express';
import { EmployeeIdGenerate } from '../../../config/keys';
import { EmployeeModel } from '../../../Models/Employee';
import { isValidObjectId } from 'mongoose'
import { performance } from 'perf_hooks'
import { LogQueryModel } from '../../../Models/LogData';
import mongoose from 'mongoose'


export const CreateEmployee = async (req: Request, res: Response, next: NextFunction, verify: any) => {
  console.log('data', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  var key: any = req.user

  const findEmployee = await EmployeeModel.find({ user_id: key._id, mobile: req.body.mobile, deleted: false })

  if (findEmployee.length > 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Employee Already Registered with the Number",
      data: 'err'
    })
  }
  var data = req.body
  data.employee_id = EmployeeIdGenerate()
  data.user_id = key._id
  data.status = true
  data.create_type = 'Shop'
  data.otp_verify = true

  const employeeData = new EmployeeModel(data)

  employeeData.save().then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err: Error) => {
    console.log('Employee Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const getShopEmployee = (req: Request, res: Response, next: NextFunction, verify: any) => {
  var key: any = req.user

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        'user_id': new mongoose.Types.ObjectId(key._id),
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'loginlogs',
        'localField': '_id',
        'foreignField': 'user_id',
        'as': 'log'
      }
    }
  ]

  EmployeeModel.aggregate(query).then(async (item) => {

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'employees',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { employee: item }
    })
  }).catch((err) => {
    console.log('Employee Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const changeEmployeeStatus = (req: Request, res: Response, next: NextFunction, verify: any) => {
  var key: any = req.user
  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }


  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  EmployeeModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      status: req.body.status
    }
  }, { new: true }).then((cc) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Employee Status Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const EditEmployee = (req: Request, res: Response, next: NextFunction, verify: any) => {
  console.log('body', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }


  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  var data

  data = {
    name: req.body.name,
    type: req.body.type,
    mobile: req.body.mobile,
  }


  EmployeeModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      ...data
    }
  }, { new: true }).then((cc) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Employee Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const deleteEmployee = (req: Request, res: Response, next: NextFunction, verify: any) => {
  var key: any = req.user
  console.log('body', req.params.id)
  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }


  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  EmployeeModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      deleted: true
    }
  }, { new: true }).then((cc) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Employee Status Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}


export const changeMultipleEmployeeStatusByAdmin = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.id == null || req.body.id == undefined || req.body.id.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  EmployeeModel.updateMany({ _id: req.body.id.map((cc: any) => cc) }, {
    $set: {
      ...req.body.status
    }
  }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Employee Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}