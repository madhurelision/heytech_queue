import { NextFunction, Request, Response } from 'express';
import { SECRET_KEY, SERVER_URL, otpGenerate, ServiceSmsMail, MAIL_SECRET_KEY, randomImageName, bookingOTPMsg, ServiceTemplateId, loginMsg, getDateArray } from '../../../config/keys';
import { ShopModel, UserModel } from '../../../Models/User';
import jwt from 'jsonwebtoken';
import axios from 'axios';
import { isValidObjectId } from 'mongoose';
import mongoose from 'mongoose'

export const getShopWiseDateBooking = async (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.body)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  const startDate = new Date(req.body.startDate)
  const endDate = new Date(req.body.endDate)

  var Arr: any = getDateArray(startDate, endDate)

  console.log('mainD', Arr)

  Promise.all(Arr.map(async (cc: any, index: any) => {

    const mainD = await ShopModel.aggregate([
      {
        '$match': {
          user_information: new mongoose.Types.ObjectId(val._id)
        }
      },
      {
        '$lookup': {
          'from': 'bookings',
          'let': { 'user_information': '$user_information' },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$gte': [
                        '$createdAt', new Date(new Date(cc).setHours(0o0, 0o0, 0o0))
                      ]
                    }, {
                      '$lt': [
                        '$createdAt', new Date(new Date(cc).setHours(23, 59, 59))
                      ]
                    },
                    // {
                    //   '$eq': [
                    //     '$queue', req.body.type
                    //   ]
                    // },
                    {
                      '$eq': [
                        '$shop_id', '$$user_information'
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'booking'
        }
      },
      {
        '$addFields': {
          'appointment': {
            '$size': {
              '$filter': {
                'input': '$booking',
                'as': 'app',
                'cond': {
                  '$eq': [
                    '$$app.queue', false
                  ]
                }
              }
            }
          },
          'queue': {
            '$size': {
              '$filter': {
                'input': '$booking',
                'as': 'app',
                'cond': {
                  '$eq': [
                    '$$app.queue', true
                  ]
                }
              }
            }
          },
          'total': {
            '$size': '$booking'
          },
          'verify': {
            '$size': {
              '$filter': {
                'input': '$booking',
                'as': 'app',
                'cond': {
                  '$eq': [
                    '$$app.otp_verify', true
                  ]
                }
              }
            }
          },
          'notVerify': {
            '$size': {
              '$filter': {
                'input': '$booking',
                'as': 'app',
                'cond': {
                  '$eq': [
                    '$$app.otp_verify', false
                  ]
                }
              }
            }
          },
          'completed': {
            '$size': {
              '$filter': {
                'input': '$booking',
                'as': 'app',
                'cond': {
                  '$eq': [
                    '$$app.status', 'completed'
                  ]
                }
              }
            }
          },
          'expired': {
            '$size': {
              '$filter': {
                'input': '$booking',
                'as': 'app',
                'cond': {
                  '$eq': [
                    '$$app.status', 'expired'
                  ]
                }
              }
            }
          },
        }
      }
    ]).exec()

    return { 'date': cc, 'value': mainD.length > 0 ? mainD[0] : {} }
  })).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { booking: cc }
    })
  }).catch((err) => {
    console.log('Primisee Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const getShopWiseTotalBooking = async (req: Request, res: Response, next: NextFunction) => {

  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  ShopModel.aggregate([
    {
      '$match': {
        user_information: new mongoose.Types.ObjectId(val._id)
      }
    },
    {
      '$lookup': {
        'from': 'bookings',
        'let': { 'user_information': '$user_information' },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  // {
                  //   '$gte': [
                  //     '$createdAt', new Date(new Date(cc).setHours(0o0, 0o0, 0o0))
                  //   ]
                  // }, {
                  //   '$lt': [
                  //     '$createdAt', new Date(new Date(cc).setHours(23, 59, 59))
                  //   ]
                  // },
                  // {
                  //   '$eq': [
                  //     '$queue', req.body.type
                  //   ]
                  // },
                  {
                    '$eq': [
                      '$shop_id', '$$user_information'
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'booking'
      }
    },
    {
      '$addFields': {
        'appointment': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.queue', false
                ]
              }
            }
          }
        },
        'queue': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.queue', true
                ]
              }
            }
          }
        },
        'total': {
          '$size': '$booking'
        },
        'verify': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.otp_verify', true
                ]
              }
            }
          }
        },
        'notVerify': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.otp_verify', false
                ]
              }
            }
          }
        },
        'completed': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.status', 'completed'
                ]
              }
            }
          }
        },
        'expired': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.status', 'expired'
                ]
              }
            }
          }
        }
      }
    }
  ]).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { booking: cc.length > 0 ? cc[0] : {} }
    })
  }).catch((err) => {
    console.log('Primisee Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}