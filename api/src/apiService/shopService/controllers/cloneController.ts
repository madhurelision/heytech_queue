import { ServiceModel } from "../../../Models/Service";
import { NextFunction, Request, Response } from 'express';
import { ShopModel } from "../../../Models/User";
import { UserUpdateModel } from "../../../Models/UserUpdates";


export const cloneAllServiceWithUpdate = async (req: Request, res: Response, next: NextFunction) => {

  console.log('req.body', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.id == "" || req.body.id.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  var key: any = req.user

  try {

    const shopData = await ShopModel.findOne({ user_information: key._id })

    const allService = await ServiceModel.find({ _id: { $in: req.body.id } })

    if (allService.length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    const createAllServiceD = await ServiceModel.insertMany(allService.map((cc) => {
      return {
        emojiIcon: cc.emojiIcon,
        motivation: cc.motivation,
        name: cc.name,
        image: cc.image,
        description: cc.description,
        user_id: key._id,
        waiting_number: cc.waiting_number,
        waiting_key: cc.waiting_key,
        waiting_time: cc.waiting_time,
        seat: [1],
        tags: cc.tags,
        price: cc.price,
      }
    }))

    console.log('all Service Created', createAllServiceD)

    var data = {
      topics: shopData.topics.concat(createAllServiceD.map((cc) => cc._id)),
    }

    console.log('Id Updated')

    ShopModel.findOneAndUpdate({ user_information: key._id }, {
      $set: {
        ...data
      }
    }).then(async (cc) => {


      if (cc == null || cc == undefined) {
        return res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: 'err'
        })
      }

      const userUpdateNew = await UserUpdateModel.create({
        type: 'Shop Service',
        old: { topics: cc.topics },
        new: { ...data },
        user_name: cc.shop_name,
        user_id: cc.user_information
      })

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: createAllServiceD.map((cc) => { return { ...cc._doc, type: shopData.type, user_name: shopData.shop_name } })
      })

    }).catch((err) => {
      console.log('Shop Update Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    })

  } catch (err) {
    console.log('All Services Clone Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  }



}