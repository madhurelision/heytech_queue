import { NextFunction, Request, Response } from 'express';
import { SECRET_KEY, SERVER_URL, otpGenerate, ServiceSmsMail, MAIL_SECRET_KEY, randomImageName, bookingOTPMsg, ServiceTemplateId, loginMsg } from '../../../config/keys';
import { ShopModel, UserModel } from '../../../Models/User';
import jwt from 'jsonwebtoken';
import QRCode from 'qrcode';
import multer from 'multer';
import { upload } from '../../../config/uploadImage';
import { ImageUpload, removeImage } from '../../../ImageService/main';
import { Con } from '../../../ImageService/source';
import axios from 'axios';
import { isValidObjectId } from 'mongoose';
import mongoose from 'mongoose'
import { UserUpdateModel } from '../../../Models/UserUpdates'
import config from 'config'
import { CustomerModel } from '../../../Models/Customer';
import { NotificationModel } from '../../../Models/Notification';
import { LoginLogModel } from '../../../Models/LoginLog';
import { performance } from 'perf_hooks'
import { LogQueryModel } from '../../../Models/LogData';

export const google = async (req: Request, accessToken: String, refreshToken: String, params: any, profile: any, done: Function) => {
  console.log('Google Profile Data ', profile)
  console.log('Google Profile Params ', params)

  UserModel.findOne({ email: profile._json.email, deleted: false }).then((item: any) => {

    if (item == null || item == undefined) {

      const UserData = new UserModel(
        {
          first_name: profile._json.given_name,
          last_name: profile._json.family_name,
          email: profile._json.email,
          image_link: profile._json.picture,
          google: profile.id,
          oauth_provider: profile.provider,
          is_mentor: true,
          signup_completed: false,
          lastLogin: new Date()
        }
      )

      UserData.save().then((main: any) => {
        console.log('inserted record', main);
        //done(null, { _id: response.insertedId })
        done(null, main)

      }).catch((err: Error) => {
        console.log('Error occurred while inserting', err);
        done(err)
      })

    } else {

      UserModel.findOneAndUpdate({ email: profile._json.email, deleted: false }, {
        $set: {
          // first_name: profile._json.given_name,
          // last_name: profile._json.family_name,
          email: profile._json.email,
          google: profile.id,
          oauth_provider: profile.provider,
          lastLogin: new Date()
        }
      }, { new: true }).populate('mentor_information').then(async (cc: any) => {
        console.log('updated record', cc);
        const logingS = await ShopModel.updateMany({ user_information: cc._id, deleted: false }, {
          $set: {
            lastLogin: new Date()
          }
        })
        //done(null, { _id: response.insertedId })
        done(null, cc)

      }).catch((err: Error) => {
        console.log('Error occurred while updating', err);
        done(err)

      })

    }

  }).catch((err: Error) => {
    console.log('Google Find error', err);
    return done(err)
  })

}

export const createShopUser = (req: Request, res: Response, next: NextFunction) => {
  const MainCheck = upload.fields([
    {
      name: 'image', maxCount: 1
    },
    {
      name: 'banner', maxCount: 1
    }
  ])

  MainCheck(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 5 MB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('image', req.file)
    console.log('body', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data
    var file: any = req.files

    if (file == null || file == undefined || Object.keys(file).length == 0) {

      data = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        shop_name: req.body.shop_name,
        image_link: req.body.oldimage,
        banner_link: req.body.bannerimage,
        email: req.body.email,
        company: req.body.company,
        description: req.body.description,
        expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
        language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
        instagram: req.body.instagram,
        //time_slot: JSON.parse(req.body.time_slot),
        user_information: req.params.id,
        waiting_number: req.body.waiting_number,
        waiting_key: req.body.waiting_key,
        waiting_time: req.body.waiting_time,
        location: req.body.location,
        lattitude: req.body.lattitude,
        longitude: req.body.longitude,
        seat: req.body.seat,
        mobile: req.body.mobile,
        type: req.body.type,
        locate: {
          type: "Point",
          coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
        }
      }

      ShopModel.findOneAndUpdate({ email: req.body.email, deleted: false }, {
        $set: {
          ...data
        }
      }, {
        upsert: true, setDefaultsOnInsert: true,
        new: true,
      }).then((item: any) => {

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: item
          })
        }

        console.log('main Item', item)

        UserModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            signup_completed: true,
            mentor_information: item._id,
            mobile: req.body.mobile,
            email: req.body.email,
          }
        }, { new: true }).then((val: any) => {

          if (val == null || val == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: val
            })
          }

          const token: any = jwt.sign({ _id: val._id, email: val.email }, SECRET_KEY)
          res.header("auth-token", token)

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: item,
            token: token
          })
        }).catch((err) => {
          console.log('uuser Merchant Id', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })
      }).catch((err: Error) => {
        console.log('Shop Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else if (file.banner == undefined && file.image.length > 0) {


      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.image[0].filename, file.image[0].filename).then((item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        data = {
          first_name: req.body.first_name,
          last_name: req.body.last_name,
          shop_name: req.body.shop_name,
          image_link: item?.response.url,
          email: req.body.email,
          company: req.body.company,
          description: req.body.description,
          expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
          language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
          instagram: req.body.instagram,
          //time_slot: JSON.parse(req.body.time_slot),
          user_information: req.params.id,
          waiting_number: req.body.waiting_number,
          waiting_key: req.body.waiting_key,
          waiting_time: req.body.waiting_time,
          location: req.body.location,
          lattitude: req.body.lattitude,
          longitude: req.body.longitude,
          seat: req.body.seat,
          mobile: req.body.mobile,
          type: req.body.type,
          locate: {
            type: "Point",
            coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
          }
        }

        ShopModel.findOneAndUpdate({ email: req.body.email, deleted: false }, {
          $set: {
            ...data
          }
        }, {
          upsert: true, setDefaultsOnInsert: true,
          new: true,
        }).then((item: any) => {

          if (item == null || item == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: item
            })
          }

          UserModel.findOneAndUpdate({ _id: req.params.id }, {
            $set: {
              signup_completed: true,
              mentor_information: item._id,
              mobile: req.body.mobile,
              email: req.body.email,
            }
          }).then((val: any) => {

            if (val == null || val == undefined) {
              return res.status(400).json({
                status: 'Failed',
                status_code: res.statusCode,
                message: "Request Failed",
                data: val
              })
            }

            const token: any = jwt.sign({ _id: val._id, email: val.email }, SECRET_KEY)
            res.header("auth-token", token)

            res.status(200).json({
              status: 'Success',
              status_code: res.statusCode,
              message: "Request Success", data: item, token: token
            })
            removeImage(process.cwd() + "/src/upload/" + file.image[0]?.filename)
          }).catch((err) => {
            console.log('uuser Merchant Id', err)
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: err
            })
          })
        }).catch((err: Error) => {
          console.log('Shop Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })

      }).catch((err) => {
        console.log('Shop User Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    } else if (file.image == undefined && file.banner.length > 0) {


      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.banner[0].filename, file.banner[0].filename).then((item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        data = {
          first_name: req.body.first_name,
          last_name: req.body.last_name,
          shop_name: req.body.shop_name,
          banner_link: item?.response.url,
          email: req.body.email,
          company: req.body.company,
          description: req.body.description,
          expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
          language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
          instagram: req.body.instagram,
          //time_slot: JSON.parse(req.body.time_slot),
          user_information: req.params.id,
          waiting_number: req.body.waiting_number,
          waiting_key: req.body.waiting_key,
          waiting_time: req.body.waiting_time,
          location: req.body.location,
          lattitude: req.body.lattitude,
          longitude: req.body.longitude,
          seat: req.body.seat,
          mobile: req.body.mobile,
          type: req.body.type,
          locate: {
            type: "Point",
            coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
          }
        }

        ShopModel.findOneAndUpdate({ email: req.body.email, deleted: false }, {
          $set: {
            ...data
          }
        }, {
          upsert: true, setDefaultsOnInsert: true,
          new: true,
        }).then((item: any) => {

          if (item == null || item == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: item
            })
          }

          UserModel.findOneAndUpdate({ _id: req.params.id }, {
            $set: {
              signup_completed: true,
              mentor_information: item._id,
              mobile: req.body.mobile,
              email: req.body.email,
            }
          }).then((val: any) => {

            if (val == null || val == undefined) {
              return res.status(400).json({
                status: 'Failed',
                status_code: res.statusCode,
                message: "Request Failed",
                data: val
              })
            }

            const token: any = jwt.sign({ _id: val._id, email: val.email }, SECRET_KEY)
            res.header("auth-token", token)

            res.status(200).json({
              status: 'Success',
              status_code: res.statusCode,
              message: "Request Success", data: item, token: token
            })
            removeImage(process.cwd() + "/src/upload/" + file.banner[0]?.filename)
          }).catch((err) => {
            console.log('uuser Merchant Id', err)
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: err
            })
          })
        }).catch((err: Error) => {
          console.log('Shop Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })

      }).catch((err) => {
        console.log('Shop User Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    } else if (file.banner.length > 0 && file.image.length > 0) {


      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.image[0].filename, file.image[0].filename).then((item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.banner[0].filename, file.banner[0].filename).then((band) => {
          console.log('band', band)

          if (band == null || band == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Image is Required",
              data: 'err'
            })
          }

          data = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            shop_name: req.body.shop_name,
            image_link: item?.response.url,
            banner_link: band?.response.url,
            email: req.body.email,
            company: req.body.company,
            description: req.body.description,
            expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
            language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
            instagram: req.body.instagram,
            //time_slot: JSON.parse(req.body.time_slot),
            user_information: req.params.id,
            waiting_number: req.body.waiting_number,
            waiting_key: req.body.waiting_key,
            waiting_time: req.body.waiting_time,
            location: req.body.location,
            lattitude: req.body.lattitude,
            longitude: req.body.longitude,
            seat: req.body.seat,
            mobile: req.body.mobile,
            type: req.body.type,
            locate: {
              type: "Point",
              coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
            }
          }

          ShopModel.findOneAndUpdate({ email: req.body.email, deleted: false }, {
            $set: {
              ...data
            }
          }, {
            upsert: true, setDefaultsOnInsert: true,
            new: true,
          }).then((item: any) => {

            if (item == null || item == undefined) {
              return res.status(400).json({
                status: 'Failed',
                status_code: res.statusCode,
                message: "Request Failed",
                data: item
              })
            }

            UserModel.findOneAndUpdate({ _id: req.params.id }, {
              $set: {
                signup_completed: true,
                mentor_information: item._id,
                mobile: req.body.mobile,
                email: req.body.email,
              }
            }).then((val: any) => {

              if (val == null || val == undefined) {
                return res.status(400).json({
                  status: 'Failed',
                  status_code: res.statusCode,
                  message: "Request Failed",
                  data: val
                })
              }

              const token: any = jwt.sign({ _id: val._id, email: val.email }, SECRET_KEY)
              res.header("auth-token", token)

              res.status(200).json({
                status: 'Success',
                status_code: res.statusCode,
                message: "Request Success", data: item, token: token
              })
              removeImage(process.cwd() + "/src/upload/" + file.image[0]?.filename)
              removeImage(process.cwd() + "/src/upload/" + file.banner[0]?.filename)
            }).catch((err) => {
              console.log('uuser Merchant Id', err)
              res.status(400).json({
                status: 'Failed',
                status_code: res.statusCode,
                message: "Request Failed",
                data: err
              })
            })
          }).catch((err: Error) => {
            console.log('Shop Create Err', err)
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: err
            })
          })


        }).catch((err) => {
          console.log('Shop User Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Shop User Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    }
  })
}

export const SingleUser = (req: Request, res: Response, next: NextFunction) => {

  UserModel.findOne({ _id: req.params.id }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}


export const getShopUserProfile = (req: Request, res: Response) => {
  console.log('Shop User', req.user)

  const val: any = req.user

  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  ShopModel.findOne({ user_information: val._id }).populate('topics').then((item) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: item
      })
    }

    var MentorData: any = item

    const token: any = jwt.sign({ _id: MentorData.user_information, email: MentorData.email }, SECRET_KEY)
    res.header("auth-token", token)

    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: item,
      token: token,
      type: 'shop'
    })

  }).catch((err) => {
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

// export const updateShopUser = async (req: Request, res: Response, next: NextFunction) => {
//   const MainCheck = upload.single('image')

//   MainCheck(req, res, function (err) {
//     if (err instanceof multer.MulterError) {
//       //console.log('Multer Error', err)
//       if (err.code == 'LIMIT_FILE_SIZE') {
//         err.message = 'File Size is too large. Allowed file size is 5 MB';
//       }
//       return res.status(400).json({
//         status: 'Failed',
//         status_code: res.statusCode,
//         message: err.message,
//         data: err
//       })
//       // A Multer error occurred when uploading.
//     } else if (err) {
//       //console.log('Other Error', err)
//       return res.status(400).json({
//         status: 'Failed',
//         status_code: res.statusCode,
//         message: 'Request Failed',
//         data: err
//       })
//     }

//     console.log('image', req.file)
//     console.log('body', req.body)

//     if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
//       return res.status(400).json({
//         status: 'Failed',
//         status_code: res.statusCode,
//         message: "Request Failed",
//         data: 'err'
//       })
//     }


//     console.log('image', req.file)
//     console.log('body', req.body)

//     var data: any
//     var key: any = req.user

//     if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

//       data = {
//         first_name: req.body.first_name,
//         last_name: req.body.last_name,
//         shop_name: req.body.shop_name,
//         image_link: req.body.oldimage,
//         email: req.body.email,
//         company: req.body.company,
//         description: req.body.description,
//         expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
//         language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
//         //topics: (typeof req.body.topics == "string") ? [req.body.topics] : req.body.topics,
//         instagram: req.body.instagram,
//         time_slot: JSON.parse(req.body.time_slot),
//         waiting_number: req.body.waiting_number,
//         waiting_key: req.body.waiting_key,
//         waiting_time: req.body.waiting_time,
//         location: req.body.location,
//         lattitude: req.body.lattitude,
//         longitude: req.body.longitude,
//         seat: req.body.seat,
//         type: req.body.type,
//         locate: {
//           type: "Point",
//           coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
//         }
//       }


//       ShopModel.findOneAndUpdate({ user_information: key._id }, {
//         $set: {
//           ...data
//         }
//       }).then(async (cc) => {
//         res.status(200).json({
//           status: 'Success',
//           status_code: res.statusCode,
//           message: "Request Success",
//           data: cc
//         })


//         const userUpdateNesw = await UserUpdateModel.create({
//           type: 'Shop Update',
//           old: cc,
//           new: data,
//           user_name: cc.shop_name,
//           user_id: key._id
//         })
//       }).catch((err) => {
//         console.log('Shop Update Err', err)
//         res.status(400).json({
//           status: 'Failed',
//           status_code: res.statusCode,
//           message: "Request Failed",
//           data: err
//         })
//       })

//     } else {



//       ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
//         console.log('item', item)

//         if (item == null || item == undefined) {
//           return res.status(400).json({
//             status: 'Failed',
//             status_code: res.statusCode,
//             message: "Image is Required",
//             data: 'err'
//           })
//         }

//         // const ImageCreate = await GalleryModel.create({
//         //   image: item?.response.url,
//         //   user_id: key._id
//         // })
//         // console.log('Image Made', ImageCreate)

//         data = {
//           first_name: req.body.first_name,
//           last_name: req.body.last_name,
//           shop_name: req.body.shop_name,
//           image_link: item?.response.url,
//           email: req.body.email,
//           company: req.body.company,
//           description: req.body.description,
//           expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
//           language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
//           //topics: (typeof req.body.topics == "string") ? [req.body.topics] : req.body.topics,
//           instagram: req.body.instagram,
//           time_slot: JSON.parse(req.body.time_slot),
//           waiting_number: req.body.waiting_number,
//           waiting_key: req.body.waiting_key,
//           waiting_time: req.body.waiting_time,
//           location: req.body.location,
//           lattitude: req.body.lattitude,
//           longitude: req.body.longitude,
//           seat: req.body.seat,
//           type: req.body.type,
//           locate: {
//             type: "Point",
//             coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
//           }
//         }


//         ShopModel.findOneAndUpdate({ user_information: key._id }, {
//           $set: {
//             ...data
//           }
//         }).then(async (cc) => {
//           res.status(200).json({
//             status: 'Success',
//             status_code: res.statusCode,
//             message: "Request Success",
//             data: cc
//           })

//           const userUpdateNew = await UserUpdateModel.create({
//             type: 'Shop Update',
//             old: cc,
//             new: { ...data },
//             user_name: cc.shop_name,
//             user_id: key._id
//           })
//           removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
//         }).catch((err) => {
//           console.log('Shop Update Err', err)
//           res.status(400).json({
//             status: 'Failed',
//             status_code: res.statusCode,
//             message: "Request Failed",
//             data: err
//           })
//         })


//       }).catch((err) => {
//         console.log('Shop User Update Err', err)
//         res.status(400).json({
//           status: 'Failed',
//           status_code: res.statusCode,
//           message: "Request Failed",
//           data: err
//         })
//       })
//     }


//   })

// }

export const updateShopUser = async (req: Request, res: Response, next: NextFunction) => {
  const MainCheck = upload.fields([
    {
      name: 'image', maxCount: 1
    },
    {
      name: 'banner', maxCount: 1
    }
  ])

  MainCheck(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 5 MB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('image', req.file)
    console.log('body', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }


    console.log('image', req.files)
    console.log('image', req.file)
    console.log('body', req.body)

    var data: any
    var key: any = req.user
    var file: any = req.files

    if (file == null || file == undefined || Object.keys(file).length == 0) {

      data = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        shop_name: req.body.shop_name,
        image_link: req.body.oldimage,
        banner_link: req.body.bannerimage,
        email: req.body.email,
        company: req.body.company,
        description: req.body.description,
        expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
        language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
        //topics: (typeof req.body.topics == "string") ? [req.body.topics] : req.body.topics,
        instagram: req.body.instagram,
        time_slot: JSON.parse(req.body.time_slot),
        waiting_number: req.body.waiting_number,
        waiting_key: req.body.waiting_key,
        waiting_time: req.body.waiting_time,
        location: req.body.location,
        lattitude: req.body.lattitude,
        longitude: req.body.longitude,
        seat: req.body.seat,
        type: req.body.type,
        time_zone: req.body.time_zone,
        time_val: req.body.time_val,
        upi_id: req.body.upi_id,
        locate: {
          type: "Point",
          coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
        }
      }


      ShopModel.findOneAndUpdate({ user_information: key._id }, {
        $set: {
          ...data
        }
      }).then(async (cc) => {

        if (cc == null || cc == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: cc
          })
        }

        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: cc
        })


        const userUpdateNesw = await UserUpdateModel.create({
          type: 'Shop Update',
          old: cc,
          new: data,
          user_name: cc.shop_name,
          user_id: key._id
        })
      }).catch((err) => {
        console.log('Shop Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else if (file.banner == undefined && file.image.length > 0) {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.image[0].filename, file.image[0].filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        // const ImageCreate = await GalleryModel.create({
        //   image: item?.response.url,
        //   user_id: key._id
        // })
        // console.log('Image Made', ImageCreate)

        data = {
          first_name: req.body.first_name,
          last_name: req.body.last_name,
          shop_name: req.body.shop_name,
          image_link: item?.response.url,
          email: req.body.email,
          company: req.body.company,
          description: req.body.description,
          expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
          language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
          //topics: (typeof req.body.topics == "string") ? [req.body.topics] : req.body.topics,
          instagram: req.body.instagram,
          time_slot: JSON.parse(req.body.time_slot),
          waiting_number: req.body.waiting_number,
          waiting_key: req.body.waiting_key,
          waiting_time: req.body.waiting_time,
          location: req.body.location,
          lattitude: req.body.lattitude,
          longitude: req.body.longitude,
          seat: req.body.seat,
          type: req.body.type,
          time_zone: req.body.time_zone,
          time_val: req.body.time_val,
          upi_id: req.body.upi_id,
          locate: {
            type: "Point",
            coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
          }
        }

        ShopModel.findOneAndUpdate({ user_information: key._id }, {
          $set: {
            ...data
          }
        }).then(async (cc) => {

          if (cc == null || cc == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: cc
            })
          }

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: cc
          })

          const userUpdateNew = await UserUpdateModel.create({
            type: 'Shop Update',
            old: cc,
            new: { ...data },
            user_name: cc.shop_name,
            user_id: key._id
          })
          removeImage(process.cwd() + "/src/upload/" + file.image[0]?.filename)
        }).catch((err) => {
          console.log('Shop Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Shop User Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    } else if (file.image == undefined && file.banner.length > 0) {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.banner[0].filename, file.banner[0].filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        // const ImageCreate = await GalleryModel.create({
        //   image: item?.response.url,
        //   user_id: key._id
        // })
        // console.log('Image Made', ImageCreate)

        data = {
          first_name: req.body.first_name,
          last_name: req.body.last_name,
          shop_name: req.body.shop_name,
          banner_link: item?.response.url,
          email: req.body.email,
          company: req.body.company,
          description: req.body.description,
          expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
          language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
          //topics: (typeof req.body.topics == "string") ? [req.body.topics] : req.body.topics,
          instagram: req.body.instagram,
          time_slot: JSON.parse(req.body.time_slot),
          waiting_number: req.body.waiting_number,
          waiting_key: req.body.waiting_key,
          waiting_time: req.body.waiting_time,
          location: req.body.location,
          lattitude: req.body.lattitude,
          longitude: req.body.longitude,
          seat: req.body.seat,
          type: req.body.type,
          time_zone: req.body.time_zone,
          time_val: req.body.time_val,
          upi_id: req.body.upi_id,
          locate: {
            type: "Point",
            coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
          }
        }

        ShopModel.findOneAndUpdate({ user_information: key._id }, {
          $set: {
            ...data
          }
        }).then(async (cc) => {

          if (cc == null || cc == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: cc
            })
          }

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: cc
          })

          const userUpdateNew = await UserUpdateModel.create({
            type: 'Shop Update',
            old: cc,
            new: { ...data },
            user_name: cc.shop_name,
            user_id: key._id
          })
          removeImage(process.cwd() + "/src/upload/" + file.banner[0]?.filename)
        }).catch((err) => {
          console.log('Shop Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Shop User Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    } else if (file.image.length > 0 && file.banner.length > 0) {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.image[0].filename, file.image[0].filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.banner[0].filename, file.banner[0].filename).then(async (band) => {
          console.log('band', band)

          if (band == null || band == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Image is Required",
              data: 'err'
            })
          }


          data = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            shop_name: req.body.shop_name,
            image_link: item?.response.url,
            banner_link: band?.response.url,
            email: req.body.email,
            company: req.body.company,
            description: req.body.description,
            expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
            language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
            //topics: (typeof req.body.topics == "string") ? [req.body.topics] : req.body.topics,
            instagram: req.body.instagram,
            time_slot: JSON.parse(req.body.time_slot),
            waiting_number: req.body.waiting_number,
            waiting_key: req.body.waiting_key,
            waiting_time: req.body.waiting_time,
            location: req.body.location,
            lattitude: req.body.lattitude,
            longitude: req.body.longitude,
            seat: req.body.seat,
            type: req.body.type,
            time_zone: req.body.time_zone,
            time_val: req.body.time_val,
            upi_id: req.body.upi_id,
            locate: {
              type: "Point",
              coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
            }
          }

          ShopModel.findOneAndUpdate({ user_information: key._id }, {
            $set: {
              ...data
            }
          }).then(async (cc) => {

            if (cc == null || cc == undefined) {
              return res.status(400).json({
                status: 'Failed',
                status_code: res.statusCode,
                message: "Request Failed",
                data: cc
              })
            }

            res.status(200).json({
              status: 'Success',
              status_code: res.statusCode,
              message: "Request Success",
              data: cc
            })

            const userUpdateNew = await UserUpdateModel.create({
              type: 'Shop Update',
              old: cc,
              new: { ...data },
              user_name: cc.shop_name,
              user_id: key._id
            })
            removeImage(process.cwd() + "/src/upload/" + file.image[0]?.filename)
            removeImage(process.cwd() + "/src/upload/" + file.banner[0]?.filename)
          }).catch((err) => {
            console.log('Shop Update Err', err)
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: err
            })
          })


        }).catch((err) => {
          console.log('Shop User Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Shop User Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    }


  })

}

export const getAllExpertise = (req: Request, res: Response, next: NextFunction, verify: any) => {

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        'status': true,
        'deleted': false
      }
    },
    {
      '$unwind': {
        'path': '$expertise'
      }
    },
    {
      '$group': {
        '_id': 'null',
        'data': {
          '$push': {
            'label': '$expertise',
            'value': '$expertise',
            'type': '$type'
          }
        }
      }
    },
    // {
    //   '$addFields': {
    //     'data': {
    //       '$map': {
    //         'input': '$data',
    //         'as': 'num',
    //         'in': {
    //           'label': '$$num',
    //           'value': '$$num'
    //         }
    //       }
    //     }
    //   }
    // }
  ]
  ShopModel.aggregate(query).then(async (cc: any) => {
    //console.log('Data fetched', cc)
    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'shops',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (cc.length > 0) ? cc[0].data : []
    })
  }).catch((err) => {
    console.log('Shop Expertise Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}


// like Shop
export const likeShopUser = async (req: Request, res: Response, next: NextFunction, verify: any) => {

  console.log('req.body', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  const custData = await CustomerModel.findOne({ _id: req.body._id })

  ShopModel.findOneAndUpdate({ _id: req.params.id, like: { $nin: [req.body._id] } }, { $push: { like: req.body._id } }).then(async (item) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: item
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
    if (custData !== null || custData !== undefined) {
      const notify = await NotificationModel.create({
        user_id: custData._id,
        shop_id: item.user_information,
        name: custData.name,
        image: custData.image_link,
        shop_name: item.shop_name,
        shop_image: item.image_link,
        message: `Dear ${item.shop_name} your profile is liked by ${custData.name}`,
        type: 'like',
        like: true,
      })
      console.log('like Notify', notify)
    }
  }).catch((err) => {
    console.log('Unlike Shop User Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const getAllActiveServices = (req: Request, res: Response, next: NextFunction, verify: any) => {

  var url: any = req.url
  var startTime = performance.now()
  var query: any = [
    {
      '$match': (req.body.type != null && req.body.type != undefined && req.body.type != "" && req.body.type != "All" && req.body.type != 'All') ? {
        'status': true,
        'deleted': false,
        'type': new mongoose.Types.ObjectId(req.body.type)
      } : {
        'status': true,
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    }, {
      '$unwind': {
        'path': '$topics'
      }
    },
    {
      '$addFields': {
        'topics.user_name': '$shop_name',
        'topics.type': '$type'
      }
    }, {
      '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
    }, {
      '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
    },
    {
      '$group': {
        '_id': null,
        'data': {
          '$push': '$topics'
        }
      }
    }
  ]

  ShopModel.aggregate(query).then(async (cc: any) => {
    //console.log('Data fetched', cc)
    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'shops',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (cc.length > 0) ? cc[0].data : []
    })
  }).catch((err) => {
    console.log('Shop Active Service Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

//unlike Shop
export const unlikeShopUser = (req: Request, res: Response, next: NextFunction, verify: any) => {

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  ShopModel.findOneAndUpdate({ _id: req.params.id }, { $pull: { like: req.body._id } }).then((item) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: item
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err) => {
    console.log('Unlike Shop User Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

// create ShopToken
export const createToken = (req: Request, res: Response, next: NextFunction, verify: any) => {

  UserModel.findOne({ mentor_information: req.params.id }).then((cc: any) => {
    const token: any = jwt.sign({ _id: cc._id, email: cc.email }, SECRET_KEY)
    res.header("auth-token", token)
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: token
    })
  }).catch((err) => {
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const createOrChangeQrCode = (req: Request, res: Response, next: NextFunction) => {

  var key: any = req.user
  console.log('user', key)
  const urlString = config.has('SERVER_URL') ? config.get('SERVER_URL') + '/api/open/' + key.mentor_information : SERVER_URL + '/api/open/' + key.mentor_information
  console.log('qrcode urle', urlString)

  QRCode.toDataURL(urlString, function (err, data) {

    if (err) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    console.log('Url Created After Qe', data)

    ShopModel.findOneAndUpdate({ user_information: key._id }, {
      $set: {
        code: data
      }
    }).then(async (cc) => {

      if (cc == null || cc == undefined) {
        return res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: cc
        })
      }

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: cc
      })

      const userUpdateNew = await UserUpdateModel.create({
        type: 'Shop QrCode',
        old: { code: cc.code },
        new: { code: data },
        user_name: cc.shop_name,
        user_id: key._id
      })

    }).catch((err) => {
      console.log('Shop Qrcode Update Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    })

  })


}

export const changeShopQueueStatus = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)

  var key: any = req.user
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  ShopModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      queue: req.body.queue
    }
  }).then(async (cc) => {

    if (cc == null || cc == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
    const userUpdateNew = await UserUpdateModel.create({
      type: 'Shop Queue Status',
      old: { queue: cc.queue },
      new: { queue: req.body.queue },
      user_name: cc.shop_name,
      user_id: key._id
    })
  }).catch((err) => {
    console.log('Shop Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const changeShopOpenStatus = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)

  var key: any = req.user
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  ShopModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      open: req.body.open
    }
  }).then(async (cc) => {

    if (cc == null || cc == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
    const userUpdateNew = await UserUpdateModel.create({
      type: 'Shop Open Status',
      old: { open: cc.open },
      new: { open: req.body.open },
      user_name: cc.shop_name,
      user_id: key._id
    })
  }).catch((err) => {
    console.log('Shop Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const changeShopAppointmentStatus = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  var key: any = req.user
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  ShopModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      appointment: req.body.appointment
    }
  }).then(async (cc) => {

    if (cc == null || cc == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })

    const userUpdateNew = await UserUpdateModel.create({
      type: 'Shop Appointment Status',
      old: { appointment: cc.appointment },
      new: { appointment: req.body.appointment },
      user_name: cc.shop_name,
      user_id: key._id
    })
  }).catch((err) => {
    console.log('Shop Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

/// quuery api

export const SearchShopQuery = (req: Request, res: Response, next: NextFunction, verify: any) => {

  var startTime = performance.now()
  const query = [
    {
      $match: {
        $text: {
          $search: req.params.id
        },
        status: true,
        deleted: false
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'shopreview'
      }
    }, {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$shopreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalShop': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$shopreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$shopreview'
                  }
                ]
              }
            }
          }
        }
      }
    }
  ]
  ShopModel.aggregate(query).exec(async (err, item) => {

    if (err) {
      console.log('Shop Query Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'shops',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  })


}

export const SearchShopRegexQuery = (req: Request, res: Response, next: NextFunction, verify: any) => {

  var startTime = performance.now()
  const query = [
    {
      $match: {
        $or: [
          { "first_name": { $regex: req.params.id, $options: "i" } },
          { "last_name": { $regex: req.params.id, $options: "i" } },
          { "shop_name": { $regex: req.params.id, $options: "i" } },
          { "expertise": { $regex: req.params.id, $options: "i" } },
          { "company": { $regex: req.params.id, $options: "i" } },
          { "location": { $regex: req.params.id, $options: "i" } },
          { "description": { $regex: req.params.id, $options: "i" } },
          { "email": { $regex: req.params.id, $options: "i" } },
          { "banner": { $regex: req.params.id, $options: "i" } },
        ],
        status: true,
        deleted: false
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'shopreview'
      }
    }, {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$shopreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalShop': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$shopreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$shopreview'
                  }
                ]
              }
            }
          }
        }
      }
    }, {
      '$lookup': {
        'from': 'banners',
        'let': {
          'main': '$_id'
        },
        pipeline: [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$deleted', false
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'value'
      }
    }
  ]
  ShopModel.aggregate(query).exec(async (err, item) => {

    if (err) {
      console.log('Shop Query Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'shops',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  })


}

/// create Otp For Mobile Verify

export const CreateOtp = async (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  try {

    const otp = otpGenerate()
    //https://heytechemailsms.herokuapp.com
    //http://localhost:5003
    const msgR = await loginMsg({ service_id: { name: 'Shop' }, shop_name: 'Mobile Verify', otp: otp, bookingid: 1, queue: true })
    const smsTokens = jwt.sign({ date: new Date(), message: msgR, type: 'mobileVerify', user_id: [req.params.id], resend: req.body.resend }, MAIL_SECRET_KEY)
    const mainData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
      number: req.body.mobile,
      otp: otp
    }, {
      headers: {
        'auth-token': smsTokens
      }
    });

    console.log('Otp Sent', mainData)

    const userData = await UserModel.findOneAndUpdate({
      _id: req.params.id
    }, {
      $set: {
        mobile: parseInt(req.body.mobile),
        otp: otp,
        mobile_verify: false
      }
    })

    console.log('Otp Sent', userData)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        number: req.body.mobile,
        otp: otp
      }
    })

  } catch (err) {
    console.log('Create otp Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  }

}


export const verifyOtp = (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  UserModel.findByIdAndUpdate({ _id: req.params.id, otp: parseInt(req.body.otp) }, {
    $set: {
      mobile_verify: true
    }
  }).then((cc) => {
    if (cc == null || cc == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    ShopModel.findOneAndUpdate({ user_information: req.params.id }, {
      $set: {
        mobile: cc.mobile,
        mobile_verify: true
      }
    }).then((item) => {

      if (item == null || item == undefined) {
        return res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: item
        })
      }

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: item
      })

    }).catch((err) => {
      console.log('Shop Mobile Update Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    })

  }).catch((err) => {
    console.log('Shop Mobile Update Err', err)
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const UpdateServiceInShop = (req: Request, res: Response, next: NextFunction) => {

  if (req.params.id == null || req.params.id == undefined || req.params.id == '') {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('body', req.body)

  var data: any
  var key: any = req.user

  data = {
    topics: (typeof req.body.topics == "string") ? [req.body.topics] : req.body.topics,
  }

  ShopModel.findOneAndUpdate({ user_information: req.params.id }, {
    $set: {
      ...data
    }
  }).then(async (cc) => {


    if (cc == null || cc == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    const userUpdateNew = await UserUpdateModel.create({
      type: 'Shop Service',
      old: { topics: cc.topics },
      new: { ...data },
      user_name: cc.shop_name,
      user_id: cc.user_information
    })

    const token: any = jwt.sign({ _id: cc.user_information, email: cc.email }, SECRET_KEY)
    res.header("auth-token", token)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: '/shoplogin/' + token
    })

  }).catch((err) => {
    console.log('Shop Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const UpdateTimeslotInShop = (req: Request, res: Response, next: NextFunction) => {

  if (req.params.id == null || req.params.id == undefined || req.params.id == '') {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('body', req.body)

  var data: any
  var key: any = req.user

  data = {
    time_slot: JSON.parse(req.body.time_slot),
    time_zone: req.body.time_zone,
    time_val: req.body.time_val
  }

  ShopModel.findOneAndUpdate({ user_information: req.params.id }, {
    $set: {
      ...data
    }
  }).then(async (cc) => {


    if (cc == null || cc == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    const userUpdateNew = await UserUpdateModel.create({
      type: 'Shop TimeSlot',
      old: { time_slot: cc.time_slot },
      new: { ...data },
      user_name: cc.shop_name,
      user_id: cc.user_information
    })

    const token: any = jwt.sign({ _id: cc.user_information, email: cc.email }, SECRET_KEY)
    res.header("auth-token", token)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: '/shoplogin/' + token
    })

  }).catch((err) => {
    console.log('Shop Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const shopMobileLoginSignup = async (req: Request, res: Response, next: NextFunction, verify: any) => {

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('body', req.body)

  const mobileUser = await UserModel.find({ mobile: req.body.mobile, deleted: false }).populate('mentor_information')

  console.log('mobile User', mobileUser)


  if (mobileUser.length > 0) {

    try {

      const otp = otpGenerate()
      const newMsgT = await loginMsg({ service_id: { name: 'Login' }, shop_name: 'Shop', otp: otp, bookingid: 1, queue: true })
      const smsToken = jwt.sign({ date: new Date(), message: newMsgT, id: ServiceTemplateId.book, type: 'shopLogin', user_id: mobileUser.map((cc) => cc._id), resend: req.body.resend }, MAIL_SECRET_KEY)

      const mainData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
        number: parseInt(req.body.mobile),
        otp: otp
      }, {
        headers: {
          'auth-token': smsToken
        }
      });


      console.log('Otp Sent', mainData)

      const userData = await UserModel.updateMany({
        mobile: req.body.mobile, deleted: false
      }, {
        $set: {
          otp: otp,
        }
      })

      console.log('Otp Sent', userData)

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: {
          redirect: false,
          data: mobileUser.map((cc) => {
            return {
              email: cc.email,
              mobile: cc.mobile,
              _id: cc._id,
              shop_name: cc?.mentor_information?.shop_name
            }
          })
        }
      })



    } catch (err) {

      console.log('Create otp Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })

    }


  } else {

    try {


      const otp = otpGenerate()
      const newMsgR = await loginMsg({ service_id: { name: 'Login' }, shop_name: 'Shop', otp: otp, bookingid: 1, queue: true })
      const smsTokens = jwt.sign({ date: new Date(), message: newMsgR, id: ServiceTemplateId.book, type: 'shopLogin', user_id: [], resend: req.body.resend }, MAIL_SECRET_KEY)
      const mainData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
        number: parseInt(req.body.mobile),
        otp: otp
      }, {
        headers: {
          'auth-token': smsTokens
        }
      });

      console.log('Otp Sent', mainData)

      const UserData = new UserModel(
        {
          mobile: req.body.mobile,
          mobile_login: true,
          otp: otp
        }
      )

      UserData.save().then((main: any) => {
        console.log('inserted record', main);
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: {
            redirect: false,
            data: [{
              email: main.email,
              mobile: main.mobile,
              _id: main._id
            }]
          }
        })


      }).catch((err: Error) => {
        console.log('Error occurred while Creating User', err);
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } catch (err) {

      console.log('Create otp Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })

    }


  }



}

export const shopMobileLoginOtpVerify = (req: Request, res: Response, next: NextFunction, verify: any) => {


  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  UserModel.findOne({ mobile: req.body.mobile, _id: req.body.email, otp: parseInt(req.body.otp), deleted: false }).then(async (cc) => {
    if (cc == null || cc == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var link

    if (cc.signup_completed == true) {
      const token: any = jwt.sign({ _id: cc._id, email: cc.email }, SECRET_KEY)
      res.header("auth-token", token)
      link = '/shoplogin/' + token;
    } else {
      link = '/register/' + cc._id;
    }

    const logingS = await ShopModel.updateMany({ _id: cc._id }, {
      $set: {
        lastLogin: new Date()
      }
    })


    const shopsLogin = await LoginLogModel.create({
      user_id: cc._id,
      user_type: 'Shop',
      type: 'Login',
      token: link,
      message: 'Login',
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: link
    })

  }).catch((err) => {
    console.log('Shop Mobile Update Err', err)
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

// export const updateShopByAdmin = async (req: Request, res: Response, next: NextFunction) => {

//   const MainCheck = upload.single('image')

//   MainCheck(req, res, function (err) {
//     if (err instanceof multer.MulterError) {
//       //console.log('Multer Error', err)
//       if (err.code == 'LIMIT_FILE_SIZE') {
//         err.message = 'File Size is too large. Allowed file size is 5 MB';
//       }
//       return res.status(400).json({
//         status: 'Failed',
//         status_code: res.statusCode,
//         message: err.message,
//         data: err
//       })
//       // A Multer error occurred when uploading.
//     } else if (err) {
//       //console.log('Other Error', err)
//       return res.status(400).json({
//         status: 'Failed',
//         status_code: res.statusCode,
//         message: 'Request Failed',
//         data: err
//       })
//     }

//     console.log('image', req.file)
//     console.log('body', req.body)

//     if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
//       return res.status(400).json({
//         status: 'Failed',
//         status_code: res.statusCode,
//         message: "Request Failed",
//         data: 'err'
//       })
//     }


//     console.log('image', req.file)
//     console.log('body', req.body)

//     var data

//     if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

//       data = {
//         first_name: req.body.first_name,
//         last_name: req.body.last_name,
//         shop_name: req.body.shop_name,
//         image_link: req.body.oldimage,
//         email: req.body.email,
//         company: req.body.company,
//         description: req.body.description,
//         expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
//         language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
//         instagram: req.body.instagram,
//         time_slot: JSON.parse(req.body.time_slot),
//         waiting_number: req.body.waiting_number,
//         waiting_key: req.body.waiting_key,
//         waiting_time: req.body.waiting_time,
//         location: req.body.location,
//         lattitude: req.body.lattitude,
//         longitude: req.body.longitude,
//         seat: req.body.seat,
//         locate: {
//           type: "Point",
//           coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
//         }
//       }


//       ShopModel.findOneAndUpdate({ _id: req.params.id }, {
//         $set: {
//           ...data
//         }
//       }).then((cc) => {
//         res.status(200).json({
//           status: 'Success',
//           status_code: res.statusCode,
//           message: "Request Success",
//           data: cc
//         })
//       }).catch((err) => {
//         console.log('Shop Update Err', err)
//         res.status(400).json({
//           status: 'Failed',
//           status_code: res.statusCode,
//           message: "Request Failed",
//           data: err
//         })
//       })

//     } else {



//       ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
//         console.log('item', item)

//         if (item == null || item == undefined) {
//           return res.status(400).json({
//             status: 'Failed',
//             status_code: res.statusCode,
//             message: "Image is Required",
//             data: 'err'
//           })
//         }

//         // const ImageCreate = await GalleryModel.create({
//         //   image: item?.response.url,
//         //   user_id: key._id
//         // })
//         // console.log('Image Made', ImageCreate)

//         data = {
//           first_name: req.body.first_name,
//           last_name: req.body.last_name,
//           shop_name: req.body.shop_name,
//           image_link: item?.response.url,
//           email: req.body.email,
//           company: req.body.company,
//           description: req.body.description,
//           expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
//           language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
//           instagram: req.body.instagram,
//           time_slot: JSON.parse(req.body.time_slot),
//           waiting_number: req.body.waiting_number,
//           waiting_key: req.body.waiting_key,
//           waiting_time: req.body.waiting_time,
//           location: req.body.location,
//           lattitude: req.body.lattitude,
//           longitude: req.body.longitude,
//           seat: req.body.seat,
//           locate: {
//             type: "Point",
//             coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
//           }
//         }


//         ShopModel.findOneAndUpdate({ _id: req.params.id }, {
//           $set: {
//             ...data
//           }
//         }).then((cc) => {
//           res.status(200).json({
//             status: 'Success',
//             status_code: res.statusCode,
//             message: "Request Success",
//             data: cc
//           })
//           removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
//         }).catch((err) => {
//           console.log('Shop Update Err', err)
//           res.status(400).json({
//             status: 'Failed',
//             status_code: res.statusCode,
//             message: "Request Failed",
//             data: err
//           })
//         })


//       }).catch((err) => {
//         console.log('Shop User Update Err', err)
//         res.status(400).json({
//           status: 'Failed',
//           status_code: res.statusCode,
//           message: "Request Failed",
//           data: err
//         })
//       })
//     }


//   })

// }

export const updateShopByAdmin = async (req: Request, res: Response, next: NextFunction) => {
  const MainCheck = upload.fields([
    {
      name: 'image', maxCount: 1
    },
    {
      name: 'banner', maxCount: 1
    }
  ])

  MainCheck(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 5 MB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('image', req.file)
    console.log('body', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }


    console.log('image', req.files)
    console.log('image', req.file)
    console.log('body', req.body)

    var data: any
    var file: any = req.files

    if (file == null || file == undefined || Object.keys(file).length == 0) {

      data = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        shop_name: req.body.shop_name,
        image_link: req.body.oldimage,
        banner_link: req.body.bannerimage,
        email: req.body.email,
        company: req.body.company,
        description: req.body.description,
        expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
        language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
        instagram: req.body.instagram,
        time_slot: JSON.parse(req.body.time_slot),
        waiting_number: req.body.waiting_number,
        waiting_key: req.body.waiting_key,
        waiting_time: req.body.waiting_time,
        location: req.body.location,
        lattitude: req.body.lattitude,
        longitude: req.body.longitude,
        seat: req.body.seat,
        type: req.body.type,
        time_zone: req.body.time_zone,
        time_val: req.body.time_val,
        upi_id: req.body.upi_id,
        locate: {
          type: "Point",
          coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
        }
      }


      ShopModel.findOneAndUpdate({ _id: req.params.id }, {
        $set: {
          ...data
        }
      }).then(async (cc) => {

        if (cc == null || cc == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: cc
          })
        }

        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: cc
        })

      }).catch((err) => {
        console.log('Shop Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else if (file.banner == undefined && file.image.length > 0) {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.image[0].filename, file.image[0].filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        data = {
          first_name: req.body.first_name,
          last_name: req.body.last_name,
          shop_name: req.body.shop_name,
          image_link: item?.response.url,
          email: req.body.email,
          company: req.body.company,
          description: req.body.description,
          expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
          language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
          instagram: req.body.instagram,
          time_slot: JSON.parse(req.body.time_slot),
          waiting_number: req.body.waiting_number,
          waiting_key: req.body.waiting_key,
          waiting_time: req.body.waiting_time,
          location: req.body.location,
          lattitude: req.body.lattitude,
          longitude: req.body.longitude,
          seat: req.body.seat,
          type: req.body.type,
          time_zone: req.body.time_zone,
          time_val: req.body.time_val,
          upi_id: req.body.upi_id,
          locate: {
            type: "Point",
            coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
          }
        }

        ShopModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            ...data
          }
        }).then(async (cc) => {

          if (cc == null || cc == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: cc
            })
          }

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: cc
          })
          removeImage(process.cwd() + "/src/upload/" + file.image[0]?.filename)
        }).catch((err) => {
          console.log('Shop Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Shop User Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    } else if (file.image == undefined && file.banner.length > 0) {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.banner[0].filename, file.banner[0].filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        data = {
          first_name: req.body.first_name,
          last_name: req.body.last_name,
          shop_name: req.body.shop_name,
          banner_link: item?.response.url,
          email: req.body.email,
          company: req.body.company,
          description: req.body.description,
          expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
          language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
          instagram: req.body.instagram,
          time_slot: JSON.parse(req.body.time_slot),
          waiting_number: req.body.waiting_number,
          waiting_key: req.body.waiting_key,
          waiting_time: req.body.waiting_time,
          location: req.body.location,
          lattitude: req.body.lattitude,
          longitude: req.body.longitude,
          seat: req.body.seat,
          type: req.body.type,
          time_zone: req.body.time_zone,
          time_val: req.body.time_val,
          upi_id: req.body.upi_id,
          locate: {
            type: "Point",
            coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
          }
        }

        ShopModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            ...data
          }
        }).then(async (cc) => {

          if (cc == null || cc == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: cc
            })
          }

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: cc
          })
          removeImage(process.cwd() + "/src/upload/" + file.banner[0]?.filename)
        }).catch((err) => {
          console.log('Shop Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Shop User Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    } else if (file.image.length > 0 && file.banner.length > 0) {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.image[0].filename, file.image[0].filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.banner[0].filename, file.banner[0].filename).then(async (band) => {
          console.log('band', band)

          if (band == null || band == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Image is Required",
              data: 'err'
            })
          }


          data = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            shop_name: req.body.shop_name,
            image_link: item?.response.url,
            banner_link: band?.response.url,
            email: req.body.email,
            company: req.body.company,
            description: req.body.description,
            expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
            language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
            instagram: req.body.instagram,
            time_slot: JSON.parse(req.body.time_slot),
            waiting_number: req.body.waiting_number,
            waiting_key: req.body.waiting_key,
            waiting_time: req.body.waiting_time,
            location: req.body.location,
            lattitude: req.body.lattitude,
            longitude: req.body.longitude,
            seat: req.body.seat,
            type: req.body.type,
            time_zone: req.body.time_zone,
            time_val: req.body.time_val,
            upi_id: req.body.upi_id,
            locate: {
              type: "Point",
              coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.lattitude)]
            }
          }

          ShopModel.findOneAndUpdate({ _id: req.params.id }, {
            $set: {
              ...data
            }
          }).then(async (cc) => {

            if (cc == null || cc == undefined) {
              return res.status(400).json({
                status: 'Failed',
                status_code: res.statusCode,
                message: "Request Failed",
                data: cc
              })
            }

            res.status(200).json({
              status: 'Success',
              status_code: res.statusCode,
              message: "Request Success",
              data: cc
            })
            removeImage(process.cwd() + "/src/upload/" + file.image[0]?.filename)
            removeImage(process.cwd() + "/src/upload/" + file.banner[0]?.filename)
          }).catch((err) => {
            console.log('Shop Update Err', err)
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: err
            })
          })


        }).catch((err) => {
          console.log('Shop User Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Shop User Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    }


  })

}

export const createOrChangeQrCodeByAdmin = (req: Request, res: Response, next: NextFunction) => {

  var key: any = req.user
  console.log('user', key)
  const urlString = config.has('SERVER_URL') ? config.get('SERVER_URL') + '/api/open/' + req.params.id : SERVER_URL + '/api/open/' + req.params.id
  console.log('qrcode urle', urlString)

  QRCode.toDataURL(urlString, function (err, data) {

    if (err) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    console.log('Url Created After Qe', data)

    ShopModel.findOneAndUpdate({ _id: req.params.id }, {
      $set: {
        code: data
      }
    }).populate('topics').then((cc) => {

      if (cc == null || cc == undefined) {
        return res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: cc
        })
      }

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: cc
      })
    }).catch((err) => {
      console.log('Shop Qrcode Update Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    })

  })


}

export const changeShopStatusByAdmin = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  ShopModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      ...req.body
    }
  }).then((cc) => {

    if (cc == null || cc == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Shop Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const changeMultipleShopStatusByAdmin = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.id == null || req.body.id == undefined || req.body.id.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  ShopModel.updateMany({ _id: req.body.id.map((cc: any) => cc) }, {
    $set: {
      ...req.body.status
    }
  }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Shop Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const shopCsvImport = async (req: Request, res: Response, next: NextFunction) => {

  if (!req.body || req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.data == null || req.body.data == undefined || req.body.data.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: 'Data Not Found',
      data: 'Data Not Found'
    })
  }

  var shopArr: any = []


  req.body.data.map((cc: any) => {
    if (cc.id == "" || cc.id == null || cc.id == undefined) {
      shopArr.push({
        id: cc.id,
        data: {
          first_name: cc["first_name"],
          last_name: cc["last_name"],
          shop_name: cc["shop_name"],
          image_link: cc["image_link"],
          email: cc["email"],
          company: cc["company"],
          description: cc["description"],
          expertise: cc["expertise"],
          language: cc["language"],
          instagram: cc["instagram"],
          topics: cc["topics"],
          time_slot: cc["time_slot"],
          waiting_key: cc["waiting_key"],
          waiting_number: cc["waiting_number"],
          location: cc["location"],
          lattitude: cc["lattitude"],
          longitude: cc["longitude"],
          mobile: cc["mobile"],
          seat: cc["seat"],
          type: cc["type"],
          locate: {
            type: "Point",
            coordinates: [parseFloat(cc["longitude"]), parseFloat(cc["lattitude"])]
          }
        }
      })
    } else {
      shopArr.push({
        id: cc.id,
        data: {
          first_name: cc["first_name"],
          last_name: cc["last_name"],
          shop_name: cc["shop_name"],
          image_link: cc["image_link"],
          email: cc["email"],
          company: cc["company"],
          description: cc["description"],
          expertise: cc["expertise"],
          language: cc["language"],
          instagram: cc["instagram"],
          topics: cc["topics"],
          time_slot: cc["time_slot"],
          waiting_key: cc["waiting_key"],
          waiting_number: cc["waiting_number"],
          location: cc["location"],
          lattitude: cc["lattitude"],
          longitude: cc["longitude"],
          mobile: cc["mobile"],
          seat: cc["seat"],
          type: cc["type"],
          locate: {
            type: "Point",
            coordinates: [parseFloat(cc["longitude"]), parseFloat(cc["lattitude"])]
          },
        },
      })
    }
  })

  const session = await mongoose.startSession();

  try {

    const shopSessionResults = await session.withTransaction(async () => {

      await Promise.all(shopArr.map(async (cc: any) => {

        if (isValidObjectId(cc.id)) {

          const createS = await ShopModel.updateMany({ _id: cc.id }, { $set: cc.data }, { upsert: true, setDefaultsOnInsert: true, new: true }).session(session)

          console.log('Updated', createS)
          return createS
        } else {

          const userData = new UserModel({
            first_name: cc.data["first_name"],
            last_name: cc.data["last_name"],
            image_link: cc.data["image_link"],
            email: cc.data["email"],
            mobile: cc.data["mobile"],
            signup_completed: true
          })

          const shopData = new ShopModel(cc.data)
          shopData.user_information = userData._id
          userData.mentor_information = shopData._id

          await userData.save(session);
          console.log('User Created', userData)
          await shopData.save(session);
          console.log('Shop Created', shopData)
          return shopData
        }

      })).then((item) => {
        console.log('comp', item)
      }).catch((err) => {
        console.log('err', err)
        session.abortTransaction()
        return
      })

    })

    console.log('Session Values', shopSessionResults)

    if (shopSessionResults !== null) {
      console.log("The reservation was successfully created.");
      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Submitted",
        data: shopSessionResults
      })
    } else {
      console.log("The transaction was intentionally aborted.");
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: shopSessionResults
      })
    }

  } catch (err) {
    console.log("The transaction was aborted due to an unexpected error: " + err);
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  } finally {
    await session.endSession();
  }


}

export const getAllQueryActiveServices = (req: Request, res: Response, next: NextFunction, verify: any) => {

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        'status': true,
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    }, {
      '$unwind': {
        'path': '$topics'
      }
    }, {
      '$addFields': {
        'topics.user_name': '$shop_name',
        'topics.type': '$type',
      }
    },
    {
      $match: {
        $or: [
          { "topics.name": { $regex: req.params.id, $options: "i" } },
          { "topics.motivation": { $regex: req.params.id, $options: "i" } },
          { "topics.description": { $regex: req.params.id, $options: "i" } },
        ]
      }
    },
    {
      '$group': {
        '_id': null,
        'data': {
          '$push': '$topics'
        }
      }
    }
  ]
  ShopModel.aggregate(query).then(async (cc: any) => {
    //console.log('Data fetched', cc)

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'shops',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (cc.length > 0) ? cc[0].data : []
    })
  }).catch((err) => {
    console.log('Shop Active Service Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const SearchShopAdminRegexQuery = (req: Request, res: Response, next: NextFunction) => {

  var startTime = performance.now()
  const query = [
    {
      $match: {
        $or: [
          { "first_name": { $regex: req.params.id, $options: "i" } },
          { "last_name": { $regex: req.params.id, $options: "i" } },
          { "shop_name": { $regex: req.params.id, $options: "i" } },
          { "expertise": { $regex: req.params.id, $options: "i" } },
          { "company": { $regex: req.params.id, $options: "i" } },
          { "location": { $regex: req.params.id, $options: "i" } },
          { "description": { $regex: req.params.id, $options: "i" } },
          { "email": { $regex: req.params.id, $options: "i" } },
          { "banner": { $regex: req.params.id, $options: "i" } },
        ],
        status: true,
        deleted: false
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    },
    {
      '$lookup': {
        'from': 'employees',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$deleted', false
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'employee'
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },
    { '$unset': ["time_slot.monday._id", "time_slot.tuesday._id", "time_slot.wednesday._id", "time_slot.thursday._id", "time_slot.friday._id", "time_slot.saturday._id", "time_slot.sunday._id"] },
    {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$review',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'total': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$review'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$review'
                  }
                ]
              }
            }
          }
        }
      }
    }
  ]
  ShopModel.aggregate(query).exec(async (err, item) => {

    if (err) {
      console.log('Shop Query Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'shops',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  })


}

export const getAllBarberExpertise = (req: Request, res: Response, next: NextFunction, verify: any) => {

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        'status': true,
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'shopcategories',
        'localField': 'type',
        'foreignField': '_id',
        'as': 'type'
      }
    }, {
      '$unwind': {
        'path': '$type'
      }
    }, {
      '$match': {
        'type.name': 'Barber'
      }
    },
    {
      '$unwind': {
        'path': '$expertise'
      }
    },
    {
      '$group': {
        '_id': 'null',
        'data': {
          '$push': {
            'label': '$expertise',
            'value': '$expertise',
            'type': '$type._id',
            'shop': '$_id',
          }
        }
      }
    },
  ]
  ShopModel.aggregate(query).then(async (cc: any) => {

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'shops',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (cc.length > 0) ? cc[0].data : []
    })
  }).catch((err) => {
    console.log('Shop Expertise Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const shopRegistrationKYC = (req: Request, res: Response, next: NextFunction) => {
  const MainCheck = upload.fields([
    {
      name: 'image', maxCount: 1
    },
    {
      name: 'banner', maxCount: 1
    }
  ])

  MainCheck(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 5 MB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('image', req.file)
    console.log('body', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data: any
    var file: any = req.files

    if (file == null || file == undefined || Object.keys(file).length == 0) {

      data = {
        aadhar_link: req.body.aadharimage,
        gst_link: req.body.gstimage,
      }

      ShopModel.findOneAndUpdate({ user_information: req.params.id }, {
        $set: {
          ...data
        }
      }, {
        new: true,
      }).then((item: any) => {

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: item
          })
        }

        console.log('main Item', item)

        const token: any = jwt.sign({ _id: item.user_information, email: item.email }, SECRET_KEY)
        res.header("auth-token", token)

        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: '/shoplogin/' + token
        })
      }).catch((err: Error) => {
        console.log('Shop Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else if (file.image.length == 0 && file.banner.length > 0) {

      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.banner[0].filename, file.banner[0].filename).then((band) => {
        console.log('band', band)

        if (band == null || band == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        data = {
          gst_link: band?.response.url,
        }

        ShopModel.findOneAndUpdate({ user_information: req.params.id }, {
          $set: {
            ...data
          }
        }).then(async (cc: any) => {

          if (cc == null || cc == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: cc
            })
          }

          const token: any = jwt.sign({ _id: cc.user_information, email: cc.email }, SECRET_KEY)
          res.header("auth-token", token)

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: '/shoplogin/' + token
          })

          removeImage(process.cwd() + "/src/upload/" + file.banner[0]?.filename)
        }).catch((err: Error) => {
          console.log('Shop Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Shop User Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    } else if (file.banner.length == 0 && file.image.length > 0) {

      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.image[0].filename, file.image[0].filename).then((item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }
        data = {
          aadhar_link: item?.response.url,
        }

        ShopModel.findOneAndUpdate({ user_information: req.params.id }, {
          $set: {
            ...data
          }
        }).then(async (cc: any) => {

          if (cc == null || cc == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: cc
            })
          }

          const token: any = jwt.sign({ _id: cc.user_information, email: cc.email }, SECRET_KEY)
          res.header("auth-token", token)

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: '/shoplogin/' + token
          })

          removeImage(process.cwd() + "/src/upload/" + file.image[0]?.filename)
        }).catch((err: Error) => {
          console.log('Shop Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })
      }).catch((err) => {
        console.log('Shop User Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    } else if (file.banner.length > 0 && file.image.length > 0) {


      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.image[0].filename, file.image[0].filename).then((item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.banner[0].filename, file.banner[0].filename).then((band) => {
          console.log('band', band)

          if (band == null || band == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Image is Required",
              data: 'err'
            })
          }

          data = {
            aadhar_link: item?.response.url,
            gst_link: band?.response.url,
          }

          ShopModel.findOneAndUpdate({ user_information: req.params.id }, {
            $set: {
              ...data
            }
          }, {
            new: true,
          }).then((item: any) => {

            if (item == null || item == undefined) {
              return res.status(400).json({
                status: 'Failed',
                status_code: res.statusCode,
                message: "Request Failed",
                data: item
              })
            }

            const token: any = jwt.sign({ _id: item.user_information, email: item.email }, SECRET_KEY)
            res.header("auth-token", token)

            res.status(200).json({
              status: 'Success',
              status_code: res.statusCode,
              message: "Request Success",
              data: '/shoplogin/' + token
            })
            removeImage(process.cwd() + "/src/upload/" + file.image[0]?.filename)
            removeImage(process.cwd() + "/src/upload/" + file.banner[0]?.filename)
          }).catch((err: Error) => {
            console.log('Shop Create Err', err)
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: err
            })
          })


        }).catch((err) => {
          console.log('Shop User Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Shop User Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    }
  })
}

export const shopLogout = (req: Request, res: Response, next: NextFunction) => {

  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  ShopModel.findOneAndUpdate({ user_information: val._id },
    {
      $set: {
        lastLogout: new Date()
      }
    }).then(async (cc) => {

      const shopLogout = await LoginLogModel.create({
        user_id: val._id,
        user_type: 'Shop',
        type: 'Logout',
        message: `Logout Url ${req.url}`,
      })

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: cc
      })
    }).catch((err) => {
      console.log('Shop Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    })

}


export const shopProfileKYC = (req: Request, res: Response, next: NextFunction) => {
  const MainCheck = upload.fields([
    {
      name: 'image', maxCount: 1
    },
    {
      name: 'banner', maxCount: 1
    }
  ])

  MainCheck(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 5 MB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('image', req.file)
    console.log('body', req.body)
    var val: any = req.user
    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data: any
    var file: any = req.files

    if (file == null || file == undefined || Object.keys(file).length == 0) {

      data = {
        aadhar_link: req.body.aadharimage,
        gst_link: req.body.gstimage,
      }

      ShopModel.findOneAndUpdate({ user_information: val._id }, {
        $set: {
          ...data
        }
      }, {
        new: true,
      }).then(async (item: any) => {

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: item
          })
        }

        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: item
        })


        const userUpdateNesw = await UserUpdateModel.create({
          type: 'Shop Update',
          old: item,
          new: data,
          user_name: item.shop_name,
          user_id: val._id
        })

      }).catch((err: Error) => {
        console.log('Shop Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else if (file.image.length == 0 && file.banner.length > 0) {

      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.banner[0].filename, file.banner[0].filename).then((band) => {
        console.log('band', band)

        if (band == null || band == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        data = {
          gst_link: band?.response.url,
        }

        ShopModel.findOneAndUpdate({ user_information: val._id }, {
          $set: {
            ...data
          }
        }).then(async (item: any) => {

          if (item == null || item == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: item
            })
          }

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: item
          })


          const userUpdateNesw = await UserUpdateModel.create({
            type: 'Shop Update',
            old: item,
            new: data,
            user_name: item.shop_name,
            user_id: val._id
          })

          removeImage(process.cwd() + "/src/upload/" + file.banner[0]?.filename)
        }).catch((err: Error) => {
          console.log('Shop Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Shop User Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    } else if (file.banner.length == 0 && file.image.length > 0) {

      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.image[0].filename, file.image[0].filename).then((item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }
        data = {
          aadhar_link: item?.response.url,
        }

        ShopModel.findOneAndUpdate({ user_information: val._id }, {
          $set: {
            ...data
          }
        }).then(async (item: any) => {

          if (item == null || item == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: item
            })
          }

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: item
          })


          const userUpdateNesw = await UserUpdateModel.create({
            type: 'Shop Update',
            old: item,
            new: data,
            user_name: item.shop_name,
            user_id: val._id
          })

          removeImage(process.cwd() + "/src/upload/" + file.image[0]?.filename)
        }).catch((err: Error) => {
          console.log('Shop Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })
      }).catch((err) => {
        console.log('Shop User Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    } else if (file.banner.length > 0 && file.image.length > 0) {


      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.image[0].filename, file.image[0].filename).then((item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + file.banner[0].filename, file.banner[0].filename).then((band) => {
          console.log('band', band)

          if (band == null || band == undefined) {
            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Image is Required",
              data: 'err'
            })
          }

          data = {
            aadhar_link: item?.response.url,
            gst_link: band?.response.url,
          }

          ShopModel.findOneAndUpdate({ user_information: val._id }, {
            $set: {
              ...data
            }
          }).then(async (item: any) => {

            if (item == null || item == undefined) {
              return res.status(400).json({
                status: 'Failed',
                status_code: res.statusCode,
                message: "Request Failed",
                data: item
              })
            }

            res.status(200).json({
              status: 'Success',
              status_code: res.statusCode,
              message: "Request Success",
              data: item
            })


            const userUpdateNesw = await UserUpdateModel.create({
              type: 'Shop Update',
              old: item,
              new: data,
              user_name: item.shop_name,
              user_id: val._id
            })

            removeImage(process.cwd() + "/src/upload/" + file.image[0]?.filename)
            removeImage(process.cwd() + "/src/upload/" + file.banner[0]?.filename)
          }).catch((err: Error) => {
            console.log('Shop Create Err', err)
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: err
            })
          })


        }).catch((err) => {
          console.log('Shop User Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Shop User Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    }
  })
}

export const getAllShopListS = (req: Request, res: Response, next: NextFunction) => {
  ShopModel.find({ deleted: false, status: true }).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? { shop: item } : { shop: [] }
    })
  }).catch((err: Error) => {
    console.log('Shop User Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}


export const UpdateShopUpiId = (req: Request, res: Response, next: NextFunction) => {

  if (req.params.id == null || req.params.id == undefined || req.params.id == '') {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('body', req.body)

  var data: any

  data = {
    upi_id: req.body.upi_id
  }

  ShopModel.findOneAndUpdate({ user_information: req.params.id }, {
    $set: {
      ...data
    }
  }).then(async (cc) => {

    if (cc == null || cc == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    const userUpdateNew = await UserUpdateModel.create({
      type: 'Shop TimeSlot',
      old: { upi_id: cc.upi_id },
      new: { ...data },
      user_name: cc.shop_name,
      user_id: cc.user_information
    })

    const token: any = jwt.sign({ _id: cc.user_information, email: cc.email }, SECRET_KEY)
    res.header("auth-token", token)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: '/shoplogin/' + token
    })

  }).catch((err) => {
    console.log('Shop Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}
