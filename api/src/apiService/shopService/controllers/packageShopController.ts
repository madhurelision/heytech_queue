import { SsrPaymentModel } from "../../../Models/SsrPage";
import { NextFunction, Request, Response } from 'express';
import mongoose from 'mongoose'
import { performance } from 'perf_hooks'
import { LogQueryModel } from '../../../Models/LogData';


export const getShopPackagesPayment = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        'user_id': new mongoose.Types.ObjectId(key._id)
      }
    }, {
      '$lookup': {
        'from': 'ssrcatalogs',
        'localField': 'product_id',
        'foreignField': '_id',
        'as': 'product_id'
      }
    }, {
      '$unwind': {
        'path': '$product_id'
      }
    },
    {
      '$lookup': {
        'from': 'ssrorders',
        'localField': 'order_id',
        'foreignField': '_id',
        'as': 'order_id'
      }
    }, {
      '$unwind': {
        'path': '$order_id'
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'payment': [
          {
            '$sort': {
              '_id': -1
            }
          },
          {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'payment': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]
  SsrPaymentModel.aggregate(query).then(async (item) => {


    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'ssrpayments',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item[0] : { payment: [], total: 0 }
    })
  }).catch((err) => {
    console.log('ShopPayment Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}