import { Router } from 'express';

import { createShopUser, getShopUserProfile, SingleUser, updateShopUser, createOrChangeQrCode, changeShopQueueStatus, changeShopAppointmentStatus, CreateOtp, verifyOtp, UpdateServiceInShop, changeShopOpenStatus, UpdateTimeslotInShop, shopLogout, shopRegistrationKYC, shopProfileKYC } from './controllers/shopController';

import { ShopVerify } from '../../config/verifyToken';

import { changeServiceAcceptStatus, cloneAllService, cloneService, CreateService, deleteService, getShopService, getSingleService, SearchServiceShopIdRegexQuery, updateService } from './controllers/serviceController';

import { getShopUserGallery, createGallery, EditGallery, deleteGallery } from '../adminService/controllers/galleryController';

import { addWaitingTime, getShopBookings, getShopUserCurrentAppointment, shopBookingCancel, shopBookingQuery, shopUserQueueList, addQueueWaitingTime, getSingleQuery, assignEmployeeIdBooking, addRemark, changeSeatShop, completeAllBookings } from '../bookingService/controllers/bookingController';


import { makeReview } from '../adminService/controllers/reviewController';

import { changeShopViewStatus, getShopNotification } from '../adminService/controllers/notificationController';
import { changeEmployeeStatus, CreateEmployee, deleteEmployee, EditEmployee, getShopEmployee } from './controllers/shopEmployeeController';
import { cloneAllServiceWithUpdate } from './controllers/cloneController';
import { CreateShopPackage, deleteShopPackage, getShopShopPackage, updateShopPackage } from './controllers/shopPackageController';
import { getShopPayment } from './controllers/shopPaymentController';
import { CreateShopFeature, deleteShopFeature, EditShopFeature, getShopFeature } from './controllers/shopFeatureController';
import { getShopPackagesPayment } from './controllers/packageShopController';
import {getShopWiseDateBooking, getShopWiseTotalBooking} from './controllers/shopCountController';

const router = Router();

// shop
router.post('/shop/createShop/:id', createShopUser)
router.get('/shop/getUser/:id', SingleUser)
router.get('/shop/profile', ShopVerify, getShopUserProfile)
router.post('/shop/updateShop', ShopVerify, updateShopUser)
router.post('/shop/updateQueueStatus/:id', ShopVerify, changeShopQueueStatus)
router.post('/shop/updateAppointmentStatus/:id', ShopVerify, changeShopAppointmentStatus)
router.post('/shop/updateOpenStatus/:id', ShopVerify, changeShopOpenStatus)
router.post('/shop/updateServiceShop/:id', UpdateServiceInShop)
router.post('/shop/updateTimeslotShop/:id', UpdateTimeslotInShop)
router.post('/shop/shopKyc/:id', shopRegistrationKYC)
router.post('/shop/shopProfileKyc', ShopVerify, shopProfileKYC)

// service
router.post('/shop/createService', ShopVerify, CreateService)
router.post('/shop/updateService/:id', ShopVerify, updateService)
router.get('/shop/getService/:skip/:limit', ShopVerify, getShopService)
router.post('/shop/updateServiceStatus/:id', ShopVerify, changeServiceAcceptStatus)
router.get('/shop/cloneService/:id', ShopVerify, cloneService)
router.post('/shop/cloneAllService', ShopVerify, cloneAllService)
router.get('/shop/deleteService/:id', ShopVerify, deleteService)
router.post('/shop/cloneAllServiceWithUpdate', ShopVerify, cloneAllServiceWithUpdate)
router.get('/shop/getSingleService/:id', ShopVerify, getSingleService)

// gallery
router.get('/shop/getGallery', ShopVerify, getShopUserGallery)
router.post('/shop/createGallery', ShopVerify, createGallery)
router.post('/shop/editGallery/:id', ShopVerify, EditGallery)
router.get('/shop/deleteGallery/:id', ShopVerify, deleteGallery)

/// Booking
router.post('/shop/addWaitingTime/:id', ShopVerify, (req, res, next) => { addWaitingTime(req, res, next, false) })
router.post('/shop/addQueueWaitingTime/:id', ShopVerify, (req, res, next) => { addQueueWaitingTime(req, res, next, false) })
router.get('/shop/getAllBooking', ShopVerify, (req, res, next) => { getShopBookings(req, res, next, false) })
router.get('/shop/getCurrentBooking/:skip/:limit', ShopVerify, (req, res, next) => { getShopUserCurrentAppointment(req, res, next, false) })
router.get('/shop/getQueueBooking/:skip/:limit', ShopVerify, (req, res, next) => { shopUserQueueList(req, res, next, false) })
router.post('/shop/getQueryBooking/:skip/:limit', ShopVerify, (req, res, next) => { shopBookingQuery(req, res, next, false) })
router.get('/shop/getBookQuery/:id', ShopVerify, (req, res, next) => { getSingleQuery(req, res, next, false) })
router.post('/shop/assignEmployee/:id', ShopVerify, (req, res, next) => { assignEmployeeIdBooking(req, res, next, false) })
//compl
router.post('/shop/completeBooking', ShopVerify, completeAllBookings)

router.post('/shop/addRemark/:id', ShopVerify, (req, res, next) => { addRemark(req, res, next, false) })
router.post('/shop/addbookingSeat/:id', ShopVerify, (req, res, next) => { changeSeatShop(req, res, next, false) })

// Shop Employee
router.post('/shop/createEmployee', ShopVerify, (req, res, next) => { CreateEmployee(req, res, next, false) });
router.post('/shop/editEmployee/:id', ShopVerify, (req, res, next) => { EditEmployee(req, res, next, false) });
router.get('/shop/getEmployee', ShopVerify, (req, res, next) => { getShopEmployee(req, res, next, false) });
router.post('/shop/updateEmployeeStatus/:id', ShopVerify, (req, res, next) => { changeEmployeeStatus(req, res, next, false) });
router.get('/shop/deleteEmployee/:id', ShopVerify, (req, res, next) => { deleteEmployee(req, res, next, false) });


// create or update QRCode
router.get('/shop/Qrcode', ShopVerify, createOrChangeQrCode)

router.get('/shop/cancelBooking/:id', ShopVerify, shopBookingCancel)

// review
router.post('/shop/makeReview', ShopVerify, (req, res, next) => { makeReview(req, res, next, false) })

router.get('/shop/notification/:skip/:limit', ShopVerify, getShopNotification)
router.get('/shop/view', ShopVerify, changeShopViewStatus)

// createOtp
router.post('/shop/createShopOtp/:id', CreateOtp)
router.post('/shop/verifyShopOtp/:id', verifyOtp)

router.get('/shop/getServiceShopRegexQuery/:id', ShopVerify, SearchServiceShopIdRegexQuery)


router.get('/shop/logout', ShopVerify, shopLogout)

// shop Package
router.post('/shop/createShopPackage', ShopVerify, CreateShopPackage)
router.post('/shop/updateShopPackage/:id', ShopVerify, updateShopPackage)
router.get('/shop/getShopPackage/:skip/:limit', ShopVerify, getShopShopPackage)
router.get('/shop/deleteShopPackage/:id', ShopVerify, deleteShopPackage)

//shop Payment
router.get('/shop/getShopPayment/:skip/:limit', ShopVerify, getShopPayment)

// shop ssr Package Payment
router.get('/shop/getPackageShop/:skip/:limit', ShopVerify, getShopPackagesPayment)

// shop Package Feature
router.post('/shop/createShopFeature', ShopVerify, CreateShopFeature)
router.post('/shop/updateShopFeature/:id', ShopVerify, EditShopFeature)
router.get('/shop/getShopFeature/:skip/:limit', ShopVerify, getShopFeature)
router.get('/shop/deleteShopFeature/:id', ShopVerify, deleteShopFeature)

//shop COunt
router.post('/shop/getDailyCount', ShopVerify, getShopWiseDateBooking)
router.get('/shop/getTotalCount', ShopVerify, getShopWiseTotalBooking)

export default router;
