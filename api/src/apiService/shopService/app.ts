import express, { Express } from 'express';
import routes from './routes';

const shopApp = (app: Express) => {

  app.use('/api', routes)

};

export default shopApp;
