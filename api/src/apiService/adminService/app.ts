import express, { Express } from 'express';
import routes from './routes';

const adminApp = (app: Express) => {

  app.use('/admin', routes)

};

export default adminApp;
