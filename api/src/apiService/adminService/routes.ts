import { Router } from 'express';
import { AdminVerify } from '../../config/verifyToken';
import { AdminLogin, getAdminProfile, CustomerListAdmin, getAdminCurrentBookings, getAllAdminService, getAllGalleryList, getAllShopList, GetDayWiseCount, dateWiseShopList, getSingleShop, getSingleCustomer, getDateWiseBookingData, getAllAdminBookingList, getAllAdminEmployee, getAllBookingIpDetail, readLogFile, deleteLogFile, SearchAdminBookingQuery, getAllLogList, getSingleBookingIpDetail, getAllLocationLogList, getSingleBooking, getAllUserShop, getAllAdminQueueBookingList, getAllBookingLogs, getExternalLink, deleteBookingIpDetail, getAllBookingTest, shopRedirect, getAllAdminUserService } from './controllers/adminController';
import { changeMultipleCustomerStatusByAdmin, customerCsvImport, SerachAdminCustomerQuery, updateCustomer, updateStatusCustomer } from '../customerService/controllers/customerController';
import { changeMultipleDescriptionTagStatusByAdmin, CreateDescriptionTag, deleteDescriptionTag, EditDescriptionTag, getAllDescriptionTagList, updateStatusDescriptionTag } from './controllers/descriptionTagController';
import { createGallery, deleteGallery, EditGallery } from './controllers/galleryController';
import { changeMultipleShopStatusByAdmin, changeShopStatusByAdmin, createOrChangeQrCodeByAdmin, SearchShopAdminRegexQuery, SearchShopRegexQuery, shopCsvImport, updateShopByAdmin } from '../shopService/controllers/shopController';
import { changeMultipleReviewTagStatusByAdmin, CreateReviewTag, deleteReviewTag, EditReviewTag, getAllReviewTagList, updateStatusReviewTag } from './controllers/reviewtagController';
import { changeMultipleServiceStatusByAdmin, changeServiceAcceptStatus, CreateService, deleteService, SearchAdminServiceRegexQuery, SearchServiceRegexQuery, serviceCsvImport, updateAdminService, updateService } from '../shopService/controllers/serviceController';
import { CreateServiceTag, deleteServiceTag, EditServiceTag, getAllServiceTagList, CreateManyServiceTag, updateStatusServiceTag, changeMultipleServiceTagStatusByAdmin } from './controllers/serviceTagController';
import { changeEmployeeStatus, changeMultipleEmployeeStatusByAdmin, deleteEmployee, EditEmployee } from '../shopService/controllers/shopEmployeeController';
import { changeMultipleTagStatusByAdmin, CreateTag, deleteTag, EditTag, getAllTagList, updateStatusTag } from './controllers/tagController';
import { deletePushNotification, getAllPushNotificationList, sendNotication } from '../../modules/pushNotification';
import { getAllTemplateList, CreateTemplate, updateTemplate, deleteTemplate } from './controllers/templateController'
import { changeMultipleTagServiceStatusByAdmin, CreateTagService, deleteTagService, EditTagService, getAllTagServiceList, updateStatusTagService } from './controllers/tagServiceController';
import { deleteUserUpdate, getAllUserUpdateList } from './controllers/userUpdateController';
import { getAllEmployeeSearchQuery } from '../employeeService/controllers/employeeController';
import { changeMultipleOfferStatusByAdmin, CreateOffer, deleteOffer, EditOffer, getAllOfferList, updateStatusOffer } from './controllers/offerController';
import { changeMultipleBannerStatusByAdmin, CreateBanner, deleteBanner, EditBanner, getAllBannerList, updateStatusBanner } from './controllers/bannerController';
import { deleteMailService, getAllMailService } from './controllers/MailServiceController';
import { changeMultipleShopCategoryStatusByAdmin, createShopCategory, deleteShopCategory, editShopCategory, getAllShopCategoryList, updateStatusShopCategory } from './controllers/shopCategoryController';
import { getAllSocketList, getAllSocketStatList } from './controllers/socketController';
import { createAdminPage, updateAdminPage } from './controllers/adminPageController';
import { createContactPage, updateContactPage } from './controllers/contactPageController';
import { SequeneceId, testDateConversion, TestShopData, testApiIntegrationFirst, testApiIntegrationSecond } from './controllers/testController';
import { CreateShopShortId, CreateShortIdLink, deleteShortId, EditCustomShortId, getAllCustomShortIdDetail, getAllShopShortIdDetail, updateShortId, getAllBookingShortIdDetail } from './controllers/ShortUrlController';
import { getAllReviewList, getShopReview, getSingleReview } from './controllers/reviewController';
import { deleteContact, getAllContactList } from './controllers/contactController';
import { createAboutPage, updateAboutPage } from './controllers/aboutPageController';
import { changeSeatAdmin } from '../bookingService/controllers/bookingController';
import { CreateAdminFeature, deleteAdminFeature, EditAdminFeature, getAllAdminFeatureList } from './controllers/adminFeatureController';
import { changeAdminSubStatus, CreateAdminSub, deleteAdminSub, EditAdminSub, getAllAdminSubList } from './controllers/adminSubController';
import { getAllLoginLogList } from './controllers/loginLogController';
import { CreateAdminUser, deleteAdminUser, EditAdminUser, getAllAdminUserList, updateStatusAdminUser } from './controllers/adminUserController';
import { changeMultipleServiceNameStatusByAdmin, CreateServiceName, deleteServiceName, EditServiceName, getAllServiceNameList, updateStatusServiceName } from './controllers/serviceNameController';
import { changeMultipleExpertiseNameStatusByAdmin, CreateExpertiseName, deleteExpertiseName, EditExpertiseName, getAllExpertiseNameList, updateStatusExpertiseName } from './controllers/expertiseNameController';
import { changeMultipleShopPackageStatusByAdmin, CreateShopPackage, deleteShopPackage, EditShopPackage, getAllShopPackageList, updateStatusShopPackage } from './controllers/shopPackageController';
import { getAllOrderList, getAllPaymentList } from './controllers/orderController';
import { CreateShopFeature, deleteShopFeature, EditShopFeature, getAllShopPackageFeatureList } from './controllers/shopPackageFeatureController';
import { CreateHost, deleteHost, EditHost, getAllHostList } from './controllers/hostController';

const router = Router();

router.post('/login', AdminLogin)
router.get('/profile', AdminVerify, getAdminProfile)

//customer
router.get('/customerList/:skip/:limit', AdminVerify, CustomerListAdmin)
router.get('/singleCustomer/:id', AdminVerify, getSingleCustomer)
router.post('/editCustomer/:id', AdminVerify, updateCustomer)
router.post('/updateCustomerStatus/:id', AdminVerify, updateStatusCustomer)
router.post('/changeMultiCustomerStatus', AdminVerify, changeMultipleCustomerStatusByAdmin)

//booking current
router.get('/currentBookingList/:skip/:limit', AdminVerify, getAdminCurrentBookings)
router.get('/bookingList/:skip/:limit', AdminVerify, getAllAdminBookingList)
router.get('/singleBooking/:id', AdminVerify, getSingleBooking)
router.post('/changeBookingSeat/:id', AdminVerify, (req, res, next) => { changeSeatAdmin(req, res, next, false) })

//service
router.get('/serviceList/:skip/:limit', AdminVerify, getAllAdminService)
router.get('/serviceUserList/:id/:skip/:limit', AdminVerify, getAllAdminUserService)
router.post('/createService', AdminVerify, CreateService)
router.post('/updateService/:id', AdminVerify, updateAdminService)
router.post('/updateServiceStatus/:id', AdminVerify, changeServiceAcceptStatus)
router.get('/deleteService/:id', AdminVerify, deleteService);
router.post('/changeMultiServiceStatus', AdminVerify, changeMultipleServiceStatusByAdmin)

//gallery
router.get('/galleryList/:skip/:limit', AdminVerify, getAllGalleryList)
router.post('/createGallery', AdminVerify, createGallery)
router.post('/editGallery/:id', AdminVerify, EditGallery)
router.get('/deleteGallery/:id', AdminVerify, deleteGallery)

//shop
router.get('/shopList/:skip/:limit', AdminVerify, getAllShopList)
router.get('/singleShop/:id', AdminVerify, getSingleShop)
router.post('/editShop/:id', AdminVerify, updateShopByAdmin)
router.get('/Qrcode/:id', AdminVerify, createOrChangeQrCodeByAdmin)
router.post('/changeShopStatus/:id', AdminVerify, changeShopStatusByAdmin)
router.post('/changeMultiShopStatus', AdminVerify, changeMultipleShopStatusByAdmin)

//shop Profile Redirect
router.get('/shopRedirect/:id', shopRedirect)

//count Boooking
//router.post('/queryBookingList', AdminVerify, GetDayWiseCount)
router.post('/queryBookingList', AdminVerify, getDateWiseBookingData)

// booking Tag
router.get('/tagList/:skip/:limit', AdminVerify, getAllTagList)
router.post('/createTag', AdminVerify, CreateTag)
router.post('/editTag/:id', AdminVerify, EditTag)
router.get('/deleteTag/:id', AdminVerify, deleteTag)
router.post('/updateTagStatus/:id', AdminVerify, updateStatusTag)
router.post('/changeMultiTagStatus', AdminVerify, changeMultipleTagStatusByAdmin)

// review Tag
router.get('/reviewTagList/:skip/:limit', AdminVerify, getAllReviewTagList)
router.post('/createReviewTag', AdminVerify, CreateReviewTag)
router.post('/editReviewTag/:id', AdminVerify, EditReviewTag)
router.get('/deleteReviewTag/:id', AdminVerify, deleteReviewTag)
router.post('/updateReviewTagStatus/:id', AdminVerify, updateStatusReviewTag)
router.post('/changeMultiReviewTagStatus', AdminVerify, changeMultipleReviewTagStatusByAdmin)

// service tag
router.get('/serviceTagList/:skip/:limit', AdminVerify, getAllServiceTagList)
router.post('/createServiceTag', AdminVerify, CreateServiceTag)
router.post('/editServiceTag/:id', AdminVerify, EditServiceTag)
router.get('/deleteServiceTag/:id', AdminVerify, deleteServiceTag)
router.post('/updateServiceTagStatus/:id', AdminVerify, updateStatusServiceTag)
router.post('/changeMultiServiceTagStatus', AdminVerify, changeMultipleServiceTagStatusByAdmin)

// description tag
router.get('/descriptionTagList/:skip/:limit', AdminVerify, getAllDescriptionTagList)
router.post('/createDescriptionTag', AdminVerify, CreateDescriptionTag)
router.post('/editDescriptionTag/:id', AdminVerify, EditDescriptionTag)
router.get('/deleteDescriptionTag/:id', AdminVerify, deleteDescriptionTag)
router.post('/updateDescriptionTagStatus/:id', AdminVerify, updateStatusDescriptionTag)
router.post('/changeMultiDescriptionTagStatus', AdminVerify, changeMultipleDescriptionTagStatusByAdmin)

////csv Import
router.post('/importServiceCsv', AdminVerify, serviceCsvImport)
router.post('/importCustomerCsv', AdminVerify, customerCsvImport)
router.post('/importShopCsv', AdminVerify, shopCsvImport)

//employee
router.get('/employeeList/:skip/:limit', AdminVerify, getAllAdminEmployee)
router.post('/editEmployee/:id', AdminVerify, (req, res, next) => { EditEmployee(req, res, next, false) });
router.post('/updateEmployeeStatus/:id', AdminVerify, (req, res, next) => { changeEmployeeStatus(req, res, next, false) });
router.get('/deleteEmployee/:id', AdminVerify, (req, res, next) => { deleteEmployee(req, res, next, false) });
router.post('/changeMultiEmployeeStatus', AdminVerify, changeMultipleEmployeeStatusByAdmin)

// ip Details
router.get('/bookingIpList/:skip/:limit', AdminVerify, getAllBookingIpDetail)
router.get('/deleteBookingIpDetail/:id', AdminVerify, deleteBookingIpDetail)

// short Id Click
router.get('/bookingShortIdList/:skip/:limit', AdminVerify, getAllBookingShortIdDetail)

//log
router.get('/logList', AdminVerify, readLogFile)
router.get('/clearlogList', AdminVerify, deleteLogFile)

//push
router.get('/pushNotificationList/:skip/:limit', AdminVerify, getAllPushNotificationList)
router.post('/sendPushNotification', AdminVerify, sendNotication)
router.get('/deletePushNotification/:id', AdminVerify, deletePushNotification);

//template
router.get('/templateList/:skip/:limit', AdminVerify, getAllTemplateList)
router.post('/createTemplate', AdminVerify, CreateTemplate)
router.post('/updateTemplate/:id', AdminVerify, updateTemplate)
router.get('/deleteTemplate/:id', AdminVerify, deleteTemplate);

// tag Service
router.get('/tagServiceList/:skip/:limit', AdminVerify, getAllTagServiceList)
router.post('/createTagService', AdminVerify, CreateTagService)
router.post('/editTagService/:id', AdminVerify, EditTagService)
router.get('/deleteTagService/:id', AdminVerify, deleteTagService)
router.post('/updateTagServiceStatus/:id', AdminVerify, updateStatusTagService)
router.post('/changeMultiTagServiceStatus', AdminVerify, changeMultipleTagServiceStatusByAdmin)

// user Updates
router.get('/userUpdateList/:skip/:limit', AdminVerify, getAllUserUpdateList)
router.get('/deleteUserUpdate/:id', AdminVerify, deleteUserUpdate)

//querySearch
router.get('/getServiceQuery/:id', AdminVerify, SearchAdminServiceRegexQuery)
router.get('/getShopQuery/:id', AdminVerify, SearchShopAdminRegexQuery)
router.get('/getEmployeeQuery/:id', AdminVerify, getAllEmployeeSearchQuery)
router.get('/getCustomerQuery/:id', AdminVerify, SerachAdminCustomerQuery)
router.get('/getBookingQuery/:id', AdminVerify, SearchAdminBookingQuery)

// offer
router.get('/offerList/:skip/:limit', AdminVerify, getAllOfferList)
router.post('/createOffer', AdminVerify, CreateOffer)
router.post('/editOffer/:id', AdminVerify, EditOffer)
router.get('/deleteOffer/:id', AdminVerify, deleteOffer)
router.post('/updateOfferStatus/:id', AdminVerify, updateStatusOffer)
router.post('/changeMultiOfferStatus', AdminVerify, changeMultipleOfferStatusByAdmin)

// banner
router.get('/bannerList/:skip/:limit', AdminVerify, getAllBannerList)
router.post('/createBanner', AdminVerify, CreateBanner)
router.post('/editBanner/:id', AdminVerify, EditBanner)
router.get('/deleteBanner/:id', AdminVerify, deleteBanner)
router.post('/updateBannerStatus/:id', AdminVerify, updateStatusBanner)
router.post('/changeMultiBannerStatus', AdminVerify, changeMultipleBannerStatusByAdmin)

/// service request
router.get('/serviceRequestList/:skip/:limit', AdminVerify, getAllMailService)
router.get('/deleteMailService/:id', AdminVerify, deleteMailService)

//// log data
router.get('/getlogList/:skip/:limit', AdminVerify, getAllLogList)
router.get('/getBookDetail/:id', AdminVerify, getSingleBookingIpDetail)

// location Log
router.get('/locationLogList/:skip/:limit', AdminVerify, getAllLocationLogList)


// shop Category
router.get('/shopCategoryList/:skip/:limit', AdminVerify, getAllShopCategoryList)
router.post('/createShopCategory', AdminVerify, createShopCategory)
router.post('/editShopCategory/:id', AdminVerify, editShopCategory)
router.get('/deleteShopCategory/:id', AdminVerify, deleteShopCategory)
router.post('/updateShopCategoryStatus/:id', AdminVerify, updateStatusShopCategory)
router.post('/changeMultiShopCategoryStatus', AdminVerify, changeMultipleShopCategoryStatusByAdmin)

// shop User
router.get('/userShopList/:skip/:limit', AdminVerify, getAllUserShop)

//socket history
router.get('/socketList/:skip/:limit', AdminVerify, getAllSocketList)
router.get('/socketStatList/:skip/:limit', AdminVerify, getAllSocketStatList)

// admin Page
router.get('/createAdminPage', AdminVerify, createAdminPage)
router.post('/editAdminPage/:id', AdminVerify, updateAdminPage)

// queue
router.get('/getQueueBooking/:skip/:limit', AdminVerify, getAllAdminQueueBookingList)

// contact Page
router.get('/createContactPage', AdminVerify, createContactPage)
router.post('/editContactPage/:id', AdminVerify, updateContactPage)

//booking logs
router.get('/getBookingLogs/:skip/:limit', AdminVerify, getAllBookingLogs)

//shop short
router.post('/createShopShortId', AdminVerify, CreateShopShortId)
router.get('/getAllShopShortId/:skip/:limit', AdminVerify, getAllShopShortIdDetail)

//custom SHort
router.post('/createCustomShortId', AdminVerify, CreateShortIdLink)
router.post('/editCustomShortId/:id', AdminVerify, EditCustomShortId)
router.get('/getAllCustomShortId/:skip/:limit', AdminVerify, getAllCustomShortIdDetail)
router.get('/updateShortId/:id', AdminVerify, updateShortId)
router.get('/deleteShortId/:id', AdminVerify, deleteShortId)

//review
router.get('/singleReview/:id', AdminVerify, getSingleReview)

//get External Link
router.get('/getExternal', AdminVerify, getExternalLink)

//get contact Data
router.get('/getAllContactList/:skip/:limit', AdminVerify, getAllContactList)
router.get('/deleteContact/:id', AdminVerify, deleteContact)

//get review Data
router.get('/getAllReviewList/:skip/:limit', AdminVerify, getAllReviewList)

//get shop review Data
router.get('/getAllShopReviewList/:skip/:limit', AdminVerify, getShopReview)

//get test booking
router.get('/getAllTestBooking/:skip/:limit', AdminVerify, getAllBookingTest)

router.get('/check', (req, res, next) => { SequeneceId(req, res, next, true) })

router.get('/getData', TestShopData)

// about Page
router.get('/createAboutPage', AdminVerify, createAboutPage)
router.post('/editAboutPage/:id', AdminVerify, updateAboutPage)

// admin Feature
router.get('/adminFeatureList/:skip/:limit', AdminVerify, getAllAdminFeatureList)
router.post('/createAdminFeature', AdminVerify, CreateAdminFeature)
router.post('/editAdminFeature/:id', AdminVerify, EditAdminFeature)
router.get('/deleteAdminFeature/:id', AdminVerify, deleteAdminFeature)

// admin
router.get('/adminSubList/:skip/:limit', AdminVerify, getAllAdminSubList)
router.post('/createAdminSub', AdminVerify, CreateAdminSub)
router.post('/editAdminSub/:id', AdminVerify, EditAdminSub)
router.get('/deleteAdminSub/:id', AdminVerify, deleteAdminSub)
router.post('/updateStatusAdminSub/:id', AdminVerify, changeAdminSubStatus)

router.get('/checkData', testDateConversion)
//loginLogs
router.get('/loginLogList/:skip/:limit', AdminVerify, getAllLoginLogList)

//admin Notify
router.get('/adminManageList/:skip/:limit', AdminVerify, getAllAdminUserList)
router.post('/createAdminManage', AdminVerify, CreateAdminUser)
router.post('/editAdminManage/:id', AdminVerify, EditAdminUser)
router.get('/deleteAdminManage/:id', AdminVerify, deleteAdminUser)
router.post('/updateAdminManageStatus/:id', AdminVerify, updateStatusAdminUser)

// service Name
router.get('/serviceNameList/:skip/:limit', AdminVerify, getAllServiceNameList)
router.post('/createServiceName', AdminVerify, CreateServiceName)
router.post('/editServiceName/:id', AdminVerify, EditServiceName)
router.get('/deleteServiceName/:id', AdminVerify, deleteServiceName)
router.post('/updateServiceNameStatus/:id', AdminVerify, updateStatusServiceName)
router.post('/changeMultiServiceNameStatus', AdminVerify, changeMultipleServiceNameStatusByAdmin)

// expertise Name
router.get('/expertiseNameList/:skip/:limit', AdminVerify, getAllExpertiseNameList)
router.post('/createExpertiseName', AdminVerify, CreateExpertiseName)
router.post('/editExpertiseName/:id', AdminVerify, EditExpertiseName)
router.get('/deleteExpertiseName/:id', AdminVerify, deleteExpertiseName)
router.post('/updateExpertiseNameStatus/:id', AdminVerify, updateStatusExpertiseName)
router.post('/changeMultiExpertiseNameStatus', AdminVerify, changeMultipleExpertiseNameStatusByAdmin)

router.get('/test1', testApiIntegrationFirst)
router.get('/test2', testApiIntegrationSecond)

// shopPackage
router.get('/shopPackageList/:skip/:limit', AdminVerify, getAllShopPackageList)
router.post('/createShopPackage', AdminVerify, CreateShopPackage)
router.post('/editShopPackage/:id', AdminVerify, EditShopPackage)
router.get('/deleteShopPackage/:id', AdminVerify, deleteShopPackage)
router.post('/updateShopPackageStatus/:id', AdminVerify, updateStatusShopPackage)
router.post('/changeMultiShopPackageStatus', AdminVerify, changeMultipleShopPackageStatusByAdmin)

//shopPackageFeature
router.get('/shopPackageFeatureList/:skip/:limit', AdminVerify, getAllShopPackageFeatureList)
router.post('/createShopPackageFeature', AdminVerify, CreateShopFeature)
router.post('/editShopPackageFeature/:id', AdminVerify, EditShopFeature)
router.get('/deleteShopPackageFeature/:id', AdminVerify, deleteShopFeature)

// shopPayment
router.get('/shopOrderList/:skip/:limit', AdminVerify, getAllOrderList)
router.get('/shopPaymentList/:skip/:limit', AdminVerify, getAllPaymentList)

// host
router.get('/hostList/:skip/:limit', AdminVerify, getAllHostList)
router.post('/createHost', AdminVerify, CreateHost)
router.post('/editHost/:id', AdminVerify, EditHost)
router.get('/deleteHost/:id', AdminVerify, deleteHost)

export default router;
