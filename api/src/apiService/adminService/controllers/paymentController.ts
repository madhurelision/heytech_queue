import { Request, Response, NextFunction } from 'express';
import Stripe from 'stripe';
import mongoose from 'mongoose';
import moment from 'moment';
const stripe = new Stripe('sk_test_51IbOuBSALt0jE426tx23jXCpOaMPEtNdniNykZk8WKoCXguWL78hK4J7kmNYMk6dWrOZNiPz0eJMyXvvsvXa48O000YTcpEUt2', {
  apiVersion: '2020-08-27',
})
import { ShopPackageModel, ShopOrderModel, ShopPaymentModel } from '../../../Models/ShopPackage';

export const StripePayment = (req: Request, res: Response, next: NextFunction) => {

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.pay_id == null || req.body.pay_id == undefined || req.body.pay_id == "" || req.body.product_id == null || req.body.product_id == null) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'No Data Found'
    })
  }

  var key: any = req.user

  const promise = new Promise((resolve, reject) => {
    ShopPackageModel.find({ _id: req.body.product_id }).exec((err, item) => {
      if (err) {
        return reject(err)
      }

      if (item.length == 0) {
        return reject({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: 'No Data Found'
        })
      } else {
        resolve(item)
      }

    })
  })

  const Promise1 = new Promise((resolve, reject) => {
    ShopPaymentModel.find({ user_id: key._id, status: true }).sort({ _id: -1 }).limit(1).then((test) => {
      resolve(test)
    }).catch((err) => {
      reject(err)
    })
  })

  Promise.all([promise, Promise1]).then(async (values: any) => {

    console.log('Product Sum', values[0][0])
    var TC = parseInt(values[0][0].price)
    console.log('All ValData', values)

    if (TC == null || TC == undefined || TC == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "The Product is Free so payment is not Required",
        data: 'No Data Found'
      })
    }

    const productOrder = await ShopOrderModel.create({
      ...req.body,
      Amount: TC,
      shop_id: values[0][0].user_id,
      user_id: key._id
    })

    console.log('Product Order Done', productOrder)

    const session = await mongoose.startSession();

    try {

      const transactionResults = await session.withTransaction(async () => {
        console.log('Amoutn', TC)

        const stripeTransaction = await stripe.charges.create({
          amount: TC * 100,
          currency: "INR",
          description: 'Thank You For Shopping',
          source: req.body.pay_id,
          capture: false,
          metadata: {
            product: JSON.stringify({
              _id: values[0][0]._id,
              name: values[0][0].name,
              price: values[0][0].price,
              image: values[0][0].image
            }),
            order: productOrder._id
          }
        })

        console.log('Stripe Transaction', stripeTransaction)

        const stripeConfirm = await stripe.charges.capture(
          stripeTransaction.id.toString()
        );

        console.log('stripe Confirm', stripeConfirm)


        const successData = {
          payment_data: { ...stripeConfirm },
          email: req.body.email,
          name: req.body.name,
          mobile: req.body.mobile,
          pay_id: req.body.pay_id,
          product_id: req.body.product_id,
          order_id: productOrder._id,
          Amount: TC,
          payment_id: stripeConfirm.payment_intent,
          payment_method_id: stripeConfirm.payment_method,
          charge_id: stripeConfirm.id,
          payment_url: stripeConfirm.receipt_url,
          refunds_url: stripeConfirm.refunds.url,
          payment_method: 'Stripe',
          payment_status: stripeConfirm.status,
          shop_id: values[0][0].user_id,
          user_id: key._id,
          startDate: values[1].length == 0 ? new Date() : values[1][0].endDate,
          endDate: values[1].length == 0 ? moment().add(1, (values[0][0].view == 'Month' || values[0][0].view == 'Monthly') ? 'M' : (values[0][0].view == 'Year' || values[0][0].view == 'Yearly') ? 'y' : 'M').toDate() : moment(values[1][0].endDate).add(1, (values[0][0].view == 'Month' || values[0][0].view == 'Monthly') ? 'M' : (values[0][0].view == 'Year' || values[0][0].view == 'Yearly') ? 'y' : 'M').toDate()
        }

        const MainTr = await ShopPaymentModel.create([successData], { session })

        console.log('Product Transaction Succes', MainTr)

        const updateOrders = await ShopOrderModel.updateOne({ _id: productOrder._id }, {
          $set: {
            status: true
          }
        }, { session })

        console.log('Order Status Updates', updateOrders)

      })


      if (transactionResults !== null) {
        console.log("The reservation was successfully created.");
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Submitted",
          data: transactionResults
        })
      } else {
        console.log("The transaction was intentionally aborted.");
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: transactionResults
        })
      }

    } catch (err) {
      console.log("The transaction was aborted due to an unexpected error: " + err);
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    } finally {
      await session.endSession();
    }

  }).catch((err) => {
    console.log('Primuse', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}