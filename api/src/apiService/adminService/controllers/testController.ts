import { Request, Response, NextFunction } from 'express'
import { isValidObjectId } from 'mongoose'
import jwt from 'jsonwebtoken'
import config from 'config'
import moment from 'moment'
import { CLIENT_URL, generateHash, SECRET_KEY, radomThreeeDigit } from '../../../config/keys'
import { BookingModel, TestingModel } from '../../../Models/Booking'
import { AdminModel } from '../../../Models/Admin'
import { GalleryModel } from '../../../Models/Gallery'
import { ServiceModel, TestingServiceModel } from '../../../Models/Service'
import { ReviewModel } from '../../../Models/review'
import { NotificationModel } from '../../../Models/Notification'
import { removeImage } from '../../../ImageService/main'
import { ShopModel, UserModel } from '../../../Models/User'
import { ServiceRequestModel } from '../../../Models/ServiceRequest'
import { ShortUrlModel, ShortUrlStatModal } from '../../../Models/ShortUrl'
//import { CounterModel } from '../../../Models/Counter'
import { Main } from '../../../config/location';
import axios from 'axios';
import Shop from '../../../ServiceClass/Shop/ShopService'

export const OpenUrl = (req: Request, res: Response, next: NextFunction, verify: any) => {

  const val: any = req.user

  console.log('req params id', req.params.id, 'req.params.token', req.params.token, 'value', val)

  if (req.params.id == null || req.params.id == undefined || req.params.id == "" || val._id == null || val._id == undefined || val._id == "" || val.email == null || val.email == undefined || val.email == "") {
    return res.redirect(config.has('CLIENT_URL') ? config.get('CLIENT_URL') : CLIENT_URL)
  }
  const token: any = jwt.sign({ _id: val._id, email: val.email }, SECRET_KEY)

  isValidObjectId(req.params.id) ? res.redirect(config.has('CLIENT_URL') ? config.get('CLIENT_URL') : CLIENT_URL + '/mobile/' + req.params.id + '/' + token) : res.redirect(config.has('CLIENT_URL') ? config.get('CLIENT_URL') : CLIENT_URL)
}

export const OpenUrlWithoutToken = (req: Request, res: Response, next: NextFunction, verify: any) => {

  console.log('req params id', req.params.id, 'req.params.token', req.params.token)

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.redirect(config.has('CLIENT_URL') ? config.get('CLIENT_URL') : CLIENT_URL)
  }

  isValidObjectId(req.params.id) ? res.redirect(config.has('CLIENT_URL') ? config.get('CLIENT_URL') + '/user/' + req.params.id : CLIENT_URL + '/user/' + req.params.id) : res.redirect(config.has('CLIENT_URL') ? config.get('CLIENT_URL') : CLIENT_URL)
}

export const UpdateAllQueuedata = async (req: Request, res: Response, next: NextFunction) => {

  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  const mainData = await TestingModel.findOne({ _id: req.params.id, shop_id: val._id })
  console.log('check Update', mainData)

  TestingModel.updateMany({
    shop_id: val._id,
    queue: true,
    appointment_date: { $gte: new Date(mainData.appointment_date) },
    _id: { $nin: [req.params.id] }
  }, [{
    $set: {
      approximate_date: {
        $add: ['$approximate_date', 30 * 60000]
      },
      waiting_date: {
        $add: ['$waiting_date', 30 * 60000]
      },
      end_time: {
        $add: ['$waiting_date', 30 * 60000]
      },
    }
  }]).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err: Error) => {
    console.log('Queue Waiting Time Add Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })


}

export const SequeneceId = (req: Request, res: Response, next: NextFunction, verify: any) => {
  ServiceModel.updateMany({}, {
    $set: {
      price: 100
    }
  }).then((cc: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err: Error) => {
    console.log('Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const TestShopData = async (req: Request, res: Response, next: NextFunction) => {

  try {

    const MainC = await new Shop('61cee5e85fccf60f4504cb60').getAvailibility()

    console.log('check', MainC)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      value: MainC
    })


  } catch (err) {

    console.log('Class SHop Add Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  }

}

export const CreateAdmin = (req: Request, res: Response, next: NextFunction) => {

  const Admin = new AdminModel({
    email: 'info.heytechitservices@gmail.com',
    password: generateHash(123456)
  })

  Admin.save().then((cc: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err: Error) => {
    console.log('Admin Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const getUserLocation = (req: Request, res: Response, next: NextFunction) => {

  var axios = require('axios');

  var config = {
    method: 'get',
    url: 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=40.6655101%2C-73.89188969999998&destinations=40.659569%2C-73.933783%7C40.729029%2C-73.851524%7C40.6860072%2C-73.6334271%7C40.598566%2C-73.7527626&key=AIzaSyCqiCKV0cm-EpHp2hev4XkD5wG9JGxFbVs',
    headers: {}
  };

  axios(config)
    .then((cc: any) => {
      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: cc.data
      })
    }).catch((err: Error) => {
      console.log('Location Find Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })

    })

}

export const testDateConversion = (req: Request, res: Response, next: NextFunction) => {

  console.log(config.get('PROD'), moment('2022-07-07T07:53:56.000+00:00').calendar(), moment('2022-07-07T07:53:56.000+00:00').utcOffset(330).calendar())

  res.status(200).json({
    status: true,
    prod: config.get('PROD'),
    offset: moment('2022-07-07T07:53:56.000+00:00').calendar(),
    woffset: moment('2022-07-07T07:53:56.000+00:00').utcOffset(330).calendar()
  })

}

export const testApiIntegrationFirst = (req: Request, res: Response, next: NextFunction) => {
  var db = req.app.get('client').db('heytechQueue');


  db.collection('services').aggregate([
    {
      '$match': {
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        }
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'service': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': 0
          }, {
            '$limit': 10
          }
        ]
      }
    }, {
      '$project': {
        'service': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).toArray((err: Error, item: any) => {
    if (err) {
      console.log('Service Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        service: [],
        total: 0
      }
    })
  })


}

export const testApiIntegrationSecond = (req: Request, res: Response, next: NextFunction) => {


  ServiceModel.aggregate([
    {
      '$match': {
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        }
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'service': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': 0
          }, {
            '$limit': 10
          }
        ]
      }
    }, {
      '$project': {
        'service': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        service: [],
        total: 0
      }
    })
  }).catch((err) => {
    console.log('Service Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

export const getApiExecute = (req: Request, res: Response, next: NextFunction) => {
  const { exec } = require("child_process");
  exec("git log -1 --format=%cd", (error: Error, stdout: any, stderr: any) => {
    if (error) {
      console.log(`error: ${error.message}`);
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: error
      })
    }
    if (stderr) {
      console.log(`stderr: ${stderr}`);
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: stderr
      })
    }
    console.log(`stdout: ${stdout}`);
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: stdout
    })
  });
}


const createUpdatedDate = (date: any, time: any, key: any) => {
  console.log('Updates Date', date, 'time', time, 'key', key, 'match', key == 'min')
  var now = new Date(date);
  console.log('check', now)
  if (key == 'min') {
    now.setMinutes(now.getMinutes() + time);
  } else {
    now.setMinutes(now.getMinutes() + time * 60)
  }
  now.setMilliseconds(0);
  return now
}

const minusDate = (start: Date, end: Date) => {
  console.log('mains', start, end)
  var now = new Date(start).getTime();
  var later = new Date(end).getTime();
  var result: any = (now - later) / 60000;
  return (result > 0) ? result : 0
}