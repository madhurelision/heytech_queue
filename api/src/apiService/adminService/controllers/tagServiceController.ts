import { NextFunction, Request, Response } from 'express';
import { isValidObjectId } from 'mongoose';
import { TagServiceModel } from '../../../Models/tagService';

export const CreateTagService = (req: Request, res: Response, next: NextFunction) => {
  console.log('data', req.body)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }
  var data = req.body
  data.admin_id = val._id

  const TagMode = new TagServiceModel(data)

  TagMode.save().then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err: Error) => {
    console.log('Tag Service Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const getAllTagService = (req: Request, res: Response, next: NextFunction, verify: any) => {

  TagServiceModel.find({ deleted: false }).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { tag: item }
    })
  }).catch((err: Error) => {
    console.log('Tag Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const getAllTagServiceList = (req: Request, res: Response, next: NextFunction) => {

  TagServiceModel.aggregate([
    {
      $match: {
        deleted: false
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'tag': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'tag': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        tag: [],
        total: 0
      }
    })
  }).catch((err: Error) => {
    console.log('Tag Service Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const EditTagService = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  var data

  data = {
    name: req.body.name,
    tag: req.body.tag,
  }


  TagServiceModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      ...data
    }
  }, { new: true }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Tag Service Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const deleteTagService = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  console.log('body', req.params.id)

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  TagServiceModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      deleted: true
    }
  }, { new: true }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Tag Service Delete Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}


export const updateStatusTagService = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  console.log('body', req.params.id)
  console.log('body', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  TagServiceModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      status: req.body.status
    }
  }, { new: true }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Tag Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}


export const changeMultipleTagServiceStatusByAdmin = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.id == null || req.body.id == undefined || req.body.id.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  TagServiceModel.updateMany({ _id: req.body.id.map((cc: any) => cc) }, {
    $set: {
      ...req.body.status
    }
  }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Tag Service Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}