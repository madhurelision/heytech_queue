import { Request, Response, NextFunction } from 'express'
import { BookingModel } from '../../../Models/Booking';
import { ReviewModel } from "../../../Models/review";
import mongoose from 'mongoose'
import { ShopModel } from '../../../Models/User';

export const makeReview = (req: Request, res: Response, next: NextFunction, verify: any) => {
  console.log('Req.body', req.body, req.baseUrl, req.url)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields Are Required",
      data: 'err'
    })
  }
  var check: any = {}
  var url: any = req.url

  if (isNaN(url.match('/shop/makeReview'))) {
    check.shop_review = true
  } else {
    check.user_review = true
  }

  const reviewData = new ReviewModel({ ...req.body, ...check })

  reviewData.save().then((item: any) => {

    BookingModel.findOneAndUpdate({ _id: req.body.booking_id }, {
      $set: {
        ...check
      }
    }, { new: true }).then((cc: any) => {
      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Submitted",
        data: cc
      })
    }).catch((err: Error) => {
      console.log('Review Booking Update Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    })

  }).catch((err: Error) => {
    console.log('Review Insert Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const getSingleReview = (req: Request, res: Response, next: NextFunction) => {

  ReviewModel.aggregate([
    {
      '$match': {
        'booking_id': new mongoose.Types.ObjectId(req.params.id)
      }
    },
    {
      '$lookup': {
        'from': 'bookings',
        'localField': 'booking_id',
        'foreignField': '_id',
        'as': 'booking_id'
      }
    },
    {
      '$unwind': {
        'path': '$booking_id'
      }
    },
    {
      '$lookup': {
        'from': 'customers',
        'localField': 'booking_id.user_id',
        'foreignField': '_id',
        'as': 'booking_id.user_id'
      }
    }, {
      '$unwind': {
        'path': '$booking_id.user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'booking_id.service_id',
        'foreignField': '_id',
        'as': 'booking_id.service_id'
      }
    }, {
      '$unwind': {
        'path': '$booking_id.service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'booking_id.shop_id',
        'foreignField': 'user_information',
        'as': 'booking_id.shop_id'
      }
    }, {
      '$unwind': {
        'path': '$booking_id.shop_id'
      }
    }
  ]).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        detail: cc
      }
    })
  }).catch((err) => {
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

export const getAllReviewList = (req: Request, res: Response, next: NextFunction) => {

  ReviewModel.aggregate([
    {
      '$lookup': {
        'from': 'bookings',
        'localField': 'booking_id',
        'foreignField': '_id',
        'as': 'booking_id'
      }
    }, {
      '$unwind': {
        'path': '$booking_id'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'booking_id.user_id',
        'foreignField': '_id',
        'as': 'booking_id.user_id'
      }
    }, {
      '$unwind': {
        'path': '$booking_id.user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'booking_id.service_id',
        'foreignField': '_id',
        'as': 'booking_id.service_id'
      }
    }, {
      '$unwind': {
        'path': '$booking_id.service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'booking_id.shop_id',
        'foreignField': 'user_information',
        'as': 'booking_id.shop_id'
      }
    }, {
      '$unwind': {
        'path': '$booking_id.shop_id'
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'review': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'review': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        review: [],
        total: 0
      }
    })
  }).catch((err) => {
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const getShopReview = (req: Request, res: Response, next: NextFunction) => {

  ShopModel.aggregate([
    {
      '$lookup': {
        'from': 'bookings',
        'localField': 'user_information',
        'foreignField': 'shop_id',
        'as': 'bookings'
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'review': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'review': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        review: [],
        total: 0
      }
    })
  }).catch((err) => {
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}