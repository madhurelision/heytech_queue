import { NextFunction, Request, Response } from 'express';
import { SocketModel, SocketStatModel } from '../../../Models/SocketSchema';

export const getAllSocketList = (req: Request, res: Response, next: NextFunction) => {

  SocketModel.aggregate([
    {
      '$match': {
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'employee'
      }
    },
    {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': {
                  '$cond': {
                    'if': {
                      '$eq': [
                        {
                          '$size': '$customer'
                        }, 1
                      ]
                    },
                    'then': '$customer.first_name',
                    'else': {
                      '$cond': {
                        'if': {
                          '$eq': [
                            {
                              '$size': '$employee'
                            }, 1
                          ]
                        },
                        'then': '$employee.name',
                        'else': '$admin.name'
                      }
                    }
                  }
                }
              }
            }, 0
          ]
        }
      }
    }, {
      '$lookup': {
        'from': 'socketstats',
        'localField': 'user_id',
        'foreignField': 'user_id',
        'as': 'stat'
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'detail': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'detail': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        detail: [],
        total: 0
      }
    })
  }).catch((err: Error) => {
    console.log('Socket Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const getAllSocketStatList = (req: Request, res: Response, next: NextFunction) => {

  SocketStatModel.aggregate([
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'employee'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': {
                  '$cond': {
                    'if': {
                      '$eq': [
                        {
                          '$size': '$customer'
                        }, 1
                      ]
                    },
                    'then': '$customer.first_name',
                    'else': {
                      '$cond': {
                        'if': {
                          '$eq': [
                            {
                              '$size': '$employee'
                            }, 1
                          ]
                        },
                        'then': '$employee.name',
                        'else': '$admin.name'
                      }
                    }
                  }
                }
              }
            }, 0
          ]
        }
      }
    }, {
      '$group': {
        '_id': '$key',
        'id': {
          '$first': '$_id'
        },
        'route': {
          '$first': '$route'
        },
        'key': {
          '$first': '$key'
        },
        'type': {
          '$first': '$type'
        },
        'user_name': {
          '$first': '$user_name'
        },
        'createdAt': {
          '$first': '$createdAt'
        },
        'data': {
          '$push': {
            'route': '$route',
            'type': '$type',
            'user_id': '$user_id',
            'user_name': '$user_name',
            'createdAt': '$createdAt'
          }
        }
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'detail': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'detail': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        detail: [],
        total: 0
      }
    })
  }).catch((err: Error) => {
    console.log('Socket Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}