import { Request, Response, NextFunction } from 'express';
import mongoose from 'mongoose'
import { NotificationModel } from '../../../Models/Notification';

export const getCustomerNotification = (req: Request, res: Response, next: NextFunction, verify: any) => {
  var key: any = req.user
  if (key == null || key == undefined || Object.keys(key).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }


  const promise1 = new Promise((resolve, reject) => {
    NotificationModel.countDocuments({
      user_id: new mongoose.Types.ObjectId(key._id),
      view: false
    }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise2 = new Promise((resolve, reject) => {
    NotificationModel.aggregate([
      {
        $match: {
          user_id: new mongoose.Types.ObjectId(key._id)
        }
      }, {
        '$facet': {
          'metadata': [
            {
              '$group': {
                '_id': null,
                'total': {
                  '$sum': 1
                }
              }
            }
          ],
          'notification': [
            {
              '$sort': {
                '_id': -1
              }
            }, {
              '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
            }, {
              '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
            }
          ]
        }
      }, {
        '$project': {
          'notification': 1,
          'total': {
            '$arrayElemAt': [
              '$metadata.total', 0
            ]
          }
        }
      }
    ]).exec((err, item) => {

      if (err) {
        reject(err)
        return
      }
      resolve((item.length > 0) ? item[0] : {
        notification: [],
        total: 0
      })
    })
  })

  Promise.all([promise1, promise2]).then((values: any) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        customerNotificationCount: values[0],
        customerNotificationTotal: values[1].total,
        customerNotification: values[1].notification
      }
    })

  }).catch((err) => {
    console.log('Notification Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const changeViewStatus = (req: Request, res: Response, next: NextFunction, verify: any) => {
  var key: any = req.user
  if (key == null || key == undefined || Object.keys(key).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  NotificationModel.updateMany({ user_id: key._id, view: false }, {
    $set: {
      view: true
    }
  }).then((cc: any) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err: Error) => {
    console.log('Notification Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}


export const getShopNotification = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  if (key == null || key == undefined || Object.keys(key).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  const promise1 = new Promise((resolve, reject) => {
    NotificationModel.countDocuments({
      shop_id: new mongoose.Types.ObjectId(key._id),
      shop_view: false
    }).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const promise2 = new Promise((resolve, reject) => {
    NotificationModel.aggregate([
      {
        $match: {
          shop_id: new mongoose.Types.ObjectId(key._id)
        }
      }, {
        '$facet': {
          'metadata': [
            {
              '$group': {
                '_id': null,
                'total': {
                  '$sum': 1
                }
              }
            }
          ],
          'notification': [
            {
              '$sort': {
                '_id': -1
              }
            }, {
              '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
            }, {
              '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
            }
          ]
        }
      }, {
        '$project': {
          'notification': 1,
          'total': {
            '$arrayElemAt': [
              '$metadata.total', 0
            ]
          }
        }
      }
    ]).exec((err, item) => {
      if (err) {
        reject(err)
        return
      }
      resolve((item.length > 0) ? item[0] : {
        notification: [],
        total: 0
      })
    })
  })

  Promise.all([promise1, promise2]).then((values: any) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        shopNotificationCount: values[0],
        shopNotificationTotal: values[1].total,
        shopNotification: values[1].notification
      }
    })

  }).catch((err) => {
    console.log('Notification Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const changeShopViewStatus = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  if (key == null || key == undefined || Object.keys(key).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  NotificationModel.updateMany({ shop_id: key._id, shop_view: false }, {
    $set: {
      shop_view: true
    }
  }).then((cc: any) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err: Error) => {
    console.log('Notification Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
} 