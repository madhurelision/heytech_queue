import { Request, Response, NextFunction } from 'express'
import { isValidObjectId } from 'mongoose';
import { ContactPageModel } from '../../../Models/Contact';

export const createContactPage = (req: Request, res: Response, next: NextFunction) => {

  const val: any = req.user

  ContactPageModel.findOne({ deleted: false }).then((cc: any) => {

    if (cc == null || cc == undefined) {

      ContactPageModel.create({
        title: '',
        title_small: '',
        description: '',
        mobile: null,
        email: '',
        admin_id: val._id
      }).then((item) => {


        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: {
            detail: item
          }
        })

      }).catch((err) => {

        console.log('admin Page Err', err)

        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })

      })

    } else {

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: {
          detail: cc
        }
      })

    }

  }).catch((err) => {

    console.log('admin Page Err', err)

    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const updateContactPage = (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.body)


  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  var data: any = req.body

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  ContactPageModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      ...data
    }
  }, { new: true }).then(async (cc) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })

  }).catch((err) => {
    console.log('Contact Page Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const getAllContactPage = (req: Request, res: Response, next: NextFunction, verify: any) => {

  ContactPageModel.findOne({ deleted: false }).then((item: any) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        contact: item
      }
    })

  }).catch((err: Error) => {
    console.log('Contact Page Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}