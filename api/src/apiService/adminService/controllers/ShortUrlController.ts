import { NextFunction, Request, Response } from 'express';
import { ShortUrlModel } from '../../../Models/ShortUrl'
import config from 'config'
import { SHORT_URL_KEY } from '../../../config/keys'
import { nanoid } from 'nanoid'
import { isValidObjectId } from 'mongoose';

export const CreateShopShortId = async (req: Request, res: Response, next: NextFunction) => {
  console.log('data', req.body)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  const shortD = await ShortUrlModel.findOne({ product_id: req.body.product_id, deleted: false })

  if (shortD !== null && shortD !== undefined) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "ShortId is Already created for the Shop Id",
      data: 'err'
    })
  }

  var data = req.body
  data.short_id = nanoid(6)

  const ShortData = new ShortUrlModel(data)

  ShortData.save().then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err: Error) => {
    console.log('ShortId Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const CreateShortIdLink = async (req: Request, res: Response, next: NextFunction) => {
  console.log('data', req.body)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  var data = req.body
  data.short_id = nanoid(6)

  const ShortData = new ShortUrlModel(data)

  ShortData.save().then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err: Error) => {
    console.log('ShortId Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const EditCustomShortId = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  var data

  data = {
    link: req.body.link,
  }


  ShortUrlModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      ...data
    }
  }, { new: true }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Custom ShortId Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

// admin ShortId Detail
export const getAllShopShortIdDetail = (req: Request, res: Response, next: NextFunction) => {

  var link = (config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') : SHORT_URL_KEY)

  ShortUrlModel.aggregate([
    {
      '$match': {
        'type': 'Shop',
        'deleted': false,
        'product_id': { $nin: [null] }
      }
    },
    {
      '$addFields': {
        'redirect_link': {
          '$concat': [
            link, '$short_id'
          ]
        }
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'product_id',
        'foreignField': '_id',
        'as': 'product_id'
      }
    },
    {
      '$unwind': {
        'path': '$product_id'
      }
    },
    {
      '$lookup': {
        'from': 'shortstats',
        'localField': '_id',
        'foreignField': 'short_id',
        'as': 'click'
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'detail': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'detail': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        detail: [],
        total: 0
      }
    })
  }).catch((err) => {
    console.log('Shop Short Detail Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

// admin ShortId Detail
export const getAllCustomShortIdDetail = (req: Request, res: Response, next: NextFunction) => {

  var link = (config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') : SHORT_URL_KEY)

  ShortUrlModel.aggregate([
    {
      '$match': {
        'type': 'Custom',
        'deleted': false,
      }
    },
    {
      '$addFields': {
        'redirect_link': {
          '$concat': [
            link, '$short_id'
          ]
        }
      }
    },
    {
      '$lookup': {
        'from': 'shortstats',
        'localField': '_id',
        'foreignField': 'short_id',
        'as': 'click'
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'detail': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'detail': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        detail: [],
        total: 0
      }
    })
  }).catch((err) => {
    console.log('Custom Short Detail Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

export const updateShortId = async function (req: Request, res: Response, next: NextFunction) {


  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  const shortD = await ShortUrlModel.findOne({ _id: req.params.id })

  if (shortD == null || shortD == undefined) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: 'Request Failed',
      data: 'Something went Wrong'
    })
  }

  const ss = new ShortUrlModel({
    product_id: shortD.product_id,
    type: shortD.type,
    link: shortD.link,
    short_id: nanoid(6)
  })

  ss.save().then((cc: any) => {
    console.log('Created Or Updated', cc)
    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: cc
    })
  }).catch((err: Error) => {
    console.log('Short Url Update Err', err)
    res.status(400).json({
      message: 'Failed',
      status: res.statusCode,
      data: err
    })
  })

  // ShortUrlModel.findOneAndUpdate({ _id: req.params.id }, {
  //   $set: {
  //     short_id: nanoid(6)
  //   }
  // }).then((cc) => {
  //   console.log('Created Or Updated', cc)
  //   res.status(200).json({
  //     message: 'Success',
  //     status: res.statusCode,
  //     data: cc
  //   })
  // }).catch((err) => {
  //   console.log('Short Url Update Err', err)
  //   res.status(400).json({
  //     message: 'Failed',
  //     status: res.statusCode,
  //     data: err
  //   })
  // })
}

export const deleteShortId = (req: Request, res: Response, next: NextFunction) => {


  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  ShortUrlModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      deleted: true
    }
  }).then((cc) => {
    console.log('delete Success', cc)
    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: cc
    })
  }).catch((err) => {
    console.log('Short Url Delete Err', err)
    res.status(400).json({
      message: 'Failed',
      status: res.statusCode,
      data: err
    })
  })


}

// admin ShortId Detail
export const getAllBookingShortIdDetail = (req: Request, res: Response, next: NextFunction) => {

  ShortUrlModel.aggregate([
    {
      '$match': {
        'type': 'Booking',
        'deleted': false,
        'product_id': { $nin: [null] }
      }
    },
    {
      '$lookup': {
        'from': 'bookings',
        'localField': 'product_id',
        'foreignField': '_id',
        'as': 'product_id'
      }
    },
    {
      '$unwind': {
        'path': '$product_id'
      }
    },
    {
      '$lookup': {
        'from': 'customers',
        'localField': 'product_id.user_id',
        'foreignField': '_id',
        'as': 'product_id.user_id'
      }
    }, {
      '$unwind': {
        'path': '$product_id.user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'product_id.service_id',
        'foreignField': '_id',
        'as': 'product_id.service_id'
      }
    }, {
      '$unwind': {
        'path': '$product_id.service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'product_id.shop_id',
        'foreignField': 'user_information',
        'as': 'product_id.shop_id'
      }
    }, {
      '$unwind': {
        'path': '$product_id.shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'product_id.created_by',
        'foreignField': 'user_information',
        'as': 'product_id.shop'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'product_id.created_by',
        'foreignField': '_id',
        'as': 'product_id.customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'product_id.created_by',
        'foreignField': '_id',
        'as': 'product_id.employee'
      }
    },
    {
      '$lookup': {
        'from': 'shortstats',
        'localField': '_id',
        'foreignField': 'short_id',
        'as': 'click'
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'detail': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'detail': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        detail: [],
        total: 0
      }
    })
  }).catch((err) => {
    console.log('Booking Short Detail Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}