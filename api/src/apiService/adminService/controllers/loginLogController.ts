import { NextFunction, Request, Response } from 'express';
import { LoginLogModel } from '../../../Models/LoginLog'

export const getAllLoginLogList = (req: Request, res: Response, next: NextFunction) => {

  LoginLogModel.aggregate([
    {
      $match: {
        deleted: false
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'employee'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop'
      }
    }, {
      '$addFields': {
        'name': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$employee'
                }, 0
              ]
            },
            'then': {
              '$arrayElemAt': [
                '$shop.shop_name', 0
              ]
            },
            'else': {
              '$arrayElemAt': [
                '$employee.name', 0
              ]
            }
          }
        }
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'log': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'log': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        log: [],
        total: 0
      }
    })
  }).catch((err: Error) => {
    console.log('Login Log Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}