import { NextFunction, Request, Response } from 'express';
import multer from 'multer';
import { LOCAL_BASE_IMAGE_URL, SERVER_URL, randomImageName } from '../../../config/keys';
import { upload } from '../../../config/uploadImage';
import { GalleryModel } from "../../../Models/Gallery";
import { ImageUpload, removeImage } from '../../../ImageService/main';
import { Con } from '../../../ImageService/source'
import { isValidObjectId } from 'mongoose';

export const getShopUserGallery = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  GalleryModel.find({ user_id: key._id, status: true }).then((item) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { gallery: item }
    })
  }).catch((err) => {
    console.log('Service Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}

export const createGallery = (req: Request, res: Response, next: NextFunction) => {

  const MainCheck = upload.single('image')

  MainCheck(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 500KB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }


    console.log('req files', req.file)


    const val: any = req.user
    var imgD: string = ''

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Image is Required",
        data: 'err'
      })
    }

    console.log('path', process.cwd(), 'fullPath', process.cwd() + '/src/upload/' + req.file.filename)

    ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then((item) => {
      console.log('item', item)

      if (item == null || item == undefined) {
        return res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Image is Required",
          data: 'err'
        })
      }

      imgD = item?.response.url

      var data: any = {}
      data.user_id = val._id
      data.image = imgD
      data.data = { ...req.file }

      const galleryCreate = new GalleryModel({ ...data })

      galleryCreate.save().then((item: any) => {
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Submitted",
          data: item
        })
        removeImage(process.cwd() + "/src/upload/" + req.file?.filename)

      }).catch((err: Error) => {
        console.log('Gallery Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    }).catch((err) => {
      console.log('Gallery Create Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    })

    // if (req.file.mimetype === 'image/jpeg' || req.file.mimetype === 'image/png') {
    //   imgD = `${LOCAL_BASE_IMAGE_URL}` + req.file.filename
    // }

  })

}

export const EditGallery = (req: Request, res: Response, next: NextFunction) => {

  const MainCheck = upload.single('image')

  MainCheck(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 500KB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }


    console.log('image', req.file)
    console.log('body', req.body)

    var data
    var imgD = ''

    if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    console.log('check', isValidObjectId(req.params.id))

    if (isValidObjectId(req.params.id) == false) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "All Fields are Required",
        data: 'err'
      })
    }

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        image: req.body.oldimage
      }


      GalleryModel.findOneAndUpdate({ _id: req.params.id }, {
        $set: {
          ...data
        }
      }).then((item: any) => {

        if (item == null || item == undefined) {
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: item
          })
        }

        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Submitted",
          data: item
        })
        removeImage(process.cwd() + "/src/upload/" + req.file?.filename)

      }).catch((err: Error) => {
        console.log('Gallery Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else {

      // if (req.file.mimetype === 'image/jpeg' || req.file.mimetype === 'image/png') {
      //   imgD = `${LOCAL_BASE_IMAGE_URL}` + req.file.filename
      // }

      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then((item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        imgD = item?.response.url

        data = {
          image: imgD,
          data: { ...req.file }
        }


        GalleryModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            ...data
          }
        }).then((item: any) => {

          if (item == null || item == undefined) {
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: item
            })
          }

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Submitted",
            data: item
          })

        }).catch((err: Error) => {
          console.log('Gallery Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })

      }).catch((err) => {
        console.log('Gallery Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    }

  })

}

export const deleteGallery = (req: Request, res: Response, next: NextFunction) => {
  GalleryModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      status: false
    }
  }).then((item: any) => {

    if (item == null || item == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: item
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Submitted",
      data: item
    })

  }).catch((err: Error) => {
    console.log('Gallery Delete Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}