import { NextFunction, Request, Response } from 'express';
import { isValidObjectId } from 'mongoose';
import { ContactModel } from '../../../Models/Contact'

export const CreateContact = async (req: Request, res: Response, next: NextFunction, verify: any) => {
  console.log('data', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  var data = req.body
  const contact = new ContactModel(data)

  contact.save().then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err: Error) => {
    console.log('Contact Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const getAllContactList = (req: Request, res: Response, next: NextFunction) => {

  ContactModel.aggregate([
    {
      '$match': {
        'deleted': false
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'contact': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'contact': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        contact: [],
        total: 0
      }
    })
  }).catch((err: Error) => {
    console.log('Contact Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const deleteContact = (req: Request, res: Response, next: NextFunction) => {

  if (req.params.id == null || req.params.id == undefined || req.params.id == 'null' || req.params.id == 'undefined' || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: 'Request Failed',
      data: 'Id Not Found'
    })
  }
  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  ContactModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      deleted: true
    }
  }).then((cc) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    console.log('delete Success', cc)
    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: cc
    })
  }).catch((err) => {
    console.log('Contact Delete Err', err)
    res.status(400).json({
      message: 'Failed',
      status: res.statusCode,
      data: err
    })
  })

}