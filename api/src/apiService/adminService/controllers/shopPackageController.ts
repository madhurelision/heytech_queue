import { ShopFeatureModel, ShopPackageModel } from "../../../Models/ShopPackage";
import { NextFunction, Request, Response } from 'express';
import { SERVER_URL, randomImageName } from '../../../config/keys';
import { upload } from '../../../config/uploadImage';
import multer from 'multer';
import { ImageUpload, removeImage } from '../../../ImageService/main';
import { Con } from '../../../ImageService/source';
import { isValidObjectId } from 'mongoose'


export const CreateShopPackage = async (req: Request, res: Response, next: NextFunction) => {
  console.log('image', req.file)

  const MainCheck = upload.single('image')

  MainCheck(req, res, async function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 500KB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('data', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data
    var imgD = ''

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        image: req.body.oldimage.replace(SERVER_URL, ""),
        name: req.body.name,
        price: req.body.price,
        description: req.body.description,
        user_id: req.body.user_id,
        btn: req.body.btn,
        view: req.body.view
      }


      const ShopPackageMde = new ShopPackageModel(data)

      ShopPackageMde.save().then((item: any) => {

        const ShopFeatueMd = new ShopFeatureModel(
          {
            package_id: item._id,
            service: (typeof req.body.service == "string") ? [req.body.service] : req.body.service,
            user_id: req.body.user_id
          }
        )

        ShopFeatueMd.save().then((cc: any) => {

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: item
          })

        }).catch((err: Error) => {
          console.log('ShopPackage Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })

        })
      }).catch((err: Error) => {
        console.log('ShopPackage Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })

      })

    } else {


      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item: any) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }
        imgD = item?.response.url

        data = {
          image: imgD,
          name: req.body.name,
          price: req.body.price,
          description: req.body.description,
          user_id: req.body.user_id,

          btn: req.body.btn,
          view: req.body.view
        }


        const ShopPackageMde = new ShopPackageModel(data)

        ShopPackageMde.save().then((item: any) => {

          const ShopFeatueMd = new ShopFeatureModel(
            {
              package_id: item._id,
              service: (typeof req.body.service == "string") ? [req.body.service] : req.body.service,
              user_id: req.body.user_id
            }
          )

          ShopFeatueMd.save().then((cc: any) => {

            res.status(200).json({
              status: 'Success',
              status_code: res.statusCode,
              message: "Request Success",
              data: item
            })
            removeImage(process.cwd() + "/src/upload/" + req.file?.filename)

          }).catch((err: Error) => {
            console.log('ShopPackage Create Err', err)
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: err
            })

          })
        }).catch((err: Error) => {
          console.log('ShopPackage Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })

        })

      }).catch((err) => {
        console.log('ShopPackage Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    }


  })

}

export const getAllShopPackages = (req: Request, res: Response, next: NextFunction, verify: any) => {

  ShopPackageModel.find({ deleted: false, status: true }).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { package: item }
    })
  }).catch((err: Error) => {
    console.log('ShopPackage Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const getAllShopPackageList = (req: Request, res: Response, next: NextFunction) => {

  ShopPackageModel.aggregate([
    {
      $match: {
        deleted: false
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'user_id'
      }
    },
    {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'shopfeatures',
        'localField': '_id',
        'foreignField': 'package_id',
        'as': 'feature'
      }
    }, {
      '$unwind': {
        'path': '$feature',
        'preserveNullAndEmptyArrays': true
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'feature.service',
        'foreignField': '_id',
        'as': 'feature.service'
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'package': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'package': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        package: [],
        total: 0
      }
    })
  }).catch((err: Error) => {
    console.log('ShopPackage Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}


export const EditShopPackage = async (req: Request, res: Response, next: NextFunction) => {
  console.log('image', req.file)

  const MainCheck = upload.single('image')

  MainCheck(req, res, async function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 500KB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('body', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data: any
    var key: any = req.user
    var imgD = ''

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        image: req.body.oldimage,
        name: req.body.name,
        price: req.body.price,
        description: req.body.description,
        user_id: req.body.user_id,

        btn: req.body.btn,
        view: req.body.view
      }


      ShopPackageModel.findOneAndUpdate({ _id: req.params.id }, {
        $set: {
          ...data
        }
      }, { new: true }).then(async (cc) => {
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: cc
        })

      }).catch((err) => {
        console.log('ShopPackage Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }
        imgD = item?.response.url

        data = {
          image: imgD,
          name: req.body.name,
          price: req.body.price,
          description: req.body.description,
          user_id: req.body.user_id,

          btn: req.body.btn,
          view: req.body.view
        }


        ShopPackageModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            ...data
          },
        }, { new: true }).then(async (cc) => {
          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: cc
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err) => {
          console.log('ShopPackage Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('ShopPackage Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    }

  })


}


export const deleteShopPackage = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  console.log('body', req.params.id)
  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }


  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  ShopPackageModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      deleted: true
    }
  }, { new: true }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('ShopPackage Delete Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const updateStatusShopPackage = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  console.log('body', req.params.id)
  console.log('body', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  ShopPackageModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      status: req.body.status
    }
  }, { new: true }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('ShopPackage Delete Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}


export const changeMultipleShopPackageStatusByAdmin = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.id == null || req.body.id == undefined || req.body.id.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  ShopPackageModel.updateMany({ _id: req.body.id.map((cc: any) => cc) }, {
    $set: {
      ...req.body.status
    }
  }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('ShopPackage Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}