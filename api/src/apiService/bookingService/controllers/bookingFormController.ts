import { NextFunction, Request, Response } from 'express';
import moment from 'moment';
import { bookingOTPMsg, createDate, generateHash, MAIL_SECRET_KEY, otpGenerate, otpPass, QUEUE_URL, SERVER_URL, ServiceSmsMail, ServiceTemplateId } from '../../../config/keys';
import { NewBookingModel } from '../../../Models/Booking'
import mongoose from 'mongoose'
import axios from 'axios';
import config from 'config';
import jwt from 'jsonwebtoken';
import { isValidObjectId } from 'mongoose';
import { ShopModel } from '../../../Models/User';


export const createNewQueue = async (req: Request, res: Response, next: NextFunction) => {

  console.log('Customer', req.user)
  console.log('Ip Book', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  const shopData = val.shopData
  console.log('body', req.body, 'shopData', shopData)

  if (shopData == null || shopData == undefined || Object.keys(shopData).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  var data = req.body
  data.appointment_date = val.date.appointment_date
  data.appointment_time = val.date.appointment_time
  data.user_id = val._id
  data.queue = true
  data.waiting_key = 'min'
  data.shop_status = shopData.open
  data.otp = otpGenerate()
  data.status = (!shopData || shopData == null || shopData == undefined) ? 'waiting' : (shopData.queue == true) ? 'accepted' : 'waiting'
  data.hash = generateHash(data)

  const BookingData = new NewBookingModel({ ...data })
  BookingData.save().then(async (item: any) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        _id: item._id,
        user_email: item.user_email,
        shop_email: item.shop_email,
        mobile: item.mobile,
        createdAt: item.createdAt,
      }
    })

  }).catch((err: Error) => {
    console.log('Booking Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}


export const CreateNewBooking = async (req: Request, res: Response, next: NextFunction) => {

  console.log('Customer', req.user)
  console.log('Ip', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  const shopData = await ShopModel.findOne({ user_information: req.body.shop_id })

  console.log('body', req.body, 'shopData', shopData)

  if (shopData == null || shopData == undefined || Object.keys(shopData).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  var data = req.body
  data.user_id = val._id
  data.appointment_date = createDate(new Date(req.body.appointment_date), parseInt(req.body.appointment_time.replace(/\-.*/, '')))
  data.end_time = createDate(new Date(req.body.appointment_date), parseInt(req.body.appointment_time.replace(/\-.*/, '')))
  data.status = (!shopData || shopData == null || shopData == undefined) ? 'waiting' : (shopData.appointment == true) ? 'accepted' : 'waiting'
  data.otp = otpGenerate()
  data.hash = generateHash(data)
  data.seat = val.service.seat[0]
  data.shop_status = shopData.open

  const BookingData = new NewBookingModel({ ...data })
  BookingData.save().then(async (item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        _id: item._id,
        user_email: item.user_email,
        shop_email: item.shop_email,
        mobile: item.mobile,
        createdAt: item.createdAt,
      }
    })
  }).catch((err: Error) => {
    console.log('Booking Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })


}
