import { NextFunction, Request, Response } from 'express';
import moment from 'moment';
import { userQueueAndBooking } from '../../../config/function';
import { bookingOTPMsg, generateHash, MAIL_SECRET_KEY, otpGenerate, otpPass, QUEUE_URL, SERVER_URL, ServiceSmsMail, ServiceTemplateId } from '../../../config/keys';
import { BookingModel } from '../../../Models/Booking'
import { createShortUrl } from '../../../modules/ShortUrl';
import mongoose from 'mongoose'
import { ShopModel } from '../../../Models/User'
import axios from 'axios';
import config from 'config'
import { saveIpDetail } from '../../../config/IpDetail';
import jwt from 'jsonwebtoken'
import { TravelModel } from '../../../Models/Travel'
import { ServiceModel } from '../../../Models/Service';
import { bookingAdd } from '../../../queue/bookingQueue';
import { isValidObjectId } from 'mongoose'
import { getQueue } from '../../../queue/multiQueue';
import { performance } from 'perf_hooks'
import { LogQueryModel } from '../../../Models/LogData';

export const CreateBookingCustomer = async (req: Request, res: Response, next: NextFunction, verify: any) => {

  console.log('Customer', req.user)
  console.log('Ip', req.ip)
  console.log('test', verify)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  const shopData = await ShopModel.findOne({ user_information: req.body.shop_id })

  console.log('body', req.body, 'shopData', shopData)

  if (shopData == null || shopData == undefined || Object.keys(shopData).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  var data = req.body

  if (verify == true) {

    data.shop_email = shopData.email
    data.shop_name = shopData.shop_name
    data.user_id = val._id
    data.appointment_date = createDate(new Date(req.body.appointment_date), parseInt(req.body.appointment_time.replace(/\-.*/, '')))
    data.end_time = createDate(new Date(req.body.appointment_date), parseInt(req.body.appointment_time.replace(/\-.*/, '')))
    data.status = (!shopData || shopData == null || shopData == undefined) ? 'waiting' : (shopData.appointment == true) ? 'accepted' : 'waiting'
    data.otp_verify = true
    data.test = true
    data.hash = generateHash(data)
    data.seat = val.service.seat[0]
    data.shop_status = shopData.open

  } else {

    data.user_id = val._id
    data.appointment_date = createDate(new Date(req.body.appointment_date), parseInt(req.body.appointment_time.replace(/\-.*/, '')))
    data.end_time = createDate(new Date(req.body.appointment_date), parseInt(req.body.appointment_time.replace(/\-.*/, '')))
    data.status = (!shopData || shopData == null || shopData == undefined) ? 'waiting' : (shopData.appointment == true) ? 'accepted' : 'waiting'
    data.otp = otpGenerate()
    data.hash = generateHash(data)
    data.seat = val.service.seat[0]
    data.shop_status = shopData.open
  }

  const BookingData = new BookingModel({ ...data })
  BookingData.save().then(async (item: any) => {

    if (verify == true) {

      const ShortUrl = await createShortUrl(item._id)
      const locationD = await saveIpDetail(req, item._id, false)
      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: {
          _id: item._id,
          user_email: item.user_email,
          shop_email: item.shop_email,
          mobile: item.mobile,
          createdAt: item.createdAt,
        }
      })

    } else {

      const locationD = await saveIpDetail(req, item._id, false)
      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: {
          _id: item._id,
          user_email: item.user_email,
          shop_email: item.shop_email,
          mobile: item.mobile,
          createdAt: item.createdAt,
        }
      })
    }
  }).catch((err: Error) => {
    console.log('Booking Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })


}

export const verifyBookingOtp = async (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.body, 'Id', req.params.id)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  //const shopData = await BookingModel.findOne({_id:req.params.id})

  BookingModel.findOneAndUpdate({ _id: req.params.id, otp: parseInt(req.body.otp) }, {
    $set: {
      otp_verify: true
    }
  }, { new: true }).then(async (item: any) => {

    console.log('test Verify Booking', item, item == null, item == undefined)

    if (item == null || item == undefined) {

      console.log('check', otpPass, parseInt(req.body.otp), otpPass == parseInt(req.body.otp))

      if (otpPass == parseInt(req.body.otp)) {

        BookingModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            otp_verify: true
          }
        }, { new: true }).then(async (val: any) => {
          const ShortUrl = await createShortUrl(val._id)

          return res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: val
          })
        }).catch((err: Error) => {
          console.log('Booking Otp Verification Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })

        })



      } else {

        return res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Otp Verification Failed",
          data: item
        })

      }
    } else {

      const ShortUrl = await createShortUrl(item._id)

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: item
      })
    }

  }).catch((err: Error) => {
    console.log('Booking Otp Verification Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })



}

export const getCustomerBookings = (req: Request, res: Response, next: NextFunction, verify: any) => {
  console.log('Customer', req.user)
  console.log('Customer', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  // BookingModel.find({ user_id: val._id, end_time: { $lte: new Date() } }).populate('shop_id service_id').sort({ '_id': -1 })
  var startTime = performance.now()
  var query = [
    {
      '$match': {
        'user_id': new mongoose.Types.ObjectId(val._id),
        'end_time': {
          '$lte': new Date()
        },
        'test': false
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'users',
        'localField': 'shop_id',
        'foreignField': '_id',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'bookingpayments',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'payment'
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'review'
      }
    }, {
      '$sort': {
        '_id': -1
      }
    }
  ]

  BookingModel.aggregate(query).then(async (item) => {

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'bookings',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { booking: item }
    })
  }).catch((err: Error) => {
    console.log('Booking Customer List Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const getShopBookings = (req: Request, res: Response, next: NextFunction, verify: any) => {
  console.log('Shop', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  // BookingModel.find({ shop_id: val._id, end_time: { $lte: new Date() } }).populate('user_id service_id').sort({ '_id': -1 })

  var startTime = performance.now()
  var query = [
    {
      '$match': {
        'shop_id': new mongoose.Types.ObjectId(val._id),
        'end_time': {
          '$lte': new Date()
        },
        'test': false
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    },
    {
      '$lookup': {
        'from': 'bookingpayments',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'payment'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'review'
      }
    }, {
      '$sort': {
        '_id': -1
      }
    }
  ]
  BookingModel.aggregate(query).then(async (item) => {

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'bookings',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { booking: item }
    })
  }).catch((err: Error) => {
    console.log('Booking Shop List Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

/// set Waiting Time Shop

export const addWaitingTime = (req: Request, res: Response, next: NextFunction, verify: any) => {

  console.log('body', req.body, 'Id', req.params.id, 'shop', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  BookingModel.findOneAndUpdate({ _id: req.params.id, shop_id: val._id }, {
    $set: {
      ...req.body,
      waiting_verify: (req.body.status == 'waiting') ? false : true
    }
  }, { new: true }).then((item: any) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Update Booking Waiting Time Failed",
        data: item
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        _id: item._id,
        user_email: item.user_email,
        shop_email: item.shop_email,
        mobile: item.mobile,
        createdAt: item.createdAt,
      }
    })
  }).catch((err: Error) => {
    console.log('Booking Waiting Time Add Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}

// Queue Waiting Approcimate Time and date Update
export const addQueueWaitingTime = async (req: Request, res: Response, next: NextFunction, verify: any) => {


  console.log('body', req.body, 'Id', req.params.id, 'shop', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }


  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  const mainData = await BookingModel.findOne({ _id: req.params.id, shop_id: val._id })
  console.log('check Update', mainData, 'difference approx', req.body.difference_approx, parseInt(req.body.difference_approx) * 60000, 'new Date', new Date(req.body.approximate_date).getTime(), 'olddate', new Date(mainData.approximate_date).getTime())

  if (req.body.difference_approx > 0 && new Date(req.body.approximate_date).getTime() > new Date(mainData.approximate_date).getTime()) {
    console.log('One Matched')

    var ss = parseInt(req.body.difference_approx) * 60000;
    console.log('ss', ss)
    console.log('ss', mainData.approximate_date)
    console.log('ss', ss)

    const checkD1 = await BookingModel.updateMany({
      shop_id: val._id,
      queue: true,
      otp_verify: true,
      test: false,
      seat: mainData.seat,
      appointment_date: { $gte: new Date(mainData.appointment_date) },
      _id: { $nin: [req.params.id] },
      status: { $in: ['waiting', 'accepted'] }
    }, [{
      $set: {
        approximate_date: {
          $add: ['$approximate_date', ss]
        },
        waiting_date: {
          $add: ['$waiting_date', ss]
        },
        end_time: {
          $add: ['$waiting_date', ss]
        },
        waiting_number: {
          $add: ['$waiting_number', parseInt(req.body.difference_approx)]
        },
        // approximate_number: {
        //   $add: ['$approximate_number', parseInt(req.body.difference_approx)]
        // }
      }
    }])

    console.log('approx grearter', checkD1)

  } else if (req.body.difference_approx < 0 && new Date(req.body.approximate_date).getTime() < new Date(mainData.approximate_date).getTime()) {
    console.log('less then zero Matched')
    var ss = parseInt(req.body.difference_approx) * 60000;
    console.log('ss', ss)

    const checkD = await BookingModel.updateMany({
      shop_id: val._id,
      queue: true,
      otp_verify: true,
      test: false,
      seat: mainData.seat,
      appointment_date: { $gte: new Date(mainData.appointment_date) },
      _id: { $nin: [req.params.id] },
      status: { $in: ['waiting', 'accepted'] }
    }, [{
      $set: {
        approximate_date: {
          $add: ['$approximate_date', ss]
        },
        waiting_date: {
          $add: ['$waiting_date', ss]
        },
        end_time: {
          $add: ['$waiting_date', ss]
        },
        waiting_number: {
          $add: ['$waiting_number', parseInt(req.body.difference_approx)]
        },
        // approximate_number: {
        //   $add: ['$approximate_number', parseInt(req.body.difference_approx)]
        // }
      }
    }])


    console.log('approx less', checkD)

  }




  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  BookingModel.findOneAndUpdate({ _id: req.params.id, shop_id: val._id }, {
    $set: {
      ...req.body,
      waiting_verify: (req.body.status == 'waiting') ? false : true
    }
  }, { new: true }).then((item: any) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Update Queue Waiting Time Failed",
        data: item
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        _id: item._id,
        user_email: item.user_email,
        shop_email: item.shop_email,
        mobile: item.mobile,
        createdAt: item.createdAt,
      }
    })
  }).catch((err: Error) => {
    console.log('Queue Waiting Time Add Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}

export const addRemark = (req: Request, res: Response, next: NextFunction, verify: any) => {

  console.log('body', req.body, 'Id', req.params.id, 'shop', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  BookingModel.findOneAndUpdate({ _id: req.params.id, shop_id: val._id }, {
    $set: {
      ...req.body
    }
  }, { new: true }).then((item: any) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Update Booking Remark Failed",
        data: item
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        _id: item._id,
        user_email: item.user_email,
        shop_email: item.shop_email,
        mobile: item.mobile,
        createdAt: item.createdAt,
      }
    })
  }).catch((err: Error) => {
    console.log('Booking Remark Add Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}

export const changeSeatShop = async (req: Request, res: Response, next: NextFunction, verify: any) => {

  console.log('body', req.body, 'Id', req.params.id)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  const promise1 = new Promise((resolve, reject) => {

    ShopModel.findOne({ user_information: val._id }).then((item) => {
      resolve(item)

    }).catch((err) => {
      reject(err)

    })

  })

  const promise2 = new Promise((resolve, reject) => {

    BookingModel.aggregate([
      {
        '$match': {
          '_id': new mongoose.Types.ObjectId(req.params.id)
        }
      },
      {
        '$lookup': {
          'from': 'services',
          'localField': 'service_id',
          'foreignField': '_id',
          'as': 'service_id'
        }
      }, {
        '$unwind': {
          'path': '$service_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
    ]).exec((err, item) => {

      if (err) {
        console.log('Err', err)
        reject(err)
        return
      }

      console.log('sD', item)
      resolve(item)

    })

  })

  Promise.allSettled([promise1, promise2]).then(async (item: any) => {

    const shopData = item[0].value
    const bookData = item[1].value

    console.log('body', req.body, 'shopData', shopData, 'bookData', bookData)

    const file: any = {
      approximate_time: parseInt(bookData[0].service_id.waiting_number) * parseInt(bookData[0].size) + ' ' + bookData[0].service_id.waiting_key,
      newtime: (bookData[0].service_id.waiting_key == 'min') ? parseInt(bookData[0].service_id.waiting_number) * parseInt(bookData[0].size) : parseInt(bookData[0].service_id.waiting_number) * parseInt(bookData[0].size) * 60
    }

    if (bookData.length > 0) {

      const timeDeduct = (bookData[0].approximate_key == 'min') ? parseInt(bookData[0].approximate_number) * 60000 : parseInt(bookData[0].approximate_number) * 60 * 60000

      const wait_time = (bookData[0].approximate_key == 'min') ? parseInt(bookData[0].approximate_number) : parseInt(bookData[0].approximate_number) * 60

      console.log('SS', timeDeduct)

      const checkD = await BookingModel.updateMany({
        shop_id: val._id,
        seat: bookData[0]?.seat,
        queue: true,
        otp_verify: true,
        approximate_date: { $gte: new Date(bookData[0].approximate_date) },
        _id: { $nin: [req.params.id] },
        status: { $in: ['waiting', 'accepted'] }
      }, [{
        $set: {
          approximate_date: {
            $subtract: ['$approximate_date', timeDeduct]
          },
          waiting_date: {
            $subtract: ['$waiting_date', timeDeduct]
          },
          end_time: {
            $subtract: ['$waiting_date', timeDeduct]
          },
          waiting_number: {
            $subtract: ['$waiting_number', wait_time]
          },
        }
      }])

      console.log('Check D Cancel Queue', checkD)

    }

    const bookingTime = await BookingModel.aggregate([
      {
        '$match': {
          'shop_id': new mongoose.Types.ObjectId(val._id),
          'queue': true,
          'test': verify,
          'appointment_date': { '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0)), $lt: new Date(new Date().setHours(23, 59, 59)) },
          'status': { '$in': ['waiting', 'accepted'] },
          'approximate_date': { '$gte': new Date() },
          'otp_verify': true,
          'seat': req.body.seat
        }
      },
      {
        '$sort': {
          '_id': -1
        }
      },
      {
        '$limit': 1
      }
    ]).exec()

    console.log('mainD Data', bookingTime)

    var mainD: any = (bookingTime.length > 0) ? bookingTime[0].approximate_date : createQueueDate(bookData[0].appointment_date, parseInt((!shopData.waiting_number || shopData.waiting_number == null || shopData.waiting_number == undefined) ? 0 : shopData.waiting_number))

    var data: any = req.body

    data.waiting_time = (bookingTime.length > 0) ? parseInt(minusDate(mainD, bookData[0].appointment_date)) + ' ' + 'min' : shopData.waiting_number
    data.waiting_number = (bookingTime.length > 0) ? parseInt(minusDate(mainD, bookData[0].appointment_date)) : parseInt(shopData.waiting_number)
    data.waiting_key = 'min'
    data.waiting_date = mainD
    data.approximate_date = createQueueDate(mainD, parseInt((!file.newtime || file.newtime == null || file.newtime == undefined) ? 0 : file.newtime))
    data.approximate_time = file.approximate_time
    data.approximate_number = parseInt(file.approximate_time)
    data.approximate_key = file.approximate_time.replace(/[0-9]/g, '').replace(' ', '')
    data.end_time = mainD


    BookingModel.findOneAndUpdate({ _id: req.params.id }, {
      $set: {
        ...data
      }
    }, { new: true }).then((item: any) => {

      if (item == null || item == undefined) {
        return res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Update seat change Failed",
          data: item
        })
      }

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: {
          _id: item._id,
          user_email: item.user_email,
          shop_email: item.shop_email,
          mobile: item.mobile,
          createdAt: item.createdAt,
        }
      })
    }).catch((err: Error) => {
      console.log('Booking Seat change Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })

    })

  }).catch((err: Error) => {
    console.log('Booking Seat change Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}

export const changeSeatAdmin = (req: Request, res: Response, next: NextFunction, verify: any) => {

  console.log('body', req.body, 'Id', req.params.id)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  const promise1 = new Promise((resolve, reject) => {

    BookingModel.aggregate([
      {
        '$match': {
          '_id': new mongoose.Types.ObjectId(req.params.id)
        }
      },
      {
        '$lookup': {
          'from': 'services',
          'localField': 'service_id',
          'foreignField': '_id',
          'as': 'service_id'
        }
      }, {
        '$unwind': {
          'path': '$service_id'
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'shop_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$unwind': {
          'path': '$shop_id'
        }
      },
    ]).exec((err, item) => {

      if (err) {
        console.log('Err', err)
        reject(err)
        return
      }

      console.log('sD', item)
      resolve(item)

    })

  })

  Promise.allSettled([promise1]).then(async (item: any) => {

    const bookData = item[0].value

    console.log('body', req.body, 'bookData', bookData)

    const file: any = {
      approximate_time: parseInt(bookData[0].service_id.waiting_number) * parseInt(bookData[0].size) + ' ' + bookData[0].service_id.waiting_key,
      newtime: (bookData[0].service_id.waiting_key == 'min') ? parseInt(bookData[0].service_id.waiting_number) * parseInt(bookData[0].size) : parseInt(bookData[0].service_id.waiting_number) * parseInt(bookData[0].size) * 60
    }


    if (bookData.length > 0) {

      const timeDeduct = (bookData[0].approximate_key == 'min') ? parseInt(bookData[0].approximate_number) * 60000 : parseInt(bookData[0].approximate_number) * 60 * 60000

      const wait_time = (bookData[0].approximate_key == 'min') ? parseInt(bookData[0].approximate_number) : parseInt(bookData[0].approximate_number) * 60

      console.log('SS', timeDeduct)

      const checkD = await BookingModel.updateMany({
        shop_id: bookData[0]?.shop_id.user_information,
        seat: bookData[0]?.seat,
        queue: true,
        otp_verify: true,
        approximate_date: { $gte: new Date(bookData[0].approximate_date) },
        _id: { $nin: [req.params.id] },
        status: { $in: ['waiting', 'accepted'] }
      }, [{
        $set: {
          approximate_date: {
            $subtract: ['$approximate_date', timeDeduct]
          },
          waiting_date: {
            $subtract: ['$waiting_date', timeDeduct]
          },
          end_time: {
            $subtract: ['$waiting_date', timeDeduct]
          },
          waiting_number: {
            $subtract: ['$waiting_number', wait_time]
          },
        }
      }])

      console.log('Check D Cancel Queue', checkD)

    }

    const bookingTime = await BookingModel.aggregate([
      {
        '$match': {
          'shop_id': new mongoose.Types.ObjectId(bookData[0].shop_id.user_information),
          'queue': true,
          'test': verify,
          'appointment_date': { '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0)), $lt: new Date(new Date().setHours(23, 59, 59)) },
          'status': { '$in': ['waiting', 'accepted'] },
          'approximate_date': { '$gte': new Date() },
          'otp_verify': true,
          'seat': req.body.seat
        }
      },
      {
        '$sort': {
          '_id': -1
        }
      },
      {
        '$limit': 1
      }
    ]).exec()

    console.log('mainD Data', bookingTime)

    var mainD: any = (bookingTime.length > 0) ? bookingTime[0].approximate_date : createQueueDate(bookData[0].appointment_date, parseInt((!bookData[0].shop_id.waiting_number || bookData[0].shop_id.waiting_number == null || bookData[0].shop_id.waiting_number == undefined) ? 0 : bookData[0].shop_id.waiting_number))

    var data: any = req.body

    data.waiting_time = (bookingTime.length > 0) ? parseInt(minusDate(mainD, bookData[0].appointment_date)) + ' ' + 'min' : bookData[0].shop_id.waiting_number
    data.waiting_number = (bookingTime.length > 0) ? parseInt(minusDate(mainD, bookData[0].appointment_date)) : parseInt(bookData[0].shop_id.waiting_number)
    data.waiting_key = 'min'
    data.waiting_date = mainD
    data.approximate_date = createQueueDate(mainD, parseInt((!file.newtime || file.newtime == null || file.newtime == undefined) ? 0 : file.newtime))
    data.approximate_time = file.approximate_time
    data.approximate_number = parseInt(file.approximate_time)
    data.approximate_key = file.approximate_time.replace(/[0-9]/g, '').replace(' ', '')
    data.end_time = mainD


    BookingModel.findOneAndUpdate({ _id: req.params.id }, {
      $set: {
        ...data
      }
    }, { new: true }).then((item: any) => {

      if (item == null || item == undefined) {
        return res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Update seat change Failed",
          data: item
        })
      }

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: {
          _id: item._id,
          user_email: item.user_email,
          shop_email: item.shop_email,
          mobile: item.mobile,
          createdAt: item.createdAt,
        }
      })
    }).catch((err: Error) => {
      console.log('Booking Seat change Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })

    })

  }).catch((err: Error) => {
    console.log('Booking Seat change Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })


  // BookingModel.findOneAndUpdate({ _id: req.params.id }, {
  //   $set: {
  //     ...req.body
  //   }
  // }, { new: true }).then((item: any) => {

  //   if (item == null || item == undefined) {
  //     return res.status(400).json({
  //       status: 'Failed',
  //       status_code: res.statusCode,
  //       message: "Update Booking Remark Failed",
  //       data: item
  //     })
  //   }

  //   res.status(200).json({
  //     status: 'Success',
  //     status_code: res.statusCode,
  //     message: "Request Success",
  //     data: item
  //   })
  // }).catch((err: Error) => {
  //   console.log('Booking Seat Add Err', err)
  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })

  // })
}

export const getSingleBooking = (req: Request, res: Response, next: NextFunction) => {
  console.log('Customer', req.ip)

  if (req.params.id == null || req.params.id == undefined || req.params.id == '') {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }


  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  var startTime = performance.now()
  const query = [
    {
      $match: {
        _id: new mongoose.Types.ObjectId(req.params.id)
      }
    },
    {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'bookingpayments',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'payment'
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$shop_id.user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'shopreview'
      }
    }, {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$shopreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalShop': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$shopreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$shopreview'
                  }
                ]
              }
            }
          }
        }
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_id._id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$shop_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'userreview'
      }
    }, {
      '$addFields': {
        'user_count': {
          '$reduce': {
            'input': '$userreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalUser': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$userreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$user_count', {
                    '$size': '$userreview'
                  }
                ]
              }
            }
          }
        }
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'created_by',
        'foreignField': 'user_information',
        'as': 'shop'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'employee'
      }
    },
    {
      '$lookup': {
        'from': 'travels',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'travel'
      }
    },
    {
      '$addFields': {
        'distance': {
          '$arrayElemAt': [
            '$travel.distance', 0
          ]
        },
        'distanceVal': {
          '$arrayElemAt': [
            '$travel.distanceVal', 0
          ]
        },
        'duration': {
          '$arrayElemAt': [
            '$travel.duration', 0
          ]
        },
        'durationVal': {
          '$arrayElemAt': [
            '$travel.durationVal', 0
          ]
        },
        'lattitude': {
          '$arrayElemAt': [
            '$travel.lattitude', 0
          ]
        },
        'longitude': {
          '$arrayElemAt': [
            '$travel.longitude', 0
          ]
        },
        'created_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop'
                    }, 1
                  ]
                },
                'then': '$shop.shop_name',
                'else': {
                  '$cond': {
                    'if': {
                      '$eq': [
                        {
                          '$size': '$customer'
                        }, 1
                      ]
                    },
                    'then': '$customer.first_name',
                    'else': {
                      '$cond': {
                        'if': {
                          '$eq': [
                            {
                              '$size': '$employee'
                            }, 1
                          ]
                        },
                        'then': '$employee.name',
                        'else': [
                          ''
                        ]
                      }
                    }
                  }
                }
              }
            }, 0
          ]
        }
      }
    },
  ]

  BookingModel.aggregate(query).then(async (item) => {

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'bookings',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        detail: (item.length > 0) ? item[0] : {}
      }
    })
  }).catch((err: Error) => {
    console.log('Booking List Single Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}

export const getShopUserCurrentAppointment = async (req: Request, res: Response, next: NextFunction, verify: any) => {
  console.log('Shop User', req.user)
  console.log('Shop User', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  // const changeData = await BookingModel.updateMany({ shop_id: val._id, end_time: { $lte: new Date() } }, {
  //   $set: {
  //     status: 'expired',
  //     expired: true
  //   }
  // })

  // console.log('Shop', changeData)
  //BookingModel.find({ shop_id: val._id, end_time: { $gte: moment().startOf('day') }, status: { $in: ['waiting', 'accepted'] } }).populate('user_id service_id').sort({ '_id': -1 })

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        'shop_id': new mongoose.Types.ObjectId(val._id),
        //'end_time': { $gte: moment().startOf('day') },
        'end_time': { $gte: new Date(new Date().setHours(0o0, 0o0, 0o0)) },
        'status': { $in: ['waiting', 'accepted'] },
        'otp_verify': true,
        'test': false
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'bookingpayments',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'payment'
      }
    },
    {
      '$lookup': {
        'from': 'travels',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'travel'
      }
    },
    {
      '$addFields': {
        'distance': {
          '$arrayElemAt': [
            '$travel.distance', 0
          ]
        },
        'distanceVal': {
          '$arrayElemAt': [
            '$travel.distanceVal', 0
          ]
        },
        'duration': {
          '$arrayElemAt': [
            '$travel.duration', 0
          ]
        },
        'durationVal': {
          '$arrayElemAt': [
            '$travel.durationVal', 0
          ]
        },
        'lattitude': {
          '$arrayElemAt': [
            '$travel.lattitude', 0
          ]
        },
        'longitude': {
          '$arrayElemAt': [
            '$travel.longitude', 0
          ]
        },
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_id._id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$shop_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'userreview'
      }
    }, {
      '$addFields': {
        'user_count': {
          '$reduce': {
            'input': '$userreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalUser': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$userreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$user_count', {
                    '$size': '$userreview'
                  }
                ]
              }
            }
          }
        }
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'created_by',
        'foreignField': 'user_information',
        'as': 'shop'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'employee'
      }
    },
    {
      '$addFields': {
        'created_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop'
                    }, 1
                  ]
                },
                'then': '$shop.shop_name',
                'else': {
                  '$cond': {
                    'if': {
                      '$eq': [
                        {
                          '$size': '$customer'
                        }, 1
                      ]
                    },
                    'then': '$customer.first_name',
                    'else': {
                      '$cond': {
                        'if': {
                          '$eq': [
                            {
                              '$size': '$employee'
                            }, 1
                          ]
                        },
                        'then': '$employee.name',
                        'else': [
                          ''
                        ]
                      }
                    }
                  }
                }
              }
            }, 0
          ]
        }
      }
    },
    {
      '$project': {
        'otp_verify': 0,
        'otp': 0
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'booking': [
          {
            '$sort': {
              '_id': -1
            }
          },
          {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'booking': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]
  BookingModel.aggregate(query).exec(async (err, item) => {

    if (err) {
      console.log('Booking Shop Current Appointment List Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }


    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'bookings',
      startTime: startTime,
      endTime: parseInt(endTime)
    })


    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item[0] : { booking: [], total: 0 }
    })
  })
  // .catch((err: Error) => {
  //   console.log('Booking Shop Current Appointment List Err', err)
  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })

  // })

}

export const getCustomerCurrentAppointment = async (req: Request, res: Response, next: NextFunction, verify: any) => {
  console.log('Customer', req.user)
  console.log('Customer', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  // const changeData = await BookingModel.updateMany({ user_id: val._id, end_time: { $lte: new Date() }, queue:false }, {
  //   $set: {
  //     status: 'expired',
  //     expired: true
  //   }
  // })

  // console.log('Customer', changeData)
  // BookingModel.find({ user_id: val._id, end_time: { $gte: moment().startOf('day') }, status: { $in: ['waiting', 'accepted'] } }).populate('shop_id service_id').sort({ '_id': -1 })

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        'user_id': new mongoose.Types.ObjectId(val._id),
        //'end_time': { $gte: moment().startOf('day') },
        'end_time': { $gte: new Date(new Date().setHours(0o0, 0o0, 0o0)) },
        'status': { $in: ['waiting', 'accepted'] },
        'otp_verify': true,
        'test': false
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'bookingpayments',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'payment'
      }
    },
    {
      '$lookup': {
        'from': 'travels',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'travel'
      }
    },
    {
      '$addFields': {
        'distance': {
          '$arrayElemAt': [
            '$travel.distance', 0
          ]
        },
        'distanceVal': {
          '$arrayElemAt': [
            '$travel.distanceVal', 0
          ]
        },
        'duration': {
          '$arrayElemAt': [
            '$travel.duration', 0
          ]
        },
        'durationVal': {
          '$arrayElemAt': [
            '$travel.durationVal', 0
          ]
        },
        'lattitude': {
          '$arrayElemAt': [
            '$travel.lattitude', 0
          ]
        },
        'longitude': {
          '$arrayElemAt': [
            '$travel.longitude', 0
          ]
        },
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$shop_id.user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'shopreview'
      }
    }, {
      '$lookup': {
        'from': 'bookings',
        'let': {
          'id': '$_id',
          'main': '$shop_id.user_information',
          'approximate_date': '$approximate_date',
          'seat': '$seat'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$seat', '$$seat'
                    ]
                  }, {
                    '$not': [
                      {
                        '$eq': [
                          '$_id', '$$id'
                        ]
                      }
                    ]
                  }, {
                    '$eq': [
                      '$queue', true
                    ]
                  }, {
                    '$eq': [
                      '$test', false
                    ]
                  }, {
                    '$lte': [
                      '$approximate_date', '$$approximate_date'
                    ]
                  }, {
                    '$in': [
                      '$status', [
                        'waiting', 'accepted'
                      ]
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'sbook'
      }
    }, {
      '$addFields': {
        'count': {
          '$reduce': {
            'input': '$sbook',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.size'
              ]
            }
          }
        }
      }
    },
    {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$shopreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalShop': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$shopreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$shopreview'
                  }
                ]
              }
            }
          }
        }
      }
    },
    {
      '$project': {
        'otp_verify': 0,
        'otp': 0
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'booking': [
          {
            '$sort': {
              '_id': -1
            }
          },
          {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'booking': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]
  BookingModel.aggregate(query).exec(async (err, item) => {

    if (err) {
      console.log('Customer Current Appointment List Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'bookings',
      startTime: startTime,
      endTime: parseInt(endTime)
    })



    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item[0] : { booking: [], total: 0 }
    })
  })
  // .catch((err: Error) => {
  //   console.log('Customer Current Appointment List Err', err)
  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })

  // })

}

export const CreateQueueBookingCustomer = async (req: Request, res: Response, next: NextFunction) => {

  console.log('Customer', req.user)
  console.log('Customer', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  const shopData = await ShopModel.findOne({ user_information: req.body.shop_id })

  console.log('body', req.body, 'shopData', shopData)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  const MainCheck: any = await BookingModel.find({
    shop_id: req.body.shop_id, queue: true, appointment_date: { $gte: moment().startOf('day'), $lte: moment().endOf('day') }, status: { $in: ['waiting', 'accepted'] }, approximate_date: { $gte: new Date(req.body.appointment_date) },
    otp_verify: true
  }).sort({ _id: -1 }).limit(1)


  console.log('mainD Data', MainCheck)

  var mainD: any = (MainCheck.length > 0) ? MainCheck[0].approximate_date : createQueueDate(req.body.appointment_date, parseInt((!req.body.time || req.body.time == null || req.body.time == undefined) ? 0 : req.body.time))

  console.log('mainD', mainD, createQueueDate(req.body.appointment_date, parseInt((!req.body.time || req.body.time == null || req.body.time == undefined) ? 0 : req.body.time)))

  var data = req.body
  data.user_id = val._id
  data.queue = true
  data.waiting_time = (MainCheck.length) ? parseInt(minusDate(mainD, req.body.appointment_date)) + ' ' + 'min' : req.body.time
  data.waiting_number = (MainCheck.length) ? parseInt(minusDate(mainD, req.body.appointment_date)) : parseInt(req.body.time)
  data.waiting_key = 'min'
  data.waiting_date = mainD
  data.approximate_date = createQueueDate(mainD, parseInt((!req.body.newtime || req.body.newtime == null || req.body.newtime == undefined) ? 0 : req.body.newtime))
  data.approximate_time = req.body.approximate_time
  data.approximate_number = parseInt(req.body.approximate_time)
  data.approximate_key = req.body.approximate_time.replace(/[0-9]/g, '').replace(' ', '')
  data.waiting_verify = true
  data.end_time = mainD
  data.otp = otpGenerate()
  data.status = (!shopData || shopData == null || shopData == undefined) ? 'waiting' : (shopData.queue == true) ? 'accepted' : 'waiting'
  data.hash = generateHash(data)
  data.shop_status = shopData.open
  // data.appointment_date = createDate(new Date(req.body.appointment_date), parseInt(req.body.appointment_time.replace(/\-.*/, '')))
  // data.end_time = createDate(new Date(req.body.appointment_date), parseInt(req.body.appointment_time.replace(/\-.*/, '')))

  const BookingData = new BookingModel({ ...data })
  BookingData.save().then(async (item: any) => {
    const locationD = await saveIpDetail(req, item._id, true)
    const travelD = await TravelModel.create({ ...data.location, booking_id: item._id })
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        _id: item._id,
        user_email: item.user_email,
        shop_email: item.shop_email,
        mobile: item.mobile,
        createdAt: item.createdAt,
      }
    })
  }).catch((err: Error) => {
    console.log('Booking Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })


}

export const shopUserQueueList = (req: Request, res: Response, next: NextFunction, verify: any) => {

  console.log('shop', req.user)
  console.log('shop', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  //  BookingModel.find({ shop_id: val._id, appointment_date: { $gte: moment().startOf('day'), $lte: moment().endOf('day') }, queue: true, status: { $in: ['waiting', 'accepted'] } }).populate('user_id service_id').sort({ 'end_time': -1 })

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        'shop_id': new mongoose.Types.ObjectId(val._id),
        'appointment_date': {
          '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0)),
          '$lt': new Date(new Date().setHours(23, 59, 59))
        },
        'queue': true,
        'test': false,
        'otp_verify': true,
        'status': { $in: ['waiting', 'accepted'] }
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'bookingpayments',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'payment'
      }
    },
    {
      '$lookup': {
        'from': 'travels',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'travel'
      }
    },
    {
      '$addFields': {
        'distance': {
          '$arrayElemAt': [
            '$travel.distance', 0
          ]
        },
        'distanceVal': {
          '$arrayElemAt': [
            '$travel.distanceVal', 0
          ]
        },
        'duration': {
          '$arrayElemAt': [
            '$travel.duration', 0
          ]
        },
        'durationVal': {
          '$arrayElemAt': [
            '$travel.durationVal', 0
          ]
        },
        'lattitude': {
          '$arrayElemAt': [
            '$travel.lattitude', 0
          ]
        },
        'longitude': {
          '$arrayElemAt': [
            '$travel.longitude', 0
          ]
        },
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_id._id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$shop_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'userreview'
      }
    }, {
      '$addFields': {
        'user_count': {
          '$reduce': {
            'input': '$userreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalUser': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$userreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$user_count', {
                    '$size': '$userreview'
                  }
                ]
              }
            }
          }
        }
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'created_by',
        'foreignField': 'user_information',
        'as': 'shop'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'employee'
      }
    },
    {
      '$addFields': {
        'created_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop'
                    }, 1
                  ]
                },
                'then': '$shop.shop_name',
                'else': {
                  '$cond': {
                    'if': {
                      '$eq': [
                        {
                          '$size': '$customer'
                        }, 1
                      ]
                    },
                    'then': '$customer.first_name',
                    'else': {
                      '$cond': {
                        'if': {
                          '$eq': [
                            {
                              '$size': '$employee'
                            }, 1
                          ]
                        },
                        'then': '$employee.name',
                        'else': [
                          ''
                        ]
                      }
                    }
                  }
                }
              }
            }, 0
          ]
        }
      }
    },
    {
      '$project': {
        'otp_verify': 0,
        'otp': 0
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'booking': [
          {
            '$sort': {
              'end_time': -1
            }
          },
          {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'booking': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]
  BookingModel.aggregate(query).exec(async (err, item) => {

    if (err) {
      console.log('Customer Current Appointment List Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'bookings',
      startTime: startTime,
      endTime: parseInt(endTime)
    })



    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item[0] : { booking: [], total: 0 }
    })
  })
  // .catch((err: Error) => {
  //   console.log('Customer Current Appointment List Err', err)
  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })

  // })

}


export const CountBooking = (req: Request, res: Response, next: NextFunction, verify: any) => {

  console.log('shop', req.user)
  console.log('shop', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  userQueueAndBooking(val._id, (status: any, err: Error, data: any) => {

    if (err) {
      console.log('Customer Appointment & Queue Count Err', err)
      return res.status(status).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    res.status(status).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: data
    })

  })

  // BookingModel.countDocuments({ user_id: val._id, appointment_date: { $gte: moment().startOf('day'), $lte: moment().endOf('day') }, status: { $in: ['waiting', 'accepted'] } }).then((item) => {
  //   res.status(200).json({
  //     status: 'Success',
  //     status_code: res.statusCode,
  //     message: "Request Success",
  //     data: item
  //   })
  // }).catch((err: Error) => {
  //   console.log('Customer Appointment & Queue Count Err', err)
  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })

  // })

}

// Cancel User Booking
export const userCancelBooking = async (req: Request, res: Response, next: NextFunction, verify: any) => {

  if (req.params.id == "" || req.params.id == null || req.params.id == undefined || req.params.id == "null" || req.params.id == 'undefined') {
    return res.status(400).json({
      message: 'Request Failed',
      status: res.statusCode,
      data: "Id Not Found"
    })
  }

  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Something Went Wrong???",
      data: 'Token Not Found'
    })
  }

  const BookingData = await BookingModel.findOne({ _id: req.params.id }).populate('service_id')

  if (BookingData == null || BookingData == undefined) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  try {

    const otp = otpGenerate()

    const msgT = await bookingOTPMsg({ service_id: BookingData.service_id, shop_name: BookingData.shop_name, otp: otp, bookingid: BookingData.bookingid, queue: BookingData.queue })
    const smsToken = jwt.sign({ date: new Date(), message: msgT, id: ServiceTemplateId.book, type: 'booking', bookingid: BookingData.bookingid, user_id: [BookingData._id] }, MAIL_SECRET_KEY)

    if (BookingData.mobile == null || BookingData.mobile == undefined) {

      const emailOtp = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'sendOtpMail' : ServiceSmsMail + 'sendOtpMail', {
        email: BookingData.user_email,
        otp: otp
      }, {
        headers: {
          'auth-token': smsToken
        }
      })



      console.log('Otp Sent', emailOtp)

    } else {
      const mobileOtp = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
        number: BookingData.mobile,
        otp: otp
      }, {
        headers: {
          'auth-token': smsToken
        }
      })


      console.log('Otp Sent', mobileOtp)

    }

    BookingModel.findOneAndUpdate({ _id: req.params.id, user_id: val._id }, {
      $set: {
        cancel_otp: otp,
        cancel_verify: true
      }
    }, { new: true }).then((item: any) => {

      if (item == null || item == undefined) {
        return res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Booking Cancel Failed",
          data: item
        })
      }

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Otp Generated Please Verify",
        data: {
          _id: item._id,
          user_email: item.user_email,
          shop_email: item.shop_email,
          mobile: item.mobile,
          createdAt: item.createdAt,
        }
      })
    }).catch((err: Error) => {
      console.log('User Booking Cancel Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    })

  } catch (err) {
    console.log('Create cancel otp Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  }

}

// cancel Shop Booking
export const shopBookingCancel = async (req: Request, res: Response, next: NextFunction) => {

  if (req.params.id == "" || req.params.id == null || req.params.id == undefined || req.params.id == "null" || req.params.id == 'undefined') {
    return res.status(400).json({
      message: 'Request Failed',
      status: res.statusCode,
      data: "Id Not Found"
    })
  }

  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Something Went Wrong???",
      data: 'Token Not Found'
    })
  }

  const mainData = await BookingModel.findOne({ _id: req.params.id, shop_id: val._id })

  console.log('Shop Cancel', mainData)

  if (mainData != null || mainData !== undefined) {

    if (mainData.queue == true) {
      var ss = (mainData.approximate_key == 'min') ? parseInt(mainData.approximate_number) * 60000 : parseInt(mainData.approximate_number) * 60 * 60000

      console.log('SS', ss)

      const checkD = await BookingModel.updateMany({
        shop_id: val._id,
        queue: true,
        otp_verify: true,
        test: false,
        seat: mainData.seat,
        appointment_date: { $gte: new Date(mainData.appointment_date) },
        _id: { $nin: [req.params.id] },
        status: { $in: ['waiting', 'accepted'] }
      }, [{
        $set: {
          approximate_date: {
            $subtract: ['$approximate_date', ss]
          },
          waiting_date: {
            $subtract: ['$waiting_date', ss]
          },
          end_time: {
            $subtract: ['$waiting_date', ss]
          },
        }
      }])

      console.log('Check D Cancel Queue', checkD)

    }

  }

  BookingModel.findOneAndUpdate({ _id: req.params.id, shop_id: val._id }, {
    $set: {
      status: 'cancelled',
      expired: true
    }
  }, { new: true }).then((item: any) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Booking Cancel Failed",
        data: item
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        _id: item._id,
        user_email: item.user_email,
        shop_email: item.shop_email,
        mobile: item.mobile,
        createdAt: item.createdAt,
      }
    })
  }).catch((err: Error) => {
    console.log('Shop Booking Cancel Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

//verrify user Cancel Booking
export const userVerifyCancelBooking = async (req: Request, res: Response, next: NextFunction, verify: any) => {

  if (req.params.id == "" || req.params.id == null || req.params.id == undefined || req.params.id == "null" || req.params.id == 'undefined') {
    return res.status(400).json({
      message: 'Request Failed',
      status: res.statusCode,
      data: "Id Not Found"
    })
  }

  console.log('data', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Otp is Required",
      data: 'err'
    })
  }

  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Something Went Wrong???",
      data: 'Token Not Found'
    })
  }


  const mainData = await BookingModel.findOne({ _id: req.params.id, user_id: val._id, cancel_otp: parseInt(req.body.otp) })

  console.log('User Cancel', mainData)

  if (mainData != null || mainData !== undefined) {

    if (mainData.queue == true) {
      var ss = (mainData.approximate_key == 'min') ? parseInt(mainData.approximate_number) * 60000 : parseInt(mainData.approximate_number) * 60 * 60000

      console.log('SS', ss)

      const checkD = await BookingModel.updateMany({
        shop_id: mainData.shop_id,
        queue: true,
        otp_verify: true,
        test: false,
        seat: mainData.seat,
        appointment_date: { $gte: new Date(mainData.appointment_date) },
        _id: { $nin: [req.params.id] },
        status: { $in: ['waiting', 'accepted'] }
      }, [{
        $set: {
          approximate_date: {
            $subtract: ['$approximate_date', ss]
          },
          waiting_date: {
            $subtract: ['$waiting_date', ss]
          },
          end_time: {
            $subtract: ['$waiting_date', ss]
          },
        }
      }])

      console.log('Check D Cancel Queue', checkD)

    }

  }

  BookingModel.findOneAndUpdate({ _id: req.params.id, user_id: val._id, cancel_otp: parseInt(req.body.otp) }, {
    $set: {
      status: 'cancelled',
      expired: true
    }
  }, { new: true }).then((item: any) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Otp Verification Failed",
        data: item
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        _id: item._id,
        user_email: item.user_email,
        shop_email: item.shop_email,
        mobile: item.mobile,
        createdAt: item.createdAt,
      }
    })
  }).catch((err: Error) => {
    console.log('User Verify Booking Cancel Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

export const shopBookingQuery = (req: Request, res: Response, next: NextFunction, verify: any) => {
  console.log('Shop', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  console.log('start Date', req.body.startDate, new Date(req.body.startDate), 'endDate', req.body.endDate, new Date(req.body.endDate), moment(req.body.startDate).startOf('day'), moment(req.body.endDate).endOf('day'))

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        'shop_id': new mongoose.Types.ObjectId(val._id),
        'createdAt': (req.body.startDate == null || req.body.startDate == undefined) ? {
          '$lte': new Date()
        } : {
          // '$gte': moment(req.body.startDate).startOf('day'),
          // '$lte': moment(req.body.endDate).endOf('day')
          '$gte': new Date(new Date(req.body.startDate).setHours(0o0, 0o0, 0o0)),
          '$lt': new Date(new Date(req.body.endDate).setHours(23, 59, 59))
        },
        'status': {
          '$nin': [
            'waiting', 'accepted'
          ]
        },
        'otp_verify': true,
        'test': false
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'bookingpayments',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'payment'
      }
    },
    {
      '$lookup': {
        'from': 'travels',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'travel'
      }
    },
    {
      '$addFields': {
        'distance': {
          '$arrayElemAt': [
            '$travel.distance', 0
          ]
        },
        'distanceVal': {
          '$arrayElemAt': [
            '$travel.distanceVal', 0
          ]
        },
        'duration': {
          '$arrayElemAt': [
            '$travel.duration', 0
          ]
        },
        'durationVal': {
          '$arrayElemAt': [
            '$travel.durationVal', 0
          ]
        },
        'lattitude': {
          '$arrayElemAt': [
            '$travel.lattitude', 0
          ]
        },
        'longitude': {
          '$arrayElemAt': [
            '$travel.longitude', 0
          ]
        },
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$_id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$booking_id', '$$main'
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },
    {
      '$project': {
        'otp_verify': 0,
        'otp': 0
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'booking': [
          {
            '$sort': {
              '_id': -1
            }
          },
          {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'booking': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]
  BookingModel.aggregate(query).then(async (item) => {

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'bookings',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item[0] : { booking: [], total: 0 }
    })
  }).catch((err: Error) => {
    console.log('Shop Query start Date Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const customerBookingQuery = (req: Request, res: Response, next: NextFunction, verify: any) => {
  console.log('Shop', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  console.log('start Date', req.body.startDate, new Date(req.body.startDate), 'endDate', req.body.endDate, new Date(req.body.endDate), moment(req.body.startDate).startOf('day'), moment(req.body.endDate).endOf('day'))

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        'user_id': new mongoose.Types.ObjectId(val._id),
        'createdAt': (req.body.startDate == null || req.body.startDate == undefined) ? {
          '$lte': new Date()
        } : {
          // '$gte': moment(req.body.startDate).startOf('day'),
          // '$lte': moment(req.body.endDate).endOf('day')
          '$gte': new Date(new Date(req.body.startDate).setHours(0o0, 0o0, 0o0)),
          '$lt': new Date(new Date(req.body.endDate).setHours(23, 59, 59))
        },
        'status': {
          '$nin': [
            'waiting', 'accepted'
          ]
        },
        'otp_verify': true,
        'test': false
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'bookingpayments',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'payment'
      }
    },
    {
      '$lookup': {
        'from': 'travels',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'travel'
      }
    },
    {
      '$addFields': {
        'distance': {
          '$arrayElemAt': [
            '$travel.distance', 0
          ]
        },
        'distanceVal': {
          '$arrayElemAt': [
            '$travel.distanceVal', 0
          ]
        },
        'duration': {
          '$arrayElemAt': [
            '$travel.duration', 0
          ]
        },
        'durationVal': {
          '$arrayElemAt': [
            '$travel.durationVal', 0
          ]
        },
        'lattitude': {
          '$arrayElemAt': [
            '$travel.lattitude', 0
          ]
        },
        'longitude': {
          '$arrayElemAt': [
            '$travel.longitude', 0
          ]
        },
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$_id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$booking_id', '$$main'
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },

    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'booking': [
          {
            '$sort': {
              '_id': -1
            }
          },
          {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'booking': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]
  BookingModel.aggregate(query).then(async (item) => {

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'bookings',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item[0] : { booking: [], total: 0 }
    })
  }).catch((err: Error) => {
    console.log('Shop Query start Date Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const CreateBookingOtp = async (req: Request, res: Response, next: NextFunction) => {

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  const BookingData = await BookingModel.findOne({ _id: req.params.id }).populate('service_id')

  if (BookingData == null || BookingData == undefined) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  try {

    const otp = otpGenerate()
    // const mainData = await axios.post('https://heytechemailsms.herokuapp.com/heytech/api/sendSms', {
    //   number: BookingData.mobile,
    //   otp: otp
    // });

    // console.log('Otp Sent', mainData)
    const msgT = await bookingOTPMsg({ service_id: BookingData.service_id, shop_name: BookingData.shop_name, otp: otp, bookingid: BookingData.bookingid, queue: BookingData.queue })
    const smsToken = jwt.sign({ date: new Date(), message: msgT, id: ServiceTemplateId.book, type: 'booking', bookingid: BookingData.bookingid, user_id: [BookingData._id], resend: 'multiple' }, MAIL_SECRET_KEY)

    if (BookingData.mobile == null || BookingData.mobile == undefined) {

      const emailOtp = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'sendOtpMail' : ServiceSmsMail + 'sendOtpMail', {
        email: BookingData.user_email,
        otp: otp
      }, {
        headers: {
          'auth-token': smsToken
        }
      })



      console.log('Otp Sent', emailOtp)

    } else {
      const mobileOtp = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
        number: BookingData.mobile,
        otp: otp
      }, {
        headers: {
          'auth-token': smsToken
        }
      })


      console.log('Otp Sent', mobileOtp)

    }

    const userData = await BookingModel.findOneAndUpdate({
      _id: req.params.id
    }, {
      $set: {
        otp: otp
      }
    }, { new: true })

    console.log('Otp Sent', userData)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        _id: userData._id,
        user_email: userData.user_email,
        shop_email: userData.shop_email,
        mobile: userData.mobile,
        createdAt: userData.createdAt,
      }
    })

  } catch (err) {
    console.log('Create otp Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  }

}

export const getSingleQuery = (req: Request, res: Response, next: NextFunction, verify: any) => {

  var startTime = performance.now()
  const query = [
    {
      '$match': {
        '_id': new mongoose.Types.ObjectId(req.params.id)
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'bookingpayments',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'payment'
      }
    },
    {
      '$lookup': {
        'from': 'travels',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'travel'
      }
    },
    {
      '$addFields': {
        'distance': {
          '$arrayElemAt': [
            '$travel.distance', 0
          ]
        },
        'distanceVal': {
          '$arrayElemAt': [
            '$travel.distanceVal', 0
          ]
        },
        'duration': {
          '$arrayElemAt': [
            '$travel.duration', 0
          ]
        },
        'durationVal': {
          '$arrayElemAt': [
            '$travel.durationVal', 0
          ]
        },
        'lattitude': {
          '$arrayElemAt': [
            '$travel.lattitude', 0
          ]
        },
        'longitude': {
          '$arrayElemAt': [
            '$travel.longitude', 0
          ]
        },
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$_id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$booking_id', '$$main'
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },
    {
      '$project': {
        'otp_verify': 0,
        'otp': 0
      }
    },
  ]
  BookingModel.aggregate(query).then(async (item) => {


    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'bookings',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { booking: item }
    })
  }).catch((err: Error) => {
    console.log('Shop Single Query start Date Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const assignEmployeeIdBooking = (req: Request, res: Response, next: NextFunction, verify: any) => {

  console.log('body', req.body, 'Id', req.params.id, 'shop', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  BookingModel.findOneAndUpdate({ _id: req.params.id, shop_id: val._id }, {
    $set: {
      ...req.body
    }
  }, { new: true }).then((item: any) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Update Booking Employee Assign Failed",
        data: item
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        _id: item._id,
        user_email: item.user_email,
        shop_email: item.shop_email,
        mobile: item.mobile,
        createdAt: item.createdAt,
      }
    })
  }).catch((err: Error) => {
    console.log('Booking Employee Assign Failed Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}

export const createQueueWithLocation = async (req: Request, res: Response, next: NextFunction, verify: any) => {

  console.log('Customer', req.user)
  console.log('Ip Book', req.ip)
  console.log('verify', verify)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  // const promise1 = new Promise((resolve, reject) => {

  //   ShopModel.findOne({ user_information: req.body.shop_id }).then((item) => {
  //     resolve(item)

  //   }).catch((err) => {
  //     reject(err)

  //   })

  // })


  // const promise2 = new Promise((resolve, reject) => {

  //   ServiceModel.aggregate([
  //     {
  //       '$match': {
  //         '_id': new mongoose.Types.ObjectId(req.body.service_id),
  //         'user_id': new mongoose.Types.ObjectId(req.body.shop_id)
  //       }
  //     }, {
  //       '$unwind': {
  //         'path': '$seat'
  //       }
  //     }, {
  //       '$lookup': {
  //         'from': 'bookings',
  //         'let': {
  //           'main': '$user_id',
  //           'service': '$_id',
  //           'seat': '$seat',
  //           'verify': verify
  //         },
  //         'pipeline': [
  //           {
  //             '$sort': {
  //               '_id': -1
  //             }
  //           }, {
  //             '$match': {
  //               '$expr': {
  //                 '$and': [
  //                   {
  //                     '$eq': [
  //                       '$shop_id', '$$main'
  //                     ]
  //                   }, {
  //                     '$eq': [
  //                       '$seat', '$$seat'
  //                     ]
  //                   }, {
  //                     '$eq': [
  //                       '$queue', true
  //                     ]
  //                   }, {
  //                     '$eq': [
  //                       '$test', '$$verify'
  //                     ]
  //                   }, {
  //                     '$gte': [
  //                       '$approximate_date', new Date()
  //                     ]
  //                   }, {
  //                     '$in': [
  //                       '$status', [
  //                         'waiting', 'accepted'
  //                       ]
  //                     ]
  //                   }, {
  //                     '$eq': [
  //                       '$otp_verify', true
  //                     ]
  //                   }
  //                 ]
  //               }
  //             }
  //           }
  //         ],
  //         'as': 'booking'
  //       }
  //     }, {
  //       '$addFields': {
  //         'user_booking': {
  //           '$arrayElemAt': [
  //             '$booking.approximate_date', 0
  //           ]
  //         }
  //       }
  //     }, {
  //       '$group': {
  //         '_id': '$seat',
  //         'data': {
  //           '$first': '$user_booking'
  //         }
  //       }
  //     }, {
  //       '$sort': {
  //         'data': 1
  //       }
  //     }
  //   ]).exec((err, item) => {

  //     if (err) {
  //       console.log('Err', err)
  //       reject(err)
  //       return
  //     }

  //     console.log('sD', item)
  //     resolve(item)

  //   })

  // })

  // Promise.allSettled([promise2]).then(async (item: any) => {


  // }).catch((err) => {
  //   console.log('Promise Booking Create Err', err)
  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })
  // })


  const shopData = val.shopData
  console.log('body', req.body, 'shopData', shopData)

  if (shopData == null || shopData == undefined || Object.keys(shopData).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  // console.log('check', sData, 'other', mData)

  // const file: any = {
  //   approximate_time: parseInt(val.service.waiting_number) * parseInt(req.body.size) + ' ' + val.service.waiting_key,
  //   newtime: (val.service.waiting_key == 'min') ? parseInt(val.service.waiting_number) * parseInt(req.body.size) : parseInt(val.service.waiting_number) * parseInt(req.body.size) * 60
  // }

  // const location: any = (req.body.location == null || req.body.location == undefined) ? 0 : (req.body.location.durationVal == null || req.body.location.durationVal == undefined) ? 0 : req.body.location.durationVal
  // const query = (location == 0) ? [
  //   {
  //     '$match': {
  //       'shop_id': new mongoose.Types.ObjectId(req.body.shop_id),
  //       'queue': true,
  //       'test': verify,
  //       'appointment_date': { '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0)), $lt: new Date(new Date().setHours(23, 59, 59)) },
  //       'status': { '$in': ['waiting', 'accepted'] },
  //       'approximate_date': { '$gte': new Date(val.date.appointment_date) },
  //       'otp_verify': true,
  //       'seat': parseInt((mData.length > 0) ? mData[0]._id : sData[0]._id)
  //     }
  //   },
  //   // {
  //   //   '$lookup': {
  //   //     'from': 'services',
  //   //     'localField': 'service_id',
  //   //     'foreignField': '_id',
  //   //     'as': 'service_id'
  //   //   }
  //   // },
  //   // {
  //   //   '$unwind': {
  //   //     'path': '$service_id'
  //   //   }
  //   // },
  //   // {
  //   //   '$unwind': {
  //   //     'path': '$service_id.seat'
  //   //   }
  //   // },
  //   // {
  //   //   '$match': {
  //   //     'service_id.seat': parseInt((mData.length > 0) ? mData[0]._id : sData[0]._id)
  //   //   }
  //   // },
  //   {
  //     '$sort': {
  //       '_id': -1
  //     }
  //   },
  //   {
  //     '$limit': 1
  //   }
  // ] : [
  //   {
  //     '$match': {
  //       'shop_id': new mongoose.Types.ObjectId(req.body.shop_id),
  //       'queue': true,
  //       'test': verify,
  //       'appointment_date': { '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0)), $lt: new Date(new Date().setHours(23, 59, 59)) },
  //       'status': { '$in': ['waiting', 'accepted'] },
  //       'approximate_date': { '$gte': new Date(val.date.appointment_date) },
  //       'otp_verify': true,
  //       'seat': parseInt((mData.length > 0) ? mData[0]._id : sData[0]._id)
  //     }
  //   },
  //   // {
  //   //   '$lookup': {
  //   //     'from': 'services',
  //   //     'localField': 'service_id',
  //   //     'foreignField': '_id',
  //   //     'as': 'service_id'
  //   //   }
  //   // },
  //   // {
  //   //   '$unwind': {
  //   //     'path': '$service_id'
  //   //   }
  //   // },
  //   // {
  //   //   '$unwind': {
  //   //     'path': '$service_id.seat'
  //   //   }
  //   // },
  //   // {
  //   //   '$match': {
  //   //     'service_id.seat': parseInt((mData.length > 0) ? mData[0]._id : sData[0]._id)
  //   //   }
  //   // },
  //   {
  //     '$sort': {
  //       '_id': -1
  //     }
  //   },
  //   {
  //     '$lookup': {
  //       'from': 'travels',
  //       'localField': '_id',
  //       'foreignField': 'booking_id',
  //       'as': 'travel'
  //     }
  //   },
  //   {
  //     '$addFields': {
  //       'distance': {
  //         '$arrayElemAt': [
  //           '$travel.distance', 0
  //         ]
  //       },
  //       'distanceVal': {
  //         '$arrayElemAt': [
  //           '$travel.distanceVal', 0
  //         ]
  //       },
  //       'duration': {
  //         '$arrayElemAt': [
  //           '$travel.duration', 0
  //         ]
  //       },
  //       'durationVal': {
  //         '$arrayElemAt': [
  //           '$travel.durationVal', 0
  //         ]
  //       }
  //     }
  //   },
  //   {
  //     '$match': {
  //       'durationVal': {
  //         '$lte': location
  //       }
  //     }
  //   },
  //   {
  //     '$sort': {
  //       'durationVal': 1
  //     }
  //   },
  //   {
  //     '$limit': 1
  //   }
  // ]

  // const MainCheck: any = await BookingModel.aggregate(query).exec()

  // console.log('mainD Data', MainCheck)

  // var mainD: any = (MainCheck.length > 0) ? MainCheck[0].approximate_date : createQueueDate(val.date.appointment_date, parseInt((!shopData.waiting_number || shopData.waiting_number == null || shopData.waiting_number == undefined) ? 0 : shopData.waiting_number))

  // console.log('mainD', mainD, createQueueDate(val.date.appointment_date, parseInt((!shopData.waiting_number || shopData.waiting_number == null || shopData.waiting_number == undefined) ? 0 : shopData.waiting_number)))

  var data = req.body

  if (verify == true) {

    console.log('Test')

    data.appointment_date = val.date.appointment_date
    data.appointment_time = val.date.appointment_time
    data.shop_email = shopData.email
    data.shop_name = shopData.shop_name
    data.user_id = val._id
    data.queue = true
    //data.waiting_time = (MainCheck.length) ? parseInt(minusDate(mainD, val.date.appointment_date)) + ' ' + 'min' : shopData.waiting_number
    // data.waiting_number = (MainCheck.length) ? parseInt(minusDate(mainD, val.date.appointment_date)) : parseInt(shopData.waiting_number)
    data.waiting_key = 'min'
    //data.waiting_date = mainD
    //data.approximate_date = createQueueDate(mainD, parseInt((!file.newtime || file.newtime == null || file.newtime == undefined) ? 0 : file.newtime))
    // data.approximate_time = file.approximate_time
    // data.approximate_number = parseInt(file.approximate_time)
    //data.approximate_key = file.approximate_time.replace(/[0-9]/g, '').replace(' ', '')
    data.waiting_verify = true
    // data.end_time = mainD
    data.shop_status = shopData.open
    data.status = (!shopData || shopData == null || shopData == undefined) ? 'waiting' : (shopData.queue == true) ? 'accepted' : 'waiting'
    data.otp_verify = true
    data.test = true
    data.hash = generateHash(data)
  } else {

    console.log('Live')

    data.appointment_date = val.date.appointment_date
    data.appointment_time = val.date.appointment_time
    data.user_id = val._id
    data.queue = true
    //data.waiting_time = (MainCheck.length) ? parseInt(minusDate(mainD, val.date.appointment_date)) + ' ' + 'min' : shopData.waiting_number
    //data.waiting_number = (MainCheck.length) ? parseInt(minusDate(mainD, val.date.appointment_date)) : parseInt(shopData.waiting_number)
    data.waiting_key = 'min'
    //data.waiting_date = mainD
    // data.approximate_date = createQueueDate(mainD, parseInt((!req.body.newtime || req.body.newtime == null || req.body.newtime == undefined) ? 0 : req.body.newtime))
    // data.approximate_time = req.body.approximate_time
    // data.approximate_number = parseInt(req.body.approximate_time)
    // data.approximate_key = req.body.approximate_time.replace(/[0-9]/g, '').replace(' ', '')
    //data.approximate_date = createQueueDate(mainD, parseInt((!file.newtime || file.newtime == null || file.newtime == undefined) ? 0 : file.newtime))
    //data.approximate_time = file.approximate_time
    // data.approximate_number = parseInt(file.approximate_time)
    // data.approximate_key = file.approximate_time.replace(/[0-9]/g, '').replace(' ', '')
    // data.end_time = mainD
    data.shop_status = shopData.open
    data.otp = otpGenerate()
    data.status = (!shopData || shopData == null || shopData == undefined) ? 'waiting' : (shopData.queue == true) ? 'accepted' : 'waiting'
    data.hash = generateHash(data)
  }


  // const appointmentData = await BookingModel.find({
  //   'shop_id': new mongoose.Types.ObjectId(req.body.shop_id),
  //   'queue': false,
  //   'appointment_date': { '$eq': data.approximate_date },
  //   'status': { '$in': ['waiting', 'accepted'] },
  //   'otp_verify': true
  // })
  // console.log('appointmentCheck for adding Queue', appointmentData);

  // if (appointmentData.length > 0) {

  //   return res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Booking Not completed Due to Timing Issue",
  //     data: 'err'
  //   })
  // }

  const BookingData = new BookingModel({ ...data })
  BookingData.save().then(async (item: any) => {

    if (verify == true) {
      console.log('Res Test')

      const ShortUrl = await createShortUrl(item._id)
      const locationD1 = await saveIpDetail(req, item._id, true)

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: {
          _id: item._id,
          user_email: item.user_email,
          shop_email: item.shop_email,
          mobile: item.mobile,
          createdAt: item.createdAt,
        }
      })

    } else {
      console.log('Live')

      const locationD = await saveIpDetail(req, item._id, true)
      const travelD = await TravelModel.create({ ...data.location, booking_id: item._id })
      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: {
          _id: item._id,
          user_email: item.user_email,
          shop_email: item.shop_email,
          mobile: item.mobile,
          createdAt: item.createdAt,
        }
      })

    }

  }).catch((err: Error) => {
    console.log('Booking Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

  // const shopData = await ShopModel.findOne({ user_information: req.body.shop_id })

  // console.log('body', req.body, 'shopData', shopData)

  // if (shopData == null || shopData == undefined || Object.keys(shopData).length == 0) {
  //   return res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "All Fields are Required",
  //     data: 'err'
  //   })
  // }

  // const file: any = {
  //   approximate_time: parseInt(val.service.waiting_number) * parseInt(req.body.size) + ' ' + val.service.waiting_key,
  //   newtime: (val.service.waiting_key == 'min') ? parseInt(val.service.waiting_number) * parseInt(req.body.size) : parseInt(val.service.waiting_number) * parseInt(req.body.size) * 60
  // }

  // const location: any = (req.body.location == null || req.body.location == undefined) ? 0 : (req.body.location.durationVal == null || req.body.location.durationVal == undefined) ? 0 : req.body.location.durationVal
  // const query = (location == 0) ? [
  //   {
  //     '$match': {
  //       'shop_id': new mongoose.Types.ObjectId(req.body.shop_id),
  //       'queue': true,
  //       'test': verify,
  //       'appointment_date': { '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0)), $lt: new Date(new Date().setHours(23, 59, 59)) },
  //       'status': { '$in': ['waiting', 'accepted'] },
  //       'approximate_date': { '$gte': new Date(req.body.appointment_date) },
  //       'otp_verify': true
  //     }
  //   },
  //   {
  //     '$lookup': {
  //       'from': 'services',
  //       'localField': 'service_id',
  //       'foreignField': '_id',
  //       'as': 'service_id'
  //     }
  //   },
  //   {
  //     '$unwind': {
  //       'path': '$service_id'
  //     }
  //   },
  //   {
  //     '$unwind': {
  //       'path': '$service_id.seat'
  //     }
  //   },
  //   {
  //     '$match': {
  //       'service_id.seat': parseInt(val.service.seat[0])
  //     }
  //   },
  //   {
  //     '$sort': {
  //       '_id': -1
  //     }
  //   },
  //   {
  //     '$limit': 1
  //   }
  // ] : [
  //   {
  //     '$match': {
  //       'shop_id': new mongoose.Types.ObjectId(req.body.shop_id),
  //       'queue': true,
  //       'test': verify,
  //       'appointment_date': { '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0)), $lt: new Date(new Date().setHours(23, 59, 59)) },
  //       'status': { '$in': ['waiting', 'accepted'] },
  //       'approximate_date': { '$gte': new Date(req.body.appointment_date) },
  //       'otp_verify': true
  //     }
  //   },
  //   {
  //     '$lookup': {
  //       'from': 'services',
  //       'localField': 'service_id',
  //       'foreignField': '_id',
  //       'as': 'service_id'
  //     }
  //   },
  //   {
  //     '$unwind': {
  //       'path': '$service_id'
  //     }
  //   },
  //   {
  //     '$unwind': {
  //       'path': '$service_id.seat'
  //     }
  //   },
  //   {
  //     '$match': {
  //       'service_id.seat': parseInt(val.service.seat[0])
  //     }
  //   },
  //   {
  //     '$sort': {
  //       '_id': -1
  //     }
  //   },
  //   {
  //     '$lookup': {
  //       'from': 'travels',
  //       'localField': '_id',
  //       'foreignField': 'booking_id',
  //       'as': 'travel'
  //     }
  //   },
  //   {
  //     '$addFields': {
  //       'distance': {
  //         '$arrayElemAt': [
  //           '$travel.distance', 0
  //         ]
  //       },
  //       'distanceVal': {
  //         '$arrayElemAt': [
  //           '$travel.distanceVal', 0
  //         ]
  //       },
  //       'duration': {
  //         '$arrayElemAt': [
  //           '$travel.duration', 0
  //         ]
  //       },
  //       'durationVal': {
  //         '$arrayElemAt': [
  //           '$travel.durationVal', 0
  //         ]
  //       }
  //     }
  //   },
  //   {
  //     '$match': {
  //       'durationVal': {
  //         '$lte': location
  //       }
  //     }
  //   },
  //   {
  //     '$sort': {
  //       'durationVal': 1
  //     }
  //   },
  //   {
  //     '$limit': 1
  //   }
  // ]

  // const MainCheck: any = await BookingModel.aggregate(query).exec()

  // console.log('mainD Data', MainCheck)

  // var mainD: any = (MainCheck.length > 0) ? MainCheck[0].approximate_date : createQueueDate(req.body.appointment_date, parseInt((!shopData.waiting_number || shopData.waiting_number == null || shopData.waiting_number == undefined) ? 0 : shopData.waiting_number))

  // console.log('mainD', mainD, createQueueDate(req.body.appointment_date, parseInt((!shopData.waiting_number || shopData.waiting_number == null || shopData.waiting_number == undefined) ? 0 : shopData.waiting_number)))

  // var data = req.body

  // if (verify == true) {

  //   console.log('Test')

  //   data.appointment_date = val.date.appointment_date
  //   data.appointment_time = val.date.appointment_time
  //   data.shop_email = shopData.email
  //   data.shop_name = shopData.shop_name
  //   data.seat = val.service.seat[0]
  //   data.user_id = val._id
  //   data.queue = true
  //   data.waiting_time = (MainCheck.length) ? parseInt(minusDate(mainD, val.date.appointment_date)) + ' ' + 'min' : shopData.waiting_number
  //   data.waiting_number = (MainCheck.length) ? parseInt(minusDate(mainD, val.date.appointment_date)) : parseInt(shopData.waiting_number)
  //   data.waiting_key = 'min'
  //   data.waiting_date = mainD
  //   data.approximate_date = createQueueDate(mainD, parseInt((!file.newtime || file.newtime == null || file.newtime == undefined) ? 0 : file.newtime))
  //   data.approximate_time = file.approximate_time
  //   data.approximate_number = parseInt(file.approximate_time)
  //   data.approximate_key = file.approximate_time.replace(/[0-9]/g, '').replace(' ', '')
  //   data.waiting_verify = true
  //   data.end_time = mainD
  //   data.status = (!shopData || shopData == null || shopData == undefined) ? 'waiting' : (shopData.queue == true) ? 'accepted' : 'waiting'
  //   data.otp_verify = true
  //   data.test = true
  //   data.hash = generateHash(data)
  // } else {

  //   console.log('Live')

  //   data.user_id = val._id
  //   data.queue = true
  //   data.waiting_time = (MainCheck.length) ? parseInt(minusDate(mainD, req.body.appointment_date)) + ' ' + 'min' : req.body.time
  //   data.waiting_number = (MainCheck.length) ? parseInt(minusDate(mainD, req.body.appointment_date)) : parseInt(req.body.time)
  //   data.waiting_key = 'min'
  //   data.waiting_date = mainD
  //   data.approximate_date = createQueueDate(mainD, parseInt((!req.body.newtime || req.body.newtime == null || req.body.newtime == undefined) ? 0 : req.body.newtime))
  //   data.approximate_time = req.body.approximate_time
  //   data.approximate_number = parseInt(req.body.approximate_time)
  //   data.approximate_key = req.body.approximate_time.replace(/[0-9]/g, '').replace(' ', '')
  //   data.waiting_verify = true
  //   data.end_time = mainD
  //   data.otp = otpGenerate()
  //   data.seat = val.service.seat[0]
  //   data.status = (!shopData || shopData == null || shopData == undefined) ? 'waiting' : (shopData.queue == true) ? 'accepted' : 'waiting'
  //   data.hash = generateHash(data)
  // }


  // const appointmentData = await BookingModel.find({
  //   'shop_id': new mongoose.Types.ObjectId(req.body.shop_id),
  //   'queue': false,
  //   'appointment_date': { '$eq': data.approximate_date },
  //   'status': { '$in': ['waiting', 'accepted'] },
  //   'otp_verify': true
  // })
  // console.log('appointmentCheck for adding Queue', appointmentData);

  // if (appointmentData.length > 0) {

  //   return res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Booking Not completed Due to Timing Issue",
  //     data: 'err'
  //   })
  // }

  // const BookingData = new BookingModel({ ...data })
  // BookingData.save().then(async (item: any) => {

  //   if (verify == true) {
  //     console.log('Res Test')

  //     const ShortUrl = await createShortUrl(item._id)
  //     const locationD1 = await saveIpDetail(req, item._id, true)

  //     res.status(200).json({
  //       status: 'Success',
  //       status_code: res.statusCode,
  //       message: "Request Success",
  //       data: {
  //         _id: item._id,
  //         user_email: item.user_email,
  //         shop_email: item.shop_email,
  //         mobile: item.mobile,
  //         createdAt: item.createdAt,
  //       }
  //     })

  //   } else {
  //     console.log('Live')

  //     const locationD = await saveIpDetail(req, item._id, true)
  //     const travelD = await TravelModel.create({ ...data.location, booking_id: item._id })
  //     res.status(200).json({
  //       status: 'Success',
  //       status_code: res.statusCode,
  //       message: "Request Success",
  //       data: {
  //         _id: item._id,
  //         user_email: item.user_email,
  //         shop_email: item.shop_email,
  //         mobile: item.mobile,
  //         createdAt: item.createdAt,
  //       }
  //     })

  //   }

  // }).catch((err: Error) => {
  //   console.log('Booking Create Err', err)
  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })

  // })


}

export const checkBookingUpdate = (req: Request, res: Response, next: NextFunction) => {

  if (req.params.id == null || req.params.id == undefined || req.params.id == '') {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('body', req.body, 'Id', req.params.id)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  var startTime = performance.now()
  const q1 = [
    {
      $match: {
        '_id': new mongoose.Types.ObjectId(req.params.id),
      }
    },
    {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    },
    {
      '$unwind': {
        'path': '$service_id'
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    },
    {
      '$unwind': {
        'path': '$shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'travels',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'travel'
      }
    },
    {
      '$addFields': {
        'distance': {
          '$arrayElemAt': [
            '$travel.distance', 0
          ]
        },
        'distanceVal': {
          '$arrayElemAt': [
            '$travel.distanceVal', 0
          ]
        },
        'duration': {
          '$arrayElemAt': [
            '$travel.duration', 0
          ]
        },
        'durationVal': {
          '$arrayElemAt': [
            '$travel.durationVal', 0
          ]
        },
        'lattitude': {
          '$arrayElemAt': [
            '$travel.lattitude', 0
          ]
        },
        'longitude': {
          '$arrayElemAt': [
            '$travel.longitude', 0
          ]
        },
      }
    },
  ]
  BookingModel.aggregate(q1).exec(async (err, item) => {
    if (err) {
      console.log('Booking, find err', err)

      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Otp Verification Failed",
        data: err
      })
    }

    if (item.length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Otp Verification Failed",
        data: item
      })
    }


    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: q1,
      collectionName: 'bookings',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    if (item[0].queue == true) {

      var sTime = performance.now()
      const q2 = [
        {
          '$match': {
            '_id': new mongoose.Types.ObjectId(item[0].service_id._id),
            'user_id': new mongoose.Types.ObjectId(item[0].shop_id.user_information)
          }
        }, {
          '$unwind': {
            'path': '$seat'
          }
        }, {
          '$lookup': {
            'from': 'bookings',
            'let': {
              'main': '$user_id',
              'service': '$_id',
              'seat': '$seat',
            },
            'pipeline': [
              {
                '$sort': {
                  '_id': -1
                }
              }, {
                '$match': {
                  '$expr': {
                    '$and': [
                      {
                        '$eq': [
                          '$shop_id', '$$main'
                        ]
                      }, {
                        '$eq': [
                          '$seat', '$$seat'
                        ]
                      }, {
                        '$eq': [
                          '$queue', true
                        ]
                      }, {
                        '$eq': [
                          '$test', false
                        ]
                      }, {
                        '$gte': [
                          '$approximate_date', new Date()
                        ]
                      }, {
                        '$in': [
                          '$status', [
                            'waiting', 'accepted'
                          ]
                        ]
                      }, {
                        '$eq': [
                          '$otp_verify', true
                        ]
                      }
                    ]
                  }
                }
              }
            ],
            'as': 'booking'
          }
        }, {
          '$addFields': {
            'user_booking': {
              '$arrayElemAt': [
                '$booking.approximate_date', 0
              ]
            }
          }
        }, {
          '$group': {
            '_id': '$seat',
            'data': {
              '$first': '$user_booking'
            }
          }
        }, {
          '$sort': {
            'data': 1
          }
        }
      ]
      ServiceModel.aggregate(q2).exec(async (error, value) => {

        if (error) {
          console.log('Booking, find err', err)

          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Otp Verification Failed",
            data: err
          })
        }

        if (value.length == 0) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Otp Verification Failed",
            data: item
          })
        }


        var eTime = (performance.now() - sTime).toFixed();

        const clogs = await LogQueryModel.create({
          queryName: q2,
          collectionName: 'services',
          startTime: sTime,
          endTime: parseInt(eTime)
        })

        console.log('Booking Data', item[0])
        console.log('Server Data', value)

        const sData = value
        const mData = value.filter((cc: any) => cc.data == null)

        const file: any = {
          approximate_time: parseInt(item[0].service_id.waiting_number) * parseInt(item[0].size) + ' ' + item[0].service_id.waiting_key,
          newtime: (item[0].service_id.waiting_key == 'min') ? parseInt(item[0].service_id.waiting_number) * parseInt(item[0].size) : parseInt(item[0].service_id.waiting_number) * parseInt(item[0].size) * 60
        }

        const location: any = (item[0].travel.length == 0) ? 0 : (item[0].travel[0].durationVal == null || item[0].travel[0].durationVal == undefined) ? 0 : item[0].travel[0].durationVal

        const text: any = (mData.length > 0) ? mData[0]._id : sData[0]._id

        console.log('text', text)

        var aTime = performance.now()

        const query = (location == 0) ? [
          {
            '$match': {
              'shop_id': new mongoose.Types.ObjectId(item[0].shop_id.user_information),
              'queue': true,
              'test': false,
              'appointment_date': { '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0)), $lt: new Date(new Date().setHours(23, 59, 59)) },
              'status': { '$in': ['waiting', 'accepted'] },
              'approximate_date': { '$gte': new Date(item[0].appointment_date) },
              'otp_verify': true,
              'seat': text
            }
          },
          {
            '$sort': {
              'approximate_date': -1
            }
          },
          {
            '$limit': 1
          }
        ] : [
          {
            '$match': {
              'shop_id': new mongoose.Types.ObjectId(item[0].shop_id.user_information),
              'queue': true,
              'test': false,
              'appointment_date': { '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0)), $lt: new Date(new Date().setHours(23, 59, 59)) },
              'status': { '$in': ['waiting', 'accepted'] },
              'approximate_date': { '$gte': new Date(item[0].appointment_date) },
              'otp_verify': true,
              'seat': text
            }
          },
          {
            '$sort': {
              'approximate_date': -1
            }
          },
          {
            '$lookup': {
              'from': 'travels',
              'localField': '_id',
              'foreignField': 'booking_id',
              'as': 'travel'
            }
          },
          {
            '$addFields': {
              'distance': {
                '$arrayElemAt': [
                  '$travel.distance', 0
                ]
              },
              'distanceVal': {
                '$arrayElemAt': [
                  '$travel.distanceVal', 0
                ]
              },
              'duration': {
                '$arrayElemAt': [
                  '$travel.duration', 0
                ]
              },
              'durationVal': {
                '$arrayElemAt': [
                  '$travel.durationVal', 0
                ]
              }
            }
          },
          {
            '$match': {
              'durationVal': {
                '$lte': location
              }
            }
          },
          {
            '$sort': {
              'durationVal': 1
            }
          },
          {
            '$limit': 1
          }
        ]

        const MainCheck: any = await BookingModel.aggregate(query).exec()



        var bTime = (performance.now() - aTime).toFixed();

        const clogw = await LogQueryModel.create({
          queryName: query,
          collectionName: 'bookings',
          startTime: aTime,
          endTime: parseInt(bTime)
        })

        console.log('mainD Data', MainCheck)

        var mainD: any = (MainCheck.length > 0) ? MainCheck[0].approximate_date : createQueueDate(item[0].appointment_date, parseInt((!item[0].shop_id.waiting_number || item[0].shop_id.waiting_number == null || item[0].shop_id.waiting_number == undefined) ? 0 : item[0].shop_id.waiting_number))

        console.log('mainD', mainD, createQueueDate(item[0].appointment_date, parseInt((!item[0].shop_id.waiting_number || item[0].shop_id.waiting_number == null || item[0].shop_id.waiting_number == undefined) ? 0 : item[0].shop_id.waiting_number)))

        var data: any = {}
        data.waiting_time = (MainCheck.length) ? parseInt(minusDate(mainD, item[0].appointment_date)) + ' ' + 'min' : item[0].shop_id.waiting_number
        data.waiting_number = (MainCheck.length) ? parseInt(minusDate(mainD, item[0].appointment_date)) : parseInt(item[0].shop_id.waiting_number)
        data.waiting_key = 'min'
        data.waiting_date = mainD
        data.approximate_date = createQueueDate(mainD, parseInt((!file.newtime || file.newtime == null || file.newtime == undefined) ? 0 : file.newtime))
        data.approximate_time = file.approximate_time
        data.approximate_number = parseInt(file.approximate_time)
        data.approximate_key = file.approximate_time.replace(/[0-9]/g, '').replace(' ', '')
        data.end_time = mainD
        data.waiting_verify = true
        data.seat = text

        console.log('data set', data)

        BookingModel.findOneAndUpdate({ _id: req.params.id, otp: parseInt(req.body.otp) }, {
          $set: {
            ...data,
            otp_verify: true
          }
        }, { new: true }).then(async (item: any) => {

          console.log('test Verify Booking', item, item == null, item == undefined)

          if (item == null || item == undefined) {

            console.log('check', otpPass, parseInt(req.body.otp), otpPass == parseInt(req.body.otp))

            if (otpPass == parseInt(req.body.otp)) {

              BookingModel.findOneAndUpdate({ _id: req.params.id }, {
                $set: {
                  ...data,
                  otp_verify: true
                }
              }, { new: true }).then(async (val: any) => {
                const ShortUrl = await createShortUrl(val._id)

                return res.status(200).json({
                  status: 'Success',
                  status_code: res.statusCode,
                  message: "Request Success",
                  data: val
                })
              }).catch((err: Error) => {
                console.log('Booking Otp Verification Err', err)
                res.status(400).json({
                  status: 'Failed',
                  status_code: res.statusCode,
                  message: "Request Failed",
                  data: err
                })

              })



            } else {

              return res.status(400).json({
                status: 'Failed',
                status_code: res.statusCode,
                message: "Otp Verification Failed",
                data: item
              })

            }
          } else {

            const ShortUrl = await createShortUrl(item._id)

            res.status(200).json({
              status: 'Success',
              status_code: res.statusCode,
              message: "Request Success",
              data: item
            })
          }

        }).catch((err: Error) => {
          console.log('Booking Otp Verification Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })

        })


      })
    } else {

      BookingModel.findOneAndUpdate({ _id: req.params.id, otp: parseInt(req.body.otp) }, {
        $set: {
          otp_verify: true
        }
      }, { new: true }).then(async (item: any) => {

        console.log('test Verify Booking', item, item == null, item == undefined)

        if (item == null || item == undefined) {

          console.log('check', otpPass, parseInt(req.body.otp), otpPass == parseInt(req.body.otp))

          if (otpPass == parseInt(req.body.otp)) {

            BookingModel.findOneAndUpdate({ _id: req.params.id }, {
              $set: {
                otp_verify: true
              }
            }, { new: true }).then(async (val: any) => {
              const ShortUrl = await createShortUrl(val._id)

              return res.status(200).json({
                status: 'Success',
                status_code: res.statusCode,
                message: "Request Success",
                data: val
              })
            }).catch((err: Error) => {
              console.log('Booking Otp Verification Err', err)
              res.status(400).json({
                status: 'Failed',
                status_code: res.statusCode,
                message: "Request Failed",
                data: err
              })

            })



          } else {

            return res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Otp Verification Failed",
              data: item
            })

          }
        } else {

          const ShortUrl = await createShortUrl(item._id)

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: item
          })
        }

      }).catch((err: Error) => {
        console.log('Booking Otp Verification Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })

      })

    }

  })


}

export const checkBookingQueueAdd = async (req: Request, res: Response, next: NextFunction) => {

  if (req.params.id == null || req.params.id == undefined || req.params.id == '') {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }
  console.log('body', req.body, 'Id', req.params.id)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }
  var data = req.body
  axios.post(`${config.has('QUEUE_URL') ? config.get('QUEUE_URL') : QUEUE_URL}api/add/${req.params.id}`, data).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('queue add err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  })

  // BookingModel.aggregate([
  //   {
  //     $match: {
  //       '_id': new mongoose.Types.ObjectId(req.params.id),
  //     }
  //   },
  //   {
  //     '$lookup': {
  //       'from': 'shops',
  //       'localField': 'shop_id',
  //       'foreignField': 'user_information',
  //       'as': 'shop_id'
  //     }
  //   },
  //   {
  //     '$unwind': {
  //       'path': '$shop_id'
  //     }
  //   },
  // ]).exec(async (err, item) => {
  //   if (err) {
  //     console.log('bookErr', err)
  //     return res.status(400).json({
  //       status: 'Failed',
  //       status_code: res.statusCode,
  //       message: "All Fields are Required",
  //       data: err
  //     })
  //   }

  //   if (item.length == 0) {
  //     return res.status(400).json({
  //       status: 'Failed',
  //       status_code: res.statusCode,
  //       message: "All Fields are Required",
  //       data: 'err'
  //     })
  //   }
  //   const add = getQueue(item[0].shop_id.shop_name)
  //   add.add({ id: req.params.id, body: req.body })
  //   res.status(200).json({
  //     status: 'Success',
  //     status_code: res.statusCode,
  //     message: "Request Success",
  //     data: 'Success'
  //   })

  // })

}

export const checkBookingQueue = (id: any, body: any, cb: any) => {

  // if (id == null || id == undefined || id == '') {
  //   return cb(400, {
  //     status: 'Failed',
  //     status_code: 400,
  //     message: "Request Failed",
  //     data: 'err'
  //   }, null)
  // }


  // console.log('body', body, 'Id', id)

  // if (body == null || body == undefined || Object.keys(body).length == 0) {
  //   return cb(400, {
  //     status: 'Failed',
  //     status_code: 400,
  //     message: "All Fields are Required",
  //     data: 'err'
  //   }, null)
  // }

  var aTime = performance.now()
  const q1 = [
    {
      $match: {
        '_id': new mongoose.Types.ObjectId(id),
      }
    },
    {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    },
    {
      '$unwind': {
        'path': '$service_id'
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    },
    {
      '$unwind': {
        'path': '$shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'travels',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'travel'
      }
    },
    {
      '$addFields': {
        'distance': {
          '$arrayElemAt': [
            '$travel.distance', 0
          ]
        },
        'distanceVal': {
          '$arrayElemAt': [
            '$travel.distanceVal', 0
          ]
        },
        'duration': {
          '$arrayElemAt': [
            '$travel.duration', 0
          ]
        },
        'durationVal': {
          '$arrayElemAt': [
            '$travel.durationVal', 0
          ]
        },
        'lattitude': {
          '$arrayElemAt': [
            '$travel.lattitude', 0
          ]
        },
        'longitude': {
          '$arrayElemAt': [
            '$travel.longitude', 0
          ]
        },
      }
    },
  ]
  BookingModel.aggregate(q1).exec(async (err, item) => {
    if (err) {
      console.log('Booking, find err', err)

      return cb(400, {
        status: 'Failed',
        status_code: 400,
        message: "Otp Verification Failed",
        data: err
      }, null)
    }

    if (item.length == 0) {
      return cb(400, {
        status: 'Failed',
        status_code: 400,
        message: "Otp Verification Failed",
        data: item
      }, null)
    }

    var bTime = (performance.now() - aTime).toFixed();

    const clogw = await LogQueryModel.create({
      queryName: q1,
      collectionName: 'bookings',
      startTime: aTime,
      endTime: parseInt(bTime)
    })

    var startTime = performance.now()
    const q2 = [
      {
        '$match': {
          '_id': new mongoose.Types.ObjectId(item[0].service_id._id),
          'user_id': new mongoose.Types.ObjectId(item[0].shop_id.user_information)
        }
      }, {
        '$unwind': {
          'path': '$seat'
        }
      }, {
        '$lookup': {
          'from': 'bookings',
          'let': {
            'main': '$user_id',
            'service': '$_id',
            'seat': '$seat',
          },
          'pipeline': [
            {
              '$sort': {
                '_id': -1
              }
            }, {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$seat', '$$seat'
                      ]
                    }, {
                      '$eq': [
                        '$queue', true
                      ]
                    }, {
                      '$eq': [
                        '$test', false
                      ]
                    }, {
                      '$gte': [
                        '$approximate_date', new Date()
                      ]
                    }, {
                      '$in': [
                        '$status', [
                          'waiting', 'accepted'
                        ]
                      ]
                    }, {
                      '$eq': [
                        '$otp_verify', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'booking'
        }
      }, {
        '$addFields': {
          'user_booking': {
            '$arrayElemAt': [
              '$booking.approximate_date', 0
            ]
          }
        }
      }, {
        '$group': {
          '_id': '$seat',
          'data': {
            '$first': '$user_booking'
          }
        }
      }, {
        '$sort': {
          'data': 1
        }
      }
    ]
    ServiceModel.aggregate(q2).exec(async (error, value) => {

      if (error) {
        console.log('Booking, find err', error)

        return cb(400, {
          status: 'Failed',
          status_code: 400,
          message: "Otp Verification Failed",
          data: error
        }, null)
      }

      if (value.length == 0) {
        return cb(400, {
          status: 'Failed',
          status_code: 400,
          message: "Otp Verification Failed",
          data: item
        }, null)
      }


      var endTime = (performance.now() - startTime).toFixed();

      const clogsw = await LogQueryModel.create({
        queryName: q2,
        collectionName: 'services',
        startTime: startTime,
        endTime: parseInt(endTime)
      })

      console.log('Booking Data', item[0])
      console.log('Server Data', value)

      const sData = value
      const mData = value.filter((cc: any) => cc.data == null)

      const file: any = {
        approximate_time: parseInt(item[0].service_id.waiting_number) * parseInt(item[0].size) + ' ' + item[0].service_id.waiting_key,
        newtime: (item[0].service_id.waiting_key == 'min') ? parseInt(item[0].service_id.waiting_number) * parseInt(item[0].size) : parseInt(item[0].service_id.waiting_number) * parseInt(item[0].size) * 60
      }

      const location: any = (item[0].travel.length == 0) ? 0 : (item[0].travel[0].durationVal == null || item[0].travel[0].durationVal == undefined) ? 0 : item[0].travel[0].durationVal

      const text: any = (mData.length > 0) ? mData[0]._id : sData[0]._id

      console.log('text', text)

      var wTime = performance.now()

      const query = (location == 0) ? [
        {
          '$match': {
            'shop_id': new mongoose.Types.ObjectId(item[0].shop_id.user_information),
            'queue': true,
            'test': false,
            'appointment_date': { '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0)), $lt: new Date(new Date().setHours(23, 59, 59)) },
            'status': { '$in': ['waiting', 'accepted'] },
            'approximate_date': { '$gte': new Date(item[0].appointment_date) },
            'otp_verify': true,
            'seat': text
          }
        },
        {
          '$sort': {
            'approximate_date': -1
          }
        },
        {
          '$limit': 1
        }
      ] : [
        {
          '$match': {
            'shop_id': new mongoose.Types.ObjectId(item[0].shop_id.user_information),
            'queue': true,
            'test': false,
            'appointment_date': { '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0)), $lt: new Date(new Date().setHours(23, 59, 59)) },
            'status': { '$in': ['waiting', 'accepted'] },
            'approximate_date': { '$gte': new Date(item[0].appointment_date) },
            'otp_verify': true,
            'seat': text
          }
        },
        {
          '$sort': {
            'approximate_date': -1
          }
        },
        {
          '$lookup': {
            'from': 'travels',
            'localField': '_id',
            'foreignField': 'booking_id',
            'as': 'travel'
          }
        },
        {
          '$addFields': {
            'distance': {
              '$arrayElemAt': [
                '$travel.distance', 0
              ]
            },
            'distanceVal': {
              '$arrayElemAt': [
                '$travel.distanceVal', 0
              ]
            },
            'duration': {
              '$arrayElemAt': [
                '$travel.duration', 0
              ]
            },
            'durationVal': {
              '$arrayElemAt': [
                '$travel.durationVal', 0
              ]
            }
          }
        },
        {
          '$match': {
            'durationVal': {
              '$lte': location
            }
          }
        },
        {
          '$sort': {
            'durationVal': 1
          }
        },
        {
          '$limit': 1
        }
      ]

      const MainCheck: any = await BookingModel.aggregate(query).exec()

      var rTime = (performance.now() - wTime).toFixed();

      const clogswa = await LogQueryModel.create({
        queryName: query,
        collectionName: 'bookings',
        startTime: wTime,
        endTime: parseInt(rTime)
      })

      console.log('mainD Data', MainCheck)

      var mainD: any = (MainCheck.length > 0) ? MainCheck[0].approximate_date : createQueueDate(item[0].appointment_date, parseInt((!item[0].shop_id.waiting_number || item[0].shop_id.waiting_number == null || item[0].shop_id.waiting_number == undefined) ? 0 : item[0].shop_id.waiting_number))

      console.log('mainD', mainD, createQueueDate(item[0].appointment_date, parseInt((!item[0].shop_id.waiting_number || item[0].shop_id.waiting_number == null || item[0].shop_id.waiting_number == undefined) ? 0 : item[0].shop_id.waiting_number)))

      var data: any = {}
      data.waiting_time = (MainCheck.length) ? parseInt(minusDate(mainD, item[0].appointment_date)) + ' ' + 'min' : item[0].shop_id.waiting_number
      data.waiting_number = (MainCheck.length) ? parseInt(minusDate(mainD, item[0].appointment_date)) : parseInt(item[0].shop_id.waiting_number)
      data.waiting_key = 'min'
      data.waiting_date = mainD
      data.approximate_date = createQueueDate(mainD, parseInt((!file.newtime || file.newtime == null || file.newtime == undefined) ? 0 : file.newtime))
      data.approximate_time = file.approximate_time
      data.approximate_number = parseInt(file.approximate_time)
      data.approximate_key = file.approximate_time.replace(/[0-9]/g, '').replace(' ', '')
      data.end_time = mainD
      data.waiting_verify = true
      data.seat = text

      console.log('data set', data)

      BookingModel.findOneAndUpdate({ _id: id, otp: parseInt(body.otp) }, {
        $set: {
          ...data,
          otp_verify: true
        }
      }, { new: true }).then(async (item: any) => {

        console.log('test Verify Booking', item, item == null, item == undefined)

        if (item == null || item == undefined) {

          console.log('check', otpPass, parseInt(body.otp), otpPass == parseInt(body.otp))

          if (otpPass == parseInt(body.otp)) {

            BookingModel.findOneAndUpdate({ _id: id }, {
              $set: {
                ...data,
                otp_verify: true
              }
            }, { new: true }).then(async (val: any) => {
              const ShortUrl = await createShortUrl(val._id)

              return cb(200, null, {
                status: 'Success',
                status_code: 200,
                message: "Request Success",
                data: val
              })
            }).catch((err: Error) => {
              console.log('Booking Otp Verification Err', err)
              cb(400, {
                status: 'Failed',
                status_code: 400,
                message: "Request Failed",
                data: err
              }, null)

            })



          } else {

            return cb(400, {
              status: 'Failed',
              status_code: 400,
              message: "Otp Verification Failed",
              data: item
            }, null)

          }
        } else {

          const ShortUrl = await createShortUrl(item._id)

          cb(200, null, {
            status: 'Success',
            status_code: 200,
            message: "Request Success",
            data: item
          })
        }

      }).catch((err: Error) => {
        console.log('Booking Otp Verification Err', err)
        cb(400, {
          status: 'Failed',
          status_code: 400,
          message: "Request Failed",
          data: err
        }, null)

      })


    })

  })


}

export const completeAllBookings = (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  const val: any = req.user
  BookingModel.updateMany({ shop_id: val._id, queue: req.body.status, expired: false }, {
    $set: {
      status: 'completed',
      expired: true
    }
  }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Primisee Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

const createDate = (date: Date, time: any) => {
  var now = new Date(date);
  now.setDate(now.getDate())
  now.setHours(time);
  now.setMinutes(0);
  now.setMilliseconds(0);
  return now
}

// const changeData = await BookingModel.updateMany({ queue: null }, {
//   $set: {
//     queue: false
//   }
// })

// console.log('Shop', changeData)

const createQueueDate = (date: any, time: any) => {
  var now = new Date(date);
  console.log('now', now, date, time)
  now.setMinutes(now.getMinutes() + time);
  now.setMilliseconds(0);
  return now
}

const minusDate = (start: Date, end: Date) => {
  var now = new Date(start).getTime();
  var later = new Date(end).getTime();
  var result: any = (now - later) / 60000;
  return (result > 0) ? result : 0
}
