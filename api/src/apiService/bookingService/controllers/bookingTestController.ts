import { NextFunction, Request, Response } from 'express';
import moment from 'moment';
import { generateHash, createDate, createQueueDate, minusDate } from '../../../config/keys';
import { BookingModel } from '../../../Models/Booking'
import { createShortUrl } from '../../../modules/ShortUrl';
import { ShopModel } from '../../../Models/User'
import { saveIpDetail } from '../../../config/IpDetail';
import mongoose from 'mongoose'

export const CreateBookingCustomer = async (req: Request, res: Response, next: NextFunction) => {

  console.log('Customer', req.user)
  console.log('Customer', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  const shopData = await ShopModel.findOne({ user_information: req.body.shop_id })

  console.log('body', req.body, 'shopData', shopData)

  if (shopData == null || shopData == undefined || Object.keys(shopData).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  var data = req.body

  data.shop_email = shopData.email
  data.shop_name = shopData.shop_name
  data.user_id = val._id
  data.appointment_date = createDate(new Date(req.body.appointment_date), parseInt(req.body.appointment_time.replace(/\-.*/, '')))
  data.end_time = createDate(new Date(req.body.appointment_date), parseInt(req.body.appointment_time.replace(/\-.*/, '')))
  data.status = (!shopData || shopData == null || shopData == undefined) ? 'waiting' : (shopData.appointment == true) ? 'accepted' : 'waiting'
  data.otp_verify = true
  data.test = true
  data.hash = generateHash(data)
  data.seat = val.service.seat

  const BookingData = new BookingModel({ ...data })
  BookingData.save().then(async (item: any) => {

    const ShortUrl = await createShortUrl(item._id)
    const locationD = await saveIpDetail(req, item._id, false)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        _id: item._id,
        user_email: item.user_email,
        shop_email: item.shop_email,
        mobile: item.mobile,
        createdAt: item.createdAt,
      }
    })
  }).catch((err: Error) => {
    console.log('Booking Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })


}

export const CreateQueueBookingCustomer = async (req: Request, res: Response, next: NextFunction) => {

  console.log('Customer', req.user)
  console.log('Customer', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  const shopData = await ShopModel.findOne({ user_information: req.body.shop_id })

  console.log('body', req.body, 'shopData', shopData)

  if (shopData == null || shopData == undefined || Object.keys(shopData).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  const file: any = {
    approximate_time: parseInt(val.service.waiting_number) * parseInt(req.body.size) + ' ' + val.service.waiting_key,
    newtime: (val.service.waiting_key == 'min') ? parseInt(val.service.waiting_number) * parseInt(req.body.size) : parseInt(val.service.waiting_number) * parseInt(req.body.size) * 60
  }

  const MainCheck: any = await BookingModel.aggregate([
    {
      '$match': {
        'shop_id': new mongoose.Types.ObjectId(req.body.shop_id),
        'queue': true,
        'test': true,
        'appointment_date': { '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0)), $lt: new Date(new Date().setHours(23, 59, 59)) },
        'status': { '$in': ['waiting', 'accepted'] },
        'approximate_date': { '$gte': new Date(val.date.appointment_date) },
        'otp_verify': true
      }
    },
    {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    },
    {
      '$unwind': {
        'path': '$service_id'
      }
    },
    {
      '$match': {
        'service_id.seat': parseInt(val.service.seat)
      }
    },
    {
      '$sort': {
        '_id': -1
      }
    },
    {
      '$limit': 1
    }
  ]).exec()

  console.log('mainD Data', MainCheck)

  var mainD: any = (MainCheck.length > 0) ? MainCheck[0].approximate_date : createQueueDate(val.date.appointment_date, parseInt((!shopData.waiting_number || shopData.waiting_number == null || shopData.waiting_number == undefined) ? 0 : shopData.waiting_number))

  console.log('mainD', mainD, createQueueDate(val.date.appointment_date, parseInt((!shopData.waiting_number || shopData.waiting_number == null || shopData.waiting_number == undefined) ? 0 : shopData.waiting_number)))

  var data = req.body
  data.appointment_date = val.date.appointment_date
  data.appointment_time = val.date.appointment_time
  data.shop_email = shopData.email
  data.shop_name = shopData.shop_name
  data.seat = val.service.seat
  data.user_id = val._id
  data.queue = true
  data.waiting_time = (MainCheck.length) ? parseInt(minusDate(mainD, val.date.appointment_date)) + ' ' + 'min' : shopData.waiting_number
  data.waiting_number = (MainCheck.length) ? parseInt(minusDate(mainD, val.date.appointment_date)) : parseInt(shopData.waiting_number)
  data.waiting_key = 'min'
  data.waiting_date = mainD
  data.approximate_date = createQueueDate(mainD, parseInt((!file.newtime || file.newtime == null || file.newtime == undefined) ? 0 : file.newtime))
  data.approximate_time = file.approximate_time
  data.approximate_number = parseInt(file.approximate_time)
  data.approximate_key = file.approximate_time.replace(/[0-9]/g, '').replace(' ', '')
  data.waiting_verify = true
  data.end_time = mainD
  data.status = (!shopData || shopData == null || shopData == undefined) ? 'waiting' : (shopData.queue == true) ? 'accepted' : 'waiting'
  data.otp_verify = true
  data.test = true
  data.hash = generateHash(data)

  const BookingData = new BookingModel({ ...data })
  BookingData.save().then(async (item: any) => {

    const ShortUrl = await createShortUrl(item._id)
    const locationD = await saveIpDetail(req, item._id, true)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        _id: item._id,
        user_email: item.user_email,
        shop_email: item.shop_email,
        mobile: item.mobile,
        createdAt: item.createdAt,
      }
    })
  }).catch((err: Error) => {
    console.log('Booking Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })


}