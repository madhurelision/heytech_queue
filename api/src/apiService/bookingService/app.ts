import express, { Express } from 'express';
import routes from './routes';
import testRoutes from './testRoutes';

const bookingApp = (app: Express) => {

  app.use('/api/booking', routes)
  app.use('/test/booking', testRoutes)

};

export default bookingApp;
