import { Router,Request, Response ,NextFunction } from 'express';
import { BookingQueueVerify, testBookingQueueVerify } from '../../config/verifyToken';
import { createQueueWithLocation, CreateBookingCustomer } from './controllers/bookingController';

const router = Router();

/// Booking
router.post('/createBooking', BookingQueueVerify, (req: Request, res: Response, next: NextFunction) => { CreateBookingCustomer(req, res, next, true) })
router.post('/createBookingQueue', testBookingQueueVerify, (req: Request, res: Response, next: NextFunction) => { createQueueWithLocation(req, res, next, true) })

export default router;
