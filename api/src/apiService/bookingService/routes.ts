import { Router } from 'express';
import { BookingQueueVerify, BookingVerify, newBookingQueueVerify } from '../../config/verifyToken';
import { CreateBookingCustomer, CreateQueueBookingCustomer, getSingleBooking, verifyBookingOtp, CreateBookingOtp, createQueueWithLocation, checkBookingUpdate, checkBookingQueueAdd } from './controllers/bookingController';

const router = Router();

/// Booking
router.post('/createBooking', BookingQueueVerify, (req, res, next) => { CreateBookingCustomer(req, res, next, false) })
//router.post('/createBookingQueue', BookingQueueVerify, CreateQueueBookingCustomer)
router.post('/createBookingQueue', newBookingQueueVerify, (req, res, next) => { createQueueWithLocation(req, res, next, false) })
router.post('/verifyBooking/:id', checkBookingUpdate)
router.get('/getSingleBooking/:id', getSingleBooking)
router.post('/generateOtpBooking/:id', CreateBookingOtp)
router.post('/verifyQueueBooking/:id', checkBookingQueueAdd)

export default router;
