import { Router } from 'express';
import { AdminVerify, shopCreateVerify } from '../../config/verifyToken';
import { CreateAbout, deleteAbout, getAllAboutList, EditAbout, updateAboutStatus, changeMultipleSsrAboutStatusByAdmin } from './controllers/aboutController';
import { changeAffiliateStatus, changeMultipleSsrAffiliateStatusByAdmin, CreateAffiliate, deleteAffiliate, getAllAffiliate, getAllAffiliateList, updateAffiliate } from './controllers/affiliateController';
import { changeBlogStatus, changeMultipleSsrBlogStatusByAdmin, CreateBlog, deleteBlog, getAllBlog, getAllBlogList, getSingleBlog, updateBlog } from './controllers/blogController';
import { changeCatalogStatus, changeMultipleSsrCatalogStatusByAdmin, CreateCatalog, deleteCatalog, getAllCatalog, getAllCatalogList, updateCatalog } from './controllers/catalogController';
import { changeCommentStatus, createComment, getAllCommentList, replyComment } from './controllers/commentController';
import { createContact, deleteContact, getAllContactList } from './controllers/contactController';
import { changeFeatureStatus, changeMultipleSsrFeatureStatusByAdmin, CreateFeature, deleteFeature, EditFeature, getAllFeatureList, getAllFeatures } from './controllers/featureController';
import { changeHomeStatus, changeMultipleSsrHomeStatusByAdmin, CreateHome, deleteHome, getAllHome, getAllHomeList, updateHome } from './controllers/homeController';
import { CreateLogo, deleteLogo, getAllLogoList, updateLogo, updateLogoStatus } from './controllers/logoController';
import { createMainPage, updateMainPage } from './controllers/mainController';
import { changeMenuStatus, changeMultipleSsrMenuStatusByAdmin, CreateMenu, deleteMenu, EditMenu, getAllMenuList, getAllMenus } from './controllers/menuController';
import { getAllMetaList, getBlogData } from './controllers/metaController';
import { createNewsletter, deleteNewsletter, getAllNewsletterList } from './controllers/newsletterConroller';
import { getAllOrderList, getAllPaymentList } from './controllers/orderController';
import { CreatePackageFeature, deletePackageFeature, EditPackageFeature, getAllPackageFeatureList } from './controllers/packageFeatureController';
import { RazorpayCompletePayment, RazorpayCreatePayment, StripePayment } from './controllers/paymentController';
import { changeMultipleSsrProductStatusByAdmin, changeProductStatus, CreateProduct, deleteProduct, getAllProduct, getAllProductList, updateProduct } from './controllers/productController';
import { changeMultipleSsrRazorpayStatusByAdmin, CreateRazorpay, deleteRazorpay, EditRazorpay, getAllRazorpayList, updateRazorpayStatus } from './controllers/razorpayController';
import { changeMultipleSsrReviewStatusByAdmin, changeReviewStatus, CreateReview, deleteReview, getAllReview, getAllReviewList, updateReview } from './controllers/reviewController';
import { changeMultipleSsrStripeStatusByAdmin, CreateStripe, deleteStripe, EditStripe, getAllStripeList, updateStripeStatus } from './controllers/stripeController';
import { createUserS, deleteUserS, getAllUserSList } from './controllers/userController';
import { createUserFeaturePage, updateUserFeaturePage } from './controllers/userfeatureController';

const router = Router();

// Menu
router.get('/menuList/:skip/:limit', AdminVerify, getAllMenuList)
router.post('/createMenu', AdminVerify, CreateMenu)
router.post('/editMenu/:id', AdminVerify, EditMenu)
router.get('/deleteMenu/:id', AdminVerify, deleteMenu)
router.post('/changeStatusMenu/:id', AdminVerify, changeMenuStatus)
router.get('/getAllMenu', getAllMenus)
router.post('/changeMultiSsrMenuStatus', AdminVerify, changeMultipleSsrMenuStatusByAdmin)

// Feature
router.get('/featureList/:skip/:limit', AdminVerify, getAllFeatureList)
router.post('/createFeature', AdminVerify, CreateFeature)
router.post('/editFeature/:id', AdminVerify, EditFeature)
router.get('/deleteFeature/:id', AdminVerify, deleteFeature)
router.post('/changeStatusFeature/:id', AdminVerify, changeFeatureStatus)
router.get('/getAllFeature', getAllFeatures)
router.post('/changeMultiSsrFeatureStatus', AdminVerify, changeMultipleSsrFeatureStatusByAdmin)

// Home
router.get('/homeList/:skip/:limit', AdminVerify, getAllHomeList)
router.post('/createHome', AdminVerify, CreateHome)
router.post('/editHome/:id', AdminVerify, updateHome)
router.get('/deleteHome/:id', AdminVerify, deleteHome)
router.post('/changeStatusHome/:id', AdminVerify, changeHomeStatus)
router.get('/getAllHome', getAllHome)
router.post('/changeMultiSsrHomeStatus', AdminVerify, changeMultipleSsrHomeStatusByAdmin)

// Product
router.get('/productList/:skip/:limit', AdminVerify, getAllProductList)
router.post('/createProduct', AdminVerify, CreateProduct)
router.post('/editProduct/:id', AdminVerify, updateProduct)
router.get('/deleteProduct/:id', AdminVerify, deleteProduct)
router.post('/changeStatusProduct/:id', AdminVerify, changeProductStatus)
router.get('/getAllProduct', getAllProduct)
router.post('/changeMultiSsrProductStatus', AdminVerify, changeMultipleSsrProductStatusByAdmin)

// Review
router.get('/reviewList/:skip/:limit', AdminVerify, getAllReviewList)
router.post('/createReview', AdminVerify, CreateReview)
router.post('/editReview/:id', AdminVerify, updateReview)
router.get('/deleteReview/:id', AdminVerify, deleteReview)
router.post('/changeStatusReview/:id', AdminVerify, changeReviewStatus)
router.get('/getAllReview', getAllReview)
router.post('/changeMultiSsrReviewStatus', AdminVerify, changeMultipleSsrReviewStatusByAdmin)

router.get('/getAllPage', getAllMetaList)

// Main
router.get('/createMain', AdminVerify, createMainPage)
router.post('/editMain/:id', AdminVerify, updateMainPage)

// About
router.get('/aboutList/:skip/:limit', AdminVerify, getAllAboutList)
router.post('/createAbout', AdminVerify, CreateAbout)
router.post('/editAbout/:id', AdminVerify, EditAbout)
router.post('/deleteAbout/:id', AdminVerify, deleteAbout)
router.post('/updateAboutStatus/:id', AdminVerify, updateAboutStatus)
router.post('/changeMultiSsrAboutStatus', AdminVerify, changeMultipleSsrAboutStatusByAdmin)

// Catalog
router.get('/catalogList/:skip/:limit', AdminVerify, getAllCatalogList)
router.post('/createCatalog', AdminVerify, CreateCatalog)
router.post('/editCatalog/:id', AdminVerify, updateCatalog)
router.get('/deleteCatalog/:id', AdminVerify, deleteCatalog)
router.post('/changeStatusCatalog/:id', AdminVerify, changeCatalogStatus)
router.get('/getAllCatalog', getAllCatalog)
router.post('/changeMultiSsrCatalogStatus', AdminVerify, changeMultipleSsrCatalogStatusByAdmin)

//create contact user
router.post('/createContact', createContact)
router.get('/contactList/:skip/:limit', AdminVerify, getAllContactList)
router.get('/deleteContact/:id', AdminVerify, deleteContact)

// Blog
router.get('/blogList/:skip/:limit', AdminVerify, getAllBlogList)
router.post('/createBlog', AdminVerify, CreateBlog)
router.post('/editBlog/:id', AdminVerify, updateBlog)
router.get('/deleteBlog/:id', AdminVerify, deleteBlog)
router.post('/changeStatusBlog/:id', AdminVerify, changeBlogStatus)
router.get('/getAllBlog', getAllBlog)
router.get('/getBlog/:id', getSingleBlog)
router.post('/changeMultiSsrBlogStatus', AdminVerify, changeMultipleSsrBlogStatusByAdmin)

//comment
router.get('/blogCommentList/:skip/:limit', AdminVerify, getAllCommentList)
router.post('/changeStatusComment/:id', AdminVerify, changeCommentStatus)
router.post('/createBlogComment', createComment)
router.post('/replyBlogComment/:id', replyComment)

// blogD
router.get('/getBlogData/:id', getBlogData)

// Affiliate
router.get('/affiliateList/:skip/:limit', AdminVerify, getAllAffiliateList)
router.post('/createAffiliate', AdminVerify, CreateAffiliate)
router.post('/editAffiliate/:id', AdminVerify, updateAffiliate)
router.get('/deleteAffiliate/:id', AdminVerify, deleteAffiliate)
router.post('/changeStatusAffiliate/:id', AdminVerify, changeAffiliateStatus)
router.get('/getAllAffiliate', getAllAffiliate)
router.post('/changeMultiSsrAffiliateStatus', AdminVerify, changeMultipleSsrAffiliateStatusByAdmin)

// Logo
router.get('/logoList/:skip/:limit', AdminVerify, getAllLogoList)
router.post('/createLogo', AdminVerify, CreateLogo)
router.post('/editLogo/:id', AdminVerify, updateLogo)
router.get('/deleteLogo/:id', AdminVerify, deleteLogo)
router.post('/changeStatusLogo/:id', AdminVerify, updateLogoStatus)

//create news
router.post('/createNewsletter', createNewsletter)
router.get('/newsletterList/:skip/:limit', AdminVerify, getAllNewsletterList)
router.get('/deleteNewsletter/:id', AdminVerify, deleteNewsletter)

//create user
router.post('/createUser', createUserS)
router.get('/userList/:skip/:limit', AdminVerify, getAllUserSList)
router.get('/deleteUser/:id', AdminVerify, deleteUserS)

// Stripe
router.get('/stripeList/:skip/:limit', AdminVerify, getAllStripeList)
router.post('/createStripe', AdminVerify, CreateStripe)
router.post('/editStripe/:id', AdminVerify, EditStripe)
router.post('/deleteStripe/:id', AdminVerify, deleteStripe)
router.post('/updateStripeStatus/:id', AdminVerify, updateStripeStatus)
router.post('/changeMultiSsrStripeStatus', AdminVerify, changeMultipleSsrStripeStatusByAdmin)

// product payment
router.post('/makePayment', shopCreateVerify, StripePayment)
router.post('/createOrder', shopCreateVerify, RazorpayCreatePayment)
router.post('/completeOrder', RazorpayCompletePayment)

//order
router.get('/orderList/:skip/:limit', AdminVerify, getAllOrderList)
router.get('/paymentList/:skip/:limit', AdminVerify, getAllPaymentList)

// UserFeature
router.get('/createUserFeature', AdminVerify, createUserFeaturePage)
router.post('/editUserFeature/:id', AdminVerify, updateUserFeaturePage)

// Package Feature
router.get('/packageFeatureList/:skip/:limit', AdminVerify, getAllPackageFeatureList)
router.post('/createPackageFeature', AdminVerify, CreatePackageFeature)
router.post('/editPackageFeature/:id', AdminVerify, EditPackageFeature)
router.get('/deletePackageFeature/:id', AdminVerify, deletePackageFeature)

// Razorpay
router.get('/razorpayList/:skip/:limit', AdminVerify, getAllRazorpayList)
router.post('/createRazorpay', AdminVerify, CreateRazorpay)
router.post('/editRazorpay/:id', AdminVerify, EditRazorpay)
router.post('/deleteRazorpay/:id', AdminVerify, deleteRazorpay)
router.post('/updateRazorpayStatus/:id', AdminVerify, updateRazorpayStatus)
router.post('/changeMultiSsrRazorpayStatus', AdminVerify, changeMultipleSsrRazorpayStatusByAdmin)

export default router;
