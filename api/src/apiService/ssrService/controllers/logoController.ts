import { SsrLogoModel } from "../../../Models/SsrPage";
import { NextFunction, Request, Response } from 'express';
import { randomImageName } from '../../../config/keys';
import { GalleryModel } from "../../../Models/Gallery";
import { upload } from '../../../config/uploadImage';
import multer from 'multer';
import { ImageUpload, removeImage } from '../../../ImageService/main';
import { Con } from '../../../ImageService/source';
import { isValidObjectId } from "mongoose";


export const CreateLogo = async (req: Request, res: Response, next: NextFunction) => {
  console.log('image', req.file)

  const MainCheck = upload.single('image')

  MainCheck(req, res, async function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 500KB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('data', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data
    var key: any = req.user
    var imgD = ''
    var check: any = { admin_id: key._id }

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        image: req.body.oldimage,
        ...check,
        title: req.body.title
      }


      const LogoMde = new SsrLogoModel(data)

      LogoMde.save().then((item: any) => {
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: item
        })
      }).catch((err: Error) => {
        console.log('Logo Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })

      })

    } else {


      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }
        imgD = item?.response.url

        const ImageCreate = await GalleryModel.create({
          image: imgD,
          user_id: key._id,
          data: { ...req.file }
        })
        console.log('Image Made', ImageCreate)
        data = {
          image: imgD,
          ...check,
          title: req.body.title
        }


        const LogoMde = new SsrLogoModel(data)

        LogoMde.save().then((item: any) => {
          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: item
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err: Error) => {
          console.log('Logo Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })

        })

      }).catch((err) => {
        console.log('Logo Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    }


  })

}


export const updateLogo = async (req: Request, res: Response, next: NextFunction) => {
  console.log('image', req.file)

  const MainCheck = upload.single('image')

  MainCheck(req, res, async function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 500KB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    console.log('check', isValidObjectId(req.params.id))

    if (isValidObjectId(req.params.id) == false) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "All Fields are Required",
        data: 'err'
      })
    }

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data: any
    var key: any = req.user
    var imgD = ''

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        image: req.body.oldimage,
        title: req.body.title
      }


      SsrLogoModel.findOneAndUpdate({ _id: req.params.id }, {
        $set: {
          ...data
        }
      }, { new: true }).then(async (cc) => {

        if (cc == null || cc == undefined) {
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: cc
          })
        }

        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: cc
        })

      }).catch((err) => {
        console.log('Logo Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }
        imgD = item?.response.url

        const ImageCreate = await GalleryModel.create({
          image: imgD,
          user_id: key._id,
          data: { ...req.file }
        })
        console.log('Image Made', ImageCreate)
        data = {
          image: imgD,
          title: req.body.title
        }


        SsrLogoModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            ...data
          },
        }, { new: true }).then(async (cc) => {

          if (cc == null || cc == undefined) {
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: cc
            })
          }

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: cc
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err) => {
          console.log('Logo Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Logo Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    }

  })


}


export const deleteLogo = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  console.log('body', req.params.id)

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  SsrLogoModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      deleted: true
    }
  }, { new: true }).then((cc) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Logo Deelete Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}


export const getAllLogoList = (req: Request, res: Response, next: NextFunction) => {

  SsrLogoModel.aggregate([
    {
      $match: {
        deleted: false
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'logo': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'logo': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        logo: [],
        total: 0
      }
    })
  }).catch((err: Error) => {
    console.log('Logo Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const getAllLogo = (req: Request, res: Response, next: NextFunction) => {

  SsrLogoModel.find({ deleted: false }).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { logo: item }
    })
  }).catch((err: Error) => {
    console.log('Logo Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const updateLogoStatus = (req: Request, res: Response, next: NextFunction) => {
  var data = req.body

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.status == null || req.body.status == undefined) {
    return res.status(400).json({
      message: 'Failed',
      status: res.statusCode,
      data: 'No Data Found'
    })
  }

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  if (req.body.status == true) {

    SsrLogoModel.updateMany({}, { $set: { status: false } }).then((ii: any) => {

      data.updatedAt = new Date()

      SsrLogoModel.findOneAndUpdate({ _id: req.params.id }, { $set: data }).then((item: any) => {

        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: 'Request Submmited',
          data: item
        })

      }).catch((err) => {
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: 'Request Failed',
          data: err
        })
      })

    }).catch((err) => {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    })


  } else {

    SsrLogoModel.findOneAndUpdate({ _id: req.params.id }, { $set: data }).then((item: any) => {

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: 'Request Submmited',
        data: item
      })

    }).catch((err) => {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    })
  }

}


export const changeMultipleSsrLogoStatusByAdmin = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.id == null || req.body.id == undefined || req.body.id.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  SsrLogoModel.updateMany({ _id: req.body.id.map((cc: any) => cc) }, {
    $set: {
      ...req.body.status
    }
  }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('SSr Logo Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}