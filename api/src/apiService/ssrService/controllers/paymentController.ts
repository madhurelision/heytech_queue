import { Request, Response, NextFunction } from 'express';
import Stripe from 'stripe';
import mongoose from 'mongoose';
import moment from 'moment';
import crypto from 'crypto';
const stripe = new Stripe('sk_test_51IbOuBSALt0jE426tx23jXCpOaMPEtNdniNykZk8WKoCXguWL78hK4J7kmNYMk6dWrOZNiPz0eJMyXvvsvXa48O000YTcpEUt2', {
  apiVersion: '2020-08-27',
})
import Razorpay from 'razorpay';
import { nanoid } from 'nanoid';
import { SsrCatalogModel, SsrOrderModel, SsrPaymentModel, SsrRazorpayModel } from '../../../Models/SsrPage';

export const StripePayment = (req: Request, res: Response, next: NextFunction) => {

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.pay_id == null || req.body.pay_id == undefined || req.body.pay_id == "" || req.body.product_id == null || req.body.product_id == null) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'No Data Found'
    })
  }

  var key: any = req.user

  const promise = new Promise((resolve, reject) => {
    SsrCatalogModel.find({ _id: req.body.product_id }).exec((err, item) => {
      if (err) {
        return reject(err)
      }

      if (item.length == 0) {
        return reject({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: 'No Data Found'
        })
      } else {
        resolve(item)
      }

    })
  })

  const promise1 = new Promise((resolve, reject) => {
    SsrPaymentModel.find({ user_id: key._id, status: true }).sort({ _id: -1 }).limit(1).then((test) => {
      resolve(test)
    }).catch((err) => {
      reject(err)
    })
  })

  Promise.all([promise, promise1]).then(async (values: any) => {

    console.log('Product Sum', values[0][0])
    var TC = (req.body.pay_type == 'monthly') ? parseInt(values[0][0].price) : parseInt(values[0][0].price_year)
    console.log('All ValData', values)

    if (TC == null || TC == undefined || TC == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "The Product is Free so payment is not Required",
        data: 'No Data Found'
      })
    }

    const productOrder = await SsrOrderModel.create({
      ...req.body,
      Amount: TC,
      user_id: key._id
    })

    console.log('Product Order Done', productOrder)

    const session = await mongoose.startSession();

    try {

      const transactionResults = await session.withTransaction(async () => {
        console.log('Amoutn', TC)

        const stripeTransaction = await stripe.charges.create({
          amount: TC * 100,
          currency: "INR",
          description: 'Thank You For Shopping',
          source: req.body.pay_id,
          capture: false,
          metadata: {
            product: JSON.stringify({
              _id: values[0][0]._id,
              title: values[0][0].title,
              price: values[0][0].price,
              image: values[0][0].image
            }),
            order: productOrder._id
          }
        })

        console.log('Stripe Transaction', stripeTransaction)

        const stripeConfirm = await stripe.charges.capture(
          stripeTransaction.id.toString()
        );

        console.log('stripe Confirm', stripeConfirm)

        const successData = {
          payment_data: { ...stripeConfirm },
          email: req.body.email,
          name: req.body.name,
          mobile: req.body.mobile,
          pay_id: req.body.pay_id,
          product_id: req.body.product_id,
          order_id: productOrder._id,
          Amount: TC,
          payment_id: stripeConfirm.payment_intent,
          payment_method_id: stripeConfirm.payment_method,
          charge_id: stripeConfirm.id,
          payment_url: stripeConfirm.receipt_url,
          refunds_url: stripeConfirm.refunds.url,
          payment_method: 'Stripe',
          payment_status: stripeConfirm.status,
          user_id: key._id,
          startDate: values[1].length == 0 ? new Date() : values[1][0].endDate,
          endDate: values[1].length == 0 ? moment().add(1, (req.body.pay_type == 'Month' || req.body.pay_type == 'Monthly' || req.body.pay_type == 'month' || req.body.pay_type == 'monthly') ? 'M' : (req.body.pay_type == 'Year' || req.body.pay_type == 'Yearly' || req.body.pay_type == 'year' || req.body.pay_type == 'yearly') ? 'y' : 'M').toDate() : moment(values[1][0].endDate).add(1, (req.body.pay_type == 'Month' || req.body.pay_type == 'Monthly' || req.body.pay_type == 'month' || req.body.pay_type == 'monthly') ? 'M' : (req.body.pay_type == 'Year' || req.body.pay_type == 'Yearly' || req.body.pay_type == 'year' || req.body.pay_type == 'yearly') ? 'y' : 'M').toDate()
        }

        const MainTr = await SsrPaymentModel.create([successData], { session })

        console.log('Product Transaction Succes', MainTr)

        const updateOrders = await SsrOrderModel.updateOne({ _id: productOrder._id }, {
          $set: {
            status: true
          }
        }, { session })

        console.log('Order Status Updates', updateOrders)

      })


      if (transactionResults !== null) {
        console.log("The reservation was successfully created.");
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Submitted",
          data: transactionResults
        })
      } else {
        console.log("The transaction was intentionally aborted.");
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: transactionResults
        })
      }

    } catch (err) {
      console.log("The transaction was aborted due to an unexpected error: " + err);
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    } finally {
      await session.endSession();
    }

  }).catch((err) => {
    console.log('Primuse', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const RazorpayCreatePayment = (req: Request, res: Response, next: NextFunction) => {

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.product_id == null || req.body.product_id == null) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'No Data Found'
    })
  }

  var key: any = req.user

  const promise = new Promise((resolve, reject) => {
    SsrCatalogModel.find({ _id: req.body.product_id }).exec((err, item) => {
      if (err) {
        return reject(err)
      }

      if (item.length == 0) {
        return reject({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: 'No Data Found'
        })
      } else {
        resolve(item)
      }

    })
  })

  const promise1 = new Promise((resolve, reject) => {
    SsrRazorpayModel.find({ status: true, deleted: false }).exec((err, item) => {
      if (err) {
        return reject(err)
      }

      if (item.length == 0) {
        return reject({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: 'No Data Found'
        })
      } else {
        resolve(item)
      }

    })
  })

  Promise.all([promise, promise1]).then(async (values: any) => {

    console.log('Product Sum', values[0][0])
    var TC = (req.body.pay_type == 'monthly') ? parseInt(values[0][0].price) : parseInt(values[0][0].price_year)
    console.log('All ValData', values)

    if (values[1].length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'No Data Found'
      })
    }

    if (TC == null || TC == undefined || TC == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "The Product is Free so payment is not Required",
        data: 'No Data Found'
      })
    }

    const productOrder = await SsrOrderModel.create({
      ...req.body,
      Amount: TC,
      user_id: key._id,
      status: false
    })

    console.log('Product Order Done', productOrder)

    try {


      var razorpay = new Razorpay({
        key_id: values[1][0].key,
        key_secret: values[1][0].mainKey
      })

      const payment_capture = 1
      const currency = 'INR'

      const options = {
        amount: TC * 100,
        currency,
        receipt: nanoid(6),
        payment_capture
      };

      console.log('option', options)

      const order = await razorpay.orders.create(options);

      console.log('order', order)

      if (!order) return res.status(400).json({
        message: "Please try razorpay after some time",
        status: res.statusCode,
        data: 'error'
      })

      const productOrders = await SsrOrderModel.findOneAndUpdate({ _id: productOrder._id }, {
        $set: {
          order: order,
          pay_id: order.id
        }
      })

      console.log('Product Order Done', productOrders)

      res.status(200).json({
        message: 'Success',
        data: { ...order, name: values[0][0].title, image: values[0][0].image, description: values[0][0].description, title: req.body.name, email: req.body.email }
      })

    } catch (err) {
      console.log('Try catch err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

  }).catch((err) => {
    console.log('Primuse', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const RazorpayCompletePayment = (req: Request, res: Response, next: NextFunction) => {

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'No Data Found'
    })
  }

  console.log('test', req.body)

  const promise = new Promise((resolve, reject) => {
    SsrRazorpayModel.find({ status: true, deleted: false }).exec((err, item) => {
      if (err) {
        return reject(err)
      }

      if (item.length == 0) {
        return reject({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: 'No Data Found'
        })
      } else {
        resolve(item)
      }

    })
  })

  const promise1 = new Promise((resolve, reject) => {
    SsrOrderModel.find({ pay_id: req.body.razorpay_order_id }).exec((err, item) => {
      if (err) {
        return reject(err)
      }

      if (item.length == 0) {
        return reject({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: 'No Data Found'
        })
      } else {
        resolve(item)
      }

    })
  })

  Promise.all([promise, promise1]).then(async (value: any) => {

    console.log('check Values', value)

    if (value[1].length == 0 || value[0].length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'No Data Found'
      })
    }

    const mainData = await SsrPaymentModel.find({ user_id: value[1][0].user_id, status: true }).sort({ _id: -1 }).limit(1)

    const session = await mongoose.startSession();


    try {

      const transactionResults = await session.withTransaction(async () => {

        // Creating our own digest
        // The format should be like this:
        // digest = hmac_sha256(orderCreationId + "|" + razorpayPaymentId, secret);
        const shasum = crypto.createHmac("sha256", value[0][0].mainKey);

        shasum.update(`${value[1][0].pay_id}|${req.body.razorpay_payment_id}`);

        const digest = shasum.digest("hex");
        console.log('digest', digest)
        console.log('main s', req.body.razorpay_signature)
        // comaparing our digest with the actual signature
        if (digest !== req.body.razorpay_signature) {
          session.abortTransaction()
          console.error("Razorpay Signature Not matched payment Not Verify");
          console.error("Any operations that already occurred as part of this transaction will be rolled back.");
          throw new Error("Razorpay Signature Not matched payment Not Verify")
        }

        const successData = {
          payment_data: { ...req.body },
          email: value[1][0].email,
          name: value[1][0].name,
          mobile: value[1][0].mobile,
          pay_id: value[1][0].pay_id,
          product_id: value[1][0].product_id,
          order_id: value[1][0]._id,
          Amount: value[1][0].amount,
          payment_id: req.body.razorpay_payment_id,
          payment_method: 'Razorpay',
          payment_status: (!req.body.status || req.body.status == null || req.body.status == undefined || req.body.status == "" || req.body.status == 'null' || req.body.status == 'undefined') ? 'Successed' : req.body.status,
          user_id: value[1][0].user_id,
          startDate: mainData.length == 0 ? new Date() : mainData[0].endDate,
          endDate: mainData.length == 0 ? moment().add(1, (value[1][0].pay_type == 'Month' || value[1][0].pay_type == 'Monthly' || value[1][0].pay_type == 'month' || value[1][0].pay_type == 'monthly') ? 'M' : (value[1][0].pay_type == 'Year' || value[1][0].pay_type == 'Yearly' || value[1][0].pay_type == 'year' || value[1][0].pay_type == 'yearly') ? 'y' : 'M').toDate() : moment(mainData[0].endDate).add(1, (value[1][0].pay_type == 'Month' || value[1][0].pay_type == 'Monthly' || value[1][0].pay_type == 'month' || value[1][0].pay_type == 'monthly') ? 'M' : (value[1][0].pay_type == 'Year' || value[1][0].pay_type == 'Yearly' || value[1][0].pay_type == 'year' || value[1][0].pay_type == 'yearly') ? 'y' : 'M').toDate()
        }

        const MainTr = await SsrPaymentModel.create([successData], { session })

        console.log('Product Transaction Succes', MainTr)

        const updateOrders = await SsrOrderModel.updateOne({ _id: value[1][0]._id }, {
          $set: {
            status: true
          }
        }, { session })

        console.log('Order Status Updates', updateOrders)

      })


      if (transactionResults !== null) {
        console.log("The reservation was successfully created.");
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Submitted",
          data: transactionResults
        })
      } else {
        console.log("The transaction was intentionally aborted.");
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: transactionResults
        })
      }

    } catch (err) {
      console.log("The transaction was aborted due to an unexpected error: " + err);
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    } finally {
      await session.endSession();
    }

  }).catch((err) => {
    console.log("The transaction was aborted due to an unexpected error: " + err);
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}