import { SsrProductModel } from "../../../Models/SsrPage";
import { NextFunction, Request, Response } from 'express';
import { randomImageName } from '../../../config/keys';
import { GalleryModel } from "../../../Models/Gallery";
import { upload } from '../../../config/uploadImage';
import multer from 'multer';
import { ImageUpload, removeImage } from '../../../ImageService/main';
import { Con } from '../../../ImageService/source';
import { isValidObjectId } from "mongoose";


export const CreateProduct = async (req: Request, res: Response, next: NextFunction) => {
  console.log('image', req.file)

  const MainCheck = upload.single('image')

  MainCheck(req, res, async function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 500KB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('data', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data
    var key: any = req.user
    var imgD = ''
    var check: any = { admin_id: key._id }

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        image: req.body.oldimage,
        ...check,
        title: req.body.title,
        description: req.body.description,
        link: req.body.link,
      }


      const ProductMde = new SsrProductModel(data)

      ProductMde.save().then((item: any) => {
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: item
        })
      }).catch((err: Error) => {
        console.log('Product Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })

      })

    } else {


      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }
        imgD = item?.response.url

        const ImageCreate = await GalleryModel.create({
          image: imgD,
          user_id: key._id,
          data: { ...req.file }
        })
        console.log('Image Made', ImageCreate)
        data = {
          image: imgD,
          ...check,
          title: req.body.title,
          description: req.body.description,
          link: req.body.link,
        }


        const ProductMde = new SsrProductModel(data)

        ProductMde.save().then((item: any) => {
          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: item
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err: Error) => {
          console.log('Product Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })

        })

      }).catch((err) => {
        console.log('Product Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    }


  })

}


export const updateProduct = async (req: Request, res: Response, next: NextFunction) => {
  console.log('image', req.file)

  const MainCheck = upload.single('image')

  MainCheck(req, res, async function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 500KB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    console.log('check', isValidObjectId(req.params.id))

    if (isValidObjectId(req.params.id) == false) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "All Fields are Required",
        data: 'err'
      })
    }

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data: any
    var key: any = req.user
    var imgD = ''

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        image: req.body.oldimage,
        title: req.body.title,
        description: req.body.description,
        link: req.body.link,
      }


      SsrProductModel.findOneAndUpdate({ _id: req.params.id }, {
        $set: {
          ...data
        }
      }, { new: true }).then(async (cc) => {

        if (cc == null || cc == undefined) {
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: cc
          })
        }

        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: cc
        })

      }).catch((err) => {
        console.log('Product Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }
        imgD = item?.response.url

        const ImageCreate = await GalleryModel.create({
          image: imgD,
          user_id: key._id,
          data: { ...req.file }
        })
        console.log('Image Made', ImageCreate)
        data = {
          image: imgD,
          user_id: key._id,
          title: req.body.title,
          description: req.body.description,
          link: req.body.link,
        }


        SsrProductModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            ...data
          },
        }, { new: true }).then(async (cc) => {

          if (cc == null || cc == undefined) {
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: cc
            })
          }

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: cc
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err) => {
          console.log('Product Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Product Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    }

  })


}


export const deleteProduct = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  console.log('body', req.params.id)

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  SsrProductModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      deleted: true
    }
  }, { new: true }).then((cc) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Product Deelete Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}


export const getAllProductList = (req: Request, res: Response, next: NextFunction) => {

  SsrProductModel.aggregate([
    {
      $match: {
        deleted: false
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'product': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'product': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        product: [],
        total: 0
      }
    })
  }).catch((err: Error) => {
    console.log('Product Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const getAllProduct = (req: Request, res: Response, next: NextFunction) => {

  SsrProductModel.find({ deleted: false }).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { product: item }
    })
  }).catch((err: Error) => {
    console.log('Product Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const changeProductStatus = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  var key: any = req.user
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  SsrProductModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      status: req.body.status
    }
  }).then(async (cc) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Product Status Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}


export const changeMultipleSsrProductStatusByAdmin = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.id == null || req.body.id == undefined || req.body.id.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  SsrProductModel.updateMany({ _id: req.body.id.map((cc: any) => cc) }, {
    $set: {
      ...req.body.status
    }
  }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('SSr Product Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}