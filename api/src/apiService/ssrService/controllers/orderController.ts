import { Request, Response, NextFunction } from 'express';
import { SsrOrderModel, SsrPaymentModel } from '../../../Models/SsrPage';


export const getAllOrderList = (req: Request, res: Response, next: NextFunction) => {

  SsrOrderModel.aggregate([
    {
      $match: {
        deleted: false
      }
    }, {
      '$lookup': {
        'from': 'ssrcatalogs',
        'localField': 'product_id',
        'foreignField': '_id',
        'as': 'product_id'
      }
    }, {
      '$unwind': {
        'path': '$product_id'
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'order': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'order': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        order: [],
        total: 0
      }
    })
  }).catch((err: Error) => {
    console.log('Order Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}


export const getAllPaymentList = (req: Request, res: Response, next: NextFunction) => {

  SsrPaymentModel.aggregate([
    {
      $match: {
        deleted: false
      }
    }, {
      '$lookup': {
        'from': 'ssrcatalogs',
        'localField': 'product_id',
        'foreignField': '_id',
        'as': 'product_id'
      }
    }, {
      '$unwind': {
        'path': '$product_id'
      }
    },
    {
      '$lookup': {
        'from': 'ssrorders',
        'localField': 'order_id',
        'foreignField': '_id',
        'as': 'order_id'
      }
    }, {
      '$unwind': {
        'path': '$order_id'
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'payment': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'payment': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        payment: [],
        total: 0
      }
    })
  }).catch((err: Error) => {
    console.log('Paymnet Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}