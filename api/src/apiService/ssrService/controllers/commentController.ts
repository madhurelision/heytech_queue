import { SsrCommentModel } from "../../../Models/SsrPage";
import { NextFunction, Request, Response } from 'express';
import mongoose, { isValidObjectId } from 'mongoose'

export const createComment = (req: Request, res: Response, next: NextFunction) => {
  console.log('data', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }
  var data = req.body
  const CommentMode = new SsrCommentModel(data)

  CommentMode.save().then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err: Error) => {
    console.log('Comment Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}


export const replyComment = (req: Request, res: Response, next: NextFunction) => {
  console.log('data', req.body)


  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }


  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      message: 'Failed',
      status: res.statusCode,
      data: 'No Data Found'
    })
  }

  var data = req.body
  data.status = true
  data.blog_id = new mongoose.Types.ObjectId(req.body.blog_id)
  data._id = new mongoose.Types.ObjectId()
  data.deleted = false
  data.createdAt = new Date();
  data.updatedAt = new Date();

  SsrCommentModel.findOneAndUpdate({ _id: req.params.id }, { $push: { reply: data } }).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err: Error) => {
    console.log('Comment Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}

export const getAllCommentList = (req: Request, res: Response, next: NextFunction) => {

  SsrCommentModel.aggregate([
    {
      $match: {
        deleted: false
      }
    },

    {
      $lookup: {
        from: 'ssrblogs',
        localField: 'blog_id',
        foreignField: '_id',
        as: 'blog_id'
      }
    },
    {
      $unwind: '$blog_id'
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'comment': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'comment': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        comment: [],
        total: 0
      }
    })
  }).catch((err: Error) => {
    console.log('Blog Comment Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const changeCommentStatus = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  var key: any = req.user
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  SsrCommentModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      status: req.body.status
    }
  }).then(async (cc) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Comment Status Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}
