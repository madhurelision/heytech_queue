import { NextFunction, Request, Response } from 'express';
import { SsrMenuModel, SsrFeatureModel, SsrProductModel, SsrReviewModel, SsrHomeModel, SsrAboutModel, SsrMainModel, SsrCatalogModel, SsrBlogModel, SsrAffiliateModel, SsrLogoModel, SsrStripeModel, SsrPackageFeatureModel, SsrUserFeatureModel, SsrRazorpayModel } from '../../../Models/SsrPage';
import mongoose from 'mongoose'
import { ShopCategoryModel } from '../../../Models/ShopCategory';

export const getAllMetaList = (req: Request, res: Response, next: NextFunction) => {

  const promise1 = new Promise((resolve, reject) => {

    SsrMenuModel.find({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  const promise2 = new Promise((resolve, reject) => {

    SsrHomeModel.find({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  const promise3 = new Promise((resolve, reject) => {

    SsrFeatureModel.find({ status: true, deleted: false }).sort({ 'position': 1 }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  const promise4 = new Promise((resolve, reject) => {

    SsrProductModel.find({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  const promise5 = new Promise((resolve, reject) => {

    SsrReviewModel.find({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  const promise6 = new Promise((resolve, reject) => {

    SsrMainModel.findOne({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  const promise7 = new Promise((resolve, reject) => {

    SsrAboutModel.findOne({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })


  const promise8 = new Promise((resolve, reject) => {

    SsrCatalogModel.find({ status: true, deleted: false }).populate('product_id').then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  const promise9 = new Promise((resolve, reject) => {

    SsrBlogModel.aggregate([
      {
        $match: {
          status: true,
          deleted: false
        }
      }, {
        '$lookup': {
          'from': 'ssrblogcomments',
          'let': {
            'main': '$_id'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$blog_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$status', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'comment'
        }
      },
    ]).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  const promise10 = new Promise((resolve, reject) => {

    SsrAffiliateModel.find({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })


  const promise11 = new Promise((resolve, reject) => {

    SsrLogoModel.findOne({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  const promise12 = new Promise((resolve, reject) => {

    ShopCategoryModel.find({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  const promise13 = new Promise((resolve, reject) => {

    SsrStripeModel.findOne({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  const promise14 = new Promise((resolve, reject) => {

    SsrUserFeatureModel.findOne({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  const promise15 = new Promise((resolve, reject) => {

    SsrPackageFeatureModel.find({ status: true, deleted: false }).populate('package_id').then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })


  const promise16 = new Promise((resolve, reject) => {

    SsrRazorpayModel.findOne({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  Promise.allSettled([promise1, promise2, promise3, promise4, promise5, promise6, promise7, promise8, promise9, promise10, promise11, promise12, promise13, promise14, promise15, promise16]).then((cc: any) => {
    // console.log('ss', cc)
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        menuList: cc[0].value,
        homeList: cc[1].value,
        featureList: cc[2].value,
        productList: cc[3].value,
        reviewList: cc[4].value,
        mainList: cc[5].value,
        aboutList: cc[6].value,
        catalogList: cc[7].value,
        blogList: cc[8].value,
        affiliateList: cc[9].value,
        logoList: cc[10].value,
        categoryList: cc[11].value,
        stripe: cc[12].value,
        userFeature: cc[13].value,
        packageList: cc[14].value,
        razorpay: cc[15].value,
      }
    })

  }).catch((err) => {
    console.log('Ssr pages Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const getBlogData = (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.params.id)
  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  const promise1 = new Promise((resolve, reject) => {

    SsrMenuModel.find({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  const promise2 = new Promise((resolve, reject) => {

    SsrMainModel.findOne({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  const promise3 = new Promise((resolve, reject) => {

    SsrBlogModel.aggregate([
      {
        $match: {
          _id: new mongoose.Types.ObjectId(req.params.id)
        }
      }, {
        '$lookup': {
          'from': 'ssrblogcomments',
          'let': {
            'main': '$_id'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$blog_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$status', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'comment'
        }
      },
    ]).then((item: any) => {
      resolve(item.length > 0 ? item[0] : {})
    }).catch((err: Error) => {
      reject(err)
    })

  })


  const promise4 = new Promise((resolve, reject) => {

    SsrLogoModel.findOne({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })


  const promise5 = new Promise((resolve, reject) => {

    SsrReviewModel.find({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  Promise.allSettled([promise1, promise2, promise3, promise4, promise5]).then((cc: any) => {
    // console.log('ss', cc)
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        menuList: cc[0].value,
        mainList: cc[1].value,
        blogDetail: cc[2].value,
        logoList: cc[3].value,
        reviewList: cc[4].value,
      }
    })

  }).catch((err) => {
    console.log('Ssr pages Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}