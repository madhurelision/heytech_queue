import express, { Express } from 'express';
import routes from './routes';

const ssrApp = (app: Express) => {

  app.use('/ssr', routes)

};

export default ssrApp;
