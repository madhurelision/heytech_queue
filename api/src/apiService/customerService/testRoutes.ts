import { Router } from 'express';
import { CustomerVerify } from '../../config/verifyToken';
import { getCustomerProfile } from './controllers/customerController';

import { CountBooking, getCustomerBookings, getCustomerCurrentAppointment, userCancelBooking, userVerifyCancelBooking, customerBookingQuery, getSingleQuery } from '../bookingService/controllers/bookingController';

import { changeViewStatus, getCustomerNotification } from '../adminService/controllers/notificationController';
import { makeReview } from '../adminService/controllers/reviewController';

const router = Router();
// customer
router.get('/user/profile', CustomerVerify, (req, res, next) => { getCustomerProfile(req, res, next, true) })

router.get('/user/getAllBooking', CustomerVerify, (req, res, next) => { getCustomerBookings(req, res, next, true) })
router.get('/user/getCurrentBooking', CustomerVerify, (req, res, next) => { getCustomerCurrentAppointment(req, res, next, true) })
router.get('/user/bookingCount', CustomerVerify, (req, res, next) => { CountBooking(req, res, next, true) })
router.post('/user/getQueryBooking/:skip/:limit', CustomerVerify, (req, res, next) => { customerBookingQuery(req, res, next, true) })
router.get('/user/getBookQuery/:id', CustomerVerify, (req, res, next) => { getSingleQuery(req, res, next, true) })

// cancelBooking
router.get('/user/cancelBooking/:id', CustomerVerify, (req, res, next) => { userCancelBooking(req, res, next, true) })
router.post('/user/verifyCancelBooking/:id', CustomerVerify, (req, res, next) => { userVerifyCancelBooking(req, res, next, true) })

router.post('/user/makeReview', CustomerVerify, (req, res, next) => { makeReview(req, res, next, true) })

// notification Customer
router.get('/user/notification', CustomerVerify, (req, res, next) => { getCustomerNotification(req, res, next, true) })
router.get('/user/view', CustomerVerify, (req, res, next) => { changeViewStatus(req, res, next, true) })


export default router;
