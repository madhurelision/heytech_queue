import { Router } from 'express';
import { CustomerVerify } from '../../config/verifyToken';
import { CreateOtp, getCustomerProfile, updateCustomerProfile, verifyOtp } from './controllers/customerController';

import { CountBooking, getCustomerBookings, getCustomerCurrentAppointment, userCancelBooking, userVerifyCancelBooking, customerBookingQuery, getSingleQuery } from '../bookingService/controllers/bookingController';

import { changeViewStatus, getCustomerNotification } from '../adminService/controllers/notificationController';
import { makeReview } from '../adminService/controllers/reviewController';
import { getCustomerPackagePayment } from './controllers/customerPackageController';

const router = Router();
// customer
router.get('/user/profile', CustomerVerify, (req, res, next) => { getCustomerProfile(req, res, next, false) })

router.get('/user/getAllBooking', CustomerVerify, (req, res, next) => { getCustomerBookings(req, res, next, false) })
router.get('/user/getCurrentBooking/:skip/:limit', CustomerVerify, (req, res, next) => { getCustomerCurrentAppointment(req, res, next, false) })
router.get('/user/bookingCount', CustomerVerify, (req, res, next) => { CountBooking(req, res, next, false) })
router.post('/user/getQueryBooking/:skip/:limit', CustomerVerify, (req, res, next) => { customerBookingQuery(req, res, next, false) })
router.get('/user/getBookQuery/:id', CustomerVerify, (req, res, next) => { getSingleQuery(req, res, next, false) })

// cancelBooking
router.get('/user/cancelBooking/:id', CustomerVerify, (req, res, next) => { userCancelBooking(req, res, next, false) })
router.post('/user/verifyCancelBooking/:id', CustomerVerify, (req, res, next) => { userVerifyCancelBooking(req, res, next, false) })

router.post('/user/makeReview', CustomerVerify, (req, res, next) => { makeReview(req, res, next, false) })

// notification Customer
router.get('/user/notification/:skip/:limit', CustomerVerify, (req, res, next) => { getCustomerNotification(req, res, next, false) })
router.get('/user/view', CustomerVerify, (req, res, next) => { changeViewStatus(req, res, next, false) })
router.post('/user/updateCustomer', CustomerVerify, updateCustomerProfile)

// createOtp
router.post('/user/createCustomerOtp/:id', CreateOtp)
router.post('/user/verifyCustomerOtp/:id', verifyOtp)

//package payment
router.get('/user/getCustomerPayment/:skip/:limit', CustomerVerify, getCustomerPackagePayment)

export default router;
