import express, { Express } from 'express';
import routes from './routes';

const customerApp = (app: Express) => {

  app.use('/api', routes)

};

export default customerApp;
