import { ShopPaymentModel } from "../../../Models/ShopPackage";
import { NextFunction, Request, Response } from 'express';
import mongoose from 'mongoose'


export const getCustomerPackagePayment = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user

  const query = [
    {
      '$match': {
        'user_id': new mongoose.Types.ObjectId(key._id)
      }
    }, {
      '$lookup': {
        'from': 'shoppackages',
        'localField': 'product_id',
        'foreignField': '_id',
        'as': 'product_id'
      }
    }, {
      '$unwind': {
        'path': '$product_id'
      }
    },
    {
      '$lookup': {
        'from': 'shoporders',
        'localField': 'order_id',
        'foreignField': '_id',
        'as': 'order_id'
      }
    }, {
      '$unwind': {
        'path': '$order_id'
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'payment': [
          {
            '$sort': {
              '_id': -1
            }
          },
          {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'payment': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]
  ShopPaymentModel.aggregate(query).then(async (item) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item[0] : { payment: [], total: 0 }
    })
  }).catch((err) => {
    console.log('ShopPayment Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}