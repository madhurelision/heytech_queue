import { NextFunction, Request, Response } from 'express';
import { SECRET_KEY, SERVER_URL, randomImageName, otpGenerate, loginMsg, ServiceTemplateId, MAIL_SECRET_KEY, ServiceSmsMail, EmployeeIdGenerate } from '../../../config/keys';
import { CustomerModel } from '../../../Models/Customer';
import jwt from 'jsonwebtoken'
import multer from 'multer';
import { upload } from '../../../config/uploadImage';
import { ImageUpload, removeImage } from '../../../ImageService/main';
import { Con } from '../../../ImageService/source';
import { isValidObjectId } from 'mongoose';
import mongoose from 'mongoose'
import axios from 'axios'
import config from 'config'
import { performance } from 'perf_hooks'
import { LogQueryModel } from '../../../Models/LogData';

export const googleNew = async (req: Request, accessToken: String, refreshToken: String, params: any, profile: any, done: Function) => {
  console.log('Google Profile Data ', profile)
  console.log('Google Profile Params ', params)

  CustomerModel.findOne({ email: profile._json.email }).then((item: any) => {

    if (item == null || item == undefined) {

      const UserData = new CustomerModel(
        {
          first_name: profile._json.given_name,
          last_name: profile._json.family_name,
          name: profile._json.given_name + ' ' + profile._json.family_name,
          email: profile._json.email,
          image_link: profile._json.picture,
          google: profile.id,
          oauth_provider: profile.provider,
          is_mentor: true,
          signup_completed: true,
          lastLogin: new Date()
        }
      )

      UserData.save().then((main: any) => {
        console.log('inserted record', main);
        //done(null, { _id: response.insertedId })
        done(null, main)

      }).catch((err: Error) => {
        console.log('Error occurred while inserting', err);
        done(err)
      })

    } else {

      CustomerModel.findOneAndUpdate({ email: profile._json.email }, {
        $set: {
          first_name: profile._json.given_name,
          last_name: profile._json.family_name,
          email: profile._json.email,
          google: profile.id,
          oauth_provider: profile.provider,
          lastLogin: new Date()
        }
      }, { new: true }).then((cc: any) => {
        console.log('updated record', cc);
        //done(null, { _id: response.insertedId })
        done(null, cc)

      }).catch((err: Error) => {
        console.log('Error occurred while updating', err);
        done(err)

      })

    }

  }).catch((err: Error) => {
    console.log('Google Find error', err);
    return done(err)
  })

}

export const getCustomerProfile = (req: Request, res: Response, next: NextFunction, verify: any) => {
  console.log('Customer', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  CustomerModel.findOne({ _id: val._id }).then((item) => {

    console.log('customer', item)

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: item
      })
    }

    var customerData: any = item

    const token: any = jwt.sign({ _id: customerData._id, email: (customerData.email == null || customerData.email == undefined) ? customerData.mobile : customerData.email }, SECRET_KEY)
    res.header("auth-token", token)

    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: item,
      token: token,
      type: 'customer'
    })

  }).catch((err) => {
    console.log('Customer err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const updateCustomerProfile = async (req: Request, res: Response, next: NextFunction) => {
  const MainCheck = upload.single('image')

  MainCheck(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 5 MB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('image', req.file)
    console.log('body', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }


    console.log('image', req.file)
    console.log('body', req.body)

    var data
    var key: any = req.user

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        name: req.body.name,
        image_link: req.body.oldimage,
        email: req.body.email,
        location: req.body.location,
        lattitude: req.body.lattitude,
        longitude: req.body.longitude,
      }


      CustomerModel.findOneAndUpdate({ _id: key._id }, {
        $set: {
          ...data
        }
      }, { new: true }).then((cc) => {

        if (cc == null || cc == undefined) {
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: cc
          })
        }

        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: cc
        })
      }).catch((err) => {
        console.log('Customer Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        data = {
          name: req.body.name,
          image_link: item?.response.url,
          email: req.body.email,
          location: req.body.location,
          lattitude: req.body.lattitude,
          longitude: req.body.longitude,
        }


        CustomerModel.findOneAndUpdate({ _id: key._id }, {
          $set: {
            ...data
          }
        }, { new: true }).then((cc) => {

          if (cc == null || cc == undefined) {
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: cc
            })
          }

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: cc
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err) => {
          console.log('Customer Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Customer Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    }


  })

}

export const updateCustomer = async (req: Request, res: Response, next: NextFunction) => {
  const MainCheck = upload.single('image')

  MainCheck(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 5 MB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('image', req.file)
    console.log('body', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }


    console.log('image', req.file)
    console.log('body', req.body)

    var data

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        name: req.body.name,
        image_link: req.body.oldimage,
        email: req.body.email,
        mobile: req.body.mobile,
        location: req.body.location,
        lattitude: req.body.lattitude,
        longitude: req.body.longitude,
      }


      CustomerModel.findOneAndUpdate({ _id: req.params.id }, {
        $set: {
          ...data
        }
      }, { new: true }).then((cc) => {

        if (cc == null || cc == undefined) {
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: cc
          })
        }

        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: cc
        })
      }).catch((err) => {
        console.log('Customer Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        data = {
          first_name: req.body.first_name,
          last_name: req.body.last_name,
          name: req.body.name,
          image_link: item?.response.url,
          email: req.body.email,
          mobile: req.body.mobile,
          location: req.body.location,
          lattitude: req.body.lattitude,
          longitude: req.body.longitude,
        }


        CustomerModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            ...data
          }
        }, { new: true }).then((cc) => {

          if (cc == null || cc == undefined) {
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: cc
            })
          }

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: cc
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err) => {
          console.log('Customer Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Customer Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    }


  })

}

export const customerCsvImport = async (req: Request, res: Response, next: NextFunction) => {

  if (!req.body || req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.data == null || req.body.data == undefined || req.body.data.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: 'Data Not Found',
      data: 'Data Not Found'
    })
  }

  var customerArr: any = []

  req.body.data.map((cc: any) => {
    if (cc.id == "" || cc.id == null || cc.id == undefined) {
      customerArr.push({
        id: cc.id,
        data: {
          first_name: cc["first_name"],
          last_name: cc["last_name"],
          mobile: cc["mobile"],
          email: cc["email"],
          name: cc["name"],
          image_link: cc["image_link"],
        }
      })
    } else {
      customerArr.push({
        id: cc.id,
        data: {
          first_name: cc["first_name"],
          last_name: cc["last_name"],
          mobile: cc["mobile"],
          email: cc["email"],
          name: cc["name"],
          image_link: cc["image_link"],
        },
      })
    }
  })

  try {

    Promise.all(customerArr.map(async (cc: any) => {
      const createS = await CustomerModel.updateMany({ _id: isValidObjectId(cc.id) ? cc.id : new mongoose.Types.ObjectId() }, { $set: cc.data }, { upsert: true, setDefaultsOnInsert: true, new: true })
      console.log('Updated', createS)
      return createS
    })).then((cc) => {

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: cc,
        check: customerArr,
      })

    }).catch((err) => {
      console.log('Customer Csv Import Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    })

  } catch (err) {

    console.log('Customer Csv Import Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  }


}

////admin Customer List
export const SerachAdminCustomerQuery = (req: Request, res: Response, next: NextFunction) => {

  var startTime = performance.now()
  const query = [
    {
      $match: {
        $or: [
          { "first_name": { $regex: req.params.id, $options: "i" } },
          { "last_name": { $regex: req.params.id, $options: "i" } },
          { "email": { $regex: req.params.id, $options: "i" } },
          { "name": { $regex: req.params.id, $options: "i" } },
        ]
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$_id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$shop_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },
    {
      '$addFields': {
        'user_count': {
          '$reduce': {
            'input': '$review',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'total': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$review'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$user_count', {
                    '$size': '$review'
                  }
                ]
              }
            }
          }
        }
      }
    },
  ]
  CustomerModel.aggregate(query).then(async (item: any) => {

    var endTime = (performance.now() - startTime).toFixed();

    const clog = await LogQueryModel.create({
      queryName: query,
      collectionName: 'customers',
      startTime: startTime,
      endTime: parseInt(endTime)
    })

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  }).catch((err: Error) => {
    console.log('Customer err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const customerMobileLogin = async (req: Request, res: Response, next: NextFunction, verify: any) => {

  if (req.body.mobile == null || req.body.mobile == undefined) {
    return res.status(400).json({
      status: '',
      message: 'Mobile Number is Required',
    })
  }

  const customerData = await CustomerModel.find({ mobile: req.body.mobile })

  if (customerData.length > 0) {
    try {

      const otp = otpGenerate()
      const newMsgT = await loginMsg({ service_id: { name: 'Login' }, shop_name: 'Customer', otp: otp, bookingid: '1', queue: true })
      const smsToken = jwt.sign({ date: new Date(), message: newMsgT, id: ServiceTemplateId.book, type: 'customerLogin', user_id: customerData.map((cc) => cc._id), resend: req.body.resend }, MAIL_SECRET_KEY)
      const mainData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
        number: parseInt(req.body.mobile),
        otp: otp
      }, {
        headers: {
          'auth-token': smsToken
        }
      });


      console.log('Otp Sent', mainData)

      const userData = await CustomerModel.updateMany({
        mobile: req.body.mobile,
        deleted: false
      }, {
        $set: {
          otp: otp,
        }
      }, { new: true })

      console.log('Otp Sent', userData)

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: {
          redirect: false,
          data: customerData.map((cc) => { return { _id: cc._id, mobile: cc.mobile, first_name: (cc.first_name == null || cc.first_name == undefined) ? '' : cc.first_name, last_name: (cc.last_name == null || cc.last_name == undefined) ? '' : cc.last_name, email: (cc.email == null || cc.email == undefined) ? '' : cc.email } })
        }
      })

    } catch (err) {

      console.log('Create otp Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })

    }
  } else {

    try {


      const otp = otpGenerate()
      const newMsgT = await loginMsg({ service_id: { name: 'Login' }, shop_name: 'Customer', otp: otp, bookingid: '1', queue: true })
      const smsToken = jwt.sign({ date: new Date(), message: newMsgT, id: ServiceTemplateId.book, type: 'customerLogin', user_id: [], resend: req.body.resend }, MAIL_SECRET_KEY)
      const mainData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
        number: parseInt(req.body.mobile),
        otp: otp
      }, {
        headers: {
          'auth-token': smsToken
        }
      });

      console.log('Otp Sent', mainData)

      const UserData = new CustomerModel(
        {
          mobile: req.body.mobile,
          otp: otp,
          name: 'guest' + EmployeeIdGenerate(),
        }
      )

      UserData.save().then((main: any) => {
        console.log('inserted record', main);
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: {
            redirect: false,
            data: [
              {
                _id: main._id,
                mobile: main.mobile,
                first_name: (main.name == null || main.name == undefined) ? '' : main.name, last_name: (main.last_name == null || main.last_name == undefined) ? '' : main.last_name,
                email: (main.email == null || main.email == undefined) ? '' : main.email
              }
            ]
          }
        })


      }).catch((err: Error) => {
        console.log('Error occurred while Creating User', err);
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } catch (err) {

      console.log('Create otp Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })

    }

  }


}

export const customerMobileLoginOtpVerify = (req: Request, res: Response, next: NextFunction, verify: any) => {


  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  CustomerModel.findOne({ mobile: req.body.mobile, _id: req.body.id, otp: parseInt(req.body.otp) }).then(async (cc) => {
    if (cc == null || cc == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var link

    const token: any = jwt.sign({ _id: cc._id }, SECRET_KEY)
    res.header("auth-token", token)
    link = '/userlogin/' + token;

    const logingS = await CustomerModel.updateMany({ _id: cc._id }, {
      $set: {
        lastLogin: new Date()
      }
    })

    if (cc.status == false) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Your Account is not Activated",
        data: cc
      })
    }

    if (cc.deleted == true) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Your Account Doesn't Exist",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: link
    })

  }).catch((err) => {
    console.log('Customer Mobile Update Err', err)
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}



export const CreateOtp = async (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }


  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  try {

    const otp = otpGenerate()
    //https://heytechemailsms.herokuapp.com
    //http://localhost:5003
    const msgR = await loginMsg({ service_id: { name: 'Customer' }, shop_name: 'Mobile Verify', otp: otp, bookingid: 1, queue: true })
    const smsTokens = jwt.sign({ date: new Date(), message: msgR, type: 'mobileVerify', user_id: [req.params.id], resend: req.body.resend }, MAIL_SECRET_KEY)
    const mainData = await axios.post(config.has('ServiceSmsMail') ? config.get('ServiceSmsMail') + 'smsSend' : ServiceSmsMail + 'smsSend', {
      number: req.body.mobile,
      otp: otp
    }, {
      headers: {
        'auth-token': smsTokens
      }
    });

    console.log('Otp Sent', mainData)


    const userData = await CustomerModel.findOneAndUpdate({
      _id: req.params.id
    }, {
      $set: {
        otp: otp,
        mobile_verify: false
      }
    })

    console.log('Otp Sent', userData)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        number: req.body.mobile,
      }
    })

  } catch (err) {
    console.log('Create otp Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  }

}


export const verifyOtp = (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  CustomerModel.findByIdAndUpdate({ _id: req.params.id, otp: parseInt(req.body.otp) }, {
    $set: {
      mobile_verify: true,
      mobile: req.body.mobile
    }
  }).then((cc) => {
    if (cc == null || cc == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        _id: cc._id
      }
    })

  }).catch((err) => {
    console.log('Shop Mobile Update Err', err)
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const CustomerSearchQuery = (req: Request, res: Response, next: NextFunction) => {

  const query = [
    {
      $match: {
        $or: [
          { "first_name": { $regex: req.params.id, $options: "i" } },
          { "last_name": { $regex: req.params.id, $options: "i" } },
          { "name": { $regex: req.params.id, $options: "i" } },
          { "email": { $regex: req.params.id, $options: "i" } },
          { "mobile": { $regex: req.params.id, $options: "i" } },
        ],
        'deleted': false,
      }
    },
    {
      '$lookup': {
        'from': 'shoppayments',
        'let': {
          'main': '$_id',
          'checkDate': new Date()
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$gte': [
                      '$endDate', '$$checkDate'
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'package'
      }
    }
  ]
  CustomerModel.aggregate(query).exec(async (err, item) => {

    if (err) {
      console.log('Customer Query Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  })


}


export const getAllCustomer = (req: Request, res: Response, next: NextFunction) => {

  const query = [
    {
      $match: {
        deleted: false,
      },
    },
    {
      '$lookup': {
        'from': 'shoppayments',
        'let': {
          'main': '$_id',
          'checkDate': new Date()
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$gte': [
                      '$endDate', '$$checkDate'
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'package'
      }
    },
    {
      $facet: {
        metadata: [
          {
            $group: {
              _id: null,
              total: {
                $sum: 1,
              },
            },
          },
        ],
        customer: [
          {
            $sort: {
              _id: -1,
            },
          },
          {
            $skip:
              req.params.skip == null ||
                req.params.skip == undefined ||
                req.params.skip == ""
                ? 0
                : parseInt(req.params.skip),
          },
          {
            $limit:
              req.params.limit == null ||
                req.params.limit == undefined ||
                req.params.limit == ""
                ? 10
                : parseInt(req.params.limit),
          },
        ],
      },
    },
    {
      $project: {
        customer: 1,
        total: {
          $arrayElemAt: ["$metadata.total", 0],
        },
      },
    },
  ];
  CustomerModel.aggregate(query)
    .then(async (item) => {
      res.status(200).json({
        status: "Success",
        status_code: res.statusCode,
        message: "Request Success",
        data: item.length > 0 ? item[0] : { customer: [], total: 0 },
      });
    })
    .catch((err) => {
      console.log("Customer Find Err", err);
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Request Failed",
        data: err,
      });
    });
};

export const getSingleCustomerPackage = (req: Request, res: Response, next: NextFunction) => {

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('check', isValidObjectId(req.params.id))

  if (isValidObjectId(req.params.id) == false) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  CustomerModel.aggregate([
    {
      '$match': {
        '_id': new mongoose.Types.ObjectId(req.params.id),
      },
    },
    {
      '$lookup': {
        'from': 'shoppayments',
        'let': {
          'main': '$_id',
          'checkDate': new Date()
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$gte': [
                      '$endDate', '$$checkDate'
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'package'
      }
    }
  ])
    .then(async (item) => {
      res.status(200).json({
        status: "Success",
        status_code: res.statusCode,
        message: "Request Success",
        data: item
      });
    })
    .catch((err) => {
      console.log("Customer Find Err", err);
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        message: "Request Failed",
        data: err,
      });
    });

}


export const updateStatusCustomer = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  console.log('body', req.params.id)
  console.log('body', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  CustomerModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      status: req.body.status
    }
  }, { new: true }).then((cc) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Customer Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}


export const changeMultipleCustomerStatusByAdmin = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.id == null || req.body.id == undefined || req.body.id.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  CustomerModel.updateMany({ _id: req.body.id.map((cc: any) => cc) }, {
    $set: {
      ...req.body.status
    }
  }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Customer Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}