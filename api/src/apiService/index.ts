import express, { Express } from 'express';
import adminRoute from './adminService/app';
import bookingRoute from './bookingService/app';
import clientRoute from './clientService/app';
import customerRoute from './customerService/app';
import employeeRoute from './employeeService/app';
import shopRoute from './shopService/app';
import webRoute from './ssrService/app';

const serviceRoutes = (app: Express) => {

  // admin service
  adminRoute(app)

  //client service
  clientRoute(app)

  //shop service
  shopRoute(app)

  // booking service
  bookingRoute(app)

  //customer service
  customerRoute(app)

  //employee service
  employeeRoute(app)

  //web service
  webRoute(app)

};

export default serviceRoutes;
