import express, { Express } from 'express';
import routes from './routes';

const employeeApp = (app: Express) => {

  app.use('/api', routes)

};

export default employeeApp;
