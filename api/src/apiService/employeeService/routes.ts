import { Router } from 'express';
import { EmployeeVerify } from '../../config/verifyToken';
import { employeeLogout, getEmployeeAppointment, getEmployeeBookings, getEmployeeProfile } from './controllers/employeeController';

const router = Router();

/// Employee
router.get('/employee/profile', EmployeeVerify, (req, res, next) => { getEmployeeProfile(req, res, next, false) })
router.get('/employee/getCurrentBooking', EmployeeVerify, (req, res, next) => { getEmployeeAppointment(req, res, next, false) })
router.get('/employee/getBookings', EmployeeVerify, (req, res, next) => { getEmployeeBookings(req, res, next, false) });

router.get('/employee/logout', EmployeeVerify, employeeLogout)

export default router;
