import { Router } from 'express';
import { EmployeeVerify } from '../../config/verifyToken';
import { getEmployeeAppointment, getEmployeeBookings, getEmployeeProfile } from './controllers/employeeController';

const router = Router();

/// Employee
router.get('/employee/profile', EmployeeVerify, (req, res, next) => { getEmployeeProfile(req, res, next, true) })
router.get('/employee/getCurrentBooking', EmployeeVerify, (req, res, next) => { getEmployeeAppointment(req, res, next, true) })
router.get('/employee/getBookings', EmployeeVerify, (req, res, next) => { getEmployeeBookings(req, res, next, true) });

export default router;
