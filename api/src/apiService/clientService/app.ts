import express, { Express } from 'express';
import routes from './routes';
import testRoutes from './testRoutes';

const clientApp = (app: Express) => {

  app.use('/api', routes)
  app.use('/test', testRoutes)

};

export default clientApp;
