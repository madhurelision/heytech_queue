import { Router } from 'express';
import {
  getMentorController,
  getMentorsController,
  getShopController
} from './controller/api-controller';

import { getAllExpertise, likeShopUser, unlikeShopUser, createToken, SearchShopQuery, SearchShopRegexQuery, getAllQueryActiveServices, getAllActiveServices, shopMobileLoginSignup, shopMobileLoginOtpVerify, getAllBarberExpertise, getAllShopListS } from '../shopService/controllers/shopController';
import { getAllBarberServices, getAllBarberTestServices, getAllService, SearchServiceQuery, SearchServiceRegexQuery } from '../shopService/controllers/serviceController';
import { getAllTags } from '../adminService/controllers/tagController';
import { getAllReviewTags } from '../adminService/controllers/reviewtagController';
import { getAllServiceTags } from '../adminService/controllers/serviceTagController';
import { getAllDescriptionTags } from '../adminService/controllers/descriptionTagController';
import { subscriberPost } from '../../modules/pushNotification';
import { getAllCombinedTagList } from './controller/metadaController';
import { getAllTagService } from '../adminService/controllers/tagServiceController';
import { getAllOffers, getAllServiceOffer } from '../adminService/controllers/offerController';
import { getAllBanners } from '../adminService/controllers/bannerController';
import { OpenUrl, OpenUrlWithoutToken } from '../adminService/controllers/testController';
import { verify } from '../../config/googleVerify';
import { employeeLoginId, employeeMobileLogin, employeeMobileLoginOtpVerify, employeeSignup, employeeSignupOtpVerify } from '../employeeService/controllers/employeeController';
import { getAllShopCategory } from '../adminService/controllers/shopCategoryController';
import { getAllAdminPage } from '../adminService/controllers/adminPageController';
import { CreateContact } from '../adminService/controllers/contactController';
import { getAllContactPage } from '../adminService/controllers/contactPageController';
import { customerMobileLogin, customerMobileLoginOtpVerify, CustomerSearchQuery, getAllCustomer, getSingleCustomerPackage } from '../customerService/controllers/customerController';
import { getAllAboutPage } from '../adminService/controllers/aboutPageController';
import { getAllServiceName } from '../adminService/controllers/serviceNameController';
import { getAllExpertiseName } from '../adminService/controllers/expertiseNameController';
import { StripePayment } from '../adminService/controllers/paymentController';
import { getAllFeatureShop, getSingleFeatureShop } from '../shopService/controllers/shopFeatureController';
import { getHostByName } from '../adminService/controllers/hostController';
import { customerCreateVerify } from '../../config/verifyToken';

const router = Router();

router.get('/get-mentor', (req, res, next) => { getMentorController(req, res, next, false) });
router.get('/get-mentors', (req, res, next) => { getMentorsController(req, res, next, false) });
router.get('/get-shops', (req, res, next) => { getShopController(req, res, next, false) })

router.get('/getAllExpertise', (req, res, next) => { getAllExpertise(req, res, next, false) })
router.get('/getBarberExpertise', (req, res, next) => { getAllBarberExpertise(req, res, next, false) })

router.get('/getAllService/:skip/:limit', (req, res, next) => { getAllService(req, res, next, false) })

router.post('/activeService/:skip/:limit', (req, res, next) => { getAllActiveServices(req, res, next, false) })
router.post('/getactiveService/:skip/:limit', (req, res, next) => { getAllActiveServices(req, res, next, false) })
router.get('/getBarberServices', (req, res, next) => { getAllBarberServices(req, res, next, false) })

//like unlike Shop
router.post('/likeShopUser/:id', (req, res, next) => { likeShopUser(req, res, next, false) })
router.post('/unlikeShopUser/:id', (req, res, next) => { unlikeShopUser(req, res, next, false) })

// createToken
router.get('/makeToken/:id', (req, res, next) => { createToken(req, res, next, false) })

// booking tag
router.get('/getAllTag', (req, res, next) => { getAllTags(req, res, next, false) });

// review tag
router.get('/getAllReviewTag', (req, res, next) => { getAllReviewTags(req, res, next, false) });

// service tag
router.get('/getAllServiceTag', (req, res, next) => { getAllServiceTags(req, res, next, false) })

// description tag
router.get('/getAllDescriptionTag', (req, res, next) => { getAllDescriptionTags(req, res, next, false) })

//tag service
router.get('/getAllTagService', (req, res, next) => { getAllTagService(req, res, next, false) })

// shop Query
router.get('/getShopQuery/:id', (req, res, next) => { SearchShopQuery(req, res, next, false) })
router.get('/getShopRegexQuery/:id', (req, res, next) => { SearchShopRegexQuery(req, res, next, false) })

// service Query
router.get('/getServiceQuery/:id', (req, res, next) => { SearchServiceQuery(req, res, next, false) })
router.get('/getServiceRegexQuery/:id', (req, res, next) => { SearchServiceRegexQuery(req, res, next, false) })
router.get('/activeServiceRegexQuery/:id', (req, res, next) => { getAllQueryActiveServices(req, res, next, false) })

//metadata
router.get('/getAllMetadata', (req, res, next) => { getAllCombinedTagList(req, res, next, false) })

// push
router.post('/subscribe', (req, res, next) => { subscriberPost(req, res, next, false) })

//offer User
router.get('/getAllOffer', (req, res, next) => { getAllOffers(req, res, next, false) })
router.get('/getAllServiceOffer', (req, res, next) => { getAllServiceOffer(req, res, next, false) })

//banner User
router.get('/getAllBanner', (req, res, next) => { getAllBanners(req, res, next, false) })

// dummy Url
router.get('/open/:id', (req, res, next) => { OpenUrlWithoutToken(req, res, next, false) })
router.get('/open/:id/:token', verify, (req, res, next) => { OpenUrl(req, res, next, false) })


// shop Mobile Login
router.post('/mobileShopLogin', (req, res, next) => { shopMobileLoginSignup(req, res, next, false) })
router.post('/mobileShopLoginVerify', (req, res, next) => { shopMobileLoginOtpVerify(req, res, next, false) })


// employee Mobile Login
router.post('/employeeLoginId', (req, res, next) => { employeeLoginId(req, res, next, false) })
router.post('/mobileEmployeeLogin', (req, res, next) => { employeeMobileLogin(req, res, next, false) })
router.post('/mobileEmployeeLoginVerify', (req, res, next) => { employeeMobileLoginOtpVerify(req, res, next, false) })
router.post('/employeeSignup', employeeSignup)
router.post('/employeeVerify', employeeSignupOtpVerify)

//customer Mobile Login
router.post('/mobileCustomerLogin', (req, res, next) => { customerMobileLogin(req, res, next, false) })
router.post('/mobileCustomerLoginVerify', (req, res, next) => { customerMobileLoginOtpVerify(req, res, next, false) })

// shop category
router.get('/getAllShopCategory', (req, res, next) => { getAllShopCategory(req, res, next, false) })

//admin page
router.get('/getAllAdminPage', (req, res, next) => { getAllAdminPage(req, res, next, false) })

//contact
router.post('/createContact', (req, res, next) => { CreateContact(req, res, next, false) })

//contact page
router.get('/getAllContactPage', (req, res, next) => { getAllContactPage(req, res, next, false) })

//About page
router.get('/getAllAboutPage', (req, res, next) => { getAllAboutPage(req, res, next, false) })

//test
router.get('/getTestService/:skip/:limit', (req, res, next) => { getAllBarberTestServices(req, res, next, false) })

//service Names
router.get('/getAllServiceName', (req, res, next) => { getAllServiceName(req, res, next, false) })

// expertise Name
router.get('/getAllExpertiseName', (req, res, next) => { getAllExpertiseName(req, res, next, false) })
//shops All
router.get('/getAllShopsList', getAllShopListS)

router.post('/shopPay', customerCreateVerify, StripePayment)

router.get('/getAllShopFeatureList', getAllFeatureShop)
router.get('/getSingleShopFeature/:id', getSingleFeatureShop)

router.post('/customerSearchQuery/:id', CustomerSearchQuery)
router.get('/getAllCustomer/:skip/:limit', getAllCustomer)
router.get('/getSingleCustomerPackage/:id', getSingleCustomerPackage)

router.post('/getHostName', getHostByName)

export default router;
