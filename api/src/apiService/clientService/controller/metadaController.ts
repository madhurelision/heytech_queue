import { NextFunction, Request, Response } from 'express';
import { DescriptionTagModel } from '../../../Models/descriptionTag';
import { ReviewTagModel } from '../../../Models/ReviewTag';
import { ServiceTagModel } from '../../../Models/serviceTag';
import { TagModel } from '../../../Models/Tag';
import { TagServiceModel } from '../../../Models/tagService';
import { ShopModel } from '../../../Models/User';
import { ServiceModel } from "../../../Models/Service";
import { OfferModel } from '../../../Models/Offer';
import { BannerModel } from '../../../Models/Banner';
import { ShopCategoryModel } from '../../../Models/ShopCategory';
import { AdminPageModel } from '../../../Models/AdminPage'
import { ContactPageModel } from '../../../Models/Contact';
import { AboutPageModel } from '../../../Models/AboutPage';
import { ExpertiseNameModel, ServiceNameModel } from '../../../Models/NamesPage';
import { SsrStripeModel } from '../../../Models/SsrPage';

export const getAllCombinedTagList = (req: Request, res: Response, next: NextFunction, verify: any) => {

  const Promise1 = new Promise((resolve, reject) => {
    DescriptionTagModel.find({ deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise2 = new Promise((resolve, reject) => {
    ReviewTagModel.find({ deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise3 = new Promise((resolve, reject) => {
    ServiceTagModel.find({ deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise4 = new Promise((resolve, reject) => {
    TagModel.find({ deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise5 = new Promise((resolve, reject) => {
    TagServiceModel.find({ deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise6 = new Promise((resolve, reject) => {
    ShopModel.aggregate([
      {
        '$match': {
          'status': true
        }
      },
      {
        '$unwind': {
          'path': '$expertise'
        }
      }, {
        '$group': {
          '_id': 'null',
          'data': {
            '$push': {
              'label': '$expertise',
              'value': '$expertise',
              'type': '$type'
            }
          }
        }
      },
      // {
      //   '$addFields': {
      //     'data': {
      //       '$map': {
      //         'input': '$data',
      //         'as': 'num',
      //         'in': {
      //           'label': '$$num',
      //           'value': '$$num'
      //         }
      //       }
      //     }
      //   }
      // }
    ]).then((cc: any) => {
      resolve((cc.length > 0) ? cc[0].data : [])
    }).catch((err) => {
      reject(err)
    })
  })

  const Promise7 = new Promise((resolve, reject) => {
    ShopModel.aggregate([
      {
        '$match': {
          'status': true
        }
      }, {
        '$lookup': {
          'from': 'services',
          'localField': 'topics',
          'foreignField': '_id',
          'as': 'topics'
        }
      }, {
        '$unwind': {
          'path': '$topics'
        }
      }, {
        '$addFields': {
          'topics.user_name': '$shop_name',
          'topics.type': '$type',
        }
      }, {
        '$group': {
          '_id': null,
          'data': {
            '$push': '$topics'
          }
        }
      }
    ]).then((cc: any) => {
      resolve((cc.length > 0) ? cc[0].data : [])
    }).catch((err) => {
      reject(err)
    })
  })

  const Promise8 = new Promise((resolve, reject) => {
    ServiceModel.aggregate([
      {
        '$match': {
          'deleted': false
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'user_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$lookup': {
          'from': 'admins',
          'localField': 'admin_id',
          'foreignField': '_id',
          'as': 'admin'
        }
      }, {
        '$addFields': {
          'user_name': {
            '$arrayElemAt': [
              {
                '$cond': {
                  'if': {
                    '$eq': [
                      {
                        '$size': '$shop_id'
                      }, 1
                    ]
                  },
                  'then': '$shop_id.shop_name',
                  'else': '$admin.name'
                }
              }, 0
            ]
          },
          'type': {
            '$arrayElemAt': [
              '$shop_id.type', 0
            ]
          }
        }
      },

      {
        '$facet': {
          'metadata': [
            {
              '$group': {
                '_id': null,
                'total': {
                  '$sum': 1
                }
              }
            }
          ],
          'service': [
            {
              '$sort': {
                '_id': 1
              }
            },
            {
              '$skip': 0
            }, {
              '$limit': 50
            }
          ]
        }
      }, {
        '$project': {
          'service': 1,
          'total': {
            '$arrayElemAt': [
              '$metadata.total', 0
            ]
          }
        }
      }
    ]).exec((err, item: any) => {

      if (err) {
        console.log('Service Find Err', err)
        return reject(err)
      }

      resolve(item.length > 0 ? item[0].service : [])
    })
  })

  const Promise9 = new Promise((resolve, reject) => {
    OfferModel.find({ deleted: false, status: true }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise10 = new Promise((resolve, reject) => {
    BannerModel.find({ deleted: false, status: true }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise11 = new Promise((resolve, reject) => {
    ShopCategoryModel.aggregate([
      {
        '$match': {
          'status': true,
          'deleted': false
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'let': {
            'id': '$_id'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$type', '$$id'
                      ]
                    }, {
                      '$eq': [
                        '$status', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shop'
        }
      }, {
        '$addFields': {
          'count': {
            '$size': '$shop'
          }
        }
      }
    ]).exec((err, item: any) => {

      if (err) {
        console.log('Shop Category Find Err', err)
        return reject(err)
      }

      resolve(item)
    })
  })

  const Promise12 = new Promise((resolve, reject) => {
    AdminPageModel.findOne({ deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise13 = new Promise((resolve, reject) => {
    ContactPageModel.findOne({ deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise14 = new Promise((resolve, reject) => {
    AboutPageModel.findOne({ deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise15 = new Promise((resolve, reject) => {
    ServiceNameModel.find({ deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise16 = new Promise((resolve, reject) => {
    ExpertiseNameModel.find({ deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise17 = new Promise((resolve, reject) => {
    ShopModel.find({ deleted: false, status: true }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise18 = new Promise((resolve, reject) => {

    SsrStripeModel.findOne({ status: true, deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })

  })

  Promise.allSettled([Promise1, Promise2, Promise3, Promise4, Promise5, Promise6, Promise7, Promise8, Promise9, Promise10, Promise11, Promise12, Promise13, Promise14, Promise15, Promise16, Promise17, Promise18]).then((cc: any) => {
    // console.log('ss', cc)
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        descriptionTagList: { tag: cc[0].value },
        reviewTagList: { review: cc[1].value },
        serviceTagList: { tag: cc[2].value },
        tagList: { tag: cc[3].value },
        tagServiceList: { tag: cc[4].value },
        serviceList: cc[7].value,
        activeServiceList: cc[6].value,
        expertiseList: cc[5].value,
        offerList: { offer: cc[8].value },
        bannerList: { banner: cc[9].value },
        shopCategoryList: { category: cc[10].value },
        adminPageList: { admin: cc[11].value },
        contactPageList: { contact: cc[12].value },
        aboutPageList: { about: cc[13].value },
        serviceNameList: { serviceName: cc[14].value },
        expertiseNameList: { expertiseName: cc[15].value },
        shop_list: { shop: cc[16].value },
        stripe: cc[17].value,
      }
    })

  }).catch((err) => {
    console.log('Tag Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}