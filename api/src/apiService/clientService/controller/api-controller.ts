import { NextFunction, Request, Response } from 'express';
import { ShopModel } from '../../../Models/User';
import { TopicModel } from '../../../Models/Topics';
import { isValidObjectId } from 'mongoose';
import mongoose from 'mongoose'
import { performance } from 'perf_hooks'
import { LogQueryModel } from '../../../Models/LogData';

// http://localhost:5000/api/get-mentors?expertise=Leadership
// export const getMentorsController = async (req: Request, res: Response, next:NextFunction, verify:any) => {
//   const expertise = req.query.expertise?.toString() || 'All';
//   const topic: number = Number(req.query.topic?.toString() || -1);
//   let mentors_;
//   if (topic == -1 && expertise == 'All') mentors_ = await MentorModel.find({});
//   else if (expertise == 'All')
//     mentors_ = await MentorModel.find({ topics: topic });
//   else if (topic == -1)
//     mentors_ = await MentorModel.find({ expertise: expertise });
//   else
//     mentors_ = await MentorModel.find({ topics: topic })
//       .where('expertise')
//       .equals(expertise);

//   const mentors = mentors_.map(
//     ({
//       _id,
//       first_name,
//       last_name,
//       company,
//       job_title,
//       expertise,
//       image_link,
//       topics,
//     }) => {
//       return {
//         _id,
//         first_name,
//         last_name,
//         company,
//         job_title,
//         expertise,
//         image_link,
//         topics,
//       };
//     },
//   );

//   res.json(mentors);
// };

export const getMentorsController = async (req: Request, res: Response, next: NextFunction, verify: any) => {
  const expertise = req.query.expertise?.toString() || 'All';
  const topic: any = (req.query.topic == 'All') ? 'All' : req.query.topic;
  const name = req.query.name?.toString() || 'All';

  console.log('expertise', expertise, 'topic', topic, 'name', name)

  let mentors_;
  if (topic == 'All' && expertise == 'All') mentors_ = await ShopModel.aggregate([
    {
      '$match': {
        'status': true,
        deleted: false
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'shopreview'
      }
    }, {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$shopreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalShop': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$shopreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$shopreview'
                  }
                ]
              }
            }
          }
        }
      }
    }, {
      '$lookup': {
        'from': 'banners',
        'let': {
          'main': '$_id'
        },
        pipeline: [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$deleted', false
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'value'
      }
    }, {
      '$sort': {
        'order': 1
      }
    }]).exec();
  else if (expertise == 'All')
    mentors_ = await ShopModel.aggregate([
      {
        $match: {
          topics: { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
          status: true
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]).exec();
  else if (topic == 'All')
    mentors_ = await ShopModel.aggregate([
      {
        $match: {
          expertise: expertise,
          status: true
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]).exec();
  else
    // mentors_ = await ShopModel.find({ topics: topic })
    //   .where('expertise')
    //   .equals(expertise);
    mentors_ = await ShopModel.aggregate([
      {
        $match: {
          topics: { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
          expertise: expertise,
          status: true
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]).exec();

  const mentors = mentors_.map(
    ({
      _id,
      first_name,
      last_name,
      shop_name,
      company,
      job_title,
      expertise,
      image_link,
      topics,
      like,
      totalShop,
      banner,
      type,
      value,
      open,
      time_slot
    }) => {
      return {
        _id,
        first_name,
        last_name,
        shop_name,
        company,
        job_title,
        expertise,
        image_link: image_link,
        topics,
        like,
        totalShop,
        banner,
        type,
        value,
        open,
        time_slot
      };
    },
  );

  res.json(mentors);
};

// http://localhost:5000/api/get-topics?textSearch=a
export const getTopicsController = async (req: Request, res: Response, next: NextFunction, verify: any) => {
  const searchString = req.query.textSearch?.toString() || '';
  let topics;
  if (searchString)
    topics = await TopicModel.find({ $text: { $search: searchString } });

  res.json(topics);
};

// http://localhost:5000/api/get-mentor?id=61a211ab8e41a1fc1c49c2a4
// export const getMentorController = async (req: Request, res: Response, next:NextFunction, verify:any) => {
//   const id = req.query.id?.toString() || '';
//   let mentor;
//   if (id && isValidObjectId(id)) mentor = await MentorModel.findById(id);
//   res.json(mentor);
// };

// export const getMentorController = async (req: Request, res: Response, next:NextFunction, verify:any) => {
//   const id = req.query.id?.toString() || '';
//   let mentor;
//   if (id && isValidObjectId(id)) mentor = await ShopModel.findById(id).populate('topics');
//   if (mentor || mentor != null || mentor != undefined || Object.keys(mentor).length > 0) {
//     if (mentor.image_link.match('https')) {
//       mentor.image_link = mentor.image_link
//     } else {
//       mentor.image_link = SERVER_URL + mentor.image_link
//     }
//     if (mentor.topics.length > 0) {
//       mentor.topics.map((cc: any) => {
//         if (cc.image.match(SERVER_URL)) {
//           cc.image = cc.image
//         } else {
//           cc.image = SERVER_URL + cc.image
//         }
//       })
//     }
//   }
//   res.json(mentor);
// };

export const getMentorController = async (req: Request, res: Response, next: NextFunction, verify: any) => {
  const id = req.query.id?.toString() || '';
  let mentor: any;
  if (id && isValidObjectId(id))
    mentor = await ShopModel.aggregate([{
      '$match': {
        '_id': new mongoose.Types.ObjectId(id)
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    }, {
      '$lookup': {
        'from': 'shoppackages',
        'localField': 'user_information',
        'foreignField': 'user_id',
        'as': 'pack'
      }
    }, {
      '$lookup': {
        'from': 'galleries',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$status', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'gallery'
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },
      // {
      //   '$unwind': {
      //     'path': '$topics',
      //     'preserveNullAndEmptyArrays': true
      //   }
      // }, {
      //   '$unwind': {
      //     'path': '$topics.seat',
      //     'preserveNullAndEmptyArrays': true
      //   }
      // }, {
      //   '$group': {
      //     '_id': '$_id',
      //     'topics': {
      //       '$push': '$topics'
      //     },
      //     'first_name': {
      //       '$first': '$first_name'
      //     },
      //     'last_name': {
      //       '$first': '$last_name'
      //     },
      //     'shop_name': {
      //       '$first': '$shop_name'
      //     },
      //     'image_link': {
      //       '$first': '$image_link'
      //     },
      //     'email': {
      //       '$first': '$email'
      //     },
      //     'company': {
      //       '$first': '$company'
      //     },
      //     'description': {
      //       '$first': '$description'
      //     },
      //     'expertise': {
      //       '$first': '$expertise'
      //     },
      //     'language': {
      //       '$first': '$language'
      //     },
      //     'like': {
      //       '$first': '$like'
      //     },
      //     'instagram': {
      //       '$first': '$instagram'
      //     },
      //     'is_mentoring': {
      //       '$first': '$is_mentoring'
      //     },
      //     'time_slot': {
      //       '$first': '$time_slot'
      //     },
      //     'user_information': {
      //       '$first': '$user_information'
      //     },
      //     'waiting_number': {
      //       '$first': '$waiting_number'
      //     },
      //     'waiting_key': {
      //       '$first': '$waiting_key'
      //     },
      //     'waiting_time': {
      //       '$first': '$waiting_time'
      //     },
      //     'code': {
      //       '$first': '$code'
      //     },
      //     'location': {
      //       '$first': '$location'
      //     },
      //     'lattitude': {
      //       '$first': '$lattitude'
      //     },
      //     'longitude': {
      //       '$first': '$longitude'
      //     },
      //     'mobile': {
      //       '$first': '$mobile'
      //     },
      //     'seat': {
      //       '$first': '$seat'
      //     },
      //     'queue': {
      //       '$first': '$queue'
      //     },
      //     'open': {
      //       '$first': '$open'
      //     },
      //     'appointment': {
      //       '$first': '$appointment'
      //     },
      //     'mobile_verify': {
      //       '$first': '$mobile_verify'
      //     },
      //     'status': {
      //       '$first': '$status'
      //     },
      //     'banner': {
      //       '$first': '$banner'
      //     },
      //     'type': {
      //       '$first': '$type'
      //     },
      //     'review': {
      //       '$first': '$review'
      //     },
      //     'gallery': {
      //       '$first': '$gallery'
      //     }
      //   }
      // }
    ]).exec();

  //console.log('mentor', mentor)
  res.json(mentor?.length > 0 ? mentor[0] : {});
}

export const getShopController = async (req: Request, res: Response, next: NextFunction, verify: any) => {
  const expertise = req.query.expertise?.toString() || 'All';
  const topic: any = (req.query.topic == 'All') ? 'All' : req.query.topic;
  const type = req.query.type?.toString() || 'All';
  const position: any = (req.query.position == 'All') ? 'All' : req.query.position;
  const distance: any = (req.query.distance == 'All') ? 'All' : req.query.distance;
  console.log('position', position)
  console.log('distance', distance)
  var startTime = performance.now()
  var query: any = []

  if (topic == 'All' && expertise == 'All' && type == 'All' && distance == 'All') {
    query = [
      {
        '$match': {
          'status': true,
          deleted: false
        }
      },
      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }
    ]
  } else if (topic == 'All' && expertise == 'All' && distance == 'All') {
    query = [
      {
        $match: {
          type: new mongoose.Types.ObjectId(type),
          'status': true,
          deleted: false
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]
  } else if (topic == 'All' && type == 'All' && distance == 'All') {
    query = [
      {
        '$match': {
          'expertise': expertise,
          'status': true,
          deleted: false
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]
  } else if (expertise == 'All' && type == 'All' && distance == 'All') {
    query = [
      {
        '$match': {
          'topics': { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
          'status': true,
          deleted: false
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]
  } else if (expertise == 'All' && type == 'All' && topic == 'All') {
    query = [
      {
        '$geoNear': {
          'near': {
            'type': 'Point',
            'coordinates': position.map((cc: any) => parseFloat(cc))
          },
          'key': 'locate',
          'distanceField': 'distance',
          'minDistance': 100,
          'maxDistance': parseInt(distance),
          'spherical': true
        }
      },
      {
        '$match': {
          'status': true,
          deleted: false
        }
      },
      {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]
  } else if (topic == 'All' && distance == 'All') {
    query = [
      {
        '$match': {
          'expertise': expertise,
          'type': new mongoose.Types.ObjectId(type),
          'status': true,
          deleted: false
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]
  } else if (expertise == 'All' && distance == 'All') {
    query = [
      {
        '$match': {
          'topics': { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
          'type': new mongoose.Types.ObjectId(type),
          'status': true,
          deleted: false
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]
  } else if (type == 'All' && distance == 'All') {
    query = [
      {
        '$match': {
          'topics': { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
          'expertise': expertise,
          'status': true,
          deleted: false
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]
  } else if (topic == 'All' && expertise == 'All') {
    query = [
      {
        '$geoNear': {
          'near': {
            'type': 'Point',
            'coordinates': position.map((cc: any) => parseFloat(cc))
          },
          'key': 'locate',
          'distanceField': 'distance',
          'minDistance': 100,
          'maxDistance': parseInt(distance),
          'spherical': true
        }
      },
      {
        '$match': {
          'type': new mongoose.Types.ObjectId(type),
          'status': true,
          deleted: false
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]
  } else if (type == 'All' && expertise == 'All') {
    query = [
      {
        '$geoNear': {
          'near': {
            'type': 'Point',
            'coordinates': position.map((cc: any) => parseFloat(cc))
          },
          'key': 'locate',
          'distanceField': 'distance',
          'minDistance': 100,
          'maxDistance': parseInt(distance),
          'spherical': true
        }
      },
      {
        '$match': {
          'topics': { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
          'status': true,
          deleted: false
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]
  } else if (type == 'All' && topic == 'All') {
    query = [
      {
        '$geoNear': {
          'near': {
            'type': 'Point',
            'coordinates': position.map((cc: any) => parseFloat(cc))
          },
          'key': 'locate',
          'distanceField': 'distance',
          'minDistance': 100,
          'maxDistance': parseInt(distance),
          'spherical': true
        }
      },
      {
        '$match': {
          'expertise': expertise,
          'status': true,
          deleted: false
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]
  } else if (topic == 'All') {
    query = [
      {
        '$geoNear': {
          'near': {
            'type': 'Point',
            'coordinates': position.map((cc: any) => parseFloat(cc))
          },
          'key': 'locate',
          'distanceField': 'distance',
          'minDistance': 100,
          'maxDistance': parseInt(distance),
          'spherical': true
        }
      },
      {
        '$match': {
          'expertise': expertise,
          'type': new mongoose.Types.ObjectId(type),
          'status': true,
          deleted: false
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]
  } else if (expertise == 'All') {
    query = [
      {
        '$geoNear': {
          'near': {
            'type': 'Point',
            'coordinates': position.map((cc: any) => parseFloat(cc))
          },
          'key': 'locate',
          'distanceField': 'distance',
          'minDistance': 100,
          'maxDistance': parseInt(distance),
          'spherical': true
        }
      },
      {
        '$match': {
          'topics': { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
          'type': new mongoose.Types.ObjectId(type),
          'status': true,
          deleted: false
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]
  } else if (type == 'All') {
    query = [
      {
        '$geoNear': {
          'near': {
            'type': 'Point',
            'coordinates': position.map((cc: any) => parseFloat(cc))
          },
          'key': 'locate',
          'distanceField': 'distance',
          'minDistance': 100,
          'maxDistance': parseInt(distance),
          'spherical': true
        }
      },
      {
        '$match': {
          'topics': { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
          'expertise': expertise,
          'status': true,
          deleted: false
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]
  } else if (distance == 'All') {
    query = [
      {
        '$match': {
          'topics': { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
          'type': new mongoose.Types.ObjectId(type),
          'expertise': expertise,
          'status': true,
          deleted: false
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]
  } else {
    query = [
      {
        '$geoNear': {
          'near': {
            'type': 'Point',
            'coordinates': position.map((cc: any) => parseFloat(cc))
          },
          'key': 'locate',
          'distanceField': 'distance',
          'minDistance': 100,
          'maxDistance': parseInt(distance),
          'spherical': true
        }
      },
      {
        '$match': {
          'topics': { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
          'type': new mongoose.Types.ObjectId(type),
          'expertise': expertise,
          'status': true,
          deleted: false
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'banners',
          'let': {
            'main': '$_id'
          },
          pipeline: [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$deleted', false
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'value'
        }
      }, {
        '$sort': {
          'order': 1
        }
      }]
  }

  let mentors_ = await ShopModel.aggregate(query).exec();

  var endTime = (performance.now() - startTime).toFixed();

  const clog = await LogQueryModel.create({
    queryName: query,
    collectionName: 'shops',
    startTime: startTime,
    endTime: parseInt(endTime)
  })

  const mentors = mentors_.map(
    ({
      _id,
      first_name,
      last_name,
      shop_name,
      company,
      job_title,
      expertise,
      image_link,
      topics,
      like,
      totalShop,
      banner,
      type,
      value,
      open,
      time_slot
    }) => {
      return {
        _id,
        first_name,
        last_name,
        shop_name,
        company,
        job_title,
        expertise,
        image_link: image_link,
        topics,
        like,
        totalShop,
        banner,
        type,
        value,
        open,
        time_slot
      };
    },
  );

  res.json(mentors);
};



// if (topic == 'All' && expertise == 'All' && type == 'All') {
//   query = [
//     {
//       '$match': {
//         'status': true,
//deleted: false
//       }
//     },
//     {
//       '$lookup': {
//         'from': 'reviews',
//         'let': {
//           'main': '$user_information'
//         },
//         'pipeline': [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$user_review', true
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'shopreview'
//       }
//     }, {
//       '$addFields': {
//         'shop_count': {
//           '$reduce': {
//             'input': '$shopreview',
//             'initialValue': 0,
//             'in': {
//               '$sum': [
//                 '$$value', '$$this.rating'
//               ]
//             }
//           }
//         }
//       }
//     }, {
//       '$addFields': {
//         'totalShop': {
//           '$cond': {
//             'if': {
//               '$eq': [
//                 {
//                   '$size': '$shopreview'
//                 }, 0
//               ]
//             },
//             'then': 0,
//             'else': {
//               '$ceil': {
//                 '$divide': [
//                   '$shop_count', {
//                     '$size': '$shopreview'
//                   }
//                 ]
//               }
//             }
//           }
//         }
//       }
//     }, {
//       '$lookup': {
//         'from': 'banners',
//         'let': {
//           'main': '$_id'
//         },
//         pipeline: [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$deleted', false
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'value'
//       }
//     }]
// } else if (topic == 'All' && expertise == 'All') {
//   query = [
//     {
//       $match: {
//         type: new mongoose.Types.ObjectId(type),
//         'status': true,
//deleted: false
//       }
//     }, {
//       '$lookup': {
//         'from': 'reviews',
//         'let': {
//           'main': '$user_information'
//         },
//         'pipeline': [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$user_review', true
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'shopreview'
//       }
//     }, {
//       '$addFields': {
//         'shop_count': {
//           '$reduce': {
//             'input': '$shopreview',
//             'initialValue': 0,
//             'in': {
//               '$sum': [
//                 '$$value', '$$this.rating'
//               ]
//             }
//           }
//         }
//       }
//     }, {
//       '$addFields': {
//         'totalShop': {
//           '$cond': {
//             'if': {
//               '$eq': [
//                 {
//                   '$size': '$shopreview'
//                 }, 0
//               ]
//             },
//             'then': 0,
//             'else': {
//               '$ceil': {
//                 '$divide': [
//                   '$shop_count', {
//                     '$size': '$shopreview'
//                   }
//                 ]
//               }
//             }
//           }
//         }
//       }
//     }, {
//       '$lookup': {
//         'from': 'banners',
//         'let': {
//           'main': '$_id'
//         },
//         pipeline: [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$deleted', false
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'value'
//       }
//     }]
// } else if (topic == 'All' && type == 'All') {
//   query = [
//     {
//       '$match': {
//         'expertise': expertise,
//         'status': true,
//deleted: false
//       }
//     }, {
//       '$lookup': {
//         'from': 'reviews',
//         'let': {
//           'main': '$user_information'
//         },
//         'pipeline': [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$user_review', true
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'shopreview'
//       }
//     }, {
//       '$addFields': {
//         'shop_count': {
//           '$reduce': {
//             'input': '$shopreview',
//             'initialValue': 0,
//             'in': {
//               '$sum': [
//                 '$$value', '$$this.rating'
//               ]
//             }
//           }
//         }
//       }
//     }, {
//       '$addFields': {
//         'totalShop': {
//           '$cond': {
//             'if': {
//               '$eq': [
//                 {
//                   '$size': '$shopreview'
//                 }, 0
//               ]
//             },
//             'then': 0,
//             'else': {
//               '$ceil': {
//                 '$divide': [
//                   '$shop_count', {
//                     '$size': '$shopreview'
//                   }
//                 ]
//               }
//             }
//           }
//         }
//       }
//     }, {
//       '$lookup': {
//         'from': 'banners',
//         'let': {
//           'main': '$_id'
//         },
//         pipeline: [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$deleted', false
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'value'
//       }
//     }]
// } else if (expertise == 'All' && type == 'All') {
//   query = [
//     {
//       '$match': {
//         'topics': { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
//         'status': true,
//deleted: false
//       }
//     }, {
//       '$lookup': {
//         'from': 'reviews',
//         'let': {
//           'main': '$user_information'
//         },
//         'pipeline': [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$user_review', true
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'shopreview'
//       }
//     }, {
//       '$addFields': {
//         'shop_count': {
//           '$reduce': {
//             'input': '$shopreview',
//             'initialValue': 0,
//             'in': {
//               '$sum': [
//                 '$$value', '$$this.rating'
//               ]
//             }
//           }
//         }
//       }
//     }, {
//       '$addFields': {
//         'totalShop': {
//           '$cond': {
//             'if': {
//               '$eq': [
//                 {
//                   '$size': '$shopreview'
//                 }, 0
//               ]
//             },
//             'then': 0,
//             'else': {
//               '$ceil': {
//                 '$divide': [
//                   '$shop_count', {
//                     '$size': '$shopreview'
//                   }
//                 ]
//               }
//             }
//           }
//         }
//       }
//     }, {
//       '$lookup': {
//         'from': 'banners',
//         'let': {
//           'main': '$_id'
//         },
//         pipeline: [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$deleted', false
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'value'
//       }
//     }]
// } else if (topic == 'All') {
//   query = [
//     {
//       '$match': {
//         'expertise': expertise,
//         'type': new mongoose.Types.ObjectId(type),
//         'status': true,
//deleted: false
//       }
//     }, {
//       '$lookup': {
//         'from': 'reviews',
//         'let': {
//           'main': '$user_information'
//         },
//         'pipeline': [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$user_review', true
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'shopreview'
//       }
//     }, {
//       '$addFields': {
//         'shop_count': {
//           '$reduce': {
//             'input': '$shopreview',
//             'initialValue': 0,
//             'in': {
//               '$sum': [
//                 '$$value', '$$this.rating'
//               ]
//             }
//           }
//         }
//       }
//     }, {
//       '$addFields': {
//         'totalShop': {
//           '$cond': {
//             'if': {
//               '$eq': [
//                 {
//                   '$size': '$shopreview'
//                 }, 0
//               ]
//             },
//             'then': 0,
//             'else': {
//               '$ceil': {
//                 '$divide': [
//                   '$shop_count', {
//                     '$size': '$shopreview'
//                   }
//                 ]
//               }
//             }
//           }
//         }
//       }
//     }, {
//       '$lookup': {
//         'from': 'banners',
//         'let': {
//           'main': '$_id'
//         },
//         pipeline: [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$deleted', false
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'value'
//       }
//     }]
// } else if (expertise == 'All') {
//   query = [
//     {
//       '$match': {
//         'topics': { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
//         'type': new mongoose.Types.ObjectId(type),
//         'status': true,
//deleted: false
//       }
//     }, {
//       '$lookup': {
//         'from': 'reviews',
//         'let': {
//           'main': '$user_information'
//         },
//         'pipeline': [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$user_review', true
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'shopreview'
//       }
//     }, {
//       '$addFields': {
//         'shop_count': {
//           '$reduce': {
//             'input': '$shopreview',
//             'initialValue': 0,
//             'in': {
//               '$sum': [
//                 '$$value', '$$this.rating'
//               ]
//             }
//           }
//         }
//       }
//     }, {
//       '$addFields': {
//         'totalShop': {
//           '$cond': {
//             'if': {
//               '$eq': [
//                 {
//                   '$size': '$shopreview'
//                 }, 0
//               ]
//             },
//             'then': 0,
//             'else': {
//               '$ceil': {
//                 '$divide': [
//                   '$shop_count', {
//                     '$size': '$shopreview'
//                   }
//                 ]
//               }
//             }
//           }
//         }
//       }
//     }, {
//       '$lookup': {
//         'from': 'banners',
//         'let': {
//           'main': '$_id'
//         },
//         pipeline: [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$deleted', false
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'value'
//       }
//     }]
// } else if (type == 'All') {
//   query = [
//     {
//       '$match': {
//         'topics': { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
//         'expertise': expertise,
//         'status': true,
//deleted: false
//       }
//     }, {
//       '$lookup': {
//         'from': 'reviews',
//         'let': {
//           'main': '$user_information'
//         },
//         'pipeline': [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$user_review', true
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'shopreview'
//       }
//     }, {
//       '$addFields': {
//         'shop_count': {
//           '$reduce': {
//             'input': '$shopreview',
//             'initialValue': 0,
//             'in': {
//               '$sum': [
//                 '$$value', '$$this.rating'
//               ]
//             }
//           }
//         }
//       }
//     }, {
//       '$addFields': {
//         'totalShop': {
//           '$cond': {
//             'if': {
//               '$eq': [
//                 {
//                   '$size': '$shopreview'
//                 }, 0
//               ]
//             },
//             'then': 0,
//             'else': {
//               '$ceil': {
//                 '$divide': [
//                   '$shop_count', {
//                     '$size': '$shopreview'
//                   }
//                 ]
//               }
//             }
//           }
//         }
//       }
//     }, {
//       '$lookup': {
//         'from': 'banners',
//         'let': {
//           'main': '$_id'
//         },
//         pipeline: [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$deleted', false
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'value'
//       }
//     }]
// } else {
//   query = [
//     {
//       '$match': {
//         'topics': { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
//         'type': new mongoose.Types.ObjectId(type),
//         'expertise': expertise,
//         'status': true,
//deleted: false
//       }
//     }, {
//       '$lookup': {
//         'from': 'reviews',
//         'let': {
//           'main': '$user_information'
//         },
//         'pipeline': [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$user_review', true
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'shopreview'
//       }
//     }, {
//       '$addFields': {
//         'shop_count': {
//           '$reduce': {
//             'input': '$shopreview',
//             'initialValue': 0,
//             'in': {
//               '$sum': [
//                 '$$value', '$$this.rating'
//               ]
//             }
//           }
//         }
//       }
//     }, {
//       '$addFields': {
//         'totalShop': {
//           '$cond': {
//             'if': {
//               '$eq': [
//                 {
//                   '$size': '$shopreview'
//                 }, 0
//               ]
//             },
//             'then': 0,
//             'else': {
//               '$ceil': {
//                 '$divide': [
//                   '$shop_count', {
//                     '$size': '$shopreview'
//                   }
//                 ]
//               }
//             }
//           }
//         }
//       }
//     }, {
//       '$lookup': {
//         'from': 'banners',
//         'let': {
//           'main': '$_id'
//         },
//         pipeline: [
//           {
//             '$match': {
//               '$expr': {
//                 '$and': [
//                   {
//                     '$eq': [
//                       '$shop_id', '$$main'
//                     ]
//                   }, {
//                     '$eq': [
//                       '$deleted', false
//                     ]
//                   }
//                 ]
//               }
//             }
//           }
//         ],
//         'as': 'value'
//       }
//     }]
// }