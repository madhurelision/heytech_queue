import { Router } from 'express';
import {
  getMentorController,
  getMentorsController,
  getShopController
} from './controller/api-controller';

import { getAllExpertise, likeShopUser, unlikeShopUser, createToken, SearchShopQuery, SearchShopRegexQuery, getAllQueryActiveServices, getAllActiveServices, shopMobileLoginSignup, shopMobileLoginOtpVerify } from '../shopService/controllers/shopController';
import { getAllService, SearchServiceQuery, SearchServiceRegexQuery } from '../shopService/controllers/serviceController';
import { getAllTags } from '../adminService/controllers/tagController';
import { getAllReviewTags } from '../adminService/controllers/reviewtagController';
import { getAllServiceTags } from '../adminService/controllers/serviceTagController';
import { getAllDescriptionTags } from '../adminService/controllers/descriptionTagController';
import { subscriberPost } from '../../modules/pushNotification';
import { getAllCombinedTagList } from './controller/metadaController';
import { getAllTagService } from '../adminService/controllers/tagServiceController';
import { getAllOffers, getAllServiceOffer } from '../adminService/controllers/offerController';
import { getAllBanners } from '../adminService/controllers/bannerController';
import { OpenUrl, OpenUrlWithoutToken } from '../adminService/controllers/testController';
import { verify } from '../../config/googleVerify';
import { employeeLoginId, employeeMobileLogin, employeeMobileLoginOtpVerify } from '../employeeService/controllers/employeeController';
import { getAllShopCategory } from '../adminService/controllers/shopCategoryController';
import { getAllAdminPage } from '../adminService/controllers/adminPageController';
import { CreateContact } from '../adminService/controllers/contactController';
import { getAllContactPage } from '../adminService/controllers/contactPageController';

const router = Router();

router.get('/get-mentor', (req, res, next) => { getMentorController(req, res, next, true) });
router.get('/get-mentors', (req, res, next) => { getMentorsController(req, res, next, true) });
router.get('/get-shops', (req, res, next) => { getShopController(req, res, next, true) })

router.get('/getAllExpertise', (req, res, next) => { getAllExpertise(req, res, next, true) })

router.get('/getAllService', (req, res, next) => { getAllService(req, res, next, true) })

router.get('/activeService', (req, res, next) => { getAllActiveServices(req, res, next, true) })
router.post('/getactiveService', (req, res, next) => { getAllActiveServices(req, res, next, true) })

//like unlike Shop
router.post('/likeShopUser/:id', (req, res, next) => { likeShopUser(req, res, next, true) })
router.post('/unlikeShopUser/:id', (req, res, next) => { unlikeShopUser(req, res, next, true) })

// createToken
router.get('/makeToken/:id', (req, res, next) => { createToken(req, res, next, true) })

// booking tag
router.get('/getAllTag', (req, res, next) => { getAllTags(req, res, next, true) });

// review tag
router.get('/getAllReviewTag', (req, res, next) => { getAllReviewTags(req, res, next, true) });

// service tag
router.get('/getAllServiceTag', (req, res, next) => { getAllServiceTags(req, res, next, true) })

// description tag
router.get('/getAllDescriptionTag', (req, res, next) => { getAllDescriptionTags(req, res, next, true) })

//tag service
router.get('/getAllTagService', (req, res, next) => { getAllTagService(req, res, next, true) })

// shop Query
router.get('/getShopQuery/:id', (req, res, next) => { SearchShopQuery(req, res, next, true) })
router.get('/getShopRegexQuery/:id', (req, res, next) => { SearchShopRegexQuery(req, res, next, true) })

// service Query
router.get('/getServiceQuery/:id', (req, res, next) => { SearchServiceQuery(req, res, next, true) })
router.get('/getServiceRegexQuery/:id', (req, res, next) => { SearchServiceRegexQuery(req, res, next, true) })
router.get('/activeServiceRegexQuery/:id', (req, res, next) => { getAllQueryActiveServices(req, res, next, true) })

//metadata
router.get('/getAllMetadata', (req, res, next) => { getAllCombinedTagList(req, res, next, true) })

// push
router.post('/subscribe', (req, res, next) => { subscriberPost(req, res, next, true) })

//offer User
router.get('/getAllOffer', (req, res, next) => { getAllOffers(req, res, next, true) })
router.get('/getAllServiceOffer', (req, res, next) => { getAllServiceOffer(req, res, next, true) })

//banner User
router.get('/getAllBanner', (req, res, next) => { getAllBanners(req, res, next, true) })

// dummy Url
router.get('/open/:id', (req, res, next) => { OpenUrlWithoutToken(req, res, next, true) })
router.get('/open/:id/:token', verify, (req, res, next) => { OpenUrl(req, res, next, true) })


// shop Mobile Login
router.post('/mobileShopLogin', (req, res, next) => { shopMobileLoginSignup(req, res, next, true) })
router.post('/mobileShopLoginVerify', (req, res, next) => { shopMobileLoginOtpVerify(req, res, next, true) })


// employee Mobile Login
router.post('/employeeLoginId', (req, res, next) => { employeeLoginId(req, res, next, true) })
router.post('/mobileEmployeeLogin', (req, res, next) => { employeeMobileLogin(req, res, next, true) })
router.post('/mobileEmployeeLoginVerify', (req, res, next) => { employeeMobileLoginOtpVerify(req, res, next, true) })

// shop category
router.get('/getAllShopCategory', (req, res, next) => { getAllShopCategory(req, res, next, true) })

//admin page
router.get('/getAllAdminPage', (req, res, next) => { getAllAdminPage(req, res, next, true) })

//contact
router.post('/createContact', (req, res, next) => { CreateContact(req, res, next, true) })

//contact page
router.get('/getAllContactPage', (req, res, next) => { getAllContactPage(req, res, next, true) })

export default router;
