import express from 'express';
import morgan from 'morgan';
import webPush from 'web-push'
import useMiddleWare from './middleware/index';
//import apiRoutes from './routes/apiRoutes';
import connectDB from './config/connectDatabase';
import './Models/User';
import passport from 'passport';
import passportRoute from './routes/passportRoutes'
//import adminRoutes from './routes/adminRoutes'
import { LoggerStream, MainLogFn } from './config/log';
import { webPushKey } from './config/keys'
import serviceRoutes from './apiService';
import config from 'config'
import { responseTime } from './config/responseTime';
import clientRegis from 'prom-client';
import discover from '../../discovery/disovery'
import defaultConfig from './config/default-config';
import init from './config/connectDB'
import { getApiExecute } from './apiService/adminService/controllers/testController';

// Create a Registry which registers the metrics
const register = new clientRegis.Registry()

// Add a default label which is added to all metrics
register.setDefaultLabels({
  app: 'example-nodejs-app'
})

// Enable the collection of default metrics
clientRegis.collectDefaultMetrics({ register })

console.log('test App', config.get('DATABASE_URL'))

// import cors from 'cors';
// import { COOKIE_KEYS, CLIENT_URL } from './config/keys';
// import cookieParser from 'cookie-parser';
// import session from 'express-session';

const app = express();

connectDB();
init(app);

var path = require('path')

// app.use(express.static(__dirname + 'client'));
// app.get('/', (req, res) => {
//   res.sendFile(path.resolve(__dirname, 'client', 'index.html'))
// })

useMiddleWare(app);

const publicVapidKey = webPushKey.Public_Key;
const privateVapidKey = webPushKey.Private_Key;

webPush.setVapidDetails(webPushKey.WEB_PUSH_CONTACT, publicVapidKey, privateVapidKey);

app.set('webpush', webPush);

// const whitelist = ['https://heybarber.herokuapp.com'];
// const corsOptions = {
//   origin: function (origin:any, callback:any) {
//     console.log("origin > : "+ origin)
//     if (!origin || whitelist.indexOf(origin) !== -1) {
//       callback(null, true)
//     } else {
//       callback(new Error("Not allowed by CORS"))
//     }
//   },
//   credentials: true,
// }

// app.use(cors(corsOptions))

// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Methods","POST, PUT, OPTIONS, GET, DELETE");
//   res.header(
//     "Access-Control-Allow-Headers",
//     "Origin, X-Requested-With, Content-Type, Accept , auth-token"
//   );
//   next();
// });
// app.use(
//   cors( { 
//     origin: '*',
//     methods: ['GET', 'POST', 'DELETE', 'PATCH', 'UPDATE','OPTIONS']
//   }),
// );


//app.use(cors())
// app.use((req, res, next) => {
//   res.header('Access-Control-Allow-Origin',  'https://heybarber.herokuapp.com');
//   res.header("Access-Control-Allow-Credentials",'true');
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, auth-token");
//   res.header("Access-Control-Allow-Methods","POST, PUT, OPTIONS, GET, DELETE");

//   next()
// })
// app.use(express.json());
// app.set('trust proxy', 1);

// app.use(cookieParser()); // parse cookies

// app.use(express.urlencoded({ extended: true }));
// app.use(
//   session({
//     secret: COOKIE_KEYS,
//     name: 'caucus-session',
//     resave: false,
//     saveUninitialized: false,
//     cookie: {
//       maxAge: 24 * 60 * 60 * 1000,
//       sameSite: true,
//       httpOnly: true,
//       secure: false,
//     },
//   }),
// );

require('./controllers/cronController').init(app)

app.use(passport.initialize());
app.use(passport.session());
app.use(responseTime);
app.use(MainLogFn.write)

app.use(discover(defaultConfig))

app.use('/image/', express.static('src/upload'))
//app.use('/api', apiRoutes);
//app.use('/admin', adminRoutes);
app.use('/web', passportRoute)

app.use('/metrics', async (req, res) => {
  const ss = await register.metrics()
  res.setHeader('Content-Type', register.contentType)
  res.end(ss)
})

app.use('/test', getApiExecute)

serviceRoutes(app)

export default app