import { NextFunction, Request, Response } from 'express';
//import ipapi from 'ipapi.co'
import { nanoid } from 'nanoid';
import { SERVER_URL, CLIENT_URL, SHORT_URL_KEY } from '../config/keys';
import { ShortUrlModel, ShortUrlStatModal } from '../Models/ShortUrl';
import { Main } from '../config/location';
import axios from 'axios';
import config from 'config'

export const createShortUrl = async function (id: any) {
  // const ShortId = new ShortUrlModel({
  //   product_id: id,
  //   short_id: nanoid()
  // })
  // const sm = await ShortId.save()
  // console.log('Short Id Created', sm)

  axios.get(`${config.has('SHORT_URL_KEY') ? config.get('SHORT_URL_KEY') : SHORT_URL_KEY}create/${id}`).then((cc) => {

    console.log('success shortUrl Create', cc)

  }).catch((err) => {
    console.log('fail shortUrl Create', err)
  })

}

export const updateShortUrl = async function (req: Request, res: Response, next: NextFunction) {
  if (req.params.id == null || req.params.id == undefined || req.params.id == 'null' || req.params.id == 'undefined' || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: 'Request Failed',
      data: 'Id Not Found'
    })
  }

  ShortUrlModel.findOneAndUpdate({ product_id: req.params.id }, {
    $set: {
      short_id: nanoid()
    }
  }, { upsert: true, new: true, returnNewDocument: true, returnOriginal: false, setDefaultsOnInsert: true }).then((cc) => {
    console.log('Created Or Updated', cc)
    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: cc
    })
  }).catch((err) => {
    console.log('Short Url Update Err', err)
    res.status(400).json({
      message: 'Failed',
      status: res.statusCode,
      data: err
    })
  })
}

export const openShortUrl = async function (req: Request, res: Response, next: NextFunction) {

  if (req.params.id == null || req.params.id == undefined || req.params.id == 'null' || req.params.id == 'undefined' || req.params.id == "") return res.redirect(CLIENT_URL)

  var value: any = {
    ip: (req.headers['x-forwarded-for'] == undefined && req.hostname == 'localhost') ? '127.0.0.1' : req.ip,
    header: req.headers,
    method: req.method,
    ipDetail: {}
  }

  await Main(Test, req.ip)

  const data = await ShortUrlModel.aggregate([
    {
      $match: {
        short_id: req.params.id
      }
    }
  ]).exec()

  if (data == null || data == undefined || data.length == 0) return res.redirect(CLIENT_URL)

  async function Test(status: any, err: Error, cc: any) {
    if (err) {
      return res.redirect(CLIENT_URL)
    }
    console.log('UpD', cc)
    value.ipDetail = (cc == null || cc == undefined) ? {} : cc

    value.product_id = data[0].product_id
    value.key = req.params.id

    console.log('Short value', value)

    const statC = await ShortUrlStatModal.create(value)

    console.log('Created Stat', statC)
  }

  const UpdateCount = await ShortUrlModel.findOneAndUpdate({ _id: data[0]._id }, {
    $inc: { count: 1 }
  }, { new: true, returnNewDocument: true, returnOriginal: false })

  console.log('Updated', UpdateCount)

  res.redirect(CLIENT_URL + '/booking/' + data[0].product_id)
}

export const getAllShortUrl = async function (req: Request, res: Response, next: NextFunction) {

  ShortUrlModel.find({}).then((item) => {
    if (item.length > 0) {
      item.map((cc: any) => {
        if (cc.short_id.match(SERVER_URL)) {
          cc.short_id = cc.short_id
        } else {
          cc.short_id = SERVER_URL + '/api/openShortUrl/' + cc.short_id
        }
      })
    }

    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: { short: item }
    })
  }).catch((err) => {
    console.log('get All Short Url Err', err)
    res.status(400).json({
      message: 'Failed',
      status: res.statusCode,
      data: err
    })
  })

}

// const Location = async (data: any, cb: Function) => {
//   if (data.headers['x-forwarded-for'] == undefined && data.hostname == 'localhost') {
//     cb(200, null, {})

//   } else {
//     const Hello = (res: any) => {
//       console.log('Ip All Data', res)
//       cb(200, null, res)
//     }
//     const wait = await ipapi.location(Hello, data.ip)
//   }
// }