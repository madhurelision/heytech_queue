import { NextFunction, Request, Response } from 'express';
import { PushNotificationModel } from '../Models/PushNotify'

// Web Push
export const subscriberPost = (req: Request, res: Response, next: NextFunction, verify: any) => {
  const subscription: any = req.body
  console.log('main', subscription)
  PushNotificationModel.findOneAndUpdate({ endpoint: subscription.endpoint, deleted:false }, { $set: subscription }, { upsert: true }).then((cc) => {
    console.log('check', cc)
    res.status(201).json({});

  }).catch((err: Error) => {
    console.log('Push Notification Create Err', err)
    res.status(400).json({
      message: 'Failed',
      status: res.statusCode,
      data: err
    })
  })
}

export const getAllPushNotificationList = (req: Request, res: Response, next: NextFunction) => {

  PushNotificationModel.aggregate([
    {
      '$match': {
        'deleted': false
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'detail': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'detail': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        detail: [],
        total: 0
      }
    })
  }).catch((err) => {
    console.log('Notification Detail Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

export const sendNotication = (req: Request, res: Response, next: NextFunction) => {
  const webPush = req.app.get('webpush');

  PushNotificationModel.find({ _id: { $in: req.body._id.map((cc: any) => cc) } }).then((val) => {

    delete req.body.template._id
    delete req.body.template.createdAt
    delete req.body.template.updatedAt


    const payload = JSON.stringify(req.body.template);


    if (val.length > 0) {
      val.map((item) => {

        webPush.sendNotification(item, payload).catch((err: Error) => {
          console.log(err)
        })
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: 'Request Success',
      data: val
    })

  }).catch((err) => {
    console.log('Notification Send Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const deletePushNotification = (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.params.id)
  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  PushNotificationModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      deleted: true
    }
  }, { new: true }).then((cc) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Push Notification Deelete Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}