import { NextFunction, Request, Response } from 'express';
import { MAIL_SECRET_KEY, otpGenerate, SECRET_KEY, ServiceSmsMail } from '../config/keys';
import { EmployeeModel } from '../Models/Employee';
import jwt from 'jsonwebtoken'
import mongoose from 'mongoose';
import { BookingModel } from '../Models/Booking';
import axios from 'axios';

export const employeeLoginId = (req: Request, res: Response) => {

  console.log('data', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  if (req.body.id == null || req.body.id == undefined || req.body.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Employee Id is Required",
      data: 'err'
    })
  }

  EmployeeModel.aggregate([
    {
      '$match': {
        'employee_id': parseInt(req.body.id)
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'user_id'
      }
    },
    {
      '$unwind': {
        'path': '$user_id'
      }
    },
    {
      '$lookup': {
        'from': 'services',
        'localField': 'user_id.topics',
        'foreignField': '_id',
        'as': 'user_id.topics'
      }
    }
  ]).exec((err, item: any) => {

    if (err) {
      console.log('Employee Login Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    if (item.length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: item
      })
    }

    if (item[0].status == false) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Your Account is not Activated",
        data: item
      })
    }

    if (item[0].deleted == true) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Your Account Doesn't Exist",
        data: item
      })
    }

    var employeeData: any = item[0]

    const token: any = jwt.sign({ _id: employeeData._id }, SECRET_KEY)
    res.header("auth-token", token)

    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: item[0],
      token: token,
      type: 'employee'
    })

  })

}

export const getEmployeeProfile = (req: Request, res: Response) => {
  console.log('Employee User', req.user)

  const val: any = req.user

  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  EmployeeModel.aggregate([
    {
      '$match': {
        '_id': new mongoose.Types.ObjectId(val._id)
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'user_id'
      }
    },
    {
      '$unwind': {
        'path': '$user_id'
      }
    },
    {
      '$lookup': {
        'from': 'services',
        'localField': 'user_id.topics',
        'foreignField': '_id',
        'as': 'user_id.topics'
      }
    }
  ]).exec((err, item: any) => {

    if (err) {
      console.log('Employee Login Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    if (item.length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: item
      })
    }

    var employeeData: any = item[0]

    const token: any = jwt.sign({ _id: employeeData._id }, SECRET_KEY)
    res.header("auth-token", token)

    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: item[0],
      token: token,
      type: 'employee'
    })

  })
}

export const getEmployeeAppointment = async (req: Request, res: Response, next: NextFunction) => {
  console.log('Employee', req.user)
  console.log('Employee', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  BookingModel.aggregate([
    {
      '$match': {
        'assign': new mongoose.Types.ObjectId(val._id),
        'end_time': { $gte: new Date(new Date().setHours(0o0, 0o0, 0o0)) },
        'status': { $in: ['waiting', 'accepted'] },
        'otp_verify': true
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_id._id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$shop_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'userreview'
      }
    }, {
      '$addFields': {
        'user_count': {
          '$reduce': {
            'input': '$userreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalUser': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$userreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$user_count', {
                    '$size': '$userreview'
                  }
                ]
              }
            }
          }
        }
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'created_by',
        'foreignField': 'user_information',
        'as': 'shop'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'employee'
      }
    },
    {
      $sort: {
        '_id': -1
      }
    }
  ]).exec((err, item) => {

    if (err) {
      console.log('Employee Appointment List Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }


    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { booking: item }
    })
  })

}

export const getEmployeeBookings = async (req: Request, res: Response, next: NextFunction) => {
  console.log('Employee', req.user)
  console.log('Employee', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  BookingModel.aggregate([
    {
      '$match': {
        'created_by': new mongoose.Types.ObjectId(val._id),
        'end_time': { $gte: new Date(new Date().setHours(0o0, 0o0, 0o0)) },
        'status': { $in: ['waiting', 'accepted'] },
        'otp_verify': true
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_id._id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$shop_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'userreview'
      }
    }, {
      '$addFields': {
        'user_count': {
          '$reduce': {
            'input': '$userreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalUser': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$userreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$user_count', {
                    '$size': '$userreview'
                  }
                ]
              }
            }
          }
        }
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'created_by',
        'foreignField': 'user_information',
        'as': 'shop'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'employee'
      }
    },
    {
      $sort: {
        '_id': -1
      }
    }
  ]).exec((err, item) => {

    if (err) {
      console.log('Employee Appointment List Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }


    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { booking: item }
    })
  })

}

export const employeeMobileLogin = async (req: Request, res: Response, next: NextFunction) => {

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('body', req.body)

  const mobileUser = await EmployeeModel.find({ mobile: req.body.mobile, deleted: false })

  console.log('mobile User', mobileUser)


  if (mobileUser.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "No Employee Registered With this Number",
      data: 'err'
    })
  }


  try {

    const otp = otpGenerate()
    const msgT = 'Your Otp for Login is' + otp
    const smsToken = jwt.sign({ date: new Date(), message: msgT }, MAIL_SECRET_KEY)
    const mainData = await axios.post(ServiceSmsMail + 'sendSms', {
      number: req.body.mobile,
      otp: otp
    }, {
      headers: {
        'auth-token': smsToken
      }
    });


    console.log('Otp Sent', mainData)

    const userData = await EmployeeModel.updateMany({
      mobile: req.body.mobile,
      deleted: false
    }, {
      $set: {
        otp: otp,
      }
    }, { new: true })

    console.log('Otp Sent', userData)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        redirect: false,
        data: mobileUser.map((cc) => { return { _id: cc._id, employee_id: cc.employee_id } })
      }
    })



  } catch (err) {

    console.log('Create otp Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  }

}

export const employeeMobileLoginOtpVerify = (req: Request, res: Response, next: NextFunction) => {


  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  EmployeeModel.findOne({ mobile: req.body.mobile, employee_id: req.body.id, otp: parseInt(req.body.otp) }).then((cc) => {
    if (cc == null || cc == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var link

    const token: any = jwt.sign({ _id: cc._id }, SECRET_KEY)
    res.header("auth-token", token)
    link = '/loginemployee/' + token;


    if (cc.status == false) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Your Account is not Activated",
        data: cc
      })
    }

    if (cc.deleted == true) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Your Account Doesn't Exist",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: link
    })

  }).catch((err) => {
    console.log('Employee Mobile Update Err', err)
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

// employee search Query
export const getAllEmployeeSearchQuery = (req: Request, res: Response, next: NextFunction) => {
  EmployeeModel.aggregate([
    {
      '$match': {
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        }
      }
    },
    {
      $match: {
        $or: [
          { "name": { $regex: req.params.id, $options: "i" } },
          { "type": { $regex: req.params.id, $options: "i" } },
          { "user_name": { $regex: req.params.id, $options: "i" } },
          { "employee_id": { $regex: req.params.id, $options: "i" } },
          { "mobile": { $regex: req.params.id, $options: "i" } },
        ]
      }
    },
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err) => {
    console.log('Empoyee Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}