import { NextFunction, Request, Response } from 'express';
import { SECRET_KEY, SERVER_URL, randomImageName } from '../config/keys';
import { CustomerModel } from '../Models/Customer';
import jwt from 'jsonwebtoken'
import multer from 'multer';
import { upload } from '../config/uploadImage';
import { ImageUpload, removeImage } from '../ImageService/main';
import { Con } from '../ImageService/source';
import { isValidObjectId } from 'mongoose';
import mongoose from 'mongoose'

export const googleNew = async (req: Request, accessToken: String, refreshToken: String, params: any, profile: any, done: Function) => {
  console.log('Google Profile Data ', profile)
  console.log('Google Profile Params ', params)

  CustomerModel.findOne({ email: profile._json.email }).then((item: any) => {

    if (item == null || item == undefined) {

      const UserData = new CustomerModel(
        {
          first_name: profile._json.given_name,
          last_name: profile._json.family_name,
          name: profile._json.given_name + ' ' + profile._json.family_name,
          email: profile._json.email,
          image_link: profile._json.picture,
          google: profile.id,
          oauth_provider: profile.provider,
          is_mentor: true,
          signup_completed: true,
        }
      )

      UserData.save().then((main: any) => {
        console.log('inserted record', main);
        //done(null, { _id: response.insertedId })
        done(null, main)

      }).catch((err: Error) => {
        console.log('Error occurred while inserting', err);
        done(err)
      })

    } else {

      CustomerModel.findOneAndUpdate({ email: profile._json.email }, {
        $set: {
          first_name: profile._json.given_name,
          last_name: profile._json.family_name,
          email: profile._json.email,
          google: profile.id,
          oauth_provider: profile.provider,
        }
      }, { new: true }).then((cc: any) => {
        console.log('updated record', cc);
        //done(null, { _id: response.insertedId })
        done(null, cc)

      }).catch((err: Error) => {
        console.log('Error occurred while updating', err);
        done(err)

      })

    }

  }).catch((err: Error) => {
    console.log('Google Find error', err);
    return done(err)
  })

}

export const getCustomerProfile = (req: Request, res: Response) => {
  console.log('Customer', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  CustomerModel.findOne({ _id: val._id }).then((item) => {

    console.log('customer', item)

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: item
      })
    }

    var customerData: any = item

    const token: any = jwt.sign({ _id: customerData._id, email: customerData.email }, SECRET_KEY)
    res.header("auth-token", token)

    if (item || item != null || item != undefined || Object.keys(item).length > 0) {
      if (item.image_link.match('https')) {
        item.image_link = item.image_link
      } else {
        item.image_link = SERVER_URL + item.image_link
      }
    }

    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: item,
      token: token,
      type: 'customer'
    })

  }).catch((err) => {
    console.log('Customer err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}


export const updateCustomer = async (req: Request, res: Response, next: NextFunction) => {
  const MainCheck = upload.single('image')

  MainCheck(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 5 MB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('image', req.file)
    console.log('body', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }


    console.log('image', req.file)
    console.log('body', req.body)

    var data

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        name: req.body.name,
        image_link: req.body.oldimage,
        email: req.body.email,
        mobile: req.body.mobile,
      }


      CustomerModel.findOneAndUpdate({ _id: req.params.id }, {
        $set: {
          ...data
        }
      }, { new: true }).then((cc) => {
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: cc
        })
      }).catch((err) => {
        console.log('Customer Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        data = {
          first_name: req.body.first_name,
          last_name: req.body.last_name,
          name: req.body.name,
          image_link: item?.response.url,
          email: req.body.email,
          mobile: req.body.mobile,
        }


        CustomerModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            ...data
          }
        }, { new: true }).then((cc) => {
          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: cc
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err) => {
          console.log('Customer Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Customer Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    }


  })

}

export const customerCsvImport = async (req: Request, res: Response, next: NextFunction) => {

  if (!req.body || req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.data == null || req.body.data == undefined || req.body.data.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: 'Data Not Found',
      data: 'Data Not Found'
    })
  }

  var customerArr: any = []

  req.body.data.map((cc: any) => {
    if (cc.id == "" || cc.id == null || cc.id == undefined) {
      customerArr.push({
        id: cc.id,
        data: {
          first_name: cc["first_name"],
          last_name: cc["last_name"],
          mobile: cc["mobile"],
          email: cc["email"],
          name: cc["name"],
          image_link: cc["image_link"],
        }
      })
    } else {
      customerArr.push({
        id: cc.id,
        data: {
          first_name: cc["first_name"],
          last_name: cc["last_name"],
          mobile: cc["mobile"],
          email: cc["email"],
          name: cc["name"],
          image_link: cc["image_link"],
        },
      })
    }
  })

  try {

    Promise.all(customerArr.map(async (cc: any) => {
      const createS = await CustomerModel.updateMany({ _id: isValidObjectId(cc.id) ? cc.id : new mongoose.Types.ObjectId() }, { $set: cc.data }, { upsert: true, setDefaultsOnInsert: true, new: true })
      console.log('Updated', createS)
      return createS
    })).then((cc) => {

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: cc,
        check: customerArr,
      })

    }).catch((err) => {
      console.log('Customer Csv Import Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    })

  } catch (err) {

    console.log('Customer Csv Import Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  }


}

////admin Customer List
export const SerachAdminCustomerQuery = (req: Request, res: Response) => {

  CustomerModel.aggregate([
    {
      $match: {
        $or: [
          { "first_name": { $regex: req.params.id, $options: "i" } },
          { "last_name": { $regex: req.params.id, $options: "i" } },
          { "email": { $regex: req.params.id, $options: "i" } },
          { "name": { $regex: req.params.id, $options: "i" } },
        ]
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$_id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$shop_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },
    {
      '$addFields': {
        'user_count': {
          '$reduce': {
            'input': '$review',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'total': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$review'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$user_count', {
                    '$size': '$review'
                  }
                ]
              }
            }
          }
        }
      }
    },
  ]).then((item: any) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  }).catch((err: Error) => {
    console.log('Customer err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}