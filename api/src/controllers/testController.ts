import { Request, Response, NextFunction } from 'express'
import { isValidObjectId } from 'mongoose'
import jwt from 'jsonwebtoken'
import { CLIENT_URL, generateHash, SECRET_KEY } from '../config/keys'
import { BookingModel, TestingModel } from '../Models/Booking'
import { AdminModel } from '../Models/Admin'
import { GalleryModel } from '../Models/Gallery'
import { ServiceModel } from '../Models/Service'
import { ReviewModel } from '../Models/review'
import { NotificationModel } from '../Models/Notification'
import { removeImage } from '../ImageService/main'
import { ShopModel, UserModel } from '../Models/User'
import { ServiceRequestModel } from '../Models/ServiceRequest'
import { ShortUrlStatModal } from '../Models/ShortUrl'
//import { CounterModel } from '../Models/Counter'
import { Main } from '../config/location';
import axios from 'axios';

export const OpenUrl = (req: Request, res: Response, next: NextFunction) => {

  const val: any = req.user

  console.log('req params id', req.params.id, 'req.params.token', req.params.token, 'value', val)

  if (req.params.id == null || req.params.id == undefined || req.params.id == "" || val._id == null || val._id == undefined || val._id == "" || val.email == null || val.email == undefined || val.email == "") {
    return res.redirect(CLIENT_URL)
  }
  const token: any = jwt.sign({ _id: val._id, email: val.email }, SECRET_KEY)

  isValidObjectId(req.params.id) ? res.redirect(CLIENT_URL + '/mobile/' + req.params.id + '/' + token) : res.redirect(CLIENT_URL)
}

export const OpenUrlWithoutToken = (req: Request, res: Response, next: NextFunction) => {

  console.log('req params id', req.params.id, 'req.params.token', req.params.token)

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.redirect(CLIENT_URL)
  }

  isValidObjectId(req.params.id) ? res.redirect(CLIENT_URL + '/user/' + req.params.id) : res.redirect(CLIENT_URL)
}

export const UpdateAllQueuedata = async (req: Request, res: Response, next: NextFunction) => {

  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  const mainData = await TestingModel.findOne({ _id: req.params.id, shop_id: val._id })
  console.log('check Update', mainData)

  TestingModel.updateMany({
    shop_id: val._id,
    queue: true,
    appointment_date: { $gte: new Date(mainData.appointment_date) },
    _id: { $nin: [req.params.id] }
  }, [{
    $set: {
      approximate_date: {
        $add: ['$approximate_date', 30 * 60000]
      },
      waiting_date: {
        $add: ['$waiting_date', 30 * 60000]
      },
      end_time: {
        $add: ['$waiting_date', 30 * 60000]
      },
    }
  }]).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err: Error) => {
    console.log('Queue Waiting Time Add Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })


}

export const SequeneceId = async (req: Request, res: Response, next: NextFunction) => {

  ///const mainC = await ServiceModel.find({})
  const fs = require('fs')
  fs.readFile('src/log/application-2022-03-15-18.log', "utf8", function (err: Error, data: any) {
    if (err) {
      console.log('checl', err)
      return res.status(400).json({
        message: 'Failed',
        data: err
      })
    }
    console.log(data)
    res.status(200).json({
      message: 'Success',
      data: data,
      val: JSON.stringify(data)
    })
  });

  // ServiceModel.updateMany({}, {
  //   tags: "6229dcbf573fa118d88b3ba6"
  // }).then((item: any) => {


  //   res.status(200).json({
  //     status: 'Success',
  //     status_code: res.statusCode,
  //     message: "Request Success",
  //     data: item
  //   })

  // }).catch((err) => {
  //   console.log('Pr Err', err)

  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })

  // })

  // const smE = new ServiceRequestModel({
  //   mobile: JSON.stringify(7903117685),
  //   data: {
  //     number: 7903117685,
  //     otp: 748596
  //   }
  // })

  // smE.save().then((cc: any) => {
  //   res.status(200).json({
  //     status: 'Success',
  //     status_code: res.statusCode,
  //     message: "Request Success",
  //     data: cc
  //   })


  // }).catch((err: Error) => {
  //   console.log('service Sms Request Err', err)
  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })

  // })

  // removeImage(process.cwd() + "/src/upload/" + 'BgycB04PeYYYbUgjplgKp-image.jpg')

  // res.status(200).json({
  //   status: 'Success',
  //   status_code: res.statusCode,
  //   message: "Request Success",
  //   data: 'Check'
  // })

  // const mainD = await UserModel.aggregate([
  //   {
  //     '$match': {
  //       'signup_completed': true
  //     }
  //   }, {
  //     '$lookup': {
  //       'from': 'shops',
  //       'localField': 'mentor_information',
  //       'foreignField': '_id',
  //       'as': 'shop_id'
  //     }
  //   }, {
  //     '$unwind': {
  //       'path': '$shop_id'
  //     }
  //   }
  // ]).exec()

  // const mainD = await ServiceModel.find({})

  // if (mainD == null || mainD == undefined || Object.keys(mainD).length == 0) {
  //   return res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: mainD
  //   })
  // }

  // const ND = new NotificationModel({
  //   message: 'The Booking Status is completed',
  //   user_id: mainD.user_id._id,
  //   shop_id: mainD.shop_id._id,
  // })

  // ND.save().then((cc: any) => {
  //   res.status(200).json({
  //     status: 'Success',
  //     status_code: res.statusCode,
  //     message: "Request Success",
  //     data: cc
  //   })
  // }).catch((err: Error) => {
  //   console.log('Counter Err', err)
  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })

  // })

  // mainD.map(async (cc: any) => {
  //   var D = await ServiceModel.updateMany({ _id: cc._id }, {
  //     status: true,
  //     deleted: false
  //   })
  //   console.log('Updated', D)
  // })

  // res.status(200).json({
  //   status: 'Success',
  //   status_code: res.statusCode,
  //   message: "Request Success",
  //   data: mainD
  // })
  // ServiceModel.updateMany({}, {
  //   image: 'https://ik.imagekit.io/nmg2p396hcb/YEpOgdxCOkhUfEodc32A_-image_xtPcddubC.jpg'
  // }).then((cc) => {
  //   res.status(200).json({
  //     status: 'Success',
  //     status_code: res.statusCode,
  //     message: "Request Success",
  //     data: cc
  //   })
  // }).catch((err: Error) => {
  //   console.log('Counter Err', err)
  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })

  // })

}

export const CreateAdmin = (req: Request, res: Response, next: NextFunction) => {

  const Admin = new AdminModel({
    email: 'info.heytechitservices@gmail.com',
    password: generateHash(123456)
  })

  Admin.save().then((cc: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err: Error) => {
    console.log('Admin Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const getUserLocation = (req: Request, res: Response, next: NextFunction) => {

  var axios = require('axios');

  var config = {
    method: 'get',
    url: 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=40.6655101%2C-73.89188969999998&destinations=40.659569%2C-73.933783%7C40.729029%2C-73.851524%7C40.6860072%2C-73.6334271%7C40.598566%2C-73.7527626&key=AIzaSyCqiCKV0cm-EpHp2hev4XkD5wG9JGxFbVs',
    headers: {}
  };

  axios(config)
    .then((cc: any) => {
      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: cc.data
      })
    }).catch((err: Error) => {
      console.log('Location Find Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })

    })

}

const createUpdatedDate = (date: any, time: any, key: any) => {
  console.log('Updates Date', date, 'time', time, 'key', key, 'match', key == 'min')
  var now = new Date(date);
  console.log('check', now)
  if (key == 'min') {
    now.setMinutes(now.getMinutes() + time);
  } else {
    now.setMinutes(now.getMinutes() + time * 60)
  }
  now.setMilliseconds(0);
  return now
}

const minusDate = (start: Date, end: Date) => {
  console.log('mains', start, end)
  var now = new Date(start).getTime();
  var later = new Date(end).getTime();
  var result: any = (now - later) / 60000;
  return (result > 0) ? result : 0
}