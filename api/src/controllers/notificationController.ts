import { Request, Response, NextFunction } from 'express';
import { NotificationModel } from '../Models/Notification';

export const getCustomerNotification = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  if (key == null || key == undefined || Object.keys(key).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }
  NotificationModel.find({ user_id: key._id, view: false }).then((item: any) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        customerNotificationCount: item.length,
        customerNotification: { notification: item }
      }
    })
  }).catch((err: Error) => {
    console.log('Notification Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}

export const changeViewStatus = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  if (key == null || key == undefined || Object.keys(key).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  NotificationModel.updateMany({ user_id: key._id, view: false }, {
    $set: {
      view: true
    }
  }).then((cc: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err: Error) => {
    console.log('Notification Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}


export const getShopNotification = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  if (key == null || key == undefined || Object.keys(key).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }
  NotificationModel.find({ shop_id: key._id, shop_view: false }).then((item: any) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        shopNotificationCount: item.length,
        shopNotification: { notification: item }
      }
    })
  }).catch((err: Error) => {
    console.log('Notification Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}

export const changeShopViewStatus = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  if (key == null || key == undefined || Object.keys(key).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  NotificationModel.updateMany({ shop_id: key._id, shop_view: false }, {
    $set: {
      shop_view: true
    }
  }).then((cc: any) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err: Error) => {
    console.log('Notification Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
} 