import cron from 'node-cron';
import moment from 'moment';
import { BookingModel } from '../Models/Booking';

module.exports = {};
var cronController = module.exports;

cronController.init = function (app: any) {

  cron.schedule('* * * * *', () => {

    BookingModel.find({ status: 'accepted', queue: false, expired: false, waiting_verify: true, end_time: { $lte: new Date() } }).then(async (item) => {

      console.log('Cron Job status accepted appointment Data', item)

      if (item.length > 0) {

        item.map(async (cc: any) => {
          const check = await BookingModel.updateMany({ _id: cc._id }, {
            $set: {
              status: 'completed',
              expired: true
            }
          })

          console.log('updated appointment', check)
        })


      }

    }).catch((err) => {
      console.log('Cron Job appointment accepted process err', err)

    })

  })

  cron.schedule('* * * * *', () => {

    BookingModel.find({ status: 'waiting', queue: false, expired: false, waiting_verify: true, end_time: { $lte: new Date() } }).then(async (item) => {


      console.log('Cron Job status waiting appointment Data', item)

      if (item.length > 0) {

        item.map(async (cc: any) => {
          const check = await BookingModel.updateMany({ _id: cc._id }, {
            $set: {
              status: 'expired',
              expired: true
            }
          })

          console.log('updated', check)
        })


      }

    }).catch((err) => {
      console.log('Cron Job appointment waiting process err', err)

    })

  })

  cron.schedule('* * * * *', () => {

    BookingModel.find({ status: 'accepted', queue: true, expired: false, approximate_date: { $lte: new Date() } }).then(async (item) => {


      console.log('Cron Job status accepted queue Data', item)

      if (item.length > 0) {

        item.map(async (cc: any) => {
          const check = await BookingModel.updateMany({ _id: cc._id }, {
            $set: {
              status: 'completed',
              expired: true
            }
          })

          console.log('updated', check)
        })


      }


    }).catch((err) => {
      console.log('Cron Job queue accepted process err', err)
    })

  })

  cron.schedule('* * * * *', () => {

    BookingModel.find({ status: 'waiting', queue: true, expired: false, approximate_date: { $lte: new Date() } }).then(async (item) => {

      console.log('Cron Job status waiting queue Data', item)

      if (item.length > 0) {

        item.map(async (cc: any) => {
          const check = await BookingModel.updateMany({ _id: cc._id }, {
            $set: {
              status: 'expired',
              expired: true
            }
          })

          console.log('updated', check)
        })


      }


    }).catch((err) => {
      console.log('Cron Job queue waiting process err', err)

    })

  })

}