import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import { Mongoose } from 'mongoose';
import { dayOfWeek, generateHash, SECRET_KEY } from '../config/keys'
import { AdminModel } from "../Models/Admin";
import { BookingModel } from '../Models/Booking';
import { CustomerModel } from '../Models/Customer';
import { GalleryModel } from '../Models/Gallery';
import { ServiceModel } from '../Models/Service';
import { ShopModel } from '../Models/User';
import mongoose from 'mongoose'
import { EmployeeModel } from '../Models/Employee';
import { IpDetailModel, ShortUrlModel } from '../Models/ShortUrl';
import { LogModel } from '../Models/LogData'
import { LocationLogModel } from '../Models/LocationLog';

export const AdminLogin = (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  if (req.body.email == null || req.body.email == undefined || req.body.email == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Email is Required",
      data: 'err'
    })
  }

  if (req.body.password == null || req.body.password == undefined || req.body.password == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Password is Required",
      data: 'err'
    })
  }

  console.log('pass', req.body.password, 'hash', generateHash(req.body.password))

  AdminModel.findOne({ email: req.body.email, password: generateHash(req.body.password) }).then((item) => {
    console.log('Admin', item)

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: item
      })
    }

    var adminData: any = item

    const token: any = jwt.sign({ _id: adminData._id, email: adminData.email }, SECRET_KEY)
    res.header("auth-token", token)

    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: item,
      token: token,
    })

  }).catch((err) => {
    console.log('Admin err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

export const getAdminProfile = (req: Request, res: Response) => {
  console.log('Admin', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  AdminModel.findOne({ _id: val._id }).then((item: any) => {

    console.log('Admin', item)

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: item
      })
    }

    var adminData: any = item

    const token: any = jwt.sign({ _id: adminData._id, email: adminData.email }, SECRET_KEY)
    res.header("auth-token", token)

    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: item,
      token: token
    })

  }).catch((err) => {
    console.log('Admin Profile err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}


////admin Customer List
export const CustomerListAdmin = (req: Request, res: Response) => {

  CustomerModel.aggregate([
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$_id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$shop_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },
    // {
    //   '$addFields': {
    //     'userreview': {
    //       '$filter': {
    //         'input': '$review',
    //         'as': 'app',
    //         'cond': {
    //           '$eq': [
    //             '$$app.shop_review', true
    //           ]
    //         }
    //       }
    //     }
    //   }
    // }, 
    {
      '$addFields': {
        'user_count': {
          '$reduce': {
            'input': '$review',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'total': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$review'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$user_count', {
                    '$size': '$review'
                  }
                ]
              }
            }
          }
        }
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'customer': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'customer': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item: any) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        customer: [],
        total: 0
      }
    })

  }).catch((err: Error) => {
    console.log('Customer err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

/// admin Booking
export const getAdminCurrentBookings = async (req: Request, res: Response, next: NextFunction) => {
  console.log('admin', req.user)
  console.log('admin', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  BookingModel.aggregate([
    {
      '$match': {
        'end_time': { $gte: new Date(new Date().setHours(0o0, 0o0, 0o0)) },
        'status': { $in: ['waiting', 'accepted'] },
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$shop_id.user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'shopreview'
      }
    }, {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$shopreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalShop': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$shopreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$shopreview'
                  }
                ]
              }
            }
          }
        }
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_id._id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$shop_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'userreview'
      }
    }, {
      '$addFields': {
        'user_count': {
          '$reduce': {
            'input': '$userreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalUser': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$userreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$user_count', {
                    '$size': '$userreview'
                  }
                ]
              }
            }
          }
        }
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'created_by',
        'foreignField': 'user_information',
        'as': 'shop'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'employee'
      }
    }, {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'booking': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'booking': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).exec((err, item) => {

    if (err) {
      console.log('Admin urrent Appointment List Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }


    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        booking: [],
        total: 0
      }
    })
  })

}

// admin service
export const getAllAdminService = (req: Request, res: Response, next: NextFunction) => {

  ServiceModel.aggregate([
    {
      '$match': {
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        }
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'service': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'service': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        service: [],
        total: 0
      }
    })
  }).catch((err) => {
    console.log('Service Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

//admin Gallery
export const getAllGalleryList = (req: Request, res: Response, next: NextFunction) => {


  GalleryModel.aggregate([
    {
      '$match': {
        'status': true
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        }
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'gallery': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'gallery': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        gallery: [],
        total: 0
      }
    })
  }).catch((err) => {
    console.log('Gallery Admin Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}

export const getAllShopList = (req: Request, res: Response, next: NextFunction) => {

  ShopModel.aggregate([
    {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    }, {
      '$lookup': {
        'from': 'offers',
        'localField': 'topics',
        'foreignField': 'service_id',
        'as': 'offers'
      }
    },
    {
      '$lookup': {
        'from': 'employees',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$deleted', false
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'employee'
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },
    // {
    //   '$addFields': {
    //     'userreview': {
    //       '$filter': {
    //         'input': '$review',
    //         'as': 'app',
    //         'cond': {
    //           '$eq': [
    //             '$$app.user_review', true
    //           ]
    //         }
    //       }
    //     }
    //   }
    // },
    { '$unset': ["time_slot.monday._id", "time_slot.tuesday._id", "time_slot.wednesday._id", "time_slot.thursday._id", "time_slot.friday._id", "time_slot.saturday._id", "time_slot.sunday._id"] },
    {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$review',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'total': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$review'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$review'
                  }
                ]
              }
            }
          }
        }
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'shop': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'shop': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        shop: [],
        total: 0
      }
    })
  }).catch((err) => {
    console.log('Shop List Admin Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const dateWiseShopList = (req: Request, res: Response, next: NextFunction) => {
  var nameque: any = `time_slot.${dayOfWeek[new Date().getDay()]}.available`
  var mainCheck = {
    nameque: true
  }
  console.log('Check', nameque, mainCheck)

  const query = [
    {
      '$match': mainCheck
    },
    {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'shop': [
          {
            '$sort': {
              '_id': -1
            }
          }
        ]
      }
    }, {
      '$project': {
        'shop': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]

  ShopModel.aggregate(query).then((item) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        shop: [],
        total: 0
      }
    })
  }).catch((err) => {
    console.log('Shop List Admin Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const GetDayWiseCount = (req: Request, res: Response, next: NextFunction) => {

  console.log('Admin', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  AdminModel.aggregate([
    {
      '$match': {
        _id: new mongoose.Types.ObjectId(val._id)
      }
    },
    {
      '$lookup': {
        'from': 'bookings',
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$gte': [
                      '$createdAt', new Date(new Date(req.body.startDate).setHours(0o0, 0o0, 0o0))
                    ]
                  }, {
                    '$lt': [
                      '$createdAt', new Date(new Date(req.body.endDate).setHours(23, 59, 59))
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'booking'
      }
    }, {
      '$addFields': {
        'appointment': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.queue', false
                ]
              }
            }
          }
        },
        'queue': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.queue', true
                ]
              }
            }
          }
        },
        'total': {
          '$size': '$booking'
        },
        'verify': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.otp_verify', true
                ]
              }
            }
          }
        },
        'notVerify': {
          '$size': {
            '$filter': {
              'input': '$booking',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.otp_verify', false
                ]
              }
            }
          }
        }
      }
    }, {
      '$lookup': {
        'from': 'servicerequests',
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$gte': [
                      '$createdAt', new Date(new Date(req.body.startDate).setHours(0o0, 0o0, 0o0))
                    ]
                  }, {
                    '$lt': [
                      '$createdAt', new Date(new Date(req.body.endDate).setHours(23, 59, 59))
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'service'
      }
    }, {
      '$addFields': {
        'sms': {
          '$size': {
            '$filter': {
              'input': '$service',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.type', 'sms'
                ]
              }
            }
          }
        },
        'email': {
          '$size': {
            '$filter': {
              'input': '$service',
              'as': 'app',
              'cond': {
                '$eq': [
                  '$$app.type', 'email'
                ]
              }
            }
          }
        },
        'totalservice': {
          '$size': '$service'
        }
      }
    },
  ]).then((item) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { booking: item }
    })
  }).catch((err) => {
    console.log('Booking Count Date WIse Admin Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })


}

export const getSingleCustomer = (req: Request, res: Response, next: NextFunction) => {

  CustomerModel.aggregate([
    {
      $match: {
        _id: new mongoose.Types.ObjectId(req.params.id)
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$_id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$shop_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },
  ]).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        detail: cc.length ? cc[0] : {}
      }
    })
  }).catch((err) => {
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const getSingleShop = (req: Request, res: Response, next: NextFunction) => {

  ShopModel.aggregate([
    {
      $match: {
        $or: [
          {
            '_id': new mongoose.Types.ObjectId(req.params.id),
          },
          {
            'user_information': new mongoose.Types.ObjectId(req.params.id)
          }
        ]
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    },
    {
      '$lookup': {
        'from': 'offers',
        'localField': 'topics',
        'foreignField': 'service_id',
        'as': 'offers'
      }
    },
    {
      '$lookup': {
        'from': 'employees',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$deleted', false
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'employee'
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },
    { '$unset': ["time_slot.monday._id", "time_slot.tuesday._id", "time_slot.wednesday._id", "time_slot.thursday._id", "time_slot.friday._id", "time_slot.saturday._id", "time_slot.sunday._id"] },
    {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$review',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'total': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$review'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$review'
                  }
                ]
              }
            }
          }
        }
      }
    },
  ]).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        detail: cc.length > 0 ? cc[0] : {}
      }
    })
  }).catch((err) => {
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const getDateWiseBookingData = async (req: Request, res: Response, next: NextFunction) => {

  console.log('Admin', req.user, 'body', req.body)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  const startDate = new Date(req.body.startDate)
  const endDate = new Date(req.body.endDate)

  var Arr: any = getDateArray(startDate, endDate)

  console.log('mainD', Arr)

  Promise.all(Arr.map(async (cc: any, index: any) => {

    const mainD = await AdminModel.aggregate([
      {
        '$match': {
          _id: new mongoose.Types.ObjectId(val._id)
        }
      },
      {
        '$lookup': {
          'from': 'bookings',
          'pipeline': [
            {
              '$match': (req.body.shop == null || req.body.shop == undefined || req.body.shop == "" || req.body.shop.length == 0) ? {
                '$expr': {
                  '$and': [
                    {
                      '$gte': [
                        '$createdAt', new Date(new Date(cc).setHours(0o0, 0o0, 0o0))
                      ]
                    }, {
                      '$lt': [
                        '$createdAt', new Date(new Date(cc).setHours(23, 59, 59))
                      ]
                    },
                    {
                      '$eq': [
                        '$queue', req.body.type
                      ]
                    },
                  ]
                }
              } : {
                '$expr': {
                  '$and': [
                    {
                      '$gte': [
                        '$createdAt', new Date(new Date(cc).setHours(0o0, 0o0, 0o0))
                      ]
                    }, {
                      '$lt': [
                        '$createdAt', new Date(new Date(cc).setHours(23, 59, 59))
                      ]
                    },
                    {
                      '$eq': [
                        '$queue', req.body.type
                      ]
                    },
                    {
                      '$in': [
                        '$shop_id', req.body.shop.map((cc: any) => new mongoose.Types.ObjectId(cc))
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'booking'
        }
      },
      {
        '$addFields': {
          // 'appointment': {
          //   '$size': {
          //     '$filter': {
          //       'input': '$booking',
          //       'as': 'app',
          //       'cond': {
          //         '$eq': [
          //           '$$app.queue', false
          //         ]
          //       }
          //     }
          //   }
          // },
          // 'queue': {
          //   '$size': {
          //     '$filter': {
          //       'input': '$booking',
          //       'as': 'app',
          //       'cond': {
          //         '$eq': [
          //           '$$app.queue', true
          //         ]
          //       }
          //     }
          //   }
          // },
          'total': {
            '$size': '$booking'
          },
          'verify': {
            '$size': {
              '$filter': {
                'input': '$booking',
                'as': 'app',
                'cond': {
                  '$eq': [
                    '$$app.otp_verify', true
                  ]
                }
              }
            }
          },
          'notVerify': {
            '$size': {
              '$filter': {
                'input': '$booking',
                'as': 'app',
                'cond': {
                  '$eq': [
                    '$$app.otp_verify', false
                  ]
                }
              }
            }
          }
        }
      }, {
        '$lookup': {
          'from': 'servicerequests',
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$gte': [
                        '$createdAt', new Date(new Date(cc).setHours(0o0, 0o0, 0o0))
                      ]
                    }, {
                      '$lt': [
                        '$createdAt', new Date(new Date(cc).setHours(23, 59, 59))
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'service'
        }
      }, {
        '$addFields': {
          'sms': {
            '$size': {
              '$filter': {
                'input': '$service',
                'as': 'app',
                'cond': {
                  '$eq': [
                    '$$app.type', 'sms'
                  ]
                }
              }
            }
          },
          'email': {
            '$size': {
              '$filter': {
                'input': '$service',
                'as': 'app',
                'cond': {
                  '$eq': [
                    '$$app.type', 'email'
                  ]
                }
              }
            }
          },
          'totalservice': {
            '$size': '$service'
          }
        }
      },
    ]).exec()

    return { 'date': cc, 'value': mainD.length > 0 ? mainD[0] : {} }
  })).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { booking: cc }
    })
  }).catch((err) => {
    console.log('Primisee Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const getAllAdminBookingList = (req: Request, res: Response, next: NextFunction) => {

  console.log('admin', req.user)
  console.log('admin', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  BookingModel.aggregate([
    {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$shop_id.user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'shopreview'
      }
    }, {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$shopreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalShop': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$shopreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$shopreview'
                  }
                ]
              }
            }
          }
        }
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_id._id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$shop_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'userreview'
      }
    }, {
      '$addFields': {
        'user_count': {
          '$reduce': {
            'input': '$userreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalUser': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$userreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$user_count', {
                    '$size': '$userreview'
                  }
                ]
              }
            }
          }
        }
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'created_by',
        'foreignField': 'user_information',
        'as': 'shop'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'employee'
      }
    }, {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'booking': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'booking': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).exec((err, item) => {

    if (err) {
      console.log('Admin All Booking List Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }


    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        booking: [],
        total: 0
      }
    })
  })


}

// admin employee
export const getAllAdminEmployee = (req: Request, res: Response, next: NextFunction) => {

  EmployeeModel.aggregate([
    {
      '$match': {
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        }
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'employee': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'employee': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        employee: [],
        total: 0
      }
    })
  }).catch((err) => {
    console.log('Empoyee Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

// admin Ip Detail
export const getAllBookingIpDetail = (req: Request, res: Response, next: NextFunction) => {

  IpDetailModel.aggregate([
    {
      '$match': {
        'request_id': { $nin: [null] }
      }
    },
    {
      '$lookup': {
        'from': 'bookings',
        'localField': 'request_id',
        'foreignField': '_id',
        'as': 'request_id'
      }
    },
    {
      '$unwind': {
        'path': '$request_id'
      }
    },
    {
      '$lookup': {
        'from': 'customers',
        'localField': 'request_id.user_id',
        'foreignField': '_id',
        'as': 'request_id.user_id'
      }
    }, {
      '$unwind': {
        'path': '$request_id.user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'request_id.service_id',
        'foreignField': '_id',
        'as': 'request_id.service_id'
      }
    }, {
      '$unwind': {
        'path': '$request_id.service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'request_id.shop_id',
        'foreignField': 'user_information',
        'as': 'request_id.shop_id'
      }
    }, {
      '$unwind': {
        'path': '$request_id.shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'request_id.created_by',
        'foreignField': 'user_information',
        'as': 'request_id.shop'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'request_id.created_by',
        'foreignField': '_id',
        'as': 'request_id.customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'request_id.created_by',
        'foreignField': '_id',
        'as': 'request_id.employee'
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'detail': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'detail': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        detail: [],
        total: 0
      }
    })
  }).catch((err) => {
    console.log('Ip Detail Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

// admin ShortId Detail
export const getAllBookingShortIdDetail = (req: Request, res: Response, next: NextFunction) => {

  ShortUrlModel.aggregate([
    {
      '$match': {
        'product_id': { $nin: [null] }
      }
    },
    {
      '$lookup': {
        'from': 'bookings',
        'localField': 'product_id',
        'foreignField': '_id',
        'as': 'product_id'
      }
    },
    {
      '$unwind': {
        'path': '$product_id'
      }
    },
    {
      '$lookup': {
        'from': 'customers',
        'localField': 'product_id.user_id',
        'foreignField': '_id',
        'as': 'product_id.user_id'
      }
    }, {
      '$unwind': {
        'path': '$product_id.user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'product_id.service_id',
        'foreignField': '_id',
        'as': 'product_id.service_id'
      }
    }, {
      '$unwind': {
        'path': '$product_id.service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'product_id.shop_id',
        'foreignField': 'user_information',
        'as': 'product_id.shop_id'
      }
    }, {
      '$unwind': {
        'path': '$product_id.shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'product_id.created_by',
        'foreignField': 'user_information',
        'as': 'product_id.shop'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'product_id.created_by',
        'foreignField': '_id',
        'as': 'product_id.customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'product_id.created_by',
        'foreignField': '_id',
        'as': 'product_id.employee'
      }
    },
    {
      '$lookup': {
        'from': 'shortstats',
        'localField': 'short_id',
        'foreignField': 'key',
        'as': 'click'
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'detail': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'detail': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        detail: [],
        total: 0
      }
    })
  }).catch((err) => {
    console.log('Booking Short Detail Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}


export const readLogFile = (req: Request, res: Response, next: NextFunction) => {
  const fs = require('fs')

  var readStream = fs.createReadStream('src/log/info.log')

  readStream.on('open', () => {
    readStream.pipe(res)
  })

  readStream.on('error', function (err: Error) {
    console.log('checl', err)
    res.end(res.status(400).json({
      message: 'Failed',
      data: err
    }))
  });
}

export const deleteLogFile = (req: Request, res: Response, next: NextFunction) => {
  const fs = require('fs')
  fs.writeFile('src/log/info.log', '', (err: Error) => {
    if (err) {
      console.log('Log File Empty', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: ""
    })
  });

}

var getDateArray = function (start: any, end: any) {
  var arr = new Array();
  var dt = new Date(start);
  while (dt <= end) {
    arr.push(new Date(dt));
    dt.setDate(dt.getDate() + 1);
  }
  return arr;
}


export const SearchAdminBookingQuery = (req: Request, res: Response, next: NextFunction) => {

  BookingModel.aggregate([
    {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$shop_id.user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'shopreview'
      }
    }, {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$shopreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalShop': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$shopreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$shopreview'
                  }
                ]
              }
            }
          }
        }
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_id._id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$shop_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'userreview'
      }
    }, {
      '$addFields': {
        'user_count': {
          '$reduce': {
            'input': '$userreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalUser': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$userreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$user_count', {
                    '$size': '$userreview'
                  }
                ]
              }
            }
          }
        }
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'created_by',
        'foreignField': 'user_information',
        'as': 'shop'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'employee'
      }
    },
    {
      $match: {
        $or: [
          { "user_email": { $regex: req.params.id, $options: "i" } },
          { "shop_email": { $regex: req.params.id, $options: "i" } },
          { "shop_name": { $regex: req.params.id, $options: "i" } },
          { "status": { $regex: req.params.id, $options: "i" } },
          { "description": { $regex: req.params.id, $options: "i" } },
          { "create_type": { $regex: req.params.id, $options: "i" } },
          { "status": { $regex: req.params.id, $options: "i" } },
          { "service_id.name": { $regex: req.params.id, $options: "i" } },
          { "service_id.motivation": { $regex: req.params.id, $options: "i" } },
        ]
      }
    },
  ]).exec((err, item) => {

    if (err) {
      console.log('Admin All Booking List Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }


    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  })


}

// Log View
export const getAllLogList = (req: Request, res: Response, next: NextFunction) => {

  LogModel.aggregate([
    {
      '$match': {
        'deleted': false
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'log': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'log': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        log: [],
        total: 0
      }
    })
  }).catch((err) => {
    console.log('Log Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

// adminSingle Ip Detail
export const getSingleBookingIpDetail = (req: Request, res: Response, next: NextFunction) => {

  IpDetailModel.aggregate([
    {
      '$match': {
        'request_id': new mongoose.Types.ObjectId(req.params.id)
      }
    },
    {
      '$lookup': {
        'from': 'bookings',
        'localField': 'request_id',
        'foreignField': '_id',
        'as': 'request_id'
      }
    },
    {
      '$unwind': {
        'path': '$request_id'
      }
    },
    {
      '$lookup': {
        'from': 'customers',
        'localField': 'request_id.user_id',
        'foreignField': '_id',
        'as': 'request_id.user_id'
      }
    }, {
      '$unwind': {
        'path': '$request_id.user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'request_id.service_id',
        'foreignField': '_id',
        'as': 'request_id.service_id'
      }
    }, {
      '$unwind': {
        'path': '$request_id.service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'request_id.shop_id',
        'foreignField': 'user_information',
        'as': 'request_id.shop_id'
      }
    }, {
      '$unwind': {
        'path': '$request_id.shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'request_id.created_by',
        'foreignField': 'user_information',
        'as': 'request_id.shop'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'request_id.created_by',
        'foreignField': '_id',
        'as': 'request_id.customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'request_id.created_by',
        'foreignField': '_id',
        'as': 'request_id.employee'
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        detail: item.length > 0 ? item[0] : {}
      }
    })
  }).catch((err) => {
    console.log('Ip Detail Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

/// location Log
export const getAllLocationLogList = (req: Request, res: Response, next: NextFunction) => {

  LocationLogModel.aggregate([
    {
      '$match': {
        'deleted': false
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'log': [
          {
            '$sort': {
              '_id': -1
            }
          }, {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'log': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (item.length > 0) ? item[0] : {
        log: [],
        total: 0
      }
    })
  }).catch((err) => {
    console.log('Log Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

export const getSingleBooking = (req: Request, res: Response, next: NextFunction) => {
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  BookingModel.aggregate([
    {
      $match: {
        _id: new mongoose.Types.ObjectId(req.params.id)
      }
    },
    {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$shop_id.user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'shopreview'
      }
    }, {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$shopreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalShop': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$shopreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$shopreview'
                  }
                ]
              }
            }
          }
        }
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_id._id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$shop_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'userreview'
      }
    }, {
      '$addFields': {
        'user_count': {
          '$reduce': {
            'input': '$userreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalUser': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$userreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$user_count', {
                    '$size': '$userreview'
                  }
                ]
              }
            }
          }
        }
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'created_by',
        'foreignField': 'user_information',
        'as': 'shop'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'employee'
      }
    }
  ]).exec((err, item) => {

    if (err) {
      console.log('Admin Single Booking Detail Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }


    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        detail: (item.length > 0) ? item[0] : {}
      }
    })
  })
}