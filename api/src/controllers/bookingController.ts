import { NextFunction, Request, Response } from 'express';
import moment from 'moment';
import { userQueueAndBooking } from '../config/function';
import { bookingOTPMsg, generateHash, MAIL_SECRET_KEY, otpGenerate, SERVER_URL, ServiceSmsMail, ServiceTemplateId } from '../config/keys';
import { BookingModel } from '../Models/Booking'
import { createShortUrl } from '../modules/ShortUrl';
import mongoose from 'mongoose'
import { ShopModel } from '../Models/User'
import axios from 'axios';
import { saveIpDetail } from '../config/IpDetail';
import jwt from 'jsonwebtoken'

export const CreateBookingCustomer = async (req: Request, res: Response, next: NextFunction) => {

  console.log('Customer', req.user)
  console.log('Customer', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  const shopData = await ShopModel.findOne({ user_information: req.body.shop_id })

  console.log('body', req.body, 'shopData', shopData)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  var data = req.body
  data.user_id = val._id
  data.appointment_date = createDate(new Date(req.body.appointment_date), parseInt(req.body.appointment_time.replace(/\-.*/, '')))
  data.end_time = createDate(new Date(req.body.appointment_date), parseInt(req.body.appointment_time.replace(/\-.*/, '')))
  data.status = (!shopData || shopData == null || shopData == undefined) ? 'waiting' : (shopData.appointment == true) ? 'accepted' : 'waiting'
  data.otp = otpGenerate()
  data.hash = generateHash(data)

  const BookingData = new BookingModel({ ...data })
  BookingData.save().then(async (item: any) => {
    const locationD = await saveIpDetail(req, item._id, false)
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        _id: item._id,
        user_email: item.user_email,
        shop_email: item.shop_email,
        mobile: item.mobile,
        createdAt: item.createdAt,
      }
    })
  }).catch((err: Error) => {
    console.log('Booking Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })


}

export const verifyBookingOtp = async (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.body, 'Id', req.params.id)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  //const shopData = await BookingModel.findOne({_id:req.params.id})

  BookingModel.findOneAndUpdate({ _id: req.params.id, otp: parseInt(req.body.otp) }, {
    $set: {
      otp_verify: true
    }
  }, { new: true }).then(async (item: any) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Otp Verification Failed",
        data: item
      })
    }

    const ShortUrl = await createShortUrl(item._id)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err: Error) => {
    console.log('Booking Otp Verification Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })



}

export const getCustomerBookings = (req: Request, res: Response, next: NextFunction) => {
  console.log('Customer', req.user)
  console.log('Customer', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  // BookingModel.find({ user_id: val._id, end_time: { $lte: new Date() } }).populate('shop_id service_id').sort({ '_id': -1 })
  BookingModel.aggregate([
    {
      '$match': {
        'user_id': new mongoose.Types.ObjectId(val._id),
        'end_time': {
          '$lte': new Date()
        }
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'users',
        'localField': 'shop_id',
        'foreignField': '_id',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'review'
      }
    }, {
      '$sort': {
        '_id': -1
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { booking: item }
    })
  }).catch((err: Error) => {
    console.log('Booking Customer List Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const getShopBookings = (req: Request, res: Response, next: NextFunction) => {
  console.log('Shop', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  // BookingModel.find({ shop_id: val._id, end_time: { $lte: new Date() } }).populate('user_id service_id').sort({ '_id': -1 })

  BookingModel.aggregate([
    {
      '$match': {
        'shop_id': new mongoose.Types.ObjectId(val._id),
        'end_time': {
          '$lte': new Date()
        }
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'localField': '_id',
        'foreignField': 'booking_id',
        'as': 'review'
      }
    }, {
      '$sort': {
        '_id': -1
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { booking: item }
    })
  }).catch((err: Error) => {
    console.log('Booking Shop List Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

/// set Waiting Time Shop

export const addWaitingTime = (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.body, 'Id', req.params.id, 'shop', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  BookingModel.findOneAndUpdate({ _id: req.params.id, shop_id: val._id }, {
    $set: {
      ...req.body,
      waiting_verify: (req.body.status == 'waiting') ? false : true
    }
  }, { new: true }).then((item: any) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Update Booking Waiting Time Failed",
        data: item
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err: Error) => {
    console.log('Booking Waiting Time Add Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}

// Queue Waiting Approcimate Time and date Update
export const addQueueWaitingTime = async (req: Request, res: Response, next: NextFunction) => {


  console.log('body', req.body, 'Id', req.params.id, 'shop', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  const mainData = await BookingModel.findOne({ _id: req.params.id, shop_id: val._id })
  console.log('check Update', mainData, 'difference approx', req.body.difference_approx, parseInt(req.body.difference_approx) * 60000, 'new Date', new Date(req.body.approximate_date).getTime(), 'olddate', new Date(mainData.approximate_date).getTime())

  if (req.body.difference_approx > 0 && new Date(req.body.approximate_date).getTime() > new Date(mainData.approximate_date).getTime()) {
    console.log('One Matched')

    var ss = parseInt(req.body.difference_approx) * 60000;
    console.log('ss', ss)
    console.log('ss', mainData.approximate_date)
    console.log('ss', ss)

    const checkD1 = await BookingModel.updateMany({
      shop_id: val._id,
      queue: true,
      appointment_date: { $gte: new Date(mainData.appointment_date) },
      _id: { $nin: [req.params.id] },
      status: { $in: ['waiting', 'accepted'] }
    }, [{
      $set: {
        approximate_date: {
          $add: ['$approximate_date', ss]
        },
        waiting_date: {
          $add: ['$waiting_date', ss]
        },
        end_time: {
          $add: ['$waiting_date', ss]
        },
        waiting_number: {
          $add: ['$waiting_number', parseInt(req.body.difference_approx)]
        },
        // approximate_number: {
        //   $add: ['$approximate_number', parseInt(req.body.difference_approx)]
        // }
      }
    }])

    console.log('approx grearter', checkD1)

  } else if (req.body.difference_approx < 0 && new Date(req.body.approximate_date).getTime() < new Date(mainData.approximate_date).getTime()) {
    console.log('less then zero Matched')
    var ss = parseInt(req.body.difference_approx) * 60000;
    console.log('ss', ss)

    const checkD = await BookingModel.updateMany({
      shop_id: val._id,
      queue: true,
      appointment_date: { $gte: new Date(mainData.appointment_date) },
      _id: { $nin: [req.params.id] },
      status: { $in: ['waiting', 'accepted'] }
    }, [{
      $set: {
        approximate_date: {
          $add: ['$approximate_date', ss]
        },
        waiting_date: {
          $add: ['$waiting_date', ss]
        },
        end_time: {
          $add: ['$waiting_date', ss]
        },
        waiting_number: {
          $add: ['$waiting_number', parseInt(req.body.difference_approx)]
        },
        // approximate_number: {
        //   $add: ['$approximate_number', parseInt(req.body.difference_approx)]
        // }
      }
    }])


    console.log('approx less', checkD)

  }




  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  BookingModel.findOneAndUpdate({ _id: req.params.id, shop_id: val._id }, {
    $set: {
      ...req.body,
      waiting_verify: (req.body.status == 'waiting') ? false : true
    }
  }, { new: true }).then((item: any) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Update Queue Waiting Time Failed",
        data: item
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err: Error) => {
    console.log('Queue Waiting Time Add Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}

export const getSingleBooking = (req: Request, res: Response, next: NextFunction) => {
  console.log('Customer', req.ip)

  if (req.params.id == null || req.params.id == undefined || req.params.id == '') {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  BookingModel.findOne({ _id: req.params.id }).populate('user_id service_id shop_id').then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err: Error) => {
    console.log('Booking List Single Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}

export const getShopUserCurrentAppointment = async (req: Request, res: Response, next: NextFunction) => {
  console.log('Shop User', req.user)
  console.log('Shop User', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  // const changeData = await BookingModel.updateMany({ shop_id: val._id, end_time: { $lte: new Date() } }, {
  //   $set: {
  //     status: 'expired',
  //     expired: true
  //   }
  // })

  // console.log('Shop', changeData)
  //BookingModel.find({ shop_id: val._id, end_time: { $gte: moment().startOf('day') }, status: { $in: ['waiting', 'accepted'] } }).populate('user_id service_id').sort({ '_id': -1 })

  BookingModel.aggregate([
    {
      '$match': {
        'shop_id': new mongoose.Types.ObjectId(val._id),
        //'end_time': { $gte: moment().startOf('day') },
        'end_time': { $gte: new Date(new Date().setHours(0o0, 0o0, 0o0)) },
        'status': { $in: ['waiting', 'accepted'] },
        'otp_verify': true
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_id._id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$shop_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'userreview'
      }
    }, {
      '$addFields': {
        'user_count': {
          '$reduce': {
            'input': '$userreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalUser': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$userreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$user_count', {
                    '$size': '$userreview'
                  }
                ]
              }
            }
          }
        }
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'created_by',
        'foreignField': 'user_information',
        'as': 'shop'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'employee'
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'booking': [
          {
            '$sort': {
              '_id': -1
            }
          },
          {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'booking': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).exec((err, item) => {

    if (err) {
      console.log('Booking Shop Current Appointment List Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }


    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item[0] : { booking: [], total: 0 }
    })
  })
  // .catch((err: Error) => {
  //   console.log('Booking Shop Current Appointment List Err', err)
  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })

  // })

}

export const getCustomerCurrentAppointment = async (req: Request, res: Response, next: NextFunction) => {
  console.log('Customer', req.user)
  console.log('Customer', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  // const changeData = await BookingModel.updateMany({ user_id: val._id, end_time: { $lte: new Date() }, queue:false }, {
  //   $set: {
  //     status: 'expired',
  //     expired: true
  //   }
  // })

  // console.log('Customer', changeData)
  // BookingModel.find({ user_id: val._id, end_time: { $gte: moment().startOf('day') }, status: { $in: ['waiting', 'accepted'] } }).populate('shop_id service_id').sort({ '_id': -1 })

  BookingModel.aggregate([
    {
      '$match': {
        'user_id': new mongoose.Types.ObjectId(val._id),
        //'end_time': { $gte: moment().startOf('day') },
        'end_time': { $gte: new Date(new Date().setHours(0o0, 0o0, 0o0)) },
        'status': { $in: ['waiting', 'accepted'] },
        'otp_verify': true
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$shop_id.user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'shopreview'
      }
    }, {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$shopreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalShop': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$shopreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$shopreview'
                  }
                ]
              }
            }
          }
        }
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'booking': [
          {
            '$sort': {
              '_id': -1
            }
          },
          // {
          //   '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          // }, {
          //   '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          // }
        ]
      }
    }, {
      '$project': {
        'booking': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).exec((err, item) => {

    if (err) {
      console.log('Customer Current Appointment List Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }


    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item[0] : { booking: [], total: 0 }
    })
  })
  // .catch((err: Error) => {
  //   console.log('Customer Current Appointment List Err', err)
  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })

  // })

}

export const CreateQueueBookingCustomer = async (req: Request, res: Response, next: NextFunction) => {

  console.log('Customer', req.user)
  console.log('Customer', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  const shopData = await ShopModel.findOne({ user_information: req.body.shop_id })

  console.log('body', req.body, 'shopData', shopData)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  const MainCheck: any = await BookingModel.find({
    shop_id: req.body.shop_id, queue: true, appointment_date: { $gte: moment().startOf('day'), $lte: moment().endOf('day') }, status: { $in: ['waiting', 'accepted'] }, approximate_date: { $gte: new Date(req.body.appointment_date) },
    otp_verify: true
  }).sort({ _id: -1 }).limit(1)


  console.log('mainD Data', MainCheck)

  var mainD: any = (MainCheck.length > 0) ? MainCheck[0].approximate_date : createQueueDate(req.body.appointment_date, parseInt((!req.body.time || req.body.time == null || req.body.time == undefined) ? 0 : req.body.time))

  console.log('mainD', mainD, createQueueDate(req.body.appointment_date, parseInt((!req.body.time || req.body.time == null || req.body.time == undefined) ? 0 : req.body.time)))

  var data = req.body
  data.user_id = val._id
  data.queue = true
  data.waiting_time = (MainCheck.length) ? parseInt(minusDate(mainD, req.body.appointment_date)) + ' ' + 'min' : req.body.time
  data.waiting_number = (MainCheck.length) ? parseInt(minusDate(mainD, req.body.appointment_date)) : parseInt(req.body.time)
  data.waiting_key = 'min'
  data.waiting_date = mainD
  data.approximate_date = createQueueDate(mainD, parseInt((!req.body.newtime || req.body.newtime == null || req.body.newtime == undefined) ? 0 : req.body.newtime))
  data.approximate_time = req.body.approximate_time
  data.approximate_number = parseInt(req.body.approximate_time)
  data.approximate_key = req.body.approximate_time.replace(/[0-9]/g, '').replace(' ', '')
  data.waiting_verify = true
  data.end_time = mainD
  data.otp = otpGenerate()
  data.status = (!shopData || shopData == null || shopData == undefined) ? 'waiting' : (shopData.queue == true) ? 'accepted' : 'waiting'
  data.hash = generateHash(data)
  // data.appointment_date = createDate(new Date(req.body.appointment_date), parseInt(req.body.appointment_time.replace(/\-.*/, '')))
  // data.end_time = createDate(new Date(req.body.appointment_date), parseInt(req.body.appointment_time.replace(/\-.*/, '')))

  const BookingData = new BookingModel({ ...data })
  BookingData.save().then(async (item: any) => {
    const locationD = await saveIpDetail(req, item._id, true)
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        _id: item._id,
        user_email: item.user_email,
        shop_email: item.shop_email,
        mobile: item.mobile,
        createdAt: item.createdAt,
      }
    })
  }).catch((err: Error) => {
    console.log('Booking Create Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })


}

export const shopUserQueueList = (req: Request, res: Response, next: NextFunction) => {

  console.log('shop', req.user)
  console.log('shop', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  //  BookingModel.find({ shop_id: val._id, appointment_date: { $gte: moment().startOf('day'), $lte: moment().endOf('day') }, queue: true, status: { $in: ['waiting', 'accepted'] } }).populate('user_id service_id').sort({ 'end_time': -1 })

  BookingModel.aggregate([
    {
      '$match': {
        'shop_id': new mongoose.Types.ObjectId(val._id),
        'appointment_date': {
          '$gte': new Date(new Date().setHours(0o0, 0o0, 0o0)),
          '$lt': new Date(new Date().setHours(23, 59, 59))
        },
        'queue': true,
        'otp_verify': true,
        'status': { $in: ['waiting', 'accepted'] }
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_id._id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$shop_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'userreview'
      }
    }, {
      '$addFields': {
        'user_count': {
          '$reduce': {
            'input': '$userreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalUser': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$userreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$user_count', {
                    '$size': '$userreview'
                  }
                ]
              }
            }
          }
        }
      }
    },
    {
      '$lookup': {
        'from': 'shops',
        'localField': 'created_by',
        'foreignField': 'user_information',
        'as': 'shop'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'customer'
      }
    }, {
      '$lookup': {
        'from': 'employees',
        'localField': 'created_by',
        'foreignField': '_id',
        'as': 'employee'
      }
    },

    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'booking': [
          {
            '$sort': {
              'end_time': -1
            }
          },
          {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'booking': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).exec((err, item) => {

    if (err) {
      console.log('Customer Current Appointment List Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item[0] : { booking: [], total: 0 }
    })
  })
  // .catch((err: Error) => {
  //   console.log('Customer Current Appointment List Err', err)
  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })

  // })

}


export const CountBooking = (req: Request, res: Response, next: NextFunction) => {

  console.log('shop', req.user)
  console.log('shop', req.ip)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  userQueueAndBooking(val._id, (status: any, err: Error, data: any) => {

    if (err) {
      console.log('Customer Appointment & Queue Count Err', err)
      return res.status(status).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    res.status(status).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: data
    })

  })

  // BookingModel.countDocuments({ user_id: val._id, appointment_date: { $gte: moment().startOf('day'), $lte: moment().endOf('day') }, status: { $in: ['waiting', 'accepted'] } }).then((item) => {
  //   res.status(200).json({
  //     status: 'Success',
  //     status_code: res.statusCode,
  //     message: "Request Success",
  //     data: item
  //   })
  // }).catch((err: Error) => {
  //   console.log('Customer Appointment & Queue Count Err', err)
  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })

  // })

}

// Cancel User Booking
export const userCancelBooking = (req: Request, res: Response, next: NextFunction) => {

  if (req.params.id == "" || req.params.id == null || req.params.id == undefined || req.params.id == "null" || req.params.id == 'undefined') {
    return res.status(400).json({
      message: 'Request Failed',
      status: res.statusCode,
      data: "Id Not Found"
    })
  }

  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Something Went Wrong???",
      data: 'Token Not Found'
    })
  }

  //var otp = otpGenerate()

  BookingModel.findOneAndUpdate({ _id: req.params.id, user_id: val._id }, {
    $set: {
      cancel_otp: 123456,
      cancel_verify: true
    }
  }, { new: true }).then((item: any) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Booking Cancel Failed",
        data: item
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Otp Generated Please Verify",
      data: item
    })
  }).catch((err: Error) => {
    console.log('User Booking Cancel Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

// cancel Shop Booking
export const shopBookingCancel = async (req: Request, res: Response, next: NextFunction) => {

  if (req.params.id == "" || req.params.id == null || req.params.id == undefined || req.params.id == "null" || req.params.id == 'undefined') {
    return res.status(400).json({
      message: 'Request Failed',
      status: res.statusCode,
      data: "Id Not Found"
    })
  }

  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Something Went Wrong???",
      data: 'Token Not Found'
    })
  }

  const mainData = await BookingModel.findOne({ _id: req.params.id, shop_id: val._id })

  console.log('Shop Cancel', mainData)

  if (mainData != null || mainData !== undefined) {

    if (mainData.queue == true) {
      var ss = (mainData.approximate_key == 'min') ? parseInt(mainData.approximate_number) * 60000 : parseInt(mainData.approximate_number) * 60 * 60000

      console.log('SS', ss)

      const checkD = await BookingModel.updateMany({
        shop_id: val._id,
        queue: true,
        appointment_date: { $gte: new Date(mainData.appointment_date) },
        _id: { $nin: [req.params.id] },
        status: { $in: ['waiting', 'accepted'] }
      }, [{
        $set: {
          approximate_date: {
            $subtract: ['$approximate_date', ss]
          },
          waiting_date: {
            $subtract: ['$waiting_date', ss]
          },
          end_time: {
            $subtract: ['$waiting_date', ss]
          },
        }
      }])

      console.log('Check D Cancel Queue', checkD)

    }

  }

  BookingModel.findOneAndUpdate({ _id: req.params.id, shop_id: val._id }, {
    $set: {
      status: 'cancelled',
      expired: true
    }
  }, { new: true }).then((item: any) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Booking Cancel Failed",
        data: item
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err: Error) => {
    console.log('Shop Booking Cancel Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

//verrify user Cancel Booking
export const userVerifyCancelBooking = async (req: Request, res: Response, next: NextFunction) => {

  if (req.params.id == "" || req.params.id == null || req.params.id == undefined || req.params.id == "null" || req.params.id == 'undefined') {
    return res.status(400).json({
      message: 'Request Failed',
      status: res.statusCode,
      data: "Id Not Found"
    })
  }

  console.log('data', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Otp is Required",
      data: 'err'
    })
  }

  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Something Went Wrong???",
      data: 'Token Not Found'
    })
  }


  const mainData = await BookingModel.findOne({ _id: req.params.id, user_id: val._id, cancel_otp: parseInt(req.body.otp) })

  console.log('User Cancel', mainData)

  if (mainData != null || mainData !== undefined) {

    if (mainData.queue == true) {
      var ss = (mainData.approximate_key == 'min') ? parseInt(mainData.approximate_number) * 60000 : parseInt(mainData.approximate_number) * 60 * 60000

      console.log('SS', ss)

      const checkD = await BookingModel.updateMany({
        shop_id: mainData.shop_id,
        queue: true,
        appointment_date: { $gte: new Date(mainData.appointment_date) },
        _id: { $nin: [req.params.id] },
        status: { $in: ['waiting', 'accepted'] }
      }, [{
        $set: {
          approximate_date: {
            $subtract: ['$approximate_date', ss]
          },
          waiting_date: {
            $subtract: ['$waiting_date', ss]
          },
          end_time: {
            $subtract: ['$waiting_date', ss]
          },
        }
      }])

      console.log('Check D Cancel Queue', checkD)

    }

  }

  BookingModel.findOneAndUpdate({ _id: req.params.id, user_id: val._id, cancel_otp: parseInt(req.body.otp) }, {
    $set: {
      status: 'cancelled',
      expired: true
    }
  }, { new: true }).then((item: any) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Otp Verification Failed",
        data: item
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err: Error) => {
    console.log('User Verify Booking Cancel Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

export const shopBookingQuery = (req: Request, res: Response, next: NextFunction) => {
  console.log('Shop', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  console.log('start Date', req.body.startDate, new Date(req.body.startDate), 'endDate', req.body.endDate, new Date(req.body.endDate), moment(req.body.startDate).startOf('day'), moment(req.body.endDate).endOf('day'))

  BookingModel.aggregate([
    {
      '$match': {
        'shop_id': new mongoose.Types.ObjectId(val._id),
        'createdAt': (req.body.startDate == null || req.body.startDate == undefined) ? {
          '$lte': new Date()
        } : {
          // '$gte': moment(req.body.startDate).startOf('day'),
          // '$lte': moment(req.body.endDate).endOf('day')
          '$gte': new Date(new Date(req.body.startDate).setHours(0o0, 0o0, 0o0)),
          '$lt': new Date(new Date(req.body.endDate).setHours(23, 59, 59))
        },
        'status': {
          '$nin': [
            'waiting', 'accepted'
          ]
        },
        'otp_verify': true
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$_id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$booking_id', '$$main'
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },

    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'booking': [
          {
            '$sort': {
              '_id': -1
            }
          },
          {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'booking': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {


    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item[0] : { booking: [], total: 0 }
    })
  }).catch((err: Error) => {
    console.log('Shop Query start Date Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const customerBookingQuery = (req: Request, res: Response, next: NextFunction) => {
  console.log('Shop', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  console.log('start Date', req.body.startDate, new Date(req.body.startDate), 'endDate', req.body.endDate, new Date(req.body.endDate), moment(req.body.startDate).startOf('day'), moment(req.body.endDate).endOf('day'))

  BookingModel.aggregate([
    {
      '$match': {
        'user_id': new mongoose.Types.ObjectId(val._id),
        'createdAt': (req.body.startDate == null || req.body.startDate == undefined) ? {
          '$lte': new Date()
        } : {
          // '$gte': moment(req.body.startDate).startOf('day'),
          // '$lte': moment(req.body.endDate).endOf('day')
          '$gte': new Date(new Date(req.body.startDate).setHours(0o0, 0o0, 0o0)),
          '$lt': new Date(new Date(req.body.endDate).setHours(23, 59, 59))
        },
        'status': {
          '$nin': [
            'waiting', 'accepted'
          ]
        },
        'otp_verify': true
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$_id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$booking_id', '$$main'
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },

    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'booking': [
          {
            '$sort': {
              '_id': -1
            }
          },
          {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'booking': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {


    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item[0] : { booking: [], total: 0 }
    })
  }).catch((err: Error) => {
    console.log('Shop Query start Date Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const CreateBookingOtp = async (req: Request, res: Response, next: NextFunction) => {

  const BookingData = await BookingModel.findOne({ _id: req.params.id }).populate('service_id')

  if (BookingData == null || BookingData == undefined) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  try {

    const otp = otpGenerate()
    // const mainData = await axios.post('https://heytechemailsms.herokuapp.com/heytech/api/sendSms', {
    //   number: BookingData.mobile,
    //   otp: otp
    // });

    // console.log('Otp Sent', mainData)
    const msgT = await bookingOTPMsg({ service_id: BookingData.service_id, shop_name: BookingData.shop_name, otp: otp, bookingid: BookingData.bookingid, queue: BookingData.queue })
    const smsToken = jwt.sign({ date: new Date(), message: msgT, id: ServiceTemplateId.book }, MAIL_SECRET_KEY)

    if (BookingData.mobile == null || BookingData.mobile == undefined) {

      const emailOtp = await axios.post(ServiceSmsMail + 'sendOtpMail', {
        email: BookingData.user_email,
        otp: otp
      }, {
        headers: {
          'auth-token': smsToken
        }
      })



      console.log('Otp Sent', emailOtp)

    } else {
      const mobileOtp = await axios.post(ServiceSmsMail + 'smsSend', {
        number: BookingData.mobile,
        otp: otp
      }, {
        headers: {
          'auth-token': smsToken
        }
      })


      console.log('Otp Sent', mobileOtp)

    }

    const userData = await BookingModel.findOneAndUpdate({
      _id: req.params.id
    }, {
      $set: {
        otp: otp
      }
    }, { new: true })

    console.log('Otp Sent', userData)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: userData
    })

  } catch (err) {
    console.log('Create otp Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  }

}

export const getSingleQuery = (req: Request, res: Response, next: NextFunction) => {

  BookingModel.aggregate([
    {
      '$match': {
        '_id': new mongoose.Types.ObjectId(req.params.id)
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'service_id',
        'foreignField': '_id',
        'as': 'service_id'
      }
    }, {
      '$unwind': {
        'path': '$service_id'
      }
    }, {
      '$lookup': {
        'from': 'customers',
        'localField': 'user_id',
        'foreignField': '_id',
        'as': 'user_id'
      }
    }, {
      '$unwind': {
        'path': '$user_id'
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'shop_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$unwind': {
        'path': '$shop_id'
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$_id'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$booking_id', '$$main'
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },
  ]).then((item) => {


    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: { booking: item }
    })
  }).catch((err: Error) => {
    console.log('Shop Single Query start Date Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const assignEmployeeIdBooking = (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.body, 'Id', req.params.id, 'shop', req.user)
  const val: any = req.user
  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  BookingModel.findOneAndUpdate({ _id: req.params.id, shop_id: val._id }, {
    $set: {
      ...req.body
    }
  }, { new: true }).then((item: any) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Update Booking Employee Assign Failed",
        data: item
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err: Error) => {
    console.log('Booking Employee Assign Failed Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })
}

const createDate = (date: Date, time: any) => {
  var now = new Date(date);
  now.setDate(now.getDate())
  now.setHours(time);
  now.setMinutes(0);
  now.setMilliseconds(0);
  return now
}

// const changeData = await BookingModel.updateMany({ queue: null }, {
//   $set: {
//     queue: false
//   }
// })

// console.log('Shop', changeData)

const createQueueDate = (date: any, time: any) => {
  var now = new Date(date);
  console.log('now', now, date, time)
  now.setMinutes(now.getMinutes() + time);
  now.setMilliseconds(0);
  return now
}

const minusDate = (start: Date, end: Date) => {
  var now = new Date(start).getTime();
  var later = new Date(end).getTime();
  var result: any = (now - later) / 60000;
  return (result > 0) ? result : 0
}
