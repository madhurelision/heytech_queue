import { NextFunction, Request, Response } from 'express';
import { DescriptionTagModel } from '../Models/descriptionTag';
import { ReviewTagModel } from '../Models/ReviewTag';
import { ServiceTagModel } from '../Models/serviceTag';
import { TagModel } from '../Models/Tag';
import { TagServiceModel } from '../Models/tagService';
import { ShopModel } from '../Models/User';
import { ServiceModel } from "../Models/Service";
import { OfferModel } from '../Models/Offer';
import { BannerModel } from '../Models/Banner';

export const getAllCombinedTagList = (req: Request, res: Response, next: NextFunction) => {

  const Promise1 = new Promise((resolve, reject) => {
    DescriptionTagModel.find({ deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise2 = new Promise((resolve, reject) => {
    ReviewTagModel.find({ deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise3 = new Promise((resolve, reject) => {
    ServiceTagModel.find({ deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise4 = new Promise((resolve, reject) => {
    TagModel.find({ deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise5 = new Promise((resolve, reject) => {
    TagServiceModel.find({ deleted: false }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise6 = new Promise((resolve, reject) => {
    ShopModel.aggregate([
      {
        '$match': {
          'status': true
        }
      },
      {
        '$unwind': {
          'path': '$expertise'
        }
      }, {
        '$group': {
          '_id': 'null',
          'data': {
            '$push': '$expertise'
          }
        }
      }, {
        '$addFields': {
          'data': {
            '$map': {
              'input': '$data',
              'as': 'num',
              'in': {
                'label': '$$num',
                'value': '$$num'
              }
            }
          }
        }
      }
    ]).then((cc: any) => {
      resolve((cc.length > 0) ? cc[0].data : [])
    }).catch((err) => {
      reject(err)
    })
  })

  const Promise7 = new Promise((resolve, reject) => {
    ShopModel.aggregate([
      {
        '$match': {
          'status': true
        }
      }, {
        '$lookup': {
          'from': 'services',
          'localField': 'topics',
          'foreignField': '_id',
          'as': 'topics'
        }
      }, {
        '$unwind': {
          'path': '$topics'
        }
      }, {
        '$addFields': {
          'topics.user_name': '$shop_name'
        }
      }, {
        '$group': {
          '_id': null,
          'data': {
            '$push': '$topics'
          }
        }
      }
    ]).then((cc: any) => {
      resolve((cc.length > 0) ? cc[0].data : [])
    }).catch((err) => {
      reject(err)
    })
  })

  const Promise8 = new Promise((resolve, reject) => {
    ServiceModel.aggregate([
      {
        '$match': {
          'deleted': false
        }
      }, {
        '$lookup': {
          'from': 'shops',
          'localField': 'user_id',
          'foreignField': 'user_information',
          'as': 'shop_id'
        }
      }, {
        '$lookup': {
          'from': 'admins',
          'localField': 'admin_id',
          'foreignField': '_id',
          'as': 'admin'
        }
      }, {
        '$addFields': {
          'user_name': {
            '$arrayElemAt': [
              {
                '$cond': {
                  'if': {
                    '$eq': [
                      {
                        '$size': '$shop_id'
                      }, 1
                    ]
                  },
                  'then': '$shop_id.shop_name',
                  'else': '$admin.name'
                }
              }, 0
            ]
          }
        }
      }
    ]).then((item) => {
      resolve(item)
    }).catch((err) => {
      reject(err)
    })
  })

  const Promise9 = new Promise((resolve, reject) => {
    OfferModel.find({ deleted: false, status: true }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  const Promise10 = new Promise((resolve, reject) => {
    BannerModel.find({ deleted: false, status: true }).then((item: any) => {
      resolve(item)
    }).catch((err: Error) => {
      reject(err)
    })
  })

  Promise.allSettled([Promise1, Promise2, Promise3, Promise4, Promise5, Promise6, Promise7, Promise8, Promise9, Promise10]).then((cc: any) => {
    // console.log('ss', cc)
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        descriptionTagList: { tag: cc[0].value },
        reviewTagList: { tag: cc[1].value },
        serviceTagList: { tag: cc[2].value },
        tagList: { tag: cc[3].value },
        tagServiceList: { tag: cc[4].value },
        serviceList: cc[7].value,
        activeServiceList: cc[6].value,
        expertiseList: cc[5].value,
        offerList: { offer: cc[8].value },
        bannerList: { banner: cc[9].value }
      }
    })

  }).catch((err) => {
    console.log('Tag Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}