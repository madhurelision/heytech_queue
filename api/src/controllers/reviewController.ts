import { Request, Response, NextFunction } from 'express'
import { BookingModel } from '../Models/Booking';
import { ReviewModel } from "../Models/review";

export const makeReview = (req: Request, res: Response, next: NextFunction) => {
  console.log('Req.body', req.body, req.baseUrl, req.url)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields Are Required",
      data: 'err'
    })
  }
  var check: any = {}
  var url: any = req.url

  if (isNaN(url.match('/shop/makeReview'))) {
    check.shop_review = true
  } else {
    check.user_review = true
  }

  const reviewData = new ReviewModel({ ...req.body, ...check })

  reviewData.save().then((item: any) => {

    BookingModel.findOneAndUpdate({ _id: req.body.booking_id }, {
      $set: {
        ...check
      }
    }, { new: true }).then((cc: any) => {
      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Submitted",
        data: cc
      })
    }).catch((err: Error) => {
      console.log('Review Booking Update Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    })

  }).catch((err: Error) => {
    console.log('Review Insert Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}