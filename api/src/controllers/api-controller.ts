import { Request, Response } from 'express';
import { ShopModel } from '../Models/User';
import { TopicModel } from '../Models/Topics';
import { isValidObjectId } from 'mongoose';
import mongoose from 'mongoose'

// http://localhost:5000/api/get-mentors?expertise=Leadership
// export const getMentorsController = async (req: Request, res: Response) => {
//   const expertise = req.query.expertise?.toString() || 'All';
//   const topic: number = Number(req.query.topic?.toString() || -1);
//   let mentors_;
//   if (topic == -1 && expertise == 'All') mentors_ = await MentorModel.find({});
//   else if (expertise == 'All')
//     mentors_ = await MentorModel.find({ topics: topic });
//   else if (topic == -1)
//     mentors_ = await MentorModel.find({ expertise: expertise });
//   else
//     mentors_ = await MentorModel.find({ topics: topic })
//       .where('expertise')
//       .equals(expertise);

//   const mentors = mentors_.map(
//     ({
//       _id,
//       first_name,
//       last_name,
//       company,
//       job_title,
//       expertise,
//       image_link,
//       topics,
//     }) => {
//       return {
//         _id,
//         first_name,
//         last_name,
//         company,
//         job_title,
//         expertise,
//         image_link,
//         topics,
//       };
//     },
//   );

//   res.json(mentors);
// };

export const getMentorsController = async (req: Request, res: Response) => {
  const expertise = req.query.expertise?.toString() || 'All';
  const topic: any = (req.query.topic == 'All') ? 'All' : req.query.topic;
  const name = req.query.name?.toString() || 'All';

  console.log('expertise', expertise, 'topic', topic, 'name', name)

  let mentors_;
  if (topic == 'All' && expertise == 'All') mentors_ = await ShopModel.aggregate([
    {
      '$match': {
        'status': true
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'shopreview'
      }
    }, {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$shopreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalShop': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$shopreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$shopreview'
                  }
                ]
              }
            }
          }
        }
      }
    }]).exec();
  else if (expertise == 'All')
    mentors_ = await ShopModel.aggregate([
      {
        $match: {
          topics: { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
          status: true
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }]).exec();
  else if (topic == 'All')
    mentors_ = await ShopModel.aggregate([
      {
        $match: {
          expertise: expertise,
          status: true
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }]).exec();
  else
    // mentors_ = await ShopModel.find({ topics: topic })
    //   .where('expertise')
    //   .equals(expertise);
    mentors_ = await ShopModel.aggregate([
      {
        $match: {
          topics: { $in: topic.map((cc: any) => new mongoose.Types.ObjectId(cc)) },
          expertise: expertise,
          status: true
        }
      }, {
        '$lookup': {
          'from': 'reviews',
          'let': {
            'main': '$user_information'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$shop_id', '$$main'
                      ]
                    }, {
                      '$eq': [
                        '$user_review', true
                      ]
                    }
                  ]
                }
              }
            }
          ],
          'as': 'shopreview'
        }
      }, {
        '$addFields': {
          'shop_count': {
            '$reduce': {
              'input': '$shopreview',
              'initialValue': 0,
              'in': {
                '$sum': [
                  '$$value', '$$this.rating'
                ]
              }
            }
          }
        }
      }, {
        '$addFields': {
          'totalShop': {
            '$cond': {
              'if': {
                '$eq': [
                  {
                    '$size': '$shopreview'
                  }, 0
                ]
              },
              'then': 0,
              'else': {
                '$ceil': {
                  '$divide': [
                    '$shop_count', {
                      '$size': '$shopreview'
                    }
                  ]
                }
              }
            }
          }
        }
      }]).exec();

  const mentors = mentors_.map(
    ({
      _id,
      first_name,
      last_name,
      shop_name,
      company,
      job_title,
      expertise,
      image_link,
      topics,
      like,
      totalShop,
      banner
    }) => {
      return {
        _id,
        first_name,
        last_name,
        shop_name,
        company,
        job_title,
        expertise,
        image_link: image_link,
        topics,
        like,
        totalShop,
        banner
      };
    },
  );

  res.json(mentors);
};

// http://localhost:5000/api/get-topics?textSearch=a
export const getTopicsController = async (req: Request, res: Response) => {
  const searchString = req.query.textSearch?.toString() || '';
  let topics;
  if (searchString)
    topics = await TopicModel.find({ $text: { $search: searchString } });

  res.json(topics);
};

// http://localhost:5000/api/get-mentor?id=61a211ab8e41a1fc1c49c2a4
// export const getMentorController = async (req: Request, res: Response) => {
//   const id = req.query.id?.toString() || '';
//   let mentor;
//   if (id && isValidObjectId(id)) mentor = await MentorModel.findById(id);
//   res.json(mentor);
// };

// export const getMentorController = async (req: Request, res: Response) => {
//   const id = req.query.id?.toString() || '';
//   let mentor;
//   if (id && isValidObjectId(id)) mentor = await ShopModel.findById(id).populate('topics');
//   if (mentor || mentor != null || mentor != undefined || Object.keys(mentor).length > 0) {
//     if (mentor.image_link.match('https')) {
//       mentor.image_link = mentor.image_link
//     } else {
//       mentor.image_link = SERVER_URL + mentor.image_link
//     }
//     if (mentor.topics.length > 0) {
//       mentor.topics.map((cc: any) => {
//         if (cc.image.match(SERVER_URL)) {
//           cc.image = cc.image
//         } else {
//           cc.image = SERVER_URL + cc.image
//         }
//       })
//     }
//   }
//   res.json(mentor);
// };

export const getMentorController = async (req: Request, res: Response) => {
  const id = req.query.id?.toString() || '';
  let mentor: any;
  if (id && isValidObjectId(id))
    mentor = await ShopModel.aggregate([{
      '$match': {
        '_id': new mongoose.Types.ObjectId(id)
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    }, {
      '$lookup': {
        'from': 'galleries',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$status', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'gallery'
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    }
    ]).exec();

  //console.log('mentor', mentor)
  res.json(mentor?.length > 0 ? mentor[0] : {});
}