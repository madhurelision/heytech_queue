import { NextFunction, Request, Response } from 'express';
import { LOCAL_BASE_IMAGE_URL, SECRET_KEY, SERVER_URL, otpGenerate, CLIENT_URL, ServiceSmsMail, MAIL_SECRET_KEY, randomImageName } from '../config/keys';
import { ShopModel, UserModel } from '../Models/User';
import jwt from 'jsonwebtoken'
import { GalleryModel } from "../Models/Gallery";
import QRCode from 'qrcode';
import multer from 'multer';
import { upload } from '../config/uploadImage';
import { ImageUpload, removeImage } from '../ImageService/main';
import { Con } from '../ImageService/source';
import axios from 'axios';
import { isValidObjectId } from 'mongoose';
import mongoose from 'mongoose'
import { UserUpdateModel } from '../Models/UserUpdates'

export const google = async (req: Request, accessToken: String, refreshToken: String, params: any, profile: any, done: Function) => {
  console.log('Google Profile Data ', profile)
  console.log('Google Profile Params ', params)

  UserModel.findOne({ email: profile._json.email }).then((item: any) => {

    if (item == null || item == undefined) {

      const UserData = new UserModel(
        {
          first_name: profile._json.given_name,
          last_name: profile._json.family_name,
          email: profile._json.email,
          image_link: profile._json.picture,
          google: profile.id,
          oauth_provider: profile.provider,
          is_mentor: true,
          signup_completed: false,
        }
      )

      UserData.save().then((main: any) => {
        console.log('inserted record', main);
        //done(null, { _id: response.insertedId })
        done(null, main)

      }).catch((err: Error) => {
        console.log('Error occurred while inserting', err);
        done(err)
      })

    } else {

      UserModel.findOneAndUpdate({ email: profile._json.email }, {
        $set: {
          // first_name: profile._json.given_name,
          // last_name: profile._json.family_name,
          email: profile._json.email,
          google: profile.id,
          oauth_provider: profile.provider,
        }
      }, { new: true }).populate('mentor_information').then((cc: any) => {
        console.log('updated record', cc);
        //done(null, { _id: response.insertedId })
        done(null, cc)

      }).catch((err: Error) => {
        console.log('Error occurred while updating', err);
        done(err)

      })

    }

  }).catch((err: Error) => {
    console.log('Google Find error', err);
    return done(err)
  })

}
// export const createShopUser = (req: Request, res: Response, next: NextFunction) => {
//   console.log('image', req.file)
//   console.log('body', req.body)

//   const MainCheck = upload.single('image')

//   var data
//   var imgD = ''

//   if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

//     data = {
//       first_name: req.body.first_name,
//       last_name: req.body.last_name,
//       image_link: req.body.oldimage.replace(SERVER_URL, ""),
//       email: req.body.email,
//       company: req.body.company,
//       description: req.body.description,
//       expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
//       language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
//       instagram: req.body.instagram,
//       time_slot: JSON.parse(req.body.time_slot),
//       user_information: req.params.id,
//       waiting_number: req.body.waiting_number,
//       waiting_key: req.body.waiting_key,
//       waiting_time: req.body.waiting_time,
//       location: req.body.location,
//       lattitude: req.body.lattitude,
//       longitude: req.body.longitude,
//     }

//   } else {

//     if (req.file.mimetype === 'image/jpeg' || req.file.mimetype === 'image/png') {
//       imgD = `${LOCAL_BASE_IMAGE_URL}` + req.file.filename
//     }

//     data = {
//       first_name: req.body.first_name,
//       last_name: req.body.last_name,
//       image_link: imgD,
//       email: req.body.email,
//       company: req.body.company,
//       description: req.body.description,
//       expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
//       language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
//       instagram: req.body.instagram,
//       time_slot: JSON.parse(req.body.time_slot),
//       user_information: req.params.id,
//       waiting_number: req.body.waiting_number,
//       waiting_key: req.body.waiting_key,
//       waiting_time: req.body.waiting_time,
//       location: req.body.location,
//       lattitude: req.body.lattitude,
//       longitude: req.body.longitude,
//     }

//   }

//   const ShopData = new ShopModel(data)

//   ShopData.save().then((item: any) => {

//     UserModel.findOneAndUpdate({ _id: req.params.id }, {
//       $set: {
//         signup_completed: true,
//         mentor_information: item._id
//       }
//     }).then((val: any) => {

//       const token: any = jwt.sign({ _id: val._id, email: val.email }, SECRET_KEY)
//       res.header("auth-token", token)

//       res.status(200).json({
//         status: 'Success',
//         status_code: res.statusCode,
//         message: "Request Success",
//         data: val
//       })
//     }).catch((err) => {
//       console.log('uuser Merchant Id', err)
//       res.status(400).json({
//         status: 'Failed',
//         status_code: res.statusCode,
//         message: "Request Failed",
//         data: err
//       })
//     })
//   }).catch((err: Error) => {
//     console.log('Shop Create Err', err)
//     res.status(400).json({
//       status: 'Failed',
//       status_code: res.statusCode,
//       message: "Request Failed",
//       data: err
//     })
//   })
// }

export const createShopUser = (req: Request, res: Response, next: NextFunction) => {
  const MainCheck = upload.single('image')

  MainCheck(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 5 MB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('image', req.file)
    console.log('body', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        shop_name: req.body.shop_name,
        image_link: req.body.oldimage,
        email: req.body.email,
        company: req.body.company,
        description: req.body.description,
        expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
        language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
        instagram: req.body.instagram,
        time_slot: JSON.parse(req.body.time_slot),
        user_information: req.params.id,
        waiting_number: req.body.waiting_number,
        waiting_key: req.body.waiting_key,
        waiting_time: req.body.waiting_time,
        location: req.body.location,
        lattitude: req.body.lattitude,
        longitude: req.body.longitude,
        seat: req.body.seat,
        mobile: req.body.mobile,
      }

      ShopModel.findOneAndUpdate({ email: req.body.email }, {
        $set: {
          ...data
        }
      }, {
        upsert: true, setDefaultsOnInsert: true,
        new: true,
      }).then((item: any) => {

        console.log('main Item', item)

        UserModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            signup_completed: true,
            mentor_information: item._id,
            mobile: req.body.mobile,
            email: req.body.email,
          }
        }, { new: true }).then((val: any) => {

          const token: any = jwt.sign({ _id: val._id, email: val.email }, SECRET_KEY)
          res.header("auth-token", token)

          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: item,
            token: token
          })
        }).catch((err) => {
          console.log('uuser Merchant Id', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })
      }).catch((err: Error) => {
        console.log('Shop Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else {


      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then((item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        data = {
          first_name: req.body.first_name,
          last_name: req.body.last_name,
          shop_name: req.body.shop_name,
          image_link: item?.response.url,
          email: req.body.email,
          company: req.body.company,
          description: req.body.description,
          expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
          language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
          instagram: req.body.instagram,
          time_slot: JSON.parse(req.body.time_slot),
          user_information: req.params.id,
          waiting_number: req.body.waiting_number,
          waiting_key: req.body.waiting_key,
          waiting_time: req.body.waiting_time,
          location: req.body.location,
          lattitude: req.body.lattitude,
          longitude: req.body.longitude,
          seat: req.body.seat,
          mobile: req.body.mobile,
        }

        ShopModel.findOneAndUpdate({ email: req.body.email }, {
          $set: {
            ...data
          }
        }, {
          upsert: true, setDefaultsOnInsert: true,
          new: true,
        }).then((item: any) => {

          UserModel.findOneAndUpdate({ _id: req.params.id }, {
            $set: {
              signup_completed: true,
              mentor_information: item._id,
              mobile: req.body.mobile,
              email: req.body.email,
            }
          }).then((val: any) => {

            const token: any = jwt.sign({ _id: val._id, email: val.email }, SECRET_KEY)
            res.header("auth-token", token)

            res.status(200).json({
              status: 'Success',
              status_code: res.statusCode,
              message: "Request Success", data: item, token: token
            })
            removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
          }).catch((err) => {
            console.log('uuser Merchant Id', err)
            res.status(400).json({
              status: 'Failed',
              status_code: res.statusCode,
              message: "Request Failed",
              data: err
            })
          })
        }).catch((err: Error) => {
          console.log('Shop Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })

      }).catch((err) => {
        console.log('Shop User Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    }
  })
}

export const SingleUser = (req: Request, res: Response, next: NextFunction) => {

  UserModel.findOne({ _id: req.params.id }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}


export const getShopUserProfile = (req: Request, res: Response) => {
  console.log('Shop User', req.user)

  const val: any = req.user

  if (val == null || val == undefined || Object.keys(val).length == 0) {
    return res.status(400).json({
      status: "Failed",
      status_code: res.statusCode,
      message: "Invalid Token",
      data: 'Token Not Found'
    })
  }

  ShopModel.findOne({ user_information: val._id }).populate('topics').then((item) => {

    if (item == null || item == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: item
      })
    }

    var MentorData: any = item

    const token: any = jwt.sign({ _id: MentorData.user_information, email: MentorData.email }, SECRET_KEY)
    res.header("auth-token", token)

    // if (item || item != null || item != undefined || Object.keys(item).length > 0) {
    //   if (item.image_link.match('https')) {
    //     item.image_link = item.image_link
    //   } else {
    //     item.image_link = SERVER_URL + item.image_link
    //   }
    // }

    res.status(200).json({
      message: 'Success',
      status: res.statusCode,
      data: item,
      token: token,
      type: 'shop'
    })

  }).catch((err) => {
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const updateShopUser = async (req: Request, res: Response, next: NextFunction) => {
  // console.log('image', req.file)
  // console.log('body', req.body)

  // var data
  // var key: any = req.user
  // var imgD = ''

  // if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

  //   data = {
  //     first_name: req.body.first_name,
  //     last_name: req.body.last_name,
  //     image_link: req.body.oldimage.replace(SERVER_URL, ""),
  //     email: req.body.email,
  //     company: req.body.company,
  //     description: req.body.description,
  //     expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
  //     language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
  //     topics: (typeof req.body.topics == "string") ? [req.body.topics] : req.body.topics,
  //     instagram: req.body.instagram,
  //     time_slot: JSON.parse(req.body.time_slot),
  //     waiting_number: req.body.waiting_number,
  //     waiting_key: req.body.waiting_key,
  //     waiting_time: req.body.waiting_time,
  //     location: req.body.location,
  //     lattitude: req.body.lattitude,
  //     longitude: req.body.longitude,
  //   }

  // } else {

  //   if (req.file.mimetype === 'image/jpeg' || req.file.mimetype === 'image/png') {
  //     imgD = `${LOCAL_BASE_IMAGE_URL}` + req.file.filename
  //     const ImageCreate = await GalleryModel.create({
  //       image: imgD,
  //       user_id: key._id
  //     })
  //     console.log('Image Made', ImageCreate)
  //   }

  //   data = {
  //     first_name: req.body.first_name,
  //     last_name: req.body.last_name,
  //     image_link: imgD,
  //     email: req.body.email,
  //     company: req.body.company,
  //     description: req.body.description,
  //     expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
  //     language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
  //     topics: (typeof req.body.topics == "string") ? [req.body.topics] : req.body.topics,
  //     instagram: req.body.instagram,
  //     time_slot: JSON.parse(req.body.time_slot),
  //     waiting_number: req.body.waiting_number,
  //     waiting_key: req.body.waiting_key,
  //     waiting_time: req.body.waiting_time,
  //     location: req.body.location,
  //     lattitude: req.body.lattitude,
  //     longitude: req.body.longitude,
  //   }

  // }

  // ShopModel.findOneAndUpdate({ user_information: key._id }, {
  //   $set: {
  //     ...data
  //   }
  // }).then((cc) => {
  //   res.status(200).json({
  //     status: 'Success',
  //     status_code: res.statusCode,
  //     message: "Request Success",
  //     data: cc
  //   })
  // }).catch((err) => {
  //   console.log('Shop Update Err', err)
  //   res.status(400).json({
  //     status: 'Failed',
  //     status_code: res.statusCode,
  //     message: "Request Failed",
  //     data: err
  //   })
  // })

  const MainCheck = upload.single('image')

  MainCheck(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 5 MB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('image', req.file)
    console.log('body', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }


    console.log('image', req.file)
    console.log('body', req.body)

    var data: any
    var key: any = req.user

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        shop_name: req.body.shop_name,
        image_link: req.body.oldimage,
        email: req.body.email,
        company: req.body.company,
        description: req.body.description,
        expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
        language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
        //topics: (typeof req.body.topics == "string") ? [req.body.topics] : req.body.topics,
        instagram: req.body.instagram,
        time_slot: JSON.parse(req.body.time_slot),
        waiting_number: req.body.waiting_number,
        waiting_key: req.body.waiting_key,
        waiting_time: req.body.waiting_time,
        location: req.body.location,
        lattitude: req.body.lattitude,
        longitude: req.body.longitude,
        seat: req.body.seat,
      }


      ShopModel.findOneAndUpdate({ user_information: key._id }, {
        $set: {
          ...data
        }
      }).then(async (cc) => {
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: cc
        })


        const userUpdateNesw = await UserUpdateModel.create({
          type: 'Shop Update',
          old: cc,
          new: data,
          user_name: cc.shop_name,
          user_id: key._id
        })
      }).catch((err) => {
        console.log('Shop Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        // const ImageCreate = await GalleryModel.create({
        //   image: item?.response.url,
        //   user_id: key._id
        // })
        // console.log('Image Made', ImageCreate)

        data = {
          first_name: req.body.first_name,
          last_name: req.body.last_name,
          shop_name: req.body.shop_name,
          image_link: item?.response.url,
          email: req.body.email,
          company: req.body.company,
          description: req.body.description,
          expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
          language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
          //topics: (typeof req.body.topics == "string") ? [req.body.topics] : req.body.topics,
          instagram: req.body.instagram,
          time_slot: JSON.parse(req.body.time_slot),
          waiting_number: req.body.waiting_number,
          waiting_key: req.body.waiting_key,
          waiting_time: req.body.waiting_time,
          location: req.body.location,
          lattitude: req.body.lattitude,
          longitude: req.body.longitude,
          seat: req.body.seat,
        }


        ShopModel.findOneAndUpdate({ user_information: key._id }, {
          $set: {
            ...data
          }
        }).then(async (cc) => {
          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: cc
          })

          const userUpdateNew = await UserUpdateModel.create({
            type: 'Shop Update',
            old: cc,
            new: { ...data },
            user_name: cc.shop_name,
            user_id: key._id
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err) => {
          console.log('Shop Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Shop User Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    }


  })

}

export const getAllExpertise = (req: Request, res: Response, next: NextFunction) => {

  ShopModel.aggregate([
    {
      '$match': {
        'status': true
      }
    },
    {
      '$unwind': {
        'path': '$expertise'
      }
    }, {
      '$group': {
        '_id': 'null',
        'data': {
          '$push': '$expertise'
        }
      }
    }, {
      '$addFields': {
        'data': {
          '$map': {
            'input': '$data',
            'as': 'num',
            'in': {
              'label': '$$num',
              'value': '$$num'
            }
          }
        }
      }
    }
  ]).then((cc: any) => {
    //console.log('Data fetched', cc)
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (cc.length > 0) ? cc[0].data : []
    })
  }).catch((err) => {
    console.log('Shop Expertise Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}


// like Shop
export const likeShopUser = (req: Request, res: Response, next: NextFunction) => {

  console.log('req.body', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  ShopModel.findOneAndUpdate({ _id: req.params.id, like: { $nin: [req.body._id] } }, { $push: { like: req.body._id } }).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err) => {
    console.log('Unlike Shop User Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const getAllActiveServices = (req: Request, res: Response, next: NextFunction) => {

  ShopModel.aggregate([
    {
      '$match': {
        'status': true
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    }, {
      '$unwind': {
        'path': '$topics'
      }
    }, {
      '$addFields': {
        'topics.user_name': '$shop_name'
      }
    }, {
      '$group': {
        '_id': null,
        'data': {
          '$push': '$topics'
        }
      }
    }
  ]).then((cc: any) => {
    //console.log('Data fetched', cc)
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (cc.length > 0) ? cc[0].data : []
    })
  }).catch((err) => {
    console.log('Shop Active Service Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

//unlike Shop
export const unlikeShopUser = (req: Request, res: Response, next: NextFunction) => {

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "All Fields are Required",
      data: 'err'
    })
  }

  ShopModel.findOneAndUpdate({ _id: req.params.id }, { $pull: { like: req.body._id } }).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err) => {
    console.log('Unlike Shop User Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

// create ShopToken
export const createToken = (req: Request, res: Response, next: NextFunction) => {

  UserModel.findOne({ mentor_information: req.params.id }).then((cc: any) => {
    const token: any = jwt.sign({ _id: cc._id, email: cc.email }, SECRET_KEY)
    res.header("auth-token", token)
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: token
    })
  }).catch((err) => {
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const createOrChangeQrCode = (req: Request, res: Response, next: NextFunction) => {

  var key: any = req.user
  console.log('user', key)
  const urlString = SERVER_URL + '/api/open/' + key.mentor_information
  console.log('qrcode urle', urlString)

  QRCode.toDataURL(urlString, function (err, data) {

    if (err) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    console.log('Url Created After Qe', data)

    ShopModel.findOneAndUpdate({ user_information: key._id }, {
      $set: {
        code: data
      }
    }).then(async (cc) => {
      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: cc
      })

      const userUpdateNew = await UserUpdateModel.create({
        type: 'Shop QrCode',
        old: { code: cc.code },
        new: { code: data },
        user_name: cc.shop_name,
        user_id: key._id
      })

    }).catch((err) => {
      console.log('Shop Qrcode Update Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    })

  })


}

export const changeShopQueueStatus = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)

  var key: any = req.user
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  ShopModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      queue: req.body.queue
    }
  }).then(async (cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
    const userUpdateNew = await UserUpdateModel.create({
      type: 'Shop Queue Status',
      old: { queue: cc.queue },
      new: { queue: req.body.queue },
      user_name: cc.shop_name,
      user_id: key._id
    })
  }).catch((err) => {
    console.log('Shop Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}


export const changeShopAppointmentStatus = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  var key: any = req.user
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  ShopModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      appointment: req.body.appointment
    }
  }).then(async (cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })

    const userUpdateNew = await UserUpdateModel.create({
      type: 'Shop Appointment Status',
      old: { appointment: cc.appointment },
      new: { appointment: req.body.appointment },
      user_name: cc.shop_name,
      user_id: key._id
    })
  }).catch((err) => {
    console.log('Shop Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

/// quuery api

export const SearchShopQuery = (req: Request, res: Response, next: NextFunction) => {


  ShopModel.aggregate([
    {
      $match: {
        $text: {
          $search: req.params.id
        },
        status: true
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'shopreview'
      }
    }, {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$shopreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalShop': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$shopreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$shopreview'
                  }
                ]
              }
            }
          }
        }
      }
    }
  ]).exec((err, item) => {

    if (err) {
      console.log('Shop Query Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  })


}

export const SearchShopRegexQuery = (req: Request, res: Response, next: NextFunction) => {

  ShopModel.aggregate([
    {
      $match: {
        $or: [
          { "first_name": { $regex: req.params.id, $options: "i" } },
          { "last_name": { $regex: req.params.id, $options: "i" } },
          { "shop_name": { $regex: req.params.id, $options: "i" } },
          { "expertise": { $regex: req.params.id, $options: "i" } },
          { "company": { $regex: req.params.id, $options: "i" } },
          { "location": { $regex: req.params.id, $options: "i" } },
          { "description": { $regex: req.params.id, $options: "i" } },
          { "email": { $regex: req.params.id, $options: "i" } },
          { "banner": { $regex: req.params.id, $options: "i" } },
        ],
        status: true
      }
    }, {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'shopreview'
      }
    }, {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$shopreview',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'totalShop': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$shopreview'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$shopreview'
                  }
                ]
              }
            }
          }
        }
      }
    }
  ]).exec((err, item) => {

    if (err) {
      console.log('Shop Query Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  })


}

/// create Otp For Mobile Verify

export const CreateOtp = async (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  try {

    const otp = otpGenerate()
    //https://heytechemailsms.herokuapp.com
    //http://localhost:5003
    const msgR = 'Your Otp for Mobile Number Verify is is' + otp
    const smsTokens = jwt.sign({ date: new Date(), message: msgR }, MAIL_SECRET_KEY)
    const mainData = await axios.post(ServiceSmsMail + 'sendSms', {
      number: req.body.mobile,
      otp: otp
    }, {
      headers: {
        'auth-token': smsTokens
      }
    });

    console.log('Otp Sent', mainData)

    const userData = await UserModel.findOneAndUpdate({
      _id: req.params.id
    }, {
      $set: {
        mobile: parseInt(req.body.mobile),
        otp: otp,
        mobile_verify: false
      }
    })

    console.log('Otp Sent', userData)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: {
        number: req.body.mobile,
        otp: otp
      }
    })

  } catch (err) {
    console.log('Create otp Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  }

}


export const verifyOtp = (req: Request, res: Response, next: NextFunction) => {

  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  UserModel.findByIdAndUpdate({ _id: req.params.id, otp: parseInt(req.body.otp) }, {
    $set: {
      mobile_verify: true
    }
  }).then((cc) => {
    if (cc == null || cc == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    ShopModel.findOneAndUpdate({ user_information: req.params.id }, {
      $set: {
        mobile: cc.mobile,
        mobile_verify: true
      }
    }).then((item) => {

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: item
      })

    }).catch((err) => {
      console.log('Shop Mobile Update Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    })

  }).catch((err) => {
    console.log('Shop Mobile Update Err', err)
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const UpdateServiceInShop = (req: Request, res: Response, next: NextFunction) => {

  if (req.params.id == null || req.params.id == undefined || req.params.id == '') {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('body', req.body)

  var data: any
  var key: any = req.user

  data = {
    topics: (typeof req.body.topics == "string") ? [req.body.topics] : req.body.topics,
  }

  ShopModel.findOneAndUpdate({ user_information: req.params.id }, {
    $set: {
      ...data
    }
  }).then(async (cc) => {


    if (cc == null || cc == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    const userUpdateNew = await UserUpdateModel.create({
      type: 'Shop Service',
      old: { topics: cc.topics },
      new: { ...data },
      user_name: cc.shop_name,
      user_id: key._id
    })

    const token: any = jwt.sign({ _id: cc.user_information, email: cc.email }, SECRET_KEY)
    res.header("auth-token", token)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: '/shoplogin/' + token
    })

  }).catch((err) => {
    console.log('Shop Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const shopMobileLoginSignup = async (req: Request, res: Response, next: NextFunction) => {

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  console.log('body', req.body)

  const mobileUser = await UserModel.find({ mobile: req.body.mobile })

  console.log('mobile User', mobileUser)


  if (mobileUser.length > 0) {

    try {

      const otp = otpGenerate()
      const msgT = 'Your Otp for Shop Login is' + otp
      const smsToken = jwt.sign({ date: new Date(), message: msgT }, MAIL_SECRET_KEY)

      const mainData = await axios.post(ServiceSmsMail + 'sendSms', {
        number: req.body.mobile,
        otp: otp
      }, {
        headers: {
          'auth-token': smsToken
        }
      });


      console.log('Otp Sent', mainData)

      const userData = await UserModel.updateMany({
        mobile: req.body.mobile
      }, {
        $set: {
          otp: otp,
        }
      })

      console.log('Otp Sent', userData)

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: {
          redirect: false,
          data: mobileUser.map((cc) => {
            return {
              email: cc.email,
              mobile: cc.mobile,
              _id: cc._id
            }
          })
        }
      })



    } catch (err) {

      console.log('Create otp Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })

    }


  } else {

    try {


      const otp = otpGenerate()
      const msgR = 'Your Otp for Shop Login is' + otp
      const smsTokens = jwt.sign({ date: new Date(), message: msgR }, MAIL_SECRET_KEY)
      const mainData = await axios.post(ServiceSmsMail + 'sendSms', {
        number: req.body.mobile,
        otp: otp
      }, {
        headers: {
          'auth-token': smsTokens
        }
      });

      console.log('Otp Sent', mainData)

      const UserData = new UserModel(
        {
          mobile: req.body.mobile,
          mobile_login: true,
          otp: otp
        }
      )

      UserData.save().then((main: any) => {
        console.log('inserted record', main);
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: {
            redirect: false,
            data: [{
              email: main.email,
              mobile: main.mobile,
              _id: main._id
            }]
          }
        })


      }).catch((err: Error) => {
        console.log('Error occurred while Creating User', err);
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } catch (err) {

      console.log('Create otp Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })

    }


  }



}

export const shopMobileLoginOtpVerify = (req: Request, res: Response, next: NextFunction) => {


  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  UserModel.findOne({ mobile: req.body.mobile, _id: req.body.email, otp: parseInt(req.body.otp) }).then((cc) => {
    if (cc == null || cc == undefined) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var link

    if (cc.signup_completed == true) {
      const token: any = jwt.sign({ _id: cc._id, email: cc.email }, SECRET_KEY)
      res.header("auth-token", token)
      link = '/shoplogin/' + token;
    } else {
      link = '/register/' + cc._id;
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: link
    })

  }).catch((err) => {
    console.log('Shop Mobile Update Err', err)
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const updateShopByAdmin = async (req: Request, res: Response, next: NextFunction) => {

  const MainCheck = upload.single('image')

  MainCheck(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 5 MB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('image', req.file)
    console.log('body', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }


    console.log('image', req.file)
    console.log('body', req.body)

    var data

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        shop_name: req.body.shop_name,
        image_link: req.body.oldimage,
        email: req.body.email,
        company: req.body.company,
        description: req.body.description,
        expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
        language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
        instagram: req.body.instagram,
        time_slot: JSON.parse(req.body.time_slot),
        waiting_number: req.body.waiting_number,
        waiting_key: req.body.waiting_key,
        waiting_time: req.body.waiting_time,
        location: req.body.location,
        lattitude: req.body.lattitude,
        longitude: req.body.longitude,
        seat: req.body.seat,
      }


      ShopModel.findOneAndUpdate({ _id: req.params.id }, {
        $set: {
          ...data
        }
      }).then((cc) => {
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: cc
        })
      }).catch((err) => {
        console.log('Shop Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }

        // const ImageCreate = await GalleryModel.create({
        //   image: item?.response.url,
        //   user_id: key._id
        // })
        // console.log('Image Made', ImageCreate)

        data = {
          first_name: req.body.first_name,
          last_name: req.body.last_name,
          shop_name: req.body.shop_name,
          image_link: item?.response.url,
          email: req.body.email,
          company: req.body.company,
          description: req.body.description,
          expertise: (typeof req.body.expertise == "string") ? [req.body.expertise] : req.body.expertise,
          language: (typeof req.body.language == "string") ? [req.body.language] : req.body.language,
          instagram: req.body.instagram,
          time_slot: JSON.parse(req.body.time_slot),
          waiting_number: req.body.waiting_number,
          waiting_key: req.body.waiting_key,
          waiting_time: req.body.waiting_time,
          location: req.body.location,
          lattitude: req.body.lattitude,
          longitude: req.body.longitude,
          seat: req.body.seat,
        }


        ShopModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            ...data
          }
        }).then((cc) => {
          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: cc
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err) => {
          console.log('Shop Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Shop User Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })
    }


  })

}

export const createOrChangeQrCodeByAdmin = (req: Request, res: Response, next: NextFunction) => {

  var key: any = req.user
  console.log('user', key)
  const urlString = SERVER_URL + '/api/open/' + req.params.id
  console.log('qrcode urle', urlString)

  QRCode.toDataURL(urlString, function (err, data) {

    if (err) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    console.log('Url Created After Qe', data)

    ShopModel.findOneAndUpdate({ _id: req.params.id }, {
      $set: {
        code: data
      }
    }).populate('topics').then((cc) => {
      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: cc
      })
    }).catch((err) => {
      console.log('Shop Qrcode Update Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    })

  })


}

export const changeShopStatusByAdmin = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  ShopModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      ...req.body
    }
  }).then((cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Shop Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const shopCsvImport = async (req: Request, res: Response, next: NextFunction) => {

  if (!req.body || req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.data == null || req.body.data == undefined || req.body.data.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: 'Data Not Found',
      data: 'Data Not Found'
    })
  }

  var shopArr: any = []


  req.body.data.map((cc: any) => {
    if (cc.id == "" || cc.id == null || cc.id == undefined) {
      shopArr.push({
        id: cc.id,
        data: {
          first_name: cc["first_name"],
          last_name: cc["last_name"],
          shop_name: cc["shop_name"],
          image_link: cc["image_link"],
          email: cc["email"],
          company: cc["company"],
          description: cc["description"],
          expertise: cc["expertise"],
          language: cc["language"],
          instagram: cc["instagram"],
          topics: cc["topics"],
          time_slot: cc["time_slot"],
          waiting_key: cc["waiting_key"],
          waiting_number: cc["waiting_number"],
          location: cc["location"],
          lattitude: cc["lattitude"],
          longitude: cc["longitude"],
          mobile: cc["mobile"],
          seat: cc["seat"],
        }
      })
    } else {
      shopArr.push({
        id: cc.id,
        data: {
          first_name: cc["first_name"],
          last_name: cc["last_name"],
          shop_name: cc["shop_name"],
          image_link: cc["image_link"],
          email: cc["email"],
          company: cc["company"],
          description: cc["description"],
          expertise: cc["expertise"],
          language: cc["language"],
          instagram: cc["instagram"],
          topics: cc["topics"],
          time_slot: cc["time_slot"],
          waiting_key: cc["waiting_key"],
          waiting_number: cc["waiting_number"],
          location: cc["location"],
          lattitude: cc["lattitude"],
          longitude: cc["longitude"],
          mobile: cc["mobile"],
          seat: cc["seat"],
        },
      })
    }
  })

  const session = await mongoose.startSession();

  // const transactionOptions = {
  //   readPreference: 'primary',
  //   readConcern: { level: 'local' },
  //   writeConcern: { w: 'majority' }
  // };

  try {

    const shopSessionResults = await session.withTransaction(async () => {

      shopArr.map(async (cc: any) => {

        if (isValidObjectId(cc.id)) {

          const createS = await ShopModel.updateMany({ _id: cc.id }, { $set: cc.data }, { upsert: true, setDefaultsOnInsert: true, new: true }).session(session)

          console.log('Updated', createS)

        } else {

          const userData = await new UserModel({
            first_name: cc.data["first_name"],
            last_name: cc.data["last_name"],
            image_link: cc.data["image_link"],
            email: cc.data["email"],
            mobile: cc.data["mobile"],
            signup_completed: true
          })

          const shopData = await new ShopModel(cc.data)
          shopData.user_information = userData._id
          userData.mentor_information = shopData._id

          await userData.save({ session });
          console.log('User Created', userData)
          await shopData.save(session);
          console.log('Shop Created', shopData)
        }

      })


    })

    console.log('Session Values', shopSessionResults)

    if (shopSessionResults !== null) {
      console.log("The reservation was successfully created.");
      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Submitted",
        data: shopSessionResults
      })
    } else {
      console.log("The transaction was intentionally aborted.");
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: shopSessionResults
      })
    }

  } catch (err) {
    console.log("The transaction was aborted due to an unexpected error: " + err);
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  } finally {
    await session.endSession();
  }


}

export const getAllQueryActiveServices = (req: Request, res: Response, next: NextFunction) => {

  ShopModel.aggregate([
    {
      '$match': {
        'status': true
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    }, {
      '$unwind': {
        'path': '$topics'
      }
    }, {
      '$addFields': {
        'topics.user_name': '$shop_name'
      }
    },
    {
      $match: {
        $or: [
          { "topics.name": { $regex: req.params.id, $options: "i" } },
          { "topics.motivation": { $regex: req.params.id, $options: "i" } },
          { "topics.description": { $regex: req.params.id, $options: "i" } },
        ]
      }
    },
    {
      '$group': {
        '_id': null,
        'data': {
          '$push': '$topics'
        }
      }
    }
  ]).then((cc: any) => {
    //console.log('Data fetched', cc)
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: (cc.length > 0) ? cc[0].data : []
    })
  }).catch((err) => {
    console.log('Shop Active Service Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const SearchShopAdminRegexQuery = (req: Request, res: Response, next: NextFunction) => {

  ShopModel.aggregate([
    {
      $match: {
        $or: [
          { "first_name": { $regex: req.params.id, $options: "i" } },
          { "last_name": { $regex: req.params.id, $options: "i" } },
          { "shop_name": { $regex: req.params.id, $options: "i" } },
          { "expertise": { $regex: req.params.id, $options: "i" } },
          { "company": { $regex: req.params.id, $options: "i" } },
          { "location": { $regex: req.params.id, $options: "i" } },
          { "description": { $regex: req.params.id, $options: "i" } },
          { "email": { $regex: req.params.id, $options: "i" } },
          { "banner": { $regex: req.params.id, $options: "i" } },
        ],
        status: true
      }
    }, {
      '$lookup': {
        'from': 'services',
        'localField': 'topics',
        'foreignField': '_id',
        'as': 'topics'
      }
    },
    {
      '$lookup': {
        'from': 'employees',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$user_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$deleted', false
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'employee'
      }
    },
    {
      '$lookup': {
        'from': 'reviews',
        'let': {
          'main': '$user_information'
        },
        'pipeline': [
          {
            '$match': {
              '$expr': {
                '$and': [
                  {
                    '$eq': [
                      '$shop_id', '$$main'
                    ]
                  }, {
                    '$eq': [
                      '$user_review', true
                    ]
                  }
                ]
              }
            }
          }
        ],
        'as': 'review'
      }
    },
    { '$unset': ["time_slot.monday._id", "time_slot.tuesday._id", "time_slot.wednesday._id", "time_slot.thursday._id", "time_slot.friday._id", "time_slot.saturday._id", "time_slot.sunday._id"] },
    {
      '$addFields': {
        'shop_count': {
          '$reduce': {
            'input': '$review',
            'initialValue': 0,
            'in': {
              '$sum': [
                '$$value', '$$this.rating'
              ]
            }
          }
        }
      }
    }, {
      '$addFields': {
        'total': {
          '$cond': {
            'if': {
              '$eq': [
                {
                  '$size': '$review'
                }, 0
              ]
            },
            'then': 0,
            'else': {
              '$ceil': {
                '$divide': [
                  '$shop_count', {
                    '$size': '$review'
                  }
                ]
              }
            }
          }
        }
      }
    }
  ]).exec((err, item) => {

    if (err) {
      console.log('Shop Query Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  })


}