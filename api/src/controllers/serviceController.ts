import { ServiceModel } from "../Models/Service";
import { NextFunction, Request, Response } from 'express';
import { LOCAL_BASE_IMAGE_URL, SERVER_URL, randomImageName } from '../config/keys';
import { GalleryModel } from "../Models/Gallery";
import { upload } from '../config/uploadImage';
import multer from 'multer';
import { ImageUpload, removeImage } from '../ImageService/main';
import { Con } from '../ImageService/source';
import { isValidObjectId } from 'mongoose'
import { ShopModel } from "../Models/User";
import mongoose from 'mongoose'
import { UserUpdateModel } from "../Models/UserUpdates";


export const CreateService = async (req: Request, res: Response, next: NextFunction) => {
  console.log('image', req.file)

  const MainCheck = upload.single('image')

  MainCheck(req, res, async function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 500KB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    console.log('data', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data
    var key: any = req.user
    var imgD = ''
    var check: any = {}
    var url: any = req.url

    if (isNaN(url.match('/shop/createService'))) {
      check.user_id = key._id
    } else {
      check.admin_id = key._id
    }

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        image: req.body.oldimage.replace(SERVER_URL, ""),
        ...check,
        name: req.body.name,
        motivation: req.body.motivation,
        emojiIcon: req.body.emojiIcon,
        description: req.body.description,
        waiting_number: req.body.waiting_number,
        waiting_key: req.body.waiting_key,
        waiting_time: req.body.waiting_time,
        seat: req.body.seat,
        tags: req.body.tags,
      }


      const ServiceMde = new ServiceModel(data)

      ServiceMde.save().then((item: any) => {
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: item
        })
      }).catch((err: Error) => {
        console.log('Service Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })

      })

    } else {


      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }
        imgD = item?.response.url

        const ImageCreate = await GalleryModel.create({
          image: imgD,
          user_id: key._id,
          data: { ...req.file }
        })
        console.log('Image Made', ImageCreate)
        data = {
          name: req.body.name,
          motivation: req.body.motivation,
          image: imgD,
          ...check,
          emojiIcon: req.body.emojiIcon,
          description: req.body.description,
          waiting_number: req.body.waiting_number,
          waiting_key: req.body.waiting_key,
          waiting_time: req.body.waiting_time,
          seat: req.body.seat,
          tags: req.body.tags,
        }


        const ServiceMde = new ServiceModel(data)

        ServiceMde.save().then((item: any) => {
          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: item
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err: Error) => {
          console.log('Service Create Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })

        })

      }).catch((err) => {
        console.log('Service Create Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    }


  })

}

export const getAllService = (req: Request, res: Response, next: NextFunction) => {

  ServiceModel.aggregate([
    {
      '$match': {
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        }
      }
    }
  ]).then((item) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })
  }).catch((err) => {
    console.log('Service Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })


}

export const getShopService = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  ServiceModel.aggregate([
    {
      '$match': {
        'user_id': new mongoose.Types.ObjectId(key._id),
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        }
      }
    },
    {
      '$facet': {
        'metadata': [
          {
            '$group': {
              '_id': null,
              'total': {
                '$sum': 1
              }
            }
          }
        ],
        'service': [
          {
            '$sort': {
              '_id': 1
            }
          },
          {
            '$skip': (req.params.skip == null || req.params.skip == undefined || req.params.skip == "") ? 0 : parseInt(req.params.skip)
          }, {
            '$limit': (req.params.limit == null || req.params.limit == undefined || req.params.limit == "") ? 10 : parseInt(req.params.limit)
          }
        ]
      }
    }, {
      '$project': {
        'service': 1,
        'total': {
          '$arrayElemAt': [
            '$metadata.total', 0
          ]
        }
      }
    }
  ]).then((item) => {

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item.length > 0 ? item[0] : { service: [], total: 0 }
    })
  }).catch((err) => {
    console.log('Service Find Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  })

}

export const updateService = async (req: Request, res: Response, next: NextFunction) => {
  console.log('image', req.file)

  const MainCheck = upload.single('image')

  MainCheck(req, res, async function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 500KB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }

    const serviD: any = await ServiceModel.findOne({ _id: req.params.id })
    console.log('body', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data: any
    var key: any = req.user
    var imgD = ''

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        image: req.body.oldimage,
        name: req.body.name,
        motivation: req.body.motivation,
        emojiIcon: req.body.emojiIcon,
        description: req.body.description,
        waiting_number: req.body.waiting_number,
        waiting_key: req.body.waiting_key,
        waiting_time: req.body.waiting_time,
        seat: req.body.seat,
        tags: req.body.tags,
      }


      ServiceModel.findOneAndUpdate({ _id: req.params.id }, {
        $set: {
          ...data
        }
      }, { new: true }).then(async (cc) => {
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: cc
        })

        const userUpdateNesw = await UserUpdateModel.create({
          type: 'Service Update',
          old: serviD,
          new: data,
          service_name: serviD.name,
          user: false,
          user_id: key._id
        })

      }).catch((err) => {
        console.log('Service Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }
        imgD = item?.response.url

        const ImageCreate = await GalleryModel.create({
          image: imgD,
          user_id: key._id,
          data: { ...req.file }
        })
        console.log('Image Made', ImageCreate)
        data = {
          name: req.body.name,
          motivation: req.body.motivation,
          image: imgD,
          user_id: key._id,
          emojiIcon: req.body.emojiIcon,
          description: req.body.description,
          waiting_number: req.body.waiting_number,
          waiting_key: req.body.waiting_key,
          waiting_time: req.body.waiting_time,
          seat: req.body.seat,
          tags: req.body.tags,
        }


        ServiceModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            ...data
          },
        }, { new: true }).then(async (cc) => {
          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: cc
          })
          const userUpdateNew = await UserUpdateModel.create({
            type: 'Service Update',
            old: serviD,
            new: data,
            service_name: serviD.name,
            user: false,
            user_id: key._id
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err) => {
          console.log('Service Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Service Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    }

  })


}

export const updateAdminService = async (req: Request, res: Response, next: NextFunction) => {
  console.log('image', req.file)

  const MainCheck = upload.single('image')

  MainCheck(req, res, async function (err) {
    if (err instanceof multer.MulterError) {
      //console.log('Multer Error', err)
      if (err.code == 'LIMIT_FILE_SIZE') {
        err.message = 'File Size is too large. Allowed file size is 500KB';
      }
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: err.message,
        data: err
      })
      // A Multer error occurred when uploading.
    } else if (err) {
      //console.log('Other Error', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: 'Request Failed',
        data: err
      })
    }


    console.log('body', req.body)

    if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    var data: any
    var key: any = req.user
    var imgD = ''

    if (req.file == null || req.file == undefined || Object.keys(req.file).length == 0) {

      data = {
        image: req.body.oldimage,
        name: req.body.name,
        motivation: req.body.motivation,
        emojiIcon: req.body.emojiIcon,
        description: req.body.description,
        waiting_number: req.body.waiting_number,
        waiting_key: req.body.waiting_key,
        waiting_time: req.body.waiting_time,
        seat: req.body.seat,
        tags: req.body.tags,
      }


      ServiceModel.findOneAndUpdate({ _id: req.params.id }, {
        $set: {
          ...data
        }
      }, { new: true }).then(async (cc) => {
        res.status(200).json({
          status: 'Success',
          status_code: res.statusCode,
          message: "Request Success",
          data: cc
        })

      }).catch((err) => {
        console.log('Service Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    } else {



      ImageUpload(Con[randomImageName()], process.cwd() + "/src/upload/" + req.file.filename, req.file.filename).then(async (item) => {
        console.log('item', item)

        if (item == null || item == undefined) {
          return res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Image is Required",
            data: 'err'
          })
        }
        imgD = item?.response.url

        const ImageCreate = await GalleryModel.create({
          image: imgD,
          user_id: key._id,
          data: { ...req.file }
        })
        console.log('Image Made', ImageCreate)
        data = {
          name: req.body.name,
          motivation: req.body.motivation,
          image: imgD,
          user_id: key._id,
          emojiIcon: req.body.emojiIcon,
          description: req.body.description,
          waiting_number: req.body.waiting_number,
          waiting_key: req.body.waiting_key,
          waiting_time: req.body.waiting_time,
          seat: req.body.seat,
          tags: req.body.tags,
        }


        ServiceModel.findOneAndUpdate({ _id: req.params.id }, {
          $set: {
            ...data
          },
        }, { new: true }).then(async (cc) => {
          res.status(200).json({
            status: 'Success',
            status_code: res.statusCode,
            message: "Request Success",
            data: cc
          })
          removeImage(process.cwd() + "/src/upload/" + req.file?.filename)
        }).catch((err) => {
          console.log('Service Update Err', err)
          res.status(400).json({
            status: 'Failed',
            status_code: res.statusCode,
            message: "Request Failed",
            data: err
          })
        })


      }).catch((err) => {
        console.log('Service Update Err', err)
        res.status(400).json({
          status: 'Failed',
          status_code: res.statusCode,
          message: "Request Failed",
          data: err
        })
      })

    }

  })


}

export const changeServiceAcceptStatus = (req: Request, res: Response, next: NextFunction) => {
  console.log('body', req.body)
  var key: any = req.user
  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  ServiceModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      accept: req.body.accept
    }
  }).then(async (cc) => {
    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })

    const userUpdateNew = await UserUpdateModel.create({
      type: 'Service Update',
      old: { accpet: cc.accept },
      new: { accept: req.body.accept },
      service_name: cc.name,
      user: false,
      user_id: key._id
    })
  }).catch((err) => {
    console.log('Service Update Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const cloneService = async (req: Request, res: Response, next: NextFunction) => {

  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }


  var key: any = req.user

  const shopData = await ShopModel.findOne({ user_information: key._id })

  const serviceData = await ServiceModel.findOne({ _id: req.params.id })

  if (serviceData == null || serviceData == undefined) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  const newService = new ServiceModel(
    {
      emojiIcon: serviceData.emojiIcon,
      motivation: serviceData.motivation,
      name: serviceData.name,
      image: serviceData.image,
      description: serviceData.description,
      user_id: key._id,
      waiting_number: serviceData.waiting_number,
      waiting_key: serviceData.waiting_key,
      waiting_time: serviceData.waiting_time,
      seat: (shopData == null || shopData == undefined) ? serviceData.seat : shopData.seat,
      tags: serviceData.tags,
    }
  )

  newService.save().then(async (cc: any) => {
    // const allD = await ServiceModel.aggregate([
    //   {
    //     $match: {
    //       _id: new mongoose.Types.ObjectId(cc._id)
    //     }
    //   },
    //   {
    //     '$lookup': {
    //       'from': 'shops',
    //       'localField': 'user_id',
    //       'foreignField': 'user_information',
    //       'as': 'user_id'
    //     }
    //   }, {
    //     '$unwind': {
    //       'path': '$user_id'
    //     }
    //   },
    // ]).exec()

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: [cc]
    })

  }).catch((err: Error) => {
    console.log('Service Clone Err Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })

}

export const cloneAllService = async (req: Request, res: Response, next: NextFunction) => {

  console.log('req.body', req.body)

  if (req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.id == "" || req.body.id.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  var key: any = req.user

  try {

    const shopData = await ShopModel.findOne({ user_information: key._id })

    const allService = await ServiceModel.find({ _id: { $in: req.body.id } })

    if (allService.length == 0) {
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: 'err'
      })
    }

    const createAllServiceD = await ServiceModel.insertMany(allService.map((cc) => {
      return {
        emojiIcon: cc.emojiIcon,
        motivation: cc.motivation,
        name: cc.name,
        image: cc.image,
        description: cc.description,
        user_id: key._id,
        waiting_number: cc.waiting_number,
        waiting_key: cc.waiting_key,
        waiting_time: cc.waiting_time,
        seat: (shopData == null || shopData == undefined) ? cc.seat : shopData.seat,
        tags: cc.tags,
      }
    }))

    console.log('all Service Created', createAllServiceD)

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: createAllServiceD
    })

  } catch (err) {
    console.log('All Services Clone Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  }



}

export const deleteService = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  console.log('body', req.params.id)
  if (req.params.id == null || req.params.id == undefined || req.params.id == "") {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: 'err'
    })
  }

  ServiceModel.findOneAndUpdate({ _id: req.params.id }, {
    $set: {
      deleted: true
    }
  }, { new: true }).then((cc) => {

    if (cc == null || cc == undefined) {
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: cc
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: cc
    })
  }).catch((err) => {
    console.log('Service Deelete Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })
  })
}

export const serviceCsvImport = async (req: Request, res: Response, next: NextFunction) => {

  if (!req.body || req.body == null || req.body == undefined || Object.keys(req.body).length == 0 || req.body.data == null || req.body.data == undefined || req.body.data.length == 0) {
    return res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: 'Data Not Found',
      data: 'Data Not Found'
    })
  }

  var key: any = req.user
  var serviceArr: any = []

  req.body.data.map((cc: any) => {
    if (cc.id == "" || cc.id == null || cc.id == undefined) {
      serviceArr.push({
        id: cc.id,
        data: {
          name: cc["name"],
          motivation: cc["motivation"],
          emojiIcon: cc["emojiIcon"],
          seat: cc["seat"],
          waiting_key: cc["waiting_key"],
          waiting_number: cc["waiting_number"],
          description: cc["description"].trim(),
          image: cc["image"],
          user_id: isValidObjectId(cc["user_id"]) ? cc["user_id"] : null,
          admin_id: key._id,
          tags: cc["tags"],
        }
      })
    } else {
      serviceArr.push({
        id: cc.id,
        data: {
          name: cc["name"],
          motivation: cc["motivation"],
          emojiIcon: cc["emojiIcon"],
          seat: cc["seat"],
          waiting_key: cc["waiting_key"],
          waiting_number: cc["waiting_number"],
          description: cc["description"].trim(),
          image: cc["image"],
          tags: cc["tags"],
        },
      })
    }
  })

  try {

    Promise.all(serviceArr.map(async (cc: any) => {
      const createS = await ServiceModel.updateMany({ _id: isValidObjectId(cc.id) ? cc.id : new mongoose.Types.ObjectId() }, { $set: cc.data }, { upsert: true, setDefaultsOnInsert: true, new: true })
      console.log('Updated', createS)
      return createS
    })).then((cc) => {

      res.status(200).json({
        status: 'Success',
        status_code: res.statusCode,
        message: "Request Success",
        data: cc,
        check: serviceArr
      })

    }).catch((err) => {
      console.log('Service Csv Import Err', err)
      res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })

    })

  } catch (err) {

    console.log('Service Csv Import Err', err)
    res.status(400).json({
      status: 'Failed',
      status_code: res.statusCode,
      message: "Request Failed",
      data: err
    })

  }

}

/// quuery api

export const SearchServiceQuery = (req: Request, res: Response, next: NextFunction) => {


  ServiceModel.aggregate([
    {
      $match: {
        $text: {
          $search: req.params.id
        }
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        }
      }
    }
  ]).exec((err, item) => {

    if (err) {
      console.log('Service Query Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  })


}

export const SearchServiceRegexQuery = (req: Request, res: Response, next: NextFunction) => {

  ServiceModel.aggregate([
    {
      $match: {
        $or: [
          { "name": { $regex: req.params.id, $options: "i" } },
          { "motivation": { $regex: req.params.id, $options: "i" } },
          { "description": { $regex: req.params.id, $options: "i" } },
        ],
        deleted: false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        }
      }
    }
  ]).exec((err, item) => {

    if (err) {
      console.log('Service Query Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  })


}

export const SearchServiceShopIdRegexQuery = (req: Request, res: Response, next: NextFunction) => {
  var key: any = req.user
  ServiceModel.aggregate([
    {
      $match: {
        $or: [
          { "name": { $regex: req.params.id, $options: "i" } },
          { "motivation": { $regex: req.params.id, $options: "i" } },
          { "description": { $regex: req.params.id, $options: "i" } },
        ],
        'deleted': false,
        'user_id': new mongoose.Types.ObjectId(key._id),
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        }
      }
    }
  ]).exec((err, item) => {

    if (err) {
      console.log('Service Query Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  })


}

export const SearchAdminServiceRegexQuery = (req: Request, res: Response, next: NextFunction) => {

  ServiceModel.aggregate([
    {
      '$match': {
        'deleted': false
      }
    }, {
      '$lookup': {
        'from': 'shops',
        'localField': 'user_id',
        'foreignField': 'user_information',
        'as': 'shop_id'
      }
    }, {
      '$lookup': {
        'from': 'admins',
        'localField': 'admin_id',
        'foreignField': '_id',
        'as': 'admin'
      }
    }, {
      '$addFields': {
        'user_name': {
          '$arrayElemAt': [
            {
              '$cond': {
                'if': {
                  '$eq': [
                    {
                      '$size': '$shop_id'
                    }, 1
                  ]
                },
                'then': '$shop_id.shop_name',
                'else': '$admin.name'
              }
            }, 0
          ]
        }
      }
    },
    {
      $match: {
        $or: [
          { "name": { $regex: req.params.id, $options: "i" } },
          { "motivation": { $regex: req.params.id, $options: "i" } },
          { "description": { $regex: req.params.id, $options: "i" } },
          { "user_name": { $regex: req.params.id, $options: "i" } },
        ]
      }
    }
  ]).exec((err, item) => {

    if (err) {
      console.log('Service Query Find Err', err)
      return res.status(400).json({
        status: 'Failed',
        status_code: res.statusCode,
        message: "Request Failed",
        data: err
      })
    }

    res.status(200).json({
      status: 'Success',
      status_code: res.statusCode,
      message: "Request Success",
      data: item
    })

  })


}